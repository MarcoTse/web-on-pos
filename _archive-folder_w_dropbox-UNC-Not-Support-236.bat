@echo off
rem get local date time
for /F "usebackq tokens=1,2 delims==" %%i in (`wmic os get LocalDateTime /VALUE 2^>NUL`) do if '.%%i.'=='.LocalDateTime.' set ldt=%%j
rem set ldt=%ldt:~0,4%-%ldt:~4,2%-%ldt:~6,2% %ldt:~8,2%:%ldt:~10,2%:%ldt:~12,6%
set ldt=%ldt:~0,4%%ldt:~4,2%%ldt:~6,2%-%ldt:~8,2%%ldt:~10,2%%ldt:~12,2%
set destination=C:\_Public-Share\_Project_Bak
set zip_exe="C:\Program Files\7-Zip"
set config_file="\\10.0.0.236\nodejs\_bak"
set dropbox_bak="\\10.0.0.236\nodejs\_bak"

rem get the drag N drop filename and zip
set "arg=%*"
set "arg=%arg:)=^)%"
set "arg=%arg:(=^(%"
rem for %%a in (%arg%) do echo %%~fa
rem zip multiple folder by dragging to this batch file
echo %arg%
echo %destination%
echo %zip_exe%

rem for %%a in (%arg%) do echo "%destination%\%%~na-%ldt%.zip" "%%~na"

for %%a in (%arg%) do %zip_exe%\7z.exe a -tzip "%destination%\%%~na-%ldt%.zip" "%%~na" -xr@"%config_file%\archive_exclude.txt"

rem echo for %%a in (%arg%) do xcopy "%destination%\%%~na-%ldt%.zip" "%dropbox_bak%" /Y

for %%a in (%arg%) do xcopy "%destination%\%%~na-%ldt%.zip" "%dropbox_bak%" /Y

pause