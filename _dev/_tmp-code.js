    <div class="inline-block"><span ng-if="MainService.remainerAmount >= 0 && MainService.isCash">{{ ::ui[\'payment_change\'] }}</span><span ng-if="MainService.remainerAmount < 0">{{ ::ui[\'remainder\'] }}</span><span ng-if="MainService.remainerAmount >= 0 && MainService.isNotCash">{{ ::ui[\'tips\'] }}</span></div>\
    <span flex></span><div class="inline-block">{{ MainService.remainerAmount | currency : INFO.currency : 1  }}</div> \
</div> \

<div class="header"> \
  <h1 class="title">報表</h1> \
</div><!-- /header --> \
<!-- <textarea flex name="" readonly class="" style="resize: none; height:100%; width: 100%; outline: none;">report content</textarea> --> \
\
\
\
          <div flex="25" class="flex-center reminder">{{ ::ui[\'total_amount\'] + ui[\'colon\'] }}{{ MainService.Cart.totalPrice | currency : INFO.currency : 1 }}</div> \
          <div flex="25" class="flex-center reminder price-received">{{ ::ui[\'payment_received\'] + ui[\'colon\'] }}{{ MainService.inputPrice | currency : INFO.currency : 1  }}</div> \
          <div flex="25" class="flex-center reminder change">{{ ::ui[\'payment_change\'] + ui[\'colon\'] }}{{ MainService.remainerAmount | currency : INFO.currency : 1  }}</div> \
          <!-- note and remark --> \
          <div flex="15" class="flex-center reminder change">{{ ::ui[\'payment_method\'] + ui[\'colon\'] }}{{ MainService.paymentMethod || ui[\'select_payment_method\'] }}</div> \
          <md-button md-no-ink="true" class="md-grid-button md-raised proceed icon-with-text" ng-click="MainService.mode = MainService.schema.mode.floorPlan;"><i class="md-icon dp45">done</i>{{::ui["done"] || "完成"}}</md-button> \

          <!-- <div class="header left-edge-margin fill-height" style="height: 6%">{{ ::ui[\'total_amount\'] }}</div> --> \


                <pager-custom icon-size="\'dp56\'" grid-height="\'96px\'" total-items="MainService.totalCats" items-per-page="MainService.catsPerPage" ng-model="currentCatsPage" style="position: absolute; \
          bottom: 0; \
          right: 0; \
          width: calc(100% / 6 * 2); height: 97px !important;"></pager-custom> \
              </div> \
               \
              <div class="menu-option md-grid-container" style="position: absolute; bottom: 0; right: 0;"> \
                  <md-grid-list md-cols="5" md-row-height="64px" md-gutter="1" style=""> \
                      <!-- <md-grid-tile> \
                          <md-button md-no-ink="true" class="md-raised" ng-click="MainService.selectAllItem();">今天資料</md-button> \
                      </md-grid-tile> \
                      <md-grid-tile> \
                          <md-button md-no-ink="true" class="md-raised" ng-click="MainService.unselectAllItem();">過去資料</md-button> \
                      </md-grid-tile> --> \
                      <md-grid-tile> \
                           \
                      </md-grid-tile> \
                      <md-grid-tile> \
                          <md-button md-no-ink="true" class="md-raised cancel icon-with-text" ng-click="MainService.cancelDeleteItem();"><i class="md-icon dp45">clear</i>關閉</md-button> \
                      </md-grid-tile> \
                      <md-grid-tile> \
                        <md-button md-no-ink="true" class="md-raised proceed icon-with-text" ng-click="MainService.cancelDeleteItem();"><i class="md-icon dp45">print</i>列印</md-button> \
                      </md-grid-tile> \
                      <!--<md-grid-tile> \
                          <md-button md-no-ink="true" class="md-raised proceed" ng-click="MainService.menu.proceed();"><i class="md-icon dp45">done</i></md-button> \
                      </md-grid-tile>--> \
                  </md-grid-list> \
              </div> \

              <div flex id="report-option" class="md-grid-container" layout="column" style="  height: calc(96px * 5 + 1px * 5);"> \
                  <!-- report option grid --> \
                <md-grid-list md-cols="6" md-row-height="96px" md-gutter="1"> \
                    <!-- ng-if="category.items.length > 0" --> \
                    <md-grid-tile on-finish-render ng-repeat="option in dummy.reportOption"> \
                        <md-button md-no-ink="true" class="md-raised" ng-click="changeReport(\'{{::option.name}}\')">{{::ui[option.name]}}</md-button> \
                    </md-grid-tile> \
                </md-grid-list> \

Snap("#svg-canvas").selectAll("#tableGroup_0 .transform text").forEach(function(element, index, array){
	console.log(element.attr('text'))
})

<div class="col s4 left-edge"><button class="btn md-button md-grid-button" ng-click="MainService.setPaymentMethod(\'cash\')">{{::ui["cash_coupon_200"]}}</button></div> \
<div class="col s4"><button class="btn md-button md-grid-button" ng-click="MainService.setPaymentMethod(\'unionpay\')">{{::ui["cash_coupon_500"]}}</button></div> \
<div class="col s4 right-edge"><button class="btn md-button md-grid-button" ng-click="MainService.setPaymentMethod(\'cash_coupon_100\')">{{::ui["octopus"]}}</button></div> \
<div class="col s4 right-edge"><button class="btn md-button md-grid-button" ng-click="MainService.setPaymentMethod(\'off_10\')">{{::ui["off_10"]}}</button></div> \
<div class="col s4 right-edge"><button class="btn md-button md-grid-button" ng-click="MainService.setPaymentMethod(\'off_20\')">{{::ui["off_20"]}}</button></div> \
<div class="col s4 right-edge"><button class="btn md-button md-grid-button" ng-click="MainService.setPaymentMethod(\'off_30\')">{{::ui["off_30"]}}</button></div> \