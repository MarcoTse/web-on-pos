var async = require("async"),
    moment = require('moment'),
    TYPES = require('tedious').TYPES,
    path = require('path'),
    express = require('express'),
    bodyParser = require('body-parser'),
    app = express();
    http = require('http').createServer(handler),
    io = require('socket.io')(http),
    RAS = null,
    ConnectionPool = require('tedious-connection-pool'),
    Connection = require('tedious').Connection;
    config = require('./config/ras.json'),
    fs = require('fs'),
    xml2js = require('xml2js'),
    mqtt = require('mqtt'),
    S = require('string'),
    soap = require('soap'),
    server = require('http').createServer(handler),
    xml = require('fs').readFileSync('./public/Emenu.wsdl', 'utf8'),
    Request = require('tedious').Request;

app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded
app.listen(81);
http.listen(5001);

app.post('*', function (request, response) {
    var filePath = '.' + request.url;
    console.log("request.url", request.url);
    if (S(filePath).startsWith("./service.ashx")) {
        //var qs = merge(QueryString.parse(S(filePath).substr(15).s), request.body);
        //console.log("POST", qs);
        //serverHandler(qs, response);
        console.log("POST", request.body);
        //RAS.SetItemQty(request.body,response);
    }
});

function handler(req, res) {
    fs.readFile(__dirname + '/index.html',
  function (err, data) {
      if (err) {
          res.writeHead(500);
          return res.end('Error loading index.html');
      }

      res.writeHead(200);
      res.end(data);
  });
}

function getToday() {
    var today = new Date();
    today = new Date(today.getFullYear(), today.getMonth(), today.getDate());
    return today;
}

function getTommorow() {
    var today = getToday();
    var tommorow = new Date(today.getFullYear(), today.getMonth(), today.getDate() + 1);
    return tommorow;
}

new mqtt.Server(function(client) {
    var self = this;

    if (!self.clients) self.clients = {};

    client.on('connect', function(packet) {
        client.connack({returnCode: 0});
        client.id = packet.clientId;
        //console.log("CONNECT(%s): %j", client.id, packet);
        self.clients[client.id] = client;
    });

    client.on('publish', function(packet) {
        console.log("PUBLISH(%s): %j", client.id, packet);
        for (var k in self.clients) {
            self.clients[k].publish({topic: packet.topic, payload: packet.payload});
        }
        var payload = JSON.parse(packet.payload);
        console.log(payload.action);
        if (payload.action != undefined)
        {
            switch (payload.action) {
                case "updateTicket": {
                    RAS.broadcastTicketList(packet.topic);
                    break;
                }
                //case "printItem": {
                //    printItem(packet.payload);
                //    break;
                //}
                case "lockGroup": {
                    console.log('lock group start');
                    RAS.lockGroup(packet);
                }
            }
        }
        switch (packet.topic) {
            case "wo/dice/broadcastMessageRequest/": {
                RAS.broadcastDiceMessage();
                break;
            }
            case "wo/dice/addMessageRequest/": {
                RAS.addDiceMessage(packet.payload);
                break;
            }
            case "wo/dice/updateMessageRequest/": {
                RAS.updateDiceMessage(packet.payload);
                break;
            }
        }
    });

    client.on('subscribe', function(packet) {
        //console.log("SUBSCRIBE(%s): %j", client.id, packet);
        var granted = [];
        for (var i = 0; i < packet.subscriptions.length; i++) {
            granted.push(packet.subscriptions[i].qos);
        }

        client.suback({granted: granted, messageId: packet.messageId});
    });

    client.on('pingreq', function(packet) {
        //console.log('PINGREQ(%s)', client.id);
        client.pingresp();
    });

    client.on('disconnect', function(packet) {
        client.stream.end();
    });

    client.on('close', function(err) {
        delete self.clients[client.id];
    });

    client.on('error', function(err) {
        console.log('error!', err);

        if (!self.clients[client.id]) return;

        client.stream.end();
        console.dir(err);
    });
}).listen(process.argv[2] || 1884);

io.on('connection', function (socket) {
    socket.on('loadTableSchema', function (data, callback) {
        RAS.LoadTableSchema(data, function (err, result) { callback(JSON.stringify(result)) });
    });

    socket.on('login', function (data, callback) {
        RAS.Login(data, callback);
    });

    socket.on('adminLoadTableOrder', function (data, callback) {
        RAS.AdminLoadTableOrder(data, function (result) {
            //console.log("socket:" + result);
            callback(JSON.stringify(result))
        })
    });


    socket.on('loadItemQty', function (data, callback) {
        RAS.LoadItemQty(data, function (err,result) {
            //console.log("socket:" + JSON.stringify(result));
            callback(JSON.stringify(result));
        })
    });


    socket.on('resetItemQty',  (data,callback) => {
        RAS.ResetItemQty(data, (result) => {
            callback(JSON.stringify(result));
        });
    });

    socket.on('saveTableOrder', function (data, callback) {
        RAS.SaveTableOrder(data, function (result) {
            //console.log("socket:" + JSON.stringify(result));
            callback(JSON.stringify(result));
        })
    });

    socket.on('setItemQty',function(data,callback){
        console.log(data);
        RAS.SetItemQty(data, function(result){
            callback(JSON.stringify(result));
        })
    })


    socket.on('billTableOrder', function (data, callback) {
        RAS.BillTableOrder(data, function (result) {
            //console.log("socket:" + JSON.stringify(result));
            callback(JSON.stringify(result));
        })
    });


    socket.on('changeTable', function (data) {
        RAS.ChangeTable(data, function () { });
    });

    socket.on('print', function (data) {
        RAS.Print(data, function () { });
    });


    socket.on('loadStaff', function (data, callback) {
        RAS.LoadStaff(data, function (result) {
            console.log("socket:" + result);
            callback(JSON.stringify(result))
        })
    });


    socket.on('editStaff', function (data, callback) {
        console.log('editStaff start');
        console.log(data);
        RAS.EditStaff(data, function (result) {
            console.log("socket:" + result);
            callback(JSON.stringify(result))
        })
    });

    socket.on('deleteStaff', function (data, callback) {
        console.log('deleteStaff start');
        console.log(data);
        RAS.DeleteStaff(data, function (result) {
            console.log("socket:" + result);
            callback(JSON.stringify(result))
        })
    });

    socket.on('loadReport', (data, callback) => {
        RAS.LoadReport(data, (result) => {
            callback(JSON.stringify(result));
        });
    })

    socket.on('saveTableSchema', (data, callback) => {
        RAS.SaveTableSchema(data, (result) => {
            callback(JSON.stringify(result));
        });
    })

    socket.on('dailyClearance', (data, callback) => {
        RAS.DailyClearance(data, (result) => {
            callback(JSON.stringify(result));
        });
    })

    socket.on('loadBill', (data, callback) => {
        RAS.LoadBill(data, (result) => {
            callback(JSON.stringify(result));
        });
    })


    socket.on('amendBill', (data, callback) => {
        RAS.AmendBill(data, (result) => {
            callback(JSON.stringify(result));
        });
    })


    socket.on('printBill', (data, callback) => {
        RAS.PrintBill(data, (result) => {
            callback(JSON.stringify(result));
        });
    })

    socket.on('loadTableForTransfer', (data, callback) => {
        RAS.LoadTableForTransfer(data, (result) => {
            callback(JSON.stringify(result));
        });
    })

    socket.on('transferItem', (data, callback) => {
        RAS.TransferItem(data, (result) => {
            callback(JSON.stringify(result));
        });
    })

    socket.on('clockIn', (data, callback) => {
        RAS.clockIn(data, (result) => {
            callback(JSON.stringify(result));
        });
    });

    socket.on('clockOut', (data, callback) => {
        RAS.clockOut(data, (result) => {
            callback(JSON.stringify(result));
        });
    });

    socket.on('cashControl', (data, callback) => {
        RAS.CashControl(data, (result) => {
            callback(JSON.stringify(result));
        });
    });

    //socket.on('')

     //io.sockets.emit('setSeatFlag', JSON.stringify({ "t": '1.1', "f": "5" }));
});

app.use(express.static(path.join(__dirname, 'public')));
app.get('/', function (req, res) {
    res.sendFile(__dirname + '/index.html');
});


["log", "warn", "error"].forEach(function (method) {
    var oldMethod = console[method].bind(console);
    console[method] = function () {
        oldMethod.apply(
            console,
            [moment().format("HH:mm:ss")].concat([].map.call(arguments, function (value, index) { return value; })).concat('\r\n')
        );
    };
});


var SoapServices = {
    'Service': {
        'ServiceSoap': {
            'MDCallService': function (args, callback) {
                console.log("mmcall" + args.tableNum.$value + args.action.$value);
                var chars = "abcdefghijklmnopqurstuvwxyz";
                var randletter = chars.substr(Math.floor(Math.random() * 26), 1);
                var now = new Date();
                var uniqueid = randletter + now.getTime();
                var formattedTime = now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds();
                var type = "vip";
                var itemCode = "07001";
                switch (args.action.$value) {
                    case "_A": type = "bill"; itemCode = "07001"; break;
                    case "_B": type = "bill"; itemCode = "07002";
                    case "_C": type = "bill"; itemCode = "07003"; break;
                    case "_D": type = "service"; itemCode = "07004"; break;
                }
                tableFlagArray.push({ id: uniqueid, tableNo: args.tableNum.$value, type: type });
                // console.log(Date.now());
                console.log("formattedTime", formattedTime);
                var tdate = new Date();
                tdate.addHours(-8);
                var _o = { 'msg': [{ action: "addDish", id: uniqueid, tableno: args.tableNum.$value, itemCode: itemCode, dish: "1", time: formattedTime, staff: "", date: "T" + moment().format("HH:mm:ss") }] };
                //2015-03-20T11:12:13.754Z
                mqttClient.publish("wo/dice/broadcastMessageResponse/", JSON.stringify(_o), { qos: 2 }, function (messageID) { });
                io.sockets.emit('setTableFlag', JSON.stringify({ "tableNo": args.tableNum.$value, "type": type }));
                //socket.emit('publish', { topic: "wo/gm/all", msg: "{\"action\":\"callService\",\"id\":\"" + uniqueid + "\",\"tableno\":\"" + args.tableNum.$value + "\",\"time\":\"" + formattedTime + "\"}" });
                callback({
                    'MDCallServiceResponse': {
                        'MDCallServiceResult': "<***><result>OK</result><timestamp>" + new Date().getTime() + "</timestamp></***>"
                    }
                });
                RMSManager.Print({ tableNo: args.tableNum.$value, action: 'bill' }, function () { })
            },
            'MDLoadTableOrder': function (args, callback) {
                RAS.AdminLoadTableOrder(args, function (r) {
                    var builder = new xml2js.Builder({ renderOpts: { 'newline': '' }, headless: true, rootName: "***" });
                    var xml = builder.buildObject(r);
                    console.log(xml);
                    callback({
                        'MDLoadTableOrderResponse': {
                            'MDLoadTableOrderResult': xml
                        }
                    });
                });
            },
            'MDSaveTableOrder': function (args, callback) {
                var parser = new xml2js.Parser();
                parser.parseString(args.itemData.$value, function (err, r) {
                    args.itemData.json = r;
                    RAS.SaveTableOrder(args, function (r) {
                        var builder = new xml2js.Builder({ renderOpts: { 'newline': '' }, headless: true, rootName: "***" });
                        var xml = builder.buildObject(r);
                        console.log(xml);
                        callback({
                            'MDSaveTableOrderResponse': {
                                'MDSaveTableOrderResult': xml
                            }
                        });
                    });
                });
            },
            'MDLinkTable': function (args, callback) {
                console.log("mdlinktable----------------------tableNum");

                RAS.LinkTable(args, function (r) {
                    var builder = new xml2js.Builder({ renderOpts: { 'newline': '' }, headless: true, rootName: "***" });
                    var xml = builder.buildObject(r);
                    console.log(xml);
                    callback({
                        'MDLinkTableResponse': {
                            'MDLinkTableResult': xml
                        }
                    });
                });
                //RMSManager.LindTable(args, callback);
                //console.log(args);
                //console.log("D:" + args.Details.$value + " R:" + args.Reference.$value + " A:" + args.AccountID.$value);
                //var t = "D:" + args.Details.$value + " R:" + args.Reference.$value + " A:" + args.AccountID.$value;
                //writeLog(":[SetItemSoldOutStatus]" + t + " \r\n");
                //var parser = new xml2js.Parser();
                //parser.parseString(args.Details.$value, function (err, result) {
                //    console.log(err);
                //    console.log(result);
                //    var code = 0, message = "Success";
                //    if (err != null) {
                //        code = 1;
                //        message = "Invalid details xml format";
                //    } else {
                //        //OutOfStock = r;
                //        //console.log(result.Items.Item[0].ItemCode[0].length);
                //        var temp = [];
                //        try {
                //            for (var i = 0; i < result.Items.Item.length; i++) {

                //                console.log(result.Items.Item[i].ItemCode[0]);
                //                temp.push(result.Items.Item[i].ItemCode[0]);
                //            }
                //        } catch (e) { console.log(e); }
                //        OutOfStock = temp;

                //    }
                //    callback({ "Code": code, "Message": message });
                //});

                //callback({
                //    'MDLinkTableResponse': {
                //        'MDLinkTableResult': "<***><result>OK</result><timestamp>" + new Date().getTime() + "</timestamp><taskID>1</taskID><note></note><order><version>1.0</version><header><tableNum>" + args.tableNum.$value + "</tableNum><member></member><memberName></memberName><memberMsg></memberMsg></header>" + "</order>" + "</***>"
                //    }
                //});
            }
        }
    }
}

server.listen(8008);
//server.listen(8001);
soap.listen(server, '/Service/EmenuService', SoapServices, xml);
soap.listen(server, '/services/EmenuService', SoapServices, xml);

function DatabaseManager(connString) {
    this.connString = connString;
    this.pool = new ConnectionPool({}, this.connString);
    var that = this;

    function es(sql, callback,args) {
        var jsonArray = [];
        that.pool.acquire(function (err, connection) {
            if (err){
                console.error(err);
                callback(err, null);
            } else {
                //use the connection as normal
                var request = new Request(sql, function (err, rowCount) {
                    if (err)
                        console.error(err);
                    //console.log('rowCount: ' + rowCount);
                    //release the connection back to the pool when finished
                    connection.release();
                    callback(err, jsonArray);
                });

                request.on('row', function (columns) {
                    var rowObject = {};
                    columns.forEach(function (column) {
                        rowObject[column.metadata.colName] = column.value;
                    });
                    jsonArray.push(rowObject)
                });
                if (args != null) {
                    if (args.length != 0) {
                        for (var i = 0; i < args.length; i++) {
                            if (args[i].o) {
                                request.addParameter(args[i].c, args[i].t, args[i].v, args[i].o);
                            } else {
                                request.addParameter(args[i].c, args[i].t, args[i].v);
                            }
                        }
                    }
                }
                connection.execSql(request);
            }
        });
    }

    this.executeStatement = function (sql, callback, type) {
        if (Array.isArray(sql)) {
            var functions = [];
            for (var i = 0; i < sql.length; i++) {
                var cf = function createfunc(i) {
                    return function (callback) {
                        es(sql[i].sql, callback, sql[i].args);
                    }
                }
                var f = cf(i)
                functions.push(f);
            }
            switch (type) {
                case "parallel":
                    async.parallel(functions,
                    // optional callback
                    function (err, results) {
                        // the results array will equal ['one','two'] even though
                        // the second function had a shorter timeout.
                        if (err) {
                            console.log(err);
                        }
                        callback(err, results);
                    });
                    break;
                default:
                    async.series(functions,
                    // optional callback
                    function (err, results) {
                        // the results array will equal ['one','two'] even though
                        // the second function had a shorter timeout.
                        if (err) {
                            console.log(err);
                        }
                        callback(err, results);
                    });
            }
        } else {
            es(sql.sql, callback, sql.args);
        }


    }
}

function RASManager(connString, options) {
    this.DM = new DatabaseManager(connString);
    this.GenerateMenu = function () {
        //this.DM.executeStatement(['select * from item where code = @code', 'select * from category'], [[{ c: 'code', t: TYPES.VarChar, v: 'AA02' }], null]
        var sql = new Array();
        sql.push({ "sql": "select * from item", "args": null });
        sql.push({ "sql": "select * from category order by seq", "args": null });
        sql.push({ "sql": "select * from itemcategory", "args": null });
        sql.push({ "sql": "select * from pricebatch pb  inner join pricebatchd pbd on pb.pricebatchid = pbd.pricebatchid where activatedate <= getdate() and (expiredate >= GETDATE() or expiredate is null)", "args": null });
        sql.push({ "sql": "select * from itemTimeRange itr inner join timeRange tr on itr.timeRangeId = tr.timeRangeId", "args": null });
        sql.push({ "sql": "select name1,name2,name3,namek,m.goMenu as mgoMenu, mg.goMenu as mggoMenu,mg.modifierGroupCode,m.modifierCode from modifier m inner join modifierGroupModifier mgm on m.modifierCode = mgm.modifierCode inner join modifierGroup mg on mg.modifierGroupCode = mgm.modifierGroupCode", "args": null })
        sql.push({ "sql": "select * from itemModifierGroup", "args": null })
        sql.push({ "sql": "select * from itemOptionGroup", "args": null })
        sql.push({ "sql": "select o.name1,o.name2,o.name3,o.namek,o.goMenu as ogoMenu, og.goMenu as oggoMenu,o.optionCode, og.optionGroupCode,og.name1 as ogName1, og.name2 as ogName2, og.name3 as ogName3 from [option] o inner join [optionGroupOption] ogo on o.optionCode = ogo.optionCode inner join optionGroup og on og.optionGroupCode = ogo.optionGroupCode", "args": null })
        sql.push({ "sql": "select * from itemSubItem", "args": null })
        sql.push({ "sql": "select * from subItem", "args": null })
        sql.push({ "sql": "select * from kitchenMsg", "args": null })

        //args = [[{ c: 'code', t: TYPES.VarChar, v: 'AA02' }], null, null, null];
        this.DM.executeStatement(sql, (err, obj) => {
            //console.log(JSON.stringify(obj));
            var items = obj[0], category = obj[1], itemcategory = obj[2], pricebatch = obj[3], timeRange = obj[4], modifier = obj[5], itemModifier = obj[6], itemOption = obj[7], option = obj[8], itemSubitem = obj[9], subitem = obj[10], kitchenMsg = obj[11];
            this.Item = items;
            //category.sort(function (a, b) {
            //    return a.seq > b.seq;
            //});
            //console.log(category)
            var _obj = { "category": [], "modifiers": [], "kitchenmsg": [] };
            for (var i = 0; i < category.length; i++) {
                var cat = category[i];
                var _c = {};
                _c.code = cat.code;
                _c.icon = cat.icon;
                _c.color = cat.color;
                _c.specialOffer = false;
                _c.memberOnly = false;
                if (cat.type != 0) {
                    _c.memberOnly = true;
                }
                _c.name = {};
                _c.name["001"] = cat.name1;
                _c.name["002"] = cat.name2;
                _c.name["003"] = cat.name3;
                _c.name1 = cat.name1;
                _c.name2 = cat.name2;
                _c.name3 = cat.name3;
                _c.items = itemcategory.map(function (obj) {
                    var returnItem = null;
                    if (obj.categoryId == cat.categoryId) {
                        items.map(function (tempItem) {
                            if (tempItem.itemId == obj.itemId) {
                                var pricelist = pricebatch.filter(function (pbobj) {
                                    return pbobj.itemId == tempItem.itemId;
                                }).sort(function (a, b) { return a.activateDate < b.activateDate });
                                if (tempItem.itemId == '050001') {
                                    console.log(pricelist);
                                }
                                if (pricelist.length != 0) {
                                    returnItem = tempItem;

                                    returnItem.name = {};
                                    returnItem.name["001"] = tempItem.name1;
                                    returnItem.name["002"] = tempItem.name2;
                                    returnItem.name["003"] = tempItem.name3;
                                    returnItem.group = cat.code;
                                    returnItem.serviceCharge = tempItem.serviceCharge;
                                    returnItem.unitprice = pricelist[0].price1 * 100;
                                    returnItem.show = tempItem.goMenu || false;

                                    var itemTimeRangeList = timeRange.filter((trobj) => {
                                        return trobj.itemId == tempItem.itemId;
                                    })
                                    if (itemTimeRangeList.length != 0) {
                                        var time = "";
                                        for (var i = 0; i < itemTimeRangeList.length; i++) {
                                            time += i == 0 ? "" : ",";
                                            time += moment(itemTimeRangeList[i].timeStart).format("HH:mm") + '-' + moment(itemTimeRangeList[i].timeEnd).format("HH:mm");
                                        }
                                        returnItem.time = time;
                                        //returnItem.time = '';
                                    }
                                    var imlist = itemModifier.filter((imObj) => {
                                        return imObj.itemCode == tempItem.code;
                                    })
                                    if (imlist.length != 0) {
                                        returnItem.modifiers = [];
                                        for (var i = 0; i < imlist.length; i++) {
                                            returnItem.modifiers.push(imlist[i].modifierGroupCode);
                                        }

                                    }

                                    var iolist = itemOption.filter((ioObj) => {
                                        return ioObj.itemCode == tempItem.code;
                                    })
                                    if (iolist.length != 0) {
                                        for (var i = 0; i < iolist.length; i++) {
                                            var io = iolist[i];
                                            io.items = [];
                                            var optionList = option.filter((oObj) => {
                                                return oObj.optionGroupCode == io.optionGroupCode;
                                            })
                                            if (optionList.length != 0) {
                                                for (var j = 0; j < optionList.length; j++) {

                                                    var optionItem = optionList[j];

                                                    var pricelist = pricebatch.filter(function (pbobj) {
                                                        return pbobj.itemId == optionItem.optionCode;
                                                    }).sort(function (a, b) { return a.activateDate < b.activateDate });
                                                    if (pricelist.length != 0) {
                                                        optionItem.unitprice = pricelist[0].price1 * 100;

                                                        optionItem.name = {};
                                                        optionItem.code = optionItem.optionCode;
                                                        optionItem.show = optionItem.ogoMenu && optionItem.oggoMenu;
                                                        optionItem.name[config.lang1] = optionItem.name1;
                                                        optionItem.name[config.lang2] = optionItem.name2;
                                                        optionItem.name[config.lang3] = optionItem.name3;


                                                        var imlist = itemModifier.filter((imObj) => {
                                                            return imObj.itemCode == optionItem.code;
                                                        })
                                                        if (imlist.length != 0) {
                                                            optionItem.modifiers = [];
                                                            for (var k = 0; k < imlist.length; k++) {
                                                                optionItem.modifiers.push(imlist[k].modifierGroupCode);
                                                            }

                                                        }


                                                        io.items.push(optionItem);
                                                    }
                                                }
                                                if (io.items.length != 0) {
                                                    if (!(returnItem.option)) {
                                                        returnItem.option = [];
                                                    }
                                                    returnItem.option.push(io);
                                                }
                                            }
                                        }
                                    }

                                    var slist = itemSubitem.filter((isObj) => {
                                        return isObj.itemCode == tempItem.code;
                                    })
                                    if (slist.length != 0) {
                                        returnItem.subitem = [];
                                        for (var i = 0; i < slist.length; i++) {
                                            var so = subitem.filter((sobj) => {
                                                return sobj.subitemCode == slist[i].subitemCode;
                                            })[0]
                                            var pricelist = pricebatch.filter(function (pbobj) {
                                                return pbobj.itemId == so.subitemCode;
                                            }).sort(function (a, b) { return a.activateDate < b.activateDate });
                                            if (pricelist.length != 0) {
                                                so.unitprice = pricelist[0].price1 * 100;
                                                so.name = {}
                                                so.name["001"] = so.name1;
                                                so.name["002"] = so.name2;
                                                so.name["003"] = so.name3;
                                                so.code = so.subitemCode;
                                                returnItem.subitem.push(so);
                                            }
                                        }

                                    }
                                }


                            }
                        })
                    }
                    return returnItem;
                }).filter(function (fobj) {
                    return fobj != null;
                })
                if (_c.items.length != 0) {
                    _obj.category.push(_c);
                }
            }
            console.log(modifier.length);
            for (var i = 0; i < modifier.length; i++) {
                var m = modifier[i];
                var _m = {};
                _m.name = {};
                _m.name["001"] = m.name1;
                _m.name["002"] = m.name2;
                _m.name["003"] = m.name3;
                _m.namek = m.namek;
                _m.label = m.name1;
                _m.show = m.mgoMenu && m.mggoMenu;
                _m.code = m.modifierCode;
                _m.modifierGroupCode = m.modifierGroupCode;
                _obj.modifiers.push(_m);
            }

            for (var i = 0; i < kitchenMsg.length; i++) {
                var k = kitchenMsg[i];
                var _k = {};
                _k.name = {};
                _k.name["001"] = k.name1;
                _k.name["002"] = k.name2;
                _k.name["003"] = k.name3;
                _k.label = k.name1;
                _k.code = k.kitchenMsgCode;
                _obj.kitchenmsg.push(_k);
            }

            this.food = _obj;
            this.food.option = option;
            this.food.subitem = subitem;
            fs.writeFile("./public/data/food.json", JSON.stringify(_obj), function (err) {
                if (err) {
                    console.log(err);
                } else {
                    //console.log("The file was saved!");
                }
            });
            fs.writeFile("./public/data/food_main.json", JSON.stringify(this.food), function (err) {
                if (err) {
                    console.log(err);
                } else {
                    //console.log("The file was saved!");
                }
            });
            //console.log(_obj.category[1].items);

        }, 'parallel')
    }

    this.Login = function (data, callback) {
        var staffName = '';
        //console.log(data);
        var query = { "sql": "select * from staff s inner join staffgroup sg on s.staffGroupCode = sg.staffGroupCode where username = @username ", "args": [{ "c": "username", "t": TYPES.VarChar, "v": data.u }] }
        this.DM.executeStatement(query, (err, result) => {
            //console.log(result);
            var isLogin = false;
            var staffDetails = {};
            if (result.length != 0) {
                if (result[0].password == data.p || result[0].password == '-999') {
                    isLogin = true;
                    staffName = result[0].name;
                    staffDetails = result[0];
                }
            }
            //callback('{"success":' + isLogin + ',"staffName":"' + staffName + ',"staffDetail":'++}');
            callback({success:isLogin,staffName:staffName,staffDetails:staffDetails});
        })
    }

    this.Print = (args, callback) => {
        console.log(args);
        var tableCode = args.tableNo == null ? "" : args.tableNo;
        var tableSubcode = args.tableSubcode == null ? "" : args.tableSubcode;
        var query = [{ "sql": "delete tablediscount where tablecode = @tableCode and tableSubcode = @tableSubCode", "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": tableCode }, { "c": "tableSubcode", "t": TYPES.VarChar, "v": tableSubcode }] }];

        if (args.discount) {
            for (var i = 0; i < args.discount.length; i++) {
                var sqlargs = [];
                sqlargs.push({ "c": "tableCode", "t": TYPES.VarChar, "v": tableCode })
                sqlargs.push({ "c": "tableSubCode", "t": TYPES.VarChar, "v": tableSubcode })

                sqlargs.push({ "c": "staffId" , "t": TYPES.VarChar, "v": args.user })

                sqlargs.push({ "c": "discountType", "t": TYPES.VarChar, "v": args.discount[i].type })
                sqlargs.push({ "c": "discountValue", "t": TYPES.Decimal, "v": parseFloat(args.discount[i].price), "o": { "scale": 2 } })
                sqlargs.push({ "c": "discountCode", "t": TYPES.VarChar, "v": args.discount[i].coupon_alias })
                sqlargs.push({ "c": "discountAmount", "t": TYPES.Decimal, "v": parseFloat(args.discount[i].amount), "o": { "scale": 2 } })

                query.push({ "sql": "insert into tablediscount ( discountCode,discountType,discountValue,staffId,discountAmount,discountDate,tableCode,tableSubCode) values (@discountCode,@discountType,@discountValue,@staffId,@discountAmount,getDate(),@tableCode,@tableSubCode)", "args": sqlargs });
            }
        }


        this.DM.executeStatement(query, (err, result) => {
            query = [
                {
                    "sql": "select * from tableOrder where tablecode = @tableCode and tableSubcode = @tableSubCode",
                    "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": tableCode }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": tableSubcode }]
                },
                {
                    "sql": "select * from tableOrderD where tableCode = @tableCode and tableSubcode = @tableSubCode order by round,seq",
                    "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": tableCode }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": tableSubcode }]
                },
                {
                    "sql": "select * from tablediscount where tablecode = @tableCode and tableSubcode = @tableSubCode",
                    "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": tableCode }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": tableSubcode }]
                }]

            this.DM.executeStatement(query, (err, result) => {

                var _obj = {};
                //console.log(result);
                if (result[0].length != 0) {
                    //console.log(result[0]);
                    _obj = result[0][0];
                    _obj.status = undefined;
                }
                console.log(result[1]);
                if (result[1].length != 0) {
                    _obj.items = [];
                    for (var i = 0; i < result[1].length; i++) {
                        var currentItem = result[1][i];
                        var items = [];
                        var name1,name2,name3;
                        if (currentItem.type == "I" || currentItem.type == "T") {
                            items = RAS.Item.filter(function (entry) {
                                return entry.code == currentItem.itemId;
                            });
                            name1 = items[0].name1;
                            name2 = items[0].name2;
                            name3 = items[0].name3;
                        } else if (currentItem.type == "M") {
                            items = RAS.food.modifiers.filter(function (entry) {
                                return entry.code == currentItem.itemId;
                            });
                            console.log(currentItem.itemId);
                            name1 = items[0].name[config.lang1];
                            name2 = items[0].name[config.lang2];
                            name3 = items[0].name[config.lang3];
                        } else if (currentItem.type == "O") {
                            items = RAS.food.option.filter(function (entry) {
                                return entry.code == currentItem.itemId;
                            });
                            console.log(items[0].name)
                            name1 = items[0].name[config.lang1];
                            name2 = items[0].name[config.lang2];
                            name3 = items[0].name[config.lang3];
                        }

                        if (items[0]) {
                            if (items[0].name) {
                                if (items[0].name[config.lang1])
                                    name1 = items[0].name[config.lang1];
                                if (items[0].name[config.lang2])
                                    name2 = items[0].name[config.lang2];
                                if (items[0].name[config.lang3])
                                    name3 = items[0].name[config.lang3];
                            }
                        }

                        if (currentItem.customName != "") {
                            name1 = name2 = name3 = currentItem.customName;
                        }

                        /*
                        var items = RAS.Item.filter(function (entry) {
                            return entry.code == currentItem.itemId;
                        });
                        //console.log(items);
                        var name1 = items[0].name1;
                        var name2 = items[0].name2;
                        var name3 = items[0].name3;
                        if (items[0].name) {
                            if (items[0].name[config.lang1])
                                name1 = items[0].name[config.lang1];
                            if (items[0].name[config.lang2])
                                name2 = items[0].name[config.lang2];
                            if (items[0].name[config.lang3])
                                name3 = items[0].name[config.lang3];
                        }
                        if (currentItem.customName != "") {
                            name1 = name2 = name3 = currentItem.customName;
                        }
                        */
                        if (name1 != "") {

                            var nameLenght = 25 - countDoubleByteChar(name1);
                            name1 = S(name1).padRight(nameLenght).s
                            //name1 = S(name1).padRight(25).s
                        }

                        var itemTotalPrice = currentItem.qty * currentItem.price;
                        var itp = S("$" + itemTotalPrice.format(1, 3)).padLeft(10).s;
                        var up = S("$" + currentItem.price.format(1, 3)).padLeft(10).s;

                        if (currentItem.type != "T") {
                            _obj.items.push({ code: currentItem.itemId, type: currentItem.type, desc1: name1, desc2: name2, desc3: name3, qty: currentItem.qty, unitPrice: up, itemTotalPrice: itp, time: currentItem.time, voidIndex: currentItem.voidIndex, voidRemark: currentItem.voidRemark, staff: currentItem.staffId, index: currentItem.seq });
                        }
                    }


                    var totaldiscount = 0;
                    _obj.discount = [];
                    for (var i = 0; i < result[2].length; i++) {
                        var _o = result[2][i];

                        //off_10: "9折",
                        //off_20: "8折",
                        //off_30: "7折",
                        var discountMsg = _o.discountCode;
                        switch (_o.discountCode) {
                            case "off_10": discountMsg = "10% off";
                                break;
                            case "off_20": discountMsg = "20% off";
                                break;
                            case "off_30": discountMsg = "30% off";
                                break;
                        }
                        _obj.discount.push({ "discountCode": discountMsg, "discountValue": S("$" + _o.discountValue.format(1, 3)).padLeft(10).s })
                        totaldiscount += _o.discountValue;
                    }
                    var totalPrice = _obj.price + _obj.serviceCharge - totaldiscount;

                    //var totalPrice = _obj.price + _obj.serviceCharge + _obj.remainings;

                    totalPrice = Math.round(totalPrice);
                    _obj.totalPrice = "$" + totalPrice.format(1, 3);
                    _obj.price = S("$" + _obj.price.format(1, 3)).padLeft(10).s;
                    _obj.serviceCharge = S("$" + _obj.serviceCharge.format(1, 3)).padLeft(10).s;

                }

                var orderid = moment().format("YYMMDDHHmmssSSSS");
                fs.writeFile("./printer/" + args.action + "/" + orderid + ".json", JSON.stringify(_obj), function (err) {
                    if (err) {
                        console.log(err);
                    } else {
                        //console.log("The file was saved!");
                    }
                });
                if (args.action == "bill") {

                    this.SetTableStatus({ "tableCode": tableCode, "status": 3 })
                }
            }, 'parallel')
        },'series');
    }

    this.LoadTableSchema = function (data,callback) {
        var sql = new Array();
        var that = this;
        sql.push({ "sql": "select * from diningTable", "args": null });
        sql.push({ "sql": "select * from shopZone", "args": null });
        this.DM.executeStatement(sql, function (err, _obj) {
            //console.log(JSON.stringify(_obj));
            that.Table = _obj[0];
            that.Scene = _obj[1];
            var obj = { scene: [] };
            //console.log(JSON.stringify(_obj[1]));
            for (var i = 0 ; i < that.Scene.length; i++) {
                var o = {};
                o.id = that.Scene[i].shopZoneId;
                o.name = that.Scene[i].name;
                o.layout = that.Scene[i].floorPlan;
                o.memberObject = [];
                var selectedMember = that.Table.filter(function (entry) { return entry.shopZoneId == that.Scene[i].shopZoneId });
                for (var j = 0 ; j < selectedMember.length; j++) {
                    var sm = selectedMember[j];
                    o.memberObject.push({ uid: sm.uid, label: sm.name, type: 'table', shape: sm.shape, size: sm.size, pos_X: sm.x, pos_Y: sm.y, scale_X: sm.scaleX, scale_Y: sm.scaleY, angle_A: sm.angleA, angle_B: sm.angleB, status: sm.status, buffetStatus: sm.buffetStatus });
                }
                obj.scene.push(o);
            }
            if (callback) {
                callback(err,obj);
            }
            //io.sockets.emit('refreshTable', JSON.stringify(obj));
            //fs.writeFile("./public/files/so.json", JSON.stringify(obj), function (err) {
            //    if (err) {
            //        console.log(err);
            //    } else {
            //        //console.log("The file was saved!");
            //    }
            //});
        })
    }

    this.AdminLoadTableOrder = (args, callback) => {
        //var callbackObj = {};
        var _o = [{ "sql": "select * from tableOrder where tableCode = @tableCode", "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": args.tableNum.$value }] },
        { "sql": "select * from tableOrderD where tableCode = @tableCode order by round,seq,type", "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": args.tableNum.$value }] }
        ,
                {
                    "sql": "select * from tablediscount where tablecode = @tableCode ",
                    "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": args.tableNum.$value }]
                }]
        this.DM.executeStatement(_o, (err, tableOrders) => {
            console.log(tableOrders);
            var orders = [];
            if (tableOrders[0].length != 0) {
                for (var j = 0; j < tableOrders[0].length ; j++) {
                    var tableOrder = tableOrders[0][j];
                    var order = { 'version': '1.0', 'header': { 'orderId': '', 'tableNum': '', 'peopleNum': 0, 'member': '', 'memberName': '', 'memberMsg': '', 'lastOrder': '', 'lastOrderWarning': '' }, 'item': [], 'itemCount': 0, 'discount': [] };
                    order.header.tableNum = tableOrder.tableCode + tableOrder.tableSubCode;
                    order.header.peopleNum = tableOrder.pax;
                    order.header.orderId = "_";
                    var contentObj = [];
                    for (var k = 0; k < tableOrders[1].length ; k++) {
                        var currentItem = tableOrders[1][k];
                        if (tableOrder.tableSubCode == currentItem.tableSubCode) {
                            var items = [];
                            var name1,name2,name3;
                            if (currentItem.type == "I" || currentItem.type == "T") {
                                items = RAS.Item.filter(function (entry) {
                                    return entry.code == currentItem.itemId;
                                });
                                name1 = items[0].name1;
                                name2 = items[0].name2;
                                name3 = items[0].name3;
                                if (currentItem.type == "T") {
                                    var _lastOrder = items[0].namek.split(',');
                                    order.header.lastOrderWarning = _lastOrder[0];
                                    order.header.lastOrder = _lastOrder[1];
                                }
                            } else if (currentItem.type == "M") {
                                items = RAS.food.modifiers.filter(function (entry) {
                                    return entry.code == currentItem.itemId;
                                });
                                console.log(currentItem.itemId);
                                name1 = items[0].name[config.lang1];
                                name2 = items[0].name[config.lang2];
                                name3 = items[0].name[config.lang3];
                            } else if (currentItem.type == "O") {
                                items = RAS.food.option.filter(function (entry) {
                                    return entry.code == currentItem.itemId;
                                });
                                if (items.length > 0) {
                                    console.log(items[0].name)
                                    name1 = items[0].name[config.lang1];
                                    name2 = items[0].name[config.lang2];
                                    name3 = items[0].name[config.lang3];
                                } else if (currentItem.itemId = 'O9999') {
                                    name1 = '未選擇菜單';
                                    name2 = '未選擇菜單';
                                    name3 = '未選擇菜單';
                                }
                            }

                            if (items[0]) {
                                if (items[0].name) {
                                    if (items[0].name[config.lang1])
                                        name1 = items[0].name[config.lang1];
                                    if (items[0].name[config.lang2])
                                        name2 = items[0].name[config.lang2];
                                    if (items[0].name[config.lang3])
                                        name3 = items[0].name[config.lang3];
                                }
                            }


                            if (currentItem.customName != "") {
                                name1 = name2 = name3 = currentItem.customName;
                            }


                            var t = moment(currentItem.time).format("HH:mm:ss");
                            contentObj.push({ code: currentItem.itemId, type: currentItem.type, desc1: name1, desc2: name2, desc3: name3, qty: currentItem.qty, unitPrice: currentItem.price, time: t, voidIndex: currentItem.voidIndex, voidRemark: currentItem.voidRemark, staff: currentItem.staffId, index: currentItem.seq, serviceCharge:currentItem.serviceCharge });

                        }
                    }

                    for (var i = 0; i < tableOrders[2].length; i++) {
                        var currentItem = tableOrders[2][i];
                        order.discount.push({ "discountCode": currentItem.discountCode, "discountType": currentItem.discountType, "discountValue": currentItem.discountValue, "discountAmount": currentItem.discountAmount });
                    }


                    order.item = contentObj;
                    order.itemCount = contentObj.length;
                    orders.push(order);
                }
                console.log(orders);
                callbackObj = { result: "OK", timestamp: new Date().getTime(), taskID: 1, note: "", order: orders, suspendItems: [] }
                var builder = new xml2js.Builder();
                var xml = builder.buildObject(callbackObj);
                console.log('callback adminloadtableorder');
                callback(callbackObj);

            } else {
                var order = { 'version': '1.0', 'header': { 'orderId': '', 'tableNum': '', 'peopleNum': 0, 'member': '', 'memberName': '', 'memberMsg': '' }, 'item': [], 'itemCount': 0 };
                callbackObj = { result: "OK", timestamp: new Date().getTime(), taskID: 1, note: "", order: [order], suspendItems: {} }
                //callbackObj = { result: "OK", timestamp: new Date().getTime(), taskID: 1, note: "", order: orders, suspendItems: [] }
                var builder = new xml2js.Builder();
                var xml = builder.buildObject(callbackObj);
                //console.log(xml);
                callback(callbackObj);
            }
        })
    }

    this.LoadItemQty = (args, callback) => {
        //callback(JSON.stringify([]));
        this.GetOutOfStock(callback)
    }

    this.ResetItemQty = (args,callback) => {
        this.DM.executeStatement({ "sql": "delete itemControl", "args": [] }, (err, result) => {
            console.log(callback);
            this.GetOutOfStock((err, result) => {
                callback(result);
            });

        });
        //this.DataManager.executeOracleStatment("delete xf_rms_item_control", function (err, result) {
        //    that.GetOutOfStock(function (err, result) {
        //        io.sockets.emit('refreshItemQty', JSON.stringify(result));
        //    })
        //}, []);
    }

    function getTableCode(tableNo) {
        try {
            var tableCode = tableNo;
            var tableSubCode = "";
            if (tableCode.indexOf('-') != -1) {
                var splitTableCode = tableCode.split('-');
                tableCode = splitTableCode[0];
                tableSubCode = splitTableCode[1];
            }
            return { tableCode: tableCode, tableSubCode: tableSubCode };
        } catch (e) {
            console.log(e);
        }
    }

    this.SaveTableOrder = (args, callback) => {
        console.log(args);
        var r = args.itemData.json;
        console.log(JSON.stringify(r));
        var orderstring = "";
        var total = 0;
        var date = new Date();
        var orderid = moment().format("YYMMDDHHmmssSSSS");
        var itemOrderTime = moment().format("HH:mm:ss");
        var tableObj = getTableCode(args.tableNum.$value);

        var noOfPeople = args.peopleNum == undefined ? '0' : args.peopleNum.$value;

        var printingArray = { TableNo: args.tableNum.$value, Item: [] };
        var checkStockArray = [];
        var commitItemStockArray = [];
        var callbackObj = {};
        var vipcode, surname, givenname, vipcenter, currgrade, dob_yyyy, dob_mm, dob_dd, orderContent;
        var staff = args.user == undefined ? "user" : args.user.$value;
        vipcode = surname = givenname = vipcenter = currgrade = dob_yyyy = dob_mm = dob_dd = orderContent = '';
        console.log ( date);
        try {
            for (var i = 0 ; i < r.order.item.length; i++) {
                //console.log(r.order.item[i]);
                var vr = r.order.item[i].voidRemark == undefined ? "" : r.order.item[i].voidRemark[0];
                if (r.order.item[i].qty[0] > 0 || r.order.item[i].qty[0] < 0) {
                    var selectItem = this.Item.filter(function (entry) {
                        return entry.code == r.order.item[i].code[0];
                    })[0];
                    //printerport = i % 2 == 0 ? "K1" : "K2";
                    printingArray.Item.push({ Name: r.order.item[i].desc1[0], Qty: r.order.item[i].qty[0], Printer: selectItem.printer, Code: r.order.item[i].code[0], Time: itemOrderTime, Msg: vr });
                    checkStockArray.push({
                        sql: "select itemCode,qty,@dname as \"name\" from itemControl where itemCode = @itemCode and (qty < 0 or qty < @qty)"
                        , args: [{ c: 'dname', t: TYPES.NVarChar, v: r.order.item[i].desc1[0] }, { c: 'itemCode', t: TYPES.VarChar, v: selectItem.code }, { c: 'qty', t: TYPES.Int, v: r.order.item[i].qty[0] }]
                    })
                    commitItemStockArray.push({ sql: "update itemControl set qty = qty - @qty where itemCode = @itemCode", argsn: [{ c: 'itemCode', t: TYPES.VarChar, v: selectItem.code }, { c: 'qty', t: TYPES.Int, v: r.order.item[i].qty[0] }] });
                }
            }
        } catch (e) {
            console.log(e);
        }
        this.DM.executeStatement(checkStockArray, (err, result) => {
            if (err != undefined) {
                console.log(err);
            }
            console.log(JSON.stringify(result));
            var isValid = true;
            var errorMsg = ""; var outOfSotckList = [];
            for (var i = 0; i < result.length; i++) {
                if (result[i].length != 0) {
                    isValid = false;
                    outOfSotckList.push(result[i][0].itemCode);
                }
            }
            if (isValid) {
                var query = [{ "sql": "select max(round) as round, max(seq) as seq, max(pax) as pax,max(memberId) as memberId from tableorderd d right outer join tableOrder o on o.tableCode = d.tableCode and o.tableSubCode = d.tableSubCode where o.tableCode = @tableCode and o.tableSubCode = @tableSubCode;", "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": tableObj.tableCode }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": tableObj.tableSubCode }] }, { "sql": "select * from tableorderd where tableCode = @tableCode and tableSubCode = @tableSubCode", "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": tableObj.tableCode }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": tableObj.tableSubCode }] }];

                this.DM.executeStatement(query, (err, tableOrderD) => {
                    var currentSeq = 0;
                    var currentRound = 0;
                    var query = [];
                    var _o = {};
                    var totalPrice = 0;
                    var serviceCharge = 0;
                    var lastOrder = '';
                    var lastOrderWarning = '';
                    console.log(tableOrderD);
                    if (tableOrderD[0][0].seq != null) {
                        currentSeq = tableOrderD[0][0].seq;
                    }
                    if (tableOrderD[0][0].round != null) {
                        currentRound = tableOrderD[0][0].round;
                    }
                    currentRound++;
                    if (tableOrderD[1].length != 0) {
                        for (var i = 0; i < tableOrderD[1].length; i++) {
                            var orderedItem = tableOrderD[1][i];
                            totalPrice += orderedItem.qty * orderedItem.price;
                        }
                    }
                    var lastPrinter = '';
                    var orderKds = null;
                    for (var i = 0 ; i < r.order.item.length; i++) {
                        var vi = r.order.item[i].voidIndex == undefined ? "-1" : r.order.item[i].voidIndex[0];
                        var vr = r.order.item[i].voidRemark == undefined ? "" : r.order.item[i].voidRemark[0];
                        var approveStaff = r.order.item[i].voidApproveStaff == undefined || null ? "" : r.order.item[i].voidApproveStaff[0];
                        var type = r.order.item[i].type;
                        var serviceCharge = true;
                        var itemOrderTime = moment().format("HH:mm");
                        if (type != "M" ) {
                            currentSeq += 1;
                        }
                        if (type == "T") {
                            var items = RAS.Item.filter(function (entry) {
                                return entry.code == r.order.item[i].code[0];
                            });
                            var _lastOrder = items[0].namek.split(',');
                            lastOrder = _lastOrder[0];
                            lastOrderWarning = _lastOrder[1];
                        }
                        totalPrice += parseFloat((parseFloat(r.order.item[i].unitPrice[0]) / 100).toFixed(2)) * parseInt(r.order.item[i].qty[0]);
                        console.log(currentSeq)
                        console.log(r.order.item[i].unitPrice[0])
                        var customName = '';
                        if (r.order.item[i].customName) {
                            if (r.order.item[i].customName[0]) {
                                customName = r.order.item[i].customName[0]
                            }
                        }
                        var itemType = r.order.item[i].type;
                        var itemCode = r.order.item[i].code[0]
                        var items = [];
                        if (itemType == "I" || itemType == "T") {
                            items = RAS.Item.filter(function (entry) {
                                return entry.code == itemCode;
                            });
                        } else if (itemType == "M") {
                            items = RAS.food.modifiers.filter(function (entry) {
                                return entry.code == itemCode;
                            });
                        } else if (itemType == "O") {
                            items = RAS.food.option.filter(function (entry) {
                                return entry.code == itemCode;
                            });
                        }

                        //var selectItem = this.Item.filter(function (entry) {
                        //    return entry.code == r.order.item[i].code[0];
                        //})[0];
                        var kds = null;
                        var _sc = true;
                        if (items.length > 0) {
                            if (itemType != "M") {
                                lastPrinter = items[0].printer;
                            }
                            if (items[0].serviceCharge != undefined) {
                                _sc = items[0].serviceCharge;
                            };
                            if (items[0].kds) {
                                kds = items[0].kds;
                            };
                        }
                        if (kds != null) {
                            orderKds = kds;
                        }
                        _o = {
                            "sql": "insert into tableOrderD (tableCode,tableSubCode,itemId,qty,price,voidIndex,voidRemark,type,seq,time,staffId,round,customName,approveStaffId,serviceCharge,printer,kdsgroup,kds) values (@tableCode,@tableSubCode,@itemId,@qty,@price,@voidIndex,@voidRemark,@type,@seq,@time,@staffId,@round,@customName,@approveStaffId,@serviceCharge,@printer,@kdsgroup,@kds)",
                            "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": tableObj.tableCode }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": tableObj.tableSubCode }, { "c": "itemId", "t": TYPES.VarChar, "v": itemCode }, { "c": "qty", "t": TYPES.Int, "v": parseInt(r.order.item[i].qty[0]) },
                                { "c": "price", "t": TYPES.Decimal, "v": parseFloat((parseFloat(r.order.item[i].unitPrice[0]) / 100).toFixed(2)), "o": { "scale": 2 } }, { "c": "voidIndex", "t": TYPES.Int, "v": vi }, { "c": "voidRemark", "t": TYPES.NVarChar, "v": vr }, { "c": "type", "t": TYPES.VarChar, "v": type }, { "c": "seq", "t": TYPES.Int, "v": currentSeq }, { "c": "time", "t": TYPES.Time, "v": date }, { "c": "staffId", "t": TYPES.VarChar, "v": staff }, { "c": "round", "t": TYPES.Int, "v": currentRound }, { "c": "customName", "t": TYPES.NVarChar, "v": customName }, { "c": "approveStaffId", "t": TYPES.VarChar, "v": approveStaff }, { "c": "serviceCharge", "t": TYPES.Bit, "v": _sc }, { "c": "printer", "t": TYPES.VarChar, "v": lastPrinter }, { "c": "kdsgroup", "t": TYPES.BigInt, "v": '-1' }, { "c": "kds", "t": TYPES.VarChar, "v": kds }]
                        }
                        query.push(_o);
                    }

                    serviceCharge = (totalPrice / 10).toFixed(1);

                    var lastOrderDate = null;
                    if (lastOrder != "") {
                        lastOrderDate = new Date();
                    }

                    _o = {
                        "sql": "insert into tableOrder (tableCode,tableSubCode,pax,memberId,startTime,staffId, price, serviceCharge, lastOrder, lastOrderWarning) values (@tableCode,@tableSubCode,@pax,@memberId,@startTime,@staffId, @price, @serviceCharge, @lastOrder, @lastOrderWarning)",
                        "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": tableObj.tableCode }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": tableObj.tableSubCode }, { "c": "pax", "t": TYPES.Int, "v": noOfPeople }, { "c": "memberId", "t": TYPES.Int, "v": null }, { "c": "startTime", "t": TYPES.DateTime, "v": date }, { "c": "staffId", "t": TYPES.VarChar, "v": staff },
                        { "c": "price", "t": TYPES.Decimal, "v": parseFloat(totalPrice), "o": { "scale": 2 } },
                        { "c": "serviceCharge", "t": TYPES.Decimal, "v": parseFloat(serviceCharge), "o": { "scale": 2 } },
                        { "c": "lastOrder", "t": TYPES.DateTime, "v": lastOrderDate },
                        { "c": "lastOrderWarning", "t": TYPES.DateTime, "v": lastOrderDate }]
                    }
                    if (tableOrderD[0][0].pax == null) {
                        query.push(_o);
                    } else {
                        _o = {
                            "sql": "update tableOrder set pax = @pax, price = @price, serviceCharge = @serviceCharge where tableCode = @tableCode and tableSubCode = @tableSubCode",
                            "args": [{ "c": "pax", "t": TYPES.Int, "v": noOfPeople }, { "c": "tableCode", "t": TYPES.VarChar, "v": tableObj.tableCode }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": tableObj.tableSubCode },
                        { "c": "price", "t": TYPES.Decimal, "v": parseFloat(totalPrice), "o": { "scale": 2 } },
                        { "c": "serviceCharge", "t": TYPES.Decimal, "v": parseFloat(serviceCharge), "o": { "scale": 2 } }]
                        }
                        query.push(_o);
                    }

                    this.DM.executeStatement(query, (err, result) => {
                        console.log(err);
                        //console.log(result);
                        var _result = "OK";
                        callbackObj = { result: _result, timestamp: new Date().getTime(), taskID: 1, note: '', suspendItems: [] };
                        callback(callbackObj);


                        printingArray.Item.sort(function (a, b) {
                            return a.Printer > b.Printer;
                        });
                        if (orderKds) {
                            this.showKds(orderKds);
                        }
                        this.SetTableStatus({"tableCode":tableObj.tableCode,"status":2})
                        //mqttClient.publish('wo/kds/p1', JSON.stringify(printingArray), { qos: 2 });
                        fs.writeFile("./printer/" + moment().format("YYMMDDHHmmss") + ".txt", JSON.stringify(printingArray), function (err) {
                            if (err) {
                                console.log(err);
                            } else {
                                //console.log("The file was saved!");
                            }
                        });
                    }, 'parallel')
                })
            } else {
                this.GetOutOfStock((err, result) => {
                    var note = "<note></note>";
                    var rItemSuspendInfo = "<suspendItems>" + this.OutOfStock + "</suspendItems>";
                    if (outOfSotckList.length != 0) {
                        note = "<note>outOfStock</note>";
                    }
                    callbackObj = { result: 'ERROR', timestamp: new Date().getTime(), taskID: 1, note: note, suspendItems: this.OutOfStock.toString() };
                    callback(callbackObj);
                })
            }
        }, 'parallel')
    }

    this.showKds = (kds) => {
        var topic = 'wo/kds/';
        var query = { "sql": "select * from tableorderd where kds is not null order by time,tablecode,tablesubcode,round,seq,type ", "args": null }
        if (kds) {
            query.sql = "select * from tableorderd where kds = @kds order by tablecode,tablesubcode,round,seq,type ";
            query.args = [{ "c": "kds", "t": TYPES.VarChar, "v": kds }]
            topic = 'wo/kds/' + kds;
        }
        this.DM.executeStatement(query, (err, result) => {
            //console.log(result);
            var obj = {
                "action": "broadcastTicketList",
                "list": []
            };
            for (var idx = 0; idx < result.length; idx++) {
                var row = result[idx];
                var itemType = row.type;
                var itemCode = row.itemId;
                var items = [];
                if (itemType == "I" || itemType == "T") {
                    items = RAS.Item.filter(function (entry) {
                        return entry.code == itemCode;
                    });
                } else if (itemType == "M") {
                    items = RAS.food.modifiers.filter(function (entry) {
                        return entry.code == itemCode;
                    });
                } else if (itemType == "O") {
                    items = RAS.food.option.filter(function (entry) {
                        return entry.code == itemCode;
                    });
                }
                function addNewItem() {
                    obj.list.push({
                        Id: row.tableCode + "_" + row.tableSubCode + "_" + row.round, TableNo: row.tableCode, TableSubCode: row.tableSubCode,
                        Item: [{ Name: items[0].namek, Qty: row.qty, Printer: row.printer, Code: row.itemId, Time: moment(row.time).format("HH:mm:ss"), Msg: "", Seq: row.seq, Round: row.round, VoidIndex: row.voidIndex, Group: row.kdsGroup }],
                        Modifier:[]
                    })
                }

                if (idx == 0) {
                    addNewItem();
                } else if (obj.list[obj.list.length - 1].Id == row.tableCode + "_" + row.tableSubCode + "_" + row.round) {
                    if (itemType == "M") {
                        obj.list[obj.list.length - 1].Modifier.push({ Name: items[0].namek, Qty: row.qty, Printer: row.printer, Code: row.itemId, Time: moment(row.time).format("HH:mm:ss"), Msg: "", Seq: row.seq, Round: row.round, VoidIndex: row.voidIndex, Group: row.kdsGroup });
                    }else{
                        obj.list[obj.list.length - 1].Item.push({ Name: items[0].namek, Qty: row.qty, Printer: row.printer, Code: row.itemId, Time: moment(row.time).format("HH:mm:ss"), Msg: "", Seq: row.seq, Round: row.round, VoidIndex: row.voidIndex, Group: row.kdsGroup });
                    }
                } else {
                    addNewItem();
                }
            }
            mqttClient.publish(topic, JSON.stringify(obj), { qos: 2 });
        })
    }

    this.broadcastTicketList = (args) => {
        console.log(args);
        var printer = args.split('/')[2];
        this.showKds(printer);
    }

    this.lockGroup = (args) => {
        //console.log(args.payload);
        var payload = JSON.parse(args.payload);
        console.log('=========================');
        var query = [];
        for (var i = 0; i < payload.DishList.length; i++) {
            var _o = payload.DishList[i];
            query.push({ "sql": "update tableorderd set kdsGroup = @time where tableCode = @tableCode and tableSubCode = @tableSubCode and round = @round and seq = @seq ", "args": [{ "c": "time", "t": TYPES.BigInt, "v": new Date().getTime() }, { "c": "tableCode", "t": TYPES.VarChar, "v": _o.TableNo }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": _o.TableSubCode }, { "c": "round", "t": TYPES.Int, "v": _o.Round }, { "c": "Seq", "t": TYPES.Int, "v": _o.Seq }] })
        }
        this.DM.executeStatement(query, (err, result) => {
            console.log(err);
            var printer = args.topic.split('/')[2];
            this.showKds(printer);
        },'parallel')

    }

    this.SetTableStatus = (args, callback) => {
        this.DM.executeStatement({ "sql": "update diningtable set status = @status where name = @tableCode;", "args": [{ "c": "status", "t": TYPES.VarChar, "v": args.status }, { "c": "tableCode", "t": TYPES.VarChar, "v": args.tableCode }] }, (err, result) => {
            console.log("SetTableStatus");
            console.log(result);
            this.LoadTableSchema(null, (err, result) => {
                io.sockets.emit('refreshTable', JSON.stringify(result));
            })
        })
    }

    this.GetOutOfStock = (callback) => {
        this.DM.executeStatement({"sql":"select itemCode as code ,qty from itemControl ","args":[]}, (err, result) => {
            if (err) { console.log("Error executing query:", err); return; }
            //console.log("item control:" + JSON.stringify(result));
            this.OutOfStock = result;
            callback(err, result);
        });
    }

    this.SetItemQty = (args, callback) => {
        console.log(JSON.stringify(args));
        var query = {};
        if (args.qty == -2) {
            query = { "sql": "delete itemControl where itemCode = @itemCode", "args": [{ "c": "itemCode", "t": TYPES.VarChar, "v": args.itemCode }] };
        } else {
            query = {
                "sql": "MERGE INTO itemControl s USING (SELECT @itemCode as  itemCode, @qty as qty, @remark as remark, @staffId as staffId ) i ON (s.itemCode = i.itemCode) WHEN MATCHED THEN UPDATE SET s.qty = i.qty,s.remark = i.remark WHEN NOT MATCHED THEN INSERT (itemCode, qty,remark,staffId) VALUES (i.itemCode, i.qty, i.remark, i.staffId);",
                "args": [{ "c": "itemCode", "t": TYPES.VarChar, "v": args.itemCode }, { "c": "qty", "t": TYPES.Int, "v": args.qty }, { "c": "remark", "t": TYPES.VarChar, "v": args.remark }, { "c": "staffId", "t": TYPES.VarChar, "v": args.staffId }]
            };
        }
        this.DM.executeStatement(query, (err, result) => {
            console.log(result);
            this.GetOutOfStock((err,outOfStock) => {
                console.log()
                callback(outOfStock);
                io.sockets.emit('refreshItemQty', JSON.stringify(outOfStock));
            })
        });
    }

    this.PrintPaymentBill = (args, callback) => {
        var refNo = args.refNo;
        var isPrintTwice = args.printTwice ? args.printTwice : false;
        var isOpenCashDrawer = args.openCashDrawer ? args.openCashDrawer : false;
        query = [{ "sql": "select * from [transaction] where refNo = @refNo;", "args": [{ "c": "refNo", "t": TYPES.VarChar, "v": refNo }] },
                 { "sql": "select * from [transactionD] where refNo = @refNo order by seq; ", "args": [{ "c": "refNo", "t": TYPES.VarChar, "v": refNo }] },
                 { "sql": "select * from [tenderpayment] where refNo = @refNo and void = 0 order by seq;", "args": [{ "c": "refNo", "t": TYPES.VarChar, "v": refNo }] },
                 { "sql": "select * from [tenderdiscount] where refNo = @refNo and void = 0 order by seq;", "args": [{ "c": "refNo", "t": TYPES.VarChar, "v": refNo }] }]
        this.DM.executeStatement(query, (err, transaction) => {
            console.log(transaction);
            var _obj = transaction[0][0];
            _obj.isPrintTwice = isPrintTwice;
            _obj.isOpenCashDrawer = isOpenCashDrawer;

            _obj.items = [];
            for (var i = 0; i < transaction[1].length; i++) {
                var currentItem = transaction[1][i];
                var items = RAS.Item.filter(function (entry) {
                    return entry.code == currentItem.itemId;
                });
                //console.log(items);
                var name1 = items[0].name1;
                var name2 = items[0].name2;
                var name3 = items[0].name3;
                if (items[0].name) {
                    if (items[0].name[config.lang1])
                        name1 = items[0].name[config.lang1];
                    if (items[0].name[config.lang2])
                        name2 = items[0].name[config.lang2];
                    if (items[0].name[config.lang3])
                        name3 = items[0].name[config.lang3];
                }

                if (currentItem.customName != "") {
                    name1 = name2 = name3 = currentItem.customName;
                }

                if (name1 != "") {
                    var nameLenght = 25 - countDoubleByteChar(name1);
                    name1 = S(name1).padRight(nameLenght).s
                }

                var itemTotalPrice = currentItem.qty * currentItem.price;
                var itp = S("$" + itemTotalPrice.format(1, 3)).padLeft(10).s;

                var up = S("$" + currentItem.price.format(1, 3)).padLeft(10).s;

                _obj.items.push({ code: currentItem.itemId, type: currentItem.type, desc1: name1, desc2: name2, desc3: name3, qty: currentItem.qty, unitPrice: up, itemTotalPrice: itp, time: currentItem.time, voidIndex: currentItem.voidIndex, voidRemark: currentItem.voidRemark, staff: currentItem.staffId, index: currentItem.seq });
            }
            _obj.payment = [];
            for (var i = 0; i < transaction[2].length; i++) {
                var _o = transaction[2][i];
                var payMsg = _o.paymentType;
                switch (_o.paymentType) {
                    case "cash": payMsg = "CASH";
                        break;
                    case "unionpay": payMsg = "UnionPay";
                        break;
                    case "master": payMsg = "Master";
                        break;
                    case "visa": payMsg = "Visa";
                        break;
                    case "entertainment": payMsg = "Ent";
                        break;
                }
                _obj.payment.push({ "paymentType": payMsg, "paymentValue": S("$" + _o.paymentValue.format(1, 3)).padLeft(10).s })
            }

            //_obj.totalPrice = "$" + totalPrice.format(1, 3);
            //_obj.price = S("$" + _obj.price.format(1, 3)).padLeft(10).s;
            //_obj.serviceCharge = S("$" + _obj.serviceCharge.format(1, 3)).padLeft(10).s;

            var totaldiscount = 0;
            _obj.discount = [];
            for (var i = 0; i < transaction[3].length; i++) {
                var _o = transaction[3][i];

                //off_10: "9折",
                //off_20: "8折",
                //off_30: "7折",
                var discountMsg = _o.discountCode;
                switch (_o.discountCode) {
                    case "off_10": discountMsg = "10% off";
                        break;
                    case "off_20": discountMsg = "20% off";
                        break;
                        //case "off_30": discountMsg = "7折";
                    case "off_30": discountMsg = "30% off";
                        break;
                }
                _obj.discount.push({ "discountCode": discountMsg, "discountValue": S("$" + _o.discountValue.format(1, 3)).padLeft(10).s })
                totaldiscount += _o.discountValue;
            }
            var tp = _obj.price + _obj.serviceCharge - totaldiscount + _obj.remainings;
            var rp = _obj.payAmount - tp;

            _obj.totalPrice = S("$" + tp.format(1, 3)).padLeft(10).s;
            _obj.returnPrice = S("$" + rp.format(1, 3)).padLeft(10).s;


            _obj.payAmount = S("$" + _obj.payAmount.format(1, 3)).padLeft(10).s;
            _obj.serviceCharge = S("$" + _obj.serviceCharge.format(1, 3)).padLeft(10).s;


            _obj.remainings = S("$" + _obj.remainings.format(1, 3)).padLeft(10).s;
            _obj.price = S("$" + _obj.price.format(1, 3)).padLeft(10).s;

            fs.writeFile("./printer/payment/" + moment().format("YYMMDDHHmmss") + ".json", JSON.stringify(_obj), function (err) {
                if (err) {
                    console.log(err);
                } else {
                    //console.log("The file was saved!");
                }
            });
            callback();

        });
    }

    this.BillTableOrder = (args, callback) => {
        console.log(JSON.stringify(args));
        //console.log(args.tableNum.$value);
        var callbackObj = {};
        var tableCode = args.tableNum.$value;
        var tableSubCode = "";
        var noOfPeople = args.peopleNum == undefined ? '' : args.peopleNum.$value;
        if (tableCode.indexOf('-') != -1) {
            var splitTableCode = tableCode.split('-');
            tableCode = splitTableCode[0];
            tableSubCode = splitTableCode[1];
        }
        //console.log("payMethod:" + args.payMethod.$value)
        //console.log("re:" + parseFloat(args.remainings.$value))
        //console.log("re:" + args.serviceCharge.$value)
        //console.log("re:" + parseFloat(args.serviceCharge.$value))
        //console.log("tableCode:" + tableCode);
        //console.log("tableSubCode:" + tableSubCode);
        var totalPayment = 0;
        var sql = "begin tran begin try \
declare @seq int EXEC @seq = spGetNextSeq \
declare @sseq varchar(10) = REPLICATE('0',6-LEN(RTRIM(@seq))) + RTRIM(@seq) \
insert into [transaction]  (tableCode, tableSubCode, pax, staffId, memberId, memberName, startTime, refNo, transTime, payAmount, price, serviceCharge, remainings ) select tableCode, tableSubCode, pax, staffId, memberId, memberName, startTime, @refNo + @sseq as refNo, getdate(), @payAmount, @price, @serviceCharge, @remainings from tableOrder where tableCode = @tableCode and tableSubCode = @tableSubCode; ";
        var totalPrice = parseFloat(args.price.$value);
        var sqlagrs = [{ "c": "tableCode", "t": TYPES.VarChar, "v": tableCode }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": tableSubCode }, { "c": "refNo", "t": TYPES.VarChar, "v": config.shopCode + moment().format("-YYMMDD-") }, { "c": "serviceCharge", "t": TYPES.Decimal, "v": parseFloat(args.serviceCharge.$value), "o": { "scale": 2 } }, { "c": "remainings", "t": TYPES.Decimal, "v": parseFloat(args.remainings.$value), "o": { "scale": 2 } }, { "c": "staffId", "t": TYPES.VarChar, "v": args.user.$value }];
        for (var i = 0; i < args.payMethod.$value.length; i++) {
            console.log("paymentValue" + parseFloat(args.payMethod.$value[i].amount));
            console.log("paymentValue" + args.payMethod.$value[i].method);
            var payseq = i + 1;
            sql += "insert into tenderpayment (paymentType,paymentValue,staffId,refNo,paymentDate,void,seq) values (@paymentType" + i.toString() + ",@paymentValue" + i.toString() + ",@staffId,@refNo + @sseq,getDate(),0,"+ payseq +");";
            sqlagrs.push({ "c": "paymentType" + i.toString(), "t": TYPES.VarChar, "v": args.payMethod.$value[i].method })
            sqlagrs.push({ "c": "paymentValue" + i.toString(), "t": TYPES.Decimal, "v": parseFloat(args.payMethod.$value[i].amount), "o": { "scale": 2 } })
            totalPayment +=  parseFloat(args.payMethod.$value[i].amount);
        }


        for (var i = 0; i < args.discountMethod.$value.length; i++) {
            var disseq = i + 1;
            sql += "insert into tenderdiscount (discountType,discountValue,discountCode,staffId,refNo,discountDate,void,seq,discountAmount) values (@discountType" + i.toString() + ",@discountValue" + i.toString() + ",@discountCode" + i.toString() + ",@staffId,@refNo + @sseq,getDate(),0," + payseq + ",@discountAmount"+i.toString()+");";
            sqlagrs.push({ "c": "discountType" + i.toString(), "t": TYPES.VarChar, "v": args.discountMethod.$value[i].type })
            sqlagrs.push({ "c": "discountValue" + i.toString(), "t": TYPES.Decimal, "v": parseFloat(args.discountMethod.$value[i].price), "o": { "scale": 2 } })
            sqlagrs.push({ "c": "discountCode" + i.toString(), "t": TYPES.VarChar, "v": args.discountMethod.$value[i].coupon_alias })
            sqlagrs.push({ "c": "discountAmount" + i.toString(), "t": TYPES.Decimal, "v": parseFloat(args.discountMethod.$value[i].amount), "o": { "scale": 2 } })
            totalPrice += parseFloat(args.discountMethod.$value[i].price)
        }
        sqlagrs.push({ "c": "price", "t": TYPES.Decimal, "v": totalPrice, "o": { "scale": 2 } })
        sqlagrs.push({ "c": "payAmount", "t": TYPES.Decimal, "v": totalPayment, "o": { "scale": 2 } })
        sql += "delete tableOrder where tableCode = @tableCode and tableSubCode = @tableSubCode; \
insert into transactionD (tableCode, tableSubCode, itemId, qty, price, discount, voidIndex, voidRemark, time, staffId, type, seq, round, refNo, customName, approveStaffId ) select tableCode, tableSubCode, itemId, qty, price, discount, voidIndex, voidRemark, time, staffId, type, seq, round, @refNo + @sseq as refNo, customName, approveStaffId from tableOrderD where tableCode = @tableCode and tableSubCode = @tableSubCode; \
delete tableOrderD where tableCode = @tableCode and tableSubCode = @tableSubCode; \
delete tablediscount where tableCode = @tableCode and tableSubCode = @tableSubCode; \
select @refNo + @sseq as refNo \
commit tran end try begin catch rollback tran; THROW; end catch "
        var query = {
            "sql": sql, "args": sqlagrs
        };
        //select * from transactionD where refNo = @refNo + cast(@seq as varchar(20))\
        this.DM.executeStatement(query, (err, result) => {
            console.log(err)
            console.log(result)
            var refNo = "";
            if (err == null) {
                refNo = result[0].refNo;
                this.PrintPaymentBill({ 'refNo': refNo, 'printTwice': true, openCashDrawer:true }, () => {

                })
                this.SetTableStatus({ "tableCode": tableCode, "status": 0 })
            }
            callback({ "result": refNo == "" ? "fail" : "OK","refNo":refNo,"tableCode":tableCode,"tableSubCode":tableSubCode })
        },'series')
    }

    this.ChangeTable = (args, callback) => {
        var dTableObj = getTableCode(args.destinationTableNo);
        var sTableObj = getTableCode(args.sourceTableNo);

        //var dTableCode = args.destinationTableNo;
        //var dTableSubCode = args.destinationTableNo.indexOf('_') == -1 ? "" : args.destinationTableNo.split('_')[1];
        //var sTableCode = args.sourceTableNo;
        //var sTableSubCode = args.sourceTableNo.indexOf('_') == -1 ? "" : args.sourceTableNo.split('_')[1];
        var query = [{ "sql": "select max(round) as round, max(seq) as seq, max(pax) as pax,max(memberId) as memberId from tableorderd d right outer join tableOrder o on o.tableCode = d.tableCode and o.tableSubCode = d.tableSubCode where o.tableCode = @tableCode and o.tableSubCode = @tableSubCode;", "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": dTableObj.tableCode }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": dTableObj.tableSubCode }] },
        { "sql": "select * from tableorderd where tableCode = @tableCode and tableSubCode = @tableSubCode", "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": dTableObj.tableCode }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": dTableObj.tableSubCode }] }];

        this.DM.executeStatement(query, (err, tableOrderD) => {
            console.log(tableOrderD);
            var currentSeq = 0;
            var currentRound = 0;
            var query = [];
            var _o = {};
            var totalPrice = 0;
            var serviceCharge = 0;
            if (tableOrderD[0][0].seq != null) {
                currentSeq = tableOrderD[0][0].seq;
            }
            if (tableOrderD[0][0].round != null) {
                currentRound = tableOrderD[0][0].round;
            }
            currentRound++;
            if (tableOrderD[1].length != 0) {
                for (var i = 0; i < tableOrderD[1].length; i++) {
                    var orderedItem = tableOrderD[1][i];
                    totalPrice += orderedItem.qty * orderedItem.price;
                }
            }

            if (currentSeq == 0 && currentRound == 1) {
                var changeTableArray = [];
                changeTableArray.push({ "sql": "update diningTable set status = 0 where name = @tableCode", "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": sTableObj.tableCode }] });
                changeTableArray.push({ "sql": "update diningTable set status = 2 where name = @tableCode", "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": dTableObj.tableCode }] });
                changeTableArray.push({ "sql": "update tableorder set tableCode = @destinationTableNo, tableSubCode = @destinationTableSubCode where tableCode = @sourceTableNo and tableSubCode = @sourceTableSubCode", "args": [{ "c": "destinationTableNo", "t": TYPES.VarChar, "v": dTableObj.tableCode }, { "c": "sourceTableNo", "t": TYPES.VarChar, "v": sTableObj.tableCode }, { "c": "destinationTableSubCode", "t": TYPES.VarChar, "v": dTableObj.tableSubCode }, { "c": "sourceTableSubCode", "t": TYPES.VarChar, "v": sTableObj.tableSubCode }] });
                changeTableArray.push({ "sql": "update tableorderd set tableCode = @destinationTableNo, tableSubCode = @destinationTableSubCode  where tableCode = @sourceTableNo and tableSubCode = @sourceTableSubCode", "args": [{ "c": "destinationTableNo", "t": TYPES.VarChar, "v": dTableObj.tableCode }, { "c": "sourceTableNo", "t": TYPES.VarChar, "v": sTableObj.tableCode }, { "c": "destinationTableSubCode", "t": TYPES.VarChar, "v": dTableObj.tableSubCode }, { "c": "sourceTableSubCode", "t": TYPES.VarChar, "v": sTableObj.tableSubCode }] });
                changeTableArray.push({ "sql": "update tableDiscount set tableCode = @destinationTableNo, tableSubCode = @destinationTableSubCode where tableCode = @sourceTableNo  and tableSubCode = @sourceTableSubCode", "args": [{ "c": "destinationTableNo", "t": TYPES.VarChar, "v": dTableObj.tableCode }, { "c": "sourceTableNo", "t": TYPES.VarChar, "v": sTableObj.tableCode }, { "c": "destinationTableSubCode", "t": TYPES.VarChar, "v": dTableObj.tableSubCode }, { "c": "sourceTableSubCode", "t": TYPES.VarChar, "v": sTableObj.tableSubCode }] });
                //changeTableArray.push({ "sql": "update xf_salesorder set xf_orderid = :1 || substr(xf_orderid,:2),xf_content = :3 || substr(xf_content,:4), xf_tableno = :5 where xf_tableno = :6", arg: [args.destinationTableNo, args.sourceTableNo.length + 1, args.destinationTableNo, args.sourceTableNo.length + 1, args.destinationTableNo, args.sourceTableNo] });
                this.DM.executeStatement(changeTableArray, (err, result) => {
                    //console.log(err);
                    this.LoadTableSchema(null, (err, result) => {
                        io.sockets.emit('refreshTable', JSON.stringify(result));
                    })
                }, 'series')
            } else {
                query = [{ "sql": "update tableOrderd set seq = seq + " + currentSeq + ", voidIndex = case when voidIndex is null then null else voidIndex + " + currentSeq + " end, tableCode =  @dTableCode, tableSubCode = @dTableSubCode where  tableCode = @tableCode and tableSubCode = @tableSubCode; update diningTable set status = 0 where name = @tableCode ", "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": sTableObj.tableCode }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": sTableObj.tableSubCode }, { "c": "dTableCode", "t": TYPES.VarChar, "v": dTableObj.tableCode }, { "c": "dTableSubCode", "t": TYPES.VarChar, "v": dTableObj.tableSubCode }, { "c": "seq", "t": TYPES.Int, "v": currentSeq }] }];

                this.DM.executeStatement(query, (err, result) => {
                    this.LoadTableSchema(null, (err, result) => {
                        io.sockets.emit('refreshTable', JSON.stringify(result));
                    })
                })

            }
        }, 'parallel');
    }

    this.PrintBill = (args, callback) => {
        console.log(args);

        this.PrintPaymentBill(args, () => {
            callback({ 'result': 'OK' });
        })
    }

    this.LoadStaff = (args, callback) => {
        console.log(args)
        console.log(callback)
        this.DM.executeStatement({ "sql": "select * FROM staff s inner join staffgroup sg on s.staffgroupcode = sg.staffgroupcode", "args": [] }, function (err, result) {
            callback(result);
        });
    }

    this.LoadBill = (args, callback) => {
        var sql = "declare @refNo varchar(500); select  top 1 @refNo = refNo from [transaction] t ";
        console.log(args);
        if (args) {
            if (args.searchNo) {
                pad(args.searchNo, 6)
                sql += " where refNo like  '%-" + pad(args.searchNo, 6) + "' ";
            } else
                if (args.refNo) {
                    var direction = " > ", orderby = " asc ";
                    if (args.direction == 'prev') {
                        direction = " < ";
                        orderby = " desc "
                    }
                    sql += " where refNo " + direction +" '" + args.refNo+ "'  order by refNo " + orderby;
                }
        } else {
            sql += " order by refNo desc ";
        }

        sql += "  select *, convert(varchar, transTime, 120) as transFormatTime from [transaction] t inner join [transactionD] td on t.refNo = td.refNo where t.refNo = @refNo; select * from tenderDiscount where refNo = @refNo and void = 0; select * from tenderPayment where refNo = @refNo  and void = 0"

        var query = { "sql": sql, "args": [] };
        //
        this.DM.executeStatement(query, (err, result) => {
            //console.log(result);
            var orders = [];
            var order = { 'version': '1.0', 'header': { 'orderId': '', 'tableNum': '', 'peopleNum': 0, 'member': '', 'memberName': '', 'memberMsg': '', 'refNo':'', 'transTime':'' }, 'item': [], 'itemCount': 0, 'payment':[],'discount':[] };
            //var tableOrder = result[0];
            if (result.length != 0) {
                var tableOrder = result[0];
                order.header.tableNum = tableOrder.tableCode + tableOrder.tableSubCode;
                order.header.peopleNum = tableOrder.pax;
                order.header.refNo = tableOrder.refNo;
                order.header.transTime = tableOrder.transFormatTime;
                order.header.orderId = "_";
            }
            var contentObj = [];
            for (var j = 0; j < result.length ; j++) {
                var currentItem = result[j];
                if (currentItem.itemId != undefined) {
                    var items = RAS.Item.filter(function (entry) {
                        return entry.code == currentItem.itemId;
                    });
                    var name1 = items[0].name1;
                    var name2 = items[0].name2;
                    var name3 = items[0].name3;
                    if (items[0].name) {
                        if (items[0].name[config.lang1])
                            name1 = items[0].name[config.lang1];
                        if (items[0].name[config.lang2])
                            name2 = items[0].name[config.lang2];
                        if (items[0].name[config.lang3])
                            name3 = items[0].name[config.lang3];
                    }
                    if (currentItem.customName != "") {
                        name1 = name2 = name3 = currentItem.customName;
                    }
                    var t = moment(currentItem.time).format("HH:mm:ss");
                    contentObj.push({ code: currentItem.itemId, type: currentItem.type, desc1: name1, desc2: name2, desc3: name3, qty: currentItem.qty, unitPrice: currentItem.price, time: t, voidIndex: currentItem.voidIndex, voidRemark: currentItem.voidRemark, staff: currentItem.staffId, index: currentItem.seq });
                }else if(currentItem.discountType != undefined){
                    order.discount.push({ "discountCode": currentItem.discountCode, "discountType": currentItem.discountType, "discountValue": currentItem.discountValue, "discountAmount": currentItem.discountAmount });
                } else if (currentItem.paymentType != undefined) {
                    order.payment.push({"paymentType":currentItem.paymentType,"paymentValue": currentItem.paymentValue})
                }
            }
            if (result.length != 0) {
                order.item = contentObj;
                order.itemCount = contentObj.length;
                orders.push(order);
            }
            var callbackObj = { result: "OK", timestamp: new Date().getTime(), taskID: 1, note: "", order: orders, suspendItems: [] }
            callback(callbackObj);
        })
    }

    this.DailyClearance = (args, callback) => {
        var query = { "sql": "select count(1) as c from tableorder; select count(1) as c from [transaction];", "args": [] };
        this.DM.executeStatement(query, (err, result) => {
            console.log(result)
            if (result[0].c != 0 ) {
                callback({ "result": "ERROR","msg":"please_complete_orders" });
            } else if (result[1].c == 0 ) {
                callback({ "result": "ERROR","msg":"no_order" });
            } else {
                this.PrepareDailyReport({ isPrint: true }, (obj) => {
                    var query = {
                        "sql": "begin tran begin try insert into [transactionArchive]  (tableCode, tableSubCode, pax, staffId, memberId, memberName, startTime, refNo, transTime, payAmount, price, serviceCharge, remainings, reprint, void, voidStaff, archiveStaff, archiveDate ) select tableCode, tableSubCode, pax, staffId, memberId, memberName, startTime, refNo, transTime, payAmount, price, serviceCharge, remainings, reprint, void, voidStaff, @archiveStaff, getDate() from [transaction];\
                    insert into transactionDArchive (tableCode, tableSubCode, itemId, qty, price, discount, voidIndex, voidRemark, time, staffId, type, seq, round, refNo, customName, approveStaffId ) select tableCode, tableSubCode, itemId, qty, price, discount, voidIndex, voidRemark, time, staffId, type, seq, round, refNo, customName,approveStaffId  from [transactionD]; \
insert into tenderpaymentArchive (paymentType,paymentValue,staffId,refNo,paymentDate,void,seq,voidDate,voidStaff) select paymentType,paymentValue,staffId,refNo,paymentDate,void,seq,voidDate,voidStaff from tenderpayment \
insert into tenderdiscountArchive (discountType,discountValue,discountCode,staffId,refNo,discountDate,void,voidStaff,voidDate,seq,discountAmount) select  discountType,discountValue,discountCode,staffId,refNo,discountDate,void,voidStaff,voidDate,seq,discountAmount from tenderDiscount\
                delete [tenderpayment]; delete [tenderDiscount]; delete [transaction];  delete transactionD;\
                commit tran end try begin catch rollback tran; THROW; end catch "
                  , "args": [{ "c": "archiveStaff", "t": TYPES.VarChar, "v": args.usercode }]
                    };
                    this.DM.executeStatement(query, (err, result) => {
                        callback({ "result": "OK","resultObj":obj });
                    })

                });

            }
        });
    }

    this.PrepareDailyReport = (args, callback) => {
        var query = [{ "sql": "select * from tenderpayment  where void = 0 ", "args": [] },
        { "sql": "select * from tenderDiscount where void = 0 ", "args": [] },
        { "sql": "select * from [transaction] where void = 0 ", "args": [] },
        { "sql": "select * from [tableOrder] ", "args": [] }];
        var date = moment().format("DD/MM/YYYY");
        console.log(args);
        if (args) {
            console.log(args.year);
            if (args.year) {
                var argsdate = pad(args.day, 2) + "/" + pad(args.month, 2) + "/" + args.year;

                if (args.toYear) {

                    var argsTodate = pad(args.toDay, 2) + "/" + pad(args.toMonth, 2) + "/" + args.toYear;
                    var sqlFromDates = new Date(args.year, parseInt(args.month) - 1 , args.day);
                    var sqlToDates = new Date(args.toYear, parseInt(args.toMonth) - 1, parseInt(args.toDay) + 1);
                    console.log(sqlFromDates);
                    console.log(sqlToDates);
                    date = argsdate + ' - ' + argsTodate;
                    query = [{ "sql": "select * from tenderpaymentArchive where  exists(select * from transactionArchive where transTime between @fromDate and @toDate and transactionArchive.refNo = tenderPaymentArchive.refNo)  and void = 0 ", "args": [{ "c": "fromDate", "t": TYPES.DateTime, "v": sqlFromDates }, { "c": "toDate", "t": TYPES.DateTime, "v": sqlToDates }] },
                        { "sql": "select * from tenderDiscountArchive where   exists(select * from transactionArchive where transTime between @fromDate and @toDate  and transactionArchive.refNo = tenderDiscountArchive.refNo)  and void = 0 ", "args": [{ "c": "fromDate", "t": TYPES.DateTime, "v": sqlFromDates }, { "c": "toDate", "t": TYPES.DateTime, "v": sqlToDates }] },
                        { "sql": "select * from transactionArchive where  transTime between  @fromDate and @toDate and void = 0 ", "args": [{ "c": "fromDate", "t": TYPES.DateTime, "v": sqlFromDates }, { "c": "toDate", "t": TYPES.DateTime, "v": sqlToDates }] },
                        { "sql": "select * from [tableOrder] where 1 = 0 ", "args": [] }];
                } else {
                    if (date == argsdate) {
                        query = [{ "sql": "select * from tenderpayment union select * from tenderpaymentArchive where exists(select * from transactionArchive where convert(char(10), transTime, 103) = @date and transactionArchive.refNo = tenderpaymentArchive.refNo)   and void = 0 ", "args": [{ "c": "date", "t": TYPES.VarChar, "v": argsdate }] },
                        { "sql": "select * from tenderDiscount where void = 0  union select * from tenderDiscountArchive where exists(select * from transactionArchive where convert(char(10), transTime, 103) = @date and transactionArchive.refNo = tenderDiscountArchive.refNo)   and void = 0 ", "args": [{ "c": "date", "t": TYPES.VarChar, "v": argsdate }] },
                        { "sql": "select * from [transaction] where void = 0  union select [tableCode] ,[tableSubCode] ,[pax],[staffId],[memberId],[memberName],[startTime],[refNo],[transTime],[payAmount],[price],[serviceCharge],[remainings],[reprint]      ,[void],[voidStaff] from transactionArchive where convert(char(10), transTime, 103) = @date  and void = 0 ", "args": [{ "c": "date", "t": TYPES.VarChar, "v": argsdate }] },
                        { "sql": "select * from [tableOrder] ", "args": [] }];
                    } else {
                        date = argsdate;
                        query = [{ "sql": "select * from tenderpaymentArchive where  exists(select * from transactionArchive where convert(char(10), transTime, 103) = @date and transactionArchive.refNo = tenderPaymentArchive.refNo)  and void = 0 ", "args": [{ "c": "date", "t": TYPES.VarChar, "v": argsdate }] },
                        { "sql": "select * from tenderDiscountArchive where   exists(select * from transactionArchive where convert(char(10), transTime, 103) = @date and transactionArchive.refNo = tenderDiscountArchive.refNo)  and void = 0 ", "args": [{ "c": "date", "t": TYPES.VarChar, "v": argsdate }] },
                        { "sql": "select * from transactionArchive where convert(char(10), transTime, 103) = @date and void = 0 ", "args": [{ "c": "date", "t": TYPES.VarChar, "v": argsdate }] },
                        { "sql": "select * from [tableOrder] where 1 = 0 ", "args": [] }];
                    }
                }
            }
        }
        this.DM.executeStatement(query, (err, result) => {
            console.log(result);
            var obj = { GROSS_TOTAL_AMOUNT: 0, DISCOUNT: 0, NO_OF_DISCOUNT: 0, SERVICE_CHARGE: 0, GROSS_SALES_NET: 0, DECIMAIL_POINT: 0, TOTAL_CARD_TIPS: 0, NO_OF_CARD_TIPS: 0, TOTAL_NO_OF_CARD: 0, TOTAL_CARD_AMOUNT: 0, NONCASHSALES: [], CASHSALES: { paymentValue: 0, count: 0 }, CARDTIPS: [], DISCOUNTDETAILS: [], NO_OF_INVOICE: 0, GROSS_SALES_NET_PER_INVOICE: 0, TOTAL_PAX: 0, TOTAL_PAX_PER_AMOUNT: 0, TABLEORDER: [], TOTAL_SALES_AMOUNT: 0, ENTERTAINMENT: [], REPORT_DATE:'' };
            var transaction = result[2];
            var discount = result[1];
            var payment = result[0];
            var tableOrder = result[3];
            if (tableOrder) {
                for (var i = 0; i < tableOrder.length; i++) {
                    var to = tableOrder[i];
                    if (i == 0) {
                        obj.TABLEORDER.push({ "pax": to.pax, "price": to.price + to.serviceCharge, "count": 1 })
                    } else {
                        obj.TABLEORDER[0].pax += to.pax;
                        obj.TABLEORDER[0].price += to.price + to.serviceCharge;
                        obj.TABLEORDER[0].count += 1;
                    }
                }
            }
            if (transaction) {

                for (var i = 0; i < transaction.length; i++) {
                    var t = transaction[i];
                    obj.REPORT_DATE = date;
                    obj.TOTAL_PAX += t.pax;
                    obj.GROSS_TOTAL_AMOUNT += t.price;
                    obj.SERVICE_CHARGE += t.serviceCharge;
                    obj.DECIMAIL_POINT += t.remainings;
                    var totalPrice = t.price + t.serviceCharge + t.remainings;
                    var dis = discount.filter(function (dis) {
                        return dis.refNo == t.refNo;
                    })
                    for (var j = 0; j < dis.length; j++) {
                        totalPrice -= dis[j].discountValue;
                    }
                    var po = payment.filter(function (po) {
                        return po.refNo == t.refNo;
                    })
                    var pricefornoncash = totalPrice
                    for (var j = 0; j < po.length; j++) {
                        var p = po[j];
                        if (p.paymentType == 'cash') {
                            if (pricefornoncash < p.paymentValue) {
                                obj.CASHSALES.paymentValue += pricefornoncash;
                                pricefornoncash = 0;
                            } else {
                                obj.CASHSALES.paymentValue += p.paymentValue;
                                pricefornoncash -= p.paymentValue;
                            }
                            obj.CASHSALES.count += 1;
                            break;
                        }
                    }
                    for (var j = 0; j < po.length; j++) {
                        var p = po[j];
                        if (p.paymentType != 'cash') {
                            var nonCashPayment = 0;
                            if (p.paymentValue > pricefornoncash) {
                                nonCashPayment = pricefornoncash;
                                if (p.paymentType != 'octopus' && p.paymentType != 'entertainment' && p.paymentType != 'on_acct') {
                                    obj.TOTAL_CARD_AMOUNT += pricefornoncash;
                                    var cardtips = p.paymentValue - pricefornoncash
                                    obj.TOTAL_CARD_TIPS += cardtips;
                                    obj.NO_OF_CARD_TIPS += 1;
                                    var foundMatchCardTipsPayment = false;
                                    for (var k = 0; k < obj.CARDTIPS.length; k++) {
                                        if (p.paymentType == obj.CARDTIPS[k].paymentType) {
                                            obj.CARDTIPS[k].tipsValue += cardtips
                                            obj.CARDTIPS[k].count += 1;
                                            foundMatchCardTipsPayment = true;
                                            break;
                                        }
                                    }
                                    if (!foundMatchCardTipsPayment) {
                                        obj.CARDTIPS.push({ paymentType: p.paymentType, tipsValue: cardtips, count: 1 })
                                    }
                                }
                                pricefornoncash = 0;
                            } else {
                                if (p.paymentType != 'octopus' && p.paymentType != 'entertainment' && p.paymentType != 'on_acct') {
                                    obj.TOTAL_CARD_AMOUNT += p.paymentValue;
                                }
                                nonCashPayment = p.paymentValue;
                                pricefornoncash -= p.paymentValue
                            }

                            var foundMatchPaymentType = false;
                            for (var k = 0; k < obj.NONCASHSALES.length; k++) {
                                if (obj.NONCASHSALES[k].paymentType == p.paymentType) {
                                    obj.NONCASHSALES[k].paymentValue += nonCashPayment;
                                    obj.NONCASHSALES[k].count += 1;
                                    foundMatchPaymentType = true;
                                }
                            }
                            if (!foundMatchPaymentType) {
                                obj.NONCASHSALES.push({ "paymentType": p.paymentType, "paymentValue": nonCashPayment, "count": 1 })
                            }
                        }
                    }
                }
            }

            for (var i = 0; i < obj.NONCASHSALES.length; i++) {
                if (obj.NONCASHSALES[i].paymentType == "entertainment") {
                    obj.ENTERTAINMENT.push({ count: obj.NONCASHSALES[i].count, paymentValue: obj.NONCASHSALES[i].paymentValue })
                }
            }

            console.log(obj.TOTAL_CARD_AMOUNT);
            for (var i = 0; i < discount.length; i++) {
                var d = discount[i];
                obj.DISCOUNT += d.discountValue;
                var foundMatchDiscountDetails = false;
                for (var k = 0; k < obj.DISCOUNTDETAILS.length; k++) {
                    if (obj.DISCOUNTDETAILS[k].discountCode == d.discountCode) {
                        obj.DISCOUNTDETAILS[k].discountValue += d.discountValue;
                        obj.DISCOUNTDETAILS[k].count += 1;
                        foundMatchDiscountDetails = true;
                        break;
                    }
                }
                if (!foundMatchDiscountDetails) {
                    obj.DISCOUNTDETAILS.push({ discountCode:d.discountCode,discountValue:d.discountValue,count:1})
                }
            }
            obj.GROSS_SALES_NET = obj.GROSS_TOTAL_AMOUNT + obj.SERVICE_CHARGE + obj.DECIMAIL_POINT - obj.DISCOUNT
            if (obj.ENTERTAINMENT.length != 0) {
                obj.GROSS_SALES_NET -= obj.ENTERTAINMENT[0].paymentValue;
            }
            var cardPayment = payment.filter((p) => { return p.paymentType != 'cash' && p.paymentType != 'octopus' && p.paymentType != 'entertainment' });
            console.log(cardPayment);
            obj.NO_OF_DISCOUNT = discount.length;
            obj.TOTAL_NO_OF_CARD = cardPayment.length;
            obj.NO_OF_INVOICE = transaction.length;
            obj.GROSS_SALES_NET_PER_INVOICE = obj.GROSS_SALES_NET / obj.NO_OF_INVOICE;
            obj.TOTAL_PAX_PER_AMOUNT = obj.GROSS_SALES_NET / obj.TOTAL_PAX;
            obj.TOTAL_SALES_AMOUNT = obj.GROSS_SALES_NET;
            if (obj.TABLEORDER.length != 0) {
                obj.TOTAL_SALES_AMOUNT += obj.TABLEORDER[0].price;
            }
            //if (obj.TOTAL_NO_OF_CARD != 0) {
            //    obj.TOTAL_CARD_AMOUNT = cardPayment.reduce((sum, val) => { return sum + val.paymentValue },0)
            //}
            //obj.TOTAL_CARD_TIPS = obj.TOTAL_CARD_TIPS.format(1, 3);
            //obj.GROSS_SALES_NET = obj.GROSS_SALES_NET.format(1, 3);
            //obj.GROSS_TOTAL_AMOUNT = obj.GROSS_TOTAL_AMOUNT.format(1, 3);
            callback(obj);

            if (args.isPrint) {

                var _obj = {};
                extend(obj, _obj);

                _obj.GROSS_TOTAL_AMOUNT = S("$" + _obj.GROSS_TOTAL_AMOUNT.format(1, 3)).padLeft(10).s;
                _obj.DISCOUNT = S("$" + _obj.DISCOUNT.format(1, 3)).padLeft(10).s;
                //_obj.NO_OF_DISCOUNT = S("$" + _obj.NO_OF_DISCOUNT.format(1, 3)).padLeft(10).s;
                _obj.SERVICE_CHARGE = S("$" + _obj.SERVICE_CHARGE.format(1, 3)).padLeft(10).s;
                _obj.GROSS_SALES_NET = S("$" + _obj.GROSS_SALES_NET.format(1, 3)).padLeft(10).s;
                _obj.DECIMAIL_POINT = S("$" + _obj.DECIMAIL_POINT.format(1, 3)).padLeft(10).s;
                _obj.TOTAL_CARD_TIPS = S("$" + _obj.TOTAL_CARD_TIPS.format(1, 3)).padLeft(10).s;
                //_obj.NO_OF_CARD_TIPS = S("$" + _obj.NO_OF_CARD_TIPS.format(1, 3)).padLeft(10).s;
                //_obj.TOTAL_NO_OF_CARD = S("$" + _obj.TOTAL_NO_OF_CARD.format(1, 3)).padLeft(10).s;
                _obj.TOTAL_CARD_AMOUNT = S("$" + _obj.TOTAL_CARD_AMOUNT.format(1, 3)).padLeft(10).s;

                for (var i = 0; i < _obj.NONCASHSALES.length; i++) {
                    _obj.NONCASHSALES[i].paymentValue = S("$" + _obj.NONCASHSALES[i].paymentValue.format(1, 3)).padLeft(10).s;
                }
                _obj.CASHSALES.paymentValue = S("$" + _obj.CASHSALES.paymentValue.format(1, 3)).padLeft(10).s;

                for (var i = 0; i < _obj.CARDTIPS.length; i++) {
                    _obj.CARDTIPS[i].tipsValue = S("$" + _obj.CARDTIPS[i].tipsValue.format(1, 3)).padLeft(10).s;
                }
                for (var i = 0; i < _obj.DISCOUNTDETAILS.length; i++) {
                    _obj.DISCOUNTDETAILS[i].discountValue = S("$" + _obj.DISCOUNTDETAILS[i].discountValue.format(1, 3)).padLeft(10).s;
                }
                _obj.GROSS_SALES_NET_PER_INVOICE = S("$" + _obj.GROSS_SALES_NET_PER_INVOICE.format(1, 3)).padLeft(10).s;
                _obj.TOTAL_PAX_PER_AMOUNT = S("$" + _obj.TOTAL_PAX_PER_AMOUNT.format(1, 3)).padLeft(10).s;


                for (var i = 0; i < _obj.TABLEORDER.length; i++) {
                    _obj.TABLEORDER[i].price = S("$" + _obj.TABLEORDER[i].price.format(1, 3)).padLeft(10).s;
                }

                _obj.TOTAL_SALES_AMOUNT = S("$" + _obj.TOTAL_SALES_AMOUNT.format(1, 3)).padLeft(10).s;

                if (_obj.ENTERTAINMENT.length != 0) {
                    _obj.ENTERTAINMENT[0].paymentValue = S("$" + _obj.ENTERTAINMENT[0].paymentValue.format(1, 3)).padLeft(10).s;;
                }
                //_obj.CASHSALESPAYMENTVALUE = _obj.CASHSALES.paymentValue;
                //_obj.CASHSALESPAYMENTCOUNT = _obj.CASHSALES.count;

                fs.writeFile("./printer/dailyReport/" + moment().format("YYMMDDHHmmss") + ".json", JSON.stringify(_obj), function (err) {
                    if (err) {
                        console.log(err);
                    } else {
                        //console.log("The file was saved!");
                    }
                });
            }

        }, 'parallel');
    }

    this.PrepareDailyAttendanceReport = (args, callback) => {
        var query = [{ "sql": "select * from staffAtt", "args": [] }];
        var date = moment().format("DD/MM/YYYY");
        var obj = {"attendances":[], "REPORT_DATE":''};
        console.log(args);
        if (args) {
            console.log(args.year);
            if (args.year) {
                var argsdate = pad(args.day, 2) + "/" + pad(args.month, 2) + "/" + args.year;

                if (args.toYear) {

                    var argsTodate = pad(args.toDay, 2) + "/" + pad(args.toMonth, 2) + "/" + args.toYear;
                    var sqlFromDates = new Date(args.year, parseInt(args.month) - 1 , args.day);
                    var sqlToDates = new Date(args.toYear, parseInt(args.toMonth) - 1, parseInt(args.toDay) + 1);
                    console.log(sqlFromDates);
                    console.log(sqlToDates);
                    date = argsdate + ' - ' + argsTodate;
                    query = [{ "sql": "select * from staffAtt where datetime between @fromDate and @toDate", "args": [{ "c": "fromDate", "t": TYPES.DateTime, "v": sqlFromDates }, { "c": "toDate", "t": TYPES.DateTime, "v": sqlToDates }] }];
                } else {
                    if (date == argsdate) {
                        query = [{ "sql": "select * from staffAtt where convert(char(10), transTime, 103) = @date ", "args": [{ "c": "date", "t": TYPES.VarChar, "v": argsdate }] }];
                    } else {
                        date = argsdate;
                        query = [{ "sql": "select * from staffAtt where convert(char(10), transTime, 103) = @date ", "args": [{ "c": "date", "t": TYPES.VarChar, "v": argsdate }] }];
                    }
                }
            }
        }

        this.DM.executeStatement(query, (err, result) => {
            console.log(result);
            obj.attendances = result[0];
            obj.REPORT_DATE = date;
            callback(obj);

            if (args.isPrint) {

                var _obj = {};
                extend(obj, _obj);

                fs.writeFile("./printer/dailyReport/attendance_" + moment().format("YYMMDDHHmmss") + ".json", JSON.stringify(_obj), function (err) {
                    if (err) {
                        console.log(err);
                    } else {
                        //console.log("The file was saved!");
                    }
                });
            }
        })
    }

    this.LoadReport = (args, callback) => {
        console.log(args);
        switch (args.type) {
            case "dailyAttendance":
                this.PrepareDailyAttendanceReport(args, (obj) => {
                    callback(obj);
                });
                break;
            default: // add type to thie daily report
                this.PrepareDailyReport(args, (obj) => {
                    callback(obj);
                });
        }
        // this.PrepareDailyReport(args, (obj) => {
        //     callback(obj);
        // });
    }


    this.EditStaff = (args,callback) => {
        //args = JSON.parse(args);
        //console.log(args);
        var validUserName = true;
        var query = {};
        if (args.oldusername != args.username) {}
        query = { "sql": "select * from staff where username = @username", "args": [{ "c": "username", "t": TYPES.VarChar, "v": args.username }] }
        this.DM.executeStatement(query, (err, result) => {
            if (args.oldusername != args.username && result.length != 0) {
                callback({ result: "ERROR", msg: "duplicateUser" });
            } else if (args.oldusername == 'new') {
                query = { "sql": "insert into staff (staffId, name, username, password, staffGroupCode) values (@staffId, @name, @username, @password, @staffGroupCode)", "args": [{ "c": "username", "t": TYPES.VarChar, "v": args.username }, { "c": "password", "t": TYPES.VarChar, "v": args.password }, { "c": "staffGroupCode", "t": TYPES.VarChar, "v": args.staffGroupCode }, { "c": "name", "t": TYPES.VarChar, "v": args.name }, { "c": "staffId", "t": TYPES.VarChar, "v": 'new' }] };
                this.DM.executeStatement(query, (err, result) => {
                    callback({ result: "OK" });
                })
            }
            else {
                query = { "sql": "update staff set username = @username, password = @password , staffGroupCode = @staffGroupCode,name = @name where username = @oldusername", "args": [{ "c": "username", "t": TYPES.VarChar, "v": args.username }, { "c": "password", "t": TYPES.VarChar, "v": args.password }, { "c": "staffGroupCode", "t": TYPES.VarChar, "v": args.staffGroupCode }, { "c": "name", "t": TYPES.VarChar, "v": args.name }, { "c": "oldusername", "t": TYPES.VarChar, "v": args.oldusername }] };
                this.DM.executeStatement(query, (err, result) => {
                    callback({ result: "OK" });
                })
            }
        });
    }

    this.DeleteStaff = (args, callback) => {
        //args = JSON.parse(args);
        //console.log(args);
        var query = {};
        query = { "sql": "delete staff where username = @username", "args": [{ "c": "username", "t": TYPES.VarChar, "v": args.username }] }
        this.DM.executeStatement(query, (err, result) => {
            callback({ result: "OK" });
        });
    }

    this.SaveTableSchema = (args, callback) => {
        var query = {};
        query = { "sql": "update shopZone set floorPlan = @floorPlan where shopZoneId =1 and shopId = 1", "args": [{ "c": "floorPlan", "t": TYPES.NVarChar, "v": args.layout }] }
        this.DM.executeStatement(query, (err, result) => {
            callback({ 'result': 'OK' });
        })
    }

    this.AmendBill = (args, callback) => {
        console.log(args);
        for (var key in args) {
            if (args.hasOwnProperty(key)) {
                console.log(key + " -> " + args[key]);
            }
        }
        var query = {};
        var sql = "update tenderpayment set void = 1, voidstaff = @staffId,voidDate = getdate() where refNo = @refNo; update [transaction] set payAmount = @payAmount where refNo = @refNo;";
        var sqlagrs = [{ "c": "refNo", "t": TYPES.VarChar, "v": args.refNo.$value }, { "c": "staffId", "t": TYPES.VarChar, "v": args.user.$value }]
        var totalPayment = 0;

        console.log(args.user);
        for (var i = 0; i < args.payMethod.$value.length; i++) {
            //console.log("paymentValue" + parseFloat(args.payMethod.$value[i].amount));
            //console.log("paymentValue" + args.payMethod.$value[i].method);
            var payseq = i + 1;
            sql += "insert into tenderpayment (paymentType,paymentValue,staffId,refNo,paymentDate,void,seq) values (@paymentType" + i.toString() + ",@paymentValue" + i.toString() + ",@staffId,@refNo ,getDate(),0," + payseq + ");";
            sqlagrs.push({ "c": "paymentType" + i.toString(), "t": TYPES.VarChar, "v": args.payMethod.$value[i].method })
            sqlagrs.push({ "c": "paymentValue" + i.toString(), "t": TYPES.Decimal, "v": parseFloat(args.payMethod.$value[i].amount), "o": { "scale": 2 } })
            totalPayment += parseFloat(args.payMethod.$value[i].amount);
        }
        sqlagrs.push({ "c": "payAmount", "t": TYPES.Decimal, "v": totalPayment, "o": { "scale": 2 } })
        this.DM.executeStatement({ "sql": sql, "args": sqlagrs }, (err, result) => {
            callback({ 'result': 'OK' });
        })
    }

    this.LinkTable = (args, callback) =>{
        var tableCodeObj = getTableCode(args.tableNum.$value);
        var query = [{
            "sql": "select startTime from tableorder where tablecode = @tableCode",
            "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": tableCodeObj.tableCode }]
        }, {
            "sql": "select * from tableorderd where tablecode = @tableCode",
            "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": tableCodeObj.tableCode }]
        }]
        this.DM.executeStatement(query, (err, result) => {
            var rCode = 'ERROR';
            var msg = "The table does not found";
            var ot = '';

            if (result[0].length > 0) {
                rCode = 'OK'
                ot = moment(result[0][0].startTime).format("HH:mm:ss");
                msg = "";
            }
            var r = { 'result': rCode, 'timestamp': new Date().getTime(), 'taskID': new Date().getTime(), 'note': msg, 'order': { 'version': '1.0', 'header': { 'tableNum': tableCodeObj.tableCode, 'openTime': ot, 'member': '', 'memberName': '', 'memberMsg': '', 'lastOrder': '', 'lastOrderWarning': '' } } }
            console.log(result);
            try {
                if (result[1].length > 0) {

                    for (var k = 0; k < result[1].length ; k++) {
                        var currentItem = result[1][k];
                        if (currentItem.type == "T") {
                            var items = RAS.Item.filter(function (entry) {
                                return entry.code == currentItem.itemId;
                            });
                            var _lastOrder = items[0].namek.split(',');
                            r.order.header.lastOrderWarning = _lastOrder[0];
                            r.order.header.lastOrder = _lastOrder[1];
                        }
                    }
                }
                console.log(JSON.stringify(r));
            } catch (e) { console.log(e)}
            callback(r);
        })
        //<result>OK</result><timestamp>" + new Date().getTime() + "</timestamp><taskID>1</taskID><note></note><order><version>1.0</version><header><tableNum>" + args.tableNum.$value + "</tableNum><member></member><memberName></memberName><memberMsg></memberMsg></header>" + "</order>
    }

    this.LoadTableForTransfer = (args, callback) => {
        var query = [{ "sql": "select name from diningTable order by name", "args": null }, { "sql": "select * from tableorder order by tablecode,tablesubcode", "args": null }];
        this.DM.executeStatement(query, (err, result) => {
            console.log(result[0]);
            result[0].sort(function (a, b) {
                try{
                    return parseInt(a.name) - parseInt(b.name);
                } catch (e) {
                    console.log(e);
                    return a.name - b.name;
                }
            });
            console.log(result[0]);
            var _o = result[0];
            callback(_o);
        }, 'parallel')
    }

    this.TransferItem = (args, callback) => {
        console.log(args);

        var query = [{ "sql": "select max(round) as round, max(seq) as seq, max(pax) as pax,max(memberId) as memberId from tableorderd d right outer join tableOrder o on o.tableCode = d.tableCode and o.tableSubCode = d.tableSubCode where o.tableCode = @tableCode and o.tableSubCode = @tableSubCode;", "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": tableCode }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": tableSubCode }] }, { "sql": "select * from tableorderd where tableCode = @tableCode and tableSubCode = @tableSubCode", "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": tableCode }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": tableSubCode }] }];

        /*
        this.DM.executeStatement(query, (err, tableOrderD) => {
            var currentSeq = 0;
            var currentRound = 0;
            var query = [];
            var _o = {};
            var totalPrice = 0;
            var serviceCharge = 0;
            console.log(tableOrderD);
            if (tableOrderD[0][0].seq != null) {
                currentSeq = tableOrderD[0][0].seq;
            }
            if (tableOrderD[0][0].round != null) {
                currentRound = tableOrderD[0][0].round;
            }
            currentRound++;
            if (tableOrderD[1].length != 0) {
                for (var i = 0; i < tableOrderD[1].length; i++) {
                    var orderedItem = tableOrderD[1][i];
                    totalPrice += orderedItem.qty * orderedItem.price;
                }
            }

            for (var i = 0 ; i < r.order.item.length; i++) {
                var vi = r.order.item[i].voidIndex == undefined ? "-1" : r.order.item[i].voidIndex[0];
                var vr = r.order.item[i].voidRemark == undefined ? "" : r.order.item[i].voidRemark[0];
                var approveStaff = r.order.item[i].voidApproveStaff == undefined || null ? "" : r.order.item[i].voidApproveStaff[0];
                var type = r.order.item[i].type;
                var itemOrderTime = moment().format("HH:mm");
                if (type == "I") {
                    currentSeq += 1;
                }
                totalPrice += parseFloat((parseFloat(r.order.item[i].unitPrice[0]) / 100).toFixed(2)) * parseInt(r.order.item[i].qty[0]);
                console.log(currentSeq)
                console.log(r.order.item[i].unitPrice[0])
                var customName = '';
                if (r.order.item[i].customName) {
                    if (r.order.item[i].customName[0]) {
                        customName = r.order.item[i].customName[0]
                    }
                }
                _o = {
                    "sql": "insert into tableOrderD (tableCode,tableSubCode,itemId,qty,price,voidIndex,voidRemark,type,seq,time,staffId,round,customName,approveStaffId) values (@tableCode,@tableSubCode,@itemId,@qty,@price,@voidIndex,@voidRemark,@type,@seq,@time,@staffId,@round,@customName,@approveStaffId)",
                    "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": tableCode }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": tableSubCode }, { "c": "itemId", "t": TYPES.VarChar, "v": r.order.item[i].code[0] }, { "c": "qty", "t": TYPES.Int, "v": parseInt(r.order.item[i].qty[0]) },
                        { "c": "price", "t": TYPES.Decimal, "v": parseFloat((parseFloat(r.order.item[i].unitPrice[0]) / 100).toFixed(2)), "o": { "scale": 2 } }, { "c": "voidIndex", "t": TYPES.Int, "v": vi }, { "c": "voidRemark", "t": TYPES.NVarChar, "v": vr }, { "c": "type", "t": TYPES.VarChar, "v": type }, { "c": "seq", "t": TYPES.Int, "v": currentSeq }, { "c": "time", "t": TYPES.Time, "v": date }, { "c": "staffId", "t": TYPES.VarChar, "v": staff }, { "c": "round", "t": TYPES.Int, "v": currentRound }, { "c": "customName", "t": TYPES.NVarChar, "v": customName }, { "c": "approve3", "t": TYPES.VarChar, "v": approveStaff }]
                }
                query.push(_o);
            }

            serviceCharge = (totalPrice / 10).toFixed(1);
            _o = {
                "sql": "insert into tableOrder (tableCode,tableSubCode,pax,memberId,startTime,staffId, price, serviceCharge) values (@tableCode,@tableSubCode,@pax,@memberId,@startTime,@staffId, @price, @serviceCharge)",
                "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": tableCode }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": tableSubCode }, { "c": "pax", "t": TYPES.Int, "v": noOfPeople }, { "c": "memberId", "t": TYPES.Int, "v": null }, { "c": "startTime", "t": TYPES.DateTime, "v": date }, { "c": "staffId", "t": TYPES.VarChar, "v": staff },
                { "c": "price", "t": TYPES.Decimal, "v": parseFloat(totalPrice), "o": { "scale": 2 } },
                { "c": "serviceCharge", "t": TYPES.Decimal, "v": parseFloat(serviceCharge), "o": { "scale": 2 } }]
            }
            if (tableOrderD[0][0].pax == null) {
                query.push(_o);
            } else {
                _o = {
                    "sql": "update tableOrder set pax = @pax, price = @price, serviceCharge = @serviceCharge where tableCode = @tableCode and tableSubCode = @tableSubCode",
                    "args": [{ "c": "pax", "t": TYPES.Int, "v": noOfPeople }, { "c": "tableCode", "t": TYPES.VarChar, "v": tableCode }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": tableSubCode },
                { "c": "price", "t": TYPES.Decimal, "v": parseFloat(totalPrice), "o": { "scale": 2 } },
                { "c": "serviceCharge", "t": TYPES.Decimal, "v": parseFloat(serviceCharge), "o": { "scale": 2 } }]
                }
                query.push(_o);
            }

            this.DM.executeStatement(query, (err, result) => {
                console.log(err);
                //console.log(result);
                var _result = "OK";
                callbackObj = { result: _result, timestamp: new Date().getTime(), taskID: 1, note: '', suspendItems: [] };
                callback(callbackObj);
                printingArray.Item.sort(function (a, b) {
                    return a.Printer > b.Printer;
                });
                this.SetTableStatus({ "tableCode": tableCode, "status": 2 })
            }, 'parallel')
        })
        */

        //this.DM.executeStatement(query, (err, r) => {

        //}, 'parallel');
        callback({'result':'OK'})
        }

        this.addDiceMessage = (args) => {
            args = JSON.parse(args);
            console.log("addMessage", args);
            if (args.action === "checkout" || args.action === "callService" || args.action === "tea") {
                var today = new Date(new Date().getTime() - (2 * 60 * 1000)), tommorow = getTommorow();
                var query = {
                    "sql": "select * from message where isDelete = @isDelete and tableno = @tableno and action = @action and date between @today and @tommorow",
                    "args": [
                        { "c": "isDelete", "t": TYPES.Bit, "v": false },
                        { "c": "tableno", "t": TYPES.VarChar, "v": args.tableno },
                        { "c": "action", "t": TYPES.VarChar, "v": args.action },
                        { "c": "today", "t": TYPES.DateTime, "v": today },
                        { "c": "tommorow", "t": TYPES.DateTime, "v": tommorow }
                    ]
                };
                this.DM.executeStatement(query, (err, _msg) => {
                    if (err) return;
                    console.log("addMessage Check", _msg);
                    if (_msg.length > 0) {
                        console.log("addMessage", "prevent multiple request");
                    }
                    else {
                        this.saveMessage(args);
                    }
                });
                //Message.find({
                //    "date": {
                //        "$gte": today,
                //        "$lt": tommorow
                //    },
                //    isDelete: false,
                //    tableno: args.tableno,
                //    action: args.action
                //}).sort({ date: -1 })
                //    .exec(function (err, _msg) {
                //        if (err) return;
                //        //console.log("addMessage Check", _msg);
                //        if (_msg.length > 0) {
                //            console.log("addMessage", "prevent multiple request");
                //        }
                //        else {
                //            saveMessage(args);
                //        }
                //    });
            }
            else {
                this.saveMessage(args);
            }
        }

        this.saveMessage = (args) => {
            var query = {
                "sql": "insert into message (action, tableno, itemCode, dish, token) VALUES (@action, @tableno, @itemCode, @dish, @token)",
                "args": [
                    { "c": "action", "t": TYPES.VarChar, "v": args.action },
                    { "c": "tableno", "t": TYPES.VarChar, "v": args.tableno },
                    { "c": "itemCode", "t": TYPES.VarChar, "v": args.itemCode },
                    { "c": "dish", "t": TYPES.VarChar, "v": args.dish },
                    { "c": "token", "t": TYPES.VarChar, "v": args.token }
                ]
            };
            this.DM.executeStatement(query, (err, data) => {
                if (err) return;
                console.log("addMessage Check", data);
                this.broadcastDiceMessage();
            });
            // var msg = new Message(args);
            // msg.save(function (error, data) {
            //     if (error) {
            //         console.log("addMessage error", error);
            //     }
            //     else {
            //         //console.log("addMessage", data);
            //         var _o = { msg: data };
            //         //mqttClient.publish("wo/dice/addMessageResponse/", JSON.stringify(_o), { qos: 2 });
            //         broadcastDiceMessage();
            //     }
            // });
        }

        this.updateDiceMessage = (args) => {
            args = JSON.parse(args);
            // var msg = new Message(JSON.parse(args));
            if (args.staff == undefined) {
                console.log("updateMessage Error", "[staff] undefined");
                return;
            }
            if (args.isDelete == undefined) {
                console.log("updateMessage Error", "[isDelete] undefined");
                return;
            }
            var query = {
                "sql": "select * from message where Id = @id",
                "args": [
                    { "c": "id", "t": TYPES.Int, "v": args.id }
                ]
            };
            this.DM.executeStatement(query, (err, data) => {
                if (err) {
                    console.log("updateMessage error", err);
                }
                _msg = data[0];
                console.log('_msg', _msg);
                var that = this;
                var update = function (callback) {
                    // if (!_msg.staff.length) _msg.staff = msg.staff;
                    // _msg.isDelete = msg.isDelete;
                    // _msg.markModified('staff isDelete');
                    // _msg.save(function () {
                    //     if (callback != null) callback(null, 1);
                    // });
                    // console.log("update msg", _msg);

                    if (!_msg.staff.length) {
                        _msg.staff = args.staff;
                        var query = {
                            "sql": "update message set staff = @staff, isDelete = @isDelete where id = @id",
                            "args": [
                                { "c": "staff", "t": TYPES.VarChar, "v": args.staff },
                                { "c": "isDelete", "t": TYPES.Bit, "v": args.isDelete },
                                { "c": "id", "t": TYPES.Int, "v": args.id }
                            ]
                        };
                    }
                    else {
                        var query = {
                            "sql": "update message set isDelete = @isDelete where id = @id",
                            "args": [
                                { "c": "isDelete", "t": TYPES.Bit, "v": args.isDelete },
                                { "c": "id", "t": TYPES.Int, "v": args.id }
                            ]
                        };
                    }
                    that.DM.executeStatement(query, (err, data) => {
                        console.log('data', data);
                        if (callback != null) callback(null, 1);
                    });
                }
                var broadcast = function (callback) {
                    that.broadcastDiceMessage(callback);
                }
                var updateResponse = function (callback) {
                    var _o = { 'msg': _msg };
                    mqttClient.publish("wo/dice/updateMessageResponse/", JSON.stringify(_o), { qos: 2 });
                    // io.sockets.emit('updateMessageResponse', _o);
                    callback(null, 3);
                }
                console.log('_msg', _msg.staff);
                if (!_msg.staff.length)
                    async.series([update, broadcast, updateResponse]);
                else
                    update();
            });
            // Message.findOne({ "id": msg.id }, function (err, _msg) {
            //     if (err) {
            //         console.log("updateMessage error", err);
            //     }
            //     var update = function (callback) {
            //         if (!_msg.staff.length) _msg.staff = msg.staff;
            //         _msg.isDelete = msg.isDelete;
            //         _msg.markModified('staff isDelete');
            //         _msg.save(function () {
            //             if (callback != null) callback(null, 1);
            //         });
            //         console.log("update msg", _msg);
            //     }
            //     var broadcast = function (callback) {
            //         broadcastDiceMessage(callback);
            //     }
            //     var updateResponse = function (callback) {
            //         var _o = { 'msg': _msg };
            //         mqttClient.publish("wo/dice/updateMessageResponse/", JSON.stringify(_o), { qos: 2 });
            //         io.sockets.emit('updateMessageResponse', _o);
            //         callback(null, 3);
            //     }
            //     if (!_msg.staff.length)
            //         async.series([update, broadcast, updateResponse]);
            //     else
            //         update();
            // });
        }

        this.broadcastDiceMessage = function(callback) {
            var today = getToday(), tommorow = getTommorow();
            var query = {
                "sql": "select * from message where isDelete = @isDelete and date between @today and @tommorow order by date desc",
                "args": [
                    { "c": "isDelete", "t": TYPES.Bit, "v": false },
                    { "c": "today", "t": TYPES.DateTime, "v": today },
                    { "c": "tommorow", "t": TYPES.DateTime, "v": tommorow }
                ]
            };
            this.DM.executeStatement(query, (err, _msg) => {
                if (err) {
                    console.log("broadcastDiceMessage error", err);
                }
                console.log('broadcastDiceMessage', _msg);
                var _o = { 'msg': _msg };
                mqttClient.publish("wo/dice/broadcastMessageResponse/", JSON.stringify(_o), { qos: 2 });
                // io.sockets.emit('broadcastMessageResponse', _o);
                if (callback != null) callback();
            });
            // Message.find({ "date": { "$gte": today, "$lt": tommorow }, isDelete: false }).sort({date: 1})
            //     .exec(function (err, _msg) {
            //         if (err) return;
            //         //console.log("broadcastDiceMessageResponse", _msg);
            //         var _o = { 'msg': _msg };
            //         mqttClient.publish("wo/dice/broadcastMessageResponse/", JSON.stringify(_o), { qos: 2 });
            //         io.sockets.emit('broadcastMessageResponse', _o);
            //         if (callback != null) callback();
            //     });
        }

        this.clockIn = (args, callback) => {
            args.type = "in";
            this.Attendance(args, callback);
        }

        this.clockOut = (args, callback) => {
            args.type = "out";
            this.Attendance(args, callback);
        }

        this.Attendance = (args, callback) => {
            var query = { "sql": "select * from staff where username = @username", "args": [{ "c": "username", "t": TYPES.VarChar, "v": args.username }] }
            this.DM.executeStatement(query, (err, result) => {
                if (result.length == 0) {
                    callback({ result: "ERROR", msg: "noUser" });
                    return;
                }
                var currentTime = new Date(),
                    query = {
                    "sql": "insert into staffAtt (staffUsername, staffName, shopId, datetime, type) VALUES (@staffUsername, (select name from staff where username = @staffUsername), @shopId, @datetime, @type); select * from staffAtt where attId = @@IDENTITY",
                    "args": [
                        { "c": "staffUsername", "t": TYPES.VarChar, "v": args.username },
                        { "c": "shopId", "t": TYPES.Int, "v": 1 },
                        { "c": "datetime", "t": TYPES.DateTime, "v": currentTime },
                        { "c": "type", "t": TYPES.VarChar, "v": args.type }
                    ]
                };
                this.DM.executeStatement(query, (err, _msg) => {
                    if (err) {
                        console.log("Attendance error", err);
                        return;
                    }
                    console.log('Attendance', _msg);
                    var att = _msg[0];
                    _o = {
                        "time": currentTime,
                        "staffName": att.staffName,
                        "type": args.type
                    };
                    callback(_o);
                });
            });
        }

        this.CashControl = (args, callback) => {
            var currentTime = new Date(),
                query = {
                "sql": "insert into cashControl (cash, type, remark, datetime, staffId) VALUES (@cash, @type, @remark, @datetime, @staffId);",
                "args": [
                    { "c": "cash", "t": TYPES.Decimal, "v": parseFloat(args.cash), "o": { "scale": 2 } },
                    { "c": "type", "t": TYPES.VarChar, "v": args.type },
                    { "c": "remark", "t": TYPES.NVarChar, "v": args.remark },
                    { "c": "datetime", "t": TYPES.DateTime, "v": currentTime },
                    { "c": "staffId", "t": TYPES.VarChar, "v": args.staffId }
                ]
            };
            this.DM.executeStatement(query, (err, _msg) => {
                if (err) {
                    console.log("CashControl error", err);
                    return;
                }
                console.log('CashControl', _msg);
                this.PrepareDailyCashReport(args, callback);
            });
        }

        this.PrepareDailyCashReport = (args, callback) => {
            var date = moment().format("DD/MM/YYYY"),
                today = getToday(),
                tommorow = getTommorow(),
                query = {
                    "sql":"select * from cashControl where datetime between @fromDate and @toDate",
                    "args": [
                        { "c": "fromDate", "t": TYPES.DateTime, "v": today },
                        { "c": "toDate", "t": TYPES.DateTime, "v": tommorow }
                    ]
                }
            this.DM.executeStatement(query, (err, result) => {
                if (err) {
                    console.log("CashControl error", err);
                    return;
                }
                var obj = { GROSS_TOTAL_AMOUNT: 0, TOTAL_CASH_SALES_AMOUNT: 0, TOTAL_AMOUNT: 0, CASH_RECORDS: [], REPORT_DATE:'' };

                console.log('PrepareDailyCashReport', result);
                obj.REPORT_DATE = date;
                obj.TOTAL_AMOUNT = 0;
                obj.CASH_SALES_AMOUNT = 0;
                obj.GROSS_TOTAL_AMOUNT = 0;

                result.forEach(function(record) {
                    if (record.type === "pettycash") {
                        record.cash *= -1;
                    }
                    obj.CASH_RECORDS.push(record);
                    obj.GROSS_TOTAL_AMOUNT += record.cash;
                });

                callback(obj);
            });
        }
}

/**
 * Number.prototype.format(n, x)
 *
 * @param integer n: length of decimal
 * @param integer x: length of sections
 */
Number.prototype.format = function (n, x) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
};

function extend(from, to)
{
    if (from == null || typeof from != "object") return from;
    if (from.constructor != Object && from.constructor != Array) return from;
    if (from.constructor == Date || from.constructor == RegExp || from.constructor == Function ||
        from.constructor == String || from.constructor == Number || from.constructor == Boolean)
        return new from.constructor(from);

    to = to || new from.constructor();

    for (var name in from)
    {
        to[name] = typeof to[name] == "undefined" ? extend(from[name], null) : to[name];
    }

    return to;
}

function pad(n, width, z) {
    z = z || '0';
    n = n + '';
    return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

function countDoubleByteChar(str) {
    var c = 0
    for (var i = 0, n = str.length; i < n; i++) {
        if (str.charCodeAt(i) > 255) { c++ }
    }
    return c;
}

function init() {
    RAS = new RASManager(config.sql)
    RAS.GenerateMenu();
    RAS.LoadTableSchema(null, (err, result) => {
        io.sockets.emit('refreshTable', JSON.stringify(result));
    })
    mqttClient = mqtt.connect('mqtt://127.0.0.1:1884');
}

init();

process.on('uncaughtException', function (err) {
    console.log('uncaughtException:' + err);
    console.log('uncaughtException:' + err.stack);
});


// dinning table status
/*
0 - can use
1 - ordering
2 - table have order
3 - order print

*/



//var connection = new sql.Connection(config.sql, function (err) {
//    console.log(err);
//    var request = new sql.Request(connection); // or: var request = connection.request();
//    request.query('select * from item', function (err, recordset) {
//        console.log(err);
//        // ... error checks
//        console.dir(recordset);
//    });
//});
