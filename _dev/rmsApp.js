(function () {
    var app = angular.module('rms', ['ngTouch', 'ngMessages', 'ngMaterial', 'ui.bootstrap', 'rms.controllers', 'rms.directives']).run(function (MainService) {
        MainService.promise;
    });
    app.config(function ($mdThemingProvider, $locationProvider, $provide) {
        $locationProvider.html5Mode({enabled : true, requireBase: false, rewriteLinks: true});
        $provide.constant('INFO', {
            'socketPort': 5000, 'lang': '003', 'currency': '$',
            "cancelRemark": ["沽清", "系統測試", "漏單", "食物問題", "食物失蹤", "客人要求(未做)", "客人要求(太遲)", "客人要求(轉食品)", "客人要求(不合口味)", "客人否認(未做)", "客人否認(已做)", "錯單(未做)", "錯單(已做)", "重單(未做)", "重單(已做) 客人失蹤", "其它"]
        });
        $provide.constant('uiLanguage', {
          "001" : {

            done : "Done",
            password_fail: "Your password or username is not correct.",
            sceneLoaded: "Table Scene is loaded.",
            sceneLoadError: "Cannot connect, demo table is loaded instead.",
            jsonLoadError: "Please check if Table JSON is loaded successfully."
          },
          "003" : {
            member_no : "會員號碼",
            member_phone : "會員電話",
            search_member : "查詢會員",
            done : "完成",
            cancel_back : "取消/返回",
            login : "登入",
            logout : "登出",
            login_name : "用戶名稱",
            Login : "用戶名稱",
            password : "密碼",
            Password : "密碼",
            password_fail: "密碼 或 用戶名稱錯誤",
            select_all: "全選",
            cancel_select: "取消選擇",
            clear_message: "清除訊息",
            affirmative: "收到",
            loading: "載入中",
            sceneLoaded: "場景已載入",
            sceneLoadError: "未能連接，系統載示範場景。",
            jsonLoadError: "請檢查 JSON 是否已成功載入或檢查網絡連線。",
            closeMsg: "關閉訊息",
            mode: "模式",
            order: "落單",
            printorder: "做檯紙",
            transfer: "轉檯",
            printbill: "印單",
            divide: "分單"
          }
        });
        $provide.constant('keyMap', {
            escape: 27,
            enter: 13
        });
    });

    app.filter('startFrom', function () {
        return function (input, start) {
            start = +start; //parse to int
            if (input == undefined) return 0;
            return input.slice(start);
        }
    });

    app.factory('SocketFactory', function ($rootScope, $timeout, INFO) {
        var socketConnection = 'http://' + location.hostname + ':' + INFO.socketPort;
        var socket = io.connect(socketConnection);
        io.Manager(socketConnection, { reconnect: true });
        var asyncAngularify = function (socket, callback) {
            return callback ? function () {
                var args = arguments;
                $timeout(function () {
                    callback.apply(socket, args);
                }, 0);
            } : angular.noop;
        };
        return {
            on: function (eventName, callback) {
                socket.on(eventName, function () {
                    var args = arguments;
                    $rootScope.$apply(function () {
                        callback.apply(socket, args);
                    });
                });
            },
            emit: function (eventName, data, callback) {
                var lastIndex = arguments.length - 1;
                var callback = arguments[lastIndex];
                if (typeof callback == 'function') {
                    callback = asyncAngularify(socket, callback);
                    arguments[lastIndex] = callback;
                }
                return socket.emit.apply(socket, arguments);
            }
        };
    });

    app.factory('MainService', function (SocketFactory, INFO, $http, $q, $mdDialog, $timeout) {
        SocketFactory.on('refreshItemQty', function (data) {
            console.log('refreshItemQty done');
            console.log(data);
            obj.stock.updateItem(JSON.parse(data));
            //initTableScene();
            // showMessage("#status-message", scope.RMSTable.messages.table_refresh, "", "warn", 30000);
        });

        //SocketFactory.emit('loadStaff', function (r) {
        //    console.log(r);
        //    console.log(JSON.stringify(r));
        //});

        var charArray = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".split('');
        var obj = {};
        obj.schema = {};
        obj.schema.mode = {};
        obj.schema.mode.floorPlan = 0;
        obj.schema.mode.order = 1;
        obj.schema.modeOrder = {};
        obj.schema.modeOrder.normal = 0;
        obj.schema.modeOrder.detail = 1;
        obj.schema.modeOrder.foodControl = 2;
        obj.schema.modeOrder.transfer = 3;
        obj.schema.modeItem = {}
        obj.schema.modeItem.normal = 0;
        obj.schema.modeItem.option = 1;
        obj.schema.modeItem.cancel = 2;
        obj.schema.modeItem.modifier = 3;
        obj.schema.modeItemDetail = {};
        obj.schema.modeItemDetail.code = 0;
        obj.schema.modeItemDetail.printer = 1;
        obj.schema.modeFloorPlan = {};
        obj.schema.modeFloorPlan.order = 0;
        obj.schema.modeFloorPlan.printOrder = 1;
        obj.schema.modeFloorPlan.changeTable = 2;
        obj.schema.modeFloorPlan.printBill = 3;
        obj.schema.modeFloorPlan.transfer = 4;

        obj.schema.member = {
            name: function () { return this.surname +' '+ this.givenname },
            dob: function () { return this.dob_yyyy + '/'+  this.dob_mm + '/' + this.dob_dd },
        }

        obj.schema.baseItem = {
            init: function () {
                if (this.option != undefined) {
                    $.each(this.option, function (optionIndex, option) {
                        $.extend(true, option, obj.schema.baseItem);
                        $.each(option.items, function (optionItemIndex, optionItem) {
                            $.extend(true, optionItem, obj.schema.baseItem, { qty: 0, _qty: 0 });
                        })
                    });
                }
                if (this.subitem != undefined) {
                    $.each(this.subitem, function (idx, subitem) {
                        $.extend(true, subitem, obj.schema.baseItem);
                    });
                }
            },
            getName: function () {
                if (this.name != undefined) {
                    return obj.fn.shorten(this.name[INFO.lang], 15);
                }
                else {
                    //console.log(2);
                    //console.log(this.desc1);
                    return obj.fn.shorten(this.desc1, 15);
                }
            },
            getQty: function () {
                return parseInt(this.qty);
            },
            getQtyWithVoid: function () {
                return parseInt(this.qty) + parseInt(this.voidQty);
            },
            getUnitPrice: function () {
                if (this.unitprice != undefined) {
                    return (this.unitprice / 100);
                }
                else if (this.unitPrice != undefined) {
                    return parseFloat(this.unitPrice);
                }
            },
            getTotalPrice: function () {
                //return 10;
                var optionPrice = 0
                if (this.option != undefined && this.option.length > 0) {
                    $.each(this.option, function (optionIndex, option) {
                        $.each(option.items, function (itemIndex, item) {
                            if (item.qty != undefined) {
                                //console.log("qty:" + item.getQty);
                                //console.log("qty:" + item.getQty());
                                //console.log("getUnitPrice:" + item.getUnitPrice());
                                if (item.getQty() > 0) {
                                    optionPrice += item.getUnitPrice() * item.getQty();
                                }
                            }
                        })
                    });
                }
                return this.getUnitPrice() * this.getQty() + optionPrice;
                //return this.getUnitPrice() ;
            },
            editItem: function ($event) {
                console.log(obj.modeMultiSelectItem);
                if (obj.modeMultiSelectItem) {
                    this.isEdit = !this.isEdit;
                } else {
                    obj.Cart.resetEditItem();
                    this.isEdit = true;
                }
                //obj.toggleActive($event, function () { });
                if ($event) {
                    $event.stopPropagation();
                }
            },
            deleteItem: function ($event, index) {
                this.editItem();
                obj.modeItem = obj.schema.modeItem.cancel;
                obj.modeMultiSelectItem = true;
                //obj.toggleActive($event, function () { });
                $event.stopPropagation();
            },
            qty: 1,
            voidQty: 0,
            voidIndex: -1,
            voidRemark: '',
            isDelete: false,
            isEdit: false,
            modifier: []
        }
        obj.schema.baseCartItem = $.extend(true, {}, obj.schema.baseItem, {
            deleteItem: function ($event, index) {
                obj.Cart.cartList.splice(index, 1);
                if ($event) {
                    $event.stopPropagation();
                }
            }, editItem: function ($event, focus) {
                
                if (obj.modeMultiSelectItem) {
                    this.isEdit = !this.isEdit;
                } else {
                    obj.Cart.resetEditItem();
                    this.isEdit = true;
                }
                //obj.toggleActive($event, function () { });
                if ($event) {
                    $event.stopPropagation();
                }
                if (focus == undefined) {
                    obj.modeItem = obj.schema.modeItem.modifier;
                }
            }
        });

        obj.splitListUpdatePrice = function () {
            var price = 0;
            if (obj.Cart.allSplitList[obj.Cart.splitListLIndex].item) {
                $.each(obj.Cart.allSplitList[obj.Cart.splitListLIndex].item, function (i, v) {
                    //if (v.getQty() > 0 && !v.isDelete)
                    price += v.getTotalPrice();
                });
                obj.Cart.splitListLPrice = price;
                var scObj = obj.calcServiceCharge(price);
                obj.Cart.splitListLServiceCharge = scObj.serviceCharge;
                obj.Cart.splitListLRemainings = scObj.remainings;
                obj.Cart.splitListLTotalPrice = price + obj.Cart.splitListLServiceCharge;
            }
            price = 0;
            if (obj.Cart.allSplitList[obj.Cart.splitListRIndex].item) {
                $.each(obj.Cart.allSplitList[obj.Cart.splitListRIndex].item, function (i, v) {
                    //if (v.getQty() > 0 && !v.isDelete)
                    price += v.getTotalPrice();
                });
                obj.Cart.splitListRPrice = price;

                var scObj = obj.calcServiceCharge(price);
                obj.Cart.splitListRServiceCharge = scObj.serviceCharge;
                obj.Cart.splitListRRemainings = scObj.remainings;
                obj.Cart.splitListRTotalPrice = price + obj.Cart.splitListRServiceCharge;
            }
        }

        obj.splitListClick = function (currentIdx,qty) {
            console.log(currentIdx);
            var checkList = obj.Cart.splitListLIndex == currentIdx ? obj.Cart.allSplitList[obj.Cart.splitListRIndex].item : obj.Cart.allSplitList[obj.Cart.splitListLIndex].item;
            var currentList = obj.Cart.allSplitList[currentIdx].item;
            var swapList = [];
            var checkFunction = function () {
                $.each(checkList, function (idx, item) {
                    if (item.isEdit) {
                        var cloneItem = {};
                        checkList[idx].isEdit = false;
                        angular.copy(checkList[idx], cloneItem);
                        swapList.push(cloneItem);
                        if(qty == undefined){
                            checkList.splice(idx, 1)
                        } else if (qty >= item.qty) {
                            checkList.splice(idx, 1)
                        } else {
                            item.qty -= qty;
                            swapList[swapList.length - 1].qty = qty;

                            var cloneVoidItem = {};
                            angular.copy(checkList[idx], cloneVoidItem);
                            cloneVoidItem.qty = -1 * qty;
                            cloneVoidItem.voidIndex = item.index;
                            cloneVoidItem.voidRemark = "Item transfer";
                            checkList.push(cloneVoidItem);
                        }
                        checkFunction();
                        return false;
                    }
                });
            }
            checkFunction();
            if (swapList.length != 0)
            {
                for (var i = 0; i < swapList.length; i++) {
                    swapList[i].isEdit = false;
                    currentList.push(swapList[i]);
                }

                currentList.sort(function (a, b) {
                    return  parseInt(a.index) > parseInt( b.index);
                });
                //obj.Cart.splitListR.concat(swapList)
                console.log(obj.Cart.splitListR.length);
                obj.splitListUpdatePrice();
                return true;
            } else {
                obj.Cart.currentSelectedSplitIndex = currentIdx;
                return false;
            }
        }

        obj.schema.baseSplitItem = $.extend(true, {}, obj.schema.baseItem, {
            editItem: function ($event, currentIdx) {
                if (obj.splitListClick(currentIdx)) {

                } else {
                    console.log(obj.modeMultiSelectItem);
                    if (obj.modeMultiSelectItem) {
                        this.isEdit = !this.isEdit;
                    } else {
                        obj.Cart.resetEditItem();
                        this.isEdit = true;
                    }
                };
                if ($event) {
                    $event.stopPropagation();
                }
            }
        });

        obj.modeItem = obj.schema.modeItem.normal;
        
        obj.mode = obj.schema.mode.floorPlan;
        //obj.mode = obj.schema.mode.order;
        obj.modeOrder = obj.schema.modeOrder.normal;
        obj.modeFloorPlan = obj.schema.modeFloorPlan.order;
        obj.modeMultiSelectItem = false;
        obj.itemsPerPage = 18;
        obj.catsPerPage = 8;
        obj.cancelOptionPerPage = 13;
        obj.cancelOptionTotalItems = INFO.cancelRemark.length;
        obj.cancelOptionCurrentPage = 1;
        obj.modeItemDetail = obj.schema.modeItemDetail.code;

        obj.modeToggleItemDetail = function (element) {
            obj.modeItemDetail = obj.modeItemDetail == obj.schema.modeItemDetail.code ? obj.schema.modeItemDetail.printer : obj.schema.modeItemDetail.code;
        }
        
        obj.resizeOrderListHeight = function( element ) {
            var target = element ? element : "order-scene .order-list-wrapper";
            // total height of display area (exclude top and status bar)
            var maxHeight = angular.element(".menu-area").height();

            // total height of account summary
            var totalHeight = 0;
            angular.element(".account-summary .row").each(
             function(index, ele){
                 totalHeight = totalHeight + angular.element(ele).height();
                 console.log(angular.element(ele).height());
             }
            );
            // order list maximum height
            var orderListMaxHeight = maxHeight - totalHeight;
            angular.element(element).css("max-height", orderListMaxHeight);
        }

        obj.modeToggleFoodControl = function () {
            obj.modeOrder = obj.modeOrder == obj.schema.modeOrder.normal ? obj.schema.modeOrder.foodControl : obj.schema.modeOrder.normal
        }

        obj.modeToggleOrder = function () {
            obj.modeOrder = obj.modeOrder == obj.schema.modeOrder.normal ? obj.schema.modeOrder.detail : obj.schema.modeOrder.normal
        }

        // for legacy RMSTable function compatibility purpose, deprecated, please merge into better code later
        obj.RMSTable = {
            editMode : "order",
            changeMode : function( mode ) {
                this.editMode = mode;
                showMessage('#status-message', mode + ' Mode', '', 'warn', 'stick');
            }
        };
        obj.UserManager = {};
        obj.UserManager.LoginRedirectToTable = '';
        obj.UserManager.staffName = '';
        obj.UserManager.staffCode = '';
        obj.UserManager.isLogin = false;
        obj.UserManager.logout = function (username, password) {
            obj.UserManager.isLogin = false;
            obj.UserManager.staffName = '';
        }
        obj.UserManager.login = function (username, password) {
            var deferred = $q.defer();
            SocketFactory.emit('login', { u: username, p: password }, function (r) {
                r = JSON.parse(r);
                if (r.success) {
                    obj.UserManager.staffName = r.staffName;
                    obj.UserManager.staffCode = username;
                    obj.UserManager.isLogin = true;
                }
                deferred.resolve(r);
            });
            return deferred.promise;
        }
        obj.fn = {};
        obj.fn.shorten = function (text) {
            return text;
            if (text)
                return shortenText(text, 15);
            else
                return "";
        }
        obj.clickTable = function (tableNo, callback) {
            switch (this.modeFloorPlan){
                case this.schema.modeFloorPlan.order:
                    this.assignTableNo(tableNo, function (cb) {
                        //callback(cb);
                        if (angular.element('md-dialog[aria-label="Bill Option"]').scope() == undefined) {
                            $mdDialog.show({
                                controller: function ($scope) {
                                    $scope.order = cb;
                                    $scope.assignTableNo = function (t) {
                                        obj.assignTableNo(t);
                                        $scope.closeDialog();
                                    }
                                    $scope.closeDialog = function () {
                                        $mdDialog.hide();
                                    }
                                },
                                templateUrl: 'tmpl/billOption.html'
                            })
                        } else {
                            $mdDialog.hide();
                        }
                    });
                    break;
                case this.schema.modeFloorPlan.transfer:
                    this.splitTableNo(tableNo);
                    break;
                case this.schema.modeFloorPlan.printOrder :
                    this.printOrder(tableNo,'order');
                    break;
                case this.schema.modeFloorPlan.printBill:
                    this.printOrder(tableNo,'bill');
                    break;

            }
        };

        obj.switchModeItem = function (modeChange) {

        }

        obj.switchModeFloorPlan = function (modeChange) {
            this.modeFloorPlan = modeChange;
            console.log("%%%"+this.modeFloorPlan);
        }
        obj.printOrder = function (tableNo,type) {
            SocketFactory.emit('print', { "tableNo": tableNo,"action":type });
        }
        obj.Cart = {
            cartList: [], orderList: [], splitListRPrice: 0, splitListRServiceCharge: 0, splitListRTotalPrice: 0, splitListRNoofpeople: 0, splitListLPrice: 0, splitListLServiceCharge: 0, splitListLTotalPrice: 0, splitListLNoofpeople: 0, splitListRIndex: 1, splitListLIndex: 0, allSplitList: [], tableNo: '', noOfPeople: '', totalPrice: 0, serviceCharge: 0, price: 0,currentSelectedSplitIndex : 0,member:{},
            currentOption: null, currentOptionIndex: -1, voidItemCount: 'ALL',
            cancelPackage: function () {
                obj.modeItem = obj.schema.modeItem.normal;
                obj.Cart.cartList.pop();
            },
            assignMemberToOrder: function(member){
                console.log(member);
                obj.Cart.member = member;
                $mdDialog.cancel();
            },
            addModifier: function (m) {
                $.each(obj.Cart.cartList, function (idx, item) {
                    if (item.isEdit) {
                        var foundModifierIdx = -1;
                        $.each(item.modifier, function (midx,mitem) {
                            if (mitem.code == m.code) {
                                foundModifierIdx = midx
                            }
                        })
                        if (foundModifierIdx == -1) {
                            item.modifier.push(m);
                        } else {
                            item.modifier.splice(foundModifierIdx, 1);
                        }
                    }
                });
            
            },
            checkItemStock: function (item, qty, prevQty) {
                prevQty == undefined ? 0 : prevQty;
                var totalqty = qty == undefined ? 1 : qty;
                totalqty -= prevQty;
                var isValid = true;
                $.each(obj.Cart.cartList, function (idx, citem) {
                    if (citem.getQty() >= 0) {
                        totalqty += citem.getQty();
                    }
                });
                $.each(obj.stock.list, function (idx, sitem) {
                    if (sitem.code == item.code) {
                        if (sitem.stock <= 0) {
                            $mdDialog.show(
                                $mdDialog.alert()
                                .parent(angular.element(document.body))
                                .title('Alert')
                                .content('Out of stock')
                                .ok('OK')
                            );
                            isValid = false;
                            return;
                        } else if (sitem.stock < totalqty) {
                            $mdDialog.show(
                                $mdDialog.alert()
                                .parent(angular.element(document.body))
                                .title('Alert')
                                .content('Require:' + totalqty + '<br/>Remaining:' + sitem.stock)
                                .ok('OK')
                            );
                            isValid = false;
                            return;
                        }
                    }
                });
                return isValid;

            },
            addItem: function (item) {
                console.log(item);
                var newItem = $.extend(true, {}, item , obj.schema.baseCartItem);
                newItem.init();
                if (obj.modeOrder != obj.schema.modeOrder.foodControl) {
                    if (!obj.Cart.checkItemStock(newItem,1)) {
                        return false;
                    } else {
                        this.cartList.push(newItem);
                        newItem.editItem(null, false);
                        console.log(newItem);
                        if (newItem.option) {
                            obj.modeItem = obj.schema.modeItem.option;
                            this.currentOption = newItem.option;
                            this.currentOptionIndex = -1;
                            this.showNextOption();
                        }
                        $timeout(function () {
                            obj.scrollToBottom();
                        }, 10);
                        return newItem;
                    }
                } else if (obj.modeOrder == obj.schema.modeOrder.foodControl) {
                    var foundItem = null;
                    $.each(obj.stock.list, function (idx, item) {
                        if (item.code == newItem.code) {
                            foundItem = item;
                        }
                    });
                    if (foundItem != null) {
                        foundItem.qty = '';
                        foundItem.editItem();
                        $timeout(function () {
                            obj.scrollToBottom();
                        }, 10);
                        return foundItem;
                    } else {
                        newItem.qty = '';
                        obj.stock.list.push(newItem);
                        newItem.editItem();
                        $timeout(function () {
                            obj.scrollToBottom();
                        }, 10);
                        return newItem;
                    }
                }
            },
            showNextOption : function () {
                    this.currentOptionIndex++;
                    if (this.currentOptionIndex < this.currentOption.length) {
                        this.currentOptionList = this.currentOption[this.currentOptionIndex];
                    } else {
                        obj.modeItem = obj.schema.modeItem.normal;
                    }
            },
            addOption : function (option) {
                console.log(option)
                $.each(this.currentOptionList.items, function (optionItemIndex, optionItem) {
                    optionItem.qty = 0;
                });
                option.qty = 1;
                console.log(obj.Cart.cartList);
                this.showNextOption();
                //console.log(obj.Cart.orderList);
            },
            cancelOrder: function () {
                if (obj.modeOrder == obj.schema.modeOrder.foodControl) {
                    obj.modeOrder = obj.schema.modeOrder.normal;
                } else {
                    obj.mode = obj.schema.mode.floorPlan;
                }
            },
            voidItem: function (voidRemark) {
                var resultItem = {};
                $.each(this.orderList, function (idx, item) {
                    if (item.isEdit && item.getQty() > 0) {
                        var finalCheckQty = item.getQty();
                        $.each(obj.Cart.orderList, function (checkidx, checkItem) {
                            console.log(checkItem.voidIndex);
                            if (checkItem.voidIndex == item.index) {
                                finalCheckQty += checkItem.getQty();
                            }
                        });
                        $.each(obj.Cart.cartList, function (checkidx, checkItem) {
                            if (checkItem.voidIndex == item.index) {
                                finalCheckQty += checkItem.getQty();
                            }
                        });
                        if (finalCheckQty > 0) {
                            var cloneItem = {};
                            angular.copy(item, cloneItem);
                            var _voidqty = obj.Cart.voidItemCount;
                            if (_voidqty == "ALL") {
                                _voidqty = finalCheckQty;
                            } else if (_voidqty > finalCheckQty) {
                                _voidqty = finalCheckQty;
                            }
                            resultItem = obj.Cart.addItem(cloneItem);
                            resultItem.qty = _voidqty * -1
                            resultItem.voidIndex = item.index;
                            resultItem.voidRemark = voidRemark;
                        }
                    }
                });
                obj.modeItem = obj.schema.modeItem.normal;
                obj.modeMultiSelectItem = false;
                $timeout(function () {
                    obj.scrollToBottom();
                }, 10);
            },
            resetEditItem : function () {
                $.each(obj.Cart.orderList, function (i, v) { v.isEdit = false; });
                $.each(obj.Cart.cartList, function (i, v) { v.isEdit = false; });
                $.each(obj.stock.list, function (i, v) { v.isEdit = false; });

            }, changeItemQty: function (qty) {
                $.each(obj.Cart.cartList, function (i, v) {
                    if (v.isEdit) {
                        if (obj.Cart.checkItemStock(v, qty, v.getQty())) {
                            console.log(v);
                            v.qty = qty;
                        } else {
                            return;
                        }
                    }
                });
                obj.modeItem = obj.schema.modeItem.normal;
            }
        };
        obj.scrollToBottom = function (element) {
            console.log('scrollToBottom');
            var target = element ? element : "order-scene .order-list-wrapper";
            var maxScrollable = angular.element(target)[0].scrollHeight - angular.element(target).height();
            angular.element( target ).scrollTop( maxScrollable );
        }

        obj.submitCart = function () {
            if (obj.modeOrder == obj.schema.modeOrder.foodControl) {
                obj.modeOrder = obj.schema.modeOrder.normal;
            } else {
                var order = { item: [] };
                angular.forEach(obj.Cart.cartList, function (item) {
                    order.item.push({
                        code: [item.code], qty: [item.qty], unitPrice: [item.getUnitPrice() * 100], voidIndex: [item.voidIndex], voidRemark: [item.voidRemark], type: ['I'], desc1: [item.getName()]
                    });
                    if (item.option != undefined && item.option.length != 0) {
                        $.each(item.option, function (optionIndex, option) {
                            $.each(option.items, function (optionItemIndex, optionItem) {
                                if (optionItem.qty > 0) {
                                    order.item.push({
                                        desc1: [optionItem.getName()],
                                        code: [optionItem.code],
                                        qty: [optionItem.getQty()],
                                        unitPrice: [optionItem.getUnitPrice() * 100],
                                        voidIndex: [optionItem.voidIndex],
                                        voidRemark: [optionItem.voidRemark],
                                        type: "S"
                                    });
                                }
                            })
                        });
                    }
                    if (item.subitem != undefined && item.subitem.length != 0) {
                        $.each(item.subitem, function (subitemIndex, subitem) {
                            order.item.push({
                                desc1: [subitem.getName()],
                                code: [subitem.code],
                                qty: [subitem.getQty()],
                                unitPrice: [subitem.getUnitPrice() * 100],
                                voidIndex: [subitem.voidIndex],
                                voidRemark: [subitem.voidRemark],
                                type: "1"
                            });
                        });
                    }
                    if (item.modifier.length != 0) {

                        $.each(item.modifier, function (modifierIndex, modifier) {
                            order.item.push({
                                desc1: [modifier.label],
                                code: [modifier.code],
                                qty: [1],
                                unitPrice: [0],
                                voidIndex: [-1],
                                voidRemark: [''],
                                type: "M"
                            });
                        })
                    }
                });
                //if (order.item.length > 0) {
                var postData = {
                    "action": "MDSaveTableOrder",
                    "user": {
                        "$value": obj.UserManager.staffCode
                    },
                    "tableNum": {
                        "$value": obj.Cart.tableNo
                    },
                    "peopleNum": {
                        "$value": obj.Cart.noOfPeople
                    },
                    "itemData": {
                        "json": {
                            "order": order
                        }
                    }
                };
                if (obj.Cart.member != null) {
                    postData.member = {};
                    postData.member.$value = obj.Cart.member;
                }
                console.log(order);
                SocketFactory.emit('saveTableOrder', postData, function (r) {
                    r = JSON.parse(r);
                    console.log(r.result);
                    if (r.result === "OK") {
                        obj.mode = obj.schema.mode.floorPlan;
                    }
                });


                //} else {
                //    obj.mode = obj.schema.mode.floorPlan;
                //}
            }
        };

        obj.submitSplitTable = function () {
            var orders = [];
            $.each(obj.Cart.allSplitList, function (idx,list) {
                var order = { 'nop': obj.Cart.allSplitList[idx].nop, 'item': [] };
                $.each(obj.Cart.allSplitList[idx].item, function (index, item) {
                    console.log(item);
                    order.item.push({
                        code: [item.code], qty: [item.qty], unitPrice: [item.getUnitPrice() * 100], voidIndex: [item.voidIndex], voidRemark: [item.voidRemark], type: ['I'], desc1: [item.getName()],index:[item.index]
                    });
                    if (item.option != undefined && item.option.length != 0) {
                        $.each(item.option, function (optionIndex, option) {
                            $.each(option.items, function (optionItemIndex, optionItem) {
                                if (optionItem.qty > 0) {
                                    order.item.push({
                                        desc1: [optionItem.getName()],
                                        code: [optionItem.code],
                                        qty: [optionItem.getQty()],
                                        unitPrice: [optionItem.getUnitPrice() * 100],
                                        voidIndex: [optionItem.voidIndex],
                                        voidRemark: [optionItem.voidRemark],
                                        type: "S"
                                    });
                                }
                            })
                        });
                    }
                    if (item.modifier.length != 0) {

                        $.each(item.modifier, function (modifierIndex, modifier) {
                            order.item.push({
                                desc1: [modifier.label],
                                code: [modifier.code],
                                qty: [1],
                                unitPrice: [0],
                                voidIndex: [-1],
                                voidRemark: [''],
                                type: "M"
                            });
                        })
                    }
                });
                orders.push(order);
            });
            //if (order.item.length > 0) {
            var postData = {
                "action": "MDSaveTableOrder",
                "user": {
                    "$value": obj.UserManager.staffName
                },
                "tableNum": {
                    "$value": obj.Cart.tableNo
                },
                "peopleNum": {
                    "$value": obj.Cart.noOfPeople
                },
                "itemData": {
                    "json": {
                        "order": orders
                    }
                }
            };
            SocketFactory.emit('splitTableOrder', postData, function (r) {
                r = JSON.parse(r);
                console.log(r.result);
                if (r.result === "OK") {
                    obj.mode = obj.schema.mode.floorPlan;
                }
            });
        }

        obj.changeSplitList = function (index) {
            if (!(obj.Cart.currentSelectedSplitIndex === index)) {
                var targetIndex = obj.Cart.currentSelectedSplitIndex == obj.Cart.splitListLIndex ? obj.Cart.splitListRIndex : obj.Cart.splitListLIndex;
                if (obj.Cart.currentSelectedSplitIndex == obj.Cart.splitListLIndex) {
                    obj.Cart.splitListRIndex = index;
                    obj.Cart.splitListRNoofpeople = obj.Cart.allSplitList[index].nop;
                } else {
                    obj.Cart.splitListLIndex = index;
                    obj.Cart.splitListLNoofpeople = obj.Cart.allSplitList[index].nop;
                }
            }

        }

        obj.calcServiceCharge = function (price) {
            var r = ((price / 10) % 1).toFixed(1);
            if (r > 0.4) {
                r = (1 - r).toFixed(1);
            } else if (r != 0) {
                r = '-' + r;
            } 
            return { serviceCharge: Math.round(price / 10), remainings: r };
        }

        obj.removeSplitList = function (ev) {
            if (obj.Cart.allSplitList.length < 3) {
                $mdDialog.show(
                 $mdDialog.alert()
                   .parent(angular.element(document.body))
                   .title('Alert')
                   .content('At least 2 groups')
                   .ok('OK')
               );
            } else {
                var lastgroup = obj.Cart.allSplitList[obj.Cart.allSplitList.length - 1];
                if (lastgroup.item.length != 0) {
                    var confirm = $mdDialog.confirm()
                   .parent(angular.element(document.body))
                   .title('Alert')
                   .content('Are you sure to delete the groups?')
                   .ok('Yes')
                   .cancel('No')
                   .targetEvent(ev);
                    $mdDialog.show(confirm).then(function () {
                        //alert('You decided to get rid of your debt.');
                        //if()

                    }, function () {
                        //alert('You decided to keep your debt.');
                    });
                } else {
                    if (obj.Cart.splitListLIndex == obj.Cart.allSplitList.length - 1) {
                        if (obj.Cart.splitListRIndex == obj.Cart.allSplitList.length - 2) {
                            obj.Cart.splitListLIndex = obj.Cart.allSplitList.length - 3;
                        } else {
                            obj.Cart.splitListLIndex = obj.Cart.allSplitList.length - 2;
                        }
                    } else if (obj.Cart.splitListRIndex == obj.Cart.allSplitList.length - 1) {
                        if (obj.Cart.splitListLIndex == obj.Cart.allSplitList.length - 2) {
                            obj.Cart.splitListRIndex = obj.Cart.allSplitList.length - 3;
                        } else {
                            obj.Cart.splitListRIndex = obj.Cart.allSplitList.length - 2;
                        }
                    }
                    obj.Cart.allSplitList.pop();
                }
            }
        }

        obj.addSplitList = function () {
            var targetIndex = obj.Cart.currentSelectedSplitIndex == obj.Cart.splitListLIndex ? obj.Cart.splitListRIndex : obj.Cart.splitListLIndex;
            
            var lasttableno = obj.Cart.allSplitList[obj.Cart.allSplitList.length - 1].name;
            lasttableno = lasttableno.slice(0,-1);
            lasttableno += charArray[obj.Cart.allSplitList.length];

            obj.Cart.allSplitList[obj.Cart.allSplitList.length] = { 'name': lasttableno, 'nop': 1, 'item': [], 'onop': -1 };

            for (var i = 0; i < obj.Cart.allSplitList.length; i++) {
                if (obj.Cart.allSplitList[i].nop > 1) {
                    obj.Cart.allSplitList[i].nop = obj.Cart.allSplitList[i].nop - 1;
                    if (i == obj.Cart.splitListLIndex) {
                        obj.Cart.splitListLNoofpeople = obj.Cart.allSplitList[i].nop;
                    } else if (i == obj.Cart.splitListLIndex) {
                        obj.Cart.splitListRNoofpeople = obj.Cart.allSplitList[i].nop;
                    }
                    break;
                }
            }
            
            if (obj.Cart.currentSelectedSplitIndex == obj.Cart.splitListLIndex) {
                obj.Cart.splitListRIndex = obj.Cart.allSplitList.length - 1;
                obj.Cart.splitListRNoofpeople = 1;
            } else {
                obj.Cart.splitListLIndex = obj.Cart.allSplitList.length - 1;
                obj.Cart.splitListLNoofpeople = 1;
            }
        }

        obj.splitTableNo = function (tableNo) {
            if (!obj.UserManager.isLogin) {
                obj.UserManager.LoginRedirectToTable = tableNo;
                if (angular.element('md-dialog[aria-label="Login"]').scope() == undefined) {
                    $mdDialog.show({
                        templateUrl: 'tmpl/login.html'
                    })
                } else {
                    $mdDialog.hide();
                }
            } else {
                SocketFactory.emit('adminLoadTableOrder', { 'tableNum': { '$value': tableNo } }, function (r) {
                    console.log('adminLoadTableOrder r' + r)
                    r = JSON.parse(r);
                    console.log(r)
                    if (r.result === "OK") {
                        if (r.order[0].header.orderId == '')
                        {
                            $mdDialog.show(
                             $mdDialog.alert()
                               .parent(angular.element(document.body))
                               .title('Alert')
                               .content('There is no order')
                               .ok('OK')
                           );
                        } else {
                            obj.Cart.orderList = [];
                            obj.Cart.splitListL = { 'name': '', 'nop': 0, 'item': [] };
                            obj.Cart.splitListR = { 'name': '', 'nop': 0, 'item': [] };
                            obj.Cart.splitListLIndex = 0;
                            obj.Cart.splitListRIndex = 1;
                            obj.Cart.currentSelectedSplitIndex = obj.Cart.splitListLIndex;
                            obj.Cart.allSplitList = [];
                            obj.Cart.allSplitList[0] = { 'name': tableNo + '_' + charArray[0], 'nop': 0, 'item': [], 'onop': -1 };
                            obj.Cart.allSplitList[1] = { 'name': tableNo + '_' + charArray[1], 'nop': 1, 'item': [], 'onop': -1 };
                            obj.mode = obj.schema.mode.order;
                            obj.modeOrder = obj.schema.modeOrder.transfer;
                            obj.modeMultiSelectItem = true;
                            obj.Cart.tableNo = tableNo;
                            for (var i = 0; i < r.order.length; i++) {
                                var order = r.order[i];
                                var itemlist = obj.Cart.allSplitList[i].item;
                                obj.Cart.allSplitList[i].nop = order.header.peopleNum;
                                obj.Cart.allSplitList[i].onop = order.header.peopleNum;
                                if (i == 0) {
                                    obj.Cart.splitListLNoofpeople = order.header.peopleNum;
                                } else if (i == 1) {
                                    obj.Cart.splitListRNoofpeople = order.header.peopleNum;
                                }
                                angular.forEach(order.item, function (v, k) {
                                    console.log(v);
                                    var item = $.extend(true, {}, obj.schema.baseSplitItem, v);
                                    if (item.type === "I") {
                                        item.init();
                                        if (v.voidIndex != undefined) {
                                            if (v.voidIndex != -1) {
                                                list[v.voidIndex].voidQty = v.qty;
                                            }
                                            item.voidRemark = v.voidRemark;
                                        }
                                        itemlist.push(item);
                                    } else if (item.type === "S") {
                                        if (itemlist[itemlist.length - 1].option == undefined)
                                            itemlist[itemlist.length - 1].option = [{ items: [] }];
                                        //obj.Cart.splitListL[obj.Cart.splitListL.length - 1].option[0].items.push(item);
                                        //obj.Cart.allSplitList[0] = [obj.Cart.allSplitList[0][obj.Cart.allSplitList[0].length - 1].option[0].items.push(item)];
                                        itemlist[itemlist.length - 1].option[0].items.push(item);
                                    } else if (item.type === "M") {
                                        var m = obj.food.modifier.filter(function (matchItem) {
                                            return matchItem.code == item.code;
                                        });
                                        itemlist[itemlist.length - 1].push(m[0]);
                                    }
                                });
                            }
                        }
                        obj.splitListUpdatePrice();
                    }
                });
            }
        };

        obj.assignTableNo = function (tableNo,callback) {
            //$(angular.element.find('.orderPanelCtrl')).removeClass('ng-hide');
            obj .removeActive('#order-list li');
            console.log(obj.UserManager.isLogin);
            if (!obj.UserManager.isLogin) {
                obj.UserManager.LoginRedirectToTable = tableNo;
                if (angular.element('md-dialog[aria-label="Login"]').scope() == undefined) {
                    $mdDialog.show({
                        templateUrl: 'tmpl/login.html'
                    })
                } else {
                    $mdDialog.hide();
                }
            } else {
                SocketFactory.emit('adminLoadTableOrder', { 'tableNum': { '$value': tableNo } }, function (r) {
                    console.log('adminLoadTableOrder r' + r)
                    //alert(r)
                    r = JSON.parse(r);
                    console.log(r)
                    if (r.result === "OK") {
                        if (r.order.length > 1) {
                            //alert(callback);
                            callback(r.order);
                        } else {
                            var order = r.order[0];
                            console.log(order);
                            obj.modeMultiSelectItem = false;
                            obj.mode = obj.schema.mode.order;
                            obj.modeOrder = obj.schema.modeOrder.normal;
                            console.log(obj.mode);
                            obj.Cart.tableNo = tableNo;
                            obj.Cart.noOfPeople = '';
                            obj.Cart.member = {};
                            //console.log(angular.element('input-keypad').scope());
                            obj.Cart.cartList = [];
                            obj.Cart.orderList = [];
                            obj.Cart.noOfPeople = order.header.peopleNum;
                            if (order.header.member != undefined) {
                                obj.Cart.member = $.extend(true, order.header.member, obj.schema.member);
                            }
                            angular.forEach(order.item, function (v, k) {
                                console.log(v);
                                //this.push(key + ': ' + value);
                                var item = $.extend(true, {}, obj.schema.baseItem, v);
                                if (item.type === "I") {
                                    item.init();
                                    if (v.voidIndex != undefined) {
                                        if (v.voidIndex != -1) {
                                            //obj.Cart.orderList[v.voidIndex].voidQty = v.qty;
                                            $.each(obj.Cart.orderList, function (idx, vitem) {
                                                if (vitem.index == v.voidIndex) {
                                                    vitem.voidQty = v.qty;
                                                }
                                            });
                                        }
                                        item.voidRemark = v.voidRemark;
                                    }
                                    obj.Cart.orderList.push(item);
                                } else if (item.type === "S") {
                                    if (obj.Cart.orderList[obj.Cart.orderList.length - 1].option == undefined)
                                        obj.Cart.orderList[obj.Cart.orderList.length - 1].option = [{ items: [] }];
                                    obj.Cart.orderList[obj.Cart.orderList.length - 1].option[0].items.push(item);
                                } else if (item.type === "M") {
                                    var m = obj.food.modifier.filter(function (matchItem) {
                                        return matchItem.code == item.code;
                                    });
                                    item.modifier.push(m[0]);
                                }
                            });
                        }
                    }
                });
            }
        }
        
        obj.foodPromise = $http.get('files/food.json');
        //obj.loadStockPromise = $http.post(serviceUrl, { "action": "MDLoadItemQty" });
        obj.loadStockPromise = function () {
            var deferred = $q.defer();
            SocketFactory.emit('loadItemQty', '', function (r) {
                console.log(4444);
                console.log(r);
                r = JSON.parse(r);
                deferred.resolve(r);
            });
            return deferred.promise;
        }

        obj.tablePromise = $http.get('data/table_type.json');

        obj.stock = {
            item: null,
            list: [], updateStock: function (qty) {
                $.each(this.list, function (idx, item) {
                    if (item.isEdit) {
                        //item.qty = qty;
                        console.log(qty);
                        qty = qty == 0 ? -1 : qty;
                        console.log(qty);
                        var serviceUrl = 'service.ashx';
                        var a = { itemCode: item.code, qty: qty, remark: "" }
                        $http.post(serviceUrl, { action: "MDSetItemQty", data: a })
                            .then(function (result) {
                                //console.log("Data Saved: ", result);
                                //$scope.stock.updateItem(result.data);
                            });
                    }
                });
            },
            updateItem: function (data) {
                obj.stock.list = [];
                $.each(obj.food.category, function (catIndex, cat) {
                    $.each(cat.items, function (itemIndex, item) {
                        //item.stock = -2;
                        var found = false;
                        $.each(data, function (i, v) {
                            if (item.code == v.code) {
                                found = true;
                                item.stock = v.qty;
                                console.log(item);
                                obj.stock.list.push($.extend(true, {}, obj.schema.baseItem, v, item));
                            }
                        });
                        if (!found) {
                            item.stock = '';
                        }
                    })
                });
            },
            submitItem: function (qty) {
                console.log(qty);
                qty = qty == 0 ? -1 : qty;
                console.log(qty);
                var a = { itemCode: obj.stock.item.code, qty: qty, remark: "" }
                $http.post(serviceUrl, { action: "MDSetItemQty", data: a })
                    .then(function (result) {
                        //console.log("Data Saved: ", result);
                        //$scope.stock.updateItem(result.data);
                    });
                $('.calc-modal').modal('hide');
            },
            resetItem: function (item) {
                obj.stock.item = item;
                obj.stock.submitItem(-2);
            },
            reset: function () {
                SocketFactory.emit('resetItemQty', {});
            }
        }

        obj.selectCategory = function (category) {
            //console.log(category);
            obj.currentCategory = category;
            obj.currentPage = 1;
            obj.totalItems = obj.currentCategory.items.length;
            // $scope.$emit('reRender');
        }

        //obj.setModeMultiSelectItem = function (mode) {
        //    var selectMode = mode ? mode : "multi"; // default
        //    this.selectMode = selectMode;
        //    return selectMode;
        //};


        obj.toggleActive = function (element, callback) {
            if (typeof element === 'object') {
                // treat as event object
                var target = element.currentTarget || element.srcElement;
            } else {
                var target = element;
            }

            if (this.modeMultiSelectItem) {
                if (angular.element(target).hasClass("active")) {
                    angular.element(target).removeClass("active");
                } else {
                    angular.element(target).addClass("active");
                }
            } else {
                obj.setActive(element);
            }

            (callback || angular.noop)();
        }

        obj.setActive = function (element, callback) {
            console.log('set active');
            if (typeof element === 'object') {
                // treat as event object
                var target = element.currentTarget || element.srcElement;
            } else {
                var target = element;
            }
            console.log(element);
            console.log(target);
            angular.element(target).siblings().removeClass("active");
            
            angular.element(target).addClass("active");
            (callback || angular.noop)();
        }

        obj.removeActive = function (element, callback) {
            if (typeof element === 'object') {
                // treat as event object
                var target = element.currentTarget || element.srcElement;
            } else {
                var target = element;
            }
            //console.log(target);
            angular.element(target).removeClass("active");

            (callback || angular.noop)();
        }

        obj.promise = $q.all([ obj.foodPromise, obj.loadStockPromise(), obj.tablePromise ]).then(function (values) {
            console.log(values);
            obj.table = values[2].data;
            obj.food = values[0].data;
            obj.currentCategory = obj.food.category[0];
            obj.totalItems = obj.currentCategory.items.length;
            obj.totalCats = obj.food.category.length;
            obj.stock.updateItem(values[1]);
            //alert(obj.totalCats);
            //console.log(11111111111111);
            //console.log(values[1]);
            //obj.stock = {};
            //obj.stock.updateItem(JSON.parse(values[1].data));
        })

        obj.cancelDeleteItem = function () {
            obj.modeMultiSelectItem = false;
            obj.modeItem = obj.schema.modeItem.normal;
        }

        obj.unselectAllItem = function () {
            obj.removeActive('.order-list li', function () { });
        }

        obj.selectAllItem = function () {
            obj.setActive('.order-list li', function () { });
        }

        

        return obj;
    })


        //app.service('SocketService', function (SOCKETPORT,$q) {
        //    this.socketConnection = 'http://' + location.hostname + ':' + SOCKETPORT;
        //    this.socket = io.connect(this.socketConnection);
        //    this.socketReady = false;
        //    io.Manager(this.socketConnection, { reconnect: true });

        //    var deferred = $q.defer();
        //    this.getSocket = function () {
        //        return deferred.promise;
        //    };
        //    this.socket.on('connect', function () {
        //        console.log('socket connected');
        //        this.socketReady = true;
        //        deferred.resolve({});
        //    });
        //})

        //app.service('MainService', function ($http) {

        //});
})();