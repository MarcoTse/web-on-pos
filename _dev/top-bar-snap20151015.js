  <md-toolbar id="toolbar-top" class="md-hue-2 md-whiteframe-glow-z2 md-rms-theme toolbar-top" > \
    <div class="md-toolbar-tools" ng-controller="headerCtrl"> \
      <div layout="row" flex class="fill-height"> \
        <h1 class="logo md-toolbar-item"> \
          <!-- RMS --> \
          <md-menu> \
              <md-button md-no-ink="true" aria-label="Open phone interactions menu" class="md-icon-button" ng-click="$mdOpenMenu()"> \
                <md-icon class="dp45" md-svg-src="assets/icon/hexagon.svg" style="color: orange;" alt="RMS"></md-icon> \
              </md-button> \
              <md-menu-content width="2"> \
                <md-menu-item> \
                  <md-button md-no-ink="true" ng-click="panelMember()"> \
                    查閱會員 \
                  </md-button> \
                </md-menu-item> \
                <md-menu-item> \
                  <md-button md-no-ink="true" ng-click="ctrl.checkVoicemail()"> \
                    報表 \
                  </md-button> \
                </md-menu-item> \
                <!-- <md-menu-divider></md-menu-divider> --> \
              </md-menu-content> \
          </md-menu> \
        </h1> \
        <span flex=""></span> \
        <div class="md-toolbar-item" layout="row"> \
          \
          <!-- <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button" title="" ><strong>template</strong></button> --> \
          <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button active" title="落單模式" ng-click="MainService.switchModeFloorPlan(MainService.schema.modeFloorPlan.order); MainService.setActive($event); changeMode(\'Order\')" ng-show="MainService.mode == MainService.schema.mode.floorPlan"><strong>落單模式</strong></button> \
          <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button" title="做檯紙" ng-click="MainService.switchModeFloorPlan(MainService.schema.modeFloorPlan.printOrder); MainService.setActive($event); changeMode(\'PrintOrder\', 1)" ng-show="MainService.mode == MainService.schema.mode.floorPlan"><strong>做檯紙</strong></button> \
          <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button" title="轉檯" ng-click="MainService.switchModeFloorPlan(MainService.schema.modeFloorPlan.changeTable); MainService.setActive($event); changeMode(\'Transfer\', 2)" ng-show="MainService.mode == MainService.schema.mode.floorPlan"><strong>轉檯</strong></button> \
          <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button" title="印單" ng-click="MainService.switchModeFloorPlan(MainService.schema.modeFloorPlan.printBill);MainService.setActive($event); changeMode(\'PrintBill\')"><strong>印單</strong></button> \
          <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button" title="分單" ng-click="MainService.switchModeFloorPlan(MainService.schema.modeFloorPlan.transfer); MainService.setActive($event); changeMode(\'Divide\')" ng-show="MainService.mode == MainService.schema.mode.floorPlan"><strong>分單</strong></button> \
          <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button" title="結賬" ng-click="MainService.switchModeFloorPlan(MainService.schema.modeFloorPlan.billing); MainService.setActive($event); changeMode(\'Bill\')" ng-show="MainService.mode == MainService.schema.mode.floorPlan"><strong>結賬</strong></button> \
          <!-- <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button" title="廚房訊息" ng-click="; MainService.setActive($event);"><strong>廚房訊息</strong></button> --> \
            <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button" title="食物控制" ng-click="MainService.modeToggleFoodControl()" ng-hide="MainService.mode == MainService.schema.mode.floorPlan"><strong>食物控制</strong></button> \
            <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button" title="詳細資料" ng-click="MainService.modeToggleItemDetail($event)" ng-show="MainService.mode != MainService.schema.mode.floorPlan"><strong>詳細資料</strong></button> \
            <!-- important function for food submit --> \
          <!-- table mode function --> \
          <!-- scene control  \
            <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button" title="修改" ng-click="MainService.setActive($event); changeMode(\'Edit\')" ng-show="MainService.mode == MainService.schema.mode.floorPlan"><strong>修改</strong></button> \
            <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button" title="上一個場景" ng-click="prevZone()" ng-show="MainService.mode == MainService.schema.mode.floorPlan"><strong>上個場景</strong></button> \
            <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button" title="下一個場景" ng-click="nextZone()" ng-show="MainService.mode == MainService.schema.mode.floorPlan"><strong>下個場景</strong></button>--> \
            <!-- <md-button class="md-raised md-icon-button md-mini md-rounded-square" style="color: black"> \
                S{{SCENE.name}} \
            </md-button> --> \
          <!-- system notification --> \
            <!--<md-button md-no-ink="true" class="md-icon-button md-mini" aria-label="系統訊息" ng-click="showAlert($event)">--> \
             <!-- <md-icon md-svg-src="assets/icon/error.svg"></md-icon> --> \
             <!-- <i class="md-icon dp45">warning</i> --> \
             <!--<md-tooltip> \
               系統訊息 \
             </md-tooltip> \
             <i class="md-icon dp45">announcement</i> \
            </md-button>--> \
            <!-- primary function/each page --> \
            <!-- <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button" title="會員資料" ng-click="panelMember()"><strong>會員資料</strong></button> --> \
            <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button" title="登入" ng-click="login()" ng-show="!MainService.UserManager.isLogin && MainService.mode == MainService.schema.mode.floorPlan"><strong>登入</strong></button> \
            <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button" title="登出" ng-click="MainService.UserManager.logout($event)" ng-show="MainService.UserManager.isLogin && MainService.mode == MainService.schema.mode.floorPlan"><strong>登出</strong></button> \
        </div> \
      </div> \
</md-toolbar>