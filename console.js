﻿var path = require('path');

require("console-stamp")(console, {
    pattern: "HH:MM:ss.l",
    metadata: function () {
        var stackReg = /at\s+(.*)\s+\((.*):(\d*):(\d*)\)/i;
        var stackReg2 = /at\s+()(.*):(\d*):(\d*)/i;
        var stacklist = (new Error()).stack.split('\n').slice(3);
        var s = stacklist[0],
                sp = stackReg.exec(s) || stackReg2.exec(s);
        var data = {};
        if (sp && sp.length === 5) {
            data.method = sp[1];
            data.path = sp[2];
            data.line = sp[3];
            data.pos = sp[4];
            data.file = path.basename(data.path);
            data.stack = stacklist.join('\n');
            return "#" + data.line + "";
        }
    },
    colors: {
        stamp: "yellow",
        label: "white",
        metadata: "green"
    }
});