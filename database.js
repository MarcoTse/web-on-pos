var sql = require('mssql');
var moment = require('moment');
var mssql;

try {
    mssql = require("./setting.json").mssql;
} catch (ex) {
    mssql = require("./config/ras.json").mssql;
}



exports = module.exports = database;
//"mssql://weboncms:web-onaz1@rare.database.windows.net/Katoya?encrypt=true"

function database() {

    this.connection = null;

    database.prototype.executeStatement = function (query, callback) {
        var self = this;
        this.connection = new sql.Connection(mssql);
        self.connection.connect().then(function () {
            var request = new sql.Request(self.connection);
            request.multiple = true;

            var paraments = [];
            var sqlList = [];

            try {

                if (!query) {
                    console.log('query is null');
                    return;
                }

                for (var i = 0; i < query.length; i++) {
                    sqlList.push(query[i].sql);
                    if (query[i].args == undefined) continue;
                    for (var a = 0; a < query[i].args.length; a++) {
                        var q = query[i].args[a];
                        if (paraments.indexOf(q.c) == -1) {
                            paraments.push(q.c);
                            request.input(q.c, sql[q.t.name], q.v);
                            //switch (q.t.name) {
                            //}
                        }
                    }
                }
            } catch (ex) {
                console.log("database.js error", ex.toString());
                callback(ex.toString(), []);
                return;
            }

            console.log(sqlList);

            //console.log('request:', moment().format('YYYY-MM-DD HH:mm:ss'));
            request.query(sqlList.join(';')).then(function (recordsets) {
                //console.log('response:', moment().format('YYYY-MM-DD HH:mm:ss'));
                callback(null, recordsets);
            }).catch(function (err) {
                console.log("database.js error", err.toString());
                callback(err, []);
                self.connection.close();
                return;
            });

        }).catch(function (err) {
            console.log("database.js error", err.toString());
            callback(err, []);
            self.connection.close();
            return;
        })

    }
}



