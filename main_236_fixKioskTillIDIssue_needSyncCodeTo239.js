var async = require("async"),
    moment = require('moment'),
    TYPES = require('tedious').TYPES,
    path = require('path'),
    express = require('express'),
    bodyParser = require('body-parser'),
    app = express(),
    http2 = require('http'),
    http = require('http').createServer(handler),
    io = require('socket.io')(http),
    RAS = null,
    PickUp = null,
    ConnectionPool = require('tedious-connection-pool'),
    Connection = require('tedious').Connection,
    config = require('./config/ras.json'),
    goMenuConfig = require('./config/goMenu.json'),
    fs = require('fs'),
    xml2js = require('xml2js'),
    mqtt = require('mqtt'),
    mqttClient = mqtt.connect(),
    S = require('string'),
    soap = require('soap'),
    server = require('http').createServer(handler),
    xml = require('fs').readFileSync('./public/Emenu.wsdl', 'utf8'),
    Request = require('tedious').Request,
    schedule = require('node-schedule'),
    Q = require('Q'),
    //goQueue vars
    QueryString = require('querystring'),
    cronJob = require('cron').CronJob,
    cronUpdateQueueJob = null,
    cronUploadConfigJob = null,
    callingList = new Array(),
    isboardcastingCallingList = false,
    logsDir = "./logs/",
    deductPtsLogsDir = "./logs/deductPts/",
    scheduleJobList = [],
    queueConfig = require('./config/goqueue.json'),
    UploadTableList = require('./config/uploadTableList.json');

var report = require("./report").create(config.currentLang, config.defaultPrinter);

var deployPort = express();
//app.use(bodyParser.json()); // for parsing application/json
//app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
app.use(function (req, res, next) {
    if (/\/ras.json\/*/.test(req.path)) {
        return res.status(404).send("Not Found"); // or 403, etc
    };
    next();
});
app.use("/", express.static('public'));
app.use("/", express.static('config'));
app.listen(config.webPort);
http.listen(config.socketPort);

deployPort.use(bodyParser.json({ limit: '50mb' }));
deployPort.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
deployPort.listen(config.deployPort);
deployPort.post('/deployDB', function (request, response) {
    var postData = request.body;
    var callback = function (data) {
        response.writeHead(200, {
            'Content-Type': 'application/json;charset=utf-8'
        });
        response.end(data, 'utf-8');
    }
    response.setHeader('Access-Control-Allow-Origin', '*');
    //response.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    //response.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    //response.setHeader('Access-Control-Allow-Credentials', true);
    fs.writeFile("copydb.json", JSON.stringify(postData), function (err) {
        if (err) {
            //console.log(err);
        } else {
            ////console.log("The file was saved!");
        }

        callback("OK");
    });
});



var preventMulitpleOctopus = false;
var lockTableList = [];

/**************************/

var AzureIO = require('socket.io-client');
var AzureSocket = AzureIO.connect('http://localhost:3002', { reconnection: true });


/***********************/


app.post('*', function (request, response) {
    var filePath = '.' + request.url;
    var qs = QueryString.parse(S(filePath).substr(15).s);
    if (S(filePath).startsWith("./service.ashx")) {
        var postData = request.body;
        var callback = function (data) {
            response.writeHead(200, {
                'Content-Type': 'application/json;charset=utf-8'
            });
            response.end(data, 'utf-8');
        }
        switch (qs.action) {
            case "requestTicket":
                RAS.GoQueue.requestTicket(postData, callback, true);
                break;
            case "requestTicketNoCaptcha":
                RAS.GoQueue.requestTicket(postData, callback, false);
                break;
            case "updateTicketPanel":
                RAS.GoQueue.updateTicketPanel(postData, callback);
                break;
            case "seatedTicket":
                RAS.GoQueue.seatedTicket(postData, callback);
                break;
            case "callTicket":
                RAS.GoQueue.callTicket(postData, callback);
                break;
            case "skipTicket":
                RAS.GoQueue.skipTicket(postData, callback);
                break;
            case "reviewTickets":
                RAS.GoQueue.reviewTickets(postData, callback);
                break;
            case "modifyTicketStatus":
                RAS.GoQueue.modifyTicketStatus(postData, callback);
                break;
            case "modifyTicket":
                RAS.GoQueue.modifyTicket(postData, callback);
                break;
            case "modifyTicketSpecialRequest":
                RAS.GoQueue.modifyTicketSpecialRequest(postData, callback);
                break;
            case "skipAllTicket":
                RAS.GoQueue.skipAllTicket(postData, callback);
                break;
            case "updateConfig":
                RAS.GoQueue.updateConfig(postData, callback);
                break;
            case "openConfig":
                RAS.GoQueue.openConfig(postData, callback);
                break;
            case "reprint":
                RAS.GoQueue.reprint(postData, callback);
                break;
            case "updateDisplayPanel":
                RAS.GoQueue.updateDisplayPanel(postData, callback);
                break;
            case "uploadLog":
                RAS.GoQueue.uploadLog(postData, callback);
                break;
            case "saveDeviceLog":
                RAS.saveDeviceLog(postData, callback);
                break;
            case "refreshDisplay":
                RAS.GoQueue.refreshshopdisplay({ topic: 'allticketno' });
                break;
            case "sufficientTableMode":
                RAS.GoQueue.sufficientTableMode(postData, callback);
                break;
            case "clockIn":
                RAS.clockIn(postData, (result) => {
                    callback(JSON.stringify(result));
                });
                break;
            case "clockOut":
                RAS.clockOut(postData, (result) => {
                    callback(JSON.stringify(result));
                });
                break;
            case "masterpassCheckout":
                RAS.MasterpassCheckout(postData, callback);
                break;
            case "masterpassMemberLogin":
                RAS.MasterpassMemberLogin(postData, callback);
                break;
            case "masterpassCheckoutFail":
                if (lockTableList["L" + postData.tableNo] != undefined) {
                    clearTimeout(lockTableList["L" + postData.tableNo].timeout);
                }
                delete lockTableList["L" + postData.tableNo];
                io.sockets.emit('releaseLockTable', postData.tableNo);
                callback(JSON.stringify({ error: { code: 0, message: null } }));
                break;
            case "masterpassCheckoutSuccess":
                console.log('MasterpassCheckoutSuccess');
                RAS.MasterpassCheckoutSuccess(postData, callback);
                break;
            case "kdstrigger": PickUp.kdstrigger(postData, callback); break;
            default:
                callback("default");
        }
    }
    else if (S(filePath).startsWith("./initdb")) {
        RAS.GoQueue.initDB(qs.mode, callback);
    }
    else {
        response.writeHead(404);
        response.end();
    }
});

app.get('*', function (request, response) {
    var filePath = '.' + request.url;
    var qs = QueryString.parse(S(filePath).substr(15).s);
    if (S(filePath).startsWith("./service.ashx")) {
        var callback = function (data) {
            response.writeHead(200, {
                'Content-Type': 'application/json;charset=utf-8'
            });
            response.end(data, 'utf-8');
        }
        switch (qs.action) {
            case "requestTicket":
                RAS.GoQueue.requestTicket(qs, callback, true);
                break;
            case "requestTicketNoCaptcha":
                RAS.GoQueue.requestTicket(qs, callback, false);
                break;
            case "updateTicketPanel":
                RAS.GoQueue.updateTicketPanel(qs, callback);
                break;
            case "seatedTicket":
                RAS.GoQueue.seatedTicket(qs, callback);
                break;
            case "callTicket":
                RAS.GoQueue.callTicket(qs, callback);
                break;
            case "skipTicket":
                RAS.GoQueue.skipTicket(qs, callback);
                break;
            case "reviewTickets":
                RAS.GoQueue.reviewTickets(qs, callback);
                break;
            case "modifyTicketStatus":
                RAS.GoQueue.modifyTicketStatus(qs, callback);
                break;
            case "modifyTicket":
                RAS.GoQueue.modifyTicket(qs, callback);
                break;
            case "modifyTicketSpecialRequest":
                RAS.GoQueue.modifyTicketSpecialRequest(qs, callback);
                break;
            case "skipAllTicket":
                RAS.GoQueue.skipAllTicket(qs, callback);
                break;
            case "updateConfig":
                RAS.GoQueue.updateConfig(qs, callback);
                break;
            case "openConfig":
                RAS.GoQueue.openConfig(qs, callback);
                break;
            case "reprint":
                RAS.GoQueue.reprint(qs, callback);
                break;
            case "updateDisplayPanel":
                RAS.GoQueue.updateDisplayPanel(qs, callback);
                break;
            case "refreshDisplay":
                RAS.GoQueue.refreshshopdisplay({ topic: 'allticketno' });
                break;
            case "sufficientTableMode":
                RAS.GoQueue.sufficientTableMode(qs, callback);
                break;
            case "GetAllItem":
                RAS.GetAllItem(qs.itemId, function (result) {
                    callback(JSON.stringify(result))
                });
                break;
            case "setChairFlag":
                io.sockets.emit('setChairFlag', {
                    tableNo: '306',
                    orderNo: '18'
                });
                callback("ok");
                break;
            case "clockIn":
                RAS.clockIn(qs, (result) => {
                    callback(JSON.stringify(result));
                });
                break;
            case "clockOut":
                RAS.clockOut(qs, (result) => {
                    callback(JSON.stringify(result));
                });
                break;
            case "masterpassCheckout":
                RAS.MasterpassCheckout(qs, function (result) {
                    callback(result)
                });
                break;
            case "masterpassMemberLogin":
                RAS.MasterpassMemberLogin(qs, callback);
                break;
            case "masterpassCheckoutFail":
                if (lockTableList["L" + qs.tableNo] != undefined) {
                    clearTimeout(lockTableList["L" + qs.tableNo].timeout);
                }
                delete lockTableList["L" + qs.tableNo];
                io.sockets.emit('releaseLockTable', qs.tableNo);
                callback(JSON.stringify({ error: { code: 0, message: null } }));
                break;
            case "masterpassCheckoutSuccess":
                RAS.MasterpassCheckoutSuccess(qs, callback);
                break;
            case "kdstrigger": PickUp.kdstrigger(qs, callback); callback("default"); break;
            case "cupBatch": RAS.ECR.checkECR({ service: RAS.ECR.service.CUP_Batch, till: 're_1' }); callback("default"); break;
            case "cupTrace": RAS.ECR.checkECR({ service: RAS.ECR.service.CUP_Trace, till: 're_1' }); callback("default"); break;
            case "cupVoid": RAS.ECR.checkECR({ service: RAS.ECR.service.CUP_Void, till: 're_1', traceNumber: '000359' }); callback("default"); break;
            case "cupRefund": RAS.ECR.checkECR({ service: RAS.ECR.service.CUP_Refund, till: 're_1', invoiceSeq: '102', totalPrice: 1 }); callback("default"); break;
            case "cupSettlement": RAS.ECR.checkECR({ service: RAS.ECR.service.CUP_Settlement, till: 're_1' }); callback("default"); break;
            case "edcSettlement": RAS.ECR.checkECR({ service: RAS.ECR.service.EDC_Settlement, till: 're_1' }); callback("default"); break;
            case "edcRefund": RAS.ECR.checkECR({ service: RAS.ECR.service.EDC_Refund, till: 're_1', invoiceSeq: '100', totalPrice: 1 }); callback("default"); break;
            case "edcEPSVoid": RAS.ECR.checkECR({ service: RAS.ECR.service.EDC_EPS_Void, till: 're_1', traceNumber: '000356' }); callback("default"); break;
            default:
                callback("default");
        }
    }
    else if (S(filePath).startsWith("./initdb")) {
        RAS.GoQueue.initDB(qs.mode, callback);
    } else {
        response.writeHead(404);
        response.end();
    }
});

Array.prototype.getUnique = function () {
    var u = {}, a = [];
    for (var i = 0, l = this.length; i < l; ++i) {
        if (u.hasOwnProperty(this[i])) {
            continue;
        }
        a.push(this[i]);
        u[this[i]] = 1;
    }
    return a;
}

function handler(req, res) {
    fs.readFile(__dirname + '/index.html',
  function (err, data) {
      if (err) {
          res.writeHead(500);
          return res.end('Error loading index.html');
      }

      res.writeHead(200);
      res.end(data);
  });
}

function getToday() {
    var today = new Date();
    today = new Date(today.getFullYear(), today.getMonth(), today.getDate());
    return today;
}

function getTomorrow() {
    var today = getToday();
    var tomorrow = new Date(today.getFullYear(), today.getMonth(), today.getDate() + 1);
    return tomorrow;
}

function extend2(target) {
    var sources = [].slice.call(arguments, 1);
    sources.forEach(function (source) {
        for (var prop in source) {
            target[prop] = source[prop];
        }
    });
    target = (JSON.parse(JSON.stringify(target)));
    return target;
}

function getPrintDate(d) {
    var month = new Array();
    month[0] = "Jan";
    month[1] = "Feb";
    month[2] = "Mar";
    month[3] = "Apr";
    month[4] = "May";
    month[5] = "Jun";
    month[6] = "Jul";
    month[7] = "Aug";
    month[8] = "Sep";
    month[9] = "Oct";
    month[10] = "Nov";
    month[11] = "Dec";
    var n = month[d.getMonth()];
    return d.getDate() + " " + month[d.getMonth()] + " " + d.getFullYear();
}

var queue_action = {
    "callTicket": 0,
    "modifyTicket": 1,
    "modifyTicketSpecialRequest": 2,
    "modifyTicketStatus": 3,
    "requestTicket": 4,
    "seatedTicket": 5,
    "skipTicket": 6,
    "skipAllTicket": 7
};

var current_datetime = function () {
    var currentdate = new Date();
    var datetime = currentdate.getFullYear() + "/" + ("0" + (currentdate.getMonth() + 1)).slice(-2) + "/" + currentdate.getDate() + " " + ("0" + currentdate.getHours()).slice(-2) + ":" + ("0" + currentdate.getMinutes()).slice(-2) + ":" + ("0" + currentdate.getSeconds()).slice(-2);
    return datetime;
}

function postToServer(postData, postOptions, callback) {
    if (postOptions == null) {
        if (queueConfig.serverModel == undefined || queueConfig.serverModel == "slave") {
            postOptions = {
                host: queueConfig.centralserver.host,
                port: queueConfig.centralserver.port,
                path: queueConfig.centralserver.goqueueservice,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Content-Length': postData.length
                }
            }
        }
        else if (queueConfig.serverModel == "master") {
            if (queueConfig.slaveServer == undefined) throw "Please add config [slaveServer]";
            postOptions = {
                host: queueConfig.slaveServer.host,
                port: queueConfig.slaveServer.port,
                path: queueConfig.slaveServer.goqueueservice,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Content-Length': postData.length
                }
            };
        }
    }
    var postReq = http2.request(postOptions, function (res) {
        var str = ''
        res.setEncoding('utf8');
        res.on('data', function (chunk) {
            str += chunk;
        });
        res.on('end', function () {
            console.log('Response: ' + str);
            callback(str);
        });
    });
    if (postOptions.method === "POST")
        postReq.write(postData);
    postReq.end();
}

function postToMaster(postData, postOptions, callback) {
    if (postOptions == null) {
        if (queueConfig.masterServer == undefined) throw "Please add config [masterServer]";
        postOptions = {
            host: queueConfig.masterServer.host,
            port: queueConfig.masterServer.port,
            path: queueConfig.masterServer.goqueueservice,
            method: 'GET',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Content-Length': postData.length
            }
        };
    }
    var postReq = http2.request(postOptions, function (res) {
        res.setEncoding('utf8');
        res.on('data', function (chunk) {
            console.log('Response: ' + chunk);
            callback(chunk);
        });
    });

    postReq.setTimeout(3000);
    postReq.on('timeout', function (e) {
        console.log("timeout", e);
        callback("{\"response\":\"connectshoperror\",\"msg\":\"Connect shop server error : [Host: " + queueConfig.masterServer.host + "] [Status: timeout]\"}");
    });
    postReq.on('error', function (e) {
        console.log("error", e);
        callback("{\"response\":\"connectshoperror\",\"msg\":\"Connect shop server error : [Host: " + queueConfig.masterServer.host + "] [Status: " + e.errno + "]\"}");
    });
    postReq.write(postData);
    postReq.end();
}

function connToOctopus(qs, getOptions, callback) {
    if (getOptions == null) {
        throw "GET options cannot be null!"
    }
    var defOptions = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Content-Length': qs.length
        }
    };
    getOptions = extend2({}, defOptions, getOptions);
    getOptions.path += "?" + qs;
    console.log("connToOctopus", getOptions);

    var isFinish = false;
    var postReq = http2.request(getOptions, function (res) {
        res.setEncoding('utf8');
        res.on('data', function (chunk) {
            console.log('HTTP Response', chunk);
            if (!isFinish) {
                isFinish = true;
                console.log("callback Response", chunk);
                callback(chunk);
            }
        });
    });
    postReq.on('error', function (e) {
        console.log("error", e);
        if (!isFinish) {
            isFinish = true;
            console.log("callback error", "resultcode=110000");
            callback("resultcode=110000");
        }
        postReq.abort();
    })
    postReq.write(qs);
    postReq.end();
}

function resetCronJob() {
    RAS.GoQueue.showQueueingList(null, null, null);
    RAS.GoQueue.updateQueue();
    RAS.GoQueue.cronUploadConfig();

    cronUpdateQueueJob = new cronJob({
        cronTime: queueConfig.updatequeuetime,
        onTick: function () {
            RAS.GoQueue.updateQueue();
        },
        start: true
    });

    cronUploadConfigJob = new cronJob({
        cronTime: queueConfig.updateconfigtime,
        onTick: function () {
            RAS.GoQueue.cronUploadConfig();
        },
        start: true
    });

    if (queueConfig.retryDeductPts) {
        RAS.GoQueue.retryDeductPts();
        cronRetryDeductPtsJob = new cronJob({
            cronTime: queueConfig.retryDeductPtsTime,
            onTick: function () {
                RAS.GoQueue.retryDeductPts();
            },
            start: true
        });
    }

    PickUp.setCronJob();
}

new mqtt.Server(function (client) {
    var self = this;

    if (!self.clients) self.clients = {};

    client.on('connect', function (packet) {
        self.clients[packet.clientId] = client;
        client.id = packet.clientId;
        //console.log("CONNECT: client id: " + client.id);
        client.subscriptions = [];
        client.connack({ returnCode: 0 });
    });

    client.on('publish', function (packet) {
        //console.log("PUBLISH(%s): %j", client.id, JSON.stringify(packet.payload));
        //for (var k in self.clients) {
        //    self.clients[k].publish({topic: packet.topic, payload: packet.payload});
        //}

        try {
            var payload = JSON.parse(packet.payload);
            //console.log("PUBLISH(%s): %j", client.id, JSON.stringify(payload));
            if (payload.action != undefined) {
                switch (payload.action) {
                    case "updateTicket": {
                        RAS.broadcastTicketList(packet.topic, payload.updateListMode);
                        break;
                    }
                    case "printItem": {
                        RAS.PrintItem(packet);
                        break;
                    }
                    case "lockGroup": {
                        RAS.lockGroup(packet);
                        break;
                    }
                    case "changeStatus": {
                        RAS.ChangeStatus(packet);
                        break;
                    }
                    case "skipAllCallNo": {
                        RAS.SkipAllCallNo(packet);
                        break;
                    }
                }
            }
        } catch (e) {

        }
        PickUp.handleMqttPubMsg(packet);
        switch (packet.topic) {
            case "wo/dice/broadcastMessageRequest/": {
                RAS.broadcastDiceMessage();
                break;
            }
            case "wo/dice/addMessageRequest/": {
                RAS.addDiceMessage(packet.payload);
                break;
            }
            case "wo/dice/updateMessageRequest/": {
                RAS.updateDiceMessage(packet.payload);
                break;
            }
            case "wo/gmb/updateItemQty/": {
                RAS.UpdateItemQty();
                break;
            }
        }
        //console.log("publish", "topic " + packet.topic);
        for (var k in self.clients) {
            var c = self.clients[k], publish = false;
            if (c.subscriptions) {
                //console.log("publish", "publish: c.subscriptions: ", c.subscriptions);
                for (var i = 0; i < c.subscriptions.length; i++) {
                    var s = c.subscriptions[i];
                    if (s.test(packet.topic)) {
                        publish = true;
                        c.publish({ topic: packet.topic, payload: packet.payload });
                        break;
                    }
                }
                //console.log("publish", "publish: ispublish: " + publish);

                //if (!publish) {
                //    c.publish({ topic: packet.topic, payload: packet.payload });
                //}
            }
        }
        if (packet.messageId) {
            client.puback({ messageId: packet.messageId });
        }
    });

    client.on('subscribe', function (packet) {
        ////console.log("SUBSCRIBE(%s): %j", client.id, packet);
        var granted = [];
        for (var i = 0; i < packet.subscriptions.length; i++) {
            //if (packet.subscriptions[i].topic) {
            //console.log("SUBSCRIBE(%s): %j", client.id, packet.subscriptions[i].topic);
            //}
            var qos = packet.subscriptions[i].qos || 2,
                topic = packet.subscriptions[i].topic,
                reg = new RegExp(topic.replace('+', '[^\/]+').replace('#', '.+') + '$');

            var _haveSubscribe = false;
            for (var j = 0; j < client.subscriptions.length; j++) {
                if (S(client.subscriptions[j].toString()).trim() == S(reg.toString()).trim()) {
                    _haveSubscribe = true;
                    break;
                }
            }

            if (!_haveSubscribe) {
                granted.push(qos);
                client.subscriptions.push(reg);
                client.subscriptions = client.subscriptions.getUnique();
            }
        }

        client.suback({ messageId: packet.messageId, granted: granted });
    });

    client.on('unsubscribe', function (packet) {
        //writeDebug("unsubscribe", "start unsubscribe:");
        try {
            for (var i = 0; i < packet.unsubscriptions.length; i++) {
                var reg = new RegExp(packet.unsubscriptions[i].replace('+', '[^\/]+').replace('#', '.+$'));
                //writeDebug("unsubscribe", reg);
                // try use reg to solve
                var _unsubscribeIndex = -1;
                for (var j = 0; j < client.subscriptions.length; j++) {
                    //writeDebug("unsubscribe", "b4 debug1 |" + S(client.subscriptions[j]).trim() + "|");
                    //writeDebug("unsubscribe", "b4 reg |" + client.subscriptions[j].test(packet.unsubscriptions[i]) + "|");
                    if (client.subscriptions[j].test(packet.unsubscriptions[i])) {
                        _unsubscribeIndex = j;
                    }
                }
                //writeDebug("unsubscribe", "client.subscriptions.indexOf(reg):" + client.subscriptions.indexOf(S(reg).trim()));
                //writeDebug("unsubscribe", "client.subscriptions.indexOf(reg):" + client.subscriptions.indexOf("/wo/gm/table/3//"));
                if (_unsubscribeIndex != -1) {
                    client.subscriptions.splice(_unsubscribeIndex, 1);
                }
                for (var j = 0; j < client.subscriptions.length; j++) {
                    //writeDebug("unsubscribe", "after debug1 |" + client.subscriptions[j] + "|");
                }
                //var _content = makeStampWithMMSeconds(new Date()) + "[mqtt unsubscribe]: " + "unsubscribe: topic: " + topic + " ;qos: " + qos + ";cid:" + client.id + ";haveSubscribe" + _haveSubscribe;
                //fs.appendFile("./log/" + makeStampWithMMSeconds(new Date()).substring(0, 8) + ".log", _content + "\r\n", function (err) {
                //    if (err) {
                //        console.log(err);
                //    }
                //});
            }
        } catch (e) {
            //writeDebug("unsubscribe", e);
        }
        //writeDebug("unsubscribe", "end unsubscribe:");
        client.unsuback({ messageId: packet.messageId });
    });

    client.on('pingreq', function (packet) {
        client.pingresp();
    });

    client.on('disconnect', function (packet) {
        client.stream.end();
    });

    client.on('close', function (err) {
        delete self.clients[client.id];
    });

    client.on('error', function (err) {
        //console.log('error!', err);

        if (!self.clients[client.id]) return;

        client.stream.end();
        console.dir(err);
    });
}).listen(process.argv[2] || 1883);

io.on('connection', function (socket) {

    var clientIp = io.sockets.connected[socket.id].request.connection.remoteAddress;
    console.log('IP', clientIp);

    socket.on('getClientIp', function (callback) {
        callback(clientIp);


    })

    socket.on('loadTableSchema', function (data, callback) {
        RAS.LoadTableSchema(data, function (err, result) { callback(JSON.stringify(result)) });
    });

    socket.on('login', function (data, callback) {
        RAS.Login(data, callback);
    });

    socket.on('adminLoadTableOrder', function (data, callback) {
        RAS.AdminLoadTableOrder(data, function (result) {
            ////console.log("socket:" + result);
            callback(JSON.stringify(result))
        })
    });


    socket.on('loadItemQty', function (data, callback) {
        RAS.LoadItemQty(data, function (err, result) {
            ////console.log("socket:" + JSON.stringify(result));
            callback(JSON.stringify(result));
        })
    });

    socket.on('loadDiscount', function (data, callback) {
        RAS.LoadDiscount(data, function (result) {
            ////console.log("loadDiscount socket:" + JSON.stringify(result));
            callback(JSON.stringify(result));
        })
    });

    socket.on('loadTender', function (data, callback) {
        RAS.LoadTender(data, function (result) {
            callback(JSON.stringify(result));
        })
    });


    socket.on('resetItemQty', (data, callback) => {
        RAS.ResetItemQty(data, (result) => {
            callback(JSON.stringify(result));
        });
    });

    socket.on('saveTableOrder', function (data, callback) {
        RAS.SaveTableOrder(data, function (result) {
            ////console.log("socket:" + JSON.stringify(result));
            callback(JSON.stringify(result));
        });
        //RAS.SaveTableOrderTest(data, function (result) {
        //    ////console.log("socket:" + JSON.stringify(result));
        //    callback(JSON.stringify(result));
        //});
    });

    socket.on('saveLayoutTable', function (data, callback) {
        RAS.SaveLayoutTable(data, function (result) {
            ////console.log("socket:" + JSON.stringify(result));
            callback(JSON.stringify(result));
        })
    });

    socket.on('removeLayoutTable', function (data, callback) {
        RAS.RemoveLayoutTable(data, function (result) {
            ////console.log("socket:" + JSON.stringify(result));
            callback(JSON.stringify(result));
        })
    });

    socket.on('setItemQty', function (data, callback) {
        //console.log(data);
        RAS.SetItemQty(data, function (result) {
            callback(JSON.stringify(result));
        })
    });


    socket.on('billTableOrder', function (data, callback) {
        RAS.BillTableOrder(data, function (result) {
            ////console.log("socket:" + JSON.stringify(result));
            callback(JSON.stringify(result));
        })
    });


    socket.on('changeTable', function (data, callback) {
        RAS.ChangeTable(data, function (result) {
            callback(result);
        });
    });

    socket.on('print', function (data) {
        RAS.Print(data, function () { });
    });


    socket.on('loadStaff', function (data, callback) {
        RAS.LoadStaff(data, function (result) {
            //console.log("socket:" + result);
            callback(JSON.stringify(result))
        })
    });


    socket.on('editStaff', function (data, callback) {
        //console.log('editStaff start');
        //console.log(data);
        RAS.EditStaff(data, function (result) {
            //console.log("socket:" + result);
            callback(JSON.stringify(result))
        })
    });

    socket.on('deleteStaff', function (data, callback) {
        //console.log('deleteStaff start');
        //console.log(data);
        RAS.DeleteStaff(data, function (result) {
            //console.log("socket:" + result);
            callback(JSON.stringify(result))
        })
    });

    socket.on('loadReport', (data, callback) => {
        RAS.LoadReport(data, (result) => {
            callback(JSON.stringify(result));
        });
    });

    socket.on('saveTableSchema', (data, callback) => {
        RAS.SaveTableSchema(data, (result) => {
            callback(JSON.stringify(result));
        });
    });

    socket.on('dailyClearance', (data, callback) => {
        RAS.DailyClearance(data, (result) => {
            callback(JSON.stringify(result));
        });
    });

    socket.on('loadBill', (data, callback) => {
        RAS.LoadBill(data, (result) => {
            callback(JSON.stringify(result));
        });
    });

    socket.on('LoadBillByTakeAway', (data, callback) => {
        RAS.LoadBillByTakeAway(data, (result) => {
            callback(JSON.stringify(result));
        });
    });

    socket.on('LoadBillByDelivery', (data, callback) => {
        RAS.LoadBillByDelivery(data, (result) => {
            callback(JSON.stringify(result));
        });
    });



    socket.on('amendBill', (data, callback) => {
        RAS.AmendBill(data, (result) => {
            callback(JSON.stringify(result));
        });
    });


    socket.on('printBill', (data, callback) => {
        RAS.PrintBill(data, (result) => {
            callback(JSON.stringify(result));
        });
    });

    socket.on('loadTableForTransfer', (data, callback) => {
        RAS.LoadTableForTransfer(data, (result) => {
            callback(JSON.stringify(result));
        });
    });

    socket.on('transferItem', (data, callback) => {
        RAS.TransferItem(data, (result) => {
            callback(JSON.stringify(result));
        });
    });

    socket.on('clockIn', (data, callback) => {
        RAS.clockIn(data, (result) => {
            callback(JSON.stringify(result));
        });
    });

    socket.on('clockOut', (data, callback) => {
        RAS.clockOut(data, (result) => {
            callback(JSON.stringify(result));
        });
    });

    socket.on('cashControl', (data, callback) => {
        RAS.CashControl(data, (result) => {
            callback(JSON.stringify(result));
        });
    });


    socket.on('prepareDailyCashReport', (data, callback) => {
        RAS.PrepareDailyCashReport(data, (result) => {
            callback(JSON.stringify(result));
        });
    });

    socket.on('loadStaffGroup', (data, callback) => {

        RAS.LoadStaffGroup(data, (result) => {
            callback(JSON.stringify(result));
        });
    });

    socket.on('editStaffGroup', (data, callback) => {
        RAS.EditStaffGroup(data, (result) => {
            callback(JSON.stringify(result));
        });
    });


    socket.on('openCashBox', (data, callback) => {
        RAS.OpenCashBox(data, (result) => {
            callback(JSON.stringify(result));
        });
    });


    socket.on('editOrder', (data, callback) => {
        console.log('editOrder start')
        RAS.EditOrder(data, (result) => {
            callback(JSON.stringify(result));
        });
    });


    socket.on('splitTableOrder', (data, callback) => {
        //console.log('openCashBox start')
        RAS.SplitTableOrder(data, (result) => {
            callback(JSON.stringify(result));
        });
    });

    // configuration save/load
    socket.on('changeSettings', function (data, callback) {
        RAS.changeSettings(data, (result) => {
            callback(JSON.stringify(result));
        });
    });

    socket.on('loadSettings', function (data, callback) {
        RAS.loadSettings(data, (result) => {
            callback(JSON.stringify(result));
        });
    });


    //goQueue sockets
    socket.on('subscribe', function (data) {
        socket.join(data.topic);
        //        mqttClient.subscribe(data.topic);
    });

    socket.on('publish', function (data) {
        //        writeDebug("iosocket", 'publish to ' + data.topic);
        mqttClient.publish(data.topic, data.msg);
    });

    socket.on('refreshshopdisplay', function (data) {
        RAS.GoQueue.refreshshopdisplay(data);
    });

    socket.on('DownloadData', function () {
        GetTableSeesionSend(0);
        //RAS.DownloadData();
    });


    socket.on('SetItem', function (result) {
        result = JSON.parse(result);
        console.log(" ---------------------- " + "Got Rows:" + result.Rows)
        //result === "success"
        if (result.Rows > 0) {
            RAS.DownloadData(result, function (isSuccess) {
                console.log(" ---------------------- " + "success")
                if (isSuccess) {
                    GetTableSeesionSend(result.counter);
                }
            });
        }
        else {
            console.log(" ---------------------- " + "success")
            GetTableSeesionSend(result.counter);
        }
    });

    socket.on('GetCategory', function (CategoryId, callback) {
        RAS.GetCategory(CategoryId, function (result) {
            callback(JSON.stringify(result))
        })

    });

    socket.on('InsertItem', function (FoodItem, callback) {
        FoodItem = JSON.parse(FoodItem);
        RAS.InsertItem(FoodItem, function (result) {
            callback(result);
        })

    });

    socket.on('UpdateItem', function (FoodItem, callback) {
        FoodItem = JSON.parse(FoodItem);
        RAS.UpdateItem(FoodItem, function (result) {
            callback(result);
        })

    });

    socket.on('InsertCategory', function (CateItem, callback) {
        CateItem = JSON.parse(CateItem);
        RAS.InsertCategory(CateItem, function (result) {
            callback(result);
        })

    });

    socket.on('UpdateCategory', function (CateItem, callback) {
        CateItem = JSON.parse(CateItem);
        RAS.UpdateCategory(CateItem, function (result) {
            callback(JSON.stringify(result));
        })

    });


    socket.on('SearchItemIsExist', function (ItemCode, callback) {

        RAS.SearchItemIsExist(ItemCode, function (result) {

            console.log('callback:' + result);
            callback(result);
        })

    });

    // menu layout
    socket.on('listMenuLayout', (data, callback) => {
        RAS.ListMenuLayout(data, (result) => {
            callback(result);
        });
    });

    socket.on('loadMenuLayout', (data, callback) => {
        RAS.LoadMenuLayout(data, (result) => {
            callback(result);
        });
    });

    socket.on('saveMenuLayout', (data, callback) => {
        RAS.SaveMenuLayout(data, (result) => {
            callback(result);
        });
    });



    socket.on('deployMenuLayout', (data, callback) => {
        callback = callback ? callback : function () { };
        RAS.GenerateMenuLayout(() => {
            mqttClient.publish("wo/gm/all", JSON.stringify({ "action": "reloadMenu", "time": new Date().getTime() }));
            callback('OK');
        });
    });

    socket.on('getCurrentMenuLayout', (data, callback) => {
        RAS.GetCurrentMenuLayout((_obj) => {
            //console.log('getCurrentMenuLayout socket', _obj);
            callback(_obj);
        });
    });

    //socket.on('changeTime', () => { })
    socket.on('changetime', (data, callback) => {
        var topic = 'wo/gm/all';
        var _obj = {
            "action": "changeTime",
            "time": data.time
        };
        var _temp = JSON.stringify(_obj);
        console.log('changeTime', topic, _temp);
        mqttClient.publish(topic, _temp, { qos: 2 });
        callback("OK");
    });

    // time range
    socket.on('listTimeRange', (data, callback) => {
        RAS.ListTimeRange(data, (result) => {
            callback(JSON.stringify(result));
        })
    });

    socket.on('InsertTimeRange', (data, callback) => {

        RAS.InsertTimeRange(JSON.parse(data), (result) => {
            callback(result);
        })
    });

    socket.on('UpdateTimeRange', (data, callback) => {

        RAS.UpdateTimeRange(JSON.parse(data), (result) => {

            callback(result);

        })

    });


    socket.on('GenerateMenu', function (callback) {
        RAS.GenerateMenu(() => {
            mqttClient.publish("wo/gm/all", JSON.stringify({
                "action": "reloadMenu",
                "time": new Date().getTime()
            }));
            console.log('GenerateMenu');
            callback('OK');
        });
    });

    socket.on('GenerateMenuLayout', function (callback) {
        callback = callback ? callback : function () { };
        RAS.GenerateMenuLayout(() => {
            mqttClient.publish("wo/gm/all", JSON.stringify({ "action": "reloadMenu", "time": new Date().getTime() }));
            console.log('GenerateMenuLayout');
            callback('OK');
        });
    });

    socket.on('GetAllModifierItem', function (data, callback) {
        RAS.GetAllModifierItem(data, (result) => {
            callback(JSON.stringify(result));
        });
    });

    socket.on('InsertModifierItem', (data, callback) => {
        RAS.InsertModifierItem(JSON.parse(data), (result) => {
            callback(result);
        });
    });

    socket.on('UpdateModifierItem', (data, callback) => {
        RAS.UpdateModifierItem(JSON.parse(data), (result) => {
            callback(result);
        });
    });

    socket.on('GetAllModifierGroup', function (data, callback) {
        RAS.GetAllModifierGroup(data, (result) => {
            callback(JSON.stringify(result));
        });
    });

    socket.on('InsertModifierGroup', function (data, callback) {
        RAS.InsertModifierGroup(JSON.parse(data), (result) => {
            callback(result);
        });
    });

    socket.on("UpdateModifierGroup", function (data, callback) {

        RAS.UpdateModifierGroup(JSON.parse(data), (result) => {
            callback(result);
        });
    });

    socket.on('GetAllModifierItemByGroup', function (data, callback) {
        RAS.GetAllModifierItemByGroup(data, (result) => {
            callback(JSON.stringify(result));
        })
    })

    socket.on('InsertItemToModifierGroup', function (data, callback) {
        RAS.InsertItemToModifierGroup(data, (result) => {
            callback(result);
        })
    })

    socket.on('RemoveItemInModifierGroup', function (data, callback) {
        RAS.RemoveItemInModifierGroup(data, (result) => {
            callback(result);
        });
    });

    socket.on('GetAllOptionItem', (data, callback) => {

        RAS.GetAllOptionItem(data, (result) => {
            callback(JSON.stringify(result));
        });


    });

    socket.on('InsertOptionItem', function (data, callback) {
        RAS.InsertOptionItem(JSON.parse(data), (result) => {
            callback(result);
        });
    });


    socket.on('UpdateOptionItem', function (data, callback) {
        RAS.UpdateOptionItem(JSON.parse(data), (result) => {
            callback(result);
        });
    });



    socket.on('GetAllOptionGroup', (data, callback) => {

        RAS.GetAllOptionGroup(data, (result) => {
            callback(JSON.stringify(result));
        });


    });

    socket.on('InsertOptionGroup', function (data, callback) {
        RAS.InsertOptionGroup(JSON.parse(data), (result) => {
            callback(result);
        });
    });


    socket.on('UpdateOptionGroup', function (data, callback) {
        RAS.UpdateOptionGroup(JSON.parse(data), (result) => {
            callback(result);
        });
    });



    socket.on('GetAllOptionItemByGroup', function (data, callback) {
        RAS.GetAllOptionItemByGroup(data, (result) => {

            callback(JSON.stringify(result));
        });
    });

    socket.on('UpdateOptionItemSeqInGroup', function (data, callback) {
        RAS.UpdateOptionItemSeqInGroup(JSON.parse(data), (result) => {
            callback(JSON.stringify(result));

        });
    });


    socket.on('AddOptionInOptionGroup', function (data, callback) {
        RAS.AddOptionInOptionGroup(JSON.parse(data), (result) => {
            callback(result);

        });
    });

    socket.on('RemoveOptionFromOptionGroup', function (data, callback) {
        RAS.RemoveOptionFromOptionGroup(JSON.parse(data), (result) => {
            callback(result);

        });
    });


    socket.on('GetAllSubItem', function (data, callback) {
        RAS.GetAllSubItem(data, (result) => {
            callback(JSON.stringify(result));
        });
    });

    socket.on('GetAllModifierByItemCode', (data, callback) => {
        RAS.GetAllModifierByItemCode(data, (result) => {
            callback(JSON.stringify(result));
        });

    });

    socket.on('GetAllSubItemByItemCode', (data, callback) => {
        RAS.GetAllSubItemByItemCode(data, (result) => {
            callback(JSON.stringify(result));
        });

    });

    socket.on('GetAllOptionGroupByItemCode', (data, callback) => {
        RAS.GetAllOptionGroupByItemCode(data, (result) => {
            callback(JSON.stringify(result));
        });

    });

    socket.on('GetAllTimeRangeByItemCode', (data, callback) => {
        RAS.GetAllTimeRangeByItemCode(data, (result) => {
            callback(JSON.stringify(result));
        });

    });

    socket.on('InsertSubitem', function (data, callback) {
        RAS.InsertSubitem(JSON.parse(data), (result) => {
            callback(result);
        });
    });

    socket.on('UpdateSubitem', function (data, callback) {
        RAS.UpdateSubitem(JSON.parse(data), (result) => {
            callback(result);
        });
    });

    socket.on('GetAllTakeOutOrder', function (data, callback) {
        RAS.GetAllTakeOutOrder(data, (result) => {
            callback(JSON.stringify(result));
        });
    });

    socket.on('GetAllDeliveryOrder', function (data, callback) {
        RAS.GetAllDeliveryOrder(data, (result) => {
            callback(JSON.stringify(result));
        });
    });

    //io.sockets.emit('setSeatFlag', JSON.stringify({ "t": '1.1', "f": "5" }));

    //octopus dinein payment
    socket.on('octopusDineinPayment', function (data, callback) {
        console.log('octopusDineinPayment 1');
        console.log(JSON.stringify(data));

        RAS.OctopusDineinPayment(data, (result) => {
            console.log('octopusDineinPayment 2')
            callback(result);
        });
    });

    socket.on('GetTableCodeByTableSubCode', function (data, callback) {
        RAS.GetTableCodeByTableSubCode(data, (result) => {
            callback(JSON.stringify(result));
        });
    })


    socket.on('voidTakeOutBill', function (data, callback) {
        RAS.voidTakeOutBill(data, (result) => {
            callback(result);
        })
    })



    socket.on('BillTableOrderByTakeOut', function (data, callback) {

        //console.log(1389, io.sockets.connected[socket.id].request.connection.remoteAddress)
        /*for (key in io.sockets.connected) {
            console.log("socketId:", key, " clientIp:", io.sockets.connected[key].request.connection.remoteAddress)
        }*/

        var vaildOrder = true;

        if (RAS.token.success.indexOf(data.token) != -1) {
            console.log("order is exist!");
            vaildOrder = false;
        }

        if (RAS.token.pending.indexOf(data.token) != -1) {
            console.log("order is waiting!");
            vaildOrder = false;
        }

        if (vaildOrder) {

            RAS.token.pending.push(data.token);
            setTimeout(function () {
                if (RAS.token.pending.indexOf(data.token) != -1) {
                    RAS.token.pending.splice(RAS.token.pending.indexOf(data.token), 1);
                }
            }, 3000);

            RAS.BillTableOrderByTakeOut(data, function (result) {
                ////console.log("socket:" + JSON.stringify(result));
                RAS.token.success.push(data.token);
                callback(JSON.stringify(result));
            });

        } else {
            callback(null);
        }

        /**/
    });

    socket.on('checkDailyClearance', function (data, callback) {
        RAS.CheckDailyClearance(data, function (result) {
            ////console.log("socket:" + JSON.stringify(result));
            callback(JSON.stringify(result));
        })
    });

    socket.on('checkBlacklistStatus', function (data, callback) {
        RAS.Octopus.CheckBlacklistStatus(data, function (result) {
            callback(JSON.stringify(result));
        });
    });

    socket.on('changeCategorySetting', function (data, callback) {
        RAS.changeCategorySetting(data, function (result) {
            ////console.log("socket:" + JSON.stringify(result));
            callback(result);
        })
    });

    socket.on('ResetCategorySetting', function (data, callback) {
        RAS.ResetCategorySetting(data, function (result) {
            ////console.log("socket:" + JSON.stringify(result));
            callback(result);
        })

    });

    socket.on('getTableCodeByCardId', function (data, callback) {
        RAS.getTableCodeByCardId(data, function (result) {
            ////console.log("socket:" + JSON.stringify(result));
            callback(result);
        })

    });

    /*socket.on('lockTableByTableNo', function (data, callback) {

        if (lockTableList["L" + data.tableCode] == undefined) {
            var timeout = setTimeout(function () {
                delete lockTableList["L" + data.tableCode];
                io.sockets.emit('releaseLockTable', data.tableCode);
            }, 10000);

            lockTableList["L" + data.tableCode] = { timeout: timeout };
            io.sockets.emit('activeLockTable', data.tableCode);
        } else {
            clearTimeout(lockTableList["L" + data.tableCode].timeout);
            delete lockTableList["L" + data.tableCode];
        }
        console.log('lockTableByTableNo', lockTableList);
        callback("OK");
    });*/



    socket.on('lockTableByTableNo', function (data, callback) {
        console.log('lockTableByTableNo', data);
        if (lockTableList["L" + data.tableCode] != undefined) {
            clearInterval(lockTableList["L" + data.tableCode].timeout);
            delete lockTableList["L" + data.tableCode];
        }

        var timeout = setInterval(function () {
            //delete lockTableList["L" + data.tableCode];
            //io.sockets.emit('releaseLockTable', data.tableCode);
            if (io.sockets.connected[socket.id] != undefined) {
                //console.log(socket.id);
                io.sockets.emit('checkIsLockTable', data.tableCode);
            } else {
                clearInterval(lockTableList["L" + data.tableCode].timeout);
                delete lockTableList["L" + data.tableCode];
                io.sockets.emit('releaseLockTable', data.tableCode);
            }
            //io.sockets.connected[socket.id].emit('isConnect', function (result) {
            //    console.log('a');
            //    console.log(result);
            //})
        }, 2000);

        var clientIp = io.sockets.connected[socket.id].request.connection.remoteAddress;
        lockTableList["L" + data.tableCode] = { timeout: timeout, order: data.orderCode, clientIp: clientIp };
        io.sockets.emit('activeLockTable', data.tableCode);
        callback("OK");
    });

    socket.on('releaseLockedTable', function (data, callback) {
        console.log(data);
        if (lockTableList["L" + data.tableCode] != undefined) {
            clearInterval(lockTableList["L" + data.tableCode].timeout);
        }
        delete lockTableList["L" + data.tableCode];
        io.sockets.emit('releaseLockTable', data.tableCode);
        callback(data.tableCode);
    });
    /*socket.on('releaseLockedTable', function (data, callback) {
        if (lockTableList["L" + data.tableCode] != undefined) {
            clearTimeout(lockTableList["L" + data.tableCode].timeout);
        }
        delete lockTableList["L" + data.tableCode];
        io.sockets.emit('releaseLockTable', data.tableCode);
        callback(data.tableCode);
    });*/

    /*socket.on('reActiveLockedTable1', function (data, callback) {
        if (lockTableList["L" + data.tableCode] != undefined) {
            clearTimeout(lockTableList["L" + data.tableCode].timeout);
            var timeout = setTimeout(function () {
                delete lockTableList["L" + data.tableCode];
                io.sockets.emit('releaseLockTable', data.tableCode);
            }, 10000);
            lockTableList["L" + data.tableCode].timeout = timeout;
        }
    });*/

    socket.on('getAllLockedTableNo', function (callback) {
        callback(Object.keys(lockTableList));
    });

    //socket.on('getAllLockTableNo', function (data, callback) {
    //    RAS.lockTableByTableNo(data, function (result) {
    //        callback(result);
    //    })
    //});


    socket.on('getAllPriceBatch', function (callback) {
        RAS.getAllPriceBatch(function (result) {
            ////console.log("socket:" + JSON.stringify(result));
            callback(result);
        })
    });

    socket.on('getBatchDetailByBatch', function (data, callback) {
        RAS.getBatchDetailByBatch(data, function (result) {
            callback(result);
        })

    });

    socket.on('updateBatchDetailItem', function (data, callback) {

        RAS.updateBatchDetailItem(data, function (result) {
            callback(result);
        })
    })

    socket.on('getAllNotInBatchDetailItem', function (data, callback) {
        RAS.getAllNotInBatchDetailItem(data, function (result) {
            callback(result);
        })
    });

    socket.on('removeDetailItemByBatch', function (data, callback) {
        RAS.removeDetailItemByBatch(data, function (result) {
            callback(result);
        })
    })

    socket.on('insertDetailItemByBatch', function (data, callback) {
        RAS.insertDetailItemByBatch(data, function (result) {
            callback(result);
        })

    })

    socket.on('updatePriceBatch', function (data, callback) {
        RAS.updatePriceBatch(data, function (result) {
            callback(result);
        })
    });

    socket.on('insertPriceBatch', function (data, callback) {
        RAS.insertPriceBatch(data, function (result) {
            callback(result);
        })
    });

    socket.on('getAllPayment', (data, callback) => {
        RAS.getAllPayment(data, (result) => {
            callback(result);
        });
    });

    socket.on('deletePayment', (data, callback) => {
        RAS.deletePayment(data, (result) => {
            callback(result);
        });
    });

    socket.on('insertPayment', (data, callback) => {
        RAS.insertPayment(data, (result) => {
            callback(JSON.stringify(result));
        })
    })


    socket.on('checkUserExist', (data, callback) => {
        RAS.checkUserExist(data, (result) => {
            callback(result);
        })

    })

    socket.on('registerStaffWatch', (data, callback) => {
        RAS.registerStaffWatch(data, (result) => {
            callback(result);
        })
    })

    socket.on('checkPaymentPermission', (data, callback) => {
        RAS.checkPaymentPermission(data, (result) => {
            callback(result);
        })
    })

    socket.on('changeCollectStatus', (data, callback) => {
        RAS.changeCollectStatus(data, (result) => {
            callback(result);
        })
    })

    socket.on('enquiryOctopus', (data, callback) => {
        //RAS.EnquiryOctopus(data, (result) => {
        //    callback(result);
        //});
        console.log("EnquiryOctopus");
        var params = extend2({
            service: RAS.Octopus.service.Octopus_enquiry,
            till: data.till.$value
        });
        RAS.Octopus.connect(params, (result) => {
            callback(result);
        });
    });

    socket.on('cancelIncompleteTransaction', (data, callback) => {
        //RAS.CancelOctopusIncompleteTransaction(data, (result) => {
        //    callback(result);
        //});
        var params = extend2({
            service: RAS.Octopus.service.Octopus_cancelIncomp,
            till: data.till.$value
        });
        RAS.Octopus.connect(params, (result) => {
            callback(result);
        });
    });

    socket.on('getCustomerData', (data, callback) => {
        var sql = "select * from [customer] where phone=@phone"
        var args = [{ "c": "phone", "t": TYPES.VarChar, "v": data.phone }]

        RAS.DM.executeStatement({ sql: sql, args: args }, (err, result) => {
            callback(result);
        });
    })

    socket.on('deleteCustomerData', (data, callback) => {
        var sql = "delete from [customer] where phone=@phone and address=@address";
        var args = [
            { "c": "phone", "t": TYPES.VarChar, "v": data.phone },
            { "c": "address", "t": TYPES.VarChar, "v": data.address }
        ]

        RAS.DM.executeStatement({ sql: sql, args: args }, (err, result) => {
            if (err == null) callback("OK");
        });
    })

    socket.on('updateTakeOutAndDeliveryCustomerData', (data, callback) => {

        var sql = "update [transaction] set customerData=@customerData, isDelivery=@isDelivery, serveTime=@serveTime where refNo=@refNo;IF NOT EXISTS (SELECT  1 FROM customer  WHERE   phone = @phone  AND  address = @address) BEGIN INSERT into customer (phone, name, address) VALUES (@phone, @name, @address) END;";


        data.customerData.lastUpdatedBy = data.staffId;
        data.customerData.lastUpdatedTime = moment().format('YYYY-MM-DD HH:mm:ss');

        var args = [
            { "c": "customerData", "t": TYPES.NVarChar, "v": JSON.stringify(data.customerData) },
            { "c": "phone", "t": TYPES.VarChar, "v": data.customerData.phone },
            { "c": "address", "t": TYPES.VarChar, "v": data.customerData.address },
            { "c": "name", "t": TYPES.VarChar, "v": data.customerData.name },
            { "c": "isDelivery", "t": TYPES.Bit, "v": data.customerData.isDelivery },
            { "c": "serveTime", "t": TYPES.Time, "v": data.customerData.time },
            { "c": "refNo", "t": TYPES.VarChar, "v": data.refNo }
        ]

        RAS.DM.executeStatement({ sql: sql, args: args }, (err, result) => {
            if (err == null) callback('OK');
        })
    })
});


function GetTableSeesionSend(successCounter) {
    //var tableName = ["item", "itemCategory", "Category", "itemOptionGroup"]
    var tableName = UploadTableList.List;
    //{ tableName: "timeslot", SkipColumn: ["timeslotId"] }


    if (successCounter < tableName.length) {
        var tempObject = {};
        tempObject.counter = successCounter;
        tempObject.tableName = tableName[successCounter].tableName;
        tempObject.SkipColumn = tableName[successCounter].SkipColumn;
        tempObject.Filter = tableName[successCounter].Filter;
        tempObject.ShopCode = config.shopCode;
        console.log(tableName[successCounter].tableName + " - " + "waiting")
        AzureSocket.emit('GetItem', JSON.stringify(tempObject));
    }
}

app.use(express.static(path.join(__dirname, 'public')));
app.get('/', function (req, res) {
    res.sendFile(__dirname + '/index.html');
});


["log", "warn", "error"].forEach(function (method) {
    var oldMethod = console[method].bind(console);
    console[method] = function () {
        oldMethod.apply(
            console,
            [moment().format("HH:mm:ss")].concat([].map.call(arguments, function (value, index) { return value; })).concat('\r\n')
        );
    };
});


var SoapServices = {
    'Service': {
        'ServiceSoap': {
            'MDLoadTableOrder': function (args, callback) {
                args.exact = { '$value': true };
                RAS.AdminLoadTableOrder(args, function (r) {
                    var builder = new xml2js.Builder({ renderOpts: { 'newline': '' }, headless: true, rootName: "***" });
                    var xml = builder.buildObject(r);
                    //console.log(xml);
                    callback({
                        'MDLoadTableOrderResponse': {
                            'MDLoadTableOrderResult': xml
                        }
                    });
                });
            },
            'MDSaveTableOrder': function (args, callback) {
                var parser = new xml2js.Parser();
                parser.parseString(args.itemData.$value, function (err, r) {
                    args.itemData.json = r;
                    //TabletSaveTableOrder
                    //RAS.TabletSaveTableOrder(args, function (r) {
                    RAS.SaveTableOrder(args, function (r) {
                        var builder = new xml2js.Builder({ renderOpts: { 'newline': '' }, headless: true, rootName: "***" });
                        var xml = builder.buildObject(r);
                        //console.log(xml);
                        //mqttClient.publish("wo/gm/table/" + args.sourceTableNo + "/", JSON.stringify({ "action": "unlinkTable", "tableno": args.sourceTableNo }), { qos: 2 });
                        //mqttClient.publish("wo/gm/table/109/", JSON.stringify({ "action": "unlinkTable", "tableno": 109 }), { qos: 2 });
                        callback({
                            'MDSaveTableOrderResponse': {
                                'MDSaveTableOrderResult': xml
                            }
                        });
                    });
                });
            },
            'MDLinkTable': function (args, callback) {
                //console.log("mdlinktable----------------------tableNum");

                RAS.LinkTable(args, function (r) {
                    var builder = new xml2js.Builder({ renderOpts: { 'newline': '' }, headless: true, rootName: "***" });
                    var xml = builder.buildObject(r);
                    //console.log(xml);
                    callback({
                        'MDLinkTableResponse': {
                            'MDLinkTableResult': xml
                        }
                    });
                });
            },
            'MDPaymentOctopus': function (args, callback) {
                console.log('MDPaymentOctopus', args);
                var parser = new xml2js.Parser();
                parser.parseString(args.itemData.$value, function (err, r) {
                    args.itemData.json = r;
                    RAS.SaveTakeoutOrder(args, function (r) {
                        r.transactionData = JSON.stringify(r.transactionData);
                        console.log("MDPaymentOctopus", r);
                        var builder = new xml2js.Builder({ renderOpts: { 'newline': '' }, headless: true, rootName: "***" });
                        var xml = builder.buildObject(r);
                        callback({
                            'MDPaymentOctopusResponse': {
                                'MDPaymentOctopusResult': xml
                            }
                        });
                    });
                });
            },
            'MDSetPreOrder': function (args, callback) {
                console.log('MDSetPreOrder', args);
                var parser = new xml2js.Parser();
                parser.parseString(args.itemData.$value, function (err, r) {
                    args.itemData.json = r;
                    RAS.SetPreOrder(args, function (r) {
                        console.log("MDSetPreOrder", r);
                        var builder = new xml2js.Builder({ renderOpts: { 'newline': '' }, headless: true, rootName: "***" });
                        var xml = builder.buildObject(r);
                        callback({
                            'MDSetPreOrderResponse': {
                                'MDSetPreOrderResult': xml
                            }
                        });
                    });
                });
            },
            'MDLoadPreOrder': function (args, callback) {
                console.log('MDLoadPreOrder', args);
                RAS.LoadPreOrder(args, function (r) {
                    var builder = new xml2js.Builder({ renderOpts: { 'newline': '' }, headless: true, rootName: "***" });
                    var xml = builder.buildObject(r);
                    //console.log(xml);
                    callback({
                        'MDLoadPreOrderResponse': {
                            'MDLoadPreOrderResult': xml
                        }
                    });
                });
            }
        }
    }
}

server.listen(8007);
//server.listen(8001);
soap.listen(server, '/Service/EmenuService', SoapServices, xml);
soap.listen(server, '/services/EmenuService', SoapServices, xml);

function DatabaseManager(connString) {
    this.connString = connString;
    this.pool = new ConnectionPool({}, this.connString);
    var that = this;

    function es(sql, callback, args) {
        var jsonArray = [];
        that.pool.acquire(function (err, connection) {
            if (err) {
                console.error(err);
                if (callback)
                    callback(err, null);
            } else {
                //use the connection as normal
                var request = new Request(sql, function (err, rowCount) {
                    if (err)
                        console.error(err);
                    ////console.log('rowCount: ' + rowCount);
                    //release the connection back to the pool when finished
                    connection.release();
                    if (callback)
                        callback(err, jsonArray);
                });

                request.on('row', function (columns) {
                    var rowObject = {};
                    columns.forEach(function (column) {
                        rowObject[column.metadata.colName] = column.value;
                    });
                    jsonArray.push(rowObject)
                });
                if (args != null) {
                    if (args.length != 0) {
                        for (var i = 0; i < args.length; i++) {
                            if (args[i].o) {
                                request.addParameter(args[i].c, args[i].t, args[i].v, args[i].o);
                            } else {
                                request.addParameter(args[i].c, args[i].t, args[i].v);
                            }
                        }
                    }
                }
                connection.execSql(request);
            }
        });
    }

    this.executeStatement = function (sql, callback, type) {
        if (Array.isArray(sql)) {
            var functions = [];
            for (var i = 0; i < sql.length; i++) {
                var cf = function createfunc(i) {
                    return function (callback) {
                        es(sql[i].sql, callback, sql[i].args);
                    }
                }
                var f = cf(i)
                functions.push(f);
            }
            switch (type) {
                case "parallel":
                    async.parallel(functions,
                    // optional callback
                    function (err, results) {
                        // the results array will equal ['one','two'] even though
                        // the second function had a shorter timeout.
                        if (err) {
                            //console.log(err);
                        }
                        callback(err, results);
                    });
                    break;
                default:
                    async.series(functions,
                    // optional callback
                    function (err, results) {
                        // the results array will equal ['one','two'] even though
                        // the second function had a shorter timeout.
                        if (err) {
                            //console.log(err);
                        }
                        callback(err, results);
                    });
            }
        } else {
            es(sql.sql, callback, sql.args);
        }


    }
}

function PickUpManager(connString) {
    this.DM = new DatabaseManager(connString);
    var cronRemoveNumberJob = null;
    var that = this;
    this.displayNumber = (callNumber) => {
        var query = [
            { "sql": "select top(1) * from pickupNumber order by id" },
            {
                "sql": "select * from manualPickupNumber where (remove is null or remove <> 1) and date between @today and @tomorrow order by date desc",
                "args": [
                    { "c": "today", "t": TYPES.DateTime, "v": getToday() },
                    { "c": "tomorrow", "t": TYPES.DateTime, "v": getTomorrow() }
                ]
            }
        ];
        this.DM.executeStatement(query, (err, result) => {
            console.log("displayNumber", result);
            if (!result[0].length) {
                result[0][0] = { ordernumber: [] };
            }
            else {
                result[0][0].ordernumber = JSON.parse(result[0][0].ordernumber);
            }

            result[1].forEach(function (man) {
                // if (result[0][0].ordernumber.indexOf(man.number) == -1) {
                // result[0][0].ordernumber.push(man.number.substr(man.number.length-3));
                // }
                var number = man.number.substr(man.number.length - 3);
                if (result[0][0].ordernumber.indexOf(number) == -1) {
                    result[0][0].ordernumber.push(number);
                }
            });
            var _obj = { "action": "displayPickup", "numbers": result[0][0].ordernumber };
            if (callNumber != undefined) _obj.callNumber = "" + callNumber.substr(callNumber.length - 3);
            mqttClient.publish('wo/gs/pickupDisplay/', JSON.stringify(_obj), { qos: 2 });
            console.log("displayNumber: ", JSON.stringify(_obj));
        });
    }

    this.kdstrigger = (qs, callback) => {
        callback("ok");
        var currentnumberlist = [];
        if (qs.o.length) {
            currentnumberlist = qs.o.split(',');
        }
        for (var i = 0; i < currentnumberlist.length; i++) {
            if (currentnumberlist[i].indexOf('-') != -1) {

                currentnumberlist[i] = currentnumberlist[i].substr(currentnumberlist[i].length - 3);
            }
        }

        var query = {
            "sql": "delete from pickupNumber; insert into pickupNumber (ordernumber) values (@ordernumber)",
            "args": [
                { "c": "ordernumber", "t": TYPES.NVarChar, "v": JSON.stringify(currentnumberlist) }
            ]
        };

        this.DM.executeStatement(query, (err) => {
            this.displayNumber();
        });
    }

    this.editNumber = (type, data) => {
        if (type === "add") {
            console.log('add number', data);
            var query = {
                "sql": "if exists (select * from manualPickupNumber where number = @number and date between @today and @tomorrow) begin \
update manualPickupNumber set date = @date, remove = 0 where number = @number and date between @today and @tomorrow end \
else begin insert into manualPickupNumber (number, date) values (@number, @date) end",
                "args": [
                    { "c": "number", "t": TYPES.NVarChar, "v": data },
                    { "c": "date", "t": TYPES.DateTime, "v": new Date() },
                    { "c": "today", "t": TYPES.DateTime, "v": getToday() },
                    { "c": "tomorrow", "t": TYPES.DateTime, "v": getTomorrow() }
                ]
            }
            this.DM.executeStatement(query, (err) => {
                this.displayNumber(data);
            });
        }
        else if (type === "remove") {
            var query = {
                "sql": "if exists (select * from manualPickupNumber where number = @number and date between @today and @tomorrow) begin \
update manualPickupNumber set remove = 1, removetime = @removetime where number = @number and date between @today and @tomorrow end",
                "args": [
                    { "c": "number", "t": TYPES.NVarChar, "v": data },
                    { "c": "removetime", "t": TYPES.DateTime, "v": new Date() },
                    { "c": "today", "t": TYPES.DateTime, "v": getToday() },
                    { "c": "tomorrow", "t": TYPES.DateTime, "v": getTomorrow() }
                ]
            }
            this.DM.executeStatement(query, (err) => {
                this.displayNumber();
            });
        }
    }

    this.clearNumber = () => {
        var query = {
            "sql": "update manualPickupNumber set remove = 1, removetime = getDate() where date between @today and @tomorrow",
            "args": [
                { "c": "today", "t": TYPES.DateTime, "v": getToday() },
                { "c": "tomorrow", "t": TYPES.DateTime, "v": getTomorrow() }
            ]
        }
        this.DM.executeStatement(query, (err) => {
            this.displayNumber();
        });
    }

    this.removeExpiryNumber = () => {
        console.log("removeExpiryNumber");
        if (config.pickupDisplay && config.pickupDisplay.maxTimeKeep) {
            var query = {
                "sql": "update manualpickupnumber set remove = 1, removetime = getDate() output inserted.* where dateadd(minute, @time, date) < getDate() and (remove != 1 or remove is null)",
                "args": [
                    { "c": "time", "t": TYPES.Int, "v": config.pickupDisplay.maxTimeKeep }
                ]
            };
            this.DM.executeStatement(query, (err, result) => {
                console.log("removeExpiryNumber: ", result);
                var sql2 = new Array();
                result.forEach((man) => {
                    sql2.push({
                        "sql": "update kdsOrders set kdsstatus = 2, dismissTime = getdate() output inserted.* where orderNo = @refNo ;", "args": [{ "c": "refNo", "t": TYPES.VarChar, "v": man.number }]
                    })
                })
                this.DM.executeStatement(sql2, (err, result) => {
                    console.log("removeExpiryNumber results: ", result);
                    if (result.length != 0) {
                        if (result[result.length - 1].length != 0) {
                            //TODO: Use the config set CallNo KDS topic.
                            RAS.showKds("KDS1", 2);
                        }
                    }
                })
                this.displayNumber()
            });
        }
    }

    this.handleMqttPubMsg = (packet) => {
        if (packet.topic.match(/wo\/[^\/]+\/pickupDisplayStartUp/g)) {
            this.displayNumber();
        }
        if (packet.topic.match(/wo\/[^\/]+\/manualAddPickup/g)) {
            this.editNumber('add', packet.payload);
        }
        if (packet.topic.match(/wo\/[^\/]+\/manualRemovePickup/g)) {
            this.editNumber('remove', packet.payload);
        }
        if (packet.topic.match(/wo\/[^\/]+\/manualClearPickup/g)) {
            this.clearNumber();
        }
    }

    this.setCronJob = () => {
        if (cronRemoveNumberJob != null) {
            cronRemoveNumberJob.stop();
        }
        if (config.pickupDisplay && config.pickupDisplay.removeNumberTime) {
            cronRemoveNumberJob = new cronJob({
                cronTime: config.pickupDisplay.removeNumberTime,
                onTick: function () {
                    that.removeExpiryNumber();
                },
                start: true
            });
        }
    }
}

function RASManager(connString, options) {

    this.DM = new DatabaseManager(connString);
    this.GenerateMenu = function (callback) {
        //this.DM.executeStatement(['select * from item where code = @code', 'select * from category'], [[{ c: 'code', t: TYPES.VarChar, v: 'AA02' }], null]
        var sql = new Array();
        sql.push({ "sql": "select * from item", "args": null });
        sql.push({ "sql": "select * from category order by seq", "args": null });
        sql.push({ "sql": "select * from itemcategory order by categoryid,seq", "args": null });
        sql.push({ "sql": "select * from pricebatch pb  inner join pricebatchd pbd on pb.pricebatchid = pbd.pricebatchid where activatedate <= getdate() and (expiredate >= GETDATE() or expiredate is null)", "args": null });
        sql.push({ "sql": "select * from itemTimeRange itr inner join timeRange tr on itr.timeRangeId = tr.timeRangeId", "args": null });
        sql.push({ "sql": "select name1,name2,name3,namek,m.goMenu as mgoMenu, mg.goMenu as mggoMenu,mg.modifierGroupCode,m.modifierCode from modifier m inner join modifierGroupModifier mgm on m.modifierCode = mgm.modifierCode inner join modifierGroup mg on mg.modifierGroupCode = mgm.modifierGroupCode", "args": null })
        sql.push({ "sql": "select * from itemModifierGroup", "args": null })
        sql.push({ "sql": "select * from itemOptionGroup", "args": null })
        sql.push({ "sql": "select o.name1,o.name2,o.name3,o.namek,o.isDefault,o.color,printer,printerGroup,individualPrint,o.goMenu as ogoMenu, og.goMenu as oggoMenu,o.optionCode,o.plu,o.holdKitchenPrinter, og.optionGroupCode,og.name1 as ogName1, og.name2 as ogName2, og.name3 as ogName3,o.kds from [option] o inner join [optionGroupOption] ogo on o.optionCode = ogo.optionCode inner join optionGroup og on og.optionGroupCode = ogo.optionGroupCode order by ogo.seq ", "args": null })
        sql.push({ "sql": "select * from itemSubItem", "args": null })
        sql.push({ "sql": "select * from subItem", "args": null })
        sql.push({ "sql": "select * from kitchenMsg", "args": null })
        sql.push({ "sql": "select * from discount order by seq", "args": null })
        sql.push({ "sql": "select * from tender order by seq", "args": null })
        sql.push({ "sql": "select * from specialDate ", "args": null })
        sql.push({ "sql": "select o.*, ogo.optionGroupCode from optionGroupOption as ogo, [option] as o where o.optionCode = ogo.optionCode and o.plu != o.optionCode and o.plu is not null and ogo.optionGroupCode in (SELECT distinct optionGroupCode  FROM [itemOptionGroup] where compositeOptionGroup = 1)" })

        //args = [[{ c: 'code', t: TYPES.VarChar, v: 'AA02' }], null, null, null];
        this.DM.executeStatement(sql, (err, obj) => {
            ////console.log(JSON.stringify(obj));
            var items = obj[0], category = obj[1], itemcategory = obj[2], pricebatch = obj[3], timeRange = obj[4], modifier = obj[5], itemModifier = obj[6], itemOption = obj[7], option = obj[8], itemSubitem = obj[9], subitem = obj[10], kitchenMsg = obj[11], discount = obj[12], tender = obj[13], specialDate = obj[14], compositeItem = obj[15];
            this.Item = items;
            //category.sort(function (a, b) {
            //    return a.seq > b.seq;
            //});
            ////console.log(category)

            var now = new Date();
            var _obj = { "category": [], "modifiers": [], "kitchenmsg": [], "compositeOptionGroup": { optionGroupList: [], itemCodeList: [], itemList: [] } };


            compositeItem.map(function (i) {
                var optionGroupCode = i.optionGroupCode;
                var plu = i.plu;
                if (_obj.compositeOptionGroup.optionGroupList.indexOf(optionGroupCode) == -1) {
                    _obj.compositeOptionGroup.optionGroupList.push(optionGroupCode);
                    _obj.compositeOptionGroup.itemList.push({ items: [] });
                    _obj.compositeOptionGroup.itemCodeList.push({ items: [plu] });
                } else {
                    var idx = _obj.compositeOptionGroup.optionGroupList.indexOf(optionGroupCode);
                    _obj.compositeOptionGroup.itemCodeList[idx].items.push(plu);
                }
            })

            function _filterItem(cat) {
                return itemcategory.map(function (obj) {
                    var returnItem = null;
                    if (obj.categoryId == cat.categoryId) {
                        items.map(function (tempItem) {

                            if (tempItem.itemId == obj.itemId) {

                                var pricelist = pricebatch.filter(function (pbobj) {
                                    return pbobj.itemId == tempItem.itemId;
                                }).sort(function (a, b) { return a.activateDate < b.activateDate });

                                if (pricelist.length != 0) {
                                    returnItem = tempItem;

                                    returnItem.name = {};
                                    returnItem.name[config.lang1] = tempItem.name1;
                                    returnItem.name[config.lang2] = tempItem.name2;
                                    returnItem.name[config.lang3] = tempItem.name3;
                                    returnItem.group = cat.code;
                                    returnItem.serviceCharge = tempItem.serviceCharge;
                                    returnItem.unitprice = pricelist[0].price1 * 100;
                                    returnItem.show = tempItem.goMenu || false;


                                    var itemTimeRangeList = timeRange.filter((trobj) => {
                                        return trobj.itemId == tempItem.itemId;
                                    })


                                    var haveDateFilter = false;
                                    if (itemTimeRangeList.length != 0) {
                                        var time = "";
                                        for (var i = 0; i < itemTimeRangeList.length; i++) {
                                            var validDate = true;
                                            var itrl = itemTimeRangeList[i];
                                            if (itrl.date != null) {
                                                haveDateFilter = true;
                                                if (itrl.date.indexOf(now.getDay() + 1) == -1) {
                                                    validDate = false;
                                                } else {
                                                    if (itrl.date.indexOf('excluding') != -1) {
                                                        var sd = specialDate.filter((sd) => {
                                                            return sd.specialDate.getFullYear() == now.getFullYear() && sd.specialDate.getMonth() == now.getMonth() && sd.specialDate.getDate() == now.getDate()
                                                        })
                                                        if (sd.length != 0) {
                                                            validDate = false;
                                                        }
                                                    }
                                                }
                                            }
                                            if (validDate) {
                                                time += i == 0 ? "" : ",";
                                                time += moment(itrl.timeStart).format("HH:mm") + '-' + moment(itrl.timeEnd).format("HH:mm");
                                            }
                                        }
                                        returnItem.time = time;
                                    }
                                    var imlist = itemModifier.filter((imObj) => {
                                        return imObj.itemCode == tempItem.code;
                                    })
                                    if (imlist.length != 0) {
                                        returnItem.modifiers = [];
                                        for (var i = 0; i < imlist.length; i++) {
                                            returnItem.modifiers.push(imlist[i].modifierGroupCode);
                                        }

                                    }

                                    var iolist = itemOption.filter((ioObj) => {
                                        return ioObj.itemCode == tempItem.code;
                                    })
                                    if (iolist.length != 0) {
                                        for (var i = 0; i < iolist.length; i++) {
                                            var io = iolist[i];
                                            io.items = [];
                                            var optionList = option.filter((oObj) => {
                                                return oObj.optionGroupCode == io.optionGroupCode;
                                            })
                                            if (optionList.length != 0) {
                                                for (var j = 0; j < optionList.length; j++) {

                                                    var optionItem = optionList[j];

                                                    var pricelist = pricebatch.filter(function (pbobj) {
                                                        return pbobj.itemId == optionItem.optionCode;
                                                    }).sort(function (a, b) { return a.activateDate < b.activateDate });
                                                    if (pricelist.length != 0) {
                                                        optionItem.unitprice = pricelist[0].price1 * 100;

                                                        optionItem.name = {};
                                                        optionItem.code = optionItem.optionCode;
                                                        optionItem.show = optionItem.ogoMenu && optionItem.oggoMenu;
                                                        optionItem.name[config.lang1] = optionItem.name1;
                                                        optionItem.name[config.lang2] = optionItem.name2;
                                                        optionItem.name[config.lang3] = optionItem.name3;


                                                        var imlist = itemModifier.filter((imObj) => {
                                                            return imObj.itemCode == optionItem.code;
                                                        })
                                                        if (imlist.length != 0) {
                                                            optionItem.modifiers = [];
                                                            for (var k = 0; k < imlist.length; k++) {
                                                                optionItem.modifiers.push(imlist[k].modifierGroupCode);
                                                            }

                                                        }


                                                        io.items.push(optionItem);
                                                    }
                                                }
                                                if (io.items.length != 0) {
                                                    if (!(returnItem.option)) {
                                                        returnItem.option = [];
                                                    }
                                                    returnItem.option.push(io);
                                                }
                                            }
                                        }
                                    }

                                    var slist = itemSubitem.filter((isObj) => {
                                        return isObj.itemCode == tempItem.code;
                                    })
                                    if (slist.length != 0) {
                                        returnItem.subitem = [];
                                        for (var i = 0; i < slist.length; i++) {
                                            var so = subitem.filter((sobj) => {
                                                return sobj.subitemCode == slist[i].subitemCode;
                                            })[0]
                                            //var pricelist = pricebatch.filter(function (pbobj) {
                                            //    return pbobj.itemId == so.subitemCode;
                                            //}).sort(function (a, b) { return a.activateDate < b.activateDate });
                                            //if (pricelist.length != 0) {

                                            //}
                                            so.unitprice = 0;
                                            so.name = {}
                                            so.name[config.lang1] = so.name1;
                                            so.name[config.lang2] = so.name2;
                                            so.name[config.lang3] = so.name3;
                                            so.code = so.subitemCode;
                                            returnItem.subitem.push(so);
                                        }

                                    }

                                    if (haveDateFilter && returnItem.time === "") {
                                        returnItem = null;
                                    }
                                }


                                _obj.compositeOptionGroup.optionGroupList.map(function (optionGroup, idx) {
                                    var itemCodeList = _obj.compositeOptionGroup.itemCodeList[idx].items;
                                    var itemList = _obj.compositeOptionGroup.itemList[idx].items;

                                    if (returnItem != null)
                                        if (itemCodeList.indexOf(returnItem.itemId) != -1) {
                                            var isExist = itemList.filter(function (item) {
                                                return item.itemId == returnItem.itemId;
                                            }).length;
                                            if (!isExist) itemList.push(returnItem);
                                        }
                                })



                            }
                        })
                    }
                    return returnItem;
                }).filter(function (fobj) {
                    return fobj != null;
                })
            }

            for (var i = 0; i < category.length; i++) {
                var cat = category[i];
                if (cat.parentCategory != 0) { continue; }
                var _c = {};
                _c.code = cat.code;
                _c.icon = cat.icon;
                _c.color = cat.color;
                _c.seq = cat.seq;
                _c.specialOffer = false;
                _c.memberOnly = false;
                if (cat.type != 0) {
                    _c.memberOnly = true;
                }
                _c.name = {};
                _c.name[config.lang1] = cat.name1;
                _c.name[config.lang2] = cat.name2;
                _c.name[config.lang3] = cat.name3;
                _c.name1 = cat.name1;
                _c.name2 = cat.name2;
                _c.name3 = cat.name3;
                _c.items = _filterItem(cat);

                _c.subCat = [];
                for (var j = 0; j < category.length; j++) {
                    var subCat = category[j];
                    if (cat.categoryId === subCat.parentCategory) {

                        var _sc = {};
                        _sc.code = subCat.code;
                        _sc.icon = subCat.icon;
                        _sc.color = subCat.color;
                        _sc.seq = subCat.seq;
                        _sc.specialOffer = false;
                        _sc.memberOnly = false;
                        if (subCat.type != 0) {
                            _sc.memberOnly = true;
                        }
                        _sc.name = {};
                        _sc.name[config.lang1] = subCat.name1;
                        _sc.name[config.lang2] = subCat.name2;
                        _sc.name[config.lang3] = subCat.name3;
                        _sc.name1 = subCat.name1;
                        _sc.name2 = subCat.name2;
                        _sc.name3 = subCat.name3;
                        _sc.items = _filterItem(subCat);


                        _sc.subCat = [];
                        if (_sc.items.length != 0) {
                            _c.subCat.push(_sc);
                        }

                    }
                }

                if (_c.items.length != 0 || _c.subCat.length != 0) {
                    _obj.category.push(_c);
                }
            }
            //console.log(modifier.length);
            for (var i = 0; i < modifier.length; i++) {
                var m = modifier[i];
                var _m = {};
                _m.name = {};
                _m.name[config.lang1] = m.name1;
                _m.name[config.lang2] = m.name2;
                _m.name[config.lang3] = m.name3;
                _m.namek = m.namek;
                _m.label = _m.name[config.currentLang]
                //_m.label = m.name1;
                _m.show = m.mgoMenu && m.mggoMenu;
                _m.code = m.modifierCode;
                _m.modifierGroupCode = m.modifierGroupCode;
                _obj.modifiers.push(_m);
            }

            for (var i = 0; i < kitchenMsg.length; i++) {
                var k = kitchenMsg[i];
                var _k = {};
                _k.name = {};
                _k.name[config.lang1] = k.name1;
                _k.name[config.lang2] = k.name2;
                _k.name[config.lang3] = k.name3;
                _k.namek = k.name1;
                //_k.label = k.name1;
                _k.label = _k.name[config.currentLang]
                _k.code = k.kitchenMsgCode;
                _obj.kitchenmsg.push(_k);
            }

            this.food = _obj;
            this.food.option = option;
            this.food.subitem = subitem;
            this.discount = discount;
            this.tender = tender;

            this.tender.forEach(function (t) {
                t.name = [];
                t.name[config.lang1] = t.tenderName1;
                t.name[config.lang2] = t.tenderName2;
                t.name[config.lang3] = t.tenderName3;
                t.tenderName1 = t.name[config.currentLang];
            })

            _obj.mifarePayment = config.mifarePayment ? true : false;
            _obj.fireCode = config.fireCode;
            _obj.takeAwayCollected = config.takeAwayCollected;

            report.setData({ item: this.Item, modifiers: this.food.modifiers, subitem: this.food.subitem, kitchenmsg: this.food.kitchenmsg, option: this.food.option, tender: this.tender, discount: this.discount, config: config.report });

            //console.log(this.discount);
            fs.writeFile("./public/data/food.json", JSON.stringify(_obj), function (err) {
                if (err) {
                    //console.log(err);
                } else {
                    ////console.log("The file was saved!");
                }
                if (callback) callback("OK");
            });
            fs.writeFile("./public/data/food_main.json", JSON.stringify(this.food), function (err) {
                if (err) {
                    //console.log(err);
                } else {
                    ////console.log("The file was saved!");
                }
            });
            ////console.log(_obj.category[1].items);

        }, 'parallel')
    }

    this.Login = function (data, callback) {
        var staffName = '';
        ////console.log(data);
        var query = { "sql": "select * from staff s inner join staffgroup sg on s.staffGroupCode = sg.staffGroupCode where username = @username ", "args": [{ "c": "username", "t": TYPES.VarChar, "v": data.u }] }
        this.DM.executeStatement(query, (err, result) => {
            ////console.log(result);
            var isLogin = false;
            var staffDetails = {};
            if (result.length != 0) {
                if (result[0].password == data.p || result[0].password == '-999') {
                    isLogin = true;
                    staffName = result[0].name;
                    staffDetails = result[0];
                }
            }
            //callback('{"success":' + isLogin + ',"staffName":"' + staffName + ',"staffDetail":'++}');
            callback({ success: isLogin, staffName: staffName, staffDetails: staffDetails });
        })
    }

    this.MasterpassMemberLogin = (args, callback) => {
        console.log(args);
        var _obj = {};
        var error = [
            { "code": 0, message: null },
            { "code": 1, message: "User is not found" }
        ]
        _obj.error = error[0];
        var query = {
            "sql": "SELECT * FROM [member] WHERE [memberId] = @memberId and [password] = @password;",
            "args": [
                { "c": "memberId", "t": TYPES.Int, "v": args.userName },
                { "c": "password", "t": TYPES.NVarChar, "v": args.password }
            ]

        }

        this.DM.executeStatement(query, (err, result) => {
            console.log('member', result);
            if (result[0] == undefined) {
                _obj.error = error[1];
                callback(JSON.stringify(_obj));
                return;
            }

            _obj.token = moment().format("YYYY-MM-DD HH:mm:ss");
            _obj.member = result[0];
            delete _obj.member.password;
            callback(JSON.stringify(_obj));
        });

    }

    this.MasterpassCheckout = (args, callback) => {
        var _obj = {};
        var error = [
            { code: 0, message: null },
            { code: 1, message: "Table is locked" },
            { code: 2, message: "Status is not available" }
        ]
        _obj.error = error[0];
        console.log(args);

        var tableObj = getTableCode(args.tableNo);

        if (lockTableList["L" + args.tableNo] != undefined) {
            _obj.error = error[1];
            callback(JSON.stringify(_obj));
            return;
        }

        var query = [
             {
                 "sql": "select * from tableOrder where tablecode = @tableCode and tableSubcode = @tableSubCode",
                 "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": tableObj.tableCode }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": tableObj.tableSubCode }]
             },
             {
                 "sql": "select * from tableOrderD where tableCode = @tableCode and tableSubcode = @tableSubCode order by round,seq, type",
                 "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": tableObj.tableCode }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": tableObj.tableSubCode }]
             },
             {
                 "sql": "select * from tablediscount where tablecode = @tableCode and tableSubcode = @tableSubCode",
                 "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": tableObj.tableCode }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": tableObj.tableSubCode }]
             }]

        this.DM.executeStatement(query, (err, result) => {


            ////console.log(result);
            if (result[0].length != 0) {
                ////console.log(result[0]);
                _obj = result[0][0];
                _obj.status = undefined;
            }
            _obj.printer = config.defaultPrinter;
            if (args.printer) {
                _obj.printer = args.printer;
            }
            //console.log(result[1]);
            sortDbItem(result[1]);

            if (result[1].length != 0) {
                _obj.items = [];
                var lastIsFreeCombo = false;
                var lastItemQty = 0;
                for (var i = 0; i < result[1].length; i++) {
                    var currentItem = result[1][i];
                    var items = [];
                    var items = getItemByItem(currentItem);
                    var name1, name2, name3;
                    name1 = name2 = name3 = "";

                    if (items.length > 0) {
                        var name1 = items[0].name1;
                        var name2 = items[0].name2;
                        var name3 = items[0].name3;
                        if (items[0].name != undefined) {
                            if (items[0].name[config.lang1] != undefined)
                                name1 = items[0].name[config.lang1];
                            if (items[0].name[config.lang2] != undefined)
                                name2 = items[0].name[config.lang2];
                            if (items[0].name[config.lang3] != undefined)
                                name3 = items[0].name[config.lang3];
                        }
                    }
                    if (currentItem.customName != "") {
                        name1 = name2 = name3 = currentItem.customName;
                    }

                    //if (name1 != "") {
                    //    var nameLenght = 25 - countDoubleByteChar(name1);
                    //    name1 = S(name1).padRight(nameLenght).s
                    //}
                    var itemTotalPrice = 0;

                    if (currentItem.type == "I" || currentItem.type == "T") {
                        lastItemQty = currentItem.qty;
                        itemTotalPrice = lastItemQty * currentItem.price;
                    } else {
                        itemTotalPrice = lastItemQty * currentItem.price * currentItem.qty;
                    }

                    //var itp = S("$" + itemTotalPrice.format(1, 3)).padLeft(10).s;
                    var itp = itemTotalPrice;
                    //var up = S("$" + currentItem.price.format(1, 3)).padLeft(10).s;
                    var up = currentItem.price;

                    if (currentItem.type == "I") {
                        if (itemTotalPrice == 0) {
                            lastIsFreeCombo = true;
                        } else {
                            lastIsFreeCombo = false;
                        }
                    }

                    if (currentItem.type != "K" && currentItem.type != "T" && !(currentItem.type == "I" && itemTotalPrice == 0 && !config.printZeroPriceItem) && !(lastIsFreeCombo && !config.printZeroPriceItem)) {
                        if (currentItem.itemId != 'O9999') {
                            _obj.items.push({ code: currentItem.itemId, type: currentItem.type, desc1: name1, desc2: name2, desc3: name3, qty: currentItem.qty, unitPrice: up, itemTotalPrice: itp, time: currentItem.time, voidIndex: currentItem.voidIndex, voidRemark: currentItem.voidRemark, staff: currentItem.staffId, index: currentItem.seq });
                        }
                    }
                }


                var totaldiscount = 0;
                _obj.discount = [];
                for (var i = 0; i < result[2].length; i++) {
                    var _o = result[2][i];

                    var discountMsg = _o.discountCode;
                    this.discount.forEach((d) => {
                        if (d.discountCode == _o.discountCode) {
                            discountMsg = d.discountName1;
                        }
                    })

                    var dvForPrint = "$" + _o.discountValue.format(1, 3);
                    if (_o.discountValue > 0) {
                        if (_o.discountType != 'servicecharge') {
                            dvForPrint = "-" + dvForPrint;
                        }
                    }
                    //_obj.discount.push({ "discountCode": discountMsg, "discountValue": S(dvForPrint).padLeft(10).s })
                    _obj.discount.push({ "discountCode": discountMsg, "discountValue": _o.discountValue })

                    totaldiscount += _o.discountValue;
                }
                var totalPrice = _obj.price + _obj.serviceCharge - totaldiscount;

                //var totalPrice = _obj.price + _obj.serviceCharge + _obj.remainings;

                totalPrice = Math.round(totalPrice);
                //_obj.totalPrice = "$" + totalPrice.format(1, 3);
                _obj.totalPrice = totalPrice;
                //_obj.price = S("$" + _obj.price.format(1, 3)).padLeft(10).s;
                _obj.price = _obj.price;
                //_obj.serviceCharge = S("$" + _obj.serviceCharge.format(1, 3)).padLeft(10).s;
                _obj.serviceCharge = _obj.serviceCharge;

                var priceWithServiceCharge = _obj.price + _obj.serviceCharge;

                var r = (priceWithServiceCharge % 1).toFixed(1);
                if (r > 0.4) {
                    r = (1 - r).toFixed(1);
                } else if (r != 0) {
                    r = '-' + r;
                }
                _obj.remainings = parseFloat(r);

            }

            if (args.action == "bill") {
                _obj.isPrintTwice = config.billPrintTwice;


                if (!config.PrintVoidItem) {
                    var filterVoidItemList = [];
                    if (typeof _obj.items === 'undefined') {
                        _obj.items = [];
                    }

                    _obj.items.forEach(function (food) {
                        if (food.type == "M" || food.type == "K") return true;
                        food.Tqty = food.qty;
                        if (food.type == "I" || food.type == "T") {
                            var tempVoidItem = {};
                            tempVoidItem.Package = food;
                            tempVoidItem.options = [];
                            filterVoidItemList.push(tempVoidItem);
                        }
                        else {
                            food.Tqty = filterVoidItemList[filterVoidItemList.length - 1].Package.qty;
                            filterVoidItemList[filterVoidItemList.length - 1].options.push(food);
                        }
                    });

                    filterVoidItemList.forEach(function (food) {
                        if (food.Package.voidIndex != '-1') {
                            var tempFood = filterVoidItemList.filter(function (voidItem) {
                                return voidItem.Package.index == food.Package.voidIndex;
                            })
                            tempFood[0].Package.qty = tempFood[0].Package.qty + food.Package.qty;
                            var tempItemPrice = tempFood[0].Package.qty * tempFood[0].Package.unitPrice.split('$')[1];

                            //tempFood[0].Package.itemTotalPrice = S("$" + tempItemPrice.format(1, 3)).padLeft(10).s
                            tempFood[0].Package.itemTotalPrice = tempItemPrice;

                            ////tempFood[0].Package.itemTotalPrice = tempFood[0].Package.itemTotalPrice - food.Package.itemTotalPrice;
                            tempFood[0].options.forEach(function (opts) {
                                //opts.qty = opts.qty + food.Package.qty;
                                opts.Tqty = opts.Tqty + food.Package.Tqty;
                                //tempItemPrice = opts.Tqty * opts.unitPrice.split('$')[1];
                                tempItemPrice = opts.Tqty * opts.unitPrice;
                                //opts.itemTotalPrice = S("$" + tempItemPrice.format(1, 3)).padLeft(10).s
                                opts.itemTotalPrice = tempItemPrice;
                            });
                        }
                    })

                    filterVoidItemList = filterVoidItemList.filter(function (food) {
                        return food.Package.qty > 0;
                    })

                    _obj.items = [];
                    filterVoidItemList.forEach(function (food) {
                        _obj.items.push(food.Package);
                        food.options.forEach(function (opts) {
                            _obj.items.push(opts);
                        })
                    })
                }


            }
            var orderid = moment().format("YYMMDDHHmmssSSSS");
            console.log("./printer/" + _obj.printer + "/" + args.action + "_" + orderid + ".json");
            callback(JSON.stringify(_obj));

            if (lockTableList["L" + args.tableNo] == undefined) {

                var timeout = setTimeout(function () {
                    clearTimeout(lockTableList["L" + args.tableNo].timeout)
                    delete lockTableList["L" + args.tableNo];
                    io.sockets.emit('releaseLockTable', args.tableNo);
                }, 10000);

                lockTableList["L" + args.tableNo] = { timeout: timeout };
                io.sockets.emit('activeLockTable', args.tableNo);
            } else {
                //clearInterval(lockTableList["L" + data.tableCode].timeout);
                //delete lockTableList["L" + data.tableCode];
            }

            //var writeFile = {}
            //extend(_obj, writeFile);
            //writeFile.items.forEach(function (item) {
            //    item.unitPrice = S("$" + item.unitPrice.format(1, 3)).padLeft(10).s;
            //    item.itemTotalPrice = S("$" + item.itemTotalPrice.format(1, 3)).padLeft(10).s;
            //});

            //writeFile.discount.forEach(function (dis) {
            //    dis.discountValue = S(dis.discountValue).padLeft(10).s
            //});

            //writeFile.totalPrice = "$" + writeFile.totalPrice.format(1, 3);
            //writeFile.price = S("$" + writeFile.price.format(1, 3)).padLeft(10).s;
            //writeFile.serviceCharge = S("$" + writeFile.serviceCharge.format(1, 3)).padLeft(10).s;

            //fs.writeFile("./printer/" + _obj.printer + "/" + args.action + "_" + orderid + ".json", JSON.stringify(_obj), function (err) {
            //});

            if (args.action == "bill") {
                this.DM.executeStatement({
                    "sql": "update tableorder set status = 3 where tablecode = @tableCode and tableSubCode = @tableSubCode",
                    args: [{ "c": "tableCode", "t": TYPES.VarChar, "v": tableObj.tableCode }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": tableObj.tableSubCode }]
                }, (err, result) => { })
                this.SetTableStatus({ "tableCode": tableObj.tableCode, "status": 3 })
            }
        }, 'parallel');
    }

    this.MasterpassCheckoutSuccess = (data, callback) => {

        var _obj = {};
        _obj.refNo = null;
        var error = [
            { code: 0, message: null },
            { code: 1, message: "Table is locked" },
            { code: 2, message: "Status is not available" },
            { code: 3, message: "Table is not exist!" }
        ]
        _obj.error = error[0];

        var tableObj = getTableCode(data.tableNo);

        //if (lockTableList["L" + data.tableNo] != undefined) {
        //    _obj.error = error[1];
        //    callback(JSON.stringify(_obj));
        //    return;
        //}


        var postData = {
            discountMethod: { $value: [] },
            payAmount: { $value: null },
            payMethod: { $value: [{ amount: null, method: "masterpass" }] },
            peopleNum: { $value: null },
            price: { $value: null },
            printer: { $value: undefined },
            refNo: { $value: {} },
            remainings: { $value: null },
            serviceCharge: { $value: null },
            tableNum: { $value: data.tableNo },
            till: { $value: null },
            user: { $value: null }
        }

        var query = {
            "sql": "select * from tableOrder where tablecode = @tableCode and tableSubcode = @tableSubCode",
            "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": tableObj.tableCode }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": tableObj.tableSubCode }]
        }

        this.DM.executeStatement(query, (err, result) => {

            var tableOrder = result[0];
            if (tableOrder == null || tableOrder == undefined) {
                _obj.error = error[3];
                callback(JSON.stringify(_obj));
                return;
            }
            var priceWithServiceCharge = tableOrder.price + tableOrder.serviceCharge;

            var r = (priceWithServiceCharge % 1).toFixed(1);
            if (r > 0.4) {
                r = (1 - r).toFixed(1);
            } else if (r != 0) {
                r = '-' + r;
            }

            r = parseFloat(r);
            //console.log('tableOrder', tableOrder);
            //console.log(tableOrder.price + tableOrder.serviceCharge);

            postData.payAmount.$value = tableOrder.price + tableOrder.serviceCharge + r;
            postData.payMethod.$value[0].amount = tableOrder.price + tableOrder.serviceCharge + r;
            postData.peopleNum.$value = tableOrder.pax;
            postData.price.$value = tableOrder.price;
            postData.remainings.$value = r;
            postData.serviceCharge.$value = tableOrder.serviceCharge;

            var action = {};
            action.msg = "masterpass";
            action.action = "addMessage";
            action.tableno = data.tableNo;
            action.itemCode = null;
            action.dish = null;
            action.token = null;

            RAS.saveMessage(action);


            RAS.BillTableOrder(postData, function (result) {
                console.log('BillTableOrder', result);
                _obj.refNo = result.refNo;



                var query = [
                            {
                                "sql": "select * from [transaction] where refNo=@refNo; ",
                                "args": [{ "c": "refNo", "t": TYPES.VarChar, "v": _obj.refNo }]
                            },
                            {
                                "sql": "select * from [transactionD] where refNo=@refNo",
                                "args": [{ "c": "refNo", "t": TYPES.VarChar, "v": _obj.refNo }]
                            },
                            {
                                "sql": "select * from [tenderDiscount] where refNo=@refNo",
                                "args": [{ "c": "refNo", "t": TYPES.VarChar, "v": _obj.refNo }]
                            },
                            {
                                "sql": "select * from [tenderPayment] where refNo=@refNo",
                                "args": [{ "c": "refNo", "t": TYPES.VarChar, "v": _obj.refNo }]
                            }
                ]

                RAS.DM.executeStatement(query, (err, result) => {

                    ////console.log(result);
                    if (result[0].length != 0) {
                        ////console.log(result[0]);
                        //_obj = result[0][0];
                        //_obj.status = undefined;
                        _obj.payment = { price: result[0][0].price, serviceCharge: result[0][0].serviceCharge, remainings: result[0][0].remainings, paymentValue: result[3][0].paymentValue, paymentDate: result[3][0].paymentDate, remark: result[3][0].remark };

                    }

                    //console.log(result[1]);
                    sortDbItem(result[1]);

                    if (result[1].length != 0) {
                        _obj.items = [];
                        var lastIsFreeCombo = false;
                        var lastItemQty = 0;
                        for (var i = 0; i < result[1].length; i++) {
                            var currentItem = result[1][i];
                            var items = [];
                            var items = getItemByItem(currentItem);
                            var name1, name2, name3;
                            name1 = name2 = name3 = "";

                            if (items.length > 0) {
                                var name1 = items[0].name1;
                                var name2 = items[0].name2;
                                var name3 = items[0].name3;
                                if (items[0].name != undefined) {
                                    if (items[0].name[config.lang1] != undefined)
                                        name1 = items[0].name[config.lang1];
                                    if (items[0].name[config.lang2] != undefined)
                                        name2 = items[0].name[config.lang2];
                                    if (items[0].name[config.lang3] != undefined)
                                        name3 = items[0].name[config.lang3];
                                }
                            }
                            if (currentItem.customName != "") {
                                name1 = name2 = name3 = currentItem.customName;
                            }

                            //if (name1 != "") {
                            //    var nameLenght = 25 - countDoubleByteChar(name1);
                            //    name1 = S(name1).padRight(nameLenght).s
                            //}
                            var itemTotalPrice = 0;

                            if (currentItem.type == "I" || currentItem.type == "T") {
                                lastItemQty = currentItem.qty;
                                itemTotalPrice = lastItemQty * currentItem.price;
                            } else {
                                itemTotalPrice = lastItemQty * currentItem.price * currentItem.qty;
                            }

                            //var itp = S("$" + itemTotalPrice.format(1, 3)).padLeft(10).s;
                            var itp = itemTotalPrice;
                            //var up = S("$" + currentItem.price.format(1, 3)).padLeft(10).s;
                            var up = currentItem.price;

                            if (currentItem.type == "I") {
                                if (itemTotalPrice == 0) {
                                    lastIsFreeCombo = true;
                                } else {
                                    lastIsFreeCombo = false;
                                }
                            }

                            if (currentItem.type != "K" && currentItem.type != "T" && !(currentItem.type == "I" && itemTotalPrice == 0 && !config.printZeroPriceItem) && !(lastIsFreeCombo && !config.printZeroPriceItem)) {
                                if (currentItem.itemId != 'O9999') {
                                    _obj.items.push({ code: currentItem.itemId, type: currentItem.type, desc1: name1, desc2: name2, desc3: name3, qty: currentItem.qty, unitPrice: up, itemTotalPrice: itp, time: currentItem.time, voidIndex: currentItem.voidIndex, voidRemark: currentItem.voidRemark, staff: currentItem.staffId, index: currentItem.seq });
                                }
                            }
                        }


                        var totaldiscount = 0;
                        _obj.discount = [];
                        for (var i = 0; i < result[2].length; i++) {
                            var _o = result[2][i];

                            var discountMsg = _o.discountCode;
                            this.discount.forEach((d) => {
                                if (d.discountCode == _o.discountCode) {
                                    discountMsg = d.discountName1;
                                }
                            })

                            var dvForPrint = "$" + _o.discountValue.format(1, 3);
                            if (_o.discountValue > 0) {
                                dvForPrint = "-" + dvForPrint;
                            }
                            //_obj.discount.push({ "discountCode": discountMsg, "discountValue": S(dvForPrint).padLeft(10).s })
                            _obj.discount.push({ "discountCode": discountMsg, "discountValue": _o.discountValue })

                            totaldiscount += _o.discountValue;
                        }

                        //var totalPrice = _obj.price + _obj.serviceCharge - totaldiscount;

                        //var totalPrice = _obj.price + _obj.serviceCharge + _obj.remainings;

                        //totalPrice = Math.round(totalPrice);
                        //_obj.totalPrice = "$" + totalPrice.format(1, 3);
                        //_obj.totalPrice = totalPrice;
                        //_obj.price = S("$" + _obj.price.format(1, 3)).padLeft(10).s;
                        //_obj.price = _obj.price;
                        //_obj.serviceCharge = S("$" + _obj.serviceCharge.format(1, 3)).padLeft(10).s;
                        //_obj.serviceCharge = _obj.serviceCharge;

                        //var priceWithServiceCharge = _obj.price + _obj.serviceCharge;

                        //var r = (priceWithServiceCharge % 1).toFixed(1);
                        //if (r > 0.4) {
                        //    r = (1 - r).toFixed(1);
                        //} else if (r != 0) {
                        //    r = '-' + r;
                        //}
                        //_obj.remainings = parseFloat(r);

                    }

                    callback(JSON.stringify(_obj));

                }, 'parallel');



            });
            //console.log(JSON.stringify(postData));
            //postData.tableNum.$value = tableOrder.serviceCharge;




        })
    }


    this.Print = (args, callback) => {
        console.log(2994, JSON.stringify(args));
        console.log('print method');
        var tableObj = getTableCode(args.tableNo);
        //var tableCode = args.tableNo == null ? "" : args.tableNo;
        //var tableSubcode = args.tableSubcode == null ? "" : args.tableSubcode;
        //var tableCode = tableObj.tableCode;
        //var tableSubcode = tableObj.tableSubcode;
        //return;

        var query = [{ "sql": "delete tablediscount where tablecode = @tableCode and tableSubcode = @tableSubCode", "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": tableObj.tableCode }, { "c": "tableSubcode", "t": TYPES.VarChar, "v": tableObj.tableSubCode }] }];

        if (args.discount) {
            for (var i = 0; i < args.discount.length; i++) {
                var sqlargs = [];
                sqlargs.push({ "c": "tableCode", "t": TYPES.VarChar, "v": tableObj.tableCode })
                sqlargs.push({ "c": "tableSubCode", "t": TYPES.VarChar, "v": tableObj.tableSubCode })

                sqlargs.push({ "c": "staffId", "t": TYPES.VarChar, "v": args.user })

                sqlargs.push({ "c": "discountType", "t": TYPES.VarChar, "v": args.discount[i].type })
                sqlargs.push({ "c": "discountValue", "t": TYPES.Decimal, "v": parseFloat(args.discount[i].price), "o": { "scale": 2 } })
                sqlargs.push({ "c": "discountCode", "t": TYPES.VarChar, "v": args.discount[i].coupon_alias })
                sqlargs.push({ "c": "discountAmount", "t": TYPES.Decimal, "v": parseFloat(args.discount[i].amount), "o": { "scale": 2 } })

                query.push({ "sql": "insert into tablediscount ( discountCode,discountType,discountValue,staffId,discountAmount,discountDate,tableCode,tableSubCode) values (@discountCode,@discountType,@discountValue,@staffId,@discountAmount,getDate(),@tableCode,@tableSubCode)", "args": sqlargs });
            }
        }


        this.DM.executeStatement(query, (err, result) => {
            query = [
                {
                    "sql": "select * from tableOrder where tablecode = @tableCode and tableSubcode = @tableSubCode",
                    "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": tableObj.tableCode }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": tableObj.tableSubCode }]
                },
                {
                    "sql": "select * from tableOrderD where tableCode = @tableCode and tableSubcode = @tableSubCode order by round,seq, type",
                    "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": tableObj.tableCode }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": tableObj.tableSubCode }]
                },
                {
                    "sql": "select * from tablediscount where tablecode = @tableCode and tableSubcode = @tableSubCode",
                    "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": tableObj.tableCode }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": tableObj.tableSubCode }]
                },
                {
                    "sql": "select * from staff",
                    "args": []
                }]

            this.DM.executeStatement(query, (err, result) => {

                var _obj = {};
                ////console.log(result);
                if (result[0].length != 0) {
                    ////console.log(result[0]);
                    _obj = result[0][0];
                    //printer split table (tableCode)
                    _obj.tableCode = tableObj.tableCode;
                    //if (tableObj.tableSubCode != "") _obj.tableCode += "_" + tableObj.tableSubCode;

                    _obj.status = undefined;
                }
                _obj.printer = config.defaultPrinter;
                if (args.printer) {
                    _obj.printer = args.printer;
                }
                //console.log(result[1]);
                sortDbItem(result[1]);

                if (result[1].length != 0) {
                    _obj.items = [];
                    var lastIsFreeCombo = false;
                    var lastItemQty = 0;
                    for (var i = 0; i < result[1].length; i++) {
                        var currentItem = result[1][i];
                        var items = [];
                        var items = getItemByItem(currentItem);
                        var name1, name2, name3;
                        name1 = name2 = name3 = "";

                        if (items.length > 0) {
                            var name1 = items[0].name1;
                            var name2 = items[0].name2;
                            var name3 = items[0].name3;
                            if (items[0].name != undefined) {
                                if (items[0].name[config.lang1] != undefined)
                                    name1 = items[0].name[config.lang1];
                                if (items[0].name[config.lang2] != undefined)
                                    name2 = items[0].name[config.lang2];
                                if (items[0].name[config.lang3] != undefined)
                                    name3 = items[0].name[config.lang3];
                            }
                        }
                        if (currentItem.customName != "") {
                            name1 = name2 = name3 = currentItem.customName;
                        }

                        //if (name1 != "") {
                        //    var nameLenght = 25 - countDoubleByteChar(name1);
                        //    name1 = S(name1).padRight(nameLenght).s
                        //}
                        var itemTotalPrice = 0;

                        if (currentItem.type == "I" || currentItem.type == "T") {
                            lastItemQty = currentItem.qty;
                            itemTotalPrice = lastItemQty * currentItem.price;
                        } else {
                            itemTotalPrice = lastItemQty * currentItem.price * currentItem.qty;
                        }

                        var itp = S("$" + itemTotalPrice.format(1, 3)).padLeft(10).s;
                        var up = S("$" + currentItem.price.format(1, 3)).padLeft(10).s;

                        if (currentItem.type == "I") {
                            if (itemTotalPrice == 0) {
                                lastIsFreeCombo = true;
                            } else {
                                lastIsFreeCombo = false;
                            }
                        }
                        if (currentItem.type != "K" && currentItem.type != "T" && !(currentItem.type == "I" && itemTotalPrice == 0 && !config.printZeroPriceItem) && !(lastIsFreeCombo && !config.printZeroPriceItem)) {
                            if (currentItem.itemId != 'O9999') {
                                _obj.items.push({ code: currentItem.itemId, type: currentItem.type, desc1: name1, desc2: name2, desc3: name3, qty: currentItem.qty, unitPrice: up, itemTotalPrice: itp, time: currentItem.time, voidIndex: currentItem.voidIndex, voidRemark: currentItem.voidRemark, staff: currentItem.staffId, index: currentItem.seq });
                            }
                        }
                    }


                    var totaldiscount = 0;
                    _obj.discount = [];
                    for (var i = 0; i < result[2].length; i++) {
                        var _o = result[2][i];

                        var discountMsg = _o.discountCode;
                        this.discount.forEach((d) => {
                            if (d.discountCode == _o.discountCode) {
                                discountMsg = d.discountName1;
                            }
                        })

                        var dvForPrint = "$" + _o.discountValue.format(1, 3);
                        if (_o.discountCode == 'servicecharge') {
                            dvForPrint = "$" + (_o.discountValue * -1).format(1, 3);
                        }
                        if (_o.discountValue > 0) {
                            dvForPrint = "-" + dvForPrint;
                        }


                        _obj.discount.push({ "discountCode": discountMsg, "discountValue": S(dvForPrint).padLeft(10).s })
                        totaldiscount += _o.discountValue;
                    }
                    var totalPrice = _obj.price + _obj.serviceCharge - totaldiscount;

                    //var totalPrice = _obj.price + _obj.serviceCharge + _obj.remainings;

                    totalPrice = Math.round(totalPrice);
                    _obj.totalPrice = "$" + totalPrice.format(1, 3);
                    _obj.price = S("$" + _obj.price.format(1, 3)).padLeft(10).s;
                    _obj.serviceCharge = S("$" + _obj.serviceCharge.format(1, 3)).padLeft(10).s;

                    _obj.user = args.user;


                    for (var i = 0; i < result[3].length; i++) {
                        if (result[3][i].staffId == args.user) {
                            _obj.user = result[3][i].name;
                            break;
                        }
                    }


                }

                if (args.action == "bill") {
                    _obj.isPrintTwice = config.billPrintTwice;


                    if (!config.PrintVoidItem) {
                        var filterVoidItemList = [];
                        if (typeof _obj.items === 'undefined') {
                            _obj.items = [];
                        }

                        console.log('test ==================')
                        _obj.items.forEach(function (food) {
                            console.log(food);

                            if (food.type == "M" || food.type == "K") return true;
                            food.Tqty = food.qty;
                            if (food.type == "I" || food.type == "T") {
                                var tempVoidItem = {};
                                tempVoidItem.Package = food;
                                tempVoidItem.options = [];
                                filterVoidItemList.push(tempVoidItem);
                            }
                            else {
                                food.Tqty = filterVoidItemList[filterVoidItemList.length - 1].Package.qty;
                                filterVoidItemList[filterVoidItemList.length - 1].options.push(food);
                            }
                        });

                        filterVoidItemList.forEach(function (food) {
                            if (food.Package.voidIndex != '-1') {
                                var tempFood = filterVoidItemList.filter(function (voidItem) {
                                    return voidItem.Package.index == food.Package.voidIndex;
                                })
                                tempFood[0].Package.qty = tempFood[0].Package.qty + food.Package.qty;
                                var tempItemPrice = tempFood[0].Package.qty * tempFood[0].Package.unitPrice.split('$')[1];
                                tempFood[0].Package.itemTotalPrice = S("$" + tempItemPrice.format(1, 3)).padLeft(10).s
                                //tempFood[0].Package.itemTotalPrice = tempFood[0].Package.itemTotalPrice - food.Package.itemTotalPrice;
                                tempFood[0].options.forEach(function (opts) {
                                    //opts.qty = opts.qty + food.Package.qty;
                                    opts.Tqty = opts.Tqty + food.Package.Tqty;
                                    tempItemPrice = opts.Tqty * opts.unitPrice.split('$')[1];
                                    opts.itemTotalPrice = S("$" + tempItemPrice.format(1, 3)).padLeft(10).s
                                });
                            }
                        })

                        filterVoidItemList = filterVoidItemList.filter(function (food) {
                            return food.Package.qty > 0;
                        })

                        _obj.items = [];
                        filterVoidItemList.forEach(function (food) {
                            _obj.items.push(food.Package);
                            food.options.forEach(function (opts) {
                                _obj.items.push(opts);
                            })
                        })
                    }


                }
                var orderid = moment().format("YYMMDDHHmmssSSSS");
                console.log("./printer/" + _obj.printer + "/" + args.action + "_" + orderid + ".json");
                console.log(JSON.stringify(_obj));
                /*fs.writeFile("./printer/" + _obj.printer + "/" + args.action + "_" + orderid + ".json", JSON.stringify(_obj), function (err) {

                });*/

                RAS.writeFile("./printer/" + _obj.printer + "/" + args.action + "_" + orderid, JSON.stringify(_obj), function (err) {

                });
                //fs.writeFile("./printer/" + args.action + "/" + orderid + ".json", JSON.stringify(_obj), function (err) {
                //    if (err) {
                //        //console.log(err);
                //    } else {
                //        ////console.log("The file was saved!");
                //    }
                //});
                if (args.action == "bill") {
                    this.DM.executeStatement({
                        "sql": "update tableorder set status = 3 where tablecode = @tableCode and tableSubCode = @tableSubCode",
                        args: [{ "c": "tableCode", "t": TYPES.VarChar, "v": tableObj.tableCode }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": tableObj.tableSubCode }]
                    }, (err, result) => { })
                    this.SetTableStatus({ "tableCode": tableObj.tableCode, "status": 3 })
                }
            }, 'parallel')
        }, 'series');
    }

    this.LoadTableSchema = function (data, callback) {
        var sql = new Array();
        var that = this;
        sql.push({ "sql": "select * from diningTable", "args": null });
        sql.push({ "sql": "select * from shopZone", "args": null });
        sql.push({ "sql": "select * from tableOrder", "args": null });
        if (config.fireDineInMode && config.fireCode) {
            sql.push({
                //"sql": "select * from tableOrderD where type = 'I' or (type = 'K' and itemId = @fireCode)",
                "sql": "select tableOrderD.* from tableOrderD left join item on tableOrderD.itemId = item.itemId where (tableOrderD.type = 'I' and item.holdKitchenPrinter = 1) or (tableOrderD.type = 'K' and tableOrderD.itemId = @fireCode) order by tableCode, type, seq",
                "args": [
                  { "c": "fireCode", "t": TYPES.VarChar, "v": config.fireCode }
                ]
            });
        }
        this.DM.executeStatement(sql, function (err, _obj) {
            ////console.log(JSON.stringify(_obj));
            that.Table = _obj[0];
            that.Scene = _obj[1];
            _obj[2].forEach(function (tableOrder) {
                that.Table
                    .filter(function (x) { return x.name == tableOrder.tableCode; })
                    .forEach(function (y) {
                        if (y.buffetStatus == null && tableOrder.buffetStatus == null) {
                            y.buffetStatus = 0;
                        }
                        else if (y.buffetStatus == null || (y.buffetStatus < tableOrder.buffetStatus)) {
                            y.buffetStatus = tableOrder.buffetStatus
                        }
                    });
            });
            var obj = { scene: [] };
            ////console.log(JSON.stringify(_obj[1]));
            for (var i = 0 ; i < that.Scene.length; i++) {
                var o = {};
                o.id = that.Scene[i].shopZoneId;
                o.name = that.Scene[i].name;
                o.layout = that.Scene[i].floorPlan;
                o.memberObject = [];
                var selectedMember = that.Table.filter(function (entry) { return entry.shopZoneId == that.Scene[i].shopZoneId });
                for (var j = 0 ; j < selectedMember.length; j++) {
                    var sm = selectedMember[j];

                    var status = sm.status;
                    //console.log("1:"+status);
                    var selectedOrder = _obj[2].filter(function (entry) { return entry.tableCode == sm.name });
                    if (selectedOrder.length > 1) {
                        selectedOrder.forEach((so) => {
                            if (sm.status > so.status && so.status != undefined) {
                                status = so.status;
                            }
                        })
                    }
                    //console.log(sm.name);
                    if (config.fireDineInMode && config.fireCode && status <= 2 && status > 0) {
                        //console.log("1:" + status);
                        var totalItem = 0, totalFire = 0;
                        _obj[3]
                            .filter(function (d) { return d.tableCode == sm.name && d.type == "I" && d.qty > 0; })
                            .forEach(function (d) {
                                // check voidIndex
                                if (!_obj[3].filter(function (f) { return f.tableCode == sm.name && f.voidIndex == d.seq; }).length) {
                                    totalItem++;
                                    // check unique fireCode option
                                    if (_obj[3].filter(function (f) { return f.tableCode == sm.name && f.seq == d.seq && f.type == "K" && f.itemId == config.fireCode; }).length) {
                                        totalFire++;
                                    }
                                }
                            });
                        //console.log("2:", status, totalFire, totalItem);
                        if (totalFire >= totalItem) {
                            status = 5;
                        }
                    }
                    //console.log("3:" + status);
                    sm.buffetStatus = sm.buffetStatus == null ? 0 : sm.buffetStatus;
                    o.memberObject.push({ uid: sm.uid, label: sm.name, type: 'table', zone: sm.shopZoneId, shape: sm.shape, size: sm.size, pos_X: sm.x, pos_Y: sm.y, scale_X: sm.scaleX, scale_Y: sm.scaleY, angle_A: sm.angleA, angle_B: sm.angleB, status: status, buffetStatus: sm.buffetStatus });
                }
                obj.scene.push(o);
            }
            console.log(JSON.stringify(obj));

            if (callback) {
                callback(err, obj);
            }
            //io.sockets.emit('refreshTable', JSON.stringify(obj));
            //fs.writeFile("./public/files/so.json", JSON.stringify(obj), function (err) {
            //    if (err) {
            //        //console.log(err);
            //    } else {
            //        ////console.log("The file was saved!");
            //    }
            //});
        })
    }

    function getItemByItem(currentItem) {
        var items = [];
        //console.log("TEST", currentItem);
        if (currentItem.type == "I" || currentItem.type == "T") {
            items = RAS.Item.filter(function (entry) {
                return entry.code == currentItem.itemId;
            });
        } else if (currentItem.type == "M") {
            items = RAS.food.modifiers.filter(function (entry) {
                return entry.code == currentItem.itemId;
            });
        } else if (currentItem.type == "S") {
            items = RAS.food.subitem.filter(function (entry) {
                return entry.code == currentItem.itemId;
            });
        } else if (currentItem.type == "K") {
            items = RAS.food.kitchenmsg.filter(function (entry) {
                return entry.code == currentItem.itemId;
            });
        } else if (currentItem.type == "O") {
            items = RAS.food.option.filter(function (entry) {
                return entry.code == currentItem.itemId;
            });
            if (items.length == 0 && currentItem.itemId == 'O9999') {
                items[0] = { name: {} };
                items[0].name[config.lang1] = '未選擇菜單';
                items[0].name[config.lang2] = 'unselect Option';
                items[0].name[config.lang3] = 'unselect Option';
                items[0].namek = 'unselect Option';
                items[0].printer = '';
            }
        }
        return items;
    }

    this.AdminLoadTableOrder = (args, callback) => {
        console.log('lockTableList 1', lockTableList)
        console.log(args.tableNum.$value, args.tableMode);
        if (lockTableList["L" + args.tableNum.$value] != undefined && (args.tableMode == "eatWriteOrder" || args.tableMode == "kitchenMessage" || args.tableMode == "printBill" || args.tableMode == "splitTable" || args.tableMode == "billing")) {
            callback({ result: "Locked", clientIp: lockTableList["L" + args.tableNum.$value].clientIp });
        }
        //var callbackObj = {};
        var tableObj = getTableCode(args.tableNum.$value);
        console.log(tableObj);
        var _o = [{
            "sql": "select * from tableOrder where tableCode = @tableCode  order by tablesubcode ", "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": tableObj.tableCode },
                { "c": "tableSubCode", "t": TYPES.VarChar, "v": tableObj.tableSubCode }]
        },
        {
            "sql": "select * from tableOrderD where tableCode = @tableCode order by tablesubcode, seq,type, time", "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": tableObj.tableCode },
                  { "c": "tableSubCode", "t": TYPES.VarChar, "v": tableObj.tableSubCode }]
        }
        ,
                {
                    "sql": "select * from tablediscount where tablecode = @tableCode ",
                    "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": args.tableNum.$value }]
                }]
        if (args.exact != undefined) {
            if (args.exact.$value) {
                _o = [{
                    "sql": "select * from tableOrder where tableCode = @tableCode and tableSubCode = @tableSubCode ", "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": tableObj.tableCode },
                        { "c": "tableSubCode", "t": TYPES.VarChar, "v": tableObj.tableSubCode }]
                },
                {
                    "sql": "select * from tableOrderD where tableCode = @tableCode and tableSubCode = @tableSubCode order by seq,type, time", "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": tableObj.tableCode },
                          { "c": "tableSubCode", "t": TYPES.VarChar, "v": tableObj.tableSubCode }]
                }
                ,
                {
                    "sql": "select * from tablediscount where tablecode = @tableCode ",
                    "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": args.tableNum.$value }]
                }]
            }
        }
        /*var _o = [{
            "sql": "select * from tableOrder where tableCode = @tableCode and tableSubCode = @tableSubCode ", "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": tableObj.tableCode },
                { "c": "tableSubCode", "t": TYPES.VarChar, "v": tableObj.tableSubCode }]
        },
        {
            "sql": "select * from tableOrderD where tableCode = @tableCode and tableSubCode = @tableSubCode order by round,seq,type", "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": args.tableNum.$value },
                  { "c": "tableSubCode", "t": TYPES.VarChar, "v": tableObj.tableSubCode }]
        }
        ,
                {
                    "sql": "select * from tablediscount where tablecode = @tableCode ",
                    "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": args.tableNum.$value }]
                }]*/
        this.DM.executeStatement(_o, (err, tableOrders) => {
            //console.log(tableOrders);
            var orders = [];
            if (tableOrders[0].length != 0) {
                for (var j = 0; j < tableOrders[0].length ; j++) {
                    var tableOrder = tableOrders[0][j];
                    var order = { 'version': '1.0', 'header': { 'orderId': '', 'tableNum': '', 'peopleNum': 0, 'member': '', 'memberName': '', 'memberMsg': '', 'lastOrder': '', 'lastOrderWarning': '', 'leaveTime': '', 'status': 0 }, 'item': [], 'itemCount': 0, 'discount': [] };
                    order.header.tableNum = tableOrder.tableCode;
                    if (tableOrder.tableSubCode != "") {
                        order.header.tableNum = tableOrder.tableCode + "_" + tableOrder.tableSubCode;
                    }
                    order.header.peopleNum = tableOrder.pax;
                    order.header.orderId = "_";
                    order.header.lastOrder = tableOrder.lastOrder == null ? '' : moment(tableOrder.lastOrder).format("HH:mm:ss");
                    order.header.lastOrderWarning = tableOrder.lastOrderWarning == null ? '' : moment(tableOrder.lastOrderWarning).format("HH:mm:ss");
                    order.header.leaveTime = tableOrder.leaveTime == null ? '' : moment(tableOrder.leaveTime).format("HH:mm:ss");
                    order.header.status = tableOrder.status;
                    order.header.cardId = tableOrder.cardId;

                    if (!(order.header.lastOrder != "" && order.header.lastOrderWarning != "" && order.header.leaveTime != "")) {
                        order.header.lastOrder = "";
                        order.header.lastOrderWarning = "";
                        order.header.leaveTime = "";
                    }
                    var contentObj = [];

                    console.log('before', tableOrders[1]);

                    //var map = ["I", "O", "S", "T", "M", "K"];
                    //tableOrders[1].sort((a, b) => {
                    //    if (a.seq < b.seq) return -1;
                    //    if (a.seq === b.seq) {
                    //        if (map.indexOf(a.type) < map.indexOf(b.type)) return -1;
                    //        else if (map.indexOf(a.type) > map.indexOf(b.type)) return 1;
                    //        else return moment(a.time).format('HHmmss') - moment(b.time).format('HHmmss');
                    //    }
                    //    if (a.seq > b.seq) return 1;
                    //});
                    sortDbItem(tableOrders[1]);

                    console.log('after 2', tableOrders[1]);


                    for (var k = 0; k < tableOrders[1].length ; k++) {
                        var currentItem = tableOrders[1][k];
                        if (tableOrder.tableSubCode == currentItem.tableSubCode) {
                            var name1, name2, name3;
                            var items = getItemByItem(currentItem);
                            name1 = name2 = name3 = "";

                            if (items.length > 0) {
                                //console.log(items[0])
                                var name1 = items[0].name1;
                                var name2 = items[0].name2;
                                var name3 = items[0].name3;

                                if (items[0].name != undefined) {
                                    if (items[0].name[config.lang1] != undefined)
                                        name1 = items[0].name[config.lang1];
                                    if (items[0].name[config.lang2] != undefined)
                                        name2 = items[0].name[config.lang2];
                                    if (items[0].name[config.lang3] != undefined)
                                        name3 = items[0].name[config.lang3];
                                }
                            }
                            if (currentItem.customName != "") {
                                name1 = name2 = name3 = currentItem.customName;
                            }

                            if (currentItem.type == "T") {
                                var _lastOrder = items[0].namek.split(',');
                                if (_lastOrder.length > 0) {
                                    order.header.lastOrderWarning = _lastOrder[1];
                                    order.header.lastOrder = _lastOrder[0];
                                }
                            }
                            var t = moment(currentItem.time).format("HH:mm:ss");
                            contentObj.push({
                                code: currentItem.itemId,
                                optionGroupCode: currentItem.optionGroupCode == null ? '' : currentItem.optionGroupCode,
                                type: currentItem.type,
                                desc1: name1,
                                desc2: name2,
                                desc3: name3,
                                qty: currentItem.qty,
                                unitPrice: currentItem.price,
                                time: t,
                                voidIndex: currentItem.voidIndex,
                                voidRemark: currentItem.voidRemark,
                                staff: currentItem.staffId,
                                index: currentItem.seq,
                                serviceCharge: currentItem.serviceCharge,
                                customName: currentItem.customName
                            });
                        }
                    }

                    for (var i = 0; i < tableOrders[2].length; i++) {
                        var currentItem = tableOrders[2][i];
                        order.discount.push({ "discountCode": currentItem.discountCode, "discountType": currentItem.discountType, "discountValue": currentItem.discountValue, "discountAmount": currentItem.discountAmount });
                    }


                    order.item = contentObj;
                    order.itemCount = contentObj.length;
                    orders.push(order);
                }

                var callbackResult = "OK";
                var lockedTableNo = "";
                if (args.tableMode == "splitTable") {
                    var locks = Object.keys(lockTableList);
                    locks.forEach(function (templ) {
                        if (templ.split('_')[0] == "L" + args.tableNum.$value) {
                            callbackResult = "Locked";
                            lockedTableNo = templ.split('L')[1];
                            return false;
                        }
                    });

                }

                //console.log(orders);
                callbackObj = { result: callbackResult, timestamp: new Date().getTime(), taskID: 1, note: "", order: orders, suspendItems: [], lockedTableNo: lockedTableNo }
                console.log("======================================");
                console.log(JSON.stringify(callbackObj));
                var builder = new xml2js.Builder();
                var xml = builder.buildObject(callbackObj);
                //console.log(xml);
                //console.log('callback adminloadtableorder');
                callback(callbackObj);

            }
            else {
                var order = { 'version': '1.0', 'header': { 'orderId': '', 'tableNum': '', 'peopleNum': 0, 'member': '', 'memberName': '', 'memberMsg': '' }, 'item': [], 'itemCount': 0 };

                callbackObj = { result: "OK", timestamp: new Date().getTime(), taskID: 1, note: "", order: [order], suspendItems: {} }
                //callbackObj = { result: "OK", timestamp: new Date().getTime(), taskID: 1, note: "", order: orders, suspendItems: [] }
                var builder = new xml2js.Builder();
                var xml = builder.buildObject(callbackObj);
                ////console.log(xml);
                callback(callbackObj);
            }
        })
    }

    this.LoadDiscount = (args, callback) => {
        callback(this.discount);
    }

    this.LoadTender = (args, callback) => {
        callback(this.tender);
    }

    this.LoadItemQty = (args, callback) => {
        //callback(JSON.stringify([]));
        this.GetOutOfStock(callback)
    }

    this.ResetItemQty = (args, callback) => {
        //console.log('ResetItemQty');
        this.DM.executeStatement({ "sql": "delete itemControl", "args": [] }, (err, result) => {
            //console.log(callback);
            //console.log('itemControl', JSON.stringify(result));
            this.GetOutOfStock((err, outOfStock) => {
                //console.log('GetOutOfStock');
                callback(outOfStock);
                io.sockets.emit('refreshItemQty', JSON.stringify(outOfStock));
                var _o = { "action": "refreshItemQty", "outOfStock": outOfStock }
                mqttClient.publish("wo/gm/all", JSON.stringify(_o));
            })

        });
        //this.DataManager.executeOracleStatment("delete xf_rms_item_control", function (err, result) {
        //    that.GetOutOfStock(function (err, result) {
        //        io.sockets.emit('refreshItemQty', JSON.stringify(result));
        //    })
        //}, []);
    }

    function getServiceCharge(orderItem) {
        console.log(orderItem)
        // var itemType = orderItem.type[0];
        var itemCode = orderItem.code[0];
        var totalPrice = 0;
        var totalPriceForServiceCharge = 0;
        var serviceCharge = 0;
        var lastIsServiceCharge = true;

        if (orderItem.type[0] == "I" || orderItem.type[0] == "T") {
            console.log(orderItem);
            console.log(orderItem.serviceCharge);
            lastIsServiceCharge = orderItem.serviceCharge[0];
            lastItemQty = orderItem.qty[0];
            totalPrice += lastItemQty * orderItem.unitPrice[0];

            if (lastIsServiceCharge) {
                totalPriceForServiceCharge += orderItem.qty[0] * orderItem.unitPrice[0];
            }


            // if (orderItem.type[0] == "T" && orderItem.voidIndex[0] == -1) {
            //     var checkIsVoidItem = tableOrderD[2].filter((checkItem) => {
            //         return checkItem.voidIndex == orderItem.seq;
            //     })
            //     if (checkIsVoidItem.length == 0) {
            //         checkIsVoidItem = r.order.item.filter((checkItem) => {
            //             if (checkItem.voidIndex == undefined) {
            //                 return false;
            //             }
            //             return checkItem.voidIndex[0] == orderItem.seq;
            //         })
            //         if (checkIsVoidItem.length == 0) {
            //             var items = RAS.Item.filter(function (entry) {
            //                 return entry.code == orderItem.itemId;
            //             });
            //             var _lastOrder = items[0].namek.split(',');
            //             if (_lastOrder.length > 1) {
            //                 lastOrderWarning = _lastOrder[0];
            //                 lastOrder = _lastOrder[1];
            //                 leaveTime = _lastOrder[2];
            //             }
            //         }
            //     }
            // }

        } else {
            totalPrice += lastItemQty * orderItem.unitPrice[0] * orderItem.qty[0];
            if (lastIsServiceCharge) {
                totalPriceForServiceCharge += lastItemQty * orderItem.unitPrice[0] * orderItem.qty[0];
            }
        }
        // if (itemType == "I") {
        //     lastIsServiceCharge = _sc;
        //     lastItemQty = parseInt(r.order.item[i].qty[0]);
        //     totalPrice += parseFloat((parseFloat(r.order.item[i].unitPrice[0]) / 100).toFixed(2)) * lastItemQty;
        //     if (_sc && lastIsServiceCharge) {
        //         totalPriceForServiceCharge += parseFloat((parseFloat(r.order.item[i].unitPrice[0]) / 100).toFixed(2)) * lastItemQty;
        //     }
        // } else {
        //     totalPrice += parseFloat((parseFloat(r.order.item[i].unitPrice[0]) / 100).toFixed(2)) * lastItemQty * r.order.item[i].qty[0];
        //     if (_sc && lastIsServiceCharge) {
        //         totalPriceForServiceCharge += parseFloat((parseFloat(r.order.item[i].unitPrice[0]) / 100).toFixed(2)) * lastItemQty * r.order.item[i].qty[0];
        //     }
        // }
        serviceCharge = (totalPriceForServiceCharge / 10).toFixed(1);

        return serviceCharge;
    }

    function getTableCode(tableNo) {
        try {
            var tableCode = tableNo;
            var tableSubCode = "";
            if (tableCode.indexOf('_') != -1) {
                var splitTableCode = tableCode.split('_');
                tableCode = splitTableCode[0];
                tableSubCode = splitTableCode[1];
            }
            return { tableCode: tableCode, tableSubCode: tableSubCode };
        } catch (e) {
            //console.log(e);
        }
    }

    this.getTakeoutCode = () => {
        try {
            console.log("getTakeoutCode 1");
            var tableCode = "takeout";
            var tableSubCode;
            var query = {
                "sql": "declare @seq int EXEC @seq = spGetNextTakeoutSeq \
                        select 'tableSubCode' = @seq;",
                "args": []
            };
            console.log("getTakeoutCode 2");
            this.DM.executeStatement(query, (err, result) => {
                console.log("getTakeoutCode 3");
                tableSubCode = result[0].tableSubCode;
                return { tableCode: tableCode, tableSubCode: tableSubCode };
            });
        } catch (e) {
            console.log(e);
        }
    }

    function sortDbItem(dbItems) {
        //dbItems.sort((a, b) => {
        //    if (a.seq != b.seq) { return a.seq - b.seq } else
        //        if (a.type == 'I') { return -1 } else
        //            if (a.type == 'O') { return -1 } else
        //                if (a.type == 'S') { return -1 } else
        //                    if (a.type == 'T') { return -1 } else
        //                        if (a.type == 'M') { return -1 } else
        //                            if (a.type == 'K') { return -1 } else
        //                            { return a.type < b.type }
        //});
        var map = ["I", "O", "S", "T", "M", "K"];

        dbItems.sort((a, b) => {
            if (a.seq < b.seq) return -1;
            if (a.seq === b.seq) {
                if (map.indexOf(a.type) < map.indexOf(b.type)) return -1;
                else if (map.indexOf(a.type) > map.indexOf(b.type)) return 1;
                else return moment(a.time).format('HHmmss') - moment(b.time).format('HHmmss');
            }
            if (a.seq > b.seq) return 1;
        });
    }

    this.TabletSaveTableOrder = (args, callback) => {
        var tableObj = getTableCode(args.tableNum.$value);
        var query = { "sql": "select * from tableorder where tablecode = @tableCode and tablesubcode = @tableSubCode", "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": tableObj.tableCode }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": tableObj.tableSubCode }] }
        this.DM.executeStatement(query, (err, result) => {
            if (result.length == 0) {

                callbackObj = { result: 'ERROR', timestamp: new Date().getTime(), taskID: 1, note: 'Table not yet open', suspendItems: [] };
                callback(callbackObj);
            } else {
                this.SaveTableOrder(args, callback);
            }
        })
    }

    this.SaveTableOrderTest = (args, callback) => {

        //console.log(args);
        var r = args.itemData.json;
        //console.log(JSON.stringify(r));
        var orderstring = "";
        var total = 0;
        var date = new Date();
        var orderid = moment().format("YYMMDDHHmmssSSSS");
        var itemOrderTime = moment().format("HH:mm:ss");
        var tableObj = getTableCode(args.tableNum.$value);

        var noOfPeople = args.peopleNum == undefined ? '0' : args.peopleNum.$value;

        var printingArray = { TableNo: args.tableNum.$value, Pax: noOfPeople, FoodItem: [] };
        var checkStockArray = [];
        var commitItemStockArray = [];
        var callbackObj = {};
        var vipcode, surname, givenname, vipcenter, currgrade, dob_yyyy, dob_mm, dob_dd, orderContent;
        var staff = args.user == undefined ? "user" : args.user.$value;
        vipcode = surname = givenname = vipcenter = currgrade = dob_yyyy = dob_mm = dob_dd = orderContent = '';
        var sqls = "begin tran begin try\
        begin\
        declare @round int, @seq int, @pax int, @itemId varchar(50)\
        select @round = max(round) , @seq = max(seq) from tableorderd where tableCode = @tableCode ;\
        if(@round is null)\
        set @round = 0;\
        if(@seq is null)\
        set @seq = 0;\
        DECLARE db_cursor CURSOR FOR  \
        select itemId from tableorderd where tableCode = @tableCode and tableSubCode = @tableSubCode order by seq, case when [type] = 'I' then 1 when [type] = 'O' then 1 when [type] = 'S' then 1 when [type] = 'T' then 1 else 2 end asc\
        OPEN db_cursor   \
        FETCH NEXT FROM db_cursor INTO @itemId   \
        WHILE @@FETCH_STATUS = 0   \
        BEGIN   \
        print(@itemId)\
        FETCH NEXT FROM db_cursor INTO @itemId   \
        END   \
        CLOSE db_cursor   \
        DEALLOCATE db_cursor \
        end\
        commit tran end try begin catch rollback tran; THROW; end catch\
        ";
        var sql = {
            sql: sqls, args: [{ "c": "tableCode", "t": TYPES.VarChar, "v": tableObj.tableCode }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": tableObj.tableSubCode }]
        };

        this.DM.executeStatement(sqls, (err, result) => {
            if (err != undefined) {
                console.log("error" + err);
            }
            console.log("error" + err);
            console.log('dgrsjk');
            console.log(result);
            var callbackObj = { result: "OK", timestamp: new Date().getTime(), taskID: 1, note: '', suspendItems: '', suspendItemsQty: '', totalPrice: 0 };
            callback(callbackObj);
        });
    }

    this.SaveTableOrder = (args, callback) => {
        // console.log("SaveTableOrder: ", args);
        var r = args.itemData.json;
        var till = args.till;
        console.log(3841, till);
        var orderstring = "";
        var total = 0;
        var date = new Date();
        var orderid = moment().format("YYMMDDHHmmssSSSS");
        var itemOrderTime = moment().format("HH:mm:ss");
        var tableObj = getTableCode(args.tableNum.$value);

        var noOfPeople = args.peopleNum == undefined ? '0' : args.peopleNum.$value;

        var printingArray = { TableNo: args.tableNum.$value, Pax: noOfPeople, FoodItem: [] };
        var checkStockArray = [];
        var commitItemStockArray = [];
        var callbackObj = {};
        var vipcode, surname, givenname, vipcenter, currgrade, dob_yyyy, dob_mm, dob_dd, orderContent;
        var staff = args.user == undefined ? "user" : args.user.$value;
        var cardId = null;
        if (args.tableKey)
            cardId = args.tableKey.$value;

        vipcode = surname = givenname = vipcenter = currgrade = dob_yyyy = dob_mm = dob_dd = orderContent = '';

        try {
            var lastItemQty = 0;
            for (var i = 0 ; i < r.order.item.length; i++) {
                //if (r.order.item[i].qty[0] > 0 || r.order.item[i].qty[0] < 0) {
                if (r.order.item[i].type[0] === "T" || r.order.item[i].type[0] === "I") {
                    lastItemQty = r.order.item[i].qty[0];
                    r.order.item[i].actualQty = lastItemQty;
                } else {
                    r.order.item[i].actualQty = lastItemQty * r.order.item[i].qty[0];
                }
                if (r.order.item[i].actualQty > 0) {
                    checkStockArray.push({
                        sql: "select itemCode,qty,@dname as \"name\" from itemControl where itemCode = @itemCode and (qty < 0 or qty < @qty)"
                        , args: [{ c: 'dname', t: TYPES.NVarChar, v: r.order.item[i].desc1[0] }, { c: 'itemCode', t: TYPES.VarChar, v: r.order.item[i].code[0] }, { c: 'qty', t: TYPES.Int, v: r.order.item[i].qty[0] }]
                    })
                    commitItemStockArray.push({ sql: "update itemControl set qty = qty - @qty where itemCode = @itemCode", args: [{ c: 'itemCode', t: TYPES.VarChar, v: r.order.item[i].code[0] }, { c: 'qty', t: TYPES.Int, v: r.order.item[i].qty[0] }] });
                }
            }
        } catch (e) {
            //console.log(e);
        }
        this.DM.executeStatement(checkStockArray, (err, result) => {
            if (err != undefined) {
                //console.log(err);
            }
            //console.log(JSON.stringify(result));
            var isValid = true;
            var errorMsg = ""; var outOfStockList = [];
            for (var i = 0; i < result.length; i++) {
                if (result[i].length != 0) {
                    isValid = false;
                    outOfStockList.push(result[i][0].itemCode);
                }
            }
            if (isValid) {
                var query = [
                    { "sql": "select max(round) as round, max(seq) as seq from tableorderd where tableCode = @tableCode ;", "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": tableObj.tableCode }] },
                    {
                        "sql": "select max(pax) as pax from tableOrder where tableCode = @tableCode and tableSubCode = @tableSubCode;", "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": tableObj.tableCode },
                          { "c": "tableSubCode", "t": TYPES.VarChar, "v": tableObj.tableSubCode },
                          { "c": "cardId", "t": TYPES.NVarChar, "v": cardId }
                        ]
                    },
                    {
                        "sql": "select * from tableorderd where tableCode = @tableCode and tableSubCode = @tableSubCode order by seq, type, time", "args": [
                          { "c": "tableCode", "t": TYPES.VarChar, "v": tableObj.tableCode },
                          { "c": "tableSubCode", "t": TYPES.VarChar, "v": tableObj.tableSubCode },
                          { "c": "cardId", "t": TYPES.NVarChar, "v": cardId }
                        ]
                    }
                ];

                if (cardId != null) {

                    query[1].sql = 'select (select max(pax) as pax  from tableOrder where tableCode = @tableCode and cardId = @cardId and status != 4) as pax, \
                        (select max(tableSubCode) from tableOrder where tableCode = @tableCode ) as maxTableSubCode,\
                        (select tableSubCode from tableOrder where tableCode = @tableCode and cardId = @cardId ) as tableSubCode ';

                    query[2].sql = 'select * from tableorderd where tableCode = (select tableCode from tableOrder where tableCode = @tableCode and cardId=@cardId and status != 4) and tableSubCode = (select tableSubCode from tableOrder where tableCode = @tableCode and cardId=@cardId and status != 4) order by seq, type, time;';
                    query.push({
                        "sql": "select max(tableSubCode) as tableSubCode from tableOrder where tableCode = @tableCode and cardId = @cardId and status != 4;",
                        "args": [
                            { "c": "tableCode", "t": TYPES.VarChar, "v": tableObj.tableCode },
                            { "c": "cardId", "t": TYPES.NVarChar, "v": cardId }
                        ]
                    })
                }

                //console.log('3774', tableObj.tableSubCode);

                this.DM.executeStatement(query, (err, tableOrderD) => {


                    console.log('3780', tableOrderD[3]);
                    console.log('3781', tableOrderD[1]);
                    console.log('3782', tableOrderD[2]);



                    var currentSeq = 0;
                    var currentRound = 0;
                    var query = [];
                    var _o = {};
                    var totalPrice = 0;
                    var totalPriceForServiceCharge = 0;
                    var serviceCharge = 0;
                    var lastOrder = '';
                    var lastOrderWarning = '';
                    var leaveTime = '';
                    var lastItemQty = 0;


                    if (tableOrderD[1][0].pax == null && tableOrderD[1][0].maxTableSubCode != null && cardId != null) {
                        var charArray = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".split('');
                        var pos = -1;
                        if (tableOrderD[1][0].maxTableSubCode == "") {
                            pos = 1;
                            query.push({
                                'sql': "update tableOrder set tableSubCode = 'A' where tableCode = @tableCode and tableSubCode = ''; update tableOrderD set tableSubCode = 'A' where tableCode = @tableCode and tableSubCode = '' ;",
                                "args": [
                                  { "c": "tableCode", "t": TYPES.VarChar, "v": tableObj.tableCode }
                                ]
                            })
                        }
                        else {
                            pos = charArray.indexOf(tableOrderD[1][0].maxTableSubCode) + 1;
                        }
                        tableObj.tableSubCode = charArray[pos];
                    } else if (tableOrderD[1][0].pax != null && cardId != null) {
                        tableObj.tableSubCode = tableOrderD[1][0].tableSubCode;
                    }

                    printingArray.TableNo = tableObj.tableCode;
                    if (tableObj.tableSubCode != "") printingArray.TableNo += "_" + tableObj.tableSubCode;

                    // console.log("SaveTableOrder query: ", tableOrderD);

                    /*var currentTableOrder = null;
                    if (tableOrderD[2].length != 0) {
                        currentTableOrder = tableOrderD[2];
                    }*/
                    // console.log("SaveTableOrder currentTableOrder: ", currentTableOrder);
                    if (tableOrderD[0][0].seq != null) {
                        currentSeq = tableOrderD[0][0].seq;
                    }
                    //if (tableOrderD[0][0].round != null) {
                    //    currentRound = tableOrderD[0][0].round;
                    //}
                    //currentRound++;

                    currentRound = 1;
                    var lastIsServiceCharge = true;
                    sortDbItem(tableOrderD[2]);

                    if (tableOrderD[2].length != 0) {
                        for (var i = 0; i < tableOrderD[2].length; i++) {
                            var orderedItem = tableOrderD[2][i];
                            if (orderedItem.type == "I" || orderedItem.type == "T") {
                                lastIsServiceCharge = orderedItem.serviceCharge;
                                lastItemQty = orderedItem.qty;
                                totalPrice += lastItemQty * orderedItem.price;

                                if (lastIsServiceCharge) {
                                    totalPriceForServiceCharge += orderedItem.qty * orderedItem.price;
                                }


                                if (orderedItem.type == "T" && orderedItem.voidIndex == -1) {
                                    var checkIsVoidItem = tableOrderD[2].filter((checkItem) => {
                                        return checkItem.voidIndex == orderedItem.seq;
                                    })
                                    if (checkIsVoidItem.length == 0) {
                                        checkIsVoidItem = r.order.item.filter((checkItem) => {
                                            if (checkItem.voidIndex == undefined) {
                                                return false;
                                            }
                                            return checkItem.voidIndex[0] == orderedItem.seq;
                                        })
                                        if (checkIsVoidItem.length == 0) {
                                            var items = RAS.Item.filter(function (entry) {
                                                return entry.code == orderedItem.itemId;
                                            });
                                            var _lastOrder = items[0].namek.split(',');
                                            if (_lastOrder.length > 1) {
                                                lastOrderWarning = _lastOrder[0];
                                                lastOrder = _lastOrder[1];
                                                leaveTime = _lastOrder[2];
                                            }
                                        }
                                    }
                                }

                            } else {
                                totalPrice += lastItemQty * orderedItem.price * orderedItem.qty;
                                if (lastIsServiceCharge) {
                                    totalPriceForServiceCharge += lastItemQty * orderedItem.price * orderedItem.qty;
                                }
                            }
                        }
                    }

                    var lastPrinter = '';
                    var orderKds = [];
                    lastIsServiceCharge = true;
                    var lastKds = null;

                    for (var i = 0 ; i < r.order.item.length; i++) {
                        var isFired = r.order.item[i].isFired;

                        var vi = r.order.item[i].voidIndex == undefined ? "-1" : r.order.item[i].voidIndex[0];
                        var vr = r.order.item[i].voidRemark == undefined ? "" : r.order.item[i].voidRemark[0];
                        var approveStaff = r.order.item[i].voidApproveStaff == undefined || null ? "" : r.order.item[i].voidApproveStaff[0];
                        var type = r.order.item[i].type[0];
                        //var serviceCharge = true;
                        var itemOrderTime = moment().format("HH:mm:ss");

                        if (type == "M" || type == "K" || !config.fireDineInMode) {
                            isFired = true;
                        }

                        if (type != "M" && type != "K") {
                            currentSeq += 1;
                        }
                        if (type == "T" && vi == "-1") {
                            var items = RAS.Item.filter(function (entry) {
                                return entry.code == r.order.item[i].code[0];
                            });
                            var _lastOrder = items[0].namek.split(',');
                            if (_lastOrder.length > 1) {
                                lastOrderWarning = _lastOrder[0];
                                lastOrder = _lastOrder[1];
                                leaveTime = _lastOrder[2];
                            }
                        }

                        //console.log(currentSeq)
                        ////console.log(r.order.item[i].unitPrice[0])
                        var customName = '';
                        if (r.order.item[i].customName) {
                            if (r.order.item[i].customName[0]) {
                                customName = r.order.item[i].customName[0]
                            }
                        }

                        var itemType = r.order.item[i].type[0];
                        var itemCode = r.order.item[i].code[0]
                        var optionGroupCode = null;
                        if (r.order.item[i].optionGroupCode) optionGroupCode = r.order.item[i].optionGroupCode[0];
                        var items = [];

                        var items = getItemByItem({ itemId: r.order.item[i].code[0], type: r.order.item[i].type[0] });
                        var namek = "";

                        var kds = null;
                        var _sc = true;
                        if (items.length > 0) {
                            if (items[0].serviceCharge != undefined) {
                                _sc = items[0].serviceCharge;
                            };
                            if (items[0].kds) {
                                kds = items[0].kds;
                            };
                            if (itemType != "M" && type != "K") {
                                lastPrinter = items[0].printer;
                                if (items[0].printerGroup) {
                                    lastPrinter += "*" + items[0].printerGroup;
                                }
                                lastKds = kds;
                            }
                            if (kds == null || config.kdsAndPrinting) {
                                var vr = r.order.item[i].voidRemark == undefined ? "" : r.order.item[i].voidRemark[0];
                                //var printer = items[0].printer == undefined ? "" : items[0].printer;
                                //if (items[0].printerGroup) {
                                //    printer += "*" + items[0].printerGroup;
                                //}


                                namek = items[0].namek;
                                if (customName != "") {
                                    namek = customName;
                                }

                                printingArray.FoodItem.push({ Name: namek, Qty: r.order.item[i].qty[0], Printer: lastPrinter, Code: items[0].code, Time: itemOrderTime, Msg: vr, individualPrint: items[0].individualPrint, Type: type, holdKitchenPrinter: items[0].holdKitchenPrinter });
                                if (r.order.item[i].index) {
                                    printingArray.FoodItem[printingArray.FoodItem.length - 1].Index = r.order.item[i].index[0];
                                }
                            }
                        }

                        if (itemType == "I") {
                            lastIsServiceCharge = _sc;
                            lastItemQty = parseInt(r.order.item[i].qty[0]);
                            totalPrice += parseFloat((parseFloat(r.order.item[i].unitPrice[0]) / 100).toFixed(2)) * lastItemQty;
                            if (_sc && lastIsServiceCharge) {
                                totalPriceForServiceCharge += parseFloat((parseFloat(r.order.item[i].unitPrice[0]) / 100).toFixed(2)) * lastItemQty;
                            }
                        }
                        else {
                            totalPrice += parseFloat((parseFloat(r.order.item[i].unitPrice[0]) / 100).toFixed(2)) * lastItemQty * r.order.item[i].qty[0];
                            if (_sc && lastIsServiceCharge) {
                                totalPriceForServiceCharge += parseFloat((parseFloat(r.order.item[i].unitPrice[0]) / 100).toFixed(2)) * lastItemQty * r.order.item[i].qty[0];
                            }
                        }

                        if (kds != null) {
                            if (orderKds.indexOf(kds) == -1) {
                                orderKds.push(kds);
                            }
                        }

                        if (r.order.item[i].index) {
                            _o = {
                                "sql": "delete from tableOrderD where itemId = \'O9999\' and type = \'O\' and seq = @seq and tableCode = @tableCode and tableSubCode = @tableSubCode;\
insert into tableOrderD (tableCode,tableSubCode,itemId,optionGroupCode,qty,price,voidIndex,voidRemark,type,seq,time,staffId,round,customName,approveStaffId,serviceCharge,printer,kdsgroup,kds) values (@tableCode,@tableSubCode,@itemId,@optionGroupCode,@qty,@price,@voidIndex,@voidRemark,@type,@seq,@time,@staffId,@round,@customName,@approveStaffId,@serviceCharge,@printer,@kdsgroup,@kds)",
                                "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": tableObj.tableCode }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": tableObj.tableSubCode }, { "c": "itemId", "t": TYPES.VarChar, "v": itemCode }, { "c": "optionGroupCode", "t": TYPES.VarChar, "v": optionGroupCode }, { "c": "qty", "t": TYPES.Int, "v": parseInt(r.order.item[i].qty[0]) },
                                    { "c": "price", "t": TYPES.Decimal, "v": parseFloat((parseFloat(r.order.item[i].unitPrice[0]) / 100).toFixed(2)), "o": { "scale": 2 } }, { "c": "voidIndex", "t": TYPES.Int, "v": vi }, { "c": "voidRemark", "t": TYPES.NVarChar, "v": vr }, { "c": "type", "t": TYPES.VarChar, "v": type }, { "c": "seq", "t": TYPES.Int, "v": r.order.item[i].index[0] }, { "c": "time", "t": TYPES.Time, "v": date }, { "c": "staffId", "t": TYPES.VarChar, "v": staff }, { "c": "round", "t": TYPES.Int, "v": currentRound }, { "c": "customName", "t": TYPES.NVarChar, "v": customName }, { "c": "approveStaffId", "t": TYPES.VarChar, "v": approveStaff }, { "c": "serviceCharge", "t": TYPES.Bit, "v": _sc }, { "c": "printer", "t": TYPES.VarChar, "v": lastPrinter }, { "c": "kdsgroup", "t": TYPES.BigInt, "v": '-1' }, { "c": "kds", "t": TYPES.VarChar, "v": lastKds }]
                            }
                            query.push(_o);
                        }
                        else {
                            _o = {
                                "sql": "insert into tableOrderD (tableCode,tableSubCode,itemId,optionGroupCode,qty,price,voidIndex,voidRemark,type,seq,time,staffId,round,customName,approveStaffId,serviceCharge,printer,kdsgroup,kds) values (@tableCode,@tableSubCode,@itemId,@optionGroupCode,@qty,@price,@voidIndex,@voidRemark,@type,@seq,@time,@staffId,@round,@customName,@approveStaffId,@serviceCharge,@printer,@kdsgroup,@kds)",
                                "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": tableObj.tableCode }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": tableObj.tableSubCode }, { "c": "itemId", "t": TYPES.VarChar, "v": itemCode }, { "c": "optionGroupCode", "t": TYPES.VarChar, "v": optionGroupCode }, { "c": "qty", "t": TYPES.Int, "v": parseInt(r.order.item[i].qty[0]) },
                                    { "c": "price", "t": TYPES.Decimal, "v": parseFloat((parseFloat(r.order.item[i].unitPrice[0]) / 100).toFixed(2)), "o": { "scale": 2 } }, { "c": "voidIndex", "t": TYPES.Int, "v": vi }, { "c": "voidRemark", "t": TYPES.NVarChar, "v": vr }, { "c": "type", "t": TYPES.VarChar, "v": type }, { "c": "seq", "t": TYPES.Int, "v": currentSeq }, { "c": "time", "t": TYPES.Time, "v": date }, { "c": "staffId", "t": TYPES.VarChar, "v": staff }, { "c": "round", "t": TYPES.Int, "v": currentRound }, { "c": "customName", "t": TYPES.NVarChar, "v": customName }, { "c": "approveStaffId", "t": TYPES.VarChar, "v": approveStaff }, { "c": "serviceCharge", "t": TYPES.Bit, "v": _sc }, { "c": "printer", "t": TYPES.VarChar, "v": lastPrinter }, { "c": "kdsgroup", "t": TYPES.BigInt, "v": '-1' }, { "c": "kds", "t": TYPES.VarChar, "v": lastKds }]
                            }
                            query.push(_o);
                        }
                        _o = {
                            "sql": "insert into kds (tableCode,tableSubCode,itemId,qty,voidIndex,type,seq,time,round,printer,kdsgroup,kds, kdsStatus) values (@tableCode,@tableSubCode,@itemId,@qty,@voidIndex,@type,@seq,@time,@round,@printer,@kdsgroup,@kds, @kdsStatus)",
                            "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": tableObj.tableCode }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": tableObj.tableSubCode }, { "c": "itemId", "t": TYPES.VarChar, "v": itemCode }, { "c": "qty", "t": TYPES.Int, "v": parseInt(r.order.item[i].qty[0]) },
                                { "c": "voidIndex", "t": TYPES.Int, "v": vi }, { "c": "type", "t": TYPES.VarChar, "v": type }, { "c": "seq", "t": TYPES.Int, "v": currentSeq }, { "c": "time", "t": TYPES.Time, "v": date }, { "c": "round", "t": TYPES.Int, "v": currentRound }, { "c": "printer", "t": TYPES.VarChar, "v": lastPrinter }, { "c": "kdsgroup", "t": TYPES.BigInt, "v": '-1' }, { "c": "kds", "t": TYPES.VarChar, "v": lastKds },
                                { "c": "kdsStatus", "t": TYPES.VarChar, "v": isFired ? null : -1 }
                            ]
                        }
                        query.push(_o);
                    }

                    if (args.serviceCharge == undefined || args.serviceCharge)
                        serviceCharge = (totalPriceForServiceCharge / 10).toFixed(1);

                    var lastOrderDate = null, lastOrderWarningDate = null, leaveTimeDate = null;
                    if (lastOrder != "") {
                        lastOrderDate = new Date(moment().format("YYYY/MM/DD ") + lastOrder);
                        AssignLastOrderSchedule("lastOrder_" + lastOrder, lastOrder);
                    }
                    if (lastOrderWarning != "" || config.lastOrderWarning) {
                        if (config.lastOrderWarning) {
                            lastOrderWarningDate = new Date();
                            var lastOrderWarningTime = config.lastOrderWarningTime.split(':');
                            lastOrderWarning = moment().add(lastOrderWarningTime[0], 'hours').add(lastOrderWarningTime[1], 'minutes');
                            lastOrderWarning = lastOrderWarning.format('HH:mm');

                        } else {
                            lastOrderWarningDate = new Date(moment().format("YYYY/MM/DD ") + lastOrderWarning);
                        }

                        AssignLastOrderSchedule("lastOrder_" + lastOrderWarning, lastOrderWarning);
                    }
                    if (leaveTime != "") {
                        leaveTimeDate = new Date(moment().format("YYYY/MM/DD ") + leaveTime);
                        AssignLastOrderSchedule("lastOrder_" + leaveTime, leaveTime);
                    }

                    _o = {
                        "sql": "declare @seq int EXEC @seq = spGetNextSeq 'sales' ; declare @sseq varchar(10) = REPLICATE('0',6-LEN(RTRIM(@seq))) + RTRIM(@seq) ;\
    insert into tableOrder (tableCode,tableSubCode,pax,memberId,startTime,staffId, price, serviceCharge, lastOrder, lastOrderWarning, leaveTime, status, orderid) \
                        values (@tableCode,@tableSubCode,@pax,@memberId,@startTime,@staffId, @price, @serviceCharge, @lastOrder, @lastOrderWarning, @leaveTime, @status, @refNo + @sseq)",
                        "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": tableObj.tableCode }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": tableObj.tableSubCode }, { "c": "pax", "t": TYPES.Int, "v": noOfPeople }, { "c": "memberId", "t": TYPES.Int, "v": null }, { "c": "startTime", "t": TYPES.DateTime, "v": date }, { "c": "staffId", "t": TYPES.VarChar, "v": staff },
                        { "c": "price", "t": TYPES.Decimal, "v": parseFloat(totalPrice), "o": { "scale": 2 } },
                        { "c": "serviceCharge", "t": TYPES.Decimal, "v": parseFloat(serviceCharge), "o": { "scale": 2 } },
                        { "c": "lastOrder", "t": TYPES.DateTime, "v": lastOrderDate },
                        { "c": "lastOrderWarning", "t": TYPES.DateTime, "v": lastOrderWarningDate },
                        { "c": "leaveTime", "t": TYPES.DateTime, "v": leaveTimeDate },
                        { "c": "status", "t": TYPES.Int, "v": 2 },
                        { "c": "refNo", "t": TYPES.VarChar, "v": config.shopCode + moment().format("YYMMDD") },
                        { "c": "cardId", "t": TYPES.NVarChar, "v": cardId }]
                    }

                    if (cardId != null) {
                        _o.sql = "declare @seq int EXEC @seq = spGetNextSeq 'sales' ; declare @sseq varchar(10) = REPLICATE('0',6-LEN(RTRIM(@seq))) + RTRIM(@seq) ;\
insert into tableOrder (tableCode,tableSubCode,pax,memberId,startTime,staffId, price, serviceCharge, lastOrder, lastOrderWarning, leaveTime, status, orderid, cardId) \
                    values (@tableCode,@tableSubCode,@pax,@memberId,@startTime,@staffId, @price, @serviceCharge, @lastOrder, @lastOrderWarning, @leaveTime, @status, @refNo + @sseq, @cardId)"
                    }

                    if (tableOrderD[1][0].pax == null) {
                        query.push(_o);
                    } else {
                        if (noOfPeople == 0) {
                            noOfPeople = tableOrderD[1][0].pax;
                            printingArray.Pax = noOfPeople;
                        }

                        _o = {
                            "sql": "update tableOrder set status = @status, pax = @pax, price = @price, serviceCharge = @serviceCharge, lastOrder = @lastOrder, lastOrderWarning = @lastOrderWarning, leaveTime = @leaveTime where tableCode = @tableCode and tableSubCode = @tableSubCode",
                            "args": [{ "c": "status", "t": TYPES.Int, "v": 2 },
                                { "c": "pax", "t": TYPES.Int, "v": noOfPeople }, { "c": "tableCode", "t": TYPES.VarChar, "v": tableObj.tableCode }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": tableObj.tableSubCode },
                                { "c": "price", "t": TYPES.Decimal, "v": parseFloat(totalPrice), "o": { "scale": 2 } },
                                { "c": "serviceCharge", "t": TYPES.Decimal, "v": parseFloat(serviceCharge), "o": { "scale": 2 } },
                                { "c": "lastOrder", "t": TYPES.DateTime, "v": lastOrderDate },
                                { "c": "lastOrderWarning", "t": TYPES.DateTime, "v": lastOrderWarningDate },
                                { "c": "leaveTime", "t": TYPES.DateTime, "v": leaveTimeDate },
                                { "c": "cardId", "t": TYPES.NVarChar, "v": cardId }]
                        }

                        //

                        if (cardId != null) {
                            _o.sql = "update tableOrder set status = @status, pax = @pax, price = @price, serviceCharge = @serviceCharge, lastOrder = @lastOrder, lastOrderWarning = @lastOrderWarning, leaveTime = @leaveTime , cardId = @cardId where tableCode = @tableCode and tableSubCode = @tableSubCode"
                        }


                        query.push(_o);
                    }
                    //console.log("TEST", query);

                    this.DM.executeStatement(query, (err, result) => {
                        // console.log("TEST", err);
                        // console.log("TEST", result);

                        this.SetTableStatus({ "tableCode": tableObj.tableCode, "status": 2 })

                        this.DM.executeStatement(commitItemStockArray, (err, result) => {
                            //console.log(err);
                            this.GetOutOfStock((err, result) => {
                                io.sockets.emit('refreshItemQty', JSON.stringify(result));
                                var _o = { "action": "refreshItemQty", "outOfStock": result }
                                mqttClient.publish("wo/gm/all", JSON.stringify(_o));

                                var _result = "OK";
                                var si = [];
                                var siq = [];
                                result.forEach((item) => {
                                    si.push(item.code);
                                    siq.push(item.qty);
                                });
                                callbackObj = { result: _result, timestamp: new Date().getTime(), taskID: 1, note: '', suspendItems: si.toString(), suspendItemsQty: siq.toString(), totalPrice: totalPrice };
                                callback(callbackObj);

                                console.log('is orderKds', orderKds);
                                for (var j = 0; j < orderKds.length; j++) {
                                    this.showKds(orderKds[j]);
                                }

                            });
                        }, "parallel");

                        console.log('save tableOrder', printingArray);

                        function findPrinterByTableNo(tableNo) {

                            if (tableNo.indexOf("_") != -1) {
                                tableNo = tableNo.split("_")[0];
                            }
                            console.log(4325, tableNo);
                            var result = { printer: null, till: null };


                            for (var till in config.tills) {
                                if (config.tills[till].tableNo) {
                                    if (config.tills[till].tableNo.indexOf(tableNo) != -1) {
                                        result.printer = config.tills[till].printer;
                                        result.till = till;
                                        break;
                                    }
                                }
                            }

                            return result;
                        }

                        if (printingArray.FoodItem.length != 0) {
                            //mqttClient.publish('wo/kds/p1', JSON.stringify(printingArray), { qos: 2 });

                            /** missing till **/
                            //printingArray.till = "re_1";

                            if (!args.till)
                                printingArray.till = findPrinterByTableNo(args.tableNum.$value).till;
                            else
                                printingArray.till = till;

                            console.log('firemode', printingArray);
                            if (config.fireDineInMode) {
                                RAS.firePrinting(printingArray, function (result) {
                                    RAS.PrintFoodItem(result);
                                })
                            }
                            else {
                                this.PrintFoodItem(printingArray);
                            }

                            /*
                            fs.writeFile("./printer/" + moment().format("YYMMDDHHmmssSSSS") + ".json", JSON.stringify(printingArray), function (err) {
                        
                        if (err) {
                                    //console.log(err);
                                } else {
                                    ////console.log("The file was saved!");
                                }
                            });
                            */
                        }


                        if (config.alwaysPrintBill) {
                            //console.log(4305, args);
                            //console.log(4309, args.itemData.json);
                            //user: { '$value': '000_000001' },
                            if (r.order.item.length > 0) {
                                var printer = null;

                                console.log(4347, args.till);
                                if (!args.till)
                                    printer = findPrinterByTableNo(args.tableNum.$value).printer;
                                else {
                                    if (config.tills[args.till] == undefined)
                                        printer = config.tills["default"].printer;
                                    else printer = config.tills[args.till].printer;
                                }

                                if (!args.user) {
                                    args.user = {};
                                    args.user.$value = "000_000001"
                                }
                                //console.log(4347, printer);
                                RAS.Print({ tableNo: args.tableNum.$value, action: "bill", printer: printer, user: args.user.$value }, function () { });
                            }

                        }


                    }, 'parallel')
                })
            } else {
                this.GetOutOfStock((err, result) => {
                    var note = "<note></note>";
                    var rItemSuspendInfo = "<suspendItems>" + this.OutOfStock + "</suspendItems>";
                    if (outOfStockList.length != 0) {
                        // note = "<note>outOfStock</note>";
                        note = "outOfStock";
                    }
                    var si = [];
                    var siq = [];
                    result.forEach((item) => {
                        si.push(item.code);
                        siq.push(item.qty);
                    });
                    console.log("si", si);
                    callbackObj = { result: 'ERROR', timestamp: new Date().getTime(), taskID: 1, note: note, suspendItems: si.toString(), suspendItemsQty: siq.toString() };
                    callback(callbackObj);
                })
            }
        }, 'parallel')
    }

    this.firePrinting = (printingArray, cb) => {
        var firePrinting = {};
        extend2(firePrinting, printingArray);

        firePrinting.FoodItem = [];

        console.log(firePrinting);
        console.log(config.fireDineInMode, config.fireCode);

        var fireCode = config.fireCode;
        var isHoldKitchenPrinter = false;

        for (var a = 0; a < printingArray.FoodItem.length; a++) {
            //var nextObjIndex = a + 1;

            if (printingArray.FoodItem[a].Qty < 0) {
                firePrinting.FoodItem.push(printingArray.FoodItem[a]);
                continue;
            }

            if (printingArray.FoodItem[a].Type != 'M' && printingArray.FoodItem[a].Type != 'K') {
                isHoldKitchenPrinter = false;
                console.log('fireprinting', printingArray.FoodItem[a]);
                if (printingArray.FoodItem[a].holdKitchenPrinter) isHoldKitchenPrinter = true;
            }

            var isFireObj = false;
            console.log('checking', a)
            console.log('isHoldKitchenPrinter', isHoldKitchenPrinter);
            if (isHoldKitchenPrinter) {
                /** check next index **/
                /** loop z <= printingArray.FoodItem.length (outofarrayindex) for final index(element) to process that some element insert to array **/
                for (var z = a + 1; z <= printingArray.FoodItem.length; z++) {
                    console.log('z', z, isFireObj);

                    function insertFireElement() {
                        /** becasuse checked next index , so need to mins one to index **/
                        for (var insertIndex = a; insertIndex <= (z - 1) ; insertIndex++) {
                            console.log('insertIndex', insertIndex);
                            if (printingArray.FoodItem[insertIndex].Code == fireCode) continue;
                            firePrinting.FoodItem.push(printingArray.FoodItem[insertIndex]);
                        }
                        a = (z - 1);
                    }

                    if (!printingArray.FoodItem[z]) { /** outofarrayindex for process that some element insert to array **/
                        if (isFireObj) {
                            insertFireElement();
                        }
                        break;
                    }

                    if (printingArray.FoodItem[z].Type != 'M' && printingArray.FoodItem[z].Type != 'K') { /** next cycle , end to action**/
                        if (isFireObj) {
                            insertFireElement();
                        }
                        break;
                    }
                    if (printingArray.FoodItem[z].Code == fireCode) {
                        //firePrinting.FoodItem.push(printingArray.FoodItem[a]);
                        isFireObj = true;
                    }
                }

            } else {
                if (printingArray.FoodItem[a].Code == fireCode) continue;
                firePrinting.FoodItem.push(printingArray.FoodItem[a]);
            }

            //if (printingArray.FoodItem[nextObjIndex].Code == fireCode) { }

        }

        console.log('fire filter', firePrinting);
        cb(firePrinting);
    }

    this.PrintFoodItemTest = (food) => {
        var printerList = [];
        var printingList = [];
        var takeawayFulllist = [];
        var seperateline = "\r\n";
        var lastItem = {};
        for (var i = 0; i < food.FoodItem.length; i++) {
            var f = food.FoodItem[i];
            if (f.Type === "I" || f.Type === "S" || f.Type === "O" || f.Type === "T") {
                if (f.Type === "I") {
                    lastItem = f;
                }
                var itemPrinter = "";
                var lastPrinter = "";
                var printObj = { FoodItem: [], Printer: "" };

                if (f.Printer != "") {
                    if (printerList.indexOf(f.Printer) === -1) {

                        printObj.Printer = f.Printer;
                        console.log(printObj);
                        printerList.push(f.Printer);
                        printingList.push(printObj);

                        console.log(printingList);
                    } else {
                        printObj = printingList[printerList.indexOf(f.Printer)];
                    }
                }
                if (f.Type === "I") {
                    printObj.FoodItem.push(lastItem);
                } else {
                    printObj.FoodItem.push(f);
                }
                for (var j = i + 1; j < food.FoodItem.length; j++) {
                    var sf = food.FoodItem[j];
                    if (sf.Type === "K" || sf.Type === "M") {
                        if (f.modifierKitchenmsg == undefined) {
                            f.modifierKitchenmsg = [];
                        }
                        f.modifierKitchenmsg.push(sf);
                    } else {
                        break;
                    }
                }
                if (f.Type === "S" || f.Type === "O") {
                    if (lastItem.subitemOption == undefined) {
                        lastItem.subitemOption = [];
                    }
                    lastItem.subitemOption.push(f);
                }
            }
        }
        for (var i = 0; i < printingList.length; i++) {
            printer = printingList[i].Printer;

            if (printer === "") continue;
            if (printer == undefined) continue;
            if (printer.indexOf('*') != -1) {
                printer = printer.substr(0, printer.indexOf('*'));
            }
            var dir = "./printer/" + printer;
            if (!fs.existsSync(dir)) {
                fs.mkdirSync(dir);
            }

            /*fs.writeFile(dir + "/food_" + moment().format("YYMMDDHHmmssSSSS_") + Math.floor(Math.random() * (10000000 - 0) + 0) + ".json", JSON.stringify(printingList[i]), function (err) {
                if (err) {
                    //console.log(err);
                } else {
                    ////console.log("The file was saved!");
                }
            });*/

            RAS.writeFile(dir + "/food_", JSON.stringify(printingList[i]), function (err) {

            })
        }
    }

    this.writeFile = (fileName, fileData, callback, fileType) => {
        if (!fileType) fileType = '.json'
        fileName = fileName + moment().format("YYMMDDHHmmssSSSS_") + Math.floor(Math.random() * (10000000 - 0) + 0) + fileType;
        fs.writeFile(fileName, fileData, function (err) {
            callback(err);
        })
    }

    this.PrintFoodItemString = (food) => {
        this.PrintFoodItem(JSON.parse(food));
    }

    this.PrintFoodItem = (food, label, cb) => {
        console.log('PrintFoodItem', JSON.stringify(food));
        var printerList = [];
        var printingList = []
        var index = 0;
        console.log('PrintFoodItem food item', JSON.stringify(food.FoodItem));
        var takeawayFulllist = [];

        for (var i = 0; i < food.FoodItem.length; i++) {
            var f = food.FoodItem[i];

            console.log('PrintFoodItem f ', f);
            if (f.Type === "I") {
                var itemPrinter = "";
                var lastPrinter = "";
                var printObj = {};
                if (f.Printer != "" || f.Printer != undefined) {
                    itemPrinter = f.Printer;
                    lastPrinter = f.Printer;

                    if (printerList.indexOf(f.Printer) === -1) {
                        printObj.Printer = f.Printer;
                        printerList.push(f.Printer);
                        printObj.TableNo = food.TableNo;
                        printObj.Pax = food.Pax;
                        printObj.isTakeAway = food.isTakeAway;
                        printObj.FoodItem = [];
                        printingList.push(printObj);
                    } else {
                        printObj = printingList[printerList.indexOf(f.Printer)];
                    }
                    printObj.FoodItem.push(f);
                    takeawayFulllist.push(f);
                }
                index++;

                var PrinterAddComboTitle = "";
                for (var j = index; j < food.FoodItem.length; j++) {
                    var nextFood = food.FoodItem[j];
                    //console.log('check next food:' + JSON.stringify(nextFood));
                    if (nextFood.Type != "I" && !nextFood.Index) {
                        if (f.SubItem == null) {
                            f.SubItem = [];
                        }
                        if (f.individualPrint) {

                            nextFood.ShowQty = nextFood.Qty == 1 ? "" : nextFood.Qty.toString();
                            nextFood.Qty = nextFood.Qty * f.Qty;
                        } else {

                            nextFood.ShowQty = nextFood.Qty == 1 ? "" : nextFood.Qty.toString();
                        }
                        nextFood.Name = " " + nextFood.Name;
                        if (nextFood.Code == undefined) continue;
                        f.SubItem.push(nextFood);

                    } else {
                        break;
                    }

                    if ((nextFood.Printer === "" || nextFood.Printer === undefined) && lastPrinter === itemPrinter) {
                        continue;
                    }

                    if (nextFood.Printer != itemPrinter) {
                        var nextFoodPrinter = nextFood.Printer;
                        if (nextFoodPrinter === "" || nextFoodPrinter === undefined) {
                            nextFoodPrinter = lastPrinter;
                        } else {
                            lastPrinter = nextFoodPrinter;
                        }
                        var subPrintObj = {};
                        if (printerList.indexOf(nextFoodPrinter) == -1) {
                            printerList.push(nextFoodPrinter);
                            subPrintObj.TableNo = food.TableNo;
                            subPrintObj.Printer = nextFoodPrinter;
                            subPrintObj.Pax = food.Pax;
                            subPrintObj.isTakeAway = food.isTakeAway;
                            subPrintObj.FoodItem = [];
                            printingList.push(subPrintObj);
                        } else {

                            subPrintObj = printingList[printerList.indexOf(nextFoodPrinter)];
                        }
                        if (PrinterAddComboTitle.indexOf("|" + nextFoodPrinter + "|") == -1) {
                            PrinterAddComboTitle += "|" + nextFoodPrinter + "|";
                            if (config.printComboTitle) {
                                var _foodCombo = {}
                                extend(f, _foodCombo)
                                _foodCombo.SubItem = [];
                                subPrintObj.FoodItem.push(_foodCombo);
                                _foodCombo.SubItem.push(nextFood);
                            } else {
                                subPrintObj.FoodItem.push(nextFood);
                            }
                            //subPrintObj.FoodItem.push(nextFood);
                        }
                        else {
                            for (var k = 0; k < printingList.length; k++) {
                                if (printingList[k].Printer === nextFoodPrinter) {
                                    if (config.printComboTitle) {
                                        if (printingList[k].FoodItem[printingList[k].FoodItem.length - 1].SubItem === undefined) {
                                            printingList[k].FoodItem[printingList[k].FoodItem.length - 1].SubItem = [];
                                        }
                                        printingList[k].FoodItem[printingList[k].FoodItem.length - 1].SubItem.push(nextFood);
                                    } else {
                                        printingList[k].FoodItem.push(nextFood);
                                    }
                                    //printingList[k].FoodItem[printingList[k].FoodItem.length - 1].push(nextFood);
                                }
                            }
                        }
                    }
                }
            }
            else if (f.Type === "O" && f.Index) {
                var itemPrinter = "";
                var lastPrinter = "";
                var printObj = {};
                if (f.Printer != "") {
                    itemPrinter = f.Printer;
                    lastPrinter = f.Printer;

                    if (printerList.indexOf(f.Printer) === -1) {
                        printObj.Printer = f.Printer;
                        printerList.push(f.Printer);
                        printObj.TableNo = food.TableNo;
                        printObj.Pax = food.Pax;
                        printObj.isTakeAway = food.isTakeAway;
                        printObj.FoodItem = [];
                        printingList.push(printObj);
                    } else {
                        printObj = printingList[printerList.indexOf(f.Printer)];
                    }
                    printObj.FoodItem.push(f);
                    takeawayFulllist.push(f);
                }
                index++;
            }
            else {
                index++;
            }

            console.log('printingList in 4391 :', i, JSON.stringify(printingList));
        }

        if (printingList.length != 0)
            console.log('printingList in kp', JSON.stringify(printingList));

        if (!food.till) {
            food.till = 'default';
        }

        console.log(food.till);
        console.log('config till', JSON.stringify(config.tills));
        //var dir = "./printer/" + config.tills[food.till].printer;
        var dir = "./printer/" + config.tills[food.till].foodFulllistprinter;
        console.log('food.isTakeAway in kds', food.isTakeAway);
        if (food.isTakeAway || config.printFoodFulllist) {
            console.log('config', config.tills[food.till].foodFulllistprinter);

            food.printer = config.tills[food.till].foodFulllistprinter;
            food.FoodItem = takeawayFulllist;

            //console.log('4516', food.FoodItem.length != 0);
            //console.log('4517', label);
            if (food.FoodItem.length != 0 && !label)

                /*fs.writeFile(dir + "/foodfulllist_" + moment().format("YYMMDDHHmmssSSSS_") + Math.floor(Math.random() * (10000000 - 0) + 0) + ".json", JSON.stringify(food), function (err) {
                    if (err) {
                        //console.log(err);
                    } else {
                        ////console.log("The file was saved!");
                    }
                });*/

                RAS.writeFile(dir + "/foodfulllist_", JSON.stringify(food), function (err) { });


        }

        //console.log(JSON.stringify(printingList));
        var printer;

        for (var i = 0; i < printingList.length; i++) {

            printer = printingList[i].Printer;
            if (printer === "") continue;
            if (printer == undefined) continue;
            if (printer.indexOf('*') != -1) {
                printer = printer.substr(0, printer.indexOf('*'));
            }
            var dir = "./printer/" + printer;
            if (!fs.existsSync(dir)) {
                fs.mkdirSync(dir);
            }

            if (label && printingList[i].FoodItem) {
                var foodItemList = [];
                printingList[i].FoodItem.forEach(function (fooditem) {
                    var subitemList = [];
                    if (!fooditem.SubItem) fooditem.SubItem = [];
                    fooditem.SubItem.forEach(function (subitem) {
                        if (subitem.Type == 'M' || subitem.Type == 'K') {
                            subitemList.push(subitem);
                            return true;
                        }

                        if (subitem.Qty > 1) {
                            //var subItemQty = subitem.Qty;
                            subitem.Qty = 1;
                            subitemList.push(subitem);
                            /*for (var i = 0; i < subItemQty; i++) {
                                subitemList.push(subitem);
                            }*/
                        } else {
                            subitemList.push(subitem);
                        }
                    })

                    if (fooditem.Qty > 1) {
                        var foodItemQty = fooditem.Qty;
                        fooditem.Qty = 1;
                        fooditem.SubItem = [];
                        fooditem.SubItem = subitemList;

                        for (var i = 0; i < foodItemQty; i++) {
                            foodItemList.push(fooditem);
                        }
                    } else {
                        fooditem.SubItem = [];
                        fooditem.SubItem = subitemList;
                        foodItemList.push(fooditem);
                    }
                })
                printingList[i].FoodItem = [];
                printingList[i].FoodItem = foodItemList;
            }



            console.log('printingList[i]', JSON.stringify(printingList[i]));

            /*fs.writeFile(dir + "/food_" + moment().format("YYMMDDHHmmssSSSS_") + Math.floor(Math.random() * (10000000 - 0) + 0) + ".json", JSON.stringify(printingList[i]), function (err) {
                if (cb) cb(null);

                if (err) {
                    //console.log(err);
                } else {
                    ////console.log("The file was saved!");
                }
            });*/

            RAS.writeFile(dir + "/food_", JSON.stringify(printingList[i]), function (err) {
                if (cb) cb(null);
            })
        }

    }

    this.SaveLayoutTable = (args, callback) => {
        var query = [],
            message = {
                warning: 'default'
            };
        // var testquery = [];

        console.log(args);
        if (typeof args === 'object' && args.length != 'undefined' && args.length > 0) {
            args.forEach((table) => {

                if (table.status === -1) {
                    table.state = 'new';
                }

                switch (table.state) {
                    case 'modified':
                        query.push({
                            "sql": "update diningTable set x = @x, y = @y, scaleX = @scaleX, scaleY = @scaleY, shopZoneId = @shopZoneId, name = @name, size = @size, angleA = @angleA, angleB = @angleB, shape = @shape where uid = @uid ;", "args": [
                                { "c": "uid", "t": TYPES.VarChar, "v": table.code },
                                { "c": "name", "t": TYPES.NVarChar, "v": table.label },
                                { "c": "x", "t": TYPES.Float, "v": table.x },
                                { "c": "y", "t": TYPES.Float, "v": table.y },
                                { "c": "scaleX", "t": TYPES.Float, "v": table.scalex },
                                { "c": "scaleY", "t": TYPES.Float, "v": table.scaley },
                                { "c": "shape", "t": TYPES.VarChar, "v": table.shape },
                                { "c": "size", "t": TYPES.Int, "v": table.size },
                                { "c": "angleA", "t": TYPES.Float, "v": table.angle },
                                { "c": "angleB", "t": TYPES.Float, "v": table.angle },
                                { "c": "shopZoneId", "t": TYPES.Int, "v": table.zone }
                            ]
                        }); // query
                        break;
                    case 'new':
                        query.push({
                            "sql": "insert into diningTable (uid, x, y, scaleX, scaleY, shopZoneId, name, size, angleA, angleB, shape, status, shopId) values (@uid, @x, @y, @scaleX, @scaleY, @shopZoneId, @name, @size, @angleA, @angleB, @shape, 0, 1)", "args": [
                                { "c": "uid", "t": TYPES.VarChar, "v": table.code },
                                { "c": "name", "t": TYPES.NVarChar, "v": table.label },
                                { "c": "x", "t": TYPES.Float, "v": table.x },
                                { "c": "y", "t": TYPES.Float, "v": table.y },
                                { "c": "scaleX", "t": TYPES.Float, "v": table.scalex },
                                { "c": "scaleY", "t": TYPES.Float, "v": table.scaley },
                                { "c": "shape", "t": TYPES.VarChar, "v": table.shape },
                                { "c": "size", "t": TYPES.Int, "v": table.size },
                                { "c": "angleA", "t": TYPES.Float, "v": table.angle },
                                { "c": "angleB", "t": TYPES.Float, "v": table.angle },
                                { "c": "shopZoneId", "t": TYPES.Int, "v": table.zone }
                            ]
                        }); // query
                        console.log('NEW table');
                        // console.log("insert into diningTable (uid, x, y, scaleX, scaleY, shopZoneId, name, size, angleA, angleB, shape, status, shopId) values (@uid, @x, @y, @scaleX, @scaleY, @shopZoneId, @name, @size, @angleA, @angleB, @shape, 0, 1)");
                        break;
                    case 'delete':
                        if (table.status === 0) {
                            query.push({
                                "sql": "delete from diningTable where uid = @uid", "args": [
                                    { "c": "uid", "t": TYPES.VarChar, "v": table.code }
                                ]
                            }); // query
                        } else {
                            console.log(table.status);
                            message.warning = 'Table is in used. Will not delete.';
                        }
                        break;
                    default:
                        break;
                }
            });
        }

        // trace
        query.forEach((query) => {
            console.log(query.sql)

            query.args.forEach((arg) => {
                console.log(arg);
            });
        });
        // console.log(testquery);
        // var query = [];
        this.DM.executeStatement(query, (err, result) => {
            this.LoadTableSchema(null, (err, result) => {
                io.sockets.emit('refreshTable', JSON.stringify(result));
            })
            message.result = 'OK';
            callback(message);
        })
    }

    this.showKds = (kds, updateListMode) => {
        console.log("showKds", kds);
        if (updateListMode == undefined) updateListMode = 0;
        var topic = 'wo/kds/';
        var query = { "sql": "select * from kds where kds is not null and (kdsstatus = 0 or kdsstatus is null)  order by time,tablecode,tablesubcode,orderNo,seq,type ", "args": null }
        if (kds) {
            query.sql = "select * from kds where kds = @kds and (kdsstatus = 0 or kdsstatus is null)  order by time,tablecode,tablesubcode,orderNo,seq,type ";
            query.args = [{ "c": "kds", "t": TYPES.VarChar, "v": kds }]
            topic = 'wo/kds/' + kds + '/';
        }
        if (updateListMode == 1) {
            query.sql = query.sql.replace("(kdsstatus = 0 or kdsstatus is null)", "(kdsstatus = 2)");
            query.sql = query.sql.replace("order by time", "order by time desc");
        }
        else if (updateListMode == 2) {
            //kdsStatus = 0: Uncalled
            //kdsStatus = 1: Called
            //kdsStatus = 2: Completed
            //kdsStatus = 3: Skipped
            query.sql = query.sql.replace("kds where kds = @kds and (kdsstatus = 0 or kdsstatus is null)", "kdsOrders where (kdsStatus != 2 and kdsStatus != 3) or ((kdsStatus = 2 or kdsStatus = 3) and dismissTime > @cutofftime and dismissTime <= @nowtime)");
            query.sql = query.sql.replace("order by time,tablecode,tablesubcode,orderNo,seq,type", "order by time,orderNo");
            var now = new Date();
            var nowtime = new Date(now.getTime() + 6 * 1000);
            var cutofftime = new Date(now.getTime() - 30 * 60000);
            query.args.push({ "c": "cutofftime", "t": TYPES.Time, "v": cutofftime });
            query.args.push({ "c": "nowtime", "t": TYPES.Time, "v": nowtime });
            topic = 'wo/kds/KDS1/';
        }
        else if (updateListMode == 3) {
            //kdsStatus = 0: Uncalled
            //kdsStatus = 1: Called
            //kdsStatus = 2: Completed
            query.sql = query.sql.replace("(kdsstatus = 0 or kdsstatus is null)", "(kdsstatus = 0 or kdsstatus is null or kdsstatus = 1)");
        }
        this.DM.executeStatement(query, (err, result) => {
            console.log("updateListMode", updateListMode);
            var obj = {
                "action": "broadcastTicketList",
                "list": [],
                "updateListMode": updateListMode
            };
            for (var idx = 0; idx < result.length; idx++) {
                var row = result[idx];
                var itemType = row.type;
                var itemCode = row.itemId;
                var itemTime = moment(row.time).format("HH:mm:ss");
                if (config.fireCode && itemCode == config.fireCode) {
                    continue;
                }
                var items = [];
                //console.log(JSON.stringify(row));
                items = getItemByItem(row);
                // console.log("TEST", items);

                var PrinterGroup = "";
                console.log(JSON.stringify(items))
                if (updateListMode != 2) {
                    if (items[0].printerGroup) {
                        PrinterGroup = items[0].printerGroup;
                    }
                }
                function addNewItem() {
                    if (updateListMode == 2) {
                        obj.list.push({
                            Id: row.orderNo, OrderNo: row.orderNo.substring(row.orderNo.length - 3, row.orderNo.length), Time: itemTime, KdsStatus: row.kdsStatus, dismissTime: moment(row.dismissTime).format("HH:mm:ss")
                        })
                    }
                    else {
                        if (row.tableCode != null) {
                            obj.list.push({
                                Id: row.tableCode + "_" + row.tableSubCode + "_" + itemTime, TableNo: row.tableCode, TableSubCode: row.tableSubCode, OrderNo: row.orderNo,
                                Item: [],
                                Modifier: []
                            })
                            if (itemType == "M" || itemType == "K") {
                                obj.list[obj.list.length - 1].Modifier.push({ Name: items[0].namek, Qty: row.qty, Printer: row.printer, PrinterGroup: PrinterGroup, Code: row.itemId, Time: itemTime, Msg: "", Seq: row.seq, Round: row.round, VoidIndex: row.voidIndex, Group: row.kdsGroup, KdsStatus: row.kdsStatus });
                            } else {
                                obj.list[obj.list.length - 1].Item.push({ Name: items[0].namek, Qty: row.qty, Printer: row.printer, PrinterGroup: PrinterGroup, Code: row.itemId, Time: itemTime, Msg: "", Seq: row.seq, Round: row.round, VoidIndex: row.voidIndex, Group: row.kdsGroup, KdsStatus: row.kdsStatus });
                            }
                        }
                        else {
                            obj.list.push({
                                Id: row.orderNo, TableNo: row.tableCode, TableSubCode: row.tableSubCode, OrderNo: row.orderNo.substring(row.orderNo.length - 3, row.orderNo.length),
                                Item: [],
                                Modifier: []
                            })
                            if (itemType == "M" || itemType == "K") {
                                obj.list[obj.list.length - 1].Modifier.push({ Name: items[0].namek, Qty: row.qty, Printer: row.printer, PrinterGroup: PrinterGroup, Code: row.itemId, Time: itemTime, Msg: "", Seq: row.seq, Round: row.round, VoidIndex: row.voidIndex, Group: row.kdsGroup, KdsStatus: row.kdsStatus });
                            } else {
                                obj.list[obj.list.length - 1].Item.push({ Name: items[0].namek, Qty: row.qty, Printer: row.printer, PrinterGroup: PrinterGroup, Code: row.itemId, Time: itemTime, Msg: "", Seq: row.seq, Round: row.round, VoidIndex: row.voidIndex, Group: row.kdsGroup, KdsStatus: row.kdsStatus });
                            }
                        }
                    }
                }

                var matchingIdIndex = -1;
                for (var j = 0; j < obj.list.length; j++) {
                    if (row.tableCode != null) {
                        if (obj.list[j].Id == row.tableCode + "_" + row.tableSubCode + "_" + itemTime) {
                            matchingIdIndex = j;
                            break;
                        }
                    }
                    else {
                        if (obj.list[j].Id == row.orderNo) {
                            matchingIdIndex = j;
                            break;
                        }
                    }
                }
                if (idx == 0) {
                    addNewItem();
                    // } else if (obj.list[obj.list.length - 1].Id == row.tableCode + "_" + row.tableSubCode + "_" + row.round) {
                } else if (matchingIdIndex != -1) {
                    if (updateListMode == 2) {

                    }
                    else {
                        if (itemType == "M" || itemType == "K") {
                            // obj.list[obj.list.length - 1].Modifier.push({ Name: items[0].namek, Qty: row.qty, Printer: row.printer, PrinterGroup: PrinterGroup, Code: row.itemId, Time: itemTime, Msg: "", Seq: row.seq, Round: row.round, VoidIndex: row.voidIndex, Group: row.kdsGroup });
                            obj.list[matchingIdIndex].Modifier.push({ Name: items[0].namek, Qty: row.qty, Printer: row.printer, PrinterGroup: PrinterGroup, Code: row.itemId, Time: itemTime, Msg: "", Seq: row.seq, Round: row.round, VoidIndex: row.voidIndex, Group: row.kdsGroup, KdsStatus: row.kdsStatus });
                        } else {
                            // obj.list[obj.list.length - 1].Item.push({ Name: items[0].namek, Qty: row.qty, Printer: row.printer, PrinterGroup: PrinterGroup, Code: row.itemId, Time: itemTime, Msg: "", Seq: row.seq, Round: row.round, VoidIndex: row.voidIndex, Group: row.kdsGroup });
                            obj.list[matchingIdIndex].Item.push({ Name: items[0].namek, Qty: row.qty, Printer: row.printer, PrinterGroup: PrinterGroup, Code: row.itemId, Time: itemTime, Msg: "", Seq: row.seq, Round: row.round, VoidIndex: row.voidIndex, Group: row.kdsGroup, KdsStatus: row.kdsStatus });
                        }
                    }
                } else {
                    addNewItem();
                }
            }
            //console.log("showkds");
            //console.log(JSON.stringify(obj));
            console.log("TEST11", topic);
            mqttClient.publish(topic, JSON.stringify(obj), { qos: 2 });
        })
    }

    this.broadcastTicketList = (args, updateListMode) => {
        console.log("broadcastTicketList");
        var printer = args.split('/')[2];
        this.showKds(printer, updateListMode);
    }

    this.PrintItem = (args) => {
        //{
        //    "action": "printItem",
        //    "round": 2,
        //    "seq": 4,
        //    "tableNo": "200",
        //    "tableSubCode": ""
        //}

        var _o = JSON.parse(args.payload);
        // console.log(_o);
        var updateListMode = _o.updateListMode;
        // console.log(_o);
        var query = [];
        _o.printList.forEach((item) => {
            if (item.tableOrder) {
                query.push(
                    {
                        "sql": "select d.*,o.pax from tableorderd d inner join tableorder o on o.tableCode = d.tableCode and o.tableSubCode = d.tableSubCode where round = @round and seq = @seq and o.tableCode = @tableCode and o.tableSubCode = @tableSubCode order by o.tableCode,o.tableSubCode,round,seq,type  ; \
                        update tableorderd set kdsstatus = 2,kdsTime = getdate() where round = @round and seq = @seq and tableCode = @tableCode and tableSubCode = @tableSubCode ; \
                        update kds set kdsstatus = 2,kdsTime = getdate() where round = @round and seq = @seq and tableCode = @tableCode and tableSubCode = @tableSubCode ",
                        "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": item.tableNo }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": item.tableSubCode }, { "c": "round", "t": TYPES.Int, "v": item.round }, { "c": "Seq", "t": TYPES.Int, "v": item.seq }]
                    }
                );
            }
            else {
                query.push(
                    {
                        "sql": "select d.*,o.pax from transactionD d inner join [transaction] o on o.refNo = d.refNo where round = @round and seq = @seq and o.refNo = @refNo order by o.refNo,round,seq,type  ; \
                    update kds set kdsstatus = 2,kdsTime = getdate() where round = @round and seq = @seq and orderNo = @refNo ",
                        "args": [{ "c": "refNo", "t": TYPES.VarChar, "v": item.orderNo }, { "c": "round", "t": TYPES.Int, "v": item.round }, { "c": "Seq", "t": TYPES.Int, "v": item.seq }]
                    }
                );
            }
        })

        this.DM.executeStatement(query, (err, result) => {
            //console.log(result);
            console.log("test: " + updateListMode);
            var printer = args.topic.split('/')[2];
            this.showKds(printer, updateListMode);
            //console.log(result);
            //sortDbItem(result);
            //this.PrintFoodItem(printingArray);
            //function writePrintingFiles(printingArray, printIndex) {
            //    fs.writeFile("./printer/" + moment().format("YYMMDDHHmmssSSSS") + "_" + printIndex + ".json", JSON.stringify(printingArray), function (err) {
            //        if (err) {
            //            //console.log(err);
            //        } else {
            //            ////console.log("The file was saved!");
            //        }
            //    });
            //}

            function AddItemToPrintingArray(printingArray, itemArray) {
                //for (var i = 0; i < result[j].length; i++) {
                //console.log(JSON.stringify(itemArray));
                sortDbItem(itemArray);
                for (var i = 0; i < itemArray.length; i++) {
                    //console.log('start loop item');
                    var r = itemArray[i];
                    var itemType = r.type;
                    var itemCode = r.itemId;
                    var items = [];
                    items = getItemByItem(r);
                    //console.log('second', JSON.stringify(items));
                    var printer = items[0].printer;
                    if (items[0].printerGroup) {
                        printer += "*" + items[0].printerGroup;
                    }
                    printingArray.FoodItem.push({ Name: items[0].namek, Qty: r.qty, Printer: printer, Code: items[0].code, Time: moment(r.time).format("HH:mm:ss"), Msg: '', individualPrint: items[0].individualPrint, Type: itemType });

                }
            }

            function pfis(f, timeout) {
                setTimeout(function () {
                    if (!config.fireDineInMode)
                        RAS.PrintFoodItemString(f)
                }, timeout);
            }

            var printingArray = { TableNo: '', Pax: 0, FoodItem: [] };
            //var lastPrintingTable = { TableNo: '', FoodItem: [] };
            var timeout = 0;
            var printIndex = 0;
            console.log("result: ", result);
            for (var j = 0; j < result.length; j++) {
                var tableNo;
                if (result[j][0].tableCode == null) {
                    tableNo = result[j][0].refNo;
                }
                else {
                    tableNo = result[j][0].tableCode;
                    if (result[j][0].tableSubCode != '') {
                        tableNo += "_" + result[j][0].tableSubCode;
                    }
                }


                if (printingArray.TableNo == tableNo) {
                    AddItemToPrintingArray(printingArray, result[j]);
                } else {
                    if (printingArray.TableNo != '' && printingArray.FoodItem.length != 0) {
                        //writePrintingFiles(printingArray, printIndex);
                        //this.PrintFoodItem(printingArray);
                        //setTimeout(function () { alert("Hello"); }, 3000);
                        //var pa = extend2({}, printingArray);
                        //console.log('b4 pfis' + JSON.stringify(printingArray));
                        pfis(JSON.stringify(printingArray), timeout);
                        //pa.push(JSON.stringify(printingArray));
                        timeout += 1000;
                        //setTimeout(function () { RAS.PrintFoodItemString(pa[i]) }, timeout);
                        printIndex++;
                    }
                    printingArray = { TableNo: '', Pax: 0, FoodItem: [] };
                    printingArray.TableNo = tableNo;
                    printingArray.Pax = result[j][0].pax;
                    AddItemToPrintingArray(printingArray, result[j]);
                }
            }
            if (printingArray.FoodItem.length != 0) {
                //writePrintingFiles(printingArray, printIndex);
                //console.log('b4 pfis' + JSON.stringify(printingArray));
                //this.PrintFoodItem(printingArray);
                //var a = JSON.stringify(printingArray);

                //setTimeout(function () { RAS.PrintFoodItemString(JSON.stringify(a)); }, timeout);
                pfis(JSON.stringify(printingArray), timeout);
            }

        })
    }

    this.lockGroup = (args) => {
        ////console.log(args.payload);
        var payload = JSON.parse(args.payload);
        //console.log('=========================');
        var query = [];
        for (var i = 0; i < payload.DishList.length; i++) {
            var _o = payload.DishList[i];
            query.push({
                "sql": "update tableorderd set kdsGroup = @time where tableCode = @tableCode and tableSubCode = @tableSubCode and round = @round and seq = @seq ; \
                update kds set kdsGroup = @time where tableCode = @tableCode and tableSubCode = @tableSubCode and round = @round and seq = @seq",
                "args": [{ "c": "time", "t": TYPES.BigInt, "v": new Date().getTime() }, { "c": "tableCode", "t": TYPES.VarChar, "v": _o.TableNo }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": _o.TableSubCode }, { "c": "round", "t": TYPES.Int, "v": _o.Round }, { "c": "Seq", "t": TYPES.Int, "v": _o.Seq }]
            })
        }
        this.DM.executeStatement(query, (err, result) => {
            //console.log(err);
            var printer = args.topic.split('/')[2];
            this.showKds(printer);
        }, 'parallel')

    }

    this.ChangeStatus = (args) => {
        console.log("change status" + args.payload);
        var _o = JSON.parse(args.payload);
        //console.log(_o);
        var query = [];
        if (_o.updateListMode == 2) {
            var curQuery = {
                "sql": "update kdsOrders set kdsStatus = @kdsstatus, calledTime, dismissTime where orderNo = @refNo ; \
                                    select * from kdsOrders where orderNo = @refNo", "args": [{ "c": "refNo", "t": TYPES.VarChar, "v": _o.orderId }, { "c": "kdsstatus", "t": TYPES.Int, "v": _o.newStatus }]
            };
            if (_o.newStatus == 0) {
                curQuery.sql = curQuery.sql.replace("calledTime", "calledTime = null");
                curQuery.sql = curQuery.sql.replace("dismissTime", "dismissTime = null");
            }
            else if (_o.newStatus == 1) {
                curQuery.sql = curQuery.sql.replace("calledTime", "calledTime = getdate()");
                curQuery.sql = curQuery.sql.replace("dismissTime", "dismissTime = null");
            }
            else if (_o.newStatus == 2) {
                curQuery.sql = curQuery.sql.replace("calledTime, ", "");
                curQuery.sql = curQuery.sql.replace("dismissTime", "dismissTime = getdate()");
            }
            else if (_o.newStatus == 3) {
                curQuery.sql = curQuery.sql.replace("calledTime", "calledTime = null");
                curQuery.sql = curQuery.sql.replace("dismissTime", "dismissTime = getdate()");
            }
            query.push(curQuery);
        }
        else if (_o.updateListMode == 3) {
            var curQuery = {
                "sql": "update kds set kdsStatus = @kdsstatus where orderNo = @refNo and time = @time; \
                        select * from kds where orderNo = @refNo and time = @time",
                "args": [
                    { "c": "refNo", "t": TYPES.VarChar, "v": _o.orderId },
                    { "c": "kdsstatus", "t": TYPES.Int, "v": _o.newStatus },
                    { "c": "time", "t": TYPES.VarChar, "v": _o.time }
                ]
            };
            query.push(curQuery)
        }
        else {
            _o.pendingList.forEach((item) => {
                query.push({
                    "sql": "update tableorderd set kdsstatus = @kdsstatus,kdsTime = getdate() where round = @round and seq = @seq and tableCode = @tableCode and tableSubCode = @tableSubCode ; \
                                update kds set kdsstatus = @kdsstatus,kdsTime = getdate() where round = @round and seq = @seq and tableCode = @tableCode and tableSubCode = @tableSubCode ; \
                                select * from kds where round = @round and seq = @seq and tableCode = @tableCode and tableSubCode = @tableSubCode"
                , "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": item.tableNo }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": item.tableSubCode }, { "c": "round", "t": TYPES.Int, "v": item.round }, { "c": "Seq", "t": TYPES.Int, "v": item.seq }, { "c": "kdsstatus", "t": TYPES.Int, "v": _o.newStatus }]
                });
            })
        }

        this.DM.executeStatement(query, (err, result) => {
            console.log("changeStatus results: ", result);
            if (_o.updateListMode == 2) {
                result.forEach((subResult) => {
                    subResult.forEach((item) => {
                        console.log("changeStatus item: ", item);
                        if (!item.tableOrder) {
                            var orderNo = item.orderNo.substr(item.orderNo.length - 3);
                            if (_o.newStatus == 1) {
                                PickUp.editNumber("add", item.orderNo);
                            }
                            else {
                                PickUp.editNumber("remove", item.orderNo);
                            }
                        }
                    })
                })
            }

            var printer = args.topic.split('/')[2];
            this.showKds(printer, _o.updateListMode);
            if (_o.updateListMode == 2) {
                this.showKds(printer, 0);
            }
        })
    }

    this.SkipAllCallNo = (args) => {
        console.log("SkipAllCallNo" + args.payload);
        var _o = JSON.parse(args.payload);
        var query = [{
            "sql": "select * from kdsOrders where kdsStatus = 1; \
                    update kdsOrders set kdsStatus = 2, dismissTime = getdate() where kdsStatus = 1; \
                    update kdsOrders set kdsStatus = 3, calledTime = null, dismissTime = getdate() where kdsStatus = 0;",
            "args": []
        }];


        this.DM.executeStatement(query, (err, result) => {
            console.log("SkipAllCallNo results: ", result);
            result.forEach((subResult) => {
                subResult.forEach((item) => {
                    console.log("changeStatus item: ", item);
                    if (!item.tableOrder) {
                        var orderNo = item.orderNo.substr(item.orderNo.length - 3);
                        PickUp.editNumber("remove", item.orderNo);
                    }
                })
            })

            var printer = args.topic.split('/')[2];
            this.showKds(printer, _o.updateListMode);
            if (_o.updateListMode == 2) {
                this.showKds(printer, 0);
            }
        })
    }

    this.SetTableStatus = (args, callback) => {
        this.DM.executeStatement({ "sql": "update diningtable set status = @status where name = @tableCode;", "args": [{ "c": "status", "t": TYPES.VarChar, "v": args.status }, { "c": "tableCode", "t": TYPES.VarChar, "v": args.tableCode }] }, (err, result) => {
            //console.log("SetTableStatus");
            //console.log(result);
            this.LoadTableSchema(null, (err, result) => {
                io.sockets.emit('refreshTable', JSON.stringify(result));
            })
        })
    }

    this.GetOutOfStock = (callback) => {
        this.DM.executeStatement({ "sql": "select itemCode as code ,qty from itemControl ", "args": [] }, (err, result) => {
            if (err) {
                //console.log("Error executing query:", err); return; 
            }
            ////console.log("item control:" + JSON.stringify(result));
            this.OutOfStock = result;
            callback(err, result);
        });
    }

    this.SetItemQty = (args, callback) => {
        console.log('SetItemQty');
        console.log(JSON.stringify(args));
        var query = {};
        if (args.qty == -2) {
            query = { "sql": "delete itemControl where itemCode = @itemCode", "args": [{ "c": "itemCode", "t": TYPES.VarChar, "v": args.itemCode }] };
        } else {
            query = {
                "sql": "MERGE INTO itemControl s USING (SELECT @itemCode as  itemCode, @qty as qty, @remark as remark, @staffId as staffId ) i ON (s.itemCode = i.itemCode) WHEN MATCHED THEN UPDATE SET s.qty = i.qty,s.remark = i.remark WHEN NOT MATCHED THEN INSERT (itemCode, qty,remark,staffId) VALUES (i.itemCode, i.qty, i.remark, i.staffId);",
                "args": [{ "c": "itemCode", "t": TYPES.VarChar, "v": args.itemCode }, { "c": "qty", "t": TYPES.Int, "v": args.qty }, { "c": "remark", "t": TYPES.VarChar, "v": args.remark }, { "c": "staffId", "t": TYPES.VarChar, "v": args.staffId }]
            };
        }
        this.DM.executeStatement(query, (err, result) => {
            //console.log(result);
            this.GetOutOfStock((err, outOfStock) => {
                console.log('GetOutOfStock');
                callback(outOfStock);
                io.sockets.emit('refreshItemQty', JSON.stringify(outOfStock));
                var _o = { "action": "refreshItemQty", "outOfStock": outOfStock }
                mqttClient.publish("wo/gm/all", JSON.stringify(_o));
            })
        });
    }

    this.UpdateItemQty = () => {
        console.log("UpdateItemQty");
        this.GetOutOfStock((err, outOfStock) => {
            console.log("OutOfStock: ", outOfStock);
            io.sockets.emit('refreshItemQty', JSON.stringify(outOfStock));
            var _o = { "action": "refreshItemQty", "outOfStock": outOfStock }
            mqttClient.publish("wo/gm/all", JSON.stringify(_o));
        })
    }

    //var transSeq = 0;
    this.PrintPaymentBill = (args, callback) => {
        var refNo = args.refNo;

        var isPrintTwice = config.paymentPrintTwice;
        var takeawayBillReceipt = false;

        if (args.isCalledByTakeaway) {
            isPrintTwice = config.takeAwayPrintTwice;
            takeawayBillReceipt = config.tills[args.till].takeawayBillReceipt;
            console.log(5388, takeawayBillReceipt)
        }


        if (args.reprint != undefined) {
            if (args.reprint) {
                isPrintTwice = false;
            }
        }
        //args.printTwice ? args.printTwice : false;
        //isPrintTwice = false;

        var printer = args.printer ? args.printer : config.defaultPrinter;
        var till = args.till ? args.till : '';

        //console.log(printer);
        var isOpenCashDrawer = args.openCashDrawer ? args.openCashDrawer : false;
        query = [{ "sql": "select t.*,s.name,e.Resp_Code,e.Resp_Text,e.Card_Type,e.Card_No,e.Terminal_Number,e.Trace_Number,e.Approval_Code from [transaction] t left outer join staff s on t.staffId = s.staffId left outer join ecrTransaction e on t.refNo = e.refNo where t.refNo = @refNo;", "args": [{ "c": "refNo", "t": TYPES.VarChar, "v": refNo }] },
                 { "sql": "select * from [transactionD] where refNo = @refNo order by round , seq, type; ", "args": [{ "c": "refNo", "t": TYPES.VarChar, "v": refNo }] },
                 { "sql": "select * from [tenderpayment] where refNo = @refNo and void = 0 order by seq;", "args": [{ "c": "refNo", "t": TYPES.VarChar, "v": refNo }] },
                 { "sql": "select * from [tenderdiscount] where refNo = @refNo and void = 0 order by seq;", "args": [{ "c": "refNo", "t": TYPES.VarChar, "v": refNo }] }]
        this.DM.executeStatement(query, (err, transaction) => {
            //console.log(transaction);
            console.log(isPrintTwice);
            var _obj = transaction[0][0];

            //console.log('5131', _obj);
            //print split table (tableCode)
            //if (_obj.tableSubCode != '') _obj.tableCode += "_" + _obj.tableSubCode;
            _obj._void = _obj.void;
            _obj.isPrintTwice = isPrintTwice;
            console.log(_obj.isPrintTwice);
            _obj.isOpenCashDrawer = isOpenCashDrawer;
            _obj.printer = printer;
            _obj.till = till;
            _obj.items = [];

            var multiPayment = false;

            if (_obj.pax == null) {
                _obj.pax = 0;
            }
            sortDbItem(transaction[1]);
            var lastIsFreeCombo = false;
            var lastItemQty = 0;
            for (var i = 0; i < transaction[1].length; i++) {
                var currentItem = transaction[1][i];
                var items = [];
                items = getItemByItem(currentItem);

                var name1, name2, name3;
                name1 = name2 = name3 = "";

                if (items.length > 0) {
                    var name1 = items[0].name1;
                    var name2 = items[0].name2;
                    var name3 = items[0].name3;
                    if (items[0].name != undefined) {
                        if (items[0].name[config.lang1] != undefined)
                            name1 = items[0].name[config.lang1];
                        if (items[0].name[config.lang2] != undefined)
                            name2 = items[0].name[config.lang2];
                        if (items[0].name[config.lang3] != undefined)
                            name3 = items[0].name[config.lang3];
                    }
                }
                if (currentItem.customName != "") {
                    name1 = name2 = name3 = currentItem.customName;
                }

                /*if (name1 != "") {
                    var nameLenght = 25 - countDoubleByteChar(name1);
                    name1 = S(name1).padRight(nameLenght).s
                }*/

                //*********************************************
                var itemTotalPrice = 0;

                if (currentItem.type == "I" || currentItem.type == "T") {
                    lastItemQty = currentItem.qty;
                    itemTotalPrice = lastItemQty * currentItem.price;
                } else {
                    itemTotalPrice = lastItemQty * currentItem.price * currentItem.qty;
                }

                var itp = S("$" + itemTotalPrice.format(1, 3)).padLeft(10).s;
                var up = S("$" + currentItem.price.format(1, 3)).padLeft(10).s;

                if (currentItem.type == "I") {
                    if (itemTotalPrice == 0) {
                        lastIsFreeCombo = true;
                    } else {
                        lastIsFreeCombo = false;
                    }
                }

                if (currentItem.type != "K" && currentItem.type != "T" && !(currentItem.type == "I" && itemTotalPrice == 0 && !config.printZeroPriceItem) && !(lastIsFreeCombo && !config.printZeroPriceItem)) {
                    if (currentItem.itemId != 'O9999') {
                        console.log('test name2', name1, name2, name3);
                        _obj.items.push({ code: currentItem.itemId, type: currentItem.type, desc1: name1, desc2: name2, desc3: name3, qty: currentItem.qty, unitPrice: up, itemTotalPrice: itp, price: currentItem.price, time: currentItem.time, voidIndex: currentItem.voidIndex, voidRemark: currentItem.voidRemark, staff: currentItem.staffId, index: currentItem.seq, lastItemQty: lastItemQty });

                    }
                }
                //*********************************************

                //var itemTotalPrice = currentItem.qty * currentItem.price;
                //var itp = S("$" + itemTotalPrice.format(1, 3)).padLeft(10).s;

                //var up = S("$" + currentItem.price.format(1, 3)).padLeft(10).s;

                //_obj.items.push({ code: currentItem.itemId, type: currentItem.type, desc1: name1, desc2: name2, desc3: name3, qty: currentItem.qty, unitPrice: up, itemTotalPrice: itp, time: currentItem.time, voidIndex: currentItem.voidIndex, voidRemark: currentItem.voidRemark, staff: currentItem.staffId, index: currentItem.seq });

            }
            console.log(5402, JSON.stringify(_obj.items));
            _obj.payment = [];

            var changeValue = 0;
            var tipsValue = 0;


            for (var i = 0; i < transaction[2].length; i++) {
                var _o = transaction[2][i];
                var payMsg = _o.paymentType;
                var tenderObj = this.tender.filter((t) => { return t.tenderCode == _o.paymentType })[0];

                var priceValue = null;
                if (_o.priceValue != null) {
                    multiPayment = true;
                    S("$" + _o.priceValue.format(1, 3)).padLeft(10).s;
                }

                if (multiPayment) {
                    if (_o.paymentType == 'cash') changeValue += _o.paymentValue - _o.priceValue;
                    else tipsValue += _o.paymentValue - _o.priceValue;
                }

                _obj.payment.push({ "paymentType": tenderObj.tenderName1, "paymentValue": S("$" + _o.paymentValue.format(1, 3)).padLeft(10).s, "remark": _o.remark, "paymentCode": _o.paymentType, "priceValue": priceValue })
            }

            var totaldiscount = 0;
            _obj.discount = [];
            for (var i = 0; i < transaction[3].length; i++) {
                var _o = transaction[3][i];
                var discountMsg = _o.discountCode;

                this.discount.forEach((d) => {
                    if (d.discountCode == _o.discountCode) {
                        discountMsg = d.discountName1;
                    }
                })
                var dvForPrint = "$" + _o.discountValue.format(1, 3);
                if (_o.discountCode == 'servicecharge') {
                    dvForPrint = "$" + (_o.discountValue * -1).format(1, 3);
                }
                if (_o.discountValue > 0) {
                    dvForPrint = "-" + dvForPrint;
                }

                _obj.discount.push({ "discountCode": discountMsg, "discountValue": S(dvForPrint).padLeft(10).s })
                totaldiscount += _o.discountValue;
            }
            var tp = _obj.price + _obj.serviceCharge - totaldiscount + _obj.remainings;
            var rp = _obj.payAmount - tp;

            var cloud = {};
            cloud.payAmount = _obj.payAmount;
            cloud.price = _obj.price;
            cloud.serviceCharge = _obj.serviceCharge;
            cloud.remainings = _obj.remainings;


            _obj.totalPrice = S("$" + tp.format(1, 3)).padLeft(10).s;
            _obj.returnPrice = S("$" + rp.format(1, 3)).padLeft(10).s;

            //_obj.changePrice = S("$")

            _obj.payAmount = S("$" + _obj.payAmount.format(1, 3)).padLeft(10).s;
            _obj.serviceCharge = S("$" + _obj.serviceCharge.format(1, 3)).padLeft(10).s;


            _obj.changeValue = S("$" + changeValue.format(1, 3)).padLeft(10).s;
            _obj.tipsValue = S("$" + tipsValue.format(1, 3)).padLeft(10).s;

            _obj.remainings = S("$" + _obj.remainings.format(1, 3)).padLeft(10).s;
            _obj.price = S("$" + _obj.price.format(1, 3)).padLeft(10).s;

            if (!config.PrintVoidItem) {
                var filterVoidItemList = [];
                _obj.items.forEach(function (food) {
                    //if (food.type == "M" || food.type == "K") return true;
                    if (food.type == "K") return true;
                    food.Tqty = food.qty;
                    if (food.type == "I" || food.type == "T") {
                        var tempVoidItem = {};
                        tempVoidItem.Package = food;
                        tempVoidItem.options = [];
                        filterVoidItemList.push(tempVoidItem);
                    }
                    else {
                        food.Tqty = filterVoidItemList[filterVoidItemList.length - 1].Package.qty;
                        filterVoidItemList[filterVoidItemList.length - 1].options.push(food);
                    }
                });

                filterVoidItemList.forEach(function (food) {
                    if (food.Package.voidIndex != '-1') {
                        var tempFood = filterVoidItemList.filter(function (voidItem) {
                            return voidItem.Package.index == food.Package.voidIndex;
                        })
                        tempFood[0].Package.qty = tempFood[0].Package.qty + food.Package.qty;
                        var tempItemPrice = tempFood[0].Package.qty * tempFood[0].Package.unitPrice.split('$')[1];
                        tempFood[0].Package.itemTotalPrice = S("$" + tempItemPrice.format(1, 3)).padLeft(10).s
                        //tempFood[0].Package.itemTotalPrice = tempFood[0].Package.itemTotalPrice - food.Package.itemTotalPrice;
                        tempFood[0].options.forEach(function (opts) {
                            //opts.qty = opts.qty + food.Package.qty;
                            opts.Tqty = opts.Tqty + food.Package.Tqty;
                            tempItemPrice = opts.Tqty * opts.unitPrice.split('$')[1];
                            opts.itemTotalPrice = S("$" + tempItemPrice.format(1, 3)).padLeft(10).s
                        });
                    }
                })

                filterVoidItemList = filterVoidItemList.filter(function (food) {
                    return food.Package.qty > 0;
                })

                _obj.items = [];
                filterVoidItemList.forEach(function (food) {
                    _obj.items.push(food.Package);
                    food.options.forEach(function (opts) {
                        _obj.items.push(opts);
                    })
                })
            }

            console.log(5524, JSON.stringify(_obj.items));
            //comboPrice
            var currentItem = {};
            var currentCombo = null;
            var comboPriceOptionGroupCode = [];
            if (config.comboPriceOptionGroupCode) {
                comboPriceOptionGroupCode = config.comboPriceOptionGroupCode;
            }
            _obj.items.forEach(function (food) {
                console.log("..::");
                console.log(JSON.stringify(food));
                if (food.type == "I") {
                    currentItem = food;
                    currentCombo = null;
                } else if (food.type == "O") {
                    var ro = RAS.food.category.forEach((c) => {
                        c.items.forEach((i) => {
                            if (i.itemId == currentItem.code) {
                                currentCombo = i;
                                return true;
                            }
                        })
                        if (currentCombo) return true;
                    })
                    console.log(currentCombo);
                    if (currentCombo) {
                        currentCombo.option.forEach((o) => {
                            var minPrice = 0;
                            var foundOption = false;
                            var haveZeroOption = false;
                            var foundItemPrice = 0;
                            console.log("f" + JSON.stringify(food));
                            console.log(food.code);

                            o.items.forEach((co) => {

                                //console.log("cooitemid:" + JSON.stringify(co));
                                if (co.unitprice > 0 && ((co.unitprice < minPrice && minPrice != 0) || (minPrice == 0))) {
                                    minPrice = co.unitprice;
                                }
                                if (co.unitprice == 0) {
                                    haveZeroOption = true;
                                }
                                if (co.code == food.code) {
                                    foundOption = true;
                                    foundItemPrice = co.unitprice;
                                }
                            })

                            if (foundOption) {
                                if (minPrice > 0 && !haveZeroOption && comboPriceOptionGroupCode.indexOf(o.optionGroupCode) != -1) {
                                    minPrice = minPrice / 100;
                                    currentItem.price += minPrice;
                                    food.price -= minPrice;

                                    currentItem.itemTotalPrice = S("$" + (currentItem.lastItemQty * currentItem.price).format(1, 3)).padLeft(10).s;
                                    currentItem.unitPrice = S("$" + currentItem.price.format(1, 3)).padLeft(10).s;
                                    food.itemTotalPrice = S("$" + (food.lastItemQty * food.price * food.qty).format(1, 3)).padLeft(10).s;
                                    food.unitPrice = S("$" + food.price.format(1, 3)).padLeft(10).s

                                }

                                /*
                                if (minPrice == 0) {
                                    food.price -= minPrice;

                                    food.itemTotalPrice = S("$" + (food.lastItemQty * food.price * food.qty).format(1, 3)).padLeft(10).s;
                                    food.unitPrice = S("$" + food.price.format(1, 3)).padLeft(10).s

                                } else {
                                    minPrice = minPrice / 100;
                                    currentItem.price += minPrice;
                                    food.price -= minPrice;

                                    currentItem.itemTotalPrice = S("$" + (currentItem.lastItemQty * currentItem.price).format(1, 3)).padLeft(10).s;
                                    currentItem.unitPrice = S("$" + currentItem.price.format(1, 3)).padLeft(10).s;
                                    food.itemTotalPrice = S("$" + (food.lastItemQty * food.price * food.qty).format(1, 3)).padLeft(10).s;
                                    food.unitPrice = S("$" + food.price.format(1, 3)).padLeft(10).s
                                }*/
                            }

                        });
                        //currentCombo.option[0]
                    }
                }
            });


            //fs.writeFile("./printer/" + _obj.printer + "/payment_" + moment().format("YYMMDDHHmmss") + ".json", JSON.stringify(_obj), function (err) {
            //    if (err) {
            //        //console.log(err);
            //    } else {
            //        ////console.log("The file was saved!");
            //    }
            //});

            RAS.writeFile("./printer/" + _obj.printer + "/payment_", JSON.stringify(_obj), function (err) {

            })

            if (takeawayBillReceipt) {
                RAS.writeFile("./printer/" + takeawayBillReceipt + "/payment_", JSON.stringify(_obj), function (err) {

                })
            }

            cloud.transaction = transaction[0];
            cloud.transactionD = transaction[1];
            cloud.payment = transaction[2];
            cloud.discount = transaction[3];
            //transSeq += 1;
            //if (transSeq > 255) transSeq = 1;
            fs.writeFile("./cloud/transaction/" + args.refNo + ".json", JSON.stringify(cloud), function (err) {
                if (err) {
                    //console.log(err);
                } else {
                    ////console.log("The file was saved!");
                }
            });
            callback();

        });
    }

    this.PrintPaymentOctopus = (args, callback) => {
        var refNo = args.refNo;
        console.log("PrintPaymentOctopus", refNo);

        var isPrintTwice = config.paymentPrintTwice ? config.paymentPrintTwice : false;
        if (args.reprint != undefined) {
            if (args.reprint) {
                isPrintTwice = false;
            }
        }
        //args.printTwice ? args.printTwice : false;
        //isPrintTwice = false;

        var printer = args.printer ? args.printer : '';
        //console.log(printer);
        var isOpenCashDrawer = args.openCashDrawer ? args.openCashDrawer : false;
        query = [{ "sql": "select * from [transaction] where refNo = @refNo;", "args": [{ "c": "refNo", "t": TYPES.VarChar, "v": refNo }] },
                 { "sql": "select * from [transactionD] where refNo = @refNo order by seq; ", "args": [{ "c": "refNo", "t": TYPES.VarChar, "v": refNo }] },
                 { "sql": "select * from [tenderpayment] where refNo = @refNo and void = 0 order by seq;", "args": [{ "c": "refNo", "t": TYPES.VarChar, "v": refNo }] },
                 { "sql": "select * from [tenderdiscount] where refNo = @refNo and void = 0 order by seq;", "args": [{ "c": "refNo", "t": TYPES.VarChar, "v": refNo }] }]
        //query = [{ "sql": "select * from [tableOrder] where tableSubCode = @refNo;", "args": [{ "c": "refNo", "t": TYPES.VarChar, "v": refNo }] },
        //         { "sql": "select * from [tableOrderD] where tableSubCode = @refNo order by seq; ", "args": [{ "c": "refNo", "t": TYPES.VarChar, "v": refNo }] }]
        this.DM.executeStatement(query, (err, transaction) => {
            //console.log("transaction", transaction);
            //console.log(isPrintTwice);
            var totalPayment = 0;
            var _obj = transaction[0][0];
            _obj.pax = 0;
            _obj.isPrintTwice = isPrintTwice;
            //console.log(_obj.isPrintTwice);
            _obj.isOpenCashDrawer = isOpenCashDrawer;
            _obj.printer = printer;
            _obj.items = [];
            sortDbItem(transaction[1]);
            for (var i = 0; i < transaction[1].length; i++) {
                var currentItem = transaction[1][i];
                var items = [];
                items = getItemByItem(currentItem);
                var name1, name2, name3;
                name1 = name2 = name3 = "";

                if (items.length > 0) {
                    var name1 = items[0].name1;
                    var name2 = items[0].name2;
                    var name3 = items[0].name3;
                    if (items[0].name != undefined) {
                        if (items[0].name[config.lang1] != undefined)
                            name1 = items[0].name[config.lang1];
                        if (items[0].name[config.lang2] != undefined)
                            name2 = items[0].name[config.lang2];
                        if (items[0].name[config.lang3] != undefined)
                            name3 = items[0].name[config.lang3];
                    }
                }
                if (currentItem.customName != "") {
                    name1 = name2 = name3 = currentItem.customName;
                }

                /*if (name1 != "") {
                    var nameLenght = 25 - countDoubleByteChar(name1);
                    name1 = S(name1).padRight(nameLenght).s
                }*/

                var itemTotalPrice = currentItem.qty * currentItem.price;
                var itp = S("$" + itemTotalPrice.format(1, 3)).padLeft(10).s;

                var up = S("$" + currentItem.price.format(1, 3)).padLeft(10).s;

                _obj.items.push({ code: currentItem.itemId, type: currentItem.type, desc1: name1, desc2: name2, desc3: name3, qty: currentItem.qty, unitPrice: up, itemTotalPrice: itp, time: currentItem.time, voidIndex: currentItem.voidIndex, voidRemark: currentItem.voidRemark, staff: currentItem.staffId, index: currentItem.seq });
            }
            _obj.payment = [];


            //_obj.octopus = args.octopus;
            //_obj.octopus.cardBalance = S("$" + parseFloat( _obj.octopus.cardBalance).format(1, 3)).padLeft(10).s;

            if (transaction[2].length === 1 && transaction[2][0].paymentType === "octopus") {
                _obj.octopus = {};
                var paymentRemark = transaction[2][0].remark.split('|');
                _obj.octopus.cardBalance = S("$" + paymentRemark[0]).padLeft(10).s
                _obj.octopus.cardId = paymentRemark[1];
                _obj.octopus.machineId = paymentRemark[2];
            } else {
                for (var i = 0; i < transaction[2].length; i++) {
                    var _o = transaction[2][i];
                    var payMsg = _o.paymentType;
                    var tenderObj = this.tender.filter((t) => { return t.tenderCode == _o.paymentType })[0];

                    _obj.payment.push({ "paymentType": tenderObj.tenderName1, "paymentValue": S("$" + _o.paymentValue.format(1, 3)).padLeft(10).s, "remark": _o.remark, "paymentCode": _o.paymentType })
                    totalPayment += parseFloat(_o.paymentValue);
                }
            }


            var totaldiscount = 0;
            _obj.discount = [];
            //for (var i = 0; i < transaction[3].length; i++) {
            //    var _o = transaction[3][i];
            //    var discountMsg = _o.discountCode;

            //    this.discount.forEach((d) => {
            //        if (d.discountCode == _o.discountCode) {
            //            discountMsg = d.discountName1;
            //        }
            //    })
            //    _obj.discount.push({ "discountCode": discountMsg, "discountValue": S("$" + _o.discountValue.format(1, 3)).padLeft(10).s })
            //    totaldiscount += _o.discountValue;
            //}
            //_obj.payAmount = args.octopus.payAmount;
            _obj.payAmount = totalPayment;
            _obj.remainings = 0.0;
            var tp = _obj.price + _obj.serviceCharge - totaldiscount + _obj.remainings;
            var rp = _obj.payAmount - tp;

            _obj.totalPrice = S("$" + tp.format(1, 3)).padLeft(10).s;
            _obj.returnPrice = S("$" + rp.format(1, 3)).padLeft(10).s;


            _obj.payAmount = S("$" + _obj.payAmount.format(1, 3)).padLeft(10).s;
            _obj.serviceCharge = S("$" + _obj.serviceCharge.format(1, 3)).padLeft(10).s;


            _obj.remainings = S("$" + _obj.remainings.format(1, 3)).padLeft(10).s;
            _obj.price = S("$" + _obj.price.format(1, 3)).padLeft(10).s;

            //_obj.octopus = args.octopus;
            if (_obj.octopus)
                _obj.octopus.cardBalance = S("$" + parseFloat(_obj.octopus.cardBalance).format(1, 3)).padLeft(10).s;

            /*fs.writeFile("./printer/" + config.defaultQueuePrinter + "/octopus_" + moment().format("YYMMDDHHmmss") + ".json", JSON.stringify(_obj), function (err) {
                if (err) {
                    //console.log(err);
                } else {
                    ////console.log("The file was saved!");
                }
            });*/

            RAS.writeFile("./printer/" + config.defaultQueuePrinter + "/payment_", JSON.stringify(_obj), function (err) { });
            callback();

        });
    }

    this.BillTableOrder = (args, callback) => {

        console.log(JSON.stringify(args));



        ////console.log(args.tableNum.$value);
        var callbackObj = {};
        var tableObj = getTableCode(args.tableNum.$value);
        var tableCode = tableObj.tableCode;
        var tableSubCode = tableObj.tableSubCode;
        var noOfPeople = args.peopleNum == undefined ? '' : args.peopleNum.$value;

        args.shopCode = config.shopCode;
        args.tableObj = { tableCode: "", tableSubCode: "" };
        args.tableObj.tableCode = tableObj.tableCode;
        args.tableObj.tableSubCode = tableObj.tableSubCode;

        //console.log(args.tableObj);




        //if (tableCode.indexOf('-') != -1) {
        //    var splitTableCode = tableCode.split('-');
        //    tableCode = splitTableCode[0];
        //    tableSubCode = splitTableCode[1];
        //}
        ////console.log("payMethod:" + args.payMethod.$value)
        ////console.log("re:" + parseFloat(args.remainings.$value))
        ////console.log("re:" + args.serviceCharge.$value)
        ////console.log("re:" + parseFloat(args.serviceCharge.$value))
        ////console.log("tableCode:" + tableCode);
        ////console.log("tableSubCode:" + tableSubCode);
        var totalPayment = 0;
        var serviceCharge = parseFloat(args.serviceCharge.$value);
        var remainings = parseFloat(args.remainings.$value);

        // generate seq
        var executeArr = [];
        executeArr.push((cb) => {
            var query = {
                "sql": "declare @seq int EXEC @seq = spGetNextSeq 'bill' \
declare @sseq varchar(10) = REPLICATE('0',6-LEN(RTRIM(@seq))) + RTRIM(@seq) \
select 'invoice' = @refNo + @sseq, 'invoiceSeq' = @seq;",
                "args": [
                    { "c": "refNo", "t": TYPES.VarChar, "v": config.shopCode + moment().format("-YYMMDD-") }
                ]
            }
            this.DM.executeStatement(query, (err, result) => {
                console.log('gen seq', result);
                cb(null, result[0]);
            });
        });

        // check if has octopus
        args.payMethod.$value.forEach((payMethod) => {
            if (payMethod.method === "octopus2") {
                executeArr.push((_o, cb) => {
                    //this.DeductOctopus(extend2({ till: args.till.$value, totalPrice: payMethod.amount }, _o), (result) => {
                    //    if (result.octopusCode <= 100000) {
                    //        cb(null, _o);
                    //    }
                    //    else {
                    //        callback(result);
                    //    }
                    //});
                    var params = extend2({
                        service: this.Octopus.service.Octopus_deduct,
                        till: args.till.$value,
                        totalPrice: payMethod.amount
                    }, _o);
                    this.Octopus.connect(params, (result) => {
                        if (result.octopusCode <= 100000) {
                            _o.octopusBalance = result.cardBalance;
                            payMethod.remark = result.cardBalance + "|" + parseFloat(result.totalPrice).toFixed(2) + "|" + result.cardId + "|" + result.machineId + "|" + result.lastAddValue + "|" + result.alertMessage;
                            cb(null, _o);
                        }
                        else {
                            callback(result);
                        }
                    });
                });
            }
        });

        executeArr.push((_o, cb) => {
            //            var sql = "begin tran begin try \
            //declare @seq int EXEC @seq = spGetNextSeq 'bill' \
            //declare @sseq varchar(10) = REPLICATE('0',6-LEN(RTRIM(@seq))) + RTRIM(@seq) \
            //insert into [transaction]  (tableCode, tableSubCode, pax, staffId, memberId, memberName, startTime, refNo, transTime, payAmount, price, serviceCharge, remainings,orderid ) select tableCode, tableSubCode, pax, staffId, memberId, memberName, startTime, @refNo + @sseq as refNo, getdate(), @payAmount, @price, @serviceCharge, @remainings,orderid from tableOrder where tableCode = @tableCode and tableSubCode = @tableSubCode; ";
            var sql = "begin tran begin try \
insert into [transaction]  (tableCode, tableSubCode, pax, staffId, memberId, memberName, startTime, refNo, transTime, payAmount, price, serviceCharge, remainings,orderid, till ) select tableCode, tableSubCode, pax, staffId, memberId, memberName, startTime, @refNo as refNo, getdate(), @payAmount, @price, @serviceCharge, @remainings,orderid, @till as till from tableOrder where tableCode = @tableCode and tableSubCode = @tableSubCode; ";
            var totalPrice = parseFloat(args.price.$value);
            var sqlagrs = [{ "c": "tableCode", "t": TYPES.VarChar, "v": tableCode }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": tableSubCode }, /*{ "c": "refNo", "t": TYPES.VarChar, "v": config.shopCode + moment().format("-YYMMDD-") },*/ { "c": "refNo", "t": TYPES.VarChar, "v": _o.invoice }, { "c": "serviceCharge", "t": TYPES.Decimal, "v": serviceCharge, "o": { "scale": 2 } }, { "c": "remainings", "t": TYPES.Decimal, "v": remainings, "o": { "scale": 2 } }, { "c": "staffId", "t": TYPES.VarChar, "v": args.user.$value }];
            var PaymentMethod = new Array;
            var PaymentMethodItem = new Object;
            var paymentDate = moment().format('YYYY-MM-DD HH:mm:ss');
            sqlagrs.push({ "c": "till", "t": TYPES.VarChar, "v": args.till.$value });
            for (var i = 0; i < args.payMethod.$value.length; i++) {
                //console.log("paymentValue" + parseFloat(args.payMethod.$value[i].amount));
                //console.log("paymentValue" + args.payMethod.$value[i].method);
                var payseq = i + 1;
                //sql += "insert into tenderpayment (paymentType,paymentValue,staffId,refNo,paymentDate,void,seq,remark,till) values (@paymentType" + i.toString() + ",@paymentValue" + i.toString() + ",@staffId,@refNo + @sseq,getDate(),0," + payseq + ",@remark" + i.toString() + ",@till);";
                sql += "insert into tenderpayment (paymentType,paymentValue,priceValue,staffId,refNo,paymentDate,void,seq,remark,till) values (@paymentType" + i.toString() + ",@paymentValue" + i.toString() + ", @priceValue" + i.toString() + ",@staffId,@refNo,getDate(),0," + payseq + ",@remark" + i.toString() + ",@till);";
                sqlagrs.push({ "c": "paymentType" + i.toString(), "t": TYPES.VarChar, "v": args.payMethod.$value[i].method })
                sqlagrs.push({ "c": "paymentValue" + i.toString(), "t": TYPES.Decimal, "v": parseFloat(args.payMethod.$value[i].amount), "o": { "scale": 2 } })
                sqlagrs.push({ "c": "remark" + i.toString(), "t": TYPES.NVarChar, "v": args.payMethod.$value[i].remark ? args.payMethod.$value[i].remark : '' })
                var priceValue = args.payMethod.$value[i].totalAmount;
                if (priceValue) priceValue = parseFloat(priceValue);
                else priceValue = null;
                sqlagrs.push({ "c": "priceValue" + i.toString(), "t": TYPES.Decimal, "v": priceValue });
                totalPayment += parseFloat(args.payMethod.$value[i].amount);
                PaymentMethodItem = new Object;
                PaymentMethodItem.paymentType = args.payMethod.$value[i].method;
                PaymentMethodItem.paymentValue = parseFloat(args.payMethod.$value[i].amount);
                PaymentMethodItem.paymentDate = new Date();
                PaymentMethodItem.paymentDate = paymentDate
                PaymentMethodItem.void = 0;
                PaymentMethodItem.seq = payseq;
                PaymentMethod.push(PaymentMethodItem);
            }

            var tenderDiscount = new Array;
            var tenderDiscountItem = new Object;

            for (var i = 0; i < args.discountMethod.$value.length; i++) {
                var disseq = i + 1;
                //sql += "insert into tenderdiscount (discountType,discountValue,discountCode,staffId,refNo,discountDate,void,seq,discountAmount) values (@discountType" + i.toString() + ",@discountValue" + i.toString() + ",@discountCode" + i.toString() + ",@staffId,@refNo + @sseq,getDate(),0," + payseq + ",@discountAmount" + i.toString() + ");";
                sql += "insert into tenderdiscount (discountType,discountValue,discountCode,staffId,refNo,discountDate,void,seq,discountAmount) values (@discountType" + i.toString() + ",@discountValue" + i.toString() + ",@discountCode" + i.toString() + ",@staffId,@refNo,getDate(),0," + disseq + ",@discountAmount" + i.toString() + ");";
                sqlagrs.push({ "c": "discountType" + i.toString(), "t": TYPES.VarChar, "v": args.discountMethod.$value[i].type })
                sqlagrs.push({ "c": "discountValue" + i.toString(), "t": TYPES.Decimal, "v": parseFloat(args.discountMethod.$value[i].price), "o": { "scale": 2 } })
                sqlagrs.push({ "c": "discountCode" + i.toString(), "t": TYPES.VarChar, "v": args.discountMethod.$value[i].coupon_alias })
                sqlagrs.push({ "c": "discountAmount" + i.toString(), "t": TYPES.Decimal, "v": parseFloat(args.discountMethod.$value[i].amount), "o": { "scale": 2 } })
                //totalPrice += parseFloat(args.discountMethod.$value[i].price)
                tenderDiscountItem = new Object;
                tenderDiscountItem.discountType = args.discountMethod.$value[i].type;
                tenderDiscountItem.discountValue = parseFloat(args.discountMethod.$value[i].price);
                tenderDiscountItem.discountCode = args.discountMethod.$value[i].coupon_alias;
                tenderDiscountItem.discountDate = paymentDate;
                tenderDiscountItem.void = 0;
                tenderDiscountItem.seq = disseq;
                tenderDiscountItem.discountAmount = parseFloat(args.discountMethod.$value[i].amount);
                tenderDiscount.push(tenderDiscountItem);
            }

            sqlagrs.push({ "c": "price", "t": TYPES.Decimal, "v": totalPrice, "o": { "scale": 2 } })
            sqlagrs.push({ "c": "payAmount", "t": TYPES.Decimal, "v": totalPayment, "o": { "scale": 2 } })
            //            sql += "insert into transactionD (tableCode, tableSubCode, itemId, qty, price, discount, voidIndex, voidRemark, time, staffId, type, seq, round, refNo, customName, approveStaffId,serviceCharge ) select tableCode, tableSubCode, itemId, qty, price, discount, voidIndex, voidRemark, time, staffId, type, seq, round, @refNo + @sseq as refNo, customName, approveStaffId,serviceCharge from tableOrderD where tableCode = @tableCode and tableSubCode = @tableSubCode; \
            //update tableOrder set status = 4 where  tableCode = @tableCode and tableSubCode = @tableSubCode;\
            //delete tableOrder where tableCode = @tableCode and not exists (select * from tableorder where tableCode = @tableCode and (status != 4 or status is null)); \
            //delete tableOrderD where tableCode = @tableCode and not exists (select * from tableorder where tableCode = @tableCode and (status != 4 or status is null)); \
            //delete kds where tableCode = @tableCode and not exists (select * from tableorder where tableCode = @tableCode and (status != 4 or status is null)); \
            //delete tablediscount where tableCode = @tableCode and not exists (select * from tableorder where tableCode = @tableCode and (status != 4 or status is null));\
            //        update diningTable set status = 0 where name = @tableCode and not exists (select * from tableorder where tableCode = @tableCode and (status != 4 or status is null));\
            //select @refNo + @sseq as refNo \
            //commit tran end try begin catch rollback tran; THROW; end catch "
            sql += "insert into transactionD (tableCode, tableSubCode, itemId, qty, price, discount, voidIndex, voidRemark, time, staffId, type, seq, round, refNo, customName, approveStaffId,serviceCharge ) select tableCode, tableSubCode, itemId, qty, price, discount, voidIndex, voidRemark, time, staffId, type, seq, round, @refNo as refNo, customName, approveStaffId,serviceCharge from tableOrderD where tableCode = @tableCode and tableSubCode = @tableSubCode; \
update tableOrder set status = 4 where  tableCode = @tableCode and tableSubCode = @tableSubCode;\
delete tableOrder where tableCode = @tableCode and not exists (select * from tableorder where tableCode = @tableCode and (status != 4 or status is null)); \
delete tableOrderD where tableCode = @tableCode and not exists (select * from tableorder where tableCode = @tableCode and (status != 4 or status is null)); \
delete kds where tableCode = @tableCode and not exists (select * from tableorder where tableCode = @tableCode and (status != 4 or status is null)); \
delete tablediscount where tableCode = @tableCode and not exists (select * from tableorder where tableCode = @tableCode and (status != 4 or status is null));\
        update diningTable set status = 0 where name = @tableCode and not exists (select * from tableorder where tableCode = @tableCode and (status != 4 or status is null));\
select @refNo as refNo \
commit tran end try begin catch rollback tran; THROW; end catch "
            var query = [
                {
                    "sql": "select * from tableOrderD where tableCode = @tableCode and tableSubCode = @tableSubCode;", "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": tableCode }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": tableSubCode }]
                },
                { "sql": "select * from tableOrder where tableCode = @tableCode and tableSubCode = @tableSubCode;", "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": tableCode }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": tableSubCode }] }
                ,
                { "sql": sql, "args": sqlagrs }];

            var AzureObject = new Object;
            AzureObject.paymentArgs = new Object;
            AzureObject.paymentArgs = args;
            AzureObject.TransactionD = new Array;
            AzureObject.TableOrder = new Array;
            AzureObject.PayAmount = totalPayment;
            AzureObject.Price = totalPrice;
            AzureObject.ServiceCharge = serviceCharge;
            AzureObject.Remainings = remainings;
            AzureObject.PaymentMethod = new Array;
            AzureObject.PaymentMethod = PaymentMethod;
            AzureObject.tenderDiscount = new Array;
            AzureObject.tenderDiscount = tenderDiscount;

            //select * from transactionD where refNo = @refNo + cast(@seq as varchar(20))\



            this.DM.executeStatement(query, (err, result) => {
                var refNo = "";
                var octopusBalance = _o.octopusBalance;
                if (err == null) {

                    AzureObject.TransactionD = result[0];
                    AzureObject.TableOrder = result[1];
                    //console.log(err)
                    refNo = result[2][0].refNo;
                    AzureObject.refNo = result[2][0].refNo;
                    AzureSocket.emit('AddBill', JSON.stringify(AzureObject));

                    this.PrintPaymentBill({ 'refNo': refNo, openCashDrawer: true, printer: args.printer.$value, till: args.till.$value }, () => {

                    })
                    this.LoadTableSchema(null, (err, result) => {
                        io.sockets.emit('refreshTable', JSON.stringify(result));
                    })
                    //this.SetTableStatus({ "tableCode": tableCode, "status": 0 })
                }
                if (refNo != "") {
                    var temp = tableCode;
                    if (tableSubCode != "") {
                        temp += "_" + tableSubCode;
                    }

                    if (lockTableList["L" + temp] != undefined) {
                        clearTimeout(lockTableList["L" + temp].timeout);
                    }
                    delete lockTableList["L" + temp];


                    //'publish', { topic: "wo/gm/all", msg: "{\"action\":\"unlinkTable\",\"tableno\":\"" + $('#tno').val() + "\"}" 
                    mqttClient.publish("wo/gm/table/" + temp + "/", JSON.stringify({ "action": "unlinkTable", "tableno": temp }), { qos: 2 });
                    // mqttClient.publish("wo/gm/table/" + temp + "/", JSON.stringify({ "action": "reloadTable", "tableno": "888", "pax": "5" }), { qos: 2 });
                    mqttClient.publish("wo/kds/all/", JSON.stringify({ "action": "reload" }), { qos: 2 });
                    //mqttClient.publish("wo/gm/tabletest/test" + temp + "/", JSON.stringify({ "action": "stressLoadOrder" }), { qos: 2 });
                    //mqttClient.publish("wo/gm/all", JSON.stringify({ "action": "stressLoadOrder" }), { qos: 2 });
                }

                //callback({ "result": refNo == "" ? "fail" : "OK", "refNo": refNo, "tableCode": tableCode, "tableSubCode": tableSubCode });
                cb({ "result": refNo == "" ? "fail" : "OK", "refNo": refNo, "tableCode": tableCode, "tableSubCode": tableSubCode, "octopusBalance": octopusBalance });
            }, 'series');
        });
        async.waterfall(executeArr, function (result) {
            callback(result);
        });
    }

    this.updateDiningTableStauts = (tableCode, callback) => {



        var query = [{
            "sql": "DECLARE @subTableNum  INT\
        DECLARE @status2 INT\
        DECLARE @status3 INT\
        DECLARE @status4 INT\
        SELECT @subTableNum  = (select count(*) from tableOrder where tableCode = @tableCode ),\
        @status2 = (select count(*) from tableOrder where tableCode = @tableCode  and [status] = '2'),\
        @status3 = (select count(*) from tableOrder where tableCode = @tableCode  and [status] = '3'),\
        @status4 = (select count(*) from tableOrder where tableCode = @tableCode  and [status] = '4')\
        SELECT @subTableNum as 'tableNum', @status2 as 'status2', @status3 as 'status3', @status4 as 'status4' ", "args": [
        { "c": "tableCode", "t": TYPES.VarChar, "v": tableCode }]
        }];

        this.DM.executeStatement(query, (err, result) => {
            console.log('diningTableStauts', result);
            var updateQuery = [];
            var status = 0;

            if (result[0][0].tableNum > 0) {
                if (result[0][0].status2 != 0) status = 2;
                else if (result[0][0].status3 != 0 && (result[0][0].status2 == 0)) status = 3;
                else if (result[0][0].status4 != 0 && (result[0][0].status2 == 0 && result[0][0].status3 == 0)) {
                    status = 0;
                    updateQuery.push({
                        "sql": "delete from tableOrder where tableCode = @tableCode; delete from tableOrderD where tableCode = @tableCode; delete from tableDiscount where tableCode = @tableCode; delete from kdstrigger where tableCode = @tableCode;", "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": tableCode }]
                    }) //
                }
            } else {
                status = 0;
            }

            updateQuery.push({
                "sql": "update diningTable set status = " + status + " where name = @tableCode;", "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": tableCode }]
            });

            //if (result[0][0].status2 == 0) {
            //    updateQuery.push({
            //        "sql": "update diningTable set status = 0 where name = @tableCode; delete from tableOrder where tableCode = @tableCode; delete from tableOrderD where tableCode = @tableCode; delete from tableDiscount where tableCode = @tableCode", "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": tableCode }]
            //    }) //

            //} else if (result[0][0].status2 != 0) {
            //    updateQuery.push({
            //        "sql": "update diningTable set status = 2 where name = @tableCode; ", "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": tableCode }]
            //    });
            //}

            console.log('updateQuery', JSON.stringify(updateQuery));

            this.DM.executeStatement(updateQuery, (updateErr, updateResult) => {
                if (updateErr == null) {
                    this.LoadTableSchema(null, (err, result) => {
                        io.sockets.emit('refreshTable', JSON.stringify(result));
                    })
                    callback("OK");

                }
            });
        })
    };

    this.ChangeTable = (args, callback) => {
        var dTableObj = getTableCode(args.destinationTableNo);
        var sTableObj = getTableCode(args.sourceTableNo);
        console.log('dTableObj', JSON.stringify(dTableObj));
        console.log('sTableObj', JSON.stringify(sTableObj));

        if (dTableObj.tableSubCode != "") {

            var dTableTempl = Object.keys(lockTableList).filter(function (key) {
                return lockTableList[key].order == dTableObj.tableCode + "_" + dTableObj.tableSubCode;
            });
            if (dTableTempl.length != 0) {
                callback({ result: "Locked", tableOrderCode: dTableObj.tableCode + "_" + dTableObj.tableSubCode })
                return;
            }
        } else {
            if (lockTableList["L" + dTableObj.tableCode] != undefined) {
                callback({ result: "Locked", tableOrderCode: dTableObj.tableCode })
                return;
            }
        }

        if (sTableObj.tableSubCode != "") {
            var dTableTempl = Object.keys(lockTableList).filter(function (key) {
                return lockTableList[key].order == sTableObj.tableCode + "_" + sTableObj.tableSubCode;
            });
            if (dTableTempl.length != 0) {
                callback({ result: "Locked", tableOrderCode: sTableObj.tableCode + "_" + sTableObj.tableSubCode })
                return;
            }
        } else {
            if (lockTableList["L" + sTableObj.tableCode] != undefined) {
                callback({ result: "Locked", tableOrderCode: sTableObj.tableCode })
                return;
            }
        }

        var query = [{ "sql": "select max(round) as round, max(seq) as seq, max(pax) as pax,max(memberId) as memberId from tableorderd d right outer join tableOrder o on o.tableCode = d.tableCode and o.tableSubCode = d.tableSubCode where o.tableCode = @tableCode and o.tableSubCode = @tableSubCode;", "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": dTableObj.tableCode }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": dTableObj.tableSubCode }] },
        { "sql": "select * from tableorderd where tableCode = @tableCode and tableSubCode = @tableSubCode", "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": dTableObj.tableCode }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": dTableObj.tableSubCode }] },
        { "sql": "select * from tableorderd where tableCode = @tableCode and tableSubCode = @tableSubCode;", "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": sTableObj.tableCode }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": sTableObj.tableSubCode }] },
        { "sql": "select count(*) as numOfTable from tableorder where tableCode = @tableCode and tableSubCode = @tableSubCode", "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": dTableObj.tableCode }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": dTableObj.tableSubCode }] }
        ];

        console.log(JSON.stringify(query[0]));

        this.DM.executeStatement(query, (err, tableOrderD) => {
            //console.log(tableOrderD);
            var currentSeq = 0;
            var currentRound = 0;
            var query = [];
            var _o = {};
            var totalPrice = 0;
            var totalPriceForServiceCharge = 0;
            var serviceCharge = 0;
            var lastIsServiceCharge = true;
            var lastItemQty = 0;
            //var dTableIsExist = 0;



            if (tableOrderD[0][0].seq != null) {
                currentSeq = tableOrderD[0][0].seq;
                //console.log(tableOrderD[0][0].seq)
            }
            if (tableOrderD[0][0].round != null) {
                currentRound = tableOrderD[0][0].round;
                //console.log(tableOrderD[0][0].round);
            }


            currentRound++;
            sortDbItem(tableOrderD[1]);




            if (tableOrderD[1].length != 0) {
                for (var i = 0; i < tableOrderD[1].length; i++) {
                    var orderedItem = tableOrderD[1][i];
                    if (orderedItem.type == "I") {
                        lastIsServiceCharge = orderedItem.serviceCharge;
                        lastItemQty = orderedItem.qty;
                        totalPrice += lastItemQty * orderedItem.price;
                        if (lastIsServiceCharge) {
                            totalPriceForServiceCharge += lastItemQty * orderedItem.price;
                        }
                    } else {
                        totalPrice += lastItemQty * orderedItem.price * orderedItem.qty;
                        if (lastIsServiceCharge) {
                            totalPriceForServiceCharge += lastItemQty * orderedItem.qty * orderedItem.price;
                        }
                    }
                }
            }

            //console.log('tableOrderD', tableOrderD[3]);
            var deleteExistTableQuery = [];
            if (tableOrderD[3][0].numOfTable == 1 && currentSeq == 0 && currentRound == 1) {
                console.log('have deleteExistTableQuery');
                deleteExistTableQuery.push({ "sql": "delete  from tableorder where tableCode = @tableCode and tableSubCode = @tableSubCode; delete from tableorderD where tableCode = @tableCode and tableSubCode = @tableSubCode; delete from tableDiscount where tableCode = @tableCode and tableSubCode = @tableSubCode; delete from kds where tableCode = @tableCode and tableSubCode = @tableSubCode;", "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": dTableObj.tableCode }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": dTableObj.tableSubCode }] })

                console.log(JSON.stringify(deleteExistTableQuery));
            }


            this.DM.executeStatement(deleteExistTableQuery, (err, resultOfDelete) => {

                if (currentSeq == 0 && currentRound == 1) {
                    //console.log('currentSeq == 0 && currentRound == 1');
                    var changeTableArray = [];

                    changeTableArray.push({ "sql": "update diningTable set status = 0 where name = @tableCode", "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": sTableObj.tableCode }] });
                    changeTableArray.push({ "sql": "update diningTable set status = 2 where name = @tableCode", "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": dTableObj.tableCode }] });
                    changeTableArray.push({ "sql": "update tableorder set tableCode = @destinationTableNo, tableSubCode = @destinationTableSubCode where tableCode = @sourceTableNo and tableSubCode = @sourceTableSubCode", "args": [{ "c": "destinationTableNo", "t": TYPES.VarChar, "v": dTableObj.tableCode }, { "c": "sourceTableNo", "t": TYPES.VarChar, "v": sTableObj.tableCode }, { "c": "destinationTableSubCode", "t": TYPES.VarChar, "v": dTableObj.tableSubCode }, { "c": "sourceTableSubCode", "t": TYPES.VarChar, "v": sTableObj.tableSubCode }] });
                    changeTableArray.push({ "sql": "update tableorderd set tableCode = @destinationTableNo, tableSubCode = @destinationTableSubCode  where tableCode = @sourceTableNo and tableSubCode = @sourceTableSubCode", "args": [{ "c": "destinationTableNo", "t": TYPES.VarChar, "v": dTableObj.tableCode }, { "c": "sourceTableNo", "t": TYPES.VarChar, "v": sTableObj.tableCode }, { "c": "destinationTableSubCode", "t": TYPES.VarChar, "v": dTableObj.tableSubCode }, { "c": "sourceTableSubCode", "t": TYPES.VarChar, "v": sTableObj.tableSubCode }] });
                    changeTableArray.push({ "sql": "update kds set tableCode = @destinationTableNo, tableSubCode = @destinationTableSubCode  where tableCode = @sourceTableNo and tableSubCode = @sourceTableSubCode", "args": [{ "c": "destinationTableNo", "t": TYPES.VarChar, "v": dTableObj.tableCode }, { "c": "sourceTableNo", "t": TYPES.VarChar, "v": sTableObj.tableCode }, { "c": "destinationTableSubCode", "t": TYPES.VarChar, "v": dTableObj.tableSubCode }, { "c": "sourceTableSubCode", "t": TYPES.VarChar, "v": sTableObj.tableSubCode }] });
                    changeTableArray.push({ "sql": "update tableDiscount set tableCode = @destinationTableNo, tableSubCode = @destinationTableSubCode where tableCode = @sourceTableNo  and tableSubCode = @sourceTableSubCode", "args": [{ "c": "destinationTableNo", "t": TYPES.VarChar, "v": dTableObj.tableCode }, { "c": "sourceTableNo", "t": TYPES.VarChar, "v": sTableObj.tableCode }, { "c": "destinationTableSubCode", "t": TYPES.VarChar, "v": dTableObj.tableSubCode }, { "c": "sourceTableSubCode", "t": TYPES.VarChar, "v": sTableObj.tableSubCode }] });

                    changeTableArray.push({ "sql": "select * from tableorderd where tableCode = @tableCode and tableSubCode = @tableSubCode", "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": dTableObj.tableCode }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": dTableObj.tableSubCode }] });

                    //changeTableArray.push({ "sql": "update xf_salesorder set xf_orderid = :1 || substr(xf_orderid,:2),xf_content = :3 || substr(xf_content,:4), xf_tableno = :5 where xf_tableno = :6", arg: [args.destinationTableNo, args.sourceTableNo.length + 1, args.destinationTableNo, args.sourceTableNo.length + 1, args.destinationTableNo, args.sourceTableNo] });
                    this.DM.executeStatement(changeTableArray, (err, result) => {

                        this.updateDiningTableStauts(sTableObj.tableCode, function () {

                        });
                        ////console.log(err);
                        var stotalPrice = 0;
                        var stotalPriceForServiceCharge = 0;
                        var sserviceCharge = 0;
                        var sitemIsServiceCharge = true;
                        var sitemQty = 0;

                        sortDbItem(result[result.length - 1]);

                        if (result[result.length - 1].length != 0) {
                            var kdsList = [];
                            for (var i = 0; i < result[result.length - 1].length; i++) {
                                var orderedItem = result[result.length - 1][i];
                                if (orderedItem.kds != undefined) {
                                    kdsList.push(orderedItem.kds);
                                }

                                if (orderedItem.type == "I") {
                                    sitemIsServiceCharge = orderedItem.serviceCharge;
                                    sitemQty = orderedItem.qty;
                                    stotalPrice += sitemQty * orderedItem.price;
                                    if (sitemIsServiceCharge) {
                                        stotalPriceForServiceCharge += sitemQty * orderedItem.price;
                                    }
                                } else {
                                    stotalPrice += (sitemQty * orderedItem.qty) * orderedItem.price;
                                    if (sitemIsServiceCharge) {
                                        stotalPriceForServiceCharge += (sitemQty * orderedItem.qty) * orderedItem.price;
                                    }
                                }
                            }
                            kdsList.filter(onlyUnique);
                            console.log(JSON.stringify(kdsList));
                            for (var j = 0; j < kdsList.length; j++) {
                                this.showKds(kdsList[j]);
                            }
                        }




                        var sserviceCharge = (stotalPriceForServiceCharge / 10).toFixed(1);
                        query.push({
                            "sql": "update tableorder set price = @price,serviceCharge = @serviceCharge where tableCode = @tableCode and tableSubCode = @tableSubCode",
                            "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": sTableObj.tableCode }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": sTableObj.tableSubCode }, { "c": "staffId", "t": TYPES.VarChar, "v": args.staffCode },
                            { "c": "price", "t": TYPES.Decimal, "v": parseFloat(stotalPrice), "o": { "scale": 2 } },
                            { "c": "serviceCharge", "t": TYPES.Decimal, "v": parseFloat(sserviceCharge), "o": { "scale": 2 } }]
                        });



                        this.DM.executeStatement(query, (err, result) => {
                            //console.log('update transfer item order');
                            //console.log(err);
                            //RAS.LoadTableSchema(null, (err, result) => {
                            //    io.sockets.emit('refreshTable', JSON.stringify(result));
                            //})
                        });
                    }, 'series')
                }
                else {

                    console.log('currentSeq != 0 && currentRound != 1')

                    //query = [{ "sql": "update tableOrderd set seq = seq + " + currentSeq + ", voidIndex = case when voidIndex is null then null else voidIndex + " + currentSeq + " end, tableCode =  @dTableCode, tableSubCode = @dTableSubCode where  tableCode = @tableCode and tableSubCode = @tableSubCode; update diningTable set status = 0 where name = @tableCode ", "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": sTableObj.tableCode }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": sTableObj.tableSubCode }, { "c": "dTableCode", "t": TYPES.VarChar, "v": dTableObj.tableCode }, { "c": "dTableSubCode", "t": TYPES.VarChar, "v": dTableObj.tableSubCode }, { "c": "seq", "t": TYPES.Int, "v": currentSeq }] }];

                    //query = [{ "sql": "select * from tableorderd where tableCode = @tableCode and tableSubCode = @tableSubCode;", "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": dTableObj.tableCode }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": dTableObj.tableSubCode }] }, { "sql": "select * from tableorderd where tableCode = @tableCode and tableSubCode = @tableSubCode;", "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": sTableObj.tableCode }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": sTableObj.tableSubCode }] }];
                    //this.DM.executeStatement(query, (err, tableOrderD) => {
                    sortDbItem(tableOrderD[2]);
                    lastIsServiceCharge = true;
                    lastItemQty = 0;
                    if (tableOrderD[2].length != 0) {
                        for (var i = 0; i < tableOrderD[2].length; i++) {
                            var orderedItem = tableOrderD[2][i];
                            if (orderedItem.type == "I") {
                                lastIsServiceCharge = orderedItem.serviceCharge;
                                lastItemQty = orderedItem.qty;
                                totalPrice += lastItemQty * orderedItem.price;
                                if (lastIsServiceCharge) {
                                    totalPriceForServiceCharge += lastItemQty * orderedItem.price;
                                }
                            } else {
                                totalPrice += lastItemQty * orderedItem.price * orderedItem.qty;
                                if (lastIsServiceCharge) {
                                    totalPriceForServiceCharge += lastItemQty * orderedItem.qty * orderedItem.price;
                                }
                            }
                        }
                    }

                    var serviceCharge = (totalPriceForServiceCharge / 10).toFixed(1);

                    query = [{ "sql": "update tableOrderd set seq = seq + " + currentSeq + ", voidIndex = case when voidIndex = -1 then -1 else voidIndex + " + currentSeq + " end, tableCode =  @dTableCode, tableSubCode = @dTableSubCode where  tableCode = @tableCode and tableSubCode = @tableSubCode; update diningTable set status = 0 where name = @tableCode; update tableorder set price = @price, serviceCharge = @serviceCharge where tableCode = @dtableCode and tableSubCode = @dtableSubCode; delete tableorder where tableCode =  @tableCode and tableSubCode = @tableSubCode; update kds set seq = seq + " + currentSeq + ", voidIndex = case when voidIndex = -1 then -1 else voidIndex + " + currentSeq + " end, tableCode =  @dTableCode, tableSubCode = @dTableSubCode where  tableCode = @tableCode and tableSubCode = @tableSubCode", "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": sTableObj.tableCode }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": sTableObj.tableSubCode }, { "c": "dTableCode", "t": TYPES.VarChar, "v": dTableObj.tableCode }, { "c": "dTableSubCode", "t": TYPES.VarChar, "v": dTableObj.tableSubCode }, { "c": "seq", "t": TYPES.Int, "v": currentSeq }, { "c": "price", "t": TYPES.Decimal, "v": totalPrice, "o": { "scale": 2 } }, { "c": "serviceCharge", "t": TYPES.Decimal, "v": serviceCharge, "o": { "scale": 2 } }] }];
                    query.push({ "sql": "update tableDiscount set tableCode = @destinationTableNo, tableSubCode = @destinationTableSubCode where tableCode = @sourceTableNo  and tableSubCode = @sourceTableSubCode", "args": [{ "c": "destinationTableNo", "t": TYPES.VarChar, "v": dTableObj.tableCode }, { "c": "sourceTableNo", "t": TYPES.VarChar, "v": sTableObj.tableCode }, { "c": "destinationTableSubCode", "t": TYPES.VarChar, "v": dTableObj.tableSubCode }, { "c": "sourceTableSubCode", "t": TYPES.VarChar, "v": sTableObj.tableSubCode }] });
                    query.push({ "sql": "select * from tableorderd where tableCode = @tableCode and tableSubCode = @tableSubCode", "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": dTableObj.tableCode }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": dTableObj.tableSubCode }] });


                    this.DM.executeStatement(query, (err, result) => {

                        this.updateDiningTableStauts(sTableObj.tableCode, function () {

                        });
                        //console.log(result);
                        var stotalPrice = 0;
                        var stotalPriceForServiceCharge = 0;
                        var sserviceCharge = 0;
                        var sitemIsServiceCharge = true;
                        var sitemQty = 0;

                        sortDbItem(result[result.length - 1]);



                        var kdsList = [];

                        if (result[result.length - 1].length != 0) {
                            for (var i = 0; i < result[result.length - 1].length; i++) {
                                var orderedItem = result[result.length - 1][i];
                                if (orderedItem.kds != undefined) {
                                    kdsList.push(orderedItem.kds);
                                }
                                if (orderedItem.type == "I") {
                                    sitemIsServiceCharge = orderedItem.serviceCharge;
                                    sitemQty = orderedItem.qty;
                                    stotalPrice += sitemQty * orderedItem.price;
                                    if (sitemIsServiceCharge) {
                                        stotalPriceForServiceCharge += sitemQty * orderedItem.price;
                                    }
                                } else {
                                    stotalPrice += (sitemQty * orderedItem.qty) * orderedItem.price;
                                    if (sitemIsServiceCharge) {
                                        stotalPriceForServiceCharge += (sitemQty * orderedItem.qty) * orderedItem.price;
                                    }
                                }
                            }
                            kdsList.filter(onlyUnique);
                            console.log(JSON.stringify(kdsList));
                            for (var j = 0; j < kdsList.length; j++) {
                                this.showKds(kdsList[j]);
                            }
                        }
                        var sserviceCharge = (stotalPriceForServiceCharge / 10).toFixed(1);
                        query.push({
                            "sql": "update tableorder set price = @price,serviceCharge = @serviceCharge where tableCode = @tableCode and tableSubCode = @tableSubCode",
                            "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": sTableObj.tableCode }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": sTableObj.tableSubCode }, { "c": "staffId", "t": TYPES.VarChar, "v": args.staffCode },
                            { "c": "price", "t": TYPES.Decimal, "v": parseFloat(stotalPrice), "o": { "scale": 2 } },
                            { "c": "serviceCharge", "t": TYPES.Decimal, "v": parseFloat(sserviceCharge), "o": { "scale": 2 } }]
                        });


                        this.DM.executeStatement(query, (err, result) => {
                            //console.log('update transfer item order');
                            //console.log(err);
                            //RAS.LoadTableSchema(null, (err, result) => {
                            //    io.sockets.emit('refreshTable', JSON.stringify(result));
                            //})


                        });
                    });


                    //})

                }
                //var _o = { action: "changeTable", time: moment().format("HH:mm:ss"), table: args.sourceTableNo };
                //mqttClient.publish("wo/gm/table/" + args.sourceTableNo + "/", JSON.stringify(_o), { qos: 2 });
                mqttClient.publish("wo/gm/table/" + args.sourceTableNo + "/", JSON.stringify({ "action": "unlinkTable", "tableno": args.sourceTableNo }), { qos: 2 });

            })





        }, 'parallel');
    }

    this.PrintBill = (args, callback) => {
        console.log(6387, args);
        args.reprint = true;
        this.PrintPaymentBill(args, () => {
            callback({ 'result': 'OK' });
        })
    }

    this.LoadStaff = (args, callback) => {
        //console.log(args)
        //console.log(callback)
        this.DM.executeStatement({ "sql": "select * FROM staff s inner join staffgroup sg on s.staffgroupcode = sg.staffgroupcode", "args": [] }, function (err, result) {
            callback(result);
        });
    }

    this.LoadBillByTakeAway = (args, callback) => {


        var sql = "declare @refNo varchar(500); select  top 1 @refNo = refNo from [transaction] as t ";
        var replacement = "";
        if (args.searchMode == 'takeAway') {
            replacement = " t.refNo like '%-TO-%' ";
        } else if (args.searchMode == 'fastFood') {
            replacement = " (t.refNo like '%-FD-%' or t.refNo like '%-FT-%') " + (args.till ? (" and t.till='" + args.till + "'") : "");
        }
        else if (args.searchMode == 'deliveryOrder') {
            replacement = " (t.refNo like '%-D-%') " + (args.till ? (" and t.till='" + args.till + "'") : "");
        }


        if (args) {
            //

            if (args.searchNo) {
                pad(args.searchNo, 6)
                sql += " where refNo like  '%-" + pad(args.searchNo, 6) + "' and ##conditionWhere## ";
            } else

                if (args.refNo) {
                    var direction = " > ", orderby = " asc ";
                    if (args.direction == 'prev') {
                        direction = " < ";
                        orderby = " desc "
                    }

                    if (args.direction == "equal") { direction = " = "; }
                    sql += " where refNo " + direction + " '" + args.refNo + "' and  ##conditionWhere##  order by refNo " + orderby;
                } else {
                    sql += " where ##conditionWhere##  order by refNo desc ";
                }
        }

        else {


            sql += " where ##conditionWhere##  order by refNo desc ";
        }



        sql += "  select *, convert(varchar, transTime, 120) as transFormatTime from [transaction] t inner join [transactionD] td on t.refNo = td.refNo where t.refNo = @refNo and ##conditionWhere##  ; select * from tenderDiscount where refNo = @refNo and void = 0; select * from tenderPayment where refNo = @refNo and void = 0 ";


        sql = sql.replace(new RegExp('##conditionWhere##', 'g'), replacement);
        var query = { "sql": sql, "args": [] };
        //
        this.DM.executeStatement(query, (err, result) => {
            ////console.log(result);
            var orders = [];
            var order = { 'version': '1.0', 'header': { 'orderId': '', 'tableNum': '', 'peopleNum': 0, 'member': '', 'memberName': '', 'memberMsg': '', 'refNo': '', 'transTime': '' }, 'item': [], 'itemCount': 0, 'payment': [], 'discount': [], 'paymentLater': '' };
            //var tableOrder = result[0];


            //result = result.filter(function (item) {
            //    return item.refNo.indexOf('-TO-') != -1 && item.void == "0";
            //})


            if (result.length != 0) {
                var tableOrder = result[0];
                order.header.tableNum = tableOrder.tableCode + tableOrder.tableSubCode;
                order.header.peopleNum = tableOrder.pax;
                order.header.refNo = tableOrder.refNo;
                order.header.transTime = tableOrder.transFormatTime;
                order.header.orderId = "_";
                order.header.void = tableOrder.void;
                order.paymentLater = tableOrder.paymentLater;
            }

            var contentObj = [];
            sortDbItem(result);
            for (var j = 0; j < result.length ; j++) {
                var currentItem = result[j];
                if (currentItem.itemId != undefined) {
                    var items = getItemByItem(currentItem);
                    var name1, name2, name3;
                    name1 = name2 = name3 = "";

                    if (items.length > 0) {
                        var name1 = items[0].name1;
                        var name2 = items[0].name2;
                        var name3 = items[0].name3;
                        if (items[0].name != undefined) {
                            if (items[0].name[config.lang1] != undefined)
                                name1 = items[0].name[config.lang1];
                            if (items[0].name[config.lang2] != undefined)
                                name2 = items[0].name[config.lang2];
                            if (items[0].name[config.lang3] != undefined)
                                name3 = items[0].name[config.lang3];
                        }
                    }
                    if (currentItem.customName != "") {
                        name1 = name2 = name3 = currentItem.customName;
                    }

                    var t = moment(currentItem.time).format("HH:mm:ss");
                    contentObj.push({ code: currentItem.itemId, type: currentItem.type, desc1: name1, desc2: name2, desc3: name3, qty: currentItem.qty, unitPrice: currentItem.price, time: t, voidIndex: currentItem.voidIndex, voidRemark: currentItem.voidRemark, staff: currentItem.staffId, index: currentItem.seq, serviceCharge: currentItem.serviceCharge });
                } else if (currentItem.discountType != undefined) {
                    order.discount.push({ "discountCode": currentItem.discountCode, "discountType": currentItem.discountType, "discountValue": currentItem.discountValue, "discountAmount": currentItem.discountAmount });
                } else if (currentItem.paymentType != undefined) {
                    var priceValue = currentItem.priceValue;
                    if (!priceValue) priceValue = currentItem.paymentValue;
                    order.payment.push({ "paymentType": currentItem.paymentType, "paymentValue": currentItem.paymentValue, "priceValue": priceValue });
                }
            }
            if (result.length != 0) {
                order.item = contentObj;
                order.itemCount = contentObj.length;
                orders.push(order);
            }
            var callbackObj = { result: "OK", timestamp: new Date().getTime(), taskID: 1, note: "", order: orders, suspendItems: [] }
            callback(callbackObj);
        })
    }

    this.LoadBillByDelivery = (args, callback) => {
        var sql = "declare @refNo varchar(500); select  top 1 @refNo = refNo from [transaction] t ";
        if (args) {

            if (args.searchNo) {
                pad(args.searchNo, 6)
                sql += " where refNo like  '%-" + pad(args.searchNo, 6) + "' and refNo like '%-D-%' and void = 0 ";
            } else

                if (args.refNo) {
                    var direction = " > ", orderby = " asc ";
                    if (args.direction == 'prev') {
                        direction = " < ";
                        orderby = " desc "
                    }

                    if (args.direction == "equal") { direction = " = "; }
                    sql += " where refNo " + direction + " '" + args.refNo + "' and void = 0 and refNo like '%-D-%' order by refNo " + orderby;
                }
        }

        else {


            sql += " where void = 0 and refNo like '%-D-%'  order by refNo desc ";
        }



        sql += "  select *, convert(varchar, transTime, 120) as transFormatTime from [transaction] t inner join [transactionD] td on t.refNo = td.refNo where t.refNo = @refNo and t.refNo like '%-D-%' and void = 0 ; select * from tenderDiscount where refNo = @refNo and void = 0; select * from tenderPayment where refNo = @refNo  and void = 0"



        var query = { "sql": sql, "args": [] };
        //
        this.DM.executeStatement(query, (err, result) => {
            ////console.log(result);
            var orders = [];
            var order = { 'version': '1.0', 'header': { 'orderId': '', 'tableNum': '', 'peopleNum': 0, 'member': '', 'memberName': '', 'memberMsg': '', 'refNo': '', 'transTime': '' }, 'item': [], 'itemCount': 0, 'payment': [], 'discount': [] };
            //var tableOrder = result[0];


            //result = result.filter(function (item) {
            //    return item.refNo.indexOf('-TO-') != -1 && item.void == "0";
            //})


            if (result.length != 0) {
                var tableOrder = result[0];
                order.header.tableNum = tableOrder.tableCode + tableOrder.tableSubCode;
                order.header.peopleNum = tableOrder.pax;
                order.header.refNo = tableOrder.refNo;
                order.header.transTime = tableOrder.transFormatTime;
                order.header.orderId = "_";
            }
            var contentObj = [];
            sortDbItem(result);
            for (var j = 0; j < result.length ; j++) {
                var currentItem = result[j];
                if (currentItem.itemId != undefined) {
                    var items = getItemByItem(currentItem);
                    var name1, name2, name3;
                    name1 = name2 = name3 = "";

                    if (items.length > 0) {
                        var name1 = items[0].name1;
                        var name2 = items[0].name2;
                        var name3 = items[0].name3;
                        if (items[0].name != undefined) {
                            if (items[0].name[config.lang1] != undefined)
                                name1 = items[0].name[config.lang1];
                            if (items[0].name[config.lang2] != undefined)
                                name2 = items[0].name[config.lang2];
                            if (items[0].name[config.lang3] != undefined)
                                name3 = items[0].name[config.lang3];
                        }
                    }
                    if (currentItem.customName != "") {
                        name1 = name2 = name3 = currentItem.customName;
                    }

                    var t = moment(currentItem.time).format("HH:mm:ss");
                    contentObj.push({ code: currentItem.itemId, type: currentItem.type, desc1: name1, desc2: name2, desc3: name3, qty: currentItem.qty, unitPrice: currentItem.price, time: t, voidIndex: currentItem.voidIndex, voidRemark: currentItem.voidRemark, staff: currentItem.staffId, index: currentItem.seq, serviceCharge: currentItem.serviceCharge });
                } else if (currentItem.discountType != undefined) {
                    order.discount.push({ "discountCode": currentItem.discountCode, "discountType": currentItem.discountType, "discountValue": currentItem.discountValue, "discountAmount": currentItem.discountAmount });
                } else if (currentItem.paymentType != undefined) {
                    order.payment.push({ "paymentType": currentItem.paymentType, "paymentValue": currentItem.paymentValue })
                }
            }
            if (result.length != 0) {
                order.item = contentObj;
                order.itemCount = contentObj.length;
                orders.push(order);
            }
            var callbackObj = { result: "OK", timestamp: new Date().getTime(), taskID: 1, note: "", order: orders, suspendItems: [] }
            callback(callbackObj);
        })
    }


    this.LoadBill = (args, callback) => {
        var sql = "declare @refNo varchar(500); select  top 1 @refNo = refNo from [transaction] as t ";

        var replacement = " t.refNo not like '%-TO-%' and t.refNo not like '%-D-%' and t.refNo not like '%-FD-%' and t.refNo not like '%-FT-%' ";


        if (args) {

            if (args.searchNo) {
                pad(args.searchNo, 6)
                sql += " where refNo like  '%-" + pad(args.searchNo, 6) + "' and ##conditionWhere## ";
            } else

                if (args.refNo) {
                    var direction = " > ", orderby = " asc ";
                    if (args.direction == 'prev') {
                        direction = " < ";
                        orderby = " desc "
                    }

                    if (args.direction == "equal") { direction = " = "; }
                    sql += " where refNo " + direction + " '" + args.refNo + "' and  ##conditionWhere##  order by refNo " + orderby;
                }
        }

        else {


            sql += " where ##conditionWhere## order by refNo desc ";
        }



        sql += "  select *, convert(varchar, transTime, 120) as transFormatTime from [transaction] t inner join [transactionD] td on t.refNo = td.refNo where t.refNo = @refNo and ##conditionWhere## ; select * from tenderDiscount where refNo = @refNo and void = 0 ; select * from tenderPayment where refNo = @refNo and void = 0"


        sql = sql.replace(new RegExp('##conditionWhere##', 'g'), replacement);
        var query = { "sql": sql, "args": [] };
        //
        console.log(sql);
        this.DM.executeStatement(query, (err, result) => {
            console.log(result);
            var orders = [];
            var order = { 'version': '1.0', 'header': { 'orderId': '', 'tableNum': '', 'peopleNum': 0, 'member': '', 'memberName': '', 'memberMsg': '', 'refNo': '', 'transTime': '' }, 'item': [], 'itemCount': 0, 'payment': [], 'discount': [] };
            //var tableOrder = result[0];

            if (result.length != 0) {
                var tableOrder = result[0];
                order.header.tableNum = tableOrder.tableCode + tableOrder.tableSubCode;
                order.header.peopleNum = tableOrder.pax;
                order.header.refNo = tableOrder.refNo;
                order.header.transTime = tableOrder.transFormatTime;
                order.header.orderId = "_";
                order.header.void = tableOrder.void;
            }

            var contentObj = [];
            sortDbItem(result);
            for (var j = 0; j < result.length ; j++) {
                var currentItem = result[j];
                if (currentItem.itemId != undefined) {
                    var items = getItemByItem(currentItem);
                    var name1, name2, name3;
                    name1 = name2 = name3 = "";

                    if (items.length > 0) {
                        var name1 = items[0].name1;
                        var name2 = items[0].name2;
                        var name3 = items[0].name3;
                        if (items[0].name != undefined) {
                            if (items[0].name[config.lang1] != undefined)
                                name1 = items[0].name[config.lang1];
                            if (items[0].name[config.lang2] != undefined)
                                name2 = items[0].name[config.lang2];
                            if (items[0].name[config.lang3] != undefined)
                                name3 = items[0].name[config.lang3];
                        }
                    }
                    if (currentItem.customName != "") {
                        name1 = name2 = name3 = currentItem.customName;
                    }

                    var t = moment(currentItem.time).format("HH:mm:ss");
                    contentObj.push({ code: currentItem.itemId, type: currentItem.type, desc1: name1, desc2: name2, desc3: name3, qty: currentItem.qty, unitPrice: currentItem.price, time: t, voidIndex: currentItem.voidIndex, voidRemark: currentItem.voidRemark, staff: currentItem.staffId, index: currentItem.seq, serviceCharge: currentItem.serviceCharge });
                } else if (currentItem.discountType != undefined) {
                    order.discount.push({ "discountCode": currentItem.discountCode, "discountType": currentItem.discountType, "discountValue": currentItem.discountValue, "discountAmount": currentItem.discountAmount });
                } else if (currentItem.paymentType != undefined) {
                    var priceValue = currentItem.priceValue;
                    if (!priceValue) priceValue = currentItem.paymentValue;
                    order.payment.push({ "paymentType": currentItem.paymentType, "paymentValue": currentItem.paymentValue, "priceValue": priceValue })
                }
            }
            if (result.length != 0) {
                order.item = contentObj;
                order.itemCount = contentObj.length;
                orders.push(order);
            }
            var callbackObj = { result: "OK", timestamp: new Date().getTime(), taskID: 1, note: "", order: orders, suspendItems: [] }
            callback(callbackObj);
        })
    }

    this.CheckDailyClearance = (args, callback) => {
        var query = { "sql": "select count(1) as c from [transaction] where transTime < cast(getdate() as date) ;", "args": [] };
        this.DM.executeStatement(query, (err, result) => {
            if (result[0].c != 0) {
                callback({ "result": "ERROR" });
            } else {
                callback({ "result": "OK" });
            }
        });
    }

    this.DailyClearance = (args, callback) => {
        var q = {
            "sql": "select refNo , payAmount - (price + serviceCharge - (case when discount is null then 0 else discount end) + remainings) as result from (SELECT refNo, payAmount, price, serviceCharge, remainings , ROUND((select SUM(discountValue) from tenderDiscount where refNo = t.refNo and void = 0),1) as discount FROM [transaction] as t where till = @till and void = 0) as temp order by refNo desc",
            "args": [
                { "c": "till", "t": TYPES.VarChar, "v": args.till }
            ]
        }
        this.DM.executeStatement(q, (e, r) => {

            var notCompleteBill = [];
            r.forEach(function (b) {
                if (b.result < 0) {
                    notCompleteBill.push(b);
                }
            })

            if (notCompleteBill.length != 0) {
                callback({ "result": "ERROR", "msg": "not_complete_payment", "notCompleteBill": notCompleteBill });
            }
            else {
                var query = { "sql": "select count(1) as c from tableorder; select count(1) as c from [transaction];", "args": [] };
                RAS.DM.executeStatement(query, (err, result) => {
                    //console.log(result)
                    if (result[0].c != 0) {
                        callback({ "result": "ERROR", "msg": "please_complete_orders" });
                    } else if (result[1].c == 0) {
                        callback({ "result": "ERROR", "msg": "no_order" });
                    } else {

                        RAS.PrepareDailyReport({ isPrint: true, printer: args.printer, till: args.till }, (obj) => {
                            var archiveDate = new Date();
                            var query = {
                                "sql": "select refNo from [transaction] " + (args.till ? (" where till = '" + args.till) + "'" : "") + "; begin tran begin try insert into [cashControlArchive] (cash, type, remark, datetime, staffId, archiveStaff, archiveDate) select cash, type, remark, datetime, staffId, @archiveStaff, @archiveDate from [cashControl];\
                            \
                        insert into [transactionArchive]  (tableCode, tableSubCode, pax, staffId, memberId, memberName, startTime, refNo, transTime, payAmount, price, serviceCharge, remainings, reprint, void, voidStaff, archiveStaff, archiveDate,orderid, till ) select tableCode, tableSubCode, pax, staffId, memberId, memberName, startTime, refNo, transTime, payAmount, price, serviceCharge, remainings, reprint, void, voidStaff, @archiveStaff, @archiveDate,orderid, till from [transaction] " + (args.till ? (" where till = '" + args.till) + "'" : "") + ";\
                        \
                    insert into transactionDArchive (tableCode, tableSubCode, itemId, qty, price, discount, voidIndex, voidRemark, time, staffId, type, seq, round, refNo, customName, approveStaffId ) select tableCode, tableSubCode, itemId, qty, price, discount, voidIndex, voidRemark, time, staffId, type, seq, round, refNo, customName,approveStaffId  from [transactionD] "  + (args.till ? (" where refNo in (select refNo from [transaction] where till = '" + args.till + "') ") : "") + "; \
                    insert into tenderpaymentArchive (paymentType,paymentValue,staffId,refNo,paymentDate,void,seq,voidDate,voidStaff,till, remark) select paymentType,paymentValue,staffId,refNo,paymentDate,void,seq,voidDate,voidStaff,till, remark from tenderpayment " + (args.till ? (" where refNo in (select refNo from [transaction] where till = '" + args.till + "') ") : "") + "; \
                    \
                    insert into tenderdiscountArchive (discountType,discountValue,discountCode,staffId,refNo,discountDate,void,voidStaff,voidDate,seq,discountAmount) select  discountType,discountValue,discountCode,staffId,refNo,discountDate,void,voidStaff,voidDate,seq,discountAmount from tenderDiscount "+ (args.till ? (" where refNo in (select refNo from [transaction] where till = '" + args.till + "') ") : "") + "; \
                insert into kdsordersArchive (orderNo, time, kdsStatus, calledTime, dismissTime, archiveDate, till) select orderNo, time, kdsStatus, calledTime, dismissTime, getDate(), " + (args.till ? ("'" + args.till + "'") : "''") + " from [kdsOrders] " + (args.till ? (" where orderNo in (select refNo from [transaction] where till = '" + args.till + "') ") : "") + ";\
                    delete from [tenderpayment] " + (args.till ? (" where refNo in (select refNo from [transaction] where till = '" + args.till + "') ") : "") + "; \
                delete from [tenderDiscount] " + (args.till ? (" where refNo in (select refNo from [transaction] where till = '" + args.till + "') ") : "") + "; \
                delete transactionD "+ (args.till ? (" where refNo in (select refNo from [transaction] where till = '" + args.till + "') ") : "") + "; \
                delete [transaction] " + (args.till ? (" where till = '" + args.till + "'") : "") + "; \
                delete cashControl; \
                DELETE [kdsOrders]\
                update [ecrTransaction] set Archive = 1 "+ (args.till ? (" where Till = '" + args.till + "'") : "") + "; \
                commit tran end try begin catch rollback tran; THROW; end catch "
                          , "args": [{ "c": "archiveStaff", "t": TYPES.VarChar, "v": args.usercode }, { "c": "archiveDate", "t": TYPES.DateTime, "v": archiveDate }]
                            };

                            RAS.DM.executeStatement(query, (err, result) => {
                                console.log(query);
                                var cloud = {};
                                cloud.refNo = [];
                                cloud.refNo = result;
                                cloud.staffCode = args.usercode;
                                cloud.archiveDate = archiveDate;


                                var AzureObject = new Object;
                                AzureObject.UserCode = args.usercode;
                                AzureSocket.emit('DailyClearance', JSON.stringify(AzureObject));

                                //if (config.tills && args.till) {
                                //    var octopus = extend2({}, config.tills[args.till].octopus);
                                //}
                                //if (!octopus) {
                                //    console.log("Octopus dayend octopus_api_setting_error");
                                //}
                                //else {
                                //    var qs = QueryString.stringify({
                                //        "sessionk": "techtranswebon",
                                //        "method": "dayend"
                                //    });
                                //    connToOctopus(qs, octopus.apiServer, function (data) {
                                //        console.log("Octopus dayend response", data);
                                //    });
                                //}
                                var params = {
                                    service: this.Octopus.service.Octopus_dayend,
                                    till: args.till
                                };
                                this.Octopus.connect(params, (data) => {
                                    console.log("Octopus dayend response", data);
                                });

                                callback({
                                    "result": "OK", "resultObj": obj
                                });
                                if (args.isUseECR)
                                    RAS.ECR.checkECR({
                                        service: RAS.ECR.service.CUP_Settlement, till: args.till
                                    }, function (result) {
                                        //RAS.ECR.checkECR({ service: RAS.ECR.service.EDC_Settlement, till: args.till });

                                        RAS.ECR.insertECRTransaction({
                                            service: RAS.ECR.service.CUP_Settlement, till: args.till, isSettlement: true
                                        }, result, function () {

                                        });

                                        setTimeout(function () {
                                            RAS.ECR.checkECR({
                                                service: RAS.ECR.service.EDC_Settlement, till: args.till
                                            }, function (result2) {
                                                RAS.ECR.insertECRTransaction({
                                                    service: RAS.ECR.service.EDC_Settlement, till: args.till, isSettlement: true
                                                }, result2, function () {

                                                });
                                            });
                                        }, 5000);
                                    });
                                fs.writeFile("./cloud/dailyclearance/" + moment().format("YYMMDDHHmmss") + ".json", JSON.stringify(cloud), function (err) {
                                    if (err) {
                                        //console.log(err);
                                    } else {
                                        ////console.log("The file was saved!");
                                    }
                                });


                            })

                        });

                    }
                });





            }

        })



    }

    this.PrepareDailyReportCloud = (args, callback) => {


        var query = [{ "sql": "select * from tenderpayment  where void = 0 ", "args": [] },
        { "sql": "select * from tenderDiscount where void = 0 ", "args": [] },
        { "sql": "select * from [transaction] where void = 0 ", "args": [] },
        { "sql": "select * from [tableOrder] ", "args": [] }];
        var date = moment().format("DD/MM/YYYY");
        //console.log(args);
        if (args) {
            //console.log(args.year);
            if (args.year) {
                var argsdate = pad(args.day, 2) + "/" + pad(args.month, 2) + "/" + args.year;

                if (args.toYear) {
                    var argsTodate = pad(args.toDay, 2) + "/" + pad(args.toMonth, 2) + "/" + args.toYear;

                    var sqlFromDates = argsdate;
                    var sqlToDates = pad(parseInt(args.toDay), 2) + "/" + pad(args.toMonth, 2) + "/" + args.toYear;



                    //console.log(sqlFromDates);
                    //console.log(sqlToDates);
                    date = argsdate + ' - ' + argsTodate;
                    query = [{ "sql": "select * from tenderpaymentArchive where  exists(select * from transactionArchive where convert(char(10), transTime, 103) between '" + sqlFromDates + "' and '" + sqlToDates + "' and transactionArchive.refNo = tenderPaymentArchive.refNo)  and void = 0 union select * from tenderpayment where  exists (select * from [transaction] where convert(char(10), transTime, 103) between '" + sqlFromDates + "' and '" + sqlToDates + "' and [transaction].refNo = tenderPayment.refNo) and void = 0 ", "args": [] },

                        { "sql": "select * from tenderDiscountArchive where   exists(select * from transactionArchive where convert(char(10), transTime, 103) between '" + sqlFromDates + "' and '" + sqlToDates + "'  and transactionArchive.refNo = tenderDiscountArchive.refNo)  and void = 0   union select * from tenderDiscount where exists (select * from [transaction] where convert(char(10), transTime, 103) between '" + sqlFromDates + "' and '" + sqlToDates + "'  and [transaction].refNo = tenderDiscount.refNo)  and void = 0 ", "args": [] },

                        { "sql": "select [tableCode] ,[tableSubCode],[pax] ,[staffId] ,[memberId] ,[memberName] ,[startTime] ,[refNo] ,[transTime] ,[payAmount],[price], [serviceCharge],[remainings]    ,[reprint]   ,[void]    ,[voidStaff]    ,[orderId]  from transactionArchive where  convert(char(10), transTime, 103) between  '" + sqlFromDates + "' and '" + sqlToDates + "' and void = 0 union select [tableCode] ,[tableSubCode],[pax] ,[staffId] ,[memberId] ,[memberName] ,[startTime] ,[refNo] ,[transTime] ,[payAmount],[price], [serviceCharge],[remainings]    ,[reprint]   ,[void]    ,[voidStaff]    ,[orderId] from [transaction] where  convert(char(10), transTime, 103) between  '" + sqlFromDates + "' and '" + sqlToDates + "'   and void = 0", "args": [] },
                        { "sql": "select * from [tableOrder] where 1 = 0 ", "args": [] }];
                } else {

                    if (date == argsdate) {
                        query = [{ "sql": "select * from tenderpayment union select * from tenderpaymentArchive where exists(select * from transactionArchive where convert(char(10), transTime, 103) = '" + argsdate + "' and transactionArchive.refNo = tenderpaymentArchive.refNo)   and void = 0 ", "args": [] },
                         { "sql": "select * from tenderDiscount where void = 0  union select * from tenderDiscountArchive where exists(select * from transactionArchive where convert(char(10), transTime, 103) = '" + argsdate + "' and transactionArchive.refNo = tenderDiscountArchive.refNo)   and void = 0 ", "args": [] },
                         { "sql": "select [tableCode] ,[tableSubCode] ,[pax],[staffId],[memberId],[memberName],[startTime],[refNo],[transTime],[payAmount],[price],[serviceCharge],[remainings],[reprint]      ,[void],[voidStaff] from [transaction] where void = 0  union select [tableCode] ,[tableSubCode] ,[pax],[staffId],[memberId],[memberName],[startTime],[refNo],[transTime],[payAmount],[price],[serviceCharge],[remainings],[reprint]      ,[void],[voidStaff] from transactionArchive where convert(char(10), transTime, 103) = '" + argsdate + "'  and void = 0 ", "args": [] },
                         { "sql": "select * from [tableOrder] ", "args": [] }];
                    } else {
                        date = argsdate;
                        query = [{ "sql": "select * from tenderpaymentArchive where  exists(select * from transactionArchive where convert(char(10), transTime, 103) = '" + argsdate + "' and transactionArchive.refNo = tenderPaymentArchive.refNo)  and void = 0 union select * from tenderpayment where  exists (select * from [transaction] where convert(char(10), transTime, 103) = '" + argsdate + "' and [transaction].refNo = tenderPayment.refNo) and void = 0 ", "args": [] },
                         { "sql": "select * from tenderDiscountArchive where   exists(select * from transactionArchive where convert(char(10), transTime, 103) = '" + argsdate + "' and transactionArchive.refNo = tenderDiscountArchive.refNo)  and void = 0 union select * from tenderDiscount where exists (select * from [transaction] where convert(char(10), transTime, 103) = '" + argsdate + "'  and [transaction].refNo = tenderDiscount.refNo)  and void = 0 ", "args": [] },
                         { "sql": "select [tableCode] ,[tableSubCode],[pax] ,[staffId] ,[memberId] ,[memberName] ,[startTime] ,[refNo] ,[transTime] ,[payAmount],[price], [serviceCharge],[remainings]    ,[reprint]   ,[void]    ,[voidStaff]    ,[orderId] from transactionArchive where convert(char(10), transTime, 103) = '" + argsdate + "' and void = 0 union select [tableCode] ,[tableSubCode],[pax] ,[staffId] ,[memberId] ,[memberName] ,[startTime] ,[refNo] ,[transTime] ,[payAmount],[price], [serviceCharge],[remainings]    ,[reprint]   ,[void]    ,[voidStaff]    ,[orderId] from [transaction] where  convert(char(10), transTime, 103) = '" + argsdate + "'   and void = 0", "args": [] },
                        { "sql": "select * from [tableOrder] where 1 = 0 ", "args": [] }]
                    }
                }
            }
        }


        var gobalTender = this.tender;

        AzureSocket.emit('DailyReport', JSON.stringify(query), function (result) {
            result = JSON.parse(result);

            var obj = { GROSS_TOTAL_AMOUNT: 0, DISCOUNT: 0, NO_OF_DISCOUNT: 0, SERVICE_CHARGE: 0, GROSS_SALES_NET: 0, DECIMAIL_POINT: 0, TOTAL_CARD_TIPS: 0, NO_OF_CARD_TIPS: 0, TOTAL_NO_OF_CARD: 0, TOTAL_CARD_AMOUNT: 0, NONCASHSALES: [], CASHSALES: { paymentValue: 0, count: 0 }, CARDTIPS: [], DISCOUNTDETAILS: [], NO_OF_INVOICE: 0, GROSS_SALES_NET_PER_INVOICE: 0, TOTAL_PAX: 0, TOTAL_PAX_PER_AMOUNT: 0, TABLEORDER: [], TOTAL_SALES_AMOUNT: 0, ENTERTAINMENT: [], REPORT_DATE: '' };
            var printer = args.printer ? args.printer : "";
            obj.REPORT_DATE = date;
            obj.printer = printer;
            var transaction = result[2];
            var discount = result[1];
            var payment = result[0];
            var tableOrder = result[3];
            if (tableOrder) {
                for (var i = 0; i < tableOrder.length; i++) {
                    var to = tableOrder[i];
                    if (i == 0) {
                        obj.TABLEORDER.push({ "pax": to.pax, "price": to.price + to.serviceCharge, "count": 1 })
                    } else {
                        obj.TABLEORDER[0].pax += to.pax;
                        obj.TABLEORDER[0].price += to.price + to.serviceCharge;
                        obj.TABLEORDER[0].count += 1;
                    }
                }
            }
            if (transaction) {

                for (var i = 0; i < transaction.length; i++) {
                    var t = transaction[i];

                    obj.TOTAL_PAX += t.pax;
                    obj.GROSS_TOTAL_AMOUNT += t.price;
                    obj.SERVICE_CHARGE += t.serviceCharge;
                    obj.DECIMAIL_POINT += t.remainings;
                    var totalPrice = t.price + t.serviceCharge + t.remainings;
                    var dis = discount.filter(function (dis) {
                        return dis.refNo == t.refNo;
                    })
                    for (var j = 0; j < dis.length; j++) {
                        totalPrice -= dis[j].discountValue;
                    }
                    var po = payment.filter(function (po) {
                        return po.refNo == t.refNo;
                    })
                    var pricefornoncash = totalPrice
                    for (var j = 0; j < po.length; j++) {
                        var p = po[j];
                        if (p.paymentType == 'cash') {
                            if (pricefornoncash < p.paymentValue) {
                                obj.CASHSALES.paymentValue += pricefornoncash;
                                pricefornoncash = 0;
                            } else {
                                obj.CASHSALES.paymentValue += p.paymentValue;
                                pricefornoncash -= p.paymentValue;
                            }
                            obj.CASHSALES.count += 1;
                            break;
                        }
                    }
                    for (var j = 0; j < po.length; j++) {
                        var p = po[j];
                        var tenderObj = gobalTender.filter((t) => { return t.tenderCode == p.paymentType })[0];
                        if (tenderObj == undefined) break;
                        if (p.paymentType != 'cash') {
                            var nonCashPayment = 0;
                            if (p.paymentValue > pricefornoncash) {
                                nonCashPayment = pricefornoncash;
                                //if (p.paymentType != 'octopus' && p.paymentType != 'entertainment' && p.paymentType != 'on_acct') {
                                if (tenderObj.isCard) {
                                    obj.TOTAL_CARD_AMOUNT += pricefornoncash;
                                    var cardtips = p.paymentValue - pricefornoncash
                                    obj.TOTAL_CARD_TIPS += cardtips;
                                    obj.NO_OF_CARD_TIPS += 1;
                                    var foundMatchCardTipsPayment = false;
                                    for (var k = 0; k < obj.CARDTIPS.length; k++) {
                                        if (p.paymentType == obj.CARDTIPS[k].paymentType) {
                                            obj.CARDTIPS[k].tipsValue += cardtips
                                            obj.CARDTIPS[k].count += 1;
                                            foundMatchCardTipsPayment = true;
                                            break;
                                        }
                                    }
                                    if (!foundMatchCardTipsPayment) {
                                        obj.CARDTIPS.push({ paymentType: p.paymentType, tipsValue: cardtips, count: 1 })
                                    }
                                }
                                pricefornoncash = 0;
                            } else {
                                //if (p.paymentType != 'octopus' && p.paymentType != 'entertainment' && p.paymentType != 'on_acct') {
                                if (tenderObj.isCard) {
                                    obj.TOTAL_CARD_AMOUNT += p.paymentValue;
                                }
                                nonCashPayment = p.paymentValue;
                                pricefornoncash -= p.paymentValue
                            }

                            var foundMatchPaymentType = false;
                            for (var k = 0; k < obj.NONCASHSALES.length; k++) {
                                if (obj.NONCASHSALES[k].paymentType == p.paymentType) {
                                    obj.NONCASHSALES[k].paymentValue += nonCashPayment;
                                    obj.NONCASHSALES[k].count += 1;
                                    foundMatchPaymentType = true;
                                }
                            }
                            if (!foundMatchPaymentType) {
                                obj.NONCASHSALES.push({ "paymentType": p.paymentType, "paymentValue": nonCashPayment, "count": 1 })
                            }
                        }
                    }
                }
            }

            for (var i = 0; i < obj.NONCASHSALES.length; i++) {

                var tenderObj = gobalTender.filter((t) => { return t.tenderCode == obj.NONCASHSALES[i].paymentType })[0];
                //if (obj.NONCASHSALES[i].paymentType == "entertainment") {
                if (tenderObj.isEnt) {
                    obj.ENTERTAINMENT.push({ count: obj.NONCASHSALES[i].count, paymentValue: obj.NONCASHSALES[i].paymentValue })
                }
            }

            //console.log(obj.TOTAL_CARD_AMOUNT);
            for (var i = 0; i < discount.length; i++) {
                var d = discount[i];
                obj.DISCOUNT += d.discountValue;
                var foundMatchDiscountDetails = false;
                for (var k = 0; k < obj.DISCOUNTDETAILS.length; k++) {
                    if (obj.DISCOUNTDETAILS[k].discountCode == d.discountCode) {
                        obj.DISCOUNTDETAILS[k].discountValue += d.discountValue;
                        obj.DISCOUNTDETAILS[k].count += 1;
                        foundMatchDiscountDetails = true;
                        break;
                    }
                }
                if (!foundMatchDiscountDetails) {
                    obj.DISCOUNTDETAILS.push({ discountCode: d.discountCode, discountValue: d.discountValue, count: 1 })
                }
            }
            obj.GROSS_SALES_NET = obj.GROSS_TOTAL_AMOUNT + obj.SERVICE_CHARGE + obj.DECIMAIL_POINT - obj.DISCOUNT
            if (obj.ENTERTAINMENT.length != 0) {
                obj.GROSS_SALES_NET -= obj.ENTERTAINMENT[0].paymentValue;
            }

            //obj.tender.

            var cardPayment = payment.filter((p) => {
                //return p.paymentType != 'cash' && p.paymentType != 'octopus' && p.paymentType != 'entertainment' 
                return gobalTender.filter((t) => {
                    return t.tenderCode == p.paymentType && t.isCard;
                }).length > 0;
            });
            //console.log(cardPayment);
            obj.NO_OF_DISCOUNT = discount.length;
            obj.TOTAL_NO_OF_CARD = cardPayment.length;
            obj.NO_OF_INVOICE = transaction.length;
            obj.GROSS_SALES_NET_PER_INVOICE = obj.GROSS_SALES_NET / obj.NO_OF_INVOICE;
            obj.TOTAL_PAX_PER_AMOUNT = obj.GROSS_SALES_NET / obj.TOTAL_PAX;
            obj.TOTAL_SALES_AMOUNT = obj.GROSS_SALES_NET;
            if (obj.TABLEORDER.length != 0) {
                obj.TOTAL_SALES_AMOUNT += obj.TABLEORDER[0].price;
            }
            //if (obj.TOTAL_NO_OF_CARD != 0) {
            //    obj.TOTAL_CARD_AMOUNT = cardPayment.reduce((sum, val) => { return sum + val.paymentValue },0)
            //}
            //obj.TOTAL_CARD_TIPS = obj.TOTAL_CARD_TIPS.format(1, 3);
            //obj.GROSS_SALES_NET = obj.GROSS_SALES_NET.format(1, 3);
            //obj.GROSS_TOTAL_AMOUNT = obj.GROSS_TOTAL_AMOUNT.format(1, 3);
            callback(obj);

            if (args.isPrint) {

                var _obj = {};
                extend(obj, _obj);

                _obj.GROSS_TOTAL_AMOUNT = S("$" + _obj.GROSS_TOTAL_AMOUNT.format(1, 3)).padLeft(10).s;
                _obj.DISCOUNT = S("$" + _obj.DISCOUNT.format(1, 3)).padLeft(10).s;
                //_obj.NO_OF_DISCOUNT = S("$" + _obj.NO_OF_DISCOUNT.format(1, 3)).padLeft(10).s;
                _obj.SERVICE_CHARGE = S("$" + _obj.SERVICE_CHARGE.format(1, 3)).padLeft(10).s;
                _obj.GROSS_SALES_NET = S("$" + _obj.GROSS_SALES_NET.format(1, 3)).padLeft(10).s;
                _obj.DECIMAIL_POINT = S("$" + _obj.DECIMAIL_POINT.format(1, 3)).padLeft(10).s;
                _obj.TOTAL_CARD_TIPS = S("$" + _obj.TOTAL_CARD_TIPS.format(1, 3)).padLeft(10).s;
                //_obj.NO_OF_CARD_TIPS = S("$" + _obj.NO_OF_CARD_TIPS.format(1, 3)).padLeft(10).s;
                //_obj.TOTAL_NO_OF_CARD = S("$" + _obj.TOTAL_NO_OF_CARD.format(1, 3)).padLeft(10).s;
                _obj.TOTAL_CARD_AMOUNT = S("$" + _obj.TOTAL_CARD_AMOUNT.format(1, 3)).padLeft(10).s;
                _obj.pettyCash = S("$" + _obj.pettyCash.format(1, 3)).padLeft(10).s;

                for (var i = 0; i < _obj.NONCASHSALES.length; i++) {
                    _obj.NONCASHSALES[i].paymentValue = S("$" + _obj.NONCASHSALES[i].paymentValue.format(1, 3)).padLeft(10).s;
                }
                _obj.CASHSALES.paymentValue = S("$" + _obj.CASHSALES.paymentValue.format(1, 3)).padLeft(10).s;

                for (var i = 0; i < _obj.CARDTIPS.length; i++) {
                    _obj.CARDTIPS[i].tipsValue = S("$" + _obj.CARDTIPS[i].tipsValue.format(1, 3)).padLeft(10).s;
                }
                for (var i = 0; i < _obj.DISCOUNTDETAILS.length; i++) {
                    _obj.DISCOUNTDETAILS[i].discountValue = S("$" + _obj.DISCOUNTDETAILS[i].discountValue.format(1, 3)).padLeft(10).s;
                }
                _obj.GROSS_SALES_NET_PER_INVOICE = S("$" + _obj.GROSS_SALES_NET_PER_INVOICE.format(1, 3)).padLeft(10).s;
                _obj.TOTAL_PAX_PER_AMOUNT = S("$" + _obj.TOTAL_PAX_PER_AMOUNT.format(1, 3)).padLeft(10).s;


                for (var i = 0; i < _obj.TABLEORDER.length; i++) {
                    _obj.TABLEORDER[i].price = S("$" + _obj.TABLEORDER[i].price.format(1, 3)).padLeft(10).s;
                }

                _obj.TOTAL_SALES_AMOUNT = S("$" + _obj.TOTAL_SALES_AMOUNT.format(1, 3)).padLeft(10).s;

                if (_obj.ENTERTAINMENT.length != 0) {
                    _obj.ENTERTAINMENT[0].paymentValue = S("$" + _obj.ENTERTAINMENT[0].paymentValue.format(1, 3)).padLeft(10).s;;
                }
                //_obj.CASHSALESPAYMENTVALUE = _obj.CASHSALES.paymentValue;
                //_obj.CASHSALESPAYMENTCOUNT = _obj.CASHSALES.count;

                fs.writeFile("./printer/dailyReport/" + moment().format("YYMMDDHHmmss") + ".json", JSON.stringify(_obj), function (err) {
                    if (err) {
                        //console.log(err);
                    } else {
                        ////console.log("The file was saved!");
                    }
                });
            }
        });

    };


    this.PrepareDailyClearanceReport = (args, callback) => {

        var reportObject = new report.dailyArchiveReport(args, "pos", this.DM);
        reportObject.getCallbackResult(function (obj) {
            callback(obj);
        })
    }

    this.PrepareDailyVoidECRReport = (args, callback) => {
        var reportObject = new report.dailyVoidECRReport(args, "pos", this.DM);
        reportObject.getCallbackResult(function (obj) {
            callback(obj);
        })
    }

    this.PrepareDailyReport = (args, callback) => {
        var reportObject = new report.dailyReport(args, "pos", this.DM);
        reportObject.getCallbackResult(function (obj) {
            callback(obj);
        })
    }

    this.getDailyReportObj = function (transaction, payment, discount, tableOrder, tender, title) {

        var obj = { title: title, GROSS_TOTAL_AMOUNT: 0, DISCOUNT: 0, NO_OF_DISCOUNT: 0, SERVICE_CHARGE: 0, GROSS_SALES_NET: 0, DECIMAIL_POINT: 0, TOTAL_CARD_TIPS: 0, NO_OF_CARD_TIPS: 0, TOTAL_NO_OF_CARD: 0, TOTAL_CARD_AMOUNT: 0, NONCASHSALES: [], CASHSALES: [], CARDTIPS: [], DISCOUNTDETAILS: [], NO_OF_INVOICE: 0, GROSS_SALES_NET_PER_INVOICE: 0, TOTAL_PAX: 0, TOTAL_PAX_PER_AMOUNT: 0, TABLEORDER: [], TOTAL_SALES_AMOUNT: 0, ENTERTAINMENT: [], REPORT_DATE: '', TOTAL_CASH_AMOUNT: 0, TOTAL_CASH_COUNT: 0, pettyCash: 0, CARDS: [], tenderPayment: [] };


        tender.forEach(function (t) {
            obj.tenderPayment[t.tenderCode] = [];
        })


        if (tableOrder) {
            for (var i = 0; i < tableOrder.length; i++) {
                var to = tableOrder[i];
                if (i == 0) {
                    obj.TABLEORDER.push({ "pax": to.pax, "price": to.price + to.serviceCharge, "count": 1 })
                } else {
                    obj.TABLEORDER[0].pax += to.pax;
                    obj.TABLEORDER[0].price += to.price + to.serviceCharge;
                    obj.TABLEORDER[0].count += 1;
                }
            }
        }
        if (transaction) {

            for (var i = 0; i < transaction.length; i++) {
                var t = transaction[i];
                obj.TOTAL_PAX += t.pax;
                obj.GROSS_TOTAL_AMOUNT += t.price;
                obj.SERVICE_CHARGE += t.serviceCharge;
                obj.DECIMAIL_POINT += t.remainings;
                var totalPrice = t.price + t.serviceCharge + t.remainings;
                var dis = discount.filter(function (dis) {
                    return dis.refNo == t.refNo;
                })
                for (var j = 0; j < dis.length; j++) {
                    totalPrice -= dis[j].discountValue;
                }
                var po = payment.filter(function (po) {
                    return po.refNo == t.refNo;
                })
                var pricefornoncash = totalPrice

                //console.log('po', po);
                for (var j = 0; j < po.length; j++) {
                    var p = po[j];
                    if (p.paymentType == 'cash') {

                        //console.log(p)
                        //console.log('pricefornoncash:', pricefornoncash);
                        var cashSales = pricefornoncash;

                        if (p.priceValue) {
                            pricefornoncash -= p.priceValue;
                            cashSales = p.priceValue;
                        } else {

                            if (pricefornoncash < p.paymentValue) {
                                pricefornoncash = 0;
                            } else {
                                cashSales = p.paymentValue;
                                pricefornoncash -= p.paymentValue;
                            }

                        }


                        var foundTill = false;

                        obj.CASHSALES.every((cs) => {
                            //console.log(cs.till === p.till);
                            //console.log(cs.till);
                            //console.log(p.till);
                            if (cs.till === p.till) {
                                cs.paymentValue += cashSales;
                                cs.count += 1;
                                foundTill = true;
                                return false;
                            }
                            return true;
                        })
                        if (!foundTill) {
                            obj.CASHSALES.push({ paymentValue: cashSales, count: 1, till: p.till });
                        }

                        obj.TOTAL_CASH_COUNT += 1;
                        obj.TOTAL_CASH_AMOUNT += cashSales;

                        obj.tenderPayment[p.paymentType].push({ refNo: p.refNo, paymentValue: cashSales })
                        //obj.CASHSALES.count += 1;
                        //console.log("price", obj.CASHSALES.paymentValue);
                        //break;?
                    }
                }

                for (var j = 0; j < po.length; j++) {
                    var p = po[j];

                    var tenderObj = this.tender.filter((t) => { return t.tenderCode == p.paymentType })[0];
                    if (tenderObj == undefined) break;
                    if (p.paymentType != 'cash') {
                        var nonCashPayment = 0;
                        var remainingPrice = pricefornoncash;
                        if (p.priceValue && tenderObj.isCard) {
                            remainingPrice = p.priceValue;
                        }

                        if (tenderObj.isCard) {
                            obj.TOTAL_NO_OF_CARD += 1;
                        }

                        if (p.paymentValue > remainingPrice) {
                            nonCashPayment = remainingPrice;
                            //if (p.paymentType != 'octopus' && p.paymentType != 'entertainment' && p.paymentType != 'on_acct') {
                            if (tenderObj.isCard) {
                                //obj.CARDS..push({ paymentType: p.paymentType, tipsValue: cardtips, count: 1, till: p.till })
                                obj.TOTAL_CARD_AMOUNT += remainingPrice;



                                var cardtips = p.paymentValue - remainingPrice;
                                obj.tenderPayment[p.paymentType].push({ refNo: p.refNo, paymentValue: remainingPrice, tips: cardtips });


                                var foundMatchCardPayment = false;
                                for (var k = 0; k < obj.CARDS.length; k++) {
                                    if (p.paymentType === obj.CARDS[k].paymentType) {
                                        foundMatchCardPayment = true;
                                        obj.CARDS[k].paymentValue += p.paymentValue;

                                        if (cardtips != p.paymentValue) {
                                            obj.CARDS[k].count += 1;
                                        }
                                    }
                                }
                                if (!foundMatchCardPayment) {
                                    obj.CARDS.push({ paymentType: p.paymentType, paymentValue: p.paymentValue, count: 1 })
                                }


                                obj.TOTAL_CARD_TIPS += cardtips;
                                obj.NO_OF_CARD_TIPS += 1;
                                var foundMatchCardTipsPayment = false;
                                for (var k = 0; k < obj.CARDTIPS.length; k++) {
                                    if (p.paymentType === obj.CARDTIPS[k].paymentType && p.till === obj.CARDTIPS[k].till) {
                                        obj.CARDTIPS[k].tipsValue += cardtips
                                        obj.CARDTIPS[k].count += 1;
                                        foundMatchCardTipsPayment = true;
                                        break;
                                    }
                                }
                                if (!foundMatchCardTipsPayment) {
                                    obj.CARDTIPS.push({ paymentType: p.paymentType, tipsValue: cardtips, count: 1, till: p.till })
                                }
                            }
                            remainingPrice = 0;
                        }
                        else {
                            //if (p.paymentType != 'octopus' && p.paymentType != 'entertainment' && p.paymentType != 'on_acct') {
                            var foundMatchCardPayment = false;
                            for (var k = 0; k < obj.CARDS.length; k++) {
                                if (p.paymentType === obj.CARDS[k].paymentType) {
                                    foundMatchCardPayment = true;
                                    obj.CARDS[k].paymentValue += p.paymentValue;

                                    if (cardtips != p.paymentValue) {
                                        obj.CARDS[k].count += 1;
                                    }
                                }
                            }
                            if (!foundMatchCardPayment) {
                                obj.CARDS.push({ paymentType: p.paymentType, paymentValue: p.paymentValue, count: 1 })
                            }

                            if (tenderObj.isCard) {
                                obj.TOTAL_CARD_AMOUNT += p.paymentValue;
                                obj.tenderPayment[p.paymentType].push({ refNo: p.refNo, paymentValue: p.paymentValue, tips: 0 });
                            }
                            nonCashPayment = p.paymentValue;
                            pricefornoncash -= p.paymentValue
                        }


                        var foundMatchPaymentType = false;
                        for (var k = 0; k < obj.NONCASHSALES.length; k++) {
                            if (obj.NONCASHSALES[k].paymentType === p.paymentType && p.till === obj.NONCASHSALES[k].till) {
                                if (p.paymentType == "cashCoupon") {
                                    obj.NONCASHSALES[k].paymentValue += p.paymentValue;
                                    obj.TOTAL_CASH_AMOUNT -= p.paymentValue;
                                    obj.TOTAL_CASH_AMOUNT += nonCashPayment;
                                    var tillCash = obj.CASHSALES.filter(function (t) { return t.till == p.till; });
                                    if (tillCash.length != 0) {
                                        tillCash[0].paymentValue -= p.paymentValue;
                                        tillCash[0].paymentValue += nonCashPayment;
                                    }

                                }
                                else {
                                    //obj.NONCASHSALES[k].paymentValue += p.paymentValue;
                                    obj.NONCASHSALES[k].paymentValue += nonCashPayment;
                                }
                                obj.NONCASHSALES[k].count += 1;
                                foundMatchPaymentType = true;
                            }
                        }
                        if (!foundMatchPaymentType) {
                            obj.NONCASHSALES.push({ "paymentType": p.paymentType, "paymentValue": p.paymentType == "cashCoupon" ? p.paymentValue : nonCashPayment, "count": 1, "till": p.till })
                            if (p.paymentType == "cashCoupon") {
                                obj.TOTAL_CASH_AMOUNT -= p.paymentValue;
                                obj.TOTAL_CASH_AMOUNT += nonCashPayment;
                                var tillCash = obj.CASHSALES.filter(function (t) { return t.till == p.till; });
                                if (tillCash.length != 0) {
                                    tillCash[0].paymentValue -= p.paymentValue;
                                    tillCash[0].paymentValue += nonCashPayment;
                                }
                            }
                        }

                        // console.log(obj.NONCASHSALES)
                    }
                }
            }
        }

        for (var i = 0; i < obj.NONCASHSALES.length; i++) {

            var tenderObj = this.tender.filter((t) => { return t.tenderCode == obj.NONCASHSALES[i].paymentType })[0];
            //if (obj.NONCASHSALES[i].paymentType == "entertainment") {
            if (tenderObj.isEnt) {
                obj.ENTERTAINMENT.push({ count: obj.NONCASHSALES[i].count, paymentValue: obj.NONCASHSALES[i].paymentValue, paymentType: tenderObj.tenderName1 })
            }

        }

        //console.log(obj.TOTAL_CARD_AMOUNT);
        for (var i = 0; i < discount.length; i++) {
            var d = discount[i];
            obj.DISCOUNT += d.discountValue;
            var foundMatchDiscountDetails = false;
            for (var k = 0; k < obj.DISCOUNTDETAILS.length; k++) {
                if (obj.DISCOUNTDETAILS[k].discountCode == d.discountCode) {
                    obj.DISCOUNTDETAILS[k].discountValue += d.discountValue;
                    obj.DISCOUNTDETAILS[k].count += 1;
                    foundMatchDiscountDetails = true;
                    break;
                }
            }
            if (!foundMatchDiscountDetails) {
                obj.DISCOUNTDETAILS.push({ discountCode: d.discountCode, discountValue: d.discountValue, count: 1 })
            }
        }
        obj.GROSS_SALES_NET = obj.GROSS_TOTAL_AMOUNT + obj.SERVICE_CHARGE + obj.DECIMAIL_POINT - obj.DISCOUNT
        if (obj.ENTERTAINMENT.length != 0) {

            obj.ENTERTAINMENT.forEach(function (e) {
                obj.GROSS_SALES_NET -= e.paymentValue;
            })
            //obj.GROSS_SALES_NET -= obj.ENTERTAINMENT[0].paymentValue;
        }

        //obj.tender.

        //var cardPayment = payment.filter((p) => {
        //    //return p.paymentType != 'cash' && p.paymentType != 'octopus' && p.paymentType != 'entertainment' 
        //    return this.tender.filter((t) => {
        //        return t.tenderCode == p.paymentType && t.isCard;
        //    }).length > 0;
        //});

        //pending
        //for (var i = 0; i < obj.CARDS.length; i++) {
        //    obj.TOTAL_NO_OF_CARD += obj.CARDS[i].count;
        //    for (var j = 0; j < obj.NONCASHSALES.length; j++) {
        //        obj.NONCASHSALES[j].paymentType == 
        //        var tenderObj = this.tender.filter((t) => { return t.tenderCode == obj.NONCASHSALES[i].paymentType })[0];
        //        //if (obj.NONCASHSALES[i].paymentType == "entertainment") {
        //        if (tenderObj.isEnt) {
        //            obj.ENTERTAINMENT.push({ count: obj.NONCASHSALES[i].count, paymentValue: obj.NONCASHSALES[i].paymentValue, paymentType: tenderObj.tenderName1 })
        //        }

        //    }

        //}





        //console.log(cardPayment);
        obj.NO_OF_DISCOUNT = discount.length;
        //obj.TOTAL_NO_OF_CARD = cardPayment.length;
        obj.NO_OF_INVOICE = transaction.length;
        obj.GROSS_SALES_NET_PER_INVOICE = obj.GROSS_SALES_NET / obj.NO_OF_INVOICE;
        obj.TOTAL_PAX_PER_AMOUNT = obj.GROSS_SALES_NET / obj.TOTAL_PAX;
        obj.TOTAL_SALES_AMOUNT = obj.GROSS_SALES_NET;
        if (obj.TABLEORDER.length != 0) {
            obj.TOTAL_SALES_AMOUNT += obj.TABLEORDER[0].price;
        }
        //if (obj.TOTAL_NO_OF_CARD != 0) {
        //    obj.TOTAL_CARD_AMOUNT = cardPayment.reduce((sum, val) => { return sum + val.paymentValue },0)
        //}
        //obj.TOTAL_CARD_TIPS = obj.TOTAL_CARD_TIPS.format(1, 3);
        //obj.GROSS_SALES_NET = obj.GROSS_SALES_NET.format(1, 3);
        //obj.GROSS_TOTAL_AMOUNT = obj.GROSS_TOTAL_AMOUNT.format(1, 3);

        return obj;
    }

    this.PrepareDailyVoidBillReport = (args, callback) => {


        var reportObject = new report.dailyVoidBillReport(args, "pos", this.DM);

        reportObject.getCallbackResult(function (obj) {
            callback(obj);
        })
    }

    this.PrepareDailydailyCancelItemReport = (args, callback) => {

        var reportObject = new report.dailyCancelItemReport(args, "pos", this.DM);

        reportObject.getCallbackResult(function (obj) {
            callback(obj);
        })
    }

    this.PrepareDailyFoodTimeslotReport = (args, callback) => {

        var reportObject = new report.dailyFoodTimeslotReport(args, "pos", this.DM);
        reportObject.getCallbackResult(function (obj) {
            callback(obj);
        })

    };

    this.PrepareDailySalesNet = (args, callback) => {

        var reportObject = new report.dailySalesNetReport(args, "pos", this.DM);
        reportObject.getCallbackResult(function (obj) {
            callback(obj);
        })
    }

    this.PrepareDailyDiscount = (args, callback) => {


        var reportObject = new report.dailyDiscountReport(args, "pos", this.DM);

        reportObject.getCallbackResult(function (obj) {
            callback(obj);
        })

    }

    this.PrepareWatchServiceReport = (args, callback) => {

        var reportObject = new report.dailyWatchServiceReport(args, "pos", this.DM);

        reportObject.getCallbackResult(function (obj) {
            callback(obj);
        })
    }

    this.PrepareDailyAttendanceReport = (args, callback) => {

        var reportObject = new report.dailyAttendanceReport(args, "pos", this.DM);

        reportObject.getCallbackResult(function (obj) {
            callback(obj);
        })
    }

    this.PrepareDailyMostSalesFood = (args, sortBy, callback) => {

        var reportObject = new report.dailyMostSalesFoodReport(args, "pos", this.DM);
        reportObject.getCallbackResult(sortBy, function (obj) {
            callback(obj);
        })
    }

    this.PrepareDailyPaymentDetail = (args, callback) => {

        var reportObject = new report.dailyPaymentDetailReport(args, "pos", this.DM);
        var query = reportObject.getQuery();

        reportObject.getCallbackResult(function (obj) {
            callback(obj);
        })

    }

    this.PrepareDailyCategorySales = (args, callback) => {

        var reportObject = new report.dailyCategoriesReport(args, "pos", this.DM);
        reportObject.getCallbackResult(function (obj) {
            callback(obj);
        })

    }

    this.PrepareDailyPerHourSales = (args, callback) => {


        var reportObject = new report.dailyPerHourSalesReport(args, "pos", this.DM);
        reportObject.getCallbackResult(function (obj) {
            callback(obj);
        })
    }

    this.PrepareDailyItemReport = (args, callback) => {

        var query = [
           {
               "sql": "SELECT t.[refNo], [startTime] ,td.tableCode, i.namek ,t.[price] , td.qty, td.[itemId] ,td.[type], td.seq, td.[time] as [Time], staff.name as staff, s2.name as ApproveName, td.voidRemark, td.price as subPrice,td.round , [option].namek as optionName, [subitem].namek as SubitemName, category.name1, category.categoryId FROM [##transaction##] as t  inner join [##transactionD##] as td on td.refNo = t.refNo   Left join item as i on i.itemId = td.itemId   Left join [option] on  [option].[optionCode] = td.itemId Left join [staff] on staff.username = td.staffId  Left join [staff] as s2 on s2.username = td.approveStaffId  Left join [subitem] on [subitem].subitemCode = td.itemId  Left join [itemCategory] on [itemCategory].itemId = td.itemId Left join category  on category.categoryId = [itemCategory].categoryId where t.void = 0   ##CoditionWhere##      order by t.refNo, td.round, td.seq, t.startTime, td.time;",
               "args": []
           },
           { "sql": "SELECT * FROM [##transaction##] as t Left join [##tenderDiscount##] as tdd  on tdd.refNo = t.refNo and tdd.void = 0 where t.void = 0 ##CoditionWhere## ;", "args": [] }];













        var obj = { "Category": new Object, "REPORT_DATE": moment().format("DD/MM/YYYY"), "AllPrice": 0.0, "AllQty": 0, "Remaining": 0.0, "SaleNetPrice": 0.0, "DiscountPrice": 0.0, "ServiceCharge": 0.0, "SaleAllPrice": 0.0 }

        var date = moment().format("DD/MM/YYYY");
        if (args) {

            if (args.year) {

                var argsdate = pad(args.day, 2) + "/" + pad(args.month, 2) + "/" + args.year;

                if (args.toYear) {

                    var argsTodate = pad(args.toDay, 2) + "/" + pad(args.toMonth, 2) + "/" + args.toYear;
                    var sqlFromDates = new Date(args.year, parseInt(args.month) - 1, args.day);
                    var sqlToDates = new Date(args.toYear, parseInt(args.toMonth) - 1, parseInt(args.toDay) + 1);

                    var tempArgs_fromDate = {};
                    tempArgs_fromDate.c = "fromDate";
                    tempArgs_fromDate.t = TYPES.DateTime;
                    tempArgs_fromDate.v = sqlFromDates;

                    var tempArgs_toDate = {};
                    tempArgs_toDate.c = "toDate";
                    tempArgs_toDate.t = TYPES.DateTime;
                    tempArgs_toDate.v = sqlToDates;

                    query[0].sql = query[0].sql.replace("##transaction##", "transactionArchive")
                    query[0].sql = query[0].sql.replace("##transactionD##", "transactionDArchive")
                    query[0].sql = query[0].sql.replace("##CoditionWhere##", "and t.startTime between @fromDate and @toDate")
                    query[0].args = [];
                    query[0].args.push(tempArgs_fromDate, tempArgs_toDate);


                    query[1].sql = query[1].sql.replace("##transaction##", "transactionArchive")
                    query[1].sql = query[1].sql.replace("##tenderDiscount##", "tenderDiscountArchive")
                    query[1].sql = query[1].sql.replace("##CoditionWhere##", "and t.startTime between @fromDate and @toDate")
                    query[1].args = [];
                    query[1].args.push(tempArgs_fromDate, tempArgs_toDate);


                    date = argsdate + ' - ' + argsTodate;


                } else {

                    if (date == argsdate) {

                        query[0].sql = query[0].sql.replace("##transaction##", "transaction")
                        query[0].sql = query[0].sql.replace("##transactionD##", "transactionD")
                        query[0].sql = query[0].sql.replace("##CoditionWhere##", "")

                        query[1].sql = query[1].sql.replace("##transaction##", "transaction")
                        query[1].sql = query[1].sql.replace("##tenderDiscount##", "tenderDiscount")
                        query[1].sql = query[1].sql.replace("##CoditionWhere##", "")


                    } else {

                        date = argsdate;

                        var argsdate = new Date(args.year, parseInt(args.month) - 1, args.day);
                        var argsToDate = new Date(args.year, parseInt(args.month) - 1, args.day);

                        var tempArgs_fromDate = {};
                        tempArgs_fromDate.c = "fromDate";
                        tempArgs_fromDate.t = TYPES.DateTime;
                        tempArgs_fromDate.v = argsdate;

                        var tempArgs_toDate = {};
                        tempArgs_toDate.c = "toDate";
                        tempArgs_toDate.t = TYPES.DateTime;
                        tempArgs_toDate.v = argsToDate;

                        query[0].sql = query[0].sql.replace("##transaction##", "transactionArchive")
                        query[0].sql = query[0].sql.replace("##transactionD##", "transactionDArchive")
                        query[0].sql = query[0].sql.replace("##CoditionWhere##", "and t.startTime between @fromDate and @toDate")
                        query[0].args = [];
                        query[0].args.push(tempArgs_fromDate, tempArgs_toDate);


                        query[1].sql = query[1].sql.replace("##transaction##", "transactionArchive")
                        query[1].sql = query[1].sql.replace("##tenderDiscount##", "tenderDiscountArchive")
                        query[1].sql = query[1].sql.replace("##CoditionWhere##", "and t.startTime between @fromDate and @toDate")
                        query[1].args = [];
                        query[1].args.push(tempArgs_fromDate, tempArgs_toDate);


                    }


                }


            }
            else {

                query[0].sql = query[0].sql.replace("##transaction##", "transaction")
                query[0].sql = query[0].sql.replace("##transactionD##", "transactionD")
                query[0].sql = query[0].sql.replace("##CoditionWhere##", "")

                //and convert(char(8), [startTime], 114) >= '16:00:00' and convert(char(8), [startTime], 114) <= '23:59:59'






                query[1].sql = query[1].sql.replace("##transaction##", "transaction")
                query[1].sql = query[1].sql.replace("##tenderDiscount##", "tenderDiscount")
                //query[1].sql = query[1].sql.replace("##CoditionWhere##", "")
                query[1].sql = query[1].sql.replace("##CoditionWhere##", "")
                //and convert(char(8), [startTime], 114) >= '16:00:00' and convert(char(8), [startTime], 114) <= '23:59:59'

            }





        }
        obj.REPORT_DATE = date;



        this.DM.executeStatement(query, (err, result) => {

            var transaction = result[0];
            var category = new Category();
            var PackageList = new Array;

            var packageObject = new Package();
            packageObject.GroupPackage(transaction);
            PackageList = packageObject.PackageList


            var TempFood = new Food();
            PackageList.forEach(function (packageItem) {
                TempFood.addFoodByCategory(packageItem);
                packageItem.options.forEach(function (option) {
                    TempFood.addFoodByCategory(option);
                })
            })


            TempFood.FoodList.forEach(function (food) {

                obj.AllPrice += food.TotalPrice;
                obj.AllQty += food.TotalQty;
                category.addPackage(food);

            })


            category.CategoryList.forEach(function (item) {

                item.PackageList.forEach(function (food) {

                    food.Percentage = ((food.TotalPrice / item.CatePrice) * 100.00).toFixed(2);


                })

                item.Percentage = ((item.CatePrice / obj.AllPrice) * 100.00).toFixed(2);
            })

            obj.CategoryList = category.CategoryList;



            var OrderList = result[1];
            //OrderList.forEach(function (order) {
            //    obj.SaleNetPrice += order.price;
            //    obj.ServiceCharge += order.serviceCharge;
            //    obj.Remaining += order.remainings;
            //    obj.DiscountPrice += order.discountValue;
            //})


            for (var a = 0; a < OrderList.length; a++) {
                //obj.SaleNetPrice += OrderList[a].price;
                //obj.NumOfPax += OrderList[a].pax;
                obj.ServiceCharge += OrderList[a].serviceCharge;
                obj.Remaining += OrderList[a].remainings;
                obj.DiscountPrice += OrderList[a].discountValue;
                //if (a == OrderList.length - 1) {
                //    obj.SaleNetPrice = (obj.SaleNetPrice + obj.ServiceCharge + obj.Remaining) - obj.DiscountPrice;

                //}

            }

            obj.SaleNetPrice = obj.AllPrice + obj.Remaining;
            obj.SaleAllPrice = (obj.AllPrice + obj.Remaining + obj.ServiceCharge) - obj.DiscountPrice;

            callback(obj);
        })
    }

    this.PrepareDailyItemReportByCategory = (args, callback) => {
        var reportObject = new report.dailyItemReportByCategory(args, "pos", this.DM);
        reportObject.getCallbackResult(function (obj) {
            callback(obj);
        })

    }

    this.LoadReport = (args, callback) => {
        switch (args.type) {
            case "dailyWatchServiceReport": case "daily_watch_service_report":
                this.PrepareWatchServiceReport(args, (obj) => {
                    callback(obj);
                });
                break;
            case "dailyAttendance": case "daily_attendance":
                this.PrepareDailyAttendanceReport(args, (obj) => {
                    callback(obj);
                });
                break;
            case "dailyCash":
                this.PrepareDailyCashReport(args, (obj) => {
                    callback(obj);
                });
                break;
            case "dailyFoodTimeslotReport": case "daily_food_timeslot_report":
                this.PrepareDailyFoodTimeslotReport(args, (obj) => {
                    callback(obj);
                })
                break;
            case "dailyCancelItem": case "daily_cancel_item":
                this.PrepareDailydailyCancelItemReport(args, (obj) => {
                    callback(obj);
                })
                break;
            case "dailyNetSales": case "daily_net_sales":
                this.PrepareDailySalesNet(args, (obj) => {
                    callback(obj);
                })
                break;
            case "dailyDiscount": case "daily_discount":
                this.PrepareDailyDiscount(args, (obj) => {
                    callback(obj);
                });
                break;
                //0: sortPrice 
            case "dailyMostSalesFood": case "daily_most_sales_food":
                this.PrepareDailyMostSalesFood(args, 0, (obj) => {
                    callback(obj);
                });
                break;
                //1: sortQty 
            case "dailyMostSalesQtyFood": case "daily_most_sales_qty_food":
                this.PrepareDailyMostSalesFood(args, 1, (obj) => {
                    callback(obj);
                });
                break;
            case "dailyPaymentDetail": case "daily_payment_detail":
                this.PrepareDailyPaymentDetail(args, (obj) => {
                    callback(obj);
                })
                break;
            case "dailyCategorySales": case "daily_category_sales":
                this.PrepareDailyCategorySales(args, (obj) => {
                    callback(obj);
                })
                break;

            case "dailyPerHourSales": case "daily_per_hour_sales":
                this.PrepareDailyPerHourSales(args, (obj) => {
                    callback(obj);
                })
                break;
            case "dailyItemReport": case "daily_item_report":
                this.PrepareDailyItemReportByCategory(args, (obj) => {
                    callback(obj);
                })
                break;
            case "dailyReportCloud": case "daily_report_cloud":
                this.PrepareDailyReportCloud(args, (obj) => {
                    callback(obj);
                });
                break;
            case "dailyReportEat": case "daily_report_eat":
                args.dailyReportType = 'eat';
                this.PrepareDailyReport(args, (obj) => {
                    callback(obj);
                });
                break;
            case "dailyReportTakeAway": case "daily_report_take_away":
                args.dailyReportType = 'takeaway';
                this.PrepareDailyReport(args, (obj) => {
                    callback(obj);
                });
                break;
            case "dailyReportDelivery": case "daily_Report_Delivery":
                console.log('delivery');
                args.dailyReportType = 'delivery';
                this.PrepareDailyReport(args, (obj) => {
                    callback(obj);
                });
                break;
            case "dailyReportVoidBill": case "daily_report_void_bill":
                this.PrepareDailyVoidBillReport(args, (obj) => {
                    callback(obj);
                })
                break;
            case "daily_Clearance_Report": case "dailyClearanceReport":
                this.PrepareDailyClearanceReport(args, (obj) => {
                    callback(obj);
                })
                break;
            case "daily_void_ECR_report": case "dailyVoidECRReport":
                this.PrepareDailyVoidECRReport(args, (obj) => {
                    callback(obj);
                })
                break;
            default: // add type to thie daily report
                args.dailyReportType = 'all';
                this.PrepareDailyReport(args, (obj) => {

                    if (args.ecrClearance) {
                        this.PrepareDailyVoidECRReport(args, (obj2) => {
                            callback(extend(obj, obj2));
                        });
                    } else {
                        callback(obj);
                    }

                });
                break;

        }
        // this.PrepareDailyReport(args, (obj) => {
        //     callback(obj);
        // });
    }

    this.EditStaff = (args, callback) => {
        //args = JSON.parse(args);
        //console.log("____editstaff");
        //console.log(args);
        var validUserName = true;
        var query = {};
        if (args.oldusername != args.username) { }
        query = { "sql": "select * from staff where username = @username", "args": [{ "c": "username", "t": TYPES.VarChar, "v": args.username }] }
        this.DM.executeStatement(query, (err, result) => {
            if (args.oldusername != args.username && result.length != 0) {
                callback({ result: "ERROR", msg: "duplicateUser" });
            } else if (args.oldusername == 'new') {
                //config.shopcode
                //console.log("shopcode");
                //console.log(config.shopCode);
                //"declare @seq int EXEC @seq = spGetNextSeq 'takeout' ; declare @sseq varchar(10) = REPLICATE('0',6-LEN(RTRIM(@seq))) + RTRIM(@seq)
                query = { "sql": "declare @seq int EXEC @seq = spGetNextSeqWithDate 'staff',null ; declare @sseq varchar(10) = REPLICATE('0',6-LEN(RTRIM(@seq))) + RTRIM(@seq); insert into staff (staffId, name, username, password, staffGroupCode) values (@staffId + '_' + @sseq, @name, @username, @password, @staffGroupCode)", "args": [{ "c": "username", "t": TYPES.VarChar, "v": args.username }, { "c": "password", "t": TYPES.VarChar, "v": args.password }, { "c": "staffGroupCode", "t": TYPES.VarChar, "v": args.staffGroupCode }, { "c": "name", "t": TYPES.NVarChar, "v": args.name }, { "c": "staffId", "t": TYPES.VarChar, "v": config.shopCode }] };
                this.DM.executeStatement(query, (err, result) => {
                    callback({ result: "OK" });
                })
            }
            else {
                query = { "sql": "update staff set username = @username, password = @password , staffGroupCode = @staffGroupCode,name = @name where username = @oldusername", "args": [{ "c": "username", "t": TYPES.VarChar, "v": args.username }, { "c": "password", "t": TYPES.VarChar, "v": args.password }, { "c": "staffGroupCode", "t": TYPES.VarChar, "v": args.staffGroupCode }, { "c": "name", "t": TYPES.NVarChar, "v": args.name }, { "c": "oldusername", "t": TYPES.VarChar, "v": args.oldusername }] };
                this.DM.executeStatement(query, (err, result) => {
                    callback({ result: "OK" });
                })
            }
        });
    }

    this.DeleteStaff = (args, callback) => {
        //args = JSON.parse(args);
        ////console.log(args);
        var query = {};
        query = { "sql": "delete staff where username = @username", "args": [{ "c": "username", "t": TYPES.VarChar, "v": args.username }] }
        this.DM.executeStatement(query, (err, result) => {
            callback({ result: "OK" });
        });
    }

    this.SaveTableSchema = (args, callback) => {
        var query = {};
        query = { "sql": "update shopZone set floorPlan = @floorPlan where shopZoneId =1 and shopId = 1", "args": [{ "c": "floorPlan", "t": TYPES.NVarChar, "v": args.layout }] }
        this.DM.executeStatement(query, (err, result) => {
            callback({ 'result': 'OK' });
        })
    }

    this.AmendBill = (args, callback) => {
        //console.log(args);
        for (var key in args) {
            if (args.hasOwnProperty(key)) {
                //console.log(key + " -> " + args[key]);
            }
        }
        var executeArr = [];

        console.log('isUseECR', args.isUseECR);
        var remarkList = [];


        var haveVoid = false;

        if (false) {
            //not use, use ecr in new layout with new flow widthout voidPayment in this method
            function voidPayment(cb) {
                var executeFunc = [];
                args.voidBill.forEach(function (v) {
                    var cardService = "";
                    haveVoid = true;
                    cardService = RAS.ECR.getVoidCommandByPaymentCode(v.method);


                    var query = { sql: 'select top 1 * from ecrTransaction where refNo = @refNo order by [DateTime] desc;', args: [{ "c": "refNo", "t": TYPES.VarChar, "v": args.refNo.$value }] };
                    RAS.DM.executeStatement(query, (err, result) => {
                        var _o = result;
                        if (_o.length == 0) cb(null); //('failInCard');
                        RAS.ECR.checkECR({ service: cardService, till: args.till.$value, traceNumber: _o[0].Trace_Number }, function (result) {
                            if (result.responseCode == 1) {
                                _o.invoice = args.refNo.$value;
                                _o.till = args.till.$value;
                                RAS.ECR.insertECRTransaction(_o, result, function () {
                                    cb(null);
                                });
                            } else {
                                cb(null); //('failInCard');
                                //cb(null)
                            }
                        });
                    });

                });

                if (args.voidBill.length == 0) cb(null);

            }
            executeArr.push(voidPayment);
        }

        /*
        args.payMethod.$value.forEach((payMethod, idx) => {

            if (payMethod.method === "octopus2") {

                executeArr.push((cb) => {

                    this.DeductOctopus(extend2({ till: args.till.$value, totalPrice: payMethod.amount }, { "invoice": args.refNo.$value }), (result) => {
                        if (result.octopusCode <= 100000) {
                            remarkList[idx] = result.cardBalance + "|" + parseFloat(result.totalPrice).toFixed(2) + "|" + result.cardId + "|" + result.machineId;
                            console.log('octopus2', JSON.stringify(result));
                            cb(null);
                        }
                        else {
                            callback(result);
                        }
                    });
                });
            }

            if (args.isUseECR && false) {
                //not use, use ecr in new layout with new flow widthout voidPayment in this method
                if (payMethod.tenderType == 'card') {
                    var cardService = "";
                    switch (payMethod.method) {

                        case "visa": case "master": default:
                            cardService = RAS.ECR.service.EDC_Sales;
                            break
                        case "unionpay":
                            cardService = RAS.ECR.service.CUP_Sales;
                            break;
                        case "eps":
                            cardService = RAS.ECR.service.EDC_EPS;
                            break;
                    }

                    executeArr.push((_o, cb) => {

                        var req = { totalPrice: payMethod.amount, service: cardService, till: args.till.$value };
                        extend2(req, _o);
                        console.log('test req', req);
                        RAS.ECR.checkECR(req, (result) => {
                            if (result.responseCode == 0) {
                                callback({ result: 'failInCard' });
                            } else if (result.responseCode == 1) {
                                RAS.ECR.insertECRTransaction(req, result, function (result2) {
                                    remarkList[idx] = result.detail;
                                    remarkList[idx] = result.detailInObject.Trace_Number + "##|##" + remarkList[idx];
                                    cb(null);
                                });
                            }
                        })
                    });
                }
            }


        });
        */


        executeArr.push((cb) => {
            var query = {};

            var sql = "declare @tillNum nvarchar(100); select top 1 @tillNum =  till  from tenderPayment where refNo = @refNo and void = 0; update tenderpayment set void = 1, voidstaff = @staffId,voidDate = getdate() where refNo = @refNo; update [transaction] set payAmount = @payAmount where refNo = @refNo;";
            var sqlagrs = [{ "c": "refNo", "t": TYPES.VarChar, "v": args.refNo.$value }, { "c": "staffId", "t": TYPES.VarChar, "v": args.user.$value }]
            var totalPayment = 0;
            var AzureObject = new Object;
            AzureObject.refNo = args.refNo.$value;
            AzureObject.staffId = args.user.$value;
            AzureObject.updateSql = "declare @tillNum nvarchar(100); select top 1 @tillNum =  till  from tenderPayment where refNo = @refNo and void = 0; update tenderpayment set void = 1, voidstaff = @staffId,voidDate = @paymentDate where refNo = @refNo; update [transaction] set payAmount = @payAmount where refNo = @refNo;"

            //if (args.isUseECR) {
            //    //new ecr flow widthout voidPayment in this method
            //    sql = "declare @tillNum nvarchar(100); select top 1 @tillNum =  till  from tenderPayment where refNo = @refNo and void = 0; update [transaction] set payAmount = @payAmount where refNo = @refNo;";
            //    AzureObject.updateSql = "update [transaction] set payAmount = @payAmount where refNo = @refNo;"
            //}

            //console.log(args.user);
            var PayMethod = new Array;
            var PayMethodItem = new Object;

            for (var i = 0; i < args.payMethod.$value.length; i++) {
                //if (remarkList[i] == "" || remarkList[i] == "null") remarkList[i] = null;
                //if (args.isUseECR) {
                //    if(!args.payMethod.$value[i].isNewAdd) continue;
                //}
                ////console.log("paymentValue" + parseFloat(args.payMethod.$value[i].amount));
                ////console.log("paymentValue" + args.payMethod.$value[i].method);
                var payseq = i + 1;
                sql += "insert into tenderpayment (paymentType,paymentValue,priceValue,staffId,refNo,paymentDate,void,seq, till, remark) values (@paymentType" + i.toString() + ",@paymentValue" + i.toString() + ", @priceValue" + i.toString() + ",@staffId,@refNo ,getDate(),0," + payseq + ", @tillNum, @remark" + i.toString() + ");";

                sqlagrs.push({ "c": "paymentType" + i.toString(), "t": TYPES.VarChar, "v": args.payMethod.$value[i].method })
                sqlagrs.push({ "c": "paymentValue" + i.toString(), "t": TYPES.Decimal, "v": parseFloat(args.payMethod.$value[i].amount), "o": { "scale": 2 } })
                sqlagrs.push({ "c": "remark" + i.toString(), "t": TYPES.NVarChar, "v": remarkList[i] ? remarkList[i] : null })
                var priceValue = args.payMethod.$value[i].totalAmount;
                if (priceValue) priceValue = parseFloat(priceValue);
                else priceValue = null;
                sqlagrs.push({ "c": "priceValue" + i.toString(), "t": TYPES.Decimal, "v": priceValue });
                totalPayment += parseFloat(args.payMethod.$value[i].amount);
                PayMethodItem = new Object;
                PayMethodItem.sql = "insert into tenderpayment (paymentType,paymentValue,priceValue,staffId,refNo,paymentDate,void,seq, till, remark) values (@paymentType" + i.toString() + ",@paymentValue" + i.toString() + ", @priceValue" + i.toString() + ",@staffId,@refNo ,@paymentDate,0," + payseq + ", @tillNum, @remark" + i.toString() + ");"

                PayMethodItem.paymentTypeName = "paymentType" + i.toString();
                PayMethodItem.paymentTypeValue = args.payMethod.$value[i].method;
                PayMethodItem.paymentValueName = "paymentValue" + i.toString();
                PayMethodItem.paymentValueValue = parseFloat(args.payMethod.$value[i].amount);
                PayMethodItem.priceValueName = "priceValue" + i.toString();
                PayMethodItem.priceValueValue = priceValue;
                PayMethodItem.remarkName = "remark" + i.toString();
                PayMethodItem.remarkValue = remarkList[i] ? remarkList[i] : null;
                PayMethod.push(PayMethodItem);
            }

            AzureObject.PayMethod = new Array;
            AzureObject.PayMethod = PayMethod;
            AzureObject.TotalPayment = totalPayment;
            AzureObject.PaymentDate = moment().format('YYYY-MM-DD HH:mm:ss');

            sqlagrs.push({ "c": "payAmount", "t": TYPES.Decimal, "v": totalPayment, "o": { "scale": 2 } })
            query.sql = sql;
            query.args = new Array;
            query.args = sqlagrs;
            RAS.DM.executeStatement(query, (err, result) => {
                //AzureSocket.emit('AmendBill', JSON.stringify(AzureObject));
                fs.writeFile("./cloud/amendbill/" + moment().format("YYMMDDHHmmss") + ".json", JSON.stringify(AzureObject), function (err) {
                    if (err) {
                        //console.log(err);
                    } else {
                        ////console.log("The file was saved!");
                    }
                });
                //callback({ 'result': 'OK' });
                cb(null);
            })
        });

        async.waterfall(executeArr, function (result) {
            callback({ result: "OK" });
        });




    }

    this.LinkTable = (args, callback) => {
        console.log('LinkTable start');
        console.log(args);
        var tableCodeObj = getTableCode(args.tableNum.$value);
        var query = [{
            // "sql": "select startTime from tableorder where tablecode = @tableCode",
            "sql": "select * from tableorder where tablecode = @tableCode",
            "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": tableCodeObj.tableCode }]
        }, {
            "sql": "select * from tableorderd where tablecode = @tableCode",
            "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": tableCodeObj.tableCode }]
        },
        {
            "sql": "select * from tablediscount where tablecode = @tableCode ",
            "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": args.tableNum.$value }]
        },
        {
            "sql": "select count(*) as isExist from diningTable where name = @tableCode",
            "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": tableCodeObj.tableCode }]
        }
        ]
        this.DM.executeStatement(query, (err, tableOrders) => {
            var rCode = 'ERROR';
            var msg = "The table does not found";
            var ot = '';
            var orders = [];
            var order;

            //console.log(tableOrders);
            if (args.tableNum.$value == "*") {
                var takeawayTableNum = args.tableNum.$value;
                rCode = 'OK';
                msg = "";
                order = { 'version': '1.0', 'header': { 'orderId': '', 'tableNum': '', 'openTime': '', 'peopleNum': 0, 'member': '', 'memberName': '', 'memberMsg': '', 'lastOrder': '', 'lastOrderWarning': '', 'leaveTime': '' }, 'item': [], 'itemCount': 0, 'discount': [] };
                order.header.tableNum = takeawayTableNum;
                order.header.openTime = '00:00:00';
                order.header.peopleNum = "1";
                order.header.orderId = "_";
                order.header.lastOrder = '';
                order.header.lastOrderWarning = '';
                order.header.leaveTime = '';
                orders.push(order);
            }
            else {
                if (tableOrders[0].length > 0) {
                    rCode = 'OK'
                    ot = moment(tableOrders[0][0].startTime).format("HH:mm:ss");
                    msg = "";
                    for (var j = 0; j < tableOrders[0].length ; j++) {
                        var tableOrder = tableOrders[0][j];
                        //console.log(tableOrder);
                        order = { 'version': '1.0', 'header': { 'orderId': '', 'tableNum': '', 'openTime': ot, 'peopleNum': 0, 'member': '', 'memberName': '', 'memberMsg': '', 'lastOrder': '', 'lastOrderWarning': '', 'leaveTime': '' }, 'item': [], 'itemCount': 0, 'discount': [] };
                        order.header.tableNum = tableOrder.tableCode + tableOrder.tableSubCode;
                        order.header.peopleNum = tableOrder.pax;
                        order.header.orderId = "_";
                        order.header.lastOrder = tableOrder.lastOrder == null ? '' : moment(tableOrder.lastOrder).format("HH:mm:ss");
                        order.header.lastOrderWarning = tableOrder.lastOrderWarning == null ? '' : moment(tableOrder.lastOrderWarning).format("HH:mm:ss");
                        order.header.leaveTime = tableOrder.leaveTime == null ? '' : moment(tableOrder.leaveTime).format("HH:mm:ss");

                        if (!(order.header.lastOrder != "" && order.header.lastOrderWarning != "" && order.header.leaveTime != "")) {
                            order.header.lastOrder = "";
                            order.header.lastOrderWarning = "";
                            order.header.leaveTime = "";
                        }

                        //console.log(order.header);
                        var contentObj = [];
                        sortDbItem(tableOrders[1]);
                        for (var k = 0; k < tableOrders[1].length ; k++) {
                            var currentItem = tableOrders[1][k];
                            if (tableOrder.tableSubCode == currentItem.tableSubCode) {
                                var name1, name2, name3;
                                var items = getItemByItem(currentItem);
                                name1 = name2 = name3 = "";

                                if (items.length > 0) {
                                    var name1 = items[0].name1;
                                    var name2 = items[0].name2;
                                    var name3 = items[0].name3;
                                    if (items[0].name != undefined) {
                                        if (items[0].name[config.lang1] != undefined)
                                            name1 = items[0].name[config.lang1];
                                        if (items[0].name[config.lang2] != undefined)
                                            name2 = items[0].name[config.lang2];
                                        if (items[0].name[config.lang3] != undefined)
                                            name3 = items[0].name[config.lang3];
                                    }
                                }
                                if (currentItem.customName != "") {
                                    name1 = name2 = name3 = currentItem.customName;
                                }

                                var t = moment(currentItem.time).format("HH:mm:ss");
                                contentObj.push({ code: currentItem.itemId, type: currentItem.type, desc1: name1, desc2: name2, desc3: name3, qty: currentItem.qty, unitPrice: currentItem.price, time: t, voidIndex: currentItem.voidIndex, voidRemark: currentItem.voidRemark, staff: currentItem.staffId, index: currentItem.seq, serviceCharge: currentItem.serviceCharge });
                            }
                        }

                        for (var i = 0; i < tableOrders[2].length; i++) {
                            var currentItem = tableOrders[2][i];
                            order.discount.push({ "discountCode": currentItem.discountCode, "discountType": currentItem.discountType, "discountValue": currentItem.discountValue, "discountAmount": currentItem.discountAmount });
                        }


                        order.item = contentObj;
                        order.itemCount = contentObj.length;
                        orders.push(order);
                    }

                } else {
                    if (args.canOpenTable.$value == "1") {
                        //console.log("TEST", tableOrders);

                        if (tableOrders[3][0]) {
                            if (tableOrders[3][0].isExist) {
                                rCode = 'OK'
                                msg = "";
                                order = { 'version': '1.0', 'header': { 'orderId': '', 'tableNum': '', 'openTime': "00:00:00", 'peopleNum': 0, 'member': '', 'memberName': '', 'memberMsg': '', 'lastOrder': '', 'lastOrderWarning': '', 'leaveTime': '' }, 'item': [], 'itemCount': 0, 'discount': [] };
                                order.header.tableNum = args.tableNum.$value;
                                order.header.peopleNum = 0;
                                order.header.orderId = "_";
                                order.header.lastOrder = null;
                                order.header.lastOrderWarning = null;
                                order.header.leaveTime = null;
                                order.item = [];
                                order.itemCount = 0;
                                orders.push(order);
                            }
                        }
                    } else {
                        rCode = 'ERROR';
                        msg = "The table not open yet";
                    }
                }
            }
            //console.log(orders);
            //console.log(new Date());
            var now = new Date();
            var utc_timestamp = Date.UTC(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(),
                  now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds(), now.getUTCMilliseconds());
            //.toUTCString();
            //var r = { result: "OK", timestamp: new Date().getTime(), taskID: new Date().getTime(), note: msg, order: orders, suspendItems: [] }
            this.GetOutOfStock((err, result) => {
                var si = [];
                var siq = [];
                result.forEach((item) => {
                    si.push(item.code);
                    siq.push(item.qty);
                });
                console.log("si", si);
                var r = { result: rCode, timestamp: utc_timestamp, taskID: utc_timestamp, note: msg, order: orders, suspendItems: si.toString(), suspendItemsQty: siq.toString() };
                callback(r);
            });

            // //console.log(tableOrders);
            // callback(r);
        })
        //<result>OK</result><timestamp>" + new Date().getTime() + "</timestamp><taskID>1</taskID><note></note><order><version>1.0</version><header><tableNum>" + args.tableNum.$value + "</tableNum><member></member><memberName></memberName><memberMsg></memberMsg></header>" + "</order>
    }

    this.LoadTableForTransfer = (args, callback) => {
        var query = [{ "sql": "select name, status from diningTable order by name desc", "args": null }, { "sql": "select * from tableorder order by tablecode,tablesubcode", "args": null }];
        this.DM.executeStatement(query, (err, result) => {
            //console.log(result[0]);
            result[0] = result[0].sort(function (a, b) {
                if (isNaN(a.name) && isNaN(b.name)) {
                    return a.name - b.name;
                } else
                    if (isNaN(a.name)) {
                        return 1;
                    } else if (isNaN(b.name)) {
                        return -1;
                    }
                return parseInt(a.name) - parseInt(b.name);
            });
            //console.log(result[0]);
            console.log(result[1]);
            var matchedTable = [],
                tmpName = '';
            result[0].forEach(function (table1) {
                var found = false;
                result[1].forEach(function (table2Ele) {
                    if (table2Ele.tableSubCode != '' && table1.name == table2Ele.tableCode) {
                        tmpName = table1.name + '_' + table2Ele.tableSubCode;
                        matchedTable.push({ name: tmpName, status: table2Ele.status });
                        // console.log('yes');
                        found = true;
                    }
                })
                if (!found) {
                    matchedTable.push(table1);
                }
            })
            // var _o = result[0];
            var _o = matchedTable;
            callback(_o);
        }, 'parallel')
    }

    this.LoadStaffGroup = (args, callback) => {
        //console.log('LoadStaffGroup start');
        var query = { "sql": "select * from staffgroup", "args": null };
        this.DM.executeStatement(query, (err, result) => {
            //console.log('LoadStaffGroup callback');
            callback(result);
        });
    }

    this.EditStaffGroup = (args, callback) => {

        //console.log(JSON.stringify(args));
        var query = { "sql": "select * from staffgroup", "args": null };
        this.DM.executeStatement(query, (err, result) => {
            this.LoadStaffGroup(args, callback);
        });

        var validUserGroup = true;
        var query = {};
        if (args.oldGroupCode != undefined) {
            query = { "sql": "select * from staffGroup where staffGroupCode = @staffGroupCode", "args": [{ "c": "staffGroupCode", "t": TYPES.VarChar, "v": args.staffGroupCode }] }
        }

        this.DM.executeStatement(query, (err, result) => {
            if (args.oldGroupCode != undefined && result.length != 0) {
                callback({ result: "ERROR", msg: "duplicateUserGroup" });
            } else {
                query = {
                    "sql": "insert into staffGroup (staffGroupCode, staffGroupName, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, p18, p19, p20) values (@staffGroupCode,@staffGroupName,@p1,@p2,@p3,@p4,@p5,@p6,@p7,@p8,@p9,@p10,@p11,@p12,@p13,@p14,@p15,@p16,@p17,@p18,@p19,@p20)",
                    "args": [{ "c": "staffGroupCode", "t": TYPES.VarChar, "v": args.staffGroupCode }, { "c": "staffGroupName", "t": TYPES.VarChar, "v": args.staffGroupName },
                        { "c": "p1", "t": TYPES.Bit, "v": args.p1 }, { "c": "p2", "t": TYPES.Bit, "v": args.p2 }, { "c": "p3", "t": TYPES.Bit, "v": args.p3 }, { "c": "p4", "t": TYPES.Bit, "v": args.p4 }, { "c": "p5", "t": TYPES.Bit, "v": args.p5 }, { "c": "p6", "t": TYPES.Bit, "v": args.p6 }, { "c": "p7", "t": TYPES.Bit, "v": args.p7 }, { "c": "p8", "t": TYPES.Bit, "v": args.p8 }, { "c": "p9", "t": TYPES.Bit, "v": args.p9 }, { "c": "p10", "t": TYPES.Bit, "v": args.p10 }, { "c": "p11", "t": TYPES.Bit, "v": args.p11 }, { "c": "p12", "t": TYPES.Bit, "v": args.p12 }, { "c": "p13", "t": TYPES.Bit, "v": args.p13 }, { "c": "p14", "t": TYPES.Bit, "v": args.p14 }, { "c": "p15", "t": TYPES.Bit, "v": args.p15 }, { "c": "p16", "t": TYPES.Bit, "v": args.p16 }, { "c": "p17", "t": TYPES.Bit, "v": args.p17 }, { "c": "p18", "t": TYPES.Bit, "v": args.p18 }, { "c": "p19", "t": TYPES.Bit, "v": args.p19 }, { "c": "p20", "t": TYPES.Bit, "v": args.p20 }]
                };

                if (args.oldGroupCode == undefined) {

                    query.sql = "update staffGroup set staffGroupName = @staffGroupName, p1 = @p1, p2 = @p2, p3 = @p3, p4 = @p4, p5 = @p5, p6 = @p6, p7 = @p7, p8 = @p8, p9 = @p9, p10 = @p10, p11 = @p11, p12 = @p12, p13 = @p13, p14 = @p14, p15 = @p15, p16 = @p16, p17 = @p17, p18 = @p18, p19 = @p19, p20 = @p20 where staffGroupCode = @staffGroupCode";
                }

                this.DM.executeStatement(query, (err, result) => {
                    callback({ result: "OK" });
                })
            }
        });
    }

    function updateTableOrder(dT, tableOrderD, pax, staffCode, resolve, reject) {
        //console.log('start updateTableOrder'); 
        var deferred = Q.defer();
        try {
            var query = [];
            var totalPrice = 0;
            var totalPriceForServiceCharge = 0;
            var serviceCharge = 0;
            var itemIsServiceCharge = true;
            var itemQty = 0;
            var lastOrder = '';
            var lastOrderWarning = '';
            var leaveTime = '';
            var lastItemQty = 0;

            sortDbItem(tableOrderD);

            if (tableOrderD.length != 0) {
                for (var i = 0; i < tableOrderD.length; i++) {
                    var orderedItem = tableOrderD[i];
                    if (orderedItem.type == "I" || orderedItem.type == "T") {
                        itemIsServiceCharge = orderedItem.serviceCharge;
                        itemQty = orderedItem.qty;
                        totalPrice += itemQty * orderedItem.price;
                        if (itemIsServiceCharge) {
                            totalPriceForServiceCharge += itemQty * orderedItem.price;
                        }

                        if (orderedItem.type == "T" && orderedItem.voidIndex == -1) {
                            var checkIsVoidItem = tableOrderD.filter((checkItem) => {
                                return checkItem.voidIndex == orderedItem.seq;
                            })
                            if (checkIsVoidItem.length == 0) {
                                var items = RAS.Item.filter(function (entry) {
                                    return entry.code == orderedItem.itemId;
                                });
                                var _lastOrder = items[0].namek.split(',');
                                if (_lastOrder.length > 1) {
                                    lastOrderWarning = _lastOrder[0];
                                    lastOrder = _lastOrder[1];
                                    leaveTime = _lastOrder[2];
                                }
                            }
                        }
                    } else {
                        totalPrice += (itemQty * orderedItem.qty) * orderedItem.price;
                        if (itemIsServiceCharge) {
                            totalPriceForServiceCharge += (itemQty * orderedItem.qty) * orderedItem.price;
                        }
                    }
                }
            }
            var serviceCharge = (totalPriceForServiceCharge / 10).toFixed(1);

            query.push({
                "sql": "MERGE INTO tableOrder d USING \
                    (SELECT @tableCode as tableCode ,@tableSubCode as tableSubCode,@pax as pax,@memberId as memberId,@startTime as startTime,@staffId as staffId, @price as price, @serviceCharge as serviceCharge, @lastOrder as lastOrder, @lastOrderWarning as lastOrderWarning, @leaveTime as leaveTime ) s ON (d.tableCode = s.tableCode and d.tableSubCode = s.tableSubCode ) \
                    WHEN MATCHED THEN UPDATE SET d.price = s.price,d.serviceCharge = s.serviceCharge,d.leaveTime = s.leaveTime,d.lastOrder = s.lastOrder,d.lastOrderWarning = s.lastOrderWarning,status = 2 WHEN NOT MATCHED THEN \
                    INSERT (tableCode,tableSubCode,pax,memberId,startTime,staffId, price, serviceCharge, lastOrder, lastOrderWarning, leaveTime, status) VALUES (s.tableCode,s.tableSubCode,s.pax,s.memberId,s.startTime,s.staffId, s.price, s.serviceCharge, s.lastOrder, s.lastOrderWarning, s.leaveTime, 2);"
            , "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": dT.tableCode }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": dT.tableSubCode }, { "c": "pax", "t": TYPES.Int, "v": pax }, { "c": "memberId", "t": TYPES.Int, "v": null }, { "c": "startTime", "t": TYPES.DateTime, "v": new Date() }, { "c": "staffId", "t": TYPES.VarChar, "v": staffCode },
                { "c": "price", "t": TYPES.Decimal, "v": parseFloat(totalPrice), "o": { "scale": 2 } },
                { "c": "serviceCharge", "t": TYPES.Decimal, "v": parseFloat(serviceCharge), "o": { "scale": 2 } },
                { "c": "lastOrder", "t": TYPES.DateTime, "v": null },
                { "c": "lastOrderWarning", "t": TYPES.DateTime, "v": null },
                { "c": "leaveTime", "t": TYPES.DateTime, "v": null }]
            });
            RAS.DM.executeStatement(query, (err, result) => {
                deferred.resolve(result);
            })
        } catch (e) {
            deferred.reject(result);
            //console.log(e);
        }
    }

    this.TransferItem = (args, callback) => {
        //console.log('transfer item start')
        console.log(args);
        console.log(JSON.stringify(args.transferList.item));
        //console.log('transfer item end')
        var sT = getTableCode(args.sourceTableNo);
        var dT = getTableCode(args.destinationTableNo);

        var query = [{ "sql": "select max(round) as round, max(seq) as seq, max(pax) as pax,max(memberId) as memberId from tableorderd d right outer join tableOrder o on o.tableCode = d.tableCode and o.tableSubCode = d.tableSubCode where o.tableCode = @tableCode and o.tableSubCode = @tableSubCode;", "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": dT.tableCode }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": dT.tableSubCode }] },
        { "sql": "select * from tableorderd where tableCode = @tableCode and tableSubCode = @tableSubCode", "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": dT.tableCode }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": dT.tableSubCode }] },
        { "sql": "select * from tableorderd where tableCode = @tableCode and tableSubCode = @tableSubCode;", "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": sT.tableCode }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": sT.tableSubCode }] }];

        this.DM.executeStatement(query, (err, tableOrderD) => {
            ////console.log(tableOrderD);
            var currentSeq = 0;
            var currentRound = 0;
            if (tableOrderD[0][0].seq != null) {
                currentSeq = tableOrderD[0][0].seq;
            }
            if (tableOrderD[0][0].round != null) {
                currentRound = tableOrderD[0][0].round;
            }
            currentRound++;
            //if (tableOrderD[0][0].pax == null) {

            query = [];
            query.push({ "sql": "update diningTable set status = 2 where name = @tableCode", "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": dT.tableCode }] });

            var transferItems = args.transferList.item;

            for (var i = 0; i < transferItems.length; i++) {
                if (transferItems[i].index[0] != null) {
                    var seq = i + 1;
                    if (!(currentSeq == 0 && currentRound == 1)) {
                        currentSeq++;
                        seq = currentSeq;
                    }
                    query.push({
                        "sql": "update tableorderd set tableCode = @tableCode, tableSubCode = @tableSubCode, seq = @seq, round = 1 where tableCode = @stableCode and tableSubCode = @stableSubCode and seq = @sseq; \
                                update tableorderd set voidIndex = @seq where tableCode = @stableCode and tableSubCode = @stableSubCode and voidIndex = @sseq; \
                                update kds set tableCode = @tableCode, tableSubCode = @tableSubCode, seq = @seq, round = 1 where tableCode = @stableCode and tableSubCode = @stableSubCode and seq = @sseq; \
                                update kds set voidIndex = @seq where tableCode = @stableCode and tableSubCode = @stableSubCode and voidIndex = @sseq; ",
                        "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": dT.tableCode }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": dT.tableSubCode }, { "c": "stableCode", "t": TYPES.VarChar, "v": sT.tableCode }, { "c": "stableSubCode", "t": TYPES.VarChar, "v": sT.tableSubCode }, { "c": "seq", "t": TYPES.Int, "v": seq }, { "c": "sseq", "t": TYPES.Int, "v": transferItems[i].index[0] }]
                    });
                }
                //console.log(transferItems[i]);
            }
            query.push({ "sql": "select * from tableorderd where tableCode = @tableCode and tableSubCode = @tableSubCode", "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": dT.tableCode }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": dT.tableSubCode }] });
            query.push({ "sql": "select * from tableorderd where tableCode = @tableCode and tableSubCode = @tableSubCode", "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": sT.tableCode }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": sT.tableSubCode }] });
            this.DM.executeStatement(query, (err, tableOrderD) => {

                //updateTableOrder(dt, tableOrderD[tableOrderD.length - 2], 1, args.staffCode);
                //updateTableOrder(st, tableOrderD[tableOrderD.length - 1], null, null);
                //console.log('Promise start');

                var kdsList = [];


                if (tableOrderD.length != 0) {
                    for (var i = 0; i < tableOrderD[tableOrderD.length - 2].length; i++) {
                        if (tableOrderD[tableOrderD.length - 2][i].kds != undefined) {
                            kdsList.push(tableOrderD[tableOrderD.length - 2][i].kds);
                        }
                    }
                    for (var i = 0; i < tableOrderD[tableOrderD.length - 1].length; i++) {
                        if (tableOrderD[tableOrderD.length - 1][i].kds != undefined) {
                            kdsList.push(tableOrderD[tableOrderD.length - 1][i].kds);
                        }
                    }
                }
                console.log(JSON.stringify(kdsList));
                kdsList.filter(onlyUnique);
                console.log(JSON.stringify(kdsList));
                for (var j = 0; j < kdsList.length; j++) {
                    this.showKds(kdsList[j]);
                }

                Q.all(
                    [updateTableOrder(dT, tableOrderD[tableOrderD.length - 2], 1, args.staffCode),
                        updateTableOrder(sT, tableOrderD[tableOrderD.length - 1], null, args.staffCode)]
                    ).then((res) => {
                        //console.log('Promise finish');

                        var argsAdminLoadTableOrder = { tableNum: { $value: args.sourceTableNo } };
                        this.AdminLoadTableOrder(argsAdminLoadTableOrder, callback);
                        this.LoadTableSchema(null, (err, result) => {
                            io.sockets.emit('refreshTable', JSON.stringify(result));
                        })
                    })
            })


            /*} else {
                    //query = [{ "sql": "update tableOrderd set seq = seq + " + currentSeq + ", voidIndex = case when voidIndex is null then null else voidIndex + " + currentSeq + " end, tableCode =  @dTableCode, tableSubCode = @dTableSubCode where  tableCode = @tableCode and tableSubCode = @tableSubCode; update diningTable set status = 0 where name = @tableCode ", "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": sTableObj.tableCode }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": sTableObj.tableSubCode }, { "c": "dTableCode", "t": TYPES.VarChar, "v": dTableObj.tableCode }, { "c": "dTableSubCode", "t": TYPES.VarChar, "v": dTableObj.tableSubCode }, { "c": "seq", "t": TYPES.Int, "v": currentSeq }] }];
    
                    //query = [{ "sql": "select * from tableorderd where tableCode = @tableCode and tableSubCode = @tableSubCode;", "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": dTableObj.tableCode }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": dTableObj.tableSubCode }] }, { "sql": "select * from tableorderd where tableCode = @tableCode and tableSubCode = @tableSubCode;", "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": sTableObj.tableCode }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": sTableObj.tableSubCode }] }];
                    //this.DM.executeStatement(query, (err, tableOrderD) => {
                    sortDbItem(tableOrderD[2]);
    
                    if (tableOrderD[2].length != 0) {
                        for (var i = 0; i < tableOrderD[2].length; i++) {
                            var orderedItem = tableOrderD[2][i];
                            totalPrice += orderedItem.qty * orderedItem.price;
                            if (orderedItem.type == "I") {
                                lastIsServiceCharge = orderedItem.serviceCharge;
                            }
                            if (lastIsServiceCharge) {
                                totalPriceForServiceCharge += orderedItem.qty * orderedItem.price;
                            }
                        }
                    }
    
                    var serviceCharge = (totalPriceForServiceCharge / 10).toFixed(1);
    
                    query = [{ "sql": "update tableOrderd set seq = seq + " + currentSeq + ", voidIndex = case when voidIndex is null then null else voidIndex + " + currentSeq + " end, tableCode =  @dTableCode, tableSubCode = @dTableSubCode where  tableCode = @tableCode and tableSubCode = @tableSubCode; update diningTable set status = 0 where name = @tableCode; update tableorder set price = @price, serviceCharge = @serviceCharge where tableCode = @dtableCode and tableSubCode = @dtableSubCode; delete tableorder where tableCode =  @tableCode and tableSubCode = @tableSubCode ", "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": sTableObj.tableCode }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": sTableObj.tableSubCode }, { "c": "dTableCode", "t": TYPES.VarChar, "v": dTableObj.tableCode }, { "c": "dTableSubCode", "t": TYPES.VarChar, "v": dTableObj.tableSubCode }, { "c": "seq", "t": TYPES.Int, "v": currentSeq }, { "c": "price", "t": TYPES.Decimal, "v": totalPrice, "o": { "scale": 2 } }, { "c": "serviceCharge", "t": TYPES.Decimal, "v": serviceCharge, "o": { "scale": 2 } }] }];
                    this.DM.executeStatement(query, (err, result) => {
                        this.LoadTableSchema(null, (err, result) => {
                            io.sockets.emit('refreshTable', JSON.stringify(result));
                        })
                    });
    
    
                    //})
    
                }*/
        }, 'parallel');
        //callback({ 'result': 'OK' })
    }

    this.addDiceMessage = (args) => {
        args = JSON.parse(args);
        //console.log("addMessage", args);
        if (args.action === "checkout" || args.action === "callService" || args.action === "tea") {
            var today = new Date(new Date().getTime() - (2 * 60 * 1000)), tommorow = getTomorrow();
            var query = {
                "sql": "select * from message where isDelete = @isDelete and tableno = @tableno and action = @action and date between @today and @tommorow",
                "args": [
                    { "c": "isDelete", "t": TYPES.Bit, "v": false },
                    { "c": "tableno", "t": TYPES.VarChar, "v": args.tableno },
                    { "c": "action", "t": TYPES.VarChar, "v": args.action },
                    { "c": "today", "t": TYPES.DateTime, "v": today },
                    { "c": "tommorow", "t": TYPES.DateTime, "v": tommorow }
                ]
            };
            this.DM.executeStatement(query, (err, _msg) => {
                if (err) return;
                //console.log("addMessage Check", _msg);
                if (_msg.length > 0) {
                    //console.log("addMessage", "prevent multiple request");
                }
                else {
                    this.saveMessage(args);
                }
            });
            //Message.find({
            //    "date": {
            //        "$gte": today,
            //        "$lt": tommorow
            //    },
            //    isDelete: false,
            //    tableno: args.tableno,
            //    action: args.action
            //}).sort({ date: -1 })
            //    .exec(function (err, _msg) {
            //        if (err) return;
            //        ////console.log("addMessage Check", _msg);
            //        if (_msg.length > 0) {
            //            //console.log("addMessage", "prevent multiple request");
            //        }
            //        else {
            //            saveMessage(args);
            //        }
            //    });
        }
        else {
            this.saveMessage(args);
        }
    }

    this.saveMessage = (args) => {
        console.log("Add Dice Message", args.msg);
        var query = {
            "sql": "insert into message (action, tableno, itemCode, dish, token, message, date) VALUES (@action, @tableno, @itemCode, @dish, @token, @msg, @date)",
            "args": [
                { "c": "action", "t": TYPES.VarChar, "v": args.action },
                { "c": "tableno", "t": TYPES.VarChar, "v": args.tableno },
                { "c": "itemCode", "t": TYPES.VarChar, "v": args.itemCode },
                { "c": "dish", "t": TYPES.VarChar, "v": args.dish },
                { "c": "token", "t": TYPES.VarChar, "v": args.token },
                { "c": "msg", "t": TYPES.NVarChar, "v": args.msg },
                { "c": "date", "t": TYPES.DateTime, "v": new Date() }
            ]
        };
        this.DM.executeStatement(query, (err, data) => {
            if (err) return;
            //console.log("addMessage Check", data);
            this.broadcastDiceMessage();
        });
        // var msg = new Message(args);
        // msg.save(function (error, data) {
        //     if (error) {
        //         //console.log("addMessage error", error);
        //     }
        //     else {
        //         ////console.log("addMessage", data);
        //         var _o = { msg: data };
        //         //mqttClient.publish("wo/dice/addMessageResponse/", JSON.stringify(_o), { qos: 2 });
        //         broadcastDiceMessage();
        //     }
        // });
    }

    this.updateDiceMessage = (args) => {
        args = JSON.parse(args);
        // var msg = new Message(JSON.parse(args));
        if (args.staff == undefined) {
            //console.log("updateMessage Error", "[staff] undefined");
            return;
        }
        if (args.isDelete == undefined) {
            //console.log("updateMessage Error", "[isDelete] undefined");
            return;
        }
        var query = {
            "sql": "select * from message where Id = @id; \
                    select top 1 * from watch, staffWatch where androidId = @androidId and watch.watchId = staffWatch.watchTagId order by staffWatch.update_time desc",
            "args": [
                { "c": "id", "t": TYPES.Int, "v": args.id },
                { "c": "androidId", "t": TYPES.VarChar, "v": args.staff }
            ]
        };
        this.DM.executeStatement(query, (err, data) => {
            if (err) {
                //console.log("updateMessage error", err);
            }
            _msg = data[0];
            var staffId = data[1].staffId;
            // console.log('_msg', _msg);
            var that = this;
            var update = function (callback) {
                // if (!_msg.staffId.length) _msg.staffId = msg.staff;
                // _msg.isDelete = msg.isDelete;
                // _msg.markModified('staff isDelete');
                // _msg.save(function () {
                //     if (callback != null) callback(null, 1);
                // });
                // //console.log("update msg", _msg);

                console.log('updateDiceMessage data: ', _msg);
                if (!_msg.staffId.length) {
                    _msg.staffId = args.staff;
                    var query = {
                        "sql": "update message set staffId = @staff, watchId = @watch, isDelete = @isDelete, %date% = getdate() where id = @id",
                        "args": [
                            { "c": "staff", "t": TYPES.VarChar, "v": staffId },
                            { "c": "watch", "t": TYPES.VarChar, "v": args.staff },
                            { "c": "isDelete", "t": TYPES.Bit, "v": args.isDelete },
                            { "c": "id", "t": TYPES.Int, "v": args.id }
                        ]
                    };
                }
                else {
                    var query = {
                        "sql": "update message set isDelete = @isDelete, %date% = getdate() where id = @id",
                        "args": [
                            { "c": "isDelete", "t": TYPES.Bit, "v": args.isDelete },
                            { "c": "id", "t": TYPES.Int, "v": args.id }
                        ]
                    };
                }
                if (args.isDelete) {
                    query.sql = query.sql.replace("%date%", "deleteDate");
                }
                else {
                    query.sql = query.sql.replace("%date%", "updateDate");
                }
                console.log("4", query.sql);
                that.DM.executeStatement(query, (err, data) => {
                    //console.log('data', data);
                    if (callback != null) callback(null, 1);
                });
            }
            var broadcast = function (callback) {
                that.broadcastDiceMessage(callback);
            }
            var updateResponse = function (callback) {
                var _o = { 'msg': _msg };
                mqttClient.publish("wo/dice/updateMessageResponse/", JSON.stringify(_o), { qos: 2 });
                // io.sockets.emit('updateMessageResponse', _o);
                callback(null, 3);
            }
            //console.log('_msg', _msg.staffId);
            if (!_msg.staffId.length)
                async.series([update, broadcast, updateResponse]);
            else
                update();
        });
        // Message.findOne({ "id": msg.id }, function (err, _msg) {
        //     if (err) {
        //         //console.log("updateMessage error", err);
        //     }
        //     var update = function (callback) {
        //         if (!_msg.staffId.length) _msg.staffId = msg.staff;
        //         _msg.isDelete = msg.isDelete;
        //         _msg.markModified('staff isDelete');
        //         _msg.save(function () {
        //             if (callback != null) callback(null, 1);
        //         });
        //         //console.log("update msg", _msg);
        //     }
        //     var broadcast = function (callback) {
        //         broadcastDiceMessage(callback);
        //     }
        //     var updateResponse = function (callback) {
        //         var _o = { 'msg': _msg };
        //         mqttClient.publish("wo/dice/updateMessageResponse/", JSON.stringify(_o), { qos: 2 });
        //         io.sockets.emit('updateMessageResponse', _o);
        //         callback(null, 3);
        //     }
        //     if (!_msg.staffId.length)
        //         async.series([update, broadcast, updateResponse]);
        //     else
        //         update();
        // });
    }

    this.broadcastDiceMessage = function (callback) {
        var today = getToday(), tomorrow = getTomorrow();
        var query = {
            "sql": "select * from message where (isDelete = @isDelete or isDelete is null) and date between @today and @tomorrow order by date desc",
            "args": [
                { "c": "isDelete", "t": TYPES.Bit, "v": false },
                { "c": "today", "t": TYPES.DateTime, "v": today },
                { "c": "tomorrow", "t": TYPES.DateTime, "v": tomorrow }
            ]
        };
        this.DM.executeStatement(query, (err, _msg) => {
            console.log("Broadcast Dice Message", _msg);
            if (err) {
                //console.log("broadcastDiceMessage error", err);
            }
            //console.log('broadcastDiceMessage', _msg);
            var _o = { 'msg': _msg };
            mqttClient.publish("wo/dice/broadcastMessageResponse/", JSON.stringify(_o), { qos: 2 });
            // io.sockets.emit('broadcastMessageResponse', _o);
            if (callback != null) callback();
        });
        // Message.find({ "date": { "$gte": today, "$lt": tomorrow }, isDelete: false }).sort({date: 1})
        //     .exec(function (err, _msg) {
        //         if (err) return;
        //         ////console.log("broadcastDiceMessageResponse", _msg);
        //         var _o = { 'msg': _msg };
        //         mqttClient.publish("wo/dice/broadcastMessageResponse/", JSON.stringify(_o), { qos: 2 });
        //         io.sockets.emit('broadcastMessageResponse', _o);
        //         if (callback != null) callback();
        //     });
    }

    this.clockIn = (args, callback) => {
        args.type = "in";
        this.Attendance(args, callback);
    }

    this.clockOut = (args, callback) => {
        args.type = "out";
        this.Attendance(args, callback);
    }

    this.Attendance = (args, callback) => {
        //console.log('Attendance', args);
        var query = { "sql": "select * from staff where username = @username", "args": [{ "c": "username", "t": TYPES.VarChar, "v": args.username }] }
        this.DM.executeStatement(query, (err, result) => {
            if (result.length == 0) {
                callback({ result: "ERROR", msg: "noUser" });
                return;
            }
            var currentTime = args.date ? new Date(parseInt(args.date)) : new Date(),
                query = {
                    "sql": "insert into staffAtt (staffUsername, staffName, shopId, datetime, type, image) VALUES (@staffUsername, (select name from staff where username = @staffUsername), @shopId, @datetime, @type, @image); select * from staffAtt where attId = @@IDENTITY",
                    "args": [
                        { "c": "staffUsername", "t": TYPES.VarChar, "v": args.username },
                        { "c": "shopId", "t": TYPES.Int, "v": 1 },
                        { "c": "datetime", "t": TYPES.DateTime, "v": currentTime },
                        { "c": "type", "t": TYPES.VarChar, "v": args.type },
                        { "c": "image", "t": TYPES.VarChar, "v": args.image }
                    ]
                };
            this.DM.executeStatement(query, (err, _msg) => {
                if (err) {
                    //console.log("Attendance error", err);
                    return;
                }
                //console.log('Attendance', _msg);
                var att = _msg[0];
                _o = {
                    "time": currentTime,
                    "staffName": att.staffName,
                    "type": args.type
                };

                //console.log(JSON.stringify(att));

                var cloud = {};
                cloud.staffUsername = att.staffUsername;
                cloud.staffName = att.staffName;
                cloud.shopId = att.shopId;
                cloud.datetime = moment(att.datetime).format('YYYY-MM-DD HH:mm:ssSSS');
                cloud.type = att.type;

                fs.writeFile("./cloud/attendance/" + moment().format("YYMMDDHHmmss") + ".json", JSON.stringify(cloud), function (err) {
                    if (err) {
                        //console.log(err);
                    } else {
                        ////console.log("The file was saved!");
                    }
                });
                callback(_o);
            });
        });
    }

    this.CashControl = (args, callback) => {
        var currentTime = new Date(),
            query = {
                "sql": "insert into cashControl (cash, type, remark, datetime, staffId) VALUES (@cash, @type, @remark, @datetime, @staffId);",
                "args": [
                    { "c": "cash", "t": TYPES.Decimal, "v": parseFloat(args.cash), "o": { "scale": 2 } },
                    { "c": "type", "t": TYPES.VarChar, "v": args.type },
                    { "c": "remark", "t": TYPES.NVarChar, "v": args.remark },
                    { "c": "datetime", "t": TYPES.DateTime, "v": currentTime },
                    { "c": "staffId", "t": TYPES.VarChar, "v": args.staffId }
                ]
            };
        this.DM.executeStatement(query, (err, _msg) => {
            if (err) {
                //console.log("CashControl error", err);
                return;
            }
            //console.log('CashControl', _msg);
            this.PrepareDailyCashReport(args, callback);
        });
    }


    this.PrepareDailyCashReport = (args, callback) => {

        var date = moment().format("DD/MM/YYYY"),
            today = getToday(),
            tomorrow = getTomorrow(),
            query = [{
                "sql": "select * from (select top 1 * from cashControl where type = 'open' and [datetime] between @fromDate and @toDate order by datetime desc ) t union select * from cashControl where type != 'open' and [datetime] between @fromDate and @toDate order by type, datetime",
                "args": [
                    { "c": "fromDate", "t": TYPES.DateTime, "v": today },
                    { "c": "toDate", "t": TYPES.DateTime, "v": tomorrow }
                ]
            }, { "sql": "select * from tenderpayment  where void = 0 ", "args": [] },
        { "sql": "select * from tenderDiscount where void = 0 ", "args": [] },
        { "sql": "select * from [transaction] where void = 0 ", "args": [] }]
        this.DM.executeStatement(query, (err, result) => {
            if (err) {
                //console.log("CashControl error", err);
                return;
            }
            var obj = { "GROSS_TOTAL_AMOUNT": 0, "TOTAL_CASH_SALES_AMOUNT": 0, "TOTAL_AMOUNT": 0, "CASH_RECORDS": [], "REPORT_DATE": "", "REPORT_TIME": "" };

            //console.log('PrepareDailyCashReport', result);
            obj.REPORT_DATE = moment().format("DD/MM/YYYY");
            obj.REPORT_TIME = moment().format("HH:mm");
            obj.TOTAL_AMOUNT = 0;
            obj.CASH_SALES_AMOUNT = 0;
            obj.GROSS_TOTAL_AMOUNT = 0;


            var transaction = result[3];
            var discount = result[2];
            var payment = result[1];

            for (var i = 0; i < transaction.length; i++) {
                var t = transaction[i];
                var totalPrice = t.price + t.serviceCharge + t.remainings;
                var dis = discount.filter(function (dis) {
                    return dis.refNo == t.refNo;
                })
                for (var j = 0; j < dis.length; j++) {
                    totalPrice -= dis[j].discountValue;
                }
                var po = payment.filter(function (po) {
                    return po.refNo == t.refNo;
                })
                var pricefornoncash = totalPrice
                for (var j = 0; j < po.length; j++) {
                    var p = po[j];
                    if (p.paymentType == 'cash') {
                        if (pricefornoncash < p.paymentValue) {
                            obj.TOTAL_CASH_SALES_AMOUNT += pricefornoncash;
                            pricefornoncash = 0;
                        } else {
                            obj.TOTAL_CASH_SALES_AMOUNT += p.paymentValue;
                            pricefornoncash -= p.paymentValue;
                        }
                        //obj.CASHSALES.count += 1;
                        break;
                    }
                }
            }

            result[0].forEach(function (record) {
                if (record.type === "petty") {
                    record.cash *= -1;
                }
                record["time"] = moment(record.datetime).format("HH:mm");
                obj.CASH_RECORDS.push(record);
                obj.TOTAL_AMOUNT += record.cash;
            });

            obj.GROSS_TOTAL_AMOUNT = obj.TOTAL_AMOUNT + obj.TOTAL_CASH_SALES_AMOUNT;
            callback(obj);
        });
    }

    function AssignLastOrderSchedule(jobName, timeString) {
        //console.log("AssignLastOrderSchedule", schedule.scheduledJobs);
        if (schedule.scheduledJobs[jobName] != undefined) {
            //console.log("[" + jobName + "] already assigned");
            return;
        }
        if (timeString.match(/^[0-9]+:[0-9]+/ig)) {
            timeString = moment().format("YYYY/MM/DD ") + timeString;
        }
        if (!Date.parse(timeString)) {
            //console.log("parse [" + timeString + "] error");
            return;
        }
        var date = new Date(timeString);
        if (date <= new Date()) return;
        var j = schedule.scheduleJob(jobName, date, function (y, RAS) {
            var logMsg = "[ScheduleReloadMenu] scheduled job [" + y + "] was executed at [" + new Date().toString() + "].";
            // writeLog(": " + logMsg + " \r\n");
            //console.log(logMsg);
            var query = {
                "sql": "select lastOrder, lastOrderWarning,leaveTime , tableCode, tableSubCode from tableorder where lastOrder <= @currentTime or lastOrderWarning <= @currentTime",
                "args": [{ "c": "currentTime", "t": TYPES.Time, "v": new Date() }]
            }
            // var DM = new DatabaseManager(connString);
            RAS.DM.executeStatement(query, function (err, results) {
                if (err) throw err;
                var queries = [];
                results.forEach(function (result) {
                    var tableNo = result.tableCode + (result.tableSubCode != "" ? "_" + result.tableSubCode : "");
                    //console.log("tableNo", tableNo);
                    if (new Date(moment().format("YYYY/MM/DD ") + moment(result.leaveTime).format("HH:mm:ss")) <= new Date()) {
                        var _o = { action: "leaveTime", time: moment(result.leaveTime).format("HH:mm:ss") };
                        mqttClient.publish("wo/gm/table/" + tableNo + "/", JSON.stringify(_o), { qos: 2 });
                        queries.push({
                            "sql": "update tableorder set buffetStatus = 3 where tableCode = @tableCode and tableSubCode = @tableSubCode",
                            "args": [
                                { "c": "tableCode", "t": TYPES.VarChar, "v": result.tableCode },
                                { "c": "tableSubCode", "t": TYPES.VarChar, "v": result.tableSubCode }
                            ]
                        })
                    } else
                        if (new Date(moment().format("YYYY/MM/DD ") + moment(result.lastOrder).format("HH:mm:ss")) <= new Date()) {
                            var _o = { action: "lastOrder", time: moment(result.lastOrder).format("HH:mm:ss") };
                            mqttClient.publish("wo/gm/table/" + tableNo + "/", JSON.stringify(_o), { qos: 2 });
                            queries.push({
                                "sql": "update tableorder set buffetStatus = 2 where tableCode = @tableCode and tableSubCode = @tableSubCode",
                                "args": [
                                    { "c": "tableCode", "t": TYPES.VarChar, "v": result.tableCode },
                                    { "c": "tableSubCode", "t": TYPES.VarChar, "v": result.tableSubCode }
                                ]
                            })
                        }
                        else if (new Date(moment().format("YYYY/MM/DD ") + moment(result.lastOrderWarning).format("HH:mm:ss")) <= new Date()) {
                            var _o = { action: "lastOrderWarning", time: moment(result.lastOrder).format("HH:mm:ss") };
                            mqttClient.publish("wo/gm/table/" + tableNo + "/", JSON.stringify(_o), { qos: 2 });
                            queries.push({
                                "sql": "update tableorder set buffetStatus = 1 where tableCode = @tableCode and tableSubCode = @tableSubCode",
                                "args": [
                                    { "c": "tableCode", "t": TYPES.VarChar, "v": result.tableCode },
                                    { "c": "tableSubCode", "t": TYPES.VarChar, "v": result.tableSubCode }
                                ]
                            })
                        }

                });
                RAS.DM.executeStatement(queries, function (err, results) {
                    if (err) throw err;
                    RAS.LoadTableSchema(null, (err, result) => {
                        io.sockets.emit('refreshTable', JSON.stringify(result));
                    })
                });
            });
        }.bind(null, jobName, new RASManager(config.sql)));
        // scheduleJobList.push(j);
    }

    this.InitLastOrderSchedule = () => {
        var query = {
            "sql": "select distinct lastOrder, lastOrderWarning from tableorder where lastOrder is not null and lastOrderWarning is not null",
            "args": []
        };
        this.DM.executeStatement(query, (err, results) => {
            results.forEach(function (result) {
                var lastOrder = moment(result.lastOrder).format("HH:mm:ss");
                var lastOrderWarning = moment(result.lastOrderWarning).format("HH:mm:ss");
                AssignLastOrderSchedule("lastOrder_" + lastOrder, lastOrder);
                AssignLastOrderSchedule("lastOrder_" + lastOrderWarning, lastOrderWarning);
            });
        });
        // AssignLastOrderSchedule("lastOrder_" + "15:49", "15:49");
    }

    this.OpenCashBox = (args, callback) => {
        console.log('OpenCashBox' + JSON.stringify(args))
        /*fs.writeFile("./printer/" + args.till + "/openCB_" + moment().format("YYMMDDHHmmssSSSS") + ".json", JSON.stringify(args), function (err) {
            if (err) {
                console.log(err);
            } else {
                ////console.log("The file was saved!");
            }
            callback({ 'result': 'OK' });
        });*/

        RAS.writeFile("./printer/" + args.till + "/openCB_", JSON.stringify(args), function (err) {

        })
    }

    this.EditOrder = (args, callback) => {

        console.log(args);
        console.log(JSON.stringify(args.existingCart.json.order));
        //console.log(JSON.stringify(args.existingCart.json.order.item[0]));

        var tableObj = getTableCode(args.tableNum.$value);

        var date = new Date();

        var staff = args.user == undefined ? "user" : args.user.$value;
        var printingArray = { TableNo: args.tableNum.$value, Pax: args.peopleNum.$value, FoodItem: [] };

        var query = [];
        var _o = {};

        console.log('k message', JSON.stringify(args.existingCart.json.order.item));
        var vi = -1, vr = "", customName = "", approveStaff = "", _sc = false, lastPrinter = "", lastKds = "";

        var kdsShow = [];
        args.existingCart.json.order.item.forEach((i, idx) => {

            var items = getItemByItem({ itemId: i.code[0], type: i.type[0] });


            if (i.customName) {
                if (i.customName[0])
                    items[0].namek = i.customName[0];
            }
            console.log('items', items);
            var printer = items[0].printer;
            //printer += items[0].printerGroup != null ? "_" + items[0].printerGroup : "";
            printingArray.FoodItem.push({ Name: items[0].namek, Qty: i.qty[0], Printer: items[0].printer, Code: items[0].code, Time: moment().format("HH:mm:ss"), Msg: '', individualPrint: items[0].individualPrint, Type: i.type[0], holdKitchenPrinter: items[0].holdKitchenPrinter });
            if (printer) lastPrinter = printer;
            if (items[0].kds) lastKds = items[0].kds;
            if (i.type[0] == "K") {

                //query.push({
                //    "sql": "insert into tableOrderD (tableCode,tableSubCode,itemId,qty,price,voidIndex,voidRemark,type,seq,time,staffId,round,customName,approveStaffId,serviceCharge,printer,kdsgroup,kds) values (@tableCode,@tableSubCode,@itemId,@qty,@price,@voidIndex,@voidRemark,@type,@seq,@time,@staffId,@round,@customName,@approveStaffId,@serviceCharge,@printer,@kdsgroup,@kds); \
                //            insert into kds (tableCode,tableSubCode,itemId,qty,voidIndex,type,seq,time,round,printer,kdsgroup,kds) values (@tableCode,@tableSubCode,@itemId,@qty,@voidIndex,@type,@seq,@time,@round,@printer,@kdsgroup,@kds)",
                //    "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": tableObj.tableCode }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": tableObj.tableSubCode }, { "c": "itemId", "t": TYPES.VarChar, "v": items[0].code }, { "c": "qty", "t": TYPES.Int, "v": i.qty[0] },
                //        { "c": "price", "t": TYPES.Decimal, "v": parseFloat((parseFloat(i.unitPrice[0]) / 100).toFixed(2)), "o": { "scale": 2 } }, { "c": "voidIndex", "t": TYPES.Int, "v": vi }, { "c": "voidRemark", "t": TYPES.NVarChar, "v": vr }, { "c": "type", "t": TYPES.VarChar, "v": i.type[0] }, { "c": "seq", "t": TYPES.Int, "v": i.index }, { "c": "time", "t": TYPES.Time, "v": date }, { "c": "staffId", "t": TYPES.VarChar, "v": staff }, { "c": "round", "t": TYPES.Int, "v": null }, { "c": "customName", "t": TYPES.NVarChar, "v": customName }, { "c": "approveStaffId", "t": TYPES.VarChar, "v": approveStaff }, { "c": "serviceCharge", "t": TYPES.Bit, "v": _sc }, { "c": "printer", "t": TYPES.VarChar, "v": lastPrinter }, { "c": "kdsgroup", "t": TYPES.BigInt, "v": '-1' }, { "c": "kds", "t": TYPES.VarChar, "v": lastKds }]
                //});

                query.push({
                    "sql": "insert into tableOrderD (tableCode,tableSubCode,itemId,qty,price,voidIndex,voidRemark,type,seq,time,staffId,round,customName,approveStaffId,serviceCharge,printer,kdsgroup,kds) values (@tableCode,@tableSubCode,@itemId,@qty,@price,@voidIndex,@voidRemark,@type,@seq,@time,@staffId,1,@customName,@approveStaffId,@serviceCharge,@printer,@kdsgroup,@kds); \
                      insert into kds (tableCode,tableSubCode,itemId,qty,voidIndex,type,seq,time,round,printer,kdsgroup,kds) values (@tableCode,@tableSubCode,@itemId,@qty,@voidIndex,@type,@seq,@time,1,@printer,@kdsgroup,@kds)",
                    "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": tableObj.tableCode }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": tableObj.tableSubCode }, { "c": "itemId", "t": TYPES.VarChar, "v": items[0].code }, { "c": "qty", "t": TYPES.Int, "v": i.qty[0] },
                        { "c": "price", "t": TYPES.Decimal, "v": parseFloat((parseFloat(i.unitPrice[0]) / 100).toFixed(2)), "o": { "scale": 2 } }, { "c": "voidIndex", "t": TYPES.Int, "v": vi }, { "c": "voidRemark", "t": TYPES.NVarChar, "v": vr }, { "c": "type", "t": TYPES.VarChar, "v": i.type[0] }, { "c": "seq", "t": TYPES.Int, "v": i.index }, { "c": "time", "t": TYPES.Time, "v": date }, { "c": "staffId", "t": TYPES.VarChar, "v": staff }, { "c": "round", "t": TYPES.Int, "v": null }, { "c": "customName", "t": TYPES.NVarChar, "v": customName }, { "c": "approveStaffId", "t": TYPES.VarChar, "v": approveStaff }, { "c": "serviceCharge", "t": TYPES.Bit, "v": _sc }, { "c": "printer", "t": TYPES.VarChar, "v": lastPrinter }, { "c": "kdsgroup", "t": TYPES.BigInt, "v": '-1' }, { "c": "kds", "t": TYPES.VarChar, "v": lastKds }]
                });


            }

            if (i.type[0] != "M" && i.type[0] != "K") {
                if (i.isFired && config.fireDineInMode) {
                    query.push({
                        "sql": "update [kds] set kdsStatus = null where tableCode=@tableCode and tableSubCode = @tableSubCode and itemId=@itemId and seq=@seq",
                        "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": tableObj.tableCode }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": tableObj.tableSubCode }, { "c": "itemId", "t": TYPES.VarChar, "v": items[0].code }, { "c": "seq", "t": TYPES.Int, "v": i.seq }]
                    });

                    kdsShow.push(lastKds);
                }
            }

        });

        this.DM.executeStatement(query, (err, result) => {
            callback({ result: 'OK' })

            if (kdsShow.length > 0) {
                for (var a = 0 ; a < kdsShow.length ; a++) {
                    console.log('kdsshow', kdsShow[a])
                    RAS.showKds(kdsShow[a]);
                }
            }
        });




        if (args.allOrderList.item.length > 0) {
            printingArray.FoodItem = [];
            args.allOrderList.item.forEach((i, idx) => {
                var items = getItemByItem({ itemId: i.code[0], type: i.type[0] });
                console.log('items', items);
                var printer = items[0].printer;
                printer += items[0].printerGroup != null ? "_" + items[0].printerGroup : "";
                printingArray.FoodItem.push({ Name: items[0].namek, Qty: i.qty[0], Printer: items[0].printer, Code: items[0].code, Time: moment().format("HH:mm:ss"), Msg: '', individualPrint: items[0].individualPrint, Type: i.type[0], holdKitchenPrinter: items[0].holdKitchenPrinter });
            })
        }

        console.log('editOrder', printingArray, printingArray.FoodItem.length);
        printingArray.till = args.till;

        console.log('fire mode2', printingArray);

        if (config.fireDineInMode) {
            RAS.firePrinting(printingArray, function (result) {
                RAS.PrintFoodItem(result);
            })
        } else {
            this.PrintFoodItem(printingArray);
        }


        this.LoadTableSchema(null, (err, result) => {
            io.sockets.emit('refreshTable', JSON.stringify(result));
        })

        //this.PrintFoodItem(printingArray);

        //fs.writeFile("./printer/" + moment().format("YYMMDDHHmmssSSSS") + ".json", JSON.stringify(printingArray), function (err) {
        //    if (err) {
        //        //console.log(err);
        //    } else {
        //        ////console.log("The file was saved!");
        //    }
        //});
    }

    this.SplitTableOrder = (args, callback) => {
        console.log(args);
        console.log(args.itemData.json.order);
        var query = [];
        query.push({ "sql": "select starttime from tableorder where tableCode = @tableCode;delete tableorder where tableCode = @tableCode and tableSubCode = '' ", args: [{ "c": "tableCode", "t": TYPES.VarChar, "v": args.tableNum.$value }] }, { "sql": "select max(round) as round, max(seq) as seq from tableorderd where tableCode = @tableCode ;", "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": args.tableNum.$value }] });
        //query.push({ "sql": "select startTime from tableorder where tableCode = @tableCode;delete tableorder where tableCode = @tableCode", args: [{ "c": "tableCode", "t": TYPES.VarChar, "v": '123' }] });
        var charArray = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".split('');

        this.DM.executeStatement(query, (err, result) => {
            query = [];
            console.log(result);
            var startTime = new Date();
            var r = args.itemData.json;
            var tableObj = {};
            var currentSeq = 1;
            //currentSeq = result[1][0].seq
            var currentRound = 1;
            //currentRound = result[1][0].round;
            if (r.order.length != 0) {
                query.push({
                    "sql": "delete from tableOrderD where tableCode = @tableCode; delete from kds where tableCode = @tableCode", args: [{ "c": "tableCode", "t": TYPES.VarChar, "v": args.tableNum.$value }]
                });
            }

            var templOrderList = [];
            var subTableDelete = "( ";

            r.order.forEach((order, idx) => {

                if (idx != r.order.length - 1) {
                    subTableDelete += "'" + order.suffix + "',";
                } else {
                    subTableDelete += "'" + order.suffix + "' ) ";
                    //delete tableOrderD where tableCode and tableSubCode not in " + subTableDelete + ";"
                    query.push({
                        "sql": "delete from tableOrder where tableCode = @tableCode  and  tableSubCode not in " + subTableDelete + "; delete tableOrderD where tableCode = @tableCode and tableSubCode not in " + subTableDelete + " ; delete tableDiscount where tableCode = @tableCode and tableSubCode not in " + subTableDelete + " ; delete kds where tableCode = @tableCode and tableSubCode not in " + subTableDelete + " ; ", "args": [
                            { "c": "tableCode", "t": TYPES.VarChar, "v": args.tableNum.$value }]
                    });

                    //var deleteTableOrder = { "sql": "delete tableOrder where tableCode = @tableCode and tableSubCode not in " + subTableDelete + "; delete tableOrderD where tableCode and tableSubCode not in " + subTableDelete + ";", "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": args.tableNum.$value }] };

                    console.log('delete tableOrder', subTableDelete);
                }

                startTime = new Date();
                if (result[0][0] && idx == 0) {
                    if (result[0][0].startTime)
                        startTime = result[0][0].startTime;
                }

                if (order.status != 4) {
                    query.push({
                        "sql": "MERGE INTO tableOrder d USING (SELECT @tableCode as  tableCode, @tableSubCode as tableSubCode, @pax as pax, \
                                @startTime as startTime, @staffId as staffId, @status as status, @cardId as cardId ) s ON \
                                (d.tableCode = s.tableCode and d.tableSubCode = s.tableSubCode) WHEN MATCHED THEN \
                                UPDATE SET d.pax = case when d.status = 4 then d.pax else s.pax end , d.status  = case when d.status = 4 then 4 else s.status end \
                                WHEN NOT MATCHED THEN INSERT (tableCode,tableSubCode,pax,startTime,staffId,status, cardId) VALUES (s.tableCode, s.tableSubCode, s.pax, s.startTime, s.staffId,s.status, s.cardId);",
                        args: [
                        { "c": "tableCode", "t": TYPES.VarChar, "v": args.tableNum.$value },
                        { "c": "tableSubCode", "t": TYPES.VarChar, "v": order.suffix },
                        { "c": "pax", "t": TYPES.VarChar, "v": order.nop },
                        { "c": "startTime", "t": TYPES.DateTime, "v": startTime },
                        { "c": "staffId", "t": TYPES.VarChar, "v": args.user.$value },
                        { "c": "status", "t": TYPES.Int, "v": 2 },
                        { "c": "cardId", "t": TYPES.VarChar, "v": order.cardId ? order.cardId : null }
                        ]
                    });
                }


                /*templOrderList.push({
                    "sql": "MERGE INTO tableOrder d USING (SELECT @tableCode as  tableCode, @tableSubCode as tableSubCode, @pax as pax, \
                            @startTime as startTime, @staffId as staffId, @status as status ) s ON \
                            (d.tableCode = s.tableCode and d.tableSubCode = s.tableSubCode) WHEN MATCHED THEN \
                            UPDATE SET d.pax = case when d.status = 4 then d.pax else s.pax end , d.status  = case when d.status = 4 then 4 else s.status end \
                            WHEN NOT MATCHED THEN INSERT (tableCode,tableSubCode,pax,startTime,staffId,status) VALUES (s.tableCode, s.tableSubCode, s.pax, s.startTime, s.staffId,s.status);",
                    args: [
                    { "c": "tableCode", "t": TYPES.VarChar, "v": args.tableNum.$value },
                    { "c": "tableSubCode", "t": TYPES.VarChar, "v": order.suffix },
                    { "c": "pax", "t": TYPES.VarChar, "v": order.nop },
                    { "c": "startTime", "t": TYPES.DateTime, "v": startTime },
                    { "c": "staffId", "t": TYPES.VarChar, "v": args.user.$value },
                    { "c": "status", "t": TYPES.Int, "v": 2 }
                    ]
                });*/


                var voidItemList = order.item.filter(function (voidItem) {
                    return voidItem.qty[0] < 0;
                });

                console.log('voidItemList', voidItemList);


                order.item.forEach((item, itemIdx) => {
                    //console.log(item);
                    //console.log(item.index[0]); item.index[0] == 0


                    if (item.type[0] != 'M' && item.type[0] != 'K')
                        currentSeq += 1;

                    query.push({
                        "sql": "insert into tableOrderD (tableCode,tableSubCode,itemId,qty,price,voidIndex,voidRemark,type,seq,time,staffId,round,customName,approveStaffId,serviceCharge,printer,kdsgroup,kds) values (@tableCode,@tableSubCode,@itemId,@qty,@price,@voidIndex,@voidRemark,@type,@seq,@time,@staffId,@round,@customName,@approveStaffId,@serviceCharge,@printer,@kdsgroup,@kds); \
                                insert into kds (tableCode,tableSubCode,itemId,qty,voidIndex,type,seq,time,round,printer,kdsgroup,kds) values (@tableCode,@tableSubCode,@itemId,@qty,@voidIndex,@type,@seq,@time,@round,@printer,@kdsgroup,@kds);",
                        "args": [
                        { "c": "tableCode", "t": TYPES.VarChar, "v": args.tableNum.$value },
                        { "c": "tableSubCode", "t": TYPES.VarChar, "v": order.suffix },
                        { "c": "itemId", "t": TYPES.VarChar, "v": item.code[0] },
                        { "c": "qty", "t": TYPES.Int, "v": parseInt(item.qty[0]) },
                        { "c": "price", "t": TYPES.Decimal, "v": parseFloat((parseFloat(item.unitPrice[0]) / 100).toFixed(2)), "o": { "scale": 2 } },
                        { "c": "voidIndex", "t": TYPES.Int, "v": item.qty[0] < 0 ? item.newVoidIndex : item.voidIndex[0] },
                        { "c": "voidRemark", "t": TYPES.NVarChar, "v": item.voidRemark[0] },
                        { "c": "type", "t": TYPES.VarChar, "v": item.type[0] },
                        { "c": "seq", "t": TYPES.Int, "v": currentSeq },
                        { "c": "time", "t": TYPES.Time, "v": new Date() },
                        { "c": "staffId", "t": TYPES.VarChar, "v": args.user.$value },
                        { "c": "round", "t": TYPES.Int, "v": currentRound },
                        { "c": "customName", "t": TYPES.NVarChar, "v": item.customName || "" },
                        { "c": "approveStaffId", "t": TYPES.VarChar, "v": null },
                        { "c": "serviceCharge", "t": TYPES.Bit, "v": item.serviceCharge[0] },
                        { "c": "printer", "t": TYPES.VarChar, "v": item.printer[0] },
                        { "c": "kdsgroup", "t": TYPES.BigInt, "v": '-1' },
                        { "c": "kds", "t": TYPES.VarChar, "v": null }]
                    });



                    voidItemList.forEach(function (voidItem) {
                        if (voidItem.voidIndex[0] === item.index[0]) {
                            voidItem.newVoidIndex = currentSeq
                            //voidItem.voidIndex[0] = currentSeq;
                            console.log('item.index[0]', item.index[0]);
                            console.log('currentSeq', currentSeq);
                            console.log('voidItem.voidIndex[0]', voidItem.voidIndex[0]);
                        }
                    });


                    //if (item.voidList != undefined) {
                    //    item.voidList.forEach(function (voidItem) {
                    //        voidListQuery.push(
                    //            {
                    //                itemId: voidItem.code[0], qty: voidItem.qty[0], price: parseFloat((parseFloat(voidItem.unitPrice[0]) / 100).toFixed(2)), voidIndex: item.voidIndex[0], voidRemark: voidItem.voidRemark[0], type: voidItem.type[0], seq: voidItem.index[0], time: new Date(), staffId: args.user.$value, round: currentRound, customName: voidItem.customName,
                    //                approveStaffId: null, serviceCharge: 0, printer: '', kdsgroup: -1,
                    //                kds: null
                    //            }
                    //        );
                    //    })
                    //}


                    //if (itemIdx == order.item.length - 1) {
                    //    maxSeq = item.index[0];
                    //}



                    //query.push({
                    //    "sql": "update tableorderd set tableSubCode = @tableSubCode where tableCode = @tableCode and seq = @seq",
                    //    args: [{ "c": "tableCode", "t": TYPES.VarChar, "v": args.tableNum.$value },
                    //{ "c": "tableSubCode", "t": TYPES.VarChar, "v": charArray[idx] },
                    //    { "c": "seq", "t": TYPES.Int, "v": item.index[0] }]
                    //})



                    // if( charArray[idx] === -1 ) {
                    //     query.push({
                    //     });
                    // }
                })

                /* simon todo list when index -1 then insert to tableorderd */

            });

            this.DM.executeStatement(query, (err, result) => {
                //callback({ result: 'OK' })

                //console.log('templOrderList', JSON.stringify(templOrderList));

                query = [];
                r.order.forEach((order, idx) => {
                    query.push({ "sql": "select * from tableorderd where tableCode = @tableCode and tableSubCode = @tableSubCode", "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": args.tableNum.$value }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": order.suffix }] });
                });
                this.DM.executeStatement(query, (err, result) => {
                    var updateTableOrderArray = [];
                    result.forEach((tableOrderD, idx) => {
                        if (r.order[idx].status != 4) {
                            var dT = getTableCode(args.tableNum.$value + "_" + r.order[idx].suffix);
                            updateTableOrderArray.push(updateTableOrder(dT, tableOrderD, r.order[idx].nop, args.user.$value));
                        }
                    });
                    //this.SetTableStatus({ "tableCode": args.tableNum.$value, "status": 2 })
                    Q.all(updateTableOrderArray).then((res) => {
                        this.LoadTableSchema(null, (err, result) => {
                            io.sockets.emit('refreshTable', JSON.stringify(result));
                        })


                        callback({ result: 'OK' })
                        //mqttClient.publish("wo/gm/table/" + args.tableNum.$value + "/", JSON.stringify({ "action": "unlinkTable", "tableno": args.tableNum.$value }), { qos: 2 });
                        mqttClient.publish("wo/gm/table/" + args.tableNum.$value + "/", JSON.stringify({ "action": "reloadTable", "tableno": args.tableNum.$value + "_" + "A", "pax": r.order[0].nop }), { qos: 2 });
                    })
                });
            })
        })


    }

    function goQueue(DM) {
        this.DM = DM;

        //goQueue functions
        //function uploadLog(qs, callback) {
        this.uploadLog = (qs, callback) => {
            var path = logsDir + qs.file_name;
            fs.writeFile(path, qs.file_content, function (err) {
                if (err) {
                    console.log(err);
                }
            });
            callback("ok");
        }

        //function writeFasspastDeductLog(content) {
        this.writeFasspastDeductLog = (context) => {
            var now = new Date();
            var fileName = "_" + now.getFullYear() + (now.getMonth() + 1).padLeft(2, "0") + now.getDate().padLeft(2, "0") + now.getHours().padLeft(2, "0") + now.getMinutes().padLeft(2, "0") + now.getSeconds().padLeft(2, "0") + now.getMilliseconds().padLeft(3, "0");
            var path = deductPtsLogsDir + fileName + ".log";
            fs.writeFile(path, content, function (err) {
                if (err) {
                    console.log(err);
                }
            });
            return path;
        }

        //function retryDeductPts() {
        this.retryDeductPts = () => {
            fs.readdir(deductPtsLogsDir, function (err, files) {
                if (err) return;

                files.forEach(function (filename) {
                    var path = deductPtsLogsDir + filename;
                    console.log('retryDeductPts', path);
                    fs.readFile(path, { 'encoding': 'utf8' }, function (err, output) {
                        if (err) throw err;
                        console.log(output);
                        var postData = QueryString.stringify({
                            'shopcode': queueConfig.shopcode,
                            'queue': output, // queue action information, the service.cs receive a jsonArray
                            'action': 'UpdateRemoteQueue'
                        });
                        postToServer(postData, null, function (chunk) {
                            var postResponse = JSON.parse(chunk),
                                feedback = {};

                            if (postResponse.UpdateRemoteQueue.expire) {
                                fs.unlink(path, function (err) {
                                    if (err)
                                        console.log('unable to delete ' + path, err);
                                    else
                                        console.log('successfully deleted ' + path);
                                });
                            }
                            else {
                                if (postResponse.UpdateRemoteQueue.separateQueueServer) {
                                    feedback = postResponse.UpdateRemoteQueue.deductPostBack.feedback;
                                }
                                else {
                                    feedback = postResponse.UpdateRemoteQueue.feedback;
                                }
                                console.log('postResponse feedback', feedback);
                                if (feedback.status == 0) {
                                    fs.unlink(path, function (err) {
                                        if (err)
                                            console.log('unable to delete ' + path, err);
                                        else
                                            console.log('successfully deleted ' + path);
                                    });
                                }
                            }
                        });
                    });
                });
            });
        }

        //function sufficientTableMode(qs, callback) {
        this.sufficientTableMode = (qs, callback) => {
            mqttClient.publish('wo/gq/client', "{\"action\":\"sufficientTableMode\"}");
            io.sockets.emit('toggleSufficientTableMode', qs.mode);
            callback("ok");
        }

        //function updateDisplayPanel(qs, callback) {
        this.updateDisplayPanel = (qs, callback) => {
            io.sockets.emit('updateDisplayPanel', qs.mode);
            callback("ok");
        }

        //function reprint(qs, callback) {
        this.reprint = (qs, callback) => {
            if (!callback) callback = function () { };

            var obj = {
                "action": "reprint",
                "date": getPrintDate(new Date()),
                "time": qs.time,
                "response": "success",
                "ppl": qs.ppl,
                "shopAddress": queueConfig.shopaddress,
                "group": qs.queuegroup,
                "queuegroup": qs.queuegroup,
                "ticketNo": qs.ticketNo
            };

            if (queueConfig.printFrom === "c") {
                mqttClient.publish('wo/gq/client', JSON.stringify(obj));
            }
            else if (queueConfig.printFrom === "u") {
                mqttClient.publish('wo/gq/usherpanel', JSON.stringify(obj));
            }
            else {

                /*fs.writeFile("./printer/" + config.defaultQueuePrinter + "/queue_" + moment().format("YYMMDDHHmmssSSSS") + ".json", JSON.stringify(_obj), function (err) {
                    if (err) {
                        console.log("queueinfofile:" + err);
                    } else {
                        //console.log("The file was saved!");
                    }
                });*/

                RAS.writeFile("./printer/" + config.defaultQueuePrinter + "/queue_", JSON.stringify(obj), function (err) {
                    if (err) {
                        console.log("queueinfofile:" + err);
                        mqttClient.publish('wo/gq/client', JSON.stringify({ "action": "printFailure" }));
                    } else {
                        //console.log("The file was saved!");
                        mqttClient.publish('wo/gq/client', JSON.stringify({ "action": "printFinish" }));
                    }
                });
            }
            callback("ok");
        }

        //function openConfig(qs, callback) {
        this.openConfig = (qs, callback) => {
            mqttClient.publish('wo/gq/client', "{\"action\":\"openConfig\"}");
            callback("ok");
        }

        //function updateConfig(qs, callback) {
        this.updateConfig = (qs, callback) => {
            var incomingconfig = JSON.parse(qs.config);
            if (qs.delete == "true") {
                for (var ickey in incomingconfig) {
                    delete queueConfig[ickey];
                }
            } else {
                for (var ickey in incomingconfig) {
                    queueConfig[ickey] = incomingconfig[ickey];
                }
            }
            fs.writeFile("./goqueue.json", JSON.stringify(queueConfig), function (err) {
                if (err) {
                    console.log(err);
                } else {
                    //console.log("The file was saved!");
                }
            });

            cronUpdateQueueJob.stop();
            cronUploadConfigJob.stop();
            resetCronJob();
            callback("ok");
        }

        //function updateTicketPanel(qs, callback) {
        this.updateTicketPanel = (qs, callback) => {
            this.showQueueingList(qs, callback, "updateTicketPanel");
        }

        //function skipAllTicket(qs, callback)
        this.skipAllTicket = (args, callback) => {
            var today = getToday(),
            tomorrow = getTomorrow(),
            now = new Date();
            var query = {
                "sql": "update ticket set skip = 1, skiptime = @skiptime where date between @today and @tomorrow and seat = 0 and skip = 0;",
                "args": [
                    { "c": "skiptime", "t": TYPES.DateTime, "v": now },
                    { "c": "today", "t": TYPES.DateTime, "v": today },
                    { "c": "tomorrow", "t": TYPES.DateTime, "v": tomorrow }
                ]
            };
            this.DM.executeStatement(query, (err, tickets) => {
                this.updateQueue();

                console.log("args=======================> for skipAllTicket");
                console.log(args);

                var output = {
                    "group": "",
                    "ticket_prefix": "",
                    "no_of_people": -1,
                    "ticketnumber": -1,
                    "modify_from": "",
                    "modify_to": "",
                    "queue_action": queue_action["skipAllTicket"],
                    "queue_action_datetime": current_datetime()
                };

                var postData = QueryString.stringify({
                    'shopcode': queueConfig.shopcode,
                    'queue': JSON.stringify(output), // queue action information, the service.cs receive a jsonArray
                    'action': 'writeLog, UpdateRemoteQueue'
                });
                postToServer(postData, null, function (chunk) { });

                this.showQueueingList(args, callback, "updateTicketPanel");
                refreshshopdisplay({
                    "action": "skipAllTicket"
                });
            });
        }

        //function reviewTickets(qs, callback)
        this.reviewTickets = (args, callback) => {
            var today = getToday(),
            tomorrow = getTomorrow();
            var resp = {};
            var query = {
                "sql": "select * from ticket where date between @today and @tomorrow and (seat = 1 or skip = 1) order by date desc",
                "args": [
                    { "c": "today", "t": TYPES.DateTime, "v": today },
                    { "c": "tomorrow", "t": TYPES.DateTime, "v": tomorrow }
                ]
            };
            this.DM.executeStatement(query, (err, tickets) => {
                for (var i = 0; i < queueConfig.queuetype.length; i++) {
                    resp["table" + i] = [];
                    var resultset = tickets.filter(function (x) {
                        // return x.pax >= queueConfig.queuetype[i].min && (x.pax <= queueConfig.queuetype[i].max || queueConfig.queuetype[i].max == -1)
                        return x.queuegroup == i
                    });
                    for (var j = 0; j < resultset.length; j++) {
                        var t = resultset[j];
                        resp["table" + i].push({
                            "group": t.queuegroup,
                            "pax": t.pax,
                            "ticket": t.ticket,
                            "specialrequest": t.specialrequest,
                            "called": t.call,
                            "seated": t.seat,
                            "timestamp": t.date.toTimeString().split(' ')[0].substr(0, 8),
                            "seattime": t.seattime === null ? "" : t.seattime.toTimeString().split(' ')[0].substr(0, 8),
                            "skiptime": t.skiptime === null ? "" : t.skiptime.toTimeString().split(' ')[0].substr(0, 8)
                        })
                    }
                    resp["queueLen" + i] = resultset.length;
                }
                callback(JSON.stringify(resp));
            });
        }

        //function showQueueingList(qs, callback, type, obj)
        this.showQueueingList = (args, callback, type, obj) => {
            if (type != "updateTicketPanel" && type != "requestTicket" && callback != null) {
                callback("ok");
            }
            var today = getToday(),
            tomorrow = getTomorrow();
            var resp = {};
            var query = {
                "sql": "select * from ticket where date between @today and @tomorrow and seat = 0 and skip = 0 order by date asc",
                "args": [
                    { "c": "today", "t": TYPES.DateTime, "v": today },
                    { "c": "tomorrow", "t": TYPES.DateTime, "v": tomorrow }
                ]
            };
            this.DM.executeStatement(query, (err, tickets) => {
                for (var i = 0; i < queueConfig.queuetype.length; i++) {
                    resp["table" + i] = [];
                    var resultset = tickets.filter(function (x) {
                        return x.queuegroup == i
                    });
                    for (var j = 0; j < resultset.length; j++) {
                        var t = resultset[j];
                        resp["table" + i].push({
                            "group": t.queuegroup,
                            "pax": t.pax,
                            "ticket": t.ticket,
                            "specialrequest": t.specialrequest,
                            "called": t.call,
                            "seated": t.seat,
                            "timestamp": t.date.toTimeString().split(' ')[0].substr(0, 8),
                            "seattime": t.seattime === null ? "" : t.seattime.toTimeString().split(' ')[0].substr(0, 8),
                            "skiptime": t.skiptime === null ? "" : t.skiptime.toTimeString().split(' ')[0].substr(0, 8)
                        })
                    }
                    resp["queueLen" + i] = resultset.length;
                }
                if (obj != undefined) {
                    for (var propt in obj) {
                        console.log(propt + ': ' + obj[propt]);
                        resp[propt] = obj[propt]
                    }
                }
                if (callback != null && (type == "updateTicketPanel" || type == "requestTicket")) {
                    callback(JSON.stringify(resp));
                }
                this.updateQueueInfoFile(resp);
                mqttClient.publish('wo/gq/usherpanel', "{\"action\":\"updateTicketPanel\",\"queue\":" + JSON.stringify(resp) + "}");
            });
        }

        //function modifyTicket(qs, callback)
        this.modifyTicket = (args, callback) => {
            var today = getToday(),
            tomorrow = getTomorrow();
            var query = {
                "sql": "update ticket set pax = @pax where date between @today and @tomorrow and queuegroup = @queuegroup and ticket = @ticket; \
                    select * from ticket where date between @today and @tomorrow and pax = @pax and queuegroup = @queuegroup and ticket = @ticket order by date desc;",
                "args": [
                    { "c": "pax", "t": TYPES.Int, "v": args.ticketPeopleChange },
                    { "c": "today", "t": TYPES.DateTime, "v": today },
                    { "c": "tomorrow", "t": TYPES.DateTime, "v": tomorrow },
                    { "c": "queuegroup", "t": TYPES.Int, "v": args.group },
                    { "c": "ticket", "t": TYPES.Int, "v": args.ticketNo }
                ]
            };
            this.DM.executeStatement(query, (err, tickets) => {
                var ticket = tickets[0];
                console.log(ticket);
                var pdate = new Date();
                if (ticket != null) {
                    pdate = ticket.date;
                }
                var obj = {
                    "date": getPrintDate(pdate),
                    "time": pdate.toTimeString().split(' ')[0].substr(0, 5),
                    "response": "success",
                    "ppl": args.ticketPeopleChange,
                    "shopAddress": queueConfig.shopaddress
                };
                obj["number" + args.group] = args.ticketNo;

                console.log("args=======================> for modifyTicket");
                console.log(args);

                var output = {
                    "group": args.group,
                    "ticket_prefix": queueConfig.queuetype[args.group].prefix,
                    "no_of_people": args.ticketPeople,
                    "ticketnumber": args.ticket_no,
                    "modify_from": args.ticketPeople,
                    "modify_to": args.ticketPeopleChange,
                    "queue_action": queue_action["modifyTicket"],
                    "queue_action_datetime": current_datetime()
                };

                var postData = QueryString.stringify({
                    'shopcode': queueConfig.shopcode,
                    'queue': JSON.stringify(output), // queue action information, the service.cs receive a jsonArray
                    'action': 'writeLog'
                });
                postToServer(postData, null, function (chunk) { });

                this.showQueueingList(args, callback, "modifyTicket", obj);
            });
        }

        //function modifyTicketSpecialRequest(qs, callback)
        this.modifyTicketSpecialRequest = (args, callback) => {
            var today = getToday(),
            tomorrow = getTomorrow();
            var query = {
                "sql": "update ticket set specialrequest = @specialrequest where date between @today and @tomorrow and queuegroup = @queuegroup and ticket = @ticket;",
                "args": [
                    { "c": "specialrequest", "t": TYPES.VarChar, "v": args.specialrequest },
                    { "c": "today", "t": TYPES.DateTime, "v": today },
                    { "c": "tomorrow", "t": TYPES.DateTime, "v": tomorrow },
                    { "c": "queuegroup", "t": TYPES.Int, "v": args.group },
                    { "c": "ticket", "t": TYPES.Int, "v": args.ticketNo }
                ]
            };
            this.DM.executeStatement(query, (err, result) => {
                console.log("args=======================> for modifyTicketSpecialRequest");
                console.log(args);
                var modify_details = args.specialrequest

                var output = {
                    "group": args.group,
                    "ticket_prefix": queueConfig.queuetype[args.group].prefix,
                    "no_of_people": args.ticketPeople,
                    "ticketnumber": args.ticket_no,
                    "modify_from": "",
                    "modify_to": modify_details,
                    "queue_action": queue_action["modifyTicketSpecialRequest"],
                    "queue_action_datetime": current_datetime()
                };

                var postData = QueryString.stringify({
                    'shopcode': queueConfig.shopcode,
                    'queue': JSON.stringify(output), // queue action information, the service.cs receive a jsonArray
                    'action': 'writeLog'
                });
                postToServer(postData, null, function (chunk) { });

                this.showQueueingList(args, callback, "modifyTicketSpecialRequest");
            });
        }

        //function modifyTicketStatus(qs, callback)
        //Modifying ticket status does not change calltime/seattime/skiptime?
        this.modifyTicketStatus = (args, callback) => {
            var today = getToday(),
            tomorrow = getTomorrow();
            var updates = {};
            switch (args.ticketStatus) {
                case "wait":
                    updates = {
                        "seat": 0,
                        "call": 0,
                        "skip": 0
                    };
                    break;
                case "seated":
                    updates = {
                        "seat": 1,
                        "call": 1,
                        "skip": 0
                    };
                    break;
                case "skip":
                    updates = {
                        "seat": 0,
                        "call": 0,
                        "skip": 1
                    };
                    break;
                case "called":
                    updates = {
                        "seat": 0,
                        "call": 1,
                        "skip": 0
                    };
                    break;
                default:
                    break;
            }
            var query = {
                "sql": "update ticket set seat = @seat, call = @call, skip = @skip where date between @today and @tomorrow and queuegroup = @queuegroup and ticket = @ticket; \
                    select * from ticket where seat = @seat and call = @call and skip = @skip and date between @today and @tomorrow and queuegroup = @queuegroup and ticket = @ticket;",
                "args": [
                    { "c": "seat", "t": TYPES.Bit, "v": updates.seat },
                    { "c": "call", "t": TYPES.Bit, "v": updates.call },
                    { "c": "skip", "t": TYPES.Bit, "v": updates.skip },
                    { "c": "today", "t": TYPES.DateTime, "v": today },
                    { "c": "tomorrow", "t": TYPES.DateTime, "v": tomorrow },
                    { "c": "queuegroup", "t": TYPES.Int, "v": args.group },
                    { "c": "ticket", "t": TYPES.Int, "v": args.ticketNo }
                ]
            }
            this.DM.executeStatement(query, (err, tickets) => {
                var ticket = tickets[0];
                console.log("args=======================> for modifyTicketStatus");
                console.log(args);

                var output = {
                    "group": args.group,
                    "ticket_prefix": queueConfig.queuetype[args.group].prefix,
                    "no_of_people": args.ticketPeople,
                    "ticketnumber": args.ticketNo,
                    "modify_from": "",
                    "modify_to": args.ticketStatus,
                    "queue_action": queue_action["modifyTicketStatus"],
                    "queue_action_datetime": current_datetime(),
                    "remote_queue_id": ticket.remotequeueid
                };

                var postData = QueryString.stringify({
                    'shopcode': queueConfig.shopcode,
                    'queue': JSON.stringify(output), // queue action information, the service.cs receive a jsonArray
                    'action': 'writeLog'
                });
                postToServer(postData, null, function (chunk) { });

                this.showQueueingList(args, callback, "modifyTicketStatus");
            });
        }

        //function seatedTicket(qs, callback)
        this.seatedTicket = (args, callback) => {
            var today = getToday(),
            tomorrow = getTomorrow(),
            now = new Date();
            var query = {
                "sql": "update ticket set seat = 1, seattime = @seattime where date between @today and @tomorrow and queuegroup = @queuegroup and ticket = @ticket; \
                    select * from ticket where seat = 1 and seattime = @seattime and date between @today and @tomorrow and queuegroup = @queuegroup and ticket = @ticket;",
                "args": [
                    { "c": "seattime", "t": TYPES.DateTime, "v": now },
                    { "c": "today", "t": TYPES.DateTime, "v": today },
                    { "c": "tomorrow", "t": TYPES.DateTime, "v": tomorrow },
                    { "c": "queuegroup", "t": TYPES.Int, "v": args.group },
                    { "c": "ticket", "t": TYPES.Int, "v": args.ticket_no }
                ]
            };
            this.DM.executeStatement(query, (err, tickets) => {
                var ticket = tickets[0];
                console.log("qs, ticket=======================> for seatedTicket");
                console.log(args, ticket);
                var output = {
                    "group": args.group,
                    "ticket_prefix": queueConfig.queuetype[args.group].prefix,
                    "no_of_people": args.ticket_people,
                    "ticketnumber": args.ticket_no,
                    "modify_from": "",
                    "modify_to": "",
                    "queue_action": queue_action["seatedTicket"],
                    "queue_action_datetime": current_datetime(),
                    "remote_queue_id": ticket.remotequeueid
                };

                var postAction = 'writeLog';
                if (queueConfig.queuetype[args.group].fastpass == 1 && ticket.remotequeueid != -1) {
                    postAction += ', UpdateRemoteQueue';
                    var path = writeFasspastDeductLog(JSON.stringify(output));
                }
                var postData = QueryString.stringify({
                    'shopcode': queueConfig.shopcode,
                    'queue': JSON.stringify(output), // queue action information, the service.cs receive a jsonArray
                    'action': postAction
                });
                postToServer(postData, null, function (chunk) {
                    var postResponse = JSON.parse(chunk),
                        feedback = {};
                    if (postResponse.UpdateRemoteQueue == undefined) return;

                    if (postResponse.UpdateRemoteQueue.separateQueueServer) {
                        feedback = postResponse.UpdateRemoteQueue.deductPostBack.feedback;
                    }
                    else {
                        feedback = postResponse.UpdateRemoteQueue.feedback;
                    }
                    console.log('postResponse feedback', feedback);
                    if (feedback.status == 0) {
                        fs.unlink(path, function (err) {
                            if (err)
                                console.log('unable to delete ' + path, err);
                            else
                                console.log('successfully deleted ' + path);
                        });
                    }
                });

                this.showQueueingList(args, callback, "seatedTicket");
            });
        }

        //function skipTicket(qs, callback)
        this.skipTicket = (args, callback) => {
            var today = getToday(),
            tomorrow = getTomorrow(),
            now = new Date();
            var query = {
                "sql": "update ticket set skip = 1, skiptime = @skiptime where date between @today and @tomorrow and queuegroup = @queuegroup and ticket = @ticket; \
                    select * from ticket where skip = 1 and skiptime = @skiptime and date between @today and @tomorrow and queuegroup = @queuegroup and ticket = @ticket;",
                "args": [
                    { "c": "skiptime", "t": TYPES.DateTime, "v": now },
                    { "c": "today", "t": TYPES.DateTime, "v": today },
                    { "c": "tomorrow", "t": TYPES.DateTime, "v": tomorrow },
                    { "c": "queuegroup", "t": TYPES.Int, "v": args.group },
                    { "c": "ticket", "t": TYPES.Int, "v": args.ticket_no }
                ]
            };
            this.DM.executeStatement(query, (err, tickets) => {
                var ticket = tickets[0];
                console.log("args=======================> for skipTicket");
                console.log(args);
                if (ticket == null) {
                    callback("ticketnotexist");
                }
                else {
                    var output = {
                        "group": args.group,
                        "ticket_prefix": queueConfig.queuetype[args.group].prefix,
                        "no_of_people": args.ticket_people,
                        "ticketnumber": args.ticket_no,
                        "modify_from": "",
                        "modify_to": "",
                        "queue_action": queue_action["skipTicket"],
                        "queue_action_datetime": current_datetime(),
                        "remote_queue_id": ticket.remotequeueid
                    };

                    var postData = QueryString.stringify({
                        'shopcode': queueConfig.shopcode,
                        'queue': JSON.stringify(output), // queue action information, the service.cs receive a jsonArray
                        'action': 'writeLog, UpdateRemoteQueue'
                    });
                    postToServer(postData, null, function (chunk) { });

                    this.showQueueingList(args, callback, "skipTicket");
                }
            });
        }

        //function callTicket(qs, callback)
        this.callTicket = (args, callback) => {
            console.log("==================================");
            console.log("call ticket");
            console.log("==================================");
            console.log(args);
            var today = getToday(),
            tomorrow = getTomorrow(),
            now = new Date();
            var query = {
                "sql": "update ticket set call = 1, calltime = @calltime where date between @today and @tomorrow and queuegroup = @queuegroup and ticket = @ticket; \
                    update queue set currentno = @currentno where date between @today and @tomorrow and queuegroup = @queuegroup; \
                    select * from ticket where call = 1 and calltime = @calltime and date between @today and @tomorrow and queuegroup = @queuegroup and ticket = @ticket;",
                "args": [
                    { "c": "calltime", "t": TYPES.DateTime, "v": now },
                    { "c": "today", "t": TYPES.DateTime, "v": today },
                    { "c": "tomorrow", "t": TYPES.DateTime, "v": tomorrow },
                    { "c": "queuegroup", "t": TYPES.Int, "v": args.group },
                    { "c": "ticket", "t": TYPES.Int, "v": args.ticket_no },
                    { "c": "currentno", "t": TYPES.Int, "v": args.ticket_no }
                ]
            };
            this.DM.executeStatement(query, (err, tickets) => {
                var ticket = tickets[0];
                var qt = 0;
                for (var i = 0; i < queueConfig.queuetype.length; i++) {
                    if (args.ticket_people >= queueConfig.queuetype[i].min && (args.ticket_people <= queueConfig.queuetype[i].max || queueConfig.queuetype[i].max == -1)) {
                        qt = queueConfig.queuetype[i].min;
                        break;
                    }
                }
                console.log("args, ticket=======================> for callTicket");
                console.log(args, ticket);
                var output = {
                    "group": args.group,
                    "ticket_prefix": queueConfig.queuetype[args.group].prefix,
                    "no_of_people": args.ticket_people,
                    "ticketnumber": args.ticket_no,
                    "modify_from": "",
                    "modify_to": "",
                    "queue_action": queue_action["callTicket"],
                    "queue_action_datetime": current_datetime(),
                    "fullTicketNo": queueConfig.queuetype[args.group].prefix + args.ticket_no,
                    "delay": args.delay,
                    "remote_queue_id": ticket.remotequeueid
                };
                callingList.push(output);
                this.broadcastCallingNumber();
                var postData = QueryString.stringify({
                    'shopcode': queueConfig.shopcode,
                    'queue': JSON.stringify(output), // queue action information, the service.cs receive a jsonArray
                    'action': 'updateQueue, writeLog, UpdateRemoteQueue'
                });
                postToServer(postData, null, function (chunk) { });
                this.showQueueingList(args, callback, "callTicket");
            });
        }

        //function callbroadcastCallingNumber() {
        this.callbroadcastCallingNumber = () => {
            isboardcastingCallingList = false;
            this.broadcastCallingNumber();
        }

        //function broadcastCallingNumber() {
        this.broadcastCallingNumber = () => {
            if (queueConfig.queueInterruptable) {
                isboardcastingCallingList = false;
            }
            if (!isboardcastingCallingList) {
                if (callingList.length != 0) {
                    isboardcastingCallingList = true;
                    var c = callingList.shift();
                    console.log("callingList: ", callingList);
                    console.log("c:", c);
                    var obj = {
                        "action": "playSound",
                        "fullTicketNo": c.fullTicketNo
                    };
                    //mqttClient.publish('wo/gq/client', JSON.stringify(obj));
                    mqttClient.publish('wo/gq/all', JSON.stringify(obj));
                    mqttClient.publish('wo/gs/all', JSON.stringify(obj));
                    // var delay2 = (parseInt(config.dingTongTimeInterval) + (parseInt(config.numberTimeInterval) * c.ticketnumber.length) ) * parseInt(config.CallNumber);
                    // c.delay2=delay2;
                    io.sockets.emit('shopdisplay', {
                        'payload': [c]
                    });
                    if (!queueConfig.queueInterruptable) {
                        setTimeout(callbroadcastCallingNumber, c.delay);
                    }
                }
            }
        }

        //function requestTicket(qs, callback, isCallClientToPrint)
        this.requestTicket = (args, callback, isPrint) => {
            console.log('requestTicket')
            var today = getToday(),
            tomorrow = getTomorrow();
            var qt = 1,
            queuetype = 0;
            if (args.fastpass == undefined) args.fastpass = 0;
            if (args.group != undefined) {
                queuetype = args.group;
                qt = args.ppl;
            }
            else {
                for (var i = 0; i < queueConfig.queuetype.length; i++) {
                    if (args.ppl >= queueConfig.queuetype[i].min
                        && (args.ppl <= queueConfig.queuetype[i].max || queueConfig.queuetype[i].max == -1)
                        && (queueConfig.queuetype[i].fastpass != undefined && args.fastpass == queueConfig.queuetype[i].fastpass)) {
                        queuetype = i;
                        qt = queueConfig.queuetype[i].min;
                        break;
                    }
                }
            }
            var query = {
                "sql": "select * from queue where date between @today and @tomorrow and queuegroup = @queuegroup order by date asc",
                "args": [
                    { "c": "today", "t": TYPES.DateTime, "v": today },
                    { "c": "tomorrow", "t": TYPES.DateTime, "v": tomorrow },
                    { "c": "queuegroup", "t": TYPES.Int, "v": queuetype }
                ]
            };
            this.DM.executeStatement(query, (err, rows) => {
                console.log(queuetype);
                var _insertTicket = (queueno, currentno) => {
                    console.log("args=======================> for requestTicket");
                    console.log(args);

                    var now = new Date();
                    var query4 = {
                        "sql": "insert into ticket (ticket, pax, date, specialrequest, queuegroup, remotequeueid) values (@ticket, @pax, @date, @specialrequest, @queuegroup, @remotequeueid)",
                        "args": [
                            { "c": "ticket", "t": TYPES.Int, "v": queueno },
                            { "c": "pax", "t": TYPES.Int, "v": args.ppl },
                            { "c": "date", "t": TYPES.DateTime, "v": now },
                            { "c": "specialrequest", "t": TYPES.VarChar, "v": args.specialrequest },
                            { "c": "queuegroup", "t": TYPES.Int, "v": queuetype },
                            { "c": "remotequeueid", "t": TYPES.Int, "v": args.remotequeueid == undefined ? -1 : args.remotequeueid }
                        ]
                    };
                    RAS.DM.executeStatement(query4, (err, result) => {
                        if (err) {
                            console.log(error);
                        }
                        var obj = {
                            "date": getPrintDate(now),
                            "time": now.toTimeString().split(' ')[0].substr(0, 8),
                            "response": "success",
                            "ppl": args.ppl,
                            "ticket_prefix": queueConfig.queuetype[queuetype].prefix,
                            "shopAddress": queueConfig.shopaddress,
                            "group": queuetype
                        };
                        obj["number" + queuetype] = queueno;
                        obj["ticketnumber"] = queueno;

                        var output = {
                            "group": queuetype,
                            "queueno": queueno,
                            "no_of_people": args.ppl,
                            "ticket_prefix": queueConfig.queuetype[queuetype].prefix,
                            "ticketnumber": currentno,
                            "modify_from": "",
                            "modify_to": "",
                            "queue_action": queue_action["requestTicket"],
                            "queue_action_datetime": current_datetime(),
                            "remote_queue_id": args.remotequeueid == undefined ? -1 : args.remotequeueid
                        };

                        var postData = QueryString.stringify({
                            'shopcode': queueConfig.shopcode,
                            'queue': JSON.stringify(output),
                            'action': 'updateQueue, writeLog, UpdateRemoteQueue'
                        });
                        postToServer(postData, null, function (chunk) { });

                        callback(JSON.stringify(obj));

                        if (isPrint) {
                            var _obj = {
                                "time": now.toTimeString().split(' ')[0].substr(0, 8),
                                "ppl": args.ppl,
                                "queuegroup": queuetype,
                                "ticketNo": queueno
                            };
                            this.reprint(_obj);
                        }

                    });
                };

                if (rows.length == 0) {
                    var query2 = {
                        "sql": "insert into queue (queuegroup, currentno, queue, date) values (@queuegroup, 0, 1, @date)",
                        "args": [
                            { "c": "queuegroup", "t": TYPES.Int, "v": queuetype },
                            { "c": "date", "t": TYPES.DateTime, "v": today }
                        ]
                    };
                    this.DM.executeStatement(query2, (err, result) => {
                        if (err) {
                            console.log(err);
                        }
                        _insertTicket(1, 0);
                    });
                } else {
                    var query3 = {
                        "sql": "update queue set queue = queue + 1 where date between @today and @tomorrow and queuegroup = @queuegroup",
                        "args": [
                            { "c": "today", "t": TYPES.DateTime, "v": today },
                            { "c": "tomorrow", "t": TYPES.DateTime, "v": tomorrow },
                            { "c": "queuegroup", "t": TYPES.Int, "v": queuetype }
                        ]
                    };
                    this.DM.executeStatement(query3, (err, result) => {
                        _insertTicket(rows[0].queue + 1, rows[0].currentno);
                    });
                }
            });
        }

        //function refreshshopdisplay(data)
        this.refreshshopdisplay = (data) => {
            console.log('refreshshopdisplay', data);
            var today = getToday(),
            tomorrow = getTomorrow();
            var query = {
                "sql": "select * from queue where date between @today and @tomorrow",
                "args": [
                    { "c": "today", "t": TYPES.DateTime, "v": today },
                    { "c": "tomorrow", "t": TYPES.DateTime, "v": tomorrow }
                ]
            };
            this.DM.executeStatement(query, (err, queues) => {
                console.log(queues);
                var queueinfo = [];
                for (var i = 0; i < queueConfig.queuetype.length; i++) {
                    var found = false;
                    for (var j = 0; j < queues.length; j++) {
                        if (queues[j].queuegroup == i) {
                            queueinfo.push({
                                "group": i,
                                "ticketnumber": queues[j].currentno == 0 ? "-" : queueConfig.queuetype[i].prefix + queues[j].currentno
                            });
                            found = true;
                        }
                    }
                    if (!found) {
                        queueinfo.push({
                            "group": i,
                            "ticketnumber": "-"
                        });
                    }
                }
                var obj = {
                    "action": "startPanel",
                    "queues": queueinfo
                };
                mqttClient.publish('wo/gq/equeuing', JSON.stringify(obj));
                io.sockets.emit('initshopdisplay', {
                    'payload': queueinfo
                });
            });
        }

        //function updateQueue
        this.updateQueue = () => {
            var today = getToday(),
            tomorrow = getTomorrow();
            var query = {
                "sql": "select * from queue where date between @today and @tomorrow",
                "args": [
                    { "c": "today", "t": TYPES.DateTime, "v": today },
                    { "c": "tomorrow", "t": TYPES.DateTime, "v": tomorrow }
                ]
            };
            this.DM.executeStatement(query, (err, queues) => {
                console.log(queues);
                var queueinfo = [];
                for (var i = 0; i < queueConfig.queuetype.length; i++) {
                    var found = false;
                    for (var j = 0; j < queues.length; j++) {
                        if (queues[j].queuegroup == i) {
                            queueinfo.push({
                                "group": i,
                                "ticketnumber": queues[j].currentno == 0 ? "-" : queues[j].currentno,
                                "queueno": queues[j].queue
                            });
                            found = true;
                        }
                    }
                    if (!found) {
                        queueinfo.push({
                            "group": i,
                            "ticketnumber": "-",
                            "queueno": 0
                        });
                    }
                }
                var postData = QueryString.stringify({
                    'shopcode': queueConfig.shopcode,
                    'queue': JSON.stringify(queueinfo),
                    'action': 'updatequeue'
                });
                postToServer(postData, null, function (chunk) { });
            });
        }

        //function cronUploadConfig() {
        this.cronUploadConfig = () => {
            var postData = QueryString.stringify({
                'shopcode': queueConfig.shopcode,
                'config': JSON.stringify(queueConfig),
                'action': 'updateconfig'
            });
            postToServer(postData, null, function (chunk) { });
        }

        //function updateQueueInfoFile(obj) {
        this.updateQueueInfoFile = (obj) => {
            var colspan = queueConfig.queuetype.length * 6; // 6 colnums for 1 queue group
            var content = "<table class=\"t\"><tr><td colspan='" + colspan + "' style='width:100%'>";
            for (var i = 0; i < queueConfig.specialrequest.length; i++) {
                if (i != 0) content += ",";
                content += i + "-" + queueConfig.specialrequest[i];
            }
            content += "</td></tr><tr>";
            for (var i = 0; i < queueConfig.queuetype.length; i++) {
                content += "<td>" + queueConfig.queuetype[i].prefix + "</td><td>Pax</td><td>Called</td><td>Time</td><td>SR</td><td></td>";
            }
            content += "</tr>"
            var longestqueue = 0;
            for (var i = 0; i < queueConfig.queuetype.length; i++) {
                if (obj["table" + i].length > longestqueue) longestqueue = obj["table" + i].length;
            }
            for (var i = 0; i < longestqueue; i++) {
                content += "<tr><td>";
                for (var j = 0; j < queueConfig.queuetype.length; j++) {
                    if (obj["table" + j][i]) {
                        var groupId = obj["table" + j][i].group
                        var groupPrefix = queueConfig.queuetype[groupId].prefix;
                        var called = obj["table" + j][i].called ? "Yes" : "No";
                        content += groupPrefix + obj["table" + j][i].ticket + " </td><td>" + obj["table" + j][i].pax + "</td><td>" + called + "</td><td>" + obj["table" + j][i].timestamp + "</td><td>" + obj["table" + j][i].specialrequest + "</td><td></td>";
                    } else content += "</td><td></td><td></td><td></td><td></td><td></td>";
                    if (j < queueConfig.queuetype.length - 1) content += "</td><td>";
                    else content += "</tr><tr>";
                }
            }
            var html = "<HTML><head><title>Queue Info</title><style>body{} .t{border-collapse:collapse;} .t tr td{text-align:center;width:50px;border: 1px solid black;}</style></head><body>" + content + "</body></HTML>";
            fs.writeFile(queueConfig.queueinfofile, html, function (err) {
                if (err) {
                    console.log("queueinfofile:" + err);
                } else {
                    //console.log("The file was saved!");
                }
            });
        }

        //function initDB(devMode, callback) {
        this.initDB = (devMode, callback) => {
            if (devMode == undefined) devMode = false;
            if (!devMode) {
                callback("The function cannot be use on production!")
                return;
            }
            var today = getToday(),
                tommorow = getTomorrow();
            var query = {
                "sql": "delete from queue where date between @today and @tomorrow; \
                    delete from ticket where date between @today and @tomorrow;",
                "args": [
                    { "c": "today", "t": TYPES.DateTime, "v": today },
                    { "c": "tomorrow", "t": TYPES.DateTime, "v": tomorrow }
                ]
            }
            this.DM.executeStatement(query, (err, result) => {
                console.log('Queue collection removed');
                console.log('Ticket collection removed');
                this.updateQueue();
                var output = {
                    "group": "",
                    "ticket_prefix": "",
                    "no_of_people": -1,
                    "ticketnumber": -1,
                    "modify_from": "",
                    "modify_to": "",
                    "queue_action": queue_action["skipAllTicket"],
                    "queue_action_datetime": current_datetime()
                };

                var postData = QueryString.stringify({
                    'shopcode': config.shopcode,
                    'queue': JSON.stringify(output), // queue action information, the service.cs receive a jsonArray
                    'action': 'writeLog, UpdateRemoteQueue'
                });
                postToServer(postData, null, function (chunk) { });

                this.showQueueingList(null, null, null);
                this.refreshshopdisplay({ topic: 'allticketno' });
                callback("initdb finished");
            });
        }
    }

    this.GoQueue = new goQueue(this.DM);

    this.changeSettings = (data, callback) => {
        console.log(JSON.stringify(data));
        config.useKds = data.useKds == undefined ? config.useKds : data.useKds;
        config.billPrintTwice = data.billPrintTwice == undefined ? config.billPrintTwice : data.billPrintTwice;
        config.paymentPrintTwice = data.paymentPrintTwice == undefined ? config.paymentPrintTwice : data.paymentPrintTwice;
        config.takeAwayMode = data.takeAwayMode == undefined ? config.takeAwayMode : data.takeAwayMode;
        config.takeAwayPrintTwice = data.takeAwayPrintTwice == undefined ? config.takeAwayPrintTwice : data.takeAwayPrintTwice;
        console.log(JSON.stringify(config));
        fs.writeFile("./config/ras.json", JSON.stringify(config, null, 4), function (err) {
            if (err) {
                console.log(err);
            } else {
                //console.log("The file was saved!");
            }
        });
        callback({ result: 'OK' })
    }


    this.loadSettings = (data, callback) => {
        config.serverTime = new Date();
        callback(config)
    }

    this.changeCategorySetting = (data, callback) => {
        var category = JSON.stringify(data, null, 4);
        io.sockets.emit('refreshCategory', category);
        fs.writeFile("./public/data/" + data.filename + ".json", category, function (err) {
            if (err) {
                console.log(err);
            } else {
                //console.log("The file was saved!");
            }
        });
        callback("OK")
    }

    this.ResetCategorySetting = (data, callback) => {
        fs.writeFile("./public/data/" + data.filename + ".json", JSON.stringify({ 'category': [], 'filename': data.filename }), function (err) {
            if (err) {
                console.log(err);
            } else {
                //console.log("The file was saved!");
            }
        });
        callback("OK")
    }

    this.getTableCodeByCardId = (data, callback) => {
        var query = {
            "sql": "select tableCode, tableSubCode from tableOrder where cardId = @cardId and status != 4",
            "args": [
                    { "c": "cardId", "t": TYPES.VarChar, "v": data.cardId },
            ]
        }

        this.DM.executeStatement(query, (err, result) => {
            if (err == null) callback(result);
        });
    }

    this.OctopusDineinPayment = (args, callback) => {
        try {
            console.log("OctopusDineinPayment", args);
            /*
            this.DeductOctopus({ totalPrice: 0.1, invoice: '123' }, (result) => {
                if (result.octopusCode <= 100000) {

                    //this.PrintPaymentOctopus({ refNo: '123', printer: "o1", octopus: result }, () => {

                    //});

                }
                callback(result);
            });
            */
        } catch (e) {
            console.log(e);
        }
    }
    //goMenu Octopus
    this.SaveTakeoutOrder = (args, callback) => {
        // var tableObj = this.getTakeoutCode();
        if (!preventMulitpleOctopus) {
            preventMulitpleOctopus = true;
            if (!(args.till && args.till.$value)) {
                args.till = { '$value': "k_1" };
            }
            var printingArray = { TableNo: "外賣", Pax: 1, FoodItem: [] };
            //var tableCode = "takeout";
            //var tableSubCode;
            //var seq;
            //            var query = {
            //                "sql": "declare @seq int EXEC @seq = spGetNextSeq 'takeout' \
            //declare @sseq varchar(10) = REPLICATE('0',6-LEN(RTRIM(@seq))) + RTRIM(@seq) \
            //                    select 'tableSubCode' = @refNo + @sseq, seq = @seq;",
            //                "args": [{
            //                    "c": "refNo", "t": TYPES.VarChar, "v": config.shopCode + "-TO" + moment().format("-YYMMDD-")
            //                }]
            //            };
            args.serviceCharge = false;

            async.waterfall([
                //(callback) => {
                //    this.DM.executeStatement(query, (err, result) => {
                //        console.log("SaveTakeoutOrder query", result)
                //        tableSubCode = result[0].tableSubCode;
                //        seq = result[0].seq;
                //        var tableObj = { tableCode: tableCode, tableSubCode: tableSubCode };

                //        args.tableObj = { tableCode: "", tableSubCode: "" };
                //        args.tableObj.tableCode = tableObj.tableCode;
                //        args.tableObj.tableSubCode = tableObj.tableSubCode;
                //        args.tableNum.$value = tableObj.tableCode + "_" + tableObj.tableSubCode;
                //        args.serviceCharge = false;
                //        args.seq = seq;
                //        callback(null, args);
                //    })
                //},
                //(args, callback) => {
                (callback) => {
                    //this.SaveTableOrder(args, (_result) => {
                    //    if (_result.result != 'OK') callback(_result);
                    //    console.log("SaveTakeoutOrder 5", _result);
                    //    var _o = {
                    //        "totalPrice": _result.totalPrice,
                    //        "invoice": args.tableObj.tableSubCode,
                    //        "invoiceSeq": args.seq
                    //    };
                    //    callback(null, _o);
                    //});
                    var r = args.itemData.json;
                    var orderstring = "";
                    var total = 0;
                    var date = new Date();
                    var orderid = moment().format("YYMMDDHHmmssSSSS");
                    var itemOrderTime = moment().format("HH:mm:ss");

                    var checkStockArray = [];
                    var commitItemStockArray = [];
                    var callbackObj = {};
                    var vipcode, surname, givenname, vipcenter, currgrade, dob_yyyy, dob_mm, dob_dd, orderContent;
                    var staff = args.user == undefined ? "user" : args.user.$value;
                    vipcode = surname = givenname = vipcenter = currgrade = dob_yyyy = dob_mm = dob_dd = orderContent = '';

                    try {
                        for (var i = 0 ; i < r.order.item.length; i++) {
                            //if (r.order.item[i].qty[0] > 0 || r.order.item[i].qty[0] < 0) {
                            if (r.order.item[i].qty[0] > 0) {
                                checkStockArray.push({
                                    sql: "select itemCode,qty,@dname as \"name\" from itemControl where itemCode = @itemCode and (qty < 0 or qty < @qty)"
                                    , args: [{ c: 'dname', t: TYPES.NVarChar, v: r.order.item[i].desc1[0] }, { c: 'itemCode', t: TYPES.VarChar, v: r.order.item[i].code[0] }, { c: 'qty', t: TYPES.Int, v: r.order.item[i].qty[0] }]
                                })
                                commitItemStockArray.push({ sql: "update itemControl set qty = qty - @qty where itemCode = @itemCode", args: [{ c: 'itemCode', t: TYPES.VarChar, v: r.order.item[i].code[0] }, { c: 'qty', t: TYPES.Int, v: r.order.item[i].qty[0] }] });
                            }
                        }
                    } catch (e) {
                        //console.log(e);
                    }
                    this.DM.executeStatement(checkStockArray, (err, result) => {
                        if (err != undefined) {
                            //console.log(err);
                        }
                        //console.log(JSON.stringify(result));
                        var isValid = true;
                        var errorMsg = ""; var outOfStockList = [];
                        for (var i = 0; i < result.length; i++) {
                            if (result[i].length != 0) {
                                isValid = false;
                                outOfStockList.push(result[i][0].itemCode);
                            }
                        }
                        if (isValid) {
                            //var query = [
                            //    { "sql": "select max(round) as round, max(seq) as seq from tableorderd where tableCode = @tableCode ;", "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": tableObj.tableCode }] },
                            //    { "sql": "select max(pax) as pax from tableOrder where tableCode = @tableCode and tableSubCode = @tableSubCode;", "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": tableObj.tableCode }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": tableObj.tableSubCode }] }, {
                            //        "sql": "select * from tableorderd where tableCode = @tableCode and tableSubCode = @tableSubCode", "args": [
                            //        { "c": "tableCode", "t": TYPES.VarChar, "v": tableObj.tableCode },
                            //        { "c": "tableSubCode", "t": TYPES.VarChar, "v": tableObj.tableSubCode }]
                            //    }];

                            //this.DM.executeStatement(query, (err, tableOrderD) => {

                            var currentSeq = 0;
                            var currentRound = 0;
                            var query = [];
                            var _o = {};
                            var totalPrice = 0;
                            var totalPriceForServiceCharge = 0;
                            var serviceCharge = 0;
                            var lastOrder = '';
                            var lastOrderWarning = '';
                            var leaveTime = '';
                            var lastItemQty = 0;
                            ////console.log(tableOrderD);
                            //if (tableOrderD[0][0].seq != null) {
                            //    currentSeq = tableOrderD[0][0].seq;
                            //}
                            //if (tableOrderD[0][0].round != null) {
                            //    currentRound = tableOrderD[0][0].round;
                            //}
                            currentRound++;
                            var lastIsServiceCharge = true;
                            //sortDbItem(tableOrderD[2]);

                            //if (tableOrderD[2].length != 0) {
                            //    for (var i = 0; i < tableOrderD[2].length; i++) {
                            //        var orderedItem = tableOrderD[2][i];
                            //        if (orderedItem.type == "I" || orderedItem.type == "T") {
                            //            lastIsServiceCharge = orderedItem.serviceCharge;
                            //            lastItemQty = orderedItem.qty;
                            //            totalPrice += lastItemQty * orderedItem.price;

                            //            if (lastIsServiceCharge) {
                            //                totalPriceForServiceCharge += orderedItem.qty * orderedItem.price;
                            //            }


                            //            if (orderedItem.type == "T" && orderedItem.voidIndex == -1) {
                            //                var checkIsVoidItem = tableOrderD[2].filter((checkItem) => {
                            //                    return checkItem.voidIndex == orderedItem.seq;
                            //                })
                            //                if (checkIsVoidItem.length == 0) {
                            //                    checkIsVoidItem = r.order.item.filter((checkItem) => {
                            //                        if (checkItem.voidIndex == undefined) {
                            //                            return false;
                            //                        }
                            //                        return checkItem.voidIndex[0] == orderedItem.seq;
                            //                    })
                            //                    if (checkIsVoidItem.length == 0) {
                            //                        var items = RAS.Item.filter(function (entry) {
                            //                            return entry.code == orderedItem.itemId;
                            //                        });
                            //                        var _lastOrder = items[0].namek.split(',');
                            //                        if (_lastOrder.length > 1) {
                            //                            lastOrderWarning = _lastOrder[0];
                            //                            lastOrder = _lastOrder[1];
                            //                            leaveTime = _lastOrder[2];
                            //                        }
                            //                    }
                            //                }
                            //            }

                            //        } else {
                            //            totalPrice += lastItemQty * orderedItem.price * orderedItem.qty;
                            //            if (lastIsServiceCharge) {
                            //                totalPriceForServiceCharge += lastItemQty * orderedItem.price * orderedItem.qty;
                            //            }
                            //        }
                            //    }
                            //}

                            var lastPrinter = '';
                            var orderKds = null;
                            lastIsServiceCharge = true;
                            var lastKds = null;
                            for (var i = 0 ; i < r.order.item.length; i++) {
                                var vi = r.order.item[i].voidIndex == undefined ? "-1" : r.order.item[i].voidIndex[0];
                                var vr = r.order.item[i].voidRemark == undefined ? "" : r.order.item[i].voidRemark[0];
                                var approveStaff = r.order.item[i].voidApproveStaff == undefined || null ? "" : r.order.item[i].voidApproveStaff[0];
                                var type = r.order.item[i].type[0];
                                //var serviceCharge = true;
                                var itemOrderTime = moment().format("HH:mm:ss");
                                if (type != "M" && type != "K") {
                                    currentSeq += 1;
                                }
                                if (type == "T" && vi == "-1") {
                                    var items = RAS.Item.filter(function (entry) {
                                        return entry.code == r.order.item[i].code[0];
                                    });
                                    var _lastOrder = items[0].namek.split(',');
                                    if (_lastOrder.length > 1) {
                                        lastOrderWarning = _lastOrder[0];
                                        lastOrder = _lastOrder[1];
                                        leaveTime = _lastOrder[2];
                                    }
                                }

                                //console.log(currentSeq)
                                ////console.log(r.order.item[i].unitPrice[0])
                                var customName = '';
                                if (r.order.item[i].customName) {
                                    if (r.order.item[i].customName[0]) {
                                        customName = r.order.item[i].customName[0]
                                    }
                                }
                                var itemType = r.order.item[i].type[0];
                                var itemCode = r.order.item[i].code[0]
                                var items = [];

                                var items = getItemByItem({ itemId: r.order.item[i].code[0], type: r.order.item[i].type[0] });
                                var namek = "";

                                var kds = null;
                                var _sc = true;
                                if (items.length > 0) {
                                    if (items[0].serviceCharge != undefined) {
                                        _sc = items[0].serviceCharge;
                                    };
                                    if (items[0].kds) {
                                        kds = items[0].kds;
                                    };
                                    if (itemType != "M" && type != "K") {
                                        lastPrinter = items[0].printer;
                                        if (items[0].printerGroup) {
                                            lastPrinter += "*" + items[0].printerGroup;
                                        }
                                        lastKds = kds;
                                    }
                                    if (kds == null) {
                                        var vr = r.order.item[i].voidRemark == undefined ? "" : r.order.item[i].voidRemark[0];
                                        //var printer = items[0].printer == undefined ? "" : items[0].printer;
                                        //if (items[0].printerGroup) {
                                        //    printer += "*" + items[0].printerGroup;
                                        //}


                                        namek = items[0].namek;
                                        if (customName != "") {
                                            namek = customName;
                                        }

                                        printingArray.FoodItem.push({ Name: namek, Qty: r.order.item[i].qty[0], Printer: lastPrinter, Code: items[0].code, Time: itemOrderTime, Msg: vr, individualPrint: items[0].individualPrint, Type: type });
                                    }
                                }

                                if (itemType == "I") {
                                    lastIsServiceCharge = _sc;
                                    lastItemQty = parseInt(r.order.item[i].qty[0]);
                                    totalPrice += parseFloat((parseFloat(r.order.item[i].unitPrice[0]) / 100).toFixed(2)) * lastItemQty;
                                    if (_sc && lastIsServiceCharge) {
                                        totalPriceForServiceCharge += parseFloat((parseFloat(r.order.item[i].unitPrice[0]) / 100).toFixed(2)) * lastItemQty;
                                    }
                                } else {
                                    totalPrice += parseFloat((parseFloat(r.order.item[i].unitPrice[0]) / 100).toFixed(2)) * lastItemQty * r.order.item[i].qty[0];
                                    if (_sc && lastIsServiceCharge) {
                                        totalPriceForServiceCharge += parseFloat((parseFloat(r.order.item[i].unitPrice[0]) / 100).toFixed(2)) * lastItemQty * r.order.item[i].qty[0];
                                    }
                                }

                                if (kds != null) {
                                    orderKds = kds;
                                }
                                _o = {
                                    "sql": "insert into takeoutD (orderId,itemId,qty,price,voidIndex,voidRemark,type,seq,time,staffId,round,customName,approveStaffId,serviceCharge,printer,kdsgroup,kds) values (@orderId,@itemId,@qty,@price,@voidIndex,@voidRemark,@type,@seq,@time,@staffId,@round,@customName,@approveStaffId,@serviceCharge,@printer,@kdsgroup,@kds)",
                                    "args": [
                                        { "c": "orderid", "t": TYPES.VarChar, "v": orderid },
                                        { "c": "itemId", "t": TYPES.VarChar, "v": itemCode },
                                        { "c": "qty", "t": TYPES.Int, "v": parseInt(r.order.item[i].qty[0]) },
                                        { "c": "price", "t": TYPES.Decimal, "v": parseFloat((parseFloat(r.order.item[i].unitPrice[0]) / 100).toFixed(2)), "o": { "scale": 2 } },
                                        { "c": "voidIndex", "t": TYPES.Int, "v": vi },
                                        { "c": "voidRemark", "t": TYPES.NVarChar, "v": vr },
                                        { "c": "type", "t": TYPES.VarChar, "v": type },
                                        { "c": "seq", "t": TYPES.Int, "v": currentSeq },
                                        { "c": "time", "t": TYPES.Time, "v": date },
                                        { "c": "staffId", "t": TYPES.VarChar, "v": staff },
                                        { "c": "round", "t": TYPES.Int, "v": currentRound },
                                        { "c": "customName", "t": TYPES.NVarChar, "v": customName },
                                        { "c": "approveStaffId", "t": TYPES.VarChar, "v": approveStaff },
                                        { "c": "serviceCharge", "t": TYPES.Bit, "v": _sc },
                                        { "c": "printer", "t": TYPES.VarChar, "v": lastPrinter },
                                        { "c": "kdsgroup", "t": TYPES.BigInt, "v": '-1' },
                                        { "c": "kds", "t": TYPES.VarChar, "v": lastKds }
                                    ]
                                }
                                query.push(_o);
                            }

                            if (args.serviceCharge == undefined || args.serviceCharge)
                                serviceCharge = (totalPriceForServiceCharge / 10).toFixed(1);

                            //var lastOrderDate = null, lastOrderWarningDate = null, leaveTimeDate = null;
                            //if (lastOrder != "") {
                            //    lastOrderDate = new Date(moment().format("YYYY/MM/DD ") + lastOrder);
                            //    AssignLastOrderSchedule("lastOrder_" + lastOrder, lastOrder);
                            //}
                            //if (lastOrderWarning != "") {
                            //    lastOrderWarningDate = new Date(moment().format("YYYY/MM/DD ") + lastOrderWarning);
                            //    AssignLastOrderSchedule("lastOrder_" + lastOrderWarning, lastOrderWarning);
                            //}
                            //if (leaveTime != "") {
                            //    leaveTimeDate = new Date(moment().format("YYYY/MM/DD ") + leaveTime);
                            //    AssignLastOrderSchedule("lastOrder_" + leaveTime, leaveTime);
                            //}

                            _o = {
                                "sql": "declare @seq int EXEC @seq = spGetNextSeq 'takeout' ; declare @sseq varchar(10) = REPLICATE('0',6-LEN(RTRIM(@seq))) + RTRIM(@seq) ;\
insert into takeout (orderId, memberId, orderTime, staffId, price, serviceCharge, status, refNo) \
values (@orderId, @memberId, @orderTime, @staffId, @price, @serviceCharge, @status, @refNo + @sseq); \
select 'invoice' = @refNo + @sseq, 'invoiceSeq' = @seq;",
                                "args": [
                                    { "c": "orderid", "t": TYPES.VarChar, "v": orderid },
                                    //{ "c": "tableCode", "t": TYPES.VarChar, "v": tableObj.tableCode },
                                    //{ "c": "tableSubCode", "t": TYPES.VarChar, "v": tableObj.tableSubCode },
                                    //{ "c": "pax", "t": TYPES.Int, "v": noOfPeople },
                                    { "c": "memberId", "t": TYPES.Int, "v": null },
                                    { "c": "orderTime", "t": TYPES.DateTime, "v": date },
                                    { "c": "staffId", "t": TYPES.VarChar, "v": staff },
                                    { "c": "price", "t": TYPES.Decimal, "v": parseFloat(totalPrice), "o": { "scale": 2 } },
                                    { "c": "serviceCharge", "t": TYPES.Decimal, "v": parseFloat(serviceCharge), "o": { "scale": 2 } },
                                    //{ "c": "lastOrder", "t": TYPES.DateTime, "v": lastOrderDate },
                                    //{ "c": "lastOrderWarning", "t": TYPES.DateTime, "v": lastOrderWarningDate },
                                    //{ "c": "leaveTime", "t": TYPES.DateTime, "v": leaveTimeDate },
                                    { "c": "status", "t": TYPES.Int, "v": 2 },
                                    //{ "c": "refNo", "t": TYPES.VarChar, "v": config.shopCode + moment().format("YYMMDD") }
                                    { "c": "refNo", "t": TYPES.VarChar, "v": config.shopCode + "-TO" + moment().format("-YYMMDD-") }
                                ]
                            };
                            query.push(_o);
                            //if (tableOrderD[1][0].pax == null) {
                            //    query.push(_o);
                            //} else {
                            //    if (noOfPeople == 0) {
                            //        noOfPeople = tableOrderD[1][0].pax;
                            //        printingArray.Pax = noOfPeople;
                            //    }
                            //    _o = {
                            //        "sql": "update takeout set status = @status, pax = @pax, price = @price, serviceCharge = @serviceCharge, lastOrder = @lastOrder, lastOrderWarning = @lastOrderWarning, leaveTime = @leaveTime where tableCode = @tableCode and tableSubCode = @tableSubCode",
                            //        "args": [
                            //            { "c": "status", "t": TYPES.Int, "v": 2 },
                            //            { "c": "pax", "t": TYPES.Int, "v": noOfPeople },
                            //            { "c": "tableCode", "t": TYPES.VarChar, "v": tableObj.tableCode },
                            //            { "c": "tableSubCode", "t": TYPES.VarChar, "v": tableObj.tableSubCode },
                            //            { "c": "price", "t": TYPES.Decimal, "v": parseFloat(totalPrice), "o": { "scale": 2 } },
                            //            { "c": "serviceCharge", "t": TYPES.Decimal, "v": parseFloat(serviceCharge), "o": { "scale": 2 } },
                            //            { "c": "lastOrder", "t": TYPES.DateTime, "v": lastOrderDate },
                            //            { "c": "lastOrderWarning", "t": TYPES.DateTime, "v": lastOrderWarningDate },
                            //            { "c": "leaveTime", "t": TYPES.DateTime, "v": leaveTimeDate }
                            //        ]
                            //    }
                            //    query.push(_o);
                            //}

                            this.DM.executeStatement(query, (err, result) => {
                                //console.log(err);
                                console.log(result);

                                //this.SetTableStatus({ "tableCode": tableObj.tableCode, "status": 2 })
                                var invoiceSeq = result[result.length - 1][0].invoiceSeq;
                                var invoice = result[result.length - 1][0].invoice;

                                this.DM.executeStatement(commitItemStockArray, (err, result) => {
                                    //console.log(err);
                                    this.GetOutOfStock((err, result) => {
                                        io.sockets.emit('refreshItemQty', JSON.stringify(result));
                                        var _o = { "action": "refreshItemQty", "outOfStock": result }
                                        mqttClient.publish("wo/gm/all", JSON.stringify(_o));

                                        var _result = "OK";
                                        var si = [];
                                        var siq = [];
                                        result.forEach((item) => {
                                            si.push(item.code);
                                            siq.push(item.qty);
                                        });
                                        console.log("si", si);
                                        callbackObj = { result: _result, timestamp: new Date().getTime(), taskID: 1, note: '', suspendItems: si.toString(), suspendItemsQty: siq.toString(), totalPrice: totalPrice, invoiceSeq: invoiceSeq, invoice: invoice };
                                        callback(null, callbackObj);

                                        if (orderKds) {
                                            this.showKds(orderKds);
                                        }

                                    });
                                }, "parallel");

                                //if (printingArray.FoodItem.length != 0) {
                                //    //mqttClient.publish('wo/kds/p1', JSON.stringify(printingArray), { qos: 2 });
                                //    console.log("printingArray", printingArray);
                                //    fs.writeFile("./printer/" + moment().format("YYMMDDHHmmssSSSS") + ".json", JSON.stringify(printingArray), function (err) {
                                //        if (err) {
                                //            //console.log(err);
                                //        } else {
                                //            ////console.log("The file was saved!");
                                //        }
                                //    });
                                //}


                            }, 'parallel')
                            //})
                        } else {
                            this.GetOutOfStock((err, result) => {
                                var note = "<note></note>";
                                var rItemSuspendInfo = "<suspendItems>" + this.OutOfStock + "</suspendItems>";
                                if (outOfStockList.length != 0) {
                                    // note = "<note>outOfStock</note>";
                                    note = "outOfStock";
                                }
                                var si = [];
                                var siq = [];
                                result.forEach((item) => {
                                    si.push(item.code);
                                    siq.push(item.qty);
                                });
                                console.log("si", si);
                                callbackObj = { result: 'ERROR', timestamp: new Date().getTime(), taskID: 1, note: note, suspendItems: si.toString(), suspendItemsQty: siq.toString() };
                                callback(null, callbackObj);
                            })
                        }
                    }, 'parallel')

                },
                //(args, callback) => {
                //    console.log("SaveTakeoutOrder 5", args);
                //    var _o = {
                //        "totalPrice": args.totalPrice,
                //        "invoice": args.invoice,
                //        "invoiceSeq": args.invoiceSeq
                //    };
                //    callback(null, args);
                //},
                (_o, cb) => {
                    //this.DeductOctopus(args, (result) => {
                    //    console.log("DeductOctopus", result);
                    //    if (parseInt(result.octopusCode) <= 100000) {
                    //        console.log("result.octopusCode", parseInt(result.octopusCode))
                    //        //callback(result);
                    //        callback(null, result);
                    //    }
                    //    else {
                    //        callback(result);
                    //    }
                    //});
                    var params = extend2({
                        service: this.Octopus.service.Octopus_deduct,
                        till: args.till.$value,
                        invoice: args.invoice,
                        invoiceSeq: args.invoiceSeq,
                        totalPrice: args.totalPrice,
                    }, _o);
                    this.Octopus.connect(params, (result) => {
                        if (result.octopusCode <= 100000) {
                            _o.octopusBalance = result.cardBalance;
                            //remark = result.cardBalance + "|" + parseFloat(result.totalPrice).toFixed(2) + "|" + result.cardId + "|" + result.machineId;
                            //remarkList[idx] = remark;

                            cb(null, result);
                        }
                        else {

                            callback(result);
                            preventMulitpleOctopus = false;
                        }
                    });
                },
                (_o, callback) => {
                    var totalPayment = 0;
                    //var serviceCharge = parseFloat(_o.serviceCharge.$value);
                    var serviceCharge = 0.0;
                    //var remainings = parseFloat(_o.remainings.$value);
                    var remainings = 0.0;

                    var sql = "declare @orderId nvarchar(max); select @orderId = orderid from takeout where refNo = @refNo; \
insert into [transaction]  (staffId, memberId, memberName, startTime, refNo, transTime, payAmount, price, serviceCharge, remainings,orderid, till ) select staffId, memberId, memberName, orderTime, refNo, getdate(), @payAmount, @price, @serviceCharge, @remainings, orderid, @till as till from takeout where refNo = @refNo; ";
                    //var totalPrice = parseFloat(_o.price.$value);
                    var totalPrice = parseFloat(_o.totalPrice);
                    var sqlagrs = [
                        { "c": "refNo", "t": TYPES.VarChar, "v": _o.invoice },
                        { "c": "serviceCharge", "t": TYPES.Decimal, "v": serviceCharge, "o": { "scale": 2 } },
                        { "c": "remainings", "t": TYPES.Decimal, "v": remainings, "o": { "scale": 2 } }
                    ];
                    var PaymentMethod = new Array;
                    var PaymentMethodItem = new Object;
                    var paymentDate = moment().format('YYYY-MM-DD HH:mm:ss');
                    sqlagrs.push({ "c": "till", "t": TYPES.VarChar, "v": args.till.$value });
                    _o.payMethod = {};
                    console.log('cardBalance', _o.cardBalance);
                    //_o.cardBalance.split('$')[1] +"|"+
                    var remark = _o.cardBalance + "|" + parseFloat(totalPrice).toFixed(2) + "|" + _o.cardId + "|" + _o.machineId + "|" + _o.lastAddValue + "|" + _o.alertMessage;
                    _o.payMethod.$value = [{ method: "octopus", amount: parseFloat(totalPrice).toFixed(2), remark: remark }];
                    if (_o.payMethod) {
                        for (var i = 0; i < _o.payMethod.$value.length; i++) {
                            //console.log("paymentValue" + parseFloat(_o.payMethod.$value[i].amount));
                            //console.log("paymentValue" + _o.payMethod.$value[i].method);
                            var payseq = i + 1;
                            sql += "insert into tenderpayment (paymentType,paymentValue,staffId,refNo,paymentDate,void,seq,remark,till) values (@paymentType" + i.toString() + ",@paymentValue" + i.toString() + ",@staffId,@refNO,getDate(),0," + payseq + ",@remark" + i.toString() + ",@till);";
                            sqlagrs.push({ "c": "paymentType" + i.toString(), "t": TYPES.VarChar, "v": _o.payMethod.$value[i].method })
                            sqlagrs.push({ "c": "paymentValue" + i.toString(), "t": TYPES.Decimal, "v": parseFloat(_o.payMethod.$value[i].amount), "o": { "scale": 2 } })
                            sqlagrs.push({ "c": "remark" + i.toString(), "t": TYPES.NVarChar, "v": _o.payMethod.$value[i].remark ? _o.payMethod.$value[i].remark : '' })

                            sqlagrs.push({ "c": "staffId", "t": TYPES.VarChar, "v": _o.user == undefined ? "user" : _o.user.$value })

                            totalPayment += parseFloat(_o.payMethod.$value[i].amount);
                            PaymentMethodItem = new Object;
                            PaymentMethodItem.paymentType = _o.payMethod.$value[i].method;
                            PaymentMethodItem.paymentValue = parseFloat(_o.payMethod.$value[i].amount);
                            PaymentMethodItem.paymentDate = new Date();
                            PaymentMethodItem.paymentDate = paymentDate
                            PaymentMethodItem.void = 0;
                            PaymentMethodItem.seq = payseq;
                            PaymentMethod.push(PaymentMethodItem);
                        }
                    }

                    var tenderDiscount = new Array;
                    var tenderDiscountItem = new Object;

                    if (_o.discountMethod) {
                        for (var i = 0; i < _o.discountMethod.$value.length; i++) {
                            var disseq = i + 1;
                            sql += "insert into tenderdiscount (discountType,discountValue,discountCode,staffId,refNo,discountDate,void,seq,discountAmount) values (@discountType" + i.toString() + ",@discountValue" + i.toString() + ",@discountCode" + i.toString() + ",@staffId,@refNo + @sseq,getDate(),0," + payseq + ",@discountAmount" + i.toString() + ");";
                            sqlagrs.push({ "c": "discountType" + i.toString(), "t": TYPES.VarChar, "v": _o.discountMethod.$value[i].type })
                            sqlagrs.push({ "c": "discountValue" + i.toString(), "t": TYPES.Decimal, "v": parseFloat(_o.discountMethod.$value[i].price), "o": { "scale": 2 } })
                            sqlagrs.push({ "c": "discountCode" + i.toString(), "t": TYPES.VarChar, "v": _o.discountMethod.$value[i].coupon_alias })
                            sqlagrs.push({ "c": "discountAmount" + i.toString(), "t": TYPES.Decimal, "v": parseFloat(_o.discountMethod.$value[i].amount), "o": { "scale": 2 } })
                            //totalPrice += parseFloat(_o.discountMethod.$value[i].price)
                            tenderDiscountItem = new Object;
                            tenderDiscountItem.discountType = _o.discountMethod.$value[i].type;
                            tenderDiscountItem.discountValue = parseFloat(_o.discountMethod.$value[i].price);
                            tenderDiscountItem.discountCode = _o.discountMethod.$value[i].coupon_alias;
                            tenderDiscountItem.discountDate = paymentDate;
                            tenderDiscountItem.void = 0;
                            tenderDiscountItem.seq = payseq;
                            tenderDiscountItem.discountAmount = parseFloat(_o.discountMethod.$value[i].amount);
                            tenderDiscount.push(tenderDiscountItem);
                        }
                    }

                    sqlagrs.push({ "c": "price", "t": TYPES.Decimal, "v": totalPrice, "o": { "scale": 2 } });
                    sqlagrs.push({ "c": "payAmount", "t": TYPES.Decimal, "v": totalPayment, "o": { "scale": 2 } });
                    sql += "\
insert into transactionD (itemId, qty, price, discount, voidIndex, voidRemark, time, staffId, type, seq, round, refNo, customName, approveStaffId,serviceCharge ) select itemId, qty, price, discount, voidIndex, voidRemark, time, staffId, type, seq, round, @refNo, customName, approveStaffId,serviceCharge from takeoutD where orderId = @orderId; \
delete takeoutD where orderId = @orderId and not exists (select * from takeout where refNo = @refNo); \
delete takeoutdiscount where orderId = @orderId and not exists (select * from takeout where refNo = @refNo);\
delete takeout where refNo = @refNo and not exists (select * from takeout where refNo = @refNo); \
select 'refNo' = @refNo";
                    //commit tran end try begin catch rollback tran; THROW; end catch 
                    var query = [
                        //{
                        //    "sql": "select * from tableOrderD where tableCode = @tableCode and tableSubCode = @tableSubCode;", "_o": [{ "c": "tableCode", "t": TYPES.VarChar, "v": tableCode }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": tableSubCode }]
                        //},
                        //{ "sql": "select * from tableOrder where tableCode = @tableCode and tableSubCode = @tableSubCode;", "_o": [{ "c": "tableCode", "t": TYPES.VarChar, "v": tableCode }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": tableSubCode }] }
                        //,
                        { "sql": sql, "args": sqlagrs }];

                    console.log("save takeout to transaction", sql);


                    this.DM.executeStatement(query, (err, result) => {
                        console.log(result);
                        var refNo = "";
                        if (err) {
                            console.log('takeout save to transaction', err);
                        }
                        if (err == null) {

                            //AzureObject.TransactionD = result[0];
                            //AzureObject.TableOrder = result[1];
                            ////console.log(err)
                            //refNo = result[2][0].refNo;
                            refNo = result[0][0].refNo;
                            //AzureObject.refNo = result[2][0].refNo;
                            //AzureSocket.emit('AddBill', JSON.stringify(AzureObject));

                            //this.PrintPaymentBill({ 'refNo': refNo, openCashDrawer: true, printer: args.printer.$value }, () => {

                            //})
                            //this.LoadTableSchema(null, (err, result) => {
                            //    io.sockets.emit('refreshTable', JSON.stringify(result));
                            //})
                            //this.SetTableStatus({ "tableCode": tableCode, "status": 0 })
                        }
                        //if (refNo != "") {
                        //    var temp = tableCode;
                        //    if (tableSubCode != "") {
                        //        temp += "_" + tableSubCode;
                        //    }
                        //    //'publish', { topic: "wo/gm/all", msg: "{\"action\":\"unlinkTable\",\"tableno\":\"" + $('#tno').val() + "\"}" 
                        //    mqttClient.publish("wo/gm/table/" + temp + "/", JSON.stringify({ "action": "unlinkTable", "tableno": temp }), { qos: 2 });
                        //    mqttClient.publish("wo/kds/all/", JSON.stringify({ "action": "reload" }), { qos: 2 });
                        //    //mqttClient.publish("wo/gm/tabletest/test" + temp + "/", JSON.stringify({ "action": "stressLoadOrder" }), { qos: 2 });
                        //    //mqttClient.publish("wo/gm/all", JSON.stringify({ "action": "stressLoadOrder" }), { qos: 2 });
                        //}

                        //callback({ "result": refNo == "" ? "fail" : "OK", "refNo": refNo })
                        //this.PrintPaymentOctopus({ refNo: tableSubCode, printer: args.printer.$value });
                        this.PrintPaymentBill({ refNo: refNo, printer: config.defaultQueuePrinter, octopus: args, till: args.till.$value }, () => {

                            if (printingArray.FoodItem.length != 0) {
                                //mqttClient.publish('wo/kds/p1', JSON.stringify(printingArray), { qos: 2 });
                                console.log(JSON.stringify(_o));
                                printingArray.TableNo = parseInt(S(_o.invoice).substr(_o.invoice.lastIndexOf('-') + 1).s).toString();
                                printingArray.isTakeAway = true;
                                console.log("printingArray", printingArray);
                                this.PrintFoodItem(printingArray);
                                //fs.writeFile("./printer/" + moment().format("YYMMDDHHmmssSSSS") + ".json", JSON.stringify(printingArray), function (err) {
                                //    if (err) {
                                //        //console.log(err);
                                //    } else {
                                //        ////console.log("The file was saved!");
                                //    }
                                //});
                            }

                        });
                        /*this.PrintPaymentOctopus({ refNo: refNo, printer: "o1", octopus: _o }, () => {

                            if (printingArray.FoodItem.length != 0) {
                                //mqttClient.publish('wo/kds/p1', JSON.stringify(printingArray), { qos: 2 });
                                console.log(JSON.stringify(_o));
                                printingArray.TableNo = parseInt(S(_o.invoice).substr(_o.invoice.lastIndexOf('-') + 1).s).toString();
                                printingArray.isTakeAway = true;
                                console.log("printingArray", printingArray);
                                this.PrintFoodItem(printingArray);
                                //fs.writeFile("./printer/" + moment().format("YYMMDDHHmmssSSSS") + ".json", JSON.stringify(printingArray), function (err) {
                                //    if (err) {
                                //        //console.log(err);
                                //    } else {
                                //        ////console.log("The file was saved!");
                                //    }
                                //});
                            }

                        });*/
                        callback(_o);
                    }, 'series')
                }
            ], (result) => {
                result.payMethod = null;
                console.log(result);
                callback(result);
                preventMulitpleOctopus = false;
            });
        }
    }
    /*
    poll
    100001 if R/W not connected; 
    100005 if invalid response from R/W; 
    100016 if card read error; 
    100017 if card write error; 
    100019 if card is blocked; 
    100023 if transaction log full; 
    100024 if card is blocked by this call. In this case, poll data contains the UD; 
    100032 if no card present; 
    100034 if card authentication error; 
    100035 if card recover error.

    deduct
    100001 if R/w not connected; 
    100003 if invalid parameter: deduct amount less then previous one; 
    100005 if invalid response from R/W; 
    100016 if card read error; 
    100017 if the card polled before is not on the target or card communication has been interrupted since last poll; 
    100020 if card hasn't been polled before or no card present; 
    100048 if card has insufficient fund; 
    100055 if purse go negative; 
    100056 if invalid deferdeduct state sequence.

    addValue
    100001 if R/W not connected; 
    100005 if invalid response from R/W; 
    100016 if card read error; 
    100017 if the card polled before is not on the target or card communication has been interupted since last poll; 100019 if card is blocked; 
    100020 if card hasn't been polled before or no card present; 
    100022 if retry please; 
    100023 if transaction log full; 
    100049 if too much money; 
    100050 if add value quota exceeded.
    */
    var setOctopusIdleHandle = 0;
    /* 
    Parameter : {totalPrice:0,invoice:'',till:''}
    */
    this.DeductOctopus = (args, callback) => {
        console.log("DeductOctopus", args);
        if (setOctopusIdleHandle) clearTimeout(setOctopusIdleHandle);
        var resultCode = "ERROR";
        var msg = "";
        var totalPrice = parseInt(args.totalPrice * 10);
        //var totalPrice = 1;
        var deley = args.delay ? args.delay : 3;
        var qs = QueryString.stringify({
            "sessionk": "techtranswebon",
            "method": "deduct",
            "amount": totalPrice,
            "invoice": args.invoice,
            "delay": deley
        });
        if (config.tills && args.till) {
            var octopus = extend2({}, config.tills[args.till].octopus);
        }
        if (config.tills && !args.till) {
            var octopus = extend2({}, config.tills[Object.keys(config.tills)[0]].octopus);
        }
        if (!octopus) {
            callback({ result: resultCode, note: "octopus_api_setting_error" });
            return;
        }
        connToOctopus(qs, octopus.apiServer, (data) => {
            // return a string from octopus api
            // "resultcode=0,amount,cardid"
            console.log("DeductOctopus response", data);
            try {
                var result = data.split(",");
                var validOrder = true;
                var octopusCode = result[0].split("=")[1];
                if (octopusCode <= 100000) {
                    resultCode = "OK";

                    var payAmount = totalPrice / 10,
                        //cardBalance = parseFloat(result[1] / 10).toFixed(2),
                        cardBalance = parseFloat(octopusCode / 10).toFixed(1),
                        cardId = result[1],
                        cusInfo = result[2],
                        octoId = result[3];
                }
                else {
                    msg = octopusCode;
                    var cardId = result[1],
                        cusInfo = result[2],
                        octoId = result[3];
                }
                var callbackObj = { result: resultCode, timestamp: new Date().getTime(), taskID: 1, note: msg, suspendItems: [], totalPrice: args.totalPrice, invoice: args.invoice }
                var octopusObj = {
                    "machineId": octopus.machineId,
                    "octopusCode": octopusCode,
                    "cardId": cardId,
                    "txId": octoId
                };
                if (cardBalance && cardId) {
                    extend2(octopusObj, {
                        "payAmount": payAmount,
                        "cardBalance": cardBalance,
                        "receiptNo": args.invoiceSeq
                    });
                }
                callbackObj = extend2(callbackObj, octopusObj);
                console.log("DeductOctopus callback", callbackObj);

                callback(callbackObj);

                var qs = QueryString.stringify({
                    "sessionk": "techtranswebon",
                    "method": "clear"
                });
                var connToOctopusServer = function () {
                    connToOctopus(qs, octopus.apiServer, function () {
                        preventMulitpleOctopus = false;
                    })
                };
                var timeout = 5000;
                if (octopusCode > 100000 && !(octopusCode === "100022" || (args.previousErrorCode && args.previousErrorCode === "100022" && octopusCode === "100032"))) {
                    timeout = 1000;
                }
                setOctopusIdleHandle = setTimeout(function () {
                    connToOctopusServer();
                }, timeout);
            }
            catch (e) {
                console.log("DeductOctopus", e);
                preventMulitpleOctopus = false;
                callback({ result: "ERROR", msg: e.stack });
            }
        });
    }

    this.EnquiryOctopus = (args, callback) => {
        console.log("EnquiryOctopus", args);
        if (setOctopusIdleHandle) clearTimeout(setOctopusIdleHandle);
        var resultCode = "ERROR";
        var msg = "";
        var deley = args.delay ? args.delay : 3;
        var qs = QueryString.stringify({
            "sessionk": "techtranswebon",
            "method": "poll",
            "delay": deley
        });
        if (config.tills && args.till) {
            var octopus = extend2({}, config.tills[args.till.$value].octopus);
        }
        if (config.tills && !args.till) {
            var octopus = extend2({}, config.tills[Object.keys(config.tills)[0]].octopus);
        }
        if (!octopus) {
            callback({ result: resultCode, note: "octopus_api_setting_error" });
            return;
        }
        connToOctopus(qs, octopus.apiServer, (data) => {
            // response result
            // <Card ID>,<Customer Info>,<IDm>,[Card Log 1],[Card Log 2],....,[Card Log 10]
            // [Card Log n] = <SP Type>,<Transaction Amt>,<Transaction Time>,<Machine ID>,<Service Info>
            console.log("EnquiryOctopus response", data);
            try {
                var transactionData = [];
                var result = data.split(",");
                var validOrder = true;
                var octopusCode = result[0].split("=")[1];
                if (octopusCode <= 100000) {
                    resultCode = "OK";

                    var cardBalance = parseFloat(octopusCode / 10).toFixed(1),
                        cardId = result[1],
                        cusInfo = result[2],
                        octoId = result[3];
                    var startIdx = 4;
                    var endIdx = (5 * 10) + startIdx;
                    var idx = -1;
                    for (i = startIdx; i < endIdx;) {
                        var subIdx = (i + 1) % 5;
                        if (subIdx == 0) {
                            if (!(result[i] >= 0 && result[i] <= 255)) {
                                i += 5;
                                continue;
                            }
                            idx++;
                            transactionData[idx] = [];
                        }
                        var val = result[i];
                        switch (subIdx) {
                            case 1:
                                val = (parseFloat(result[i]) / 10);
                                val = (val > 0 ? "+" : "") + val.toFixed(1);
                                transactionData[idx][2] = val;
                                break;
                            case 2:
                                val = moment(new Date(parseInt(result[i]) * 1000 + (new Date(Date.UTC(2000, 0, 1))).getTime())).format("YYYY-MM-DD HH:mm:ss");
                                transactionData[idx][1] = val;
                                break;
                            case 3:
                                val = parseInt(result[i]).toString(16).toUpperCase();
                                var sameDevice = octopus.machineId.search(val + "$") != -1;
                                transactionData[idx][3] = val;
                                transactionData[idx][0] = "" + (idx + 1) + (sameDevice ? "  #" : "");
                                break;
                        }
                        i++;
                    }
                }
                else {
                    msg = octopusCode;
                    var cardId = result[1],
                        cusInfo = result[2],
                        octoId = result[3];
                }
                var callbackObj = { result: resultCode, timestamp: new Date().getTime(), taskID: 1, note: msg, suspendItems: [], totalPrice: args.totalPrice, invoice: args.invoice }
                var octopusObj = {
                    "machineId": octopus.machineId,
                    "octopusCode": octopusCode,
                    "cardId": cardId,
                    "cardBalance": cardBalance,
                    "txId": octoId,
                    "transactionData": transactionData
                };
                callbackObj = extend2(callbackObj, octopusObj);
                console.log("EnquiryOctopus callback", callbackObj);
                callback(callbackObj);

                var qs = QueryString.stringify({
                    "sessionk": "techtranswebon",
                    "method": "clear"
                });
                var connToOctopusServer = function () {
                    connToOctopus(qs, octopus.apiServer, function () {
                        preventMulitpleOctopus = false;
                    })
                };
                var timeout = 5000;
                if (octopusCode > 100000) {
                    timeout = 1000;
                }
                setOctopusIdleHandle = setTimeout(function () {
                    connToOctopusServer();
                }, timeout);
            }
            catch (e) {
                console.log("EnquiryOctopus", e);
                preventMulitpleOctopus = false;
                callback({ result: "ERROR", msg: e.stack });
            }
        });
    }

    this.CancelOctopusIncompleteTransaction = (args, callback) => {
        console.log("CancelOctopusIncompleteTransaction", args);
        if (setOctopusIdleHandle) clearTimeout(setOctopusIdleHandle);
        var resultCode = "ERROR";
        var msg = "";
        var deley = args.delay ? args.delay : 3;
        var qs = QueryString.stringify({
            "sessionk": "techtranswebon",
            "method": "cancel"
        });
        if (config.tills && args.till) {
            var octopus = extend2({}, config.tills[args.till.$value].octopus);
        }
        if (config.tills && !args.till) {
            var octopus = extend2({}, config.tills[Object.keys(config.tills)[0]].octopus);
        }
        if (!octopus) {
            callback({ result: resultCode, note: "octopus_api_setting_error" });
            return;
        }

        connToOctopus(qs, octopus.apiServer, (data) => {
            // response result
            // <Card ID>,<Customer Info>,<IDm>,[Card Log 1],[Card Log 2],....,[Card Log 10]
            // [Card Log n] = <SP Type>,<Transaction Amt>,<Transaction Time>,<Machine ID>,<Service Info>
            console.log("CancelOctopusIncompleteTransaction response", data);
            try {
                var transactionData = [];
                var result = data.split(",");
                var validOrder = true;
                var octopusCode = result[0].split("=")[1];
                callback(octopusCode);
            }
            catch (e) {
                console.log("CancelOctopusIncompleteTransaction", e);
                preventMulitpleOctopus = false;
                callback({ result: "ERROR", msg: e.stack });
            }
        });
    }

    this.DownloadData = (args, callback) => {

        var query = [];
        //query.push({sql:" SET IDENTITY_INSERT " + args.tableName + "  ON ;", args:[]});

        //var sql = "delete from " + args.tableName + " ; ";
        query.push({ sql: args.result, args: [] });
        //query.push({sql:" SET IDENTITY_INSERT " + args.tableName + "  OFF ;", args:[]});


        //console.log("----------------------------------");
        this.DM.executeStatement(query, (err, result) => {
            if (err == null)
                callback(true);
        })
    }

    this.ListMenuLayout = (args, callback) => {
        var query = [
            {
                "sql": "select layoutId, layoutName, panel, dayOfWeek, priority, convert(varchar, activateDate, 101) activateDate, convert(varchar, expireDate, 101) expireDate, staffId, convert(varchar, lastUpdateDate, 120) lastUpdateDate from menuLayout"
            },
            {
                "sql": "select * from menuLayoutResolution order by seq"
            },
            {
                "sql": "select * from timeRange"
            },
            {
                "sql": "select * from menuTileTemplate"
            }
        ];
        this.DM.executeStatement(query, (err, result) => {
            if (err) {
                callback({ result: "ERROR", "msg": err });
                return;
            }
            result[2].forEach(function (v) {
                v.timeStart = moment(v.timeStart).format("HH:mm:ss");
                v.timeEnd = moment(v.timeEnd).format("HH:mm:ss");
            })
            var imageList = [];
            fs.readdir('./public/eMenuImages', function (err, files) {
                //console.log(files);
                if (!err) {
                    var imageList = [];
                    files.forEach(function (f, i) {
                        if (!f.match('.db$')) {
                            imageList.push({
                                id: "/eMenuImages/" + f,
                                text: f
                            });
                        }
                    });
                    result.push(imageList);
                }

                callback({ result: "success", data: result });
            });
        });
    }


    this.LoadMenuLayout = (args, callback) => {
        var query = {
            "sql": "select layoutId, layoutName, panel, dayOfWeek, priority, width, height, resId, backgroundColor, fontColor, orientation, flow, convert(varchar, activateDate, 101) activateDate, convert(varchar, expireDate, 101) expireDate, staffId, convert(varchar, lastUpdateDate, 120) lastUpdateDate from menuLayout where layoutId = @layoutId",
            "args": [
                { "c": "layoutId", "t": TYPES.Int, "v": args.id }
            ]
        };
        this.DM.executeStatement(query, (err, result) => {
            if (err) {
                callback({ result: "ERROR", "msg": err });
                return;
            }

            callback({ result: "success", data: result });
            //result[2].forEach(function (v) {
            //    v.timeStart = moment(v.timeStart).format("HH:mm:ss");
            //    v.timeEnd = moment(v.timeEnd).format("HH:mm:ss");
            //})
            //var imageList = [];
            //fs.readdir('./public/eMenuImages', function (err, files) {
            //    //console.log(files);
            //    if (!err) {
            //        var imageList = [];
            //        files.forEach(function (f, i) {
            //            if (!f.match('.db$')) {
            //                imageList.push({
            //                    id: "/eMenuImages/" + f,
            //                    text: f
            //                });
            //            }
            //        });
            //        result.push(imageList);
            //    }
            //});
        });
    }

    this.SaveMenuLayout = (args, callback) => {
        // todo: check input 
        console.log('SaveMenuLayout', args);
        //var args = JSON.parse(args);
        //var _data = args;
        console.log('args.flow', args.flow);
        console.log('args.dayOfWeek', args.dayOfWeek);
        if (args.layoutId > 0) {
            if (args.flowOnly) {
                var query = {
                    "sql": "update menuLayout set flow = @flow, staffId = @staffId, lastUpdateDate = @lastUpdateDate where layoutId = @layoutId",
                    "args": [
                        { "c": "layoutId", "t": TYPES.Int, "v": args.layoutId },
                        { "c": "flow", "t": TYPES.NVarChar, "v": JSON.stringify(args.flow) },
                        { "c": "staffId", "t": TYPES.VarChar, "v": args.staffId },
                        { "c": "lastUpdateDate", "t": TYPES.DateTime, "v": new Date() }
                    ]
                };
            }
            else {
                var query = {
                    "sql": "update menuLayout set layoutName = @layoutName, panel = @panel, dayOfWeek = @dayOfWeek, priority = @priority, width = @width, height = @height, resId = @resId, backgroundColor = @backgroundColor, fontColor = @fontColor, orientation = @orientation, activateDate = @activateDate, expireDate = @expireDate, flow = @flow, staffId = @staffId, lastUpdateDate = @lastUpdateDate where layoutId = @layoutId",
                    "args": [
                        { "c": "layoutId", "t": TYPES.Int, "v": args.layoutId },
                        { "c": "layoutName", "t": TYPES.NVarChar, "v": args.layoutName },
                        { "c": "panel", "t": TYPES.VarChar, "v": args.panel },
                        { "c": "dayOfWeek", "t": TYPES.VarChar, "v": args.dayOfWeek },
                        { "c": "priority", "t": TYPES.Int, "v": args.priority },
                        { "c": "width", "t": TYPES.Int, "v": args.width },
                        { "c": "height", "t": TYPES.Int, "v": args.height },
                        { "c": "resId", "t": TYPES.Int, "v": args.resId },
                        { "c": "backgroundColor", "t": TYPES.VarChar, "v": args.backgroundColor },
                        { "c": "fontColor", "t": TYPES.VarChar, "v": args.fontColor },
                        { "c": "orientation", "t": TYPES.VarChar, "v": args.orientation },
                        { "c": "activateDate", "t": TYPES.DateTime, "v": new Date(args.activateDate) },
                        { "c": "expireDate", "t": TYPES.DateTime, "v": new Date(args.expireDate) },
                        { "c": "flow", "t": TYPES.NVarChar, "v": JSON.stringify(args.flow) },
                        { "c": "staffId", "t": TYPES.VarChar, "v": args.staffId },
                        { "c": "lastUpdateDate", "t": TYPES.DateTime, "v": new Date() }
                    ]
                };
            }
        }
        else {
            var query = {
                "sql": "insert into menuLayout (layoutName, panel, dayOfWeek, priority, width, height, resId, backgroundColor, fontColor, orientation, activateDate, expireDate, flow, staffId, lastUpdateDate) values (@layoutName, @panel, @dayOfWeek, @priority, @width, @height, @resId, @backgroundColor, @fontColor, @orientation, @activateDate, @expireDate, @flow, @staffId, @lastUpdateDate)",
                "args": [
                    //{ "c": "layoutId", "t": TYPES.Int, "v": args.layoutId },
                    { "c": "layoutName", "t": TYPES.NVarChar, "v": args.layoutName },
                    { "c": "panel", "t": TYPES.VarChar, "v": args.panel },
                    { "c": "dayOfWeek", "t": TYPES.VarChar, "v": args.dayOfWeek },
                    { "c": "priority", "t": TYPES.Int, "v": args.priority },
                    { "c": "width", "t": TYPES.Int, "v": args.width },
                    { "c": "height", "t": TYPES.Int, "v": args.height },
                    { "c": "resId", "t": TYPES.Int, "v": args.resId },
                    { "c": "backgroundColor", "t": TYPES.VarChar, "v": args.backgroundColor },
                    { "c": "fontColor", "t": TYPES.VarChar, "v": args.fontColor },
                    { "c": "orientation", "t": TYPES.VarChar, "v": args.orientation },
                    { "c": "activateDate", "t": TYPES.DateTime, "v": new Date(args.activateDate) },
                    { "c": "expireDate", "t": TYPES.DateTime, "v": new Date(args.expireDate) },
                    { "c": "flow", "t": TYPES.NVarChar, "v": JSON.stringify(args.flow) },
                    { "c": "staffId", "t": TYPES.VarChar, "v": args.staffId },
                    { "c": "lastUpdateDate", "t": TYPES.DateTime, "v": new Date() }
                ]
            };
        }
        this.DM.executeStatement(query, (err, result) => {
            if (err) {
                console.log("SaveMenuLayout", err);
                callback({ result: "ERROR", "msg": err });
                return;
            }
            //this.ListMenuLayout(null, callback);
            callback({ result: "success", data: result });
        });
    }

    this.ListTimeRange = (args, callback) => {
        //callback({ result: "success", data: [] });

        this.DM.executeStatement({ sql: "select * from timeRange;", args: [] }, (err, result) => {
            if (err == null) {
                //result.forEach(function (v) {
                //    v.timeStart = moment(v.timeStart).format("HH:mm:ss");
                //    v.timeEnd = moment(v.timeEnd).format("HH:mm:ss");
                //})
                callback(result);
            }
        });
    }

    this.InsertTimeRange = (args, callback) => {

        var sql = "  INSERT INTO [timeRange]  ([timeStart] ,[timeEnd] ,[name]  ,[date]) VALUES ( @timeStart, @timeEnd, @name, @date); select * from [timeRange] where timeRangeId = @@IDENTITY;";
        var args = [
            { "c": "timeStart", "t": TYPES.VarChar, "v": args.startTime },
            { "c": "timeEnd", "t": TYPES.VarChar, "v": args.endTime },
            { "c": "name", "t": TYPES.VarChar, "v": args.name },
            { "c": "date", "t": TYPES.VarChar, "v": args.dateString }
        ];

        var query = [{ "sql": sql, "args": args }, { "sql": "SELECT TOP 1 *  FROM [timeRange] order by timeRangeId desc", "args": [] }];

        this.DM.executeStatement(query, function (err, result) {

            if (err == null) callback(result[1]);


        })

    }

    this.UpdateTimeRange = (args, callback) => {

        var sql = "update [timeRange] set name=@name , timeStart=@timeStart, timeEnd=@timeEnd, date=@date where timeRangeId=@timeRangeId; select * from [timeRange] where [timeRangeId] = @timeRangeId ";
        var args = [
            { "c": "timeStart", "t": TYPES.VarChar, "v": args.startTime },
            { "c": "timeEnd", "t": TYPES.VarChar, "v": args.endTime },
            { "c": "name", "t": TYPES.VarChar, "v": args.name },
            { "c": "date", "t": TYPES.VarChar, "v": args.dateString },
            { "c": "timeRangeId", "t": TYPES.Int, "v": parseInt(args.timeRangeId) }

        ]

        this.DM.executeStatement({ "sql": sql, "args": args }, (err, result) => {
            if (!err) console.log("UpdateTimeRange", err);
            if (err == null) callback(result);

        })


    }

    this.GetAllItem = (ItemCode, callback) => {

        console.log(ItemCode)

        var sql = "select DISTINCT i.*, ic.categoryId, pbd.price1, c.name1 as categoryname from item as i     Inner join itemCategory as ic on ic.itemId = i.itemId  Inner join category as c on c.categoryId = ic.categoryId Left join priceBatchD as pbd on pbd.itemId = i.itemId ##where## order by ic.categoryId, i.itemId ";

        var args = [];
        if (ItemCode === undefined) {
            sql = sql.replace('##where##', '');

        } else {
            sql = sql.replace('##where##', 'where i.itemId = @ItemCode');
            args.push({ "c": "ItemCode", "t": TYPES.VarChar, "v": ItemCode });

        }




        this.DM.executeStatement({ "sql": sql, "args": args }, (err, result) => {

            if (err == null) {

                callback(result);

            }


        })

    }

    this.GetCategory = (CategoryId, callback) => {

        var sql = "select * from category ##where## order by categoryId";
        if (CategoryId == null) sql = sql.replace('##where##', '');
        else sql = sql.replace('##where##', 'where categoryId =' + CategoryId);



        this.DM.executeStatement({ sql: sql, args: [] }, (err, result) => {

            if (err == null) {
                callback(result);
            }

        })


    }

    this.SearchItemIsExist = (ItemCode, callback) => {

        var sql = "select count(*) as isExist from [item] where itemId = '" + ItemCode + "'";

        this.DM.executeStatement({ "sql": sql, "args": [] }, (err, result) => {

            callback(result[0].isExist);
        })
    }


    this.InsertItem = (InsertArgs, callback) => {
        var FoodItem = InsertArgs.ItemInfo
        var query = [
            {
                "sql": "INSERT INTO [dbo].[item]  ([itemId],[code],[plu],[name1],[name2],[name3],[namek],[color],[printer],[printerGroup],[serviceCharge],[discount],[kds],[type],[goMenu], [individualPrint], [suspend]) VALUES (@itemId, @code, @plu, @name1, @name2, @name3, @namek, @color, @printer, @printerGroup, @serviceCharge, @discount, @kds, @type, @goMenu, @individualPrint, @suspend)",

                "args": [
                    { "c": "itemId", "t": TYPES.VarChar, "v": FoodItem.code },
                    { "c": "code", "t": TYPES.VarChar, "v": FoodItem.code },
                    { "c": "plu", "t": TYPES.VarChar, "v": FoodItem.plu },
                    { "c": "name1", "t": TYPES.NVarChar, "v": FoodItem.cname },
                    { "c": "name2", "t": TYPES.NVarChar, "v": FoodItem.cname2 },
                    { "c": "name3", "t": TYPES.NVarChar, "v": FoodItem.ename },
                    { "c": "namek", "t": TYPES.NVarChar, "v": FoodItem.kname },
                    { "c": "color", "t": TYPES.VarChar, "v": FoodItem.color },
                    { "c": "printer", "t": TYPES.VarChar, "v": FoodItem.printer },
                    { "c": "printerGroup", "t": TYPES.VarChar, "v": FoodItem.printerGroup },
                    { "c": "serviceCharge", "t": TYPES.Bit, "v": FoodItem.serviceCharge == "0" ? 0 : 1 },
                    { "c": "discount", "t": TYPES.Int, "v": parseInt(FoodItem.discount) },
                    { "c": "kds", "t": TYPES.VarChar, "v": FoodItem.kds },
                    { "c": "type", "t": TYPES.VarChar, "v": FoodItem.ItemType == "null" ? null : FoodItem.ItemType },
                    { "c": "goMenu", "t": TYPES.Bit, "v": FoodItem.screenShow == "0" ? 0 : 1 },
                    { "c": "individualPrint", "t": TYPES.Bit, "v": FoodItem.individualPrint == "0" ? 0 : 1 },
                    { "c": "suspend", "t": TYPES.Bit, "v": FoodItem.suspend == "0" ? 0 : 1 }
                ]
            },
            {

                "sql": "INSERT INTO [itemCategory] ([categoryId], [itemId], [seq]) VALUES (@categoryId, @itemId, (select case WHEN Max(seq) is null then 1 ELSE Max(seq) + 1 end  from itemCategory where categoryId = @categoryId)    )",
                "args": [
                            { "c": "categoryId", "t": TYPES.Int, "v": FoodItem.category },
                            { "c": "itemId", "t": TYPES.VarChar, "v": FoodItem.code }
                ]

            }
        ]

        var ModifierList = InsertArgs.Modifier;

        ModifierList.forEach(function (item, index) {
            query.push({
                "sql": "INSERT INTO [itemModifierGroup] ([itemCode], [modifierGroupCode]) VALUES (@itemId, @modifierGroupCode" + index + ");",
                "args": [
                            { "c": "itemId", "t": TYPES.VarChar, "v": FoodItem.code },
                            { "c": "modifierGroupCode" + index, "t": TYPES.VarChar, "v": item.modifierGroupCode }
                ]
            })

        })

        var SubitemList = InsertArgs.SubItem;

        SubitemList.forEach(function (item, index) {
            query.push({
                "sql": "INSERT INTO [itemSubitem] ([itemCode], [subitemCode]) VALUES (@itemId, @subitemCode" + index + ");",
                "args": [
                            { "c": "itemId", "t": TYPES.VarChar, "v": FoodItem.code },
                            { "c": "subitemCode" + index, "t": TYPES.VarChar, "v": item.subitemCode }
                ]
            })
        })

        var OptionList = InsertArgs.Option;
        OptionList.forEach(function (item, index) {

            query.push({
                "sql": "INSERT INTO [itemOptionGroup] ([itemCode],[optionGroupCode],[minQty],[maxQty]) VALUES (@itemId, @optionGroupCode" + index + ", @minQty" + index + ", @maxQty" + index + " );",
                "args": [
                            { "c": "itemId", "t": TYPES.VarChar, "v": FoodItem.code },
                            { "c": "optionGroupCode" + index, "t": TYPES.VarChar, "v": item.optionGroupCode },
                            { "c": "minQty" + index, "t": TYPES.Int, "v": item.minQty },
                            { "c": "maxQty" + index, "t": TYPES.Int, "v": item.maxQty }
                ]
            })

        });



        var TimeRangeList = InsertArgs.TimeRange

        TimeRangeList.forEach(function (item, index) {
            query.push({
                "sql": "INSERT INTO [itemTimeRange] ([itemId],[timeRangeId],[datActive],[datExpire]) VALUES (@itemId, @timeRangeId" + index + ", @datActive" + index + ", @datExpire" + index + " );",
                "args": [
                            { "c": "itemId", "t": TYPES.VarChar, "v": FoodItem.code },
                            { "c": "timeRangeId" + index, "t": TYPES.VarChar, "v": item.timeRangeId },
                            { "c": "datActive" + index, "t": TYPES.VarChar, "v": item.ActiveDate },
                            { "c": "datExpire" + index, "t": TYPES.VarChar, "v": '2099-12-31' }
                ]
            })

        })

        this.DM.executeStatement(query, (err, result) => {

            if (err == null) {
                callback('OK');
            }

        })

    }

    this.InsertCategory = (CateItem, callback) => {

        var sql = "insert into [category] (categoryId, code ,name1, name2, name3, seq, parentCategory, mainCatrgory, icon, type, color) Values ( ((select MAX(categoryId) from Category) +1),  ((select MAX(categoryId) from Category) +1), @name1, @name2, @name3, @seq, 0, 0, @icon, 0, @color);select MAX(categoryId) as categoryId from Category"

        var args = [
            { "c": "name1", "t": TYPES.NVarChar, "v": CateItem.cname },
            { "c": "name2", "t": TYPES.NVarChar, "v": CateItem.ename },
            { "c": "name3", "t": TYPES.NVarChar, "v": CateItem.cname2 },
            { "c": "seq", "t": TYPES.Int, "v": CateItem.seq },
            { "c": "icon", "t": TYPES.Int, "v": CateItem.icon === "null" ? null : parseInt(CateItem.icon) },
            { "c": "color", "t": TYPES.VarChar, "v": CateItem.color }
        ]

        this.DM.executeStatement({ "sql": sql, "args": args }, (err, result) => {

            if (err == null) {
                callback(parseInt(result[0].categoryId));
            }

        })

    }

    this.UpdateItem = (InsertArgs, callback) => {
        var FoodItem = InsertArgs.ItemInfo;
        var query = [
            {
                "sql": "UPDATE [dbo].[item] SET [itemId] = @itemId, [code] = @code, [plu] = @plu, [name1] = @name1, [name2] = @name2, [name3] = @name3, [namek] = @namek, [color] = @color, [printer] = @printer, [printerGroup] = @printerGroup, [serviceCharge] = @serviceCharge, [discount] = @discount, [kds] =  @kds, [type] =  @type, [goMenu] = @goMenu, [individualPrint] = @individualPrint, [suspend] = @suspend WHERE code = @ocode",

                "args": [
                    { "c": "itemId", "t": TYPES.VarChar, "v": FoodItem.code },
                    { "c": "code", "t": TYPES.VarChar, "v": FoodItem.code },
                    { "c": "ocode", "t": TYPES.VarChar, "v": FoodItem.ocode },
                    { "c": "plu", "t": TYPES.VarChar, "v": FoodItem.plu },
                    { "c": "oplu", "t": TYPES.VarChar, "v": FoodItem.oplu },
                    { "c": "name1", "t": TYPES.NVarChar, "v": FoodItem.cname },
                    { "c": "name2", "t": TYPES.NVarChar, "v": FoodItem.cname2 },
                    { "c": "name3", "t": TYPES.NVarChar, "v": FoodItem.ename },
                    { "c": "namek", "t": TYPES.NVarChar, "v": FoodItem.kname },
                    { "c": "color", "t": TYPES.VarChar, "v": FoodItem.color },
                    { "c": "printer", "t": TYPES.VarChar, "v": FoodItem.printer },
                    { "c": "printerGroup", "t": TYPES.VarChar, "v": FoodItem.printerGroup },
                    { "c": "serviceCharge", "t": TYPES.Bit, "v": FoodItem.serviceCharge === "0" ? 0 : 1 },
                    { "c": "discount", "t": TYPES.Int, "v": parseInt(FoodItem.discount) },
                    { "c": "kds", "t": TYPES.VarChar, "v": FoodItem.kds },
                    { "c": "type", "t": TYPES.VarChar, "v": FoodItem.ItemType == "null" ? null : FoodItem.ItemType },
                    { "c": "goMenu", "t": TYPES.Bit, "v": FoodItem.screenShow == "0" ? 0 : 1 },
                    { "c": "individualPrint", "t": TYPES.Bit, "v": FoodItem.individualPrint == "0" ? 0 : 1 },
                    { "c": "suspend", "t": TYPES.Bit, "v": FoodItem.suspend == "0" ? 0 : 1 }
                ]
            },
            {

                "sql": "UPDATE [itemCategory] SET [categoryId] = @categoryId , [itemId] = @itemId, [seq] =(select case WHEN Max(seq) is null then 1 ELSE Max(seq) + 1 end  from itemCategory where categoryId = @categoryId) where [itemId] = @ocode",
                "args": [
                            { "c": "categoryId", "t": TYPES.Int, "v": FoodItem.category },
                            { "c": "itemId", "t": TYPES.VarChar, "v": FoodItem.code },
                            { "c": "ocode", "t": TYPES.VarChar, "v": FoodItem.ocode }
                ]

            },
            {
                "sql": "Delete from [itemModifierGroup] where [itemCode]=@itemId",

                "args": [
                            { "c": "itemId", "t": TYPES.VarChar, "v": FoodItem.code },

                ]


            },
            {
                "sql": "Delete from [itemOptionGroup] where [itemCode]=@itemId",

                "args": [
                            { "c": "itemId", "t": TYPES.VarChar, "v": FoodItem.code },

                ]


            },
            {
                "sql": "Delete from [itemSubitem] where [itemCode]=@itemId",

                "args": [
                            { "c": "itemId", "t": TYPES.VarChar, "v": FoodItem.code },

                ]


            },
             {
                 "sql": "Delete from [itemTimeRange] where [itemId]=@itemId",

                 "args": [
                             { "c": "itemId", "t": TYPES.VarChar, "v": FoodItem.code },

                 ]


             }
        ]


        var ModifierList = InsertArgs.Modifier;

        ModifierList.forEach(function (item, index) {
            query.push({
                "sql": "INSERT INTO [itemModifierGroup] ([itemCode], [modifierGroupCode]) VALUES (@itemId, @modifierGroupCode" + index + ");",
                "args": [
                            { "c": "itemId", "t": TYPES.VarChar, "v": FoodItem.code },
                            { "c": "modifierGroupCode" + index, "t": TYPES.VarChar, "v": item.modifierGroupCode }
                ]
            })

        })

        var SubitemList = InsertArgs.SubItem;

        SubitemList.forEach(function (item, index) {
            query.push({
                "sql": "INSERT INTO [itemSubitem] ([itemCode], [subitemCode]) VALUES (@itemId, @subitemCode" + index + ");",
                "args": [
                            { "c": "itemId", "t": TYPES.VarChar, "v": FoodItem.code },
                            { "c": "subitemCode" + index, "t": TYPES.VarChar, "v": item.subitemCode }
                ]
            })
        })

        var OptionList = InsertArgs.Option;
        OptionList.forEach(function (item, index) {

            query.push({
                "sql": "INSERT INTO [itemOptionGroup] ([itemCode],[optionGroupCode],[minQty],[maxQty]) VALUES (@itemId, @optionGroupCode" + index + ", @minQty" + index + ", @maxQty" + index + " );",
                "args": [
                            { "c": "itemId", "t": TYPES.VarChar, "v": FoodItem.code },
                            { "c": "optionGroupCode" + index, "t": TYPES.VarChar, "v": item.optionGroupCode },
                            { "c": "minQty" + index, "t": TYPES.Int, "v": item.minQty },
                            { "c": "maxQty" + index, "t": TYPES.Int, "v": item.maxQty }
                ]
            })

        });



        var TimeRangeList = InsertArgs.TimeRange

        TimeRangeList.forEach(function (item, index) {
            query.push({
                "sql": "INSERT INTO [itemTimeRange] ([itemId],[timeRangeId],[datActive],[datExpire]) VALUES (@itemId, @timeRangeId" + index + ", @datActive" + index + ", @datExpire" + index + " );",
                "args": [
                            { "c": "itemId", "t": TYPES.VarChar, "v": FoodItem.code },
                            { "c": "timeRangeId" + index, "t": TYPES.VarChar, "v": item.timeRangeId },
                            { "c": "datActive" + index, "t": TYPES.VarChar, "v": item.ActiveDate },
                            { "c": "datExpire" + index, "t": TYPES.VarChar, "v": '2099-12-31' }
                ]
            })

        })

        this.DM.executeStatement(query, (err, result) => {

            if (err == null) {
                callback('OK');
            }

        })

    }



    this.UpdateCategory = (CateItem, callback) => {

        var query = [
            {
                "sql": "Update [category] SET name1 = @name1, name2 = @name2, name3 = @name3, seq = @seq, icon = @icon,  color = @color where categoryId = @categoryId; select * from [category] where categoryId = @categoryId",

                "args": [
                   { "c": "name1", "t": TYPES.NVarChar, "v": CateItem.cname },
                   { "c": "name2", "t": TYPES.NVarChar, "v": CateItem.ename },
                   { "c": "name3", "t": TYPES.NVarChar, "v": CateItem.cname2 },
                   { "c": "seq", "t": TYPES.Int, "v": CateItem.seq },
                   { "c": "icon", "t": TYPES.Int, "v": CateItem.icon === "null" ? null : parseInt(CateItem.icon) },
                   { "c": "color", "t": TYPES.VarChar, "v": CateItem.color },
                   { "c": "categoryId", "t": TYPES.Int, "v": CateItem.categoryId }
                ]
            }
        ]

        this.DM.executeStatement(query, (err, result) => {

            if (err == null) {
                callback(result[0]);
            }

        })

    }

    this.GetCurrentMenuLayout = (callback) => {
        var query = [
            {
                "sql": "select * from menuLayout where getdate() between activateDate and expireDate order by priority desc"
            },
            {
                "sql": "select * from timeRange"
            },
            {
                "sql": "select * from menuTileTemplate"
            }
        ];
        this.DM.executeStatement(query, (err, result) => {
            // day of week : from 1-7 for Sunday to Saturday
            var _obj = {},
                timeRange = result[1],
                templateList = extend2([], result[2]);
            //console.log("GenerateMenuLayout", timeRange);
            //console.log("GenerateMenuLayout", result);
            result[0].forEach(function (layout) {
                var dayOfWeek = layout.dayOfWeek.split(','),
                    today = new Date(),
                    panel = !layout.panel ? 'goMenu' : layout.panel;
                //console.log("GenerateMenuLayout", dayOfWeek);
                if (dayOfWeek.indexOf("" + (today.getDay() + 1)) != -1) {
                    //console.log("day of week match");
                    if (!_obj[panel] || (_obj[panel] && _obj[panel].priority < layout.priority)) {
                        _obj[panel] = layout;
                    }
                }
            });
            templateList.forEach(function (tmpl) {
                tmpl.template = JSON.parse(tmpl.template);
                extend2(tmpl, tmpl.template);
                delete tmpl.template;
            });
            result[2].forEach(function (tmpl) {
                tmpl.template = JSON.parse(tmpl.template);
            });
            if (_obj) {
                var callbackObj = {};
                for (var key in _obj) {
                    var _o = _obj[key];
                    _o.flow = JSON.parse(_o.flow);
                    _o.flow.forEach(function (flow) {
                        var matchTime = timeRange.filter(function (x) { return flow.timeRangeId && x.timeRangeId == parseInt(flow.timeRangeId); });
                        if (typeof matchTime != 'undefined' && matchTime.length) {
                            flow.time = moment(matchTime[0].timeStart).format("HH:mm:ss") + "-" + moment(matchTime[0].timeEnd).format("HH:mm:ss");
                            //console.log("set time range", flow.time);
                        }
                        else {
                            flow.time = "00:00:00-23:59:59";
                        }

                    });
                    var output = {
                        "layoutId": _o.layoutId,
                        "layoutName": _o.layoutName,
                        "panel": _o.panel,
                        "width": _o.width,
                        "height": _o.height,
                        "backgroundColor": _o.backgroundColor,
                        "fontColor": _o.fontColor,
                        "orientation": _o.orientation,
                        "flow": _o.flow,
                        "template": templateList
                    };
                    callbackObj[key] = output;
                }
                //console.log("getCurrentMenuLayout", callbackObj);
                var imageList = [];
                fs.readdir('./public/eMenuImages', function (err, files) {
                    //console.log(files);
                    if (!err) {
                        var imageList = [];
                        files.forEach(function (f, i) {
                            if (!f.match('.db$')) {
                                imageList.push({
                                    id: "/eMenuImages/" + f,
                                    text: f
                                });
                            }
                        });
                        //result.push(imageList);
                    }

                    callback({ "layouts": callbackObj, "templateList": result[2], "imageList": imageList, "serverTime": new Date() });
                });
            }
        });
    }

    this.GenerateMenuLayout = (callback) => {
        if (!callback) callback = function () { };
        var doWriteFile = function (fileName, text) {
            return function (callback) {
                fs.writeFile("./public/data/" + fileName, text, function (err) {
                    if (err) {
                        console.log("Save file error!", fileName, err);
                    } else {
                        //console.log("The file was saved!", fileName);
                    }
                    callback(null, fileName);
                });
            }
        }
        this.GetCurrentMenuLayout((_r) => {
            var saveSeries = [];
            var _obj = _r.layouts;
            for (var key in _obj) {
                var _o = _obj[key];
                var fileName = "layoutFlow" + (key != "goMenu" ? "_" + key : "") + ".json";
                saveSeries.push(doWriteFile(fileName, JSON.stringify(_o)));
            }
            async.parallel(saveSeries, function (err, results) {
                console.log("The files were saved!", results);
                callback();
            });
        });
    }

    this.GetAllModifierItem = (modifierId, callback) => {
        var sql = "select * from [modifier] ##where##";
        var args = [];

        if (modifierId == undefined && modifierId == null) {
            sql = sql.replace('##where##', '');
        }
        else {
            sql = sql.replace('##where##', 'where [modifierCode] = @code')
            args.push({ "c": "code", "t": TYPES.VarChar, "v": modifierId })
        }


        this.DM.executeStatement({ "sql": sql, args: args }, (err, result) => {


            if (err == null) callback(result);

        })


    }

    this.InsertModifierItem = (item, callback) => {
        var sql = "Insert INTO [modifier] ([modifierCode], [name1], [name2], [name3], [namek], [goMenu]) VALUES (@code, @name1, @name2, @name3, @namek, @goMenu)";
        var args = [
             { "c": "code", "t": TYPES.VarChar, "v": item.modifierCode },
             { "c": "name1", "t": TYPES.NVarChar, "v": item.name1 },
             { "c": "name2", "t": TYPES.NVarChar, "v": item.name2 },
             { "c": "name3", "t": TYPES.NVarChar, "v": item.name3 },
             { "c": "namek", "t": TYPES.NVarChar, "v": item.namek },
             { "c": "goMenu", "t": TYPES.Bit, "v": item.goMenu == "0" ? 0 : 1 }
        ]

        this.DM.executeStatement({ "sql": sql, "args": args }, (err, result) => {
            if (err == null) callback("OK");
        })


    }

    this.UpdateModifierItem = (item, callback) => {

        var sql = "UPDATE [modifier] set [name1]=@name1, [name2]=@name2, [name3]=@name3, [namek]=@namek, [goMenu]=@goMenu where   [modifierCode]=@ocode";
        var args = [
             { "c": "ocode", "t": TYPES.VarChar, "v": item.modifierCode },
             { "c": "name1", "t": TYPES.NVarChar, "v": item.name1 },
             { "c": "name2", "t": TYPES.NVarChar, "v": item.name2 },
             { "c": "name3", "t": TYPES.NVarChar, "v": item.name3 },
             { "c": "namek", "t": TYPES.NVarChar, "v": item.namek },
             { "c": "goMenu", "t": TYPES.Bit, "v": item.goMenu == "0" ? 0 : 1 }
        ]

        this.DM.executeStatement({ "sql": sql, "args": args }, (err, result) => {
            if (err == null) callback("OK");
        })


    }



    this.GetAllModifierGroup = (modifierGroupId, callback) => {
        var sql = "select * from [modifierGroup] ##where## ";
        var args = [];
        console.log(modifierGroupId);
        if (modifierGroupId == undefined || modifierGroupId == null) {
            sql = sql.replace('##where##', '');
        }
        else {
            sql = sql.replace('##where##', 'where [modifierGroupCode] = @code')
            args.push({ "c": "code", "t": TYPES.VarChar, "v": modifierGroupId })
        }


        this.DM.executeStatement({ "sql": sql, args: args }, (err, result) => {
            if (err == null) callback(result);
        })


    }


    this.InsertModifierGroup = (GroupItem, callback) => {

        var sql = "INSERT INTO [dbo].[modifierGroup] ([modifierGroupCode] ,[modifierGroupName], [goMenu]) VALUES (@code, @name, @goMenu)";
        var args = [
             { "c": "code", "t": TYPES.VarChar, "v": GroupItem.Code },
             { "c": "name", "t": TYPES.VarChar, "v": GroupItem.Name },
             { "c": "goMenu", "t": TYPES.Bit, "v": GroupItem.goMenu == "0" ? 0 : 1 }
        ]


        this.DM.executeStatement({ "sql": sql, "args": args }, (err, result) => {
            if (err == null) callback("OK");

        })

    }

    this.UpdateModifierGroup = (GroupItem, callback) => {

        var sql = "UPDATE [dbo].[modifierGroup] SET [modifierGroupName]=@name, [goMenu]=@goMenu where [modifierGroupCode]=@ocode";
        var args = [
             { "c": "ocode", "t": TYPES.VarChar, "v": GroupItem.Code },
             { "c": "name", "t": TYPES.VarChar, "v": GroupItem.Name },
             { "c": "goMenu", "t": TYPES.Bit, "v": GroupItem.goMenu == "0" ? 0 : 1 }
        ]
        console.log(args)
        this.DM.executeStatement({ sql: sql, args: args }, (err, result) => {
            if (err == null) callback("OK");

        })

    }


    this.GetAllModifierItemByGroup = (modifierGroupId, callback) => {

        //var sql = "select * from modifierGroupModifier as mgm inner join modifier on modifier.modifierCode = mgm.modifierCode where mgm.modifierGroupCode=@groupId";
        //var args = [{ "c": "groupId", "t": TYPES.VarChar, "v": modifierGroupId }];

        var query = [
            {
                "sql": "select * from modifierGroupModifier as mgm inner join modifier on modifier.modifierCode = mgm.modifierCode where mgm.modifierGroupCode=@groupId", "args": [{ "c": "groupId", "t": TYPES.VarChar, "v": modifierGroupId }]
            },
            {
                "sql": "select * from modifier where modifierCode not in ( select mgm.modifierCode from modifierGroupModifier as mgm inner join modifier on modifier.modifierCode = mgm.modifierCode where mgm.modifierGroupCode=@groupId )", "args": [{ "c": "groupId", "t": TYPES.VarChar, "v": modifierGroupId }]
            }
        ]

        this.DM.executeStatement(query, (err, result) => {
            if (err == null) callback(result);
        })


    }

    this.InsertItemToModifierGroup = (item, callback) => {

        var sql = "INSERT INTO [dbo].[modifierGroupModifier] ([modifierGroupCode] ,[modifierCode]) VALUES (@groupId, @itemCode)";
        var args = [
             { "c": "groupId", "t": TYPES.VarChar, "v": item.GroupId },
             { "c": "itemCode", "t": TYPES.VarChar, "v": item.ItemCode }
        ]

        this.DM.executeStatement({ "sql": sql, "args": args }, (err, result) => {
            if (err == null) callback("OK");
        })


    }

    this.RemoveItemInModifierGroup = (item, callback) => {

        var sql = "delete from [modifierGroupModifier] where [modifierGroupCode] = @groupId and [modifierCode] = @itemCode";
        var args = [
             { "c": "groupId", "t": TYPES.VarChar, "v": item.GroupId },
             { "c": "itemCode", "t": TYPES.VarChar, "v": item.ItemCode }
        ]

        this.DM.executeStatement({ "sql": sql, "args": args }, (err, result) => {
            if (err == null) callback("OK");
        })


    }

    this.GetAllOptionItem = (data, callback) => {

        var sql = "select * from [option] ##where## ";
        var args = [];
        if (data == null) {
            sql = sql.replace('##where##', '');
        }
        else {

            sql = sql.replace('##where##', 'where [optionCode]=@optionCode');
            args.push({ "c": "optionCode", "t": TYPES.VarChar, "v": data });
        }


        this.DM.executeStatement({ "sql": sql, "args": args }, (err, result) => {

            if (err == null) callback(result);
        })

    }

    this.InsertOptionItem = (data, callback) => {
        var option = data.option;
        var sql = "INSERT INTO [option] ([optionCode],[name1],[name2],[name3],[namek],[printer],[printerGroup],[kds],[goMenu], [individualPrint],[isDefault],[color]) VALUES (@code, @name1, @name2, @name3, @namek, @printer, @printerGroup, @kds, @goMenu, @individualPrint,@isDefault,@color)";
        var args = [
             { "c": "code", "t": TYPES.VarChar, "v": option.optionCode },
             { "c": "name1", "t": TYPES.NVarChar, "v": option.name1 },
             { "c": "name2", "t": TYPES.NVarChar, "v": option.name2 },
             { "c": "name3", "t": TYPES.NVarChar, "v": option.name3 },
             { "c": "namek", "t": TYPES.NVarChar, "v": option.namek },
             { "c": "printer", "t": TYPES.VarChar, "v": option.printer },
             { "c": "printerGroup", "t": TYPES.VarChar, "v": option.printerGroup },
             { "c": "kds", "t": TYPES.VarChar, "v": option.kds },
             { "c": "goMenu", "t": TYPES.Bit, "v": option.goMenu == "0" ? 0 : 1 },
             { "c": "individualPrint", "t": TYPES.Bit, "v": option.individualPrint == "0" ? 0 : 1 },
             { "c": "isDefault", "t": TYPES.Bit, "v": option.isDefault == "0" ? 0 : 1 },
             { "c": "color", "t": TYPES.VarChar, "v": option.color }
        ];

        var query = [];
        query.push({ sql: sql, args: args });

        var ModifierList = data.modifier;

        ModifierList.forEach(function (item, index) {
            query.push({
                "sql": "INSERT INTO [itemModifierGroup] ([itemCode], [modifierGroupCode]) VALUES (@itemId, @modifierGroupCode" + index + ");",
                "args": [
                            { "c": "itemId", "t": TYPES.VarChar, "v": option.optionCode },
                            { "c": "modifierGroupCode" + index, "t": TYPES.VarChar, "v": item.modifierGroupCode }
                ]
            })

        })

        this.DM.executeStatement(query, (err, result) => {
            if (err == null) callback("OK");

        })

    }

    this.UpdateOptionItem = (data, callback) => {
        var option = data.option;
        var sql = "UPDATE [option] SET [name1]=@name1,[name2]=@name2,[name3]=@name3,[namek]=@namek,[printer]=@printer,[printerGroup]=@printerGroup,[kds]=@kds,[goMenu]=@goMenu, [individualPrint] = @individualPrint,[isDefault] = @isDefault,[color] = @color where [optionCode]=@code";
        var args = [
             { "c": "code", "t": TYPES.VarChar, "v": option.optionCode },
             { "c": "name1", "t": TYPES.NVarChar, "v": option.name1 },
             { "c": "name2", "t": TYPES.NVarChar, "v": option.name2 },
             { "c": "name3", "t": TYPES.NVarChar, "v": option.name3 },
             { "c": "namek", "t": TYPES.NVarChar, "v": option.namek },
             { "c": "printer", "t": TYPES.VarChar, "v": option.printer },
             { "c": "printerGroup", "t": TYPES.VarChar, "v": option.printerGroup },
             { "c": "kds", "t": TYPES.VarChar, "v": option.kds },
             { "c": "goMenu", "t": TYPES.Bit, "v": option.goMenu == "0" ? 0 : 1 },
             { "c": "individualPrint", "t": TYPES.Bit, "v": option.individualPrint == "0" ? 0 : 1 },
             { "c": "isDefault", "t": TYPES.Bit, "v": option.isDefault == "0" ? 0 : 1 },
             { "c": "color", "t": TYPES.VarChar, "v": option.color }
        ];

        var query = [];
        query.push({ sql: sql, args: args });
        query.push({
            "sql": "Delete from [itemModifierGroup] where itemCode=@itemId;",
            "args": [
                        { "c": "itemId", "t": TYPES.VarChar, "v": option.optionCode }
            ]
        })

        var ModifierList = data.modifier;

        ModifierList.forEach(function (item, index) {
            query.push({
                "sql": "INSERT INTO [itemModifierGroup] ([itemCode], [modifierGroupCode]) VALUES (@itemId, @modifierGroupCode" + index + ");",
                "args": [
                            { "c": "itemId", "t": TYPES.VarChar, "v": option.optionCode },
                            { "c": "modifierGroupCode" + index, "t": TYPES.VarChar, "v": item.modifierGroupCode }
                ]
            })

        })

        this.DM.executeStatement(query, (err, result) => {
            if (err == null) callback("OK");

        })

    }

    this.GetAllOptionGroup = (data, callback) => {

        var sql = "select * from [optionGroup] ##where## ";
        var args = [];
        if (data == null) {
            sql = sql.replace('##where##', '');
        }
        else {

            sql = sql.replace('##where##', 'where [optionGroupCode]=@optionGroupCode')
            args.push({ "c": "optionGroupCode", "t": TYPES.VarChar, "v": data })
        }


        this.DM.executeStatement({ "sql": sql, "args": args }, (err, result) => {

            if (err == null) callback(result);
        })

    }

    this.InsertOptionGroup = (data, callback) => {
        var sql = "INSERT INTO [optionGroup] ([optionGroupCode],[name1],[name2],[name3],[goMenu]) VALUES (@code, @name1, @name2, @name3, @goMenu)";
        var args = [
             { "c": "code", "t": TYPES.VarChar, "v": data.optionGroupCode },
             { "c": "name1", "t": TYPES.NVarChar, "v": data.name1 },
             { "c": "name2", "t": TYPES.NVarChar, "v": data.name2 },
             { "c": "name3", "t": TYPES.NVarChar, "v": data.name3 },
             { "c": "goMenu", "t": TYPES.Bit, "v": data.goMenu == "0" ? 0 : 1 }

        ];

        this.DM.executeStatement({ sql: sql, args: args }, (err, result) => {
            if (err == null) callback("OK");

        })

    }

    this.UpdateOptionGroup = (data, callback) => {
        var sql = "UPDATE  [optionGroup] SET [name1]=@name1,[name2]=@name2,[name3]=@name3,[goMenu]=@goMenu  where [optionGroupCode] = @code";
        var args = [
             { "c": "code", "t": TYPES.VarChar, "v": data.optionGroupCode },
             { "c": "name1", "t": TYPES.NVarChar, "v": data.name1 },
             { "c": "name2", "t": TYPES.NVarChar, "v": data.name2 },
             { "c": "name3", "t": TYPES.NVarChar, "v": data.name3 },
             { "c": "goMenu", "t": TYPES.Bit, "v": data.goMenu == "0" ? 0 : 1 }

        ];

        this.DM.executeStatement({ sql: sql, args: args }, (err, result) => {
            if (err == null) callback("OK");

        })


    }

    this.GetAllOptionItemByGroup = (OptionGroupCode, callback) => {

        //var sql = "select * from modifierGroupModifier as mgm inner join modifier on modifier.modifierCode = mgm.modifierCode where mgm.modifierGroupCode=@groupId";
        //var args = [{ "c": "groupId", "t": TYPES.VarChar, "v": modifierGroupId }];

        var query = [
            {
                "sql": "select ogo.*, o.[optionCode] ,o.[name1],o.[name2],o.[name3],o.[namek],o.[printer],o.[printerGroup],o.[kds],o.[goMenu],o.[individualPrint] from [optionGroupOption] as ogo inner join [option] as o on o.optionCode = ogo.optionCode where ogo.optionGroupCode=@groupId order by ogo.[seq]",
                "args": [{ "c": "groupId", "t": TYPES.VarChar, "v": OptionGroupCode }]
            },
            {
                "sql": "select * from [option] where optionCode not in ( select ogo.optionCode from [optionGroupOption] as ogo inner join [option] as o on o.optionCode = ogo.optionCode where ogo.optionGroupCode=@groupId )", "args": [{ "c": "groupId", "t": TYPES.VarChar, "v": OptionGroupCode }]
            }
        ]

        this.DM.executeStatement(query, (err, result) => {
            if (err == null) callback(result);
        })


    }

    this.UpdateOptionItemSeqInGroup = (data, callback) => {

        var sql = "UPDATE [optionGroupOption] SET [seq]=@seq where [optionCode]=@optionCode and [optionGroupCode]=@groupCode;select ogo.*, o.[optionCode] ,o.[name1],o.[name2],o.[name3],o.[namek],o.[printer],o.[printerGroup],o.[kds],o.[goMenu],o.[individualPrint] from [optionGroupOption] as ogo inner join [option] as o on o.optionCode = ogo.optionCode where ogo.optionGroupCode=@groupCode order by ogo.[seq];"

        var args = [
            { "c": "groupCode", "t": TYPES.VarChar, "v": data.groupCode },
            { "c": "optionCode", "t": TYPES.VarChar, "v": data.optionCode },
            { "c": "seq", "t": TYPES.Int, "v": data.seq }
        ]
        this.DM.executeStatement({ sql: sql, args: args }, (err, result) => {
            if (err == null) callback(result);

        })

    }

    this.AddOptionInOptionGroup = (data, callback) => {

        var sql = "INSERT INTO optionGroupOption (optionCode, optionGroupCode, seq) values (@optionCode, @groupCode, (select case WHEN Max(seq) is null then 1 ELSE Max(seq) + 1 end  from optionGroupOption where optionGroupCode = @groupCode)  );";

        var args = [
            { "c": "groupCode", "t": TYPES.VarChar, "v": data.groupCode },
            { "c": "optionCode", "t": TYPES.VarChar, "v": data.optionCode }
        ];

        this.DM.executeStatement({ sql: sql, args: args }, (err, result) => {
            if (err == null) callback('OK');
        })
    }

    this.RemoveOptionFromOptionGroup = (data, callback) => {

        var sql = "Delete from optionGroupOption where [optionCode]=@optionCode and [optionGroupCode]= @groupCode ;";

        var args = [
            { "c": "groupCode", "t": TYPES.VarChar, "v": data.groupCode },
            { "c": "optionCode", "t": TYPES.VarChar, "v": data.optionCode }
        ];

        this.DM.executeStatement({ sql: sql, args: args }, (err, result) => {
            if (err == null) callback('OK');
        })
    }

    this.GetAllSubItem = (data, callback) => {

        var sql = "select * from [subitem] ##where## ";
        var args = [];
        if (data == null) {
            sql = sql.replace('##where##', '');
        }
        else {

            sql = sql.replace('##where##', 'where [subitemCode]=@subitemCode')
            args.push({ "c": "subitemCode", "t": TYPES.VarChar, "v": data })
        }


        this.DM.executeStatement({ "sql": sql, "args": args }, (err, result) => {

            if (err == null) callback(result);
        })

    }


    this.GetAllModifierByItemCode = (ItemCode, callback) => {
        var sql = "SELECT mg.*   FROM [itemModifierGroup] as img  inner join [modifierGroup] as mg on mg.modifierGroupCode = img.modifierGroupCode where img.itemCode = @code";
        var args = [{ "c": "code", "t": TYPES.VarChar, "v": ItemCode }];

        this.DM.executeStatement({ "sql": sql, "args": args }, (err, result) => {
            if (err == null) callback(result);
        })

    }

    this.GetAllSubItemByItemCode = (ItemCode, callback) => {
        var sql = "SELECT s.*  FROM [itemSubitem] as itemS inner join [subitem] as s on s.subitemCode = itemS.subitemCode where itemS.itemCode = @code";
        var args = [{ "c": "code", "t": TYPES.VarChar, "v": ItemCode }];

        this.DM.executeStatement({ "sql": sql, "args": args }, (err, result) => {
            if (err == null) callback(result);
        })

    }


    this.GetAllOptionGroupByItemCode = (ItemCode, callback) => {
        var sql = "SELECT og.*,iog.minQty, iog.maxQty FROM [itemOptionGroup] as iog inner join [optionGroup] as og on og.optionGroupCode = iog.optionGroupCode where iog.itemCode = @code";
        var args = [{ "c": "code", "t": TYPES.VarChar, "v": ItemCode }];

        this.DM.executeStatement({ "sql": sql, "args": args }, (err, result) => {
            if (err == null) callback(result);
        })

    }

    this.GetAllTimeRangeByItemCode = (ItemCode, callback) => {
        var sql = "SELECT tr.*, itr.datActive as ActiveDate  FROM [itemTimeRange] as itr inner join [timeRange] as tr on tr.timeRangeId = itr.timeRangeId where itr.itemId=@code";
        var args = [{ "c": "code", "t": TYPES.VarChar, "v": ItemCode }];

        this.DM.executeStatement({ "sql": sql, "args": args }, (err, result) => {
            if (err == null) {

                result.forEach(function (v) {
                    v.timeStart = moment(v.timeStart).format("HH:mm:ss");
                    v.timeEnd = moment(v.timeEnd).format("HH:mm:ss");
                    v.ActiveDate = moment(v.ActiveDate).format("YYYY-MM-DD");
                })
                callback(result);

            }
        })

    }

    this.InsertSubitem = (data, callback) => {
        var sql = "INSERT INTO [subitem] ([subitemCode],[name1],[name2],[name3],[namek],[printer],[printerGroup], [individualPrint]) VALUES (@subitemCode, @name1, @name2, @name3, @namek, @printer, @printerGroup, @individualPrint)";
        var args = [
             { "c": "subitemCode", "t": TYPES.VarChar, "v": data.subitemCode },
             { "c": "name1", "t": TYPES.NVarChar, "v": data.name1 },
             { "c": "name2", "t": TYPES.NVarChar, "v": data.name2 },
             { "c": "name3", "t": TYPES.NVarChar, "v": data.name3 },
             { "c": "namek", "t": TYPES.NVarChar, "v": data.namek },
             { "c": "printer", "t": TYPES.VarChar, "v": data.printer },
             { "c": "printerGroup", "t": TYPES.VarChar, "v": data.printerGroup },
             { "c": "individualPrint", "t": TYPES.Bit, "v": data.individualPrint == "0" ? 0 : 1 }

        ];

        this.DM.executeStatement({ sql: sql, args: args }, (err, result) => {
            if (err == null) callback("OK");

        })

    }

    this.UpdateSubitem = (data, callback) => {
        var sql = "UPDATE [subitem] SET [name1]=@name1,[name2]=@name2,[name3]=@name3,[namek]=@namek,[printer]=@printer,[printerGroup]=@printerGroup, [individualPrint] = @individualPrint where [subitemCode]=@code";
        var args = [
             { "c": "code", "t": TYPES.VarChar, "v": data.subitemCode },
             { "c": "name1", "t": TYPES.NVarChar, "v": data.name1 },
             { "c": "name2", "t": TYPES.NVarChar, "v": data.name2 },
             { "c": "name3", "t": TYPES.NVarChar, "v": data.name3 },
             { "c": "namek", "t": TYPES.NVarChar, "v": data.namek },
             { "c": "printer", "t": TYPES.VarChar, "v": data.printer },
             { "c": "printerGroup", "t": TYPES.VarChar, "v": data.printerGroup },
             { "c": "individualPrint", "t": TYPES.Bit, "v": data.individualPrint == "0" ? 0 : 1 }
        ];

        this.DM.executeStatement({ sql: sql, args: args }, (err, result) => {
            if (err == null) callback("OK");

        })

    }

    this.GetAllTakeOutOrder = (data, callback) => {
        //
        var till = data.till;
        //console.log(12812, data.phone);
        var condition = "";
        if (data.phone) {
            condition += ' and customerData like \'%"phone":"' + data.phone + '%\'';
            //console.log(condition);
        }
        var sql = ""
        if (data.serviceType == 'takeAway') {
            sql = "Select * from [transaction] where refNo like '%-TO-%' and void=0 " + condition + " order by startTime desc";
        }
        else if (data.serviceType == 'fastFood') {
            sql = "Select * from [transaction] where (refNo like '%-FD-%' or refNo like '%-FT-%') and void=0 and till = '" + till + "'order by startTime desc";
        } else if (data.serviceType == 'deliveryOrder') {
            sql = "Select * from [transaction] where refNo like '%-D-%' and void=0 order by startTime desc"
        }

        var args = [
            // { "c": "code", "t": TYPES.VarChar, "v": data.subitemCode },
        ];

        this.DM.executeStatement({ sql: sql, args: args }, (err, result) => {
            if (err == null) callback(result);

        })

    }

    this.GetAllDeliveryOrder = (data, callback) => {
        //
        var sql = "Select * from [transaction] where refNo like '%-D-%' and void=0 order by startTime desc";
        var args = [
            // { "c": "code", "t": TYPES.VarChar, "v": data.subitemCode },
        ];

        this.DM.executeStatement({ sql: sql, args: args }, (err, result) => {
            if (err == null) callback(result);

        })

    }

    this.GetTableCodeByTableSubCode = (data, callback) => {

        var sql = 'Select tableCode from tableOrder where tableSubCode = @tableSubCode';
        var args = [
            { "c": "tableSubCode", "t": TYPES.VarChar, "v": data }
        ]

        this.DM.executeStatement({ sql: sql, args: args }, (err, result) => {
            if (err == null) callback(result);

        })
    }

    this.voidTakeOutBill = (data, callback) => {




        var executeFunc = [];

        if (data.isCard && data.isUseECR) {
            executeFunc.push((cb) => {
                var query = { sql: "select top 1 * from ecrTransaction where refNo = @refNo and (Command_Code = '0' or Command_Code = 'a' ) order by [DateTime] desc;", args: [{ "c": "refNo", "t": TYPES.VarChar, "v": data.refNo }] };
                this.DM.executeStatement(query, (err, result) => {
                    cb(null, result);
                });

            });

            executeFunc.push((_o, cb) => {
                //console.log(_o);
                if (_o.length == 0) callback('fail');
                var cardService = "";
                cardService = this.ECR.getVoidCommandByPaymentCode(data.cardType);

                if (data.isUseECR) {
                    this.ECR.checkECR({ service: cardService, till: data.till, traceNumber: _o[0].Trace_Number }, function (result) {
                        if (result.responseCode == 1) {
                            _o.invoice = data.refNo;
                            _o.till = data.till;
                            RAS.ECR.insertECRTransaction(_o, result, function () {
                                cb(null);
                            });
                        } else {
                            callback('fail');
                        }
                    });
                } else {
                    _o.invoice = data.refNo;
                    _o.till = data.till;
                    var res = {};

                    res.detailInObject = {};
                    extend2(res.detailInObject, _o[0]);
                    res.detailInObject.Amount = res.detailInObject.Amount * 100;
                    res.detailInObject.DateTime = moment().format('YYMMDDHHmmss');
                    res.commandCode = cardService;

                    RAS.ECR.insertECRTransaction(_o, res, function () {
                        cb(null);
                    });
                }

            })
        }


        executeFunc.push((cb) => {
            //update [tenderPayment] set void = 1, voidStaff = @staffCode, voidDate = @updateTime where [refNo] = @refNo; update [tenderDiscount] set void = 1, voidstaff =  @staffCode, voidDate = @updateTime where [refNo] = @refNo 
            var sql = 'update [transaction] set void = 1, voidStaff = @staffCode, transTime = @updateTime where [refNo]= @refNo; ';
            var updateTime = new Date();
            var args = [
                { "c": "refNo", "t": TYPES.VarChar, "v": data.refNo },
                { "c": "staffCode", "t": TYPES.VarChar, "v": data.staffCode },
                { "c": "updateTime", "t": TYPES.DateTime, "v": updateTime }
            ];

            var cloud = {};
            cloud.sql = sql;
            cloud.refNo = data.refNo;
            cloud.staffCode = data.staffCode;
            cloud.updateTime = updateTime;

            fs.writeFile("./cloud/voidbill/" + moment().format("YYMMDDHHmmss") + ".json", JSON.stringify(cloud), function (err) {
                if (err) {
                    //console.log(err);
                } else {
                    ////console.log("The file was saved!");
                }
            });

            this.DM.executeStatement({ sql: sql, args: args }, (err, result) => {
                if (err == null) {

                    cb(null);
                }

            })
        });
        executeFunc.push((cb) => {
            this.PrintBill(data, function (r) {
                cb(null);
            });
        });


        async.waterfall(executeFunc, function (result) {
            callback("OK");
        });


    }

    this.getAllPriceBatch = (callback) => {

        var query = { "sql": "select * from priceBatch order by activateDate desc, priceBatchId" }

        this.DM.executeStatement(query, (err, result) => {
            if (err == null) {
                callback(result);
            }
        });

    }

    this.getBatchDetailByBatch = (args, callback) => {
        var q = [
            {
                "sql": "SELECT [code], [name1] FROM [item] union select [optionCode] as [code] , [name1] from [option]",
                "args": []
            },
            {
                "sql": "select * from [priceBatchD] where [priceBatchId] = @priceBatchId order by itemId",
                "args": [{ "c": "priceBatchId", "t": TYPES.Int, "v": args }]

            }
        ];

        this.DM.executeStatement(q, (err, result) => {
            var itemList = result[0];
            var patchDetailList = result[1];

            patchDetailList.forEach(function (elem) {
                var templItem = itemList.filter(function (Item) {
                    return Item.code == elem.itemId;
                });
                if (templItem.length != 0) elem.Item = templItem[0]
            })

            callback(patchDetailList);
            //if(err==null) 
        });
    }


    this.updateBatchDetailItem = (args, callback) => {

        var item = args.detail;
        var batch = args.batch;
        console.log(batch.priceBatchId);
        var sql = "update [priceBatchD] set [price1] = @price1, [price2] = @price2, [price3] = @price3, [price4] = @price4, [price5] = @price5, [timeStart] = @timeStart, [timeEnd] = @timeEnd where [priceBatchId] = @priceBatchId and [itemId] = @itemId";
        var args = [
            { 'c': 'price1', 't': TYPES.Decimal, "v": item.price1 != null ? parseFloat(item.price1).toFixed(2) : null },
            { 'c': 'price2', 't': TYPES.Decimal, "v": item.price2 != null ? parseFloat(item.price2).toFixed(2) : null },
            { 'c': 'price3', 't': TYPES.Decimal, "v": item.price3 != null ? parseFloat(item.price3).toFixed(2) : null },
            { 'c': 'price4', 't': TYPES.Decimal, "v": item.price4 != null ? parseFloat(item.price4).toFixed(2) : null },
            { 'c': 'price5', 't': TYPES.Decimal, "v": item.price5 != null ? parseFloat(item.price5).toFixed(2) : null },
            { "c": "timeStart", "t": TYPES.DateTime, "v": (item.timeStart != null && item.timeStart != undefined) ? moment(item.timeStart).toDate() : null },
            {
                "c": "timeEnd", "t": TYPES.DateTime, "v": (item.timeEnd != null && item.timeEnd != undefined) ? moment(item.timeEnd).toDate() : null
            },
            { "c": "priceBatchId", "t": TYPES.Int, "v": batch.priceBatchId },
            { "c": "itemId", "t": TYPES.VarChar, "v": item.itemId }
        ]

        this.DM.executeStatement({ sql: sql, args: args }, (err, result) => {
            if (err == null) callback('OK');
        });

    }

    this.getAllNotInBatchDetailItem = (args, callback) => {
        var q = {
            'sql': "select *, '' as price  from (SELECT [code], [name1] FROM [item] union select [optionCode] as [code] , [name1] from [option] ) as temp where temp.code not in (select itemId from priceBatchD where priceBatchId = @priceBatchId) order by code",
            args: [{ "c": "priceBatchId", "t": TYPES.Int, "v": args }]
        }

        this.DM.executeStatement(q, (err, result) => {
            if (err == null) callback(result);
        })
    }

    this.removeDetailItemByBatch = (args, callback) => {
        var sql = "delete from priceBatchD where [priceBatchId] = @priceBatchId and [itemId] = @itemId";
        var args = [
            { "c": "priceBatchId", "t": TYPES.Int, "v": args.priceBatchId },
            { "c": "itemId", "t": TYPES.VarChar, "v": args.itemId }
        ]

        this.DM.executeStatement({ sql: sql, args: args }, (err, result) => {
            if (err == null) callback('OK');
        })
    }

    this.insertDetailItemByBatch = (data, callback) => {
        console.log(data);
        var sql = "Insert into priceBatchD (priceBatchId, priceZoneId, itemId, price1) VALUES (@priceBatchId, @priceZoneId, @itemId, @price1)";
        var args = [
            { "c": "priceBatchId", "t": TYPES.Int, "v": data.priceBatchId },
            { "c": "priceZoneId", "t": TYPES.Int, "v": data.priceBatchId },
            { "c": "itemId", "t": TYPES.VarChar, "v": data.itemId },
            { "c": "price1", "t": TYPES.Decimal, "v": (data.price1 != null && data.price1 != '') ? parseFloat(data.price1).toFixed(2) : null }
        ]

        this.DM.executeStatement({ sql: sql, args: args }, (err, result) => {
            if (err == null) callback('OK');
        })
    }

    this.updatePriceBatch = (args, callback) => {
        var sql = "update [priceBatch] set [name] = @name, [activateDate] = @activateDate, [expireDate] = @expireDate where [priceBatchId] = @priceBatchId";
        var args = [
            { "c": "priceBatchId", "t": TYPES.Int, "v": args.priceBatchId },
            { "c": "name", "t": TYPES.NVarChar, "v": args.name },
            { "c": "activateDate", "t": TYPES.DateTime, "v": moment(args.activateDate).toDate() },
            { "c": "expireDate", "t": TYPES.DateTime, "v": (args.expireDate != null && args.expireDate != undefined) ? moment(args.expireDate).toDate() : null },
        ];
        this.DM.executeStatement({ sql: sql, args: args }, (err, result) => {
            if (err == null) callback('OK');
        })
    }
    this.insertPriceBatch = (args, callback) => {
        var sql = "insert into [priceBatch] (priceBatchId, name, activateDate, expireDate) values ((select MAX(priceBatchId) from priceBatch) +1, @name, @activateDate, @expireDate)";
        var args = [
            { "c": "name", "t": TYPES.NVarChar, "v": args.name },
            { "c": "activateDate", "t": TYPES.DateTime, "v": moment(args.activateDate).toDate() },
            { "c": "expireDate", "t": TYPES.DateTime, "v": (args.expireDate != null && args.expireDate != undefined) ? moment(args.expireDate).toDate() : null },
        ];
        this.DM.executeStatement({ sql: sql, args: args }, (err, result) => {
            if (err == null) callback('OK');
        })

    }

    this.token = {
        pending: [],
        success: []
    }

    this.BillTableOrderByTakeOut = (args, callback) => {
        //console.log(13115, 'BillTableOrderByTakeOut');
        //console.log(args.tableNum.$value);
        var callbackObj = {};
        var tableObj = getTableCode(args.tableNum.$value);
        if (!tableObj) tableObj = { tableCode: '', tableSubCode: '' }
        var tableCode = tableObj.tableCode;
        var tableSubCode = tableObj.tableSubCode;
        var noOfPeople = null;

        args.shopCode = config.shopCode;
        args.tableObj = { tableCode: "", tableSubCode: "" };
        args.tableObj.tableCode = tableObj.tableCode;
        args.tableObj.tableSubCode = tableObj.tableSubCode;

        var totalPayment = 0;
        var serviceCharge = parseFloat(args.serviceCharge.$value);
        var remainings = parseFloat(args.remainings.$value);
        var staff = args.user.$value;

        var printingArray = { TableNo: null, Pax: null, FoodItem: [] };
        //var FoodItem = {Name:null, Qty:null, Printer:null, Code:null, Time:null, Msg:null, individualPrint:false,Type:null}

        /*********************************/
        var orderKds = null;
        var checkStockArray = [];
        var commitItemStockArray = [];
        var transactionDStatement = [];
        var r = args.itemData.json;
        var currentSeq = 0;
        var currentRound = 1;
        var date = new Date();
        var totalPrice = 0;
        var totalPriceForServiceCharge = 0;
        //var serviceCharge = 0; ?
        var lastIsServiceCharge = true;
        var lastOrder = '';
        var lastOrderWarning = '';
        var leaveTime = '';
        var lastItemQty = 0;
        var cash = false;
        /*********************************/


        for (var i = 0 ; i < r.order.item.length; i++) {
            var vi = r.order.item[i].voidIndex == undefined ? "-1" : r.order.item[i].voidIndex[0];
            var vr = r.order.item[i].voidRemark == undefined ? "" : r.order.item[i].voidRemark[0];
            var approveStaff = r.order.item[i].voidApproveStaff == undefined || null ? "" : r.order.item[i].voidApproveStaff[0];
            var type = r.order.item[i].type[0];
            //var serviceCharge = true;
            var itemOrderTime = moment().format("HH:mm:ss");
            if (type != "M" && type != "K") {
                currentSeq += 1;
            }
            if (type == "T" && vi == "-1") {
                var items = RAS.Item.filter(function (entry) {
                    return entry.code == r.order.item[i].code[0];
                });
                var _lastOrder = items[0].namek.split(',');
                if (_lastOrder.length > 1) {
                    lastOrderWarning = _lastOrder[0];
                    lastOrder = _lastOrder[1];
                    leaveTime = _lastOrder[2];
                }
            }

            //console.log(currentSeq)
            ////console.log(r.order.item[i].unitPrice[0])
            var customName = '';
            if (r.order.item[i].customName) {
                if (r.order.item[i].customName[0]) {
                    customName = r.order.item[i].customName[0]
                }
            }
            var itemType = r.order.item[i].type[0];
            var itemCode = r.order.item[i].code[0]
            var items = [];

            var items = getItemByItem({ itemId: r.order.item[i].code[0], type: r.order.item[i].type[0] });
            var namek = "";

            var kds = null;
            var _sc = true;
            if (items.length > 0) {
                if (items[0].serviceCharge != undefined) {
                    _sc = items[0].serviceCharge;
                };
                if (items[0].kds) {
                    kds = items[0].kds;
                };
                if (itemType != "M" && type != "K") {
                    lastPrinter = items[0].printer;
                    if (items[0].printerGroup) {
                        lastPrinter += "*" + items[0].printerGroup;
                    }
                    lastKds = kds;
                }
                if (kds == null || config.kdsAndPrinting) {
                    var vr = r.order.item[i].voidRemark == undefined ? "" : r.order.item[i].voidRemark[0];
                    //var printer = items[0].printer == undefined ? "" : items[0].printer;
                    //if (items[0].printerGroup) {
                    //    printer += "*" + items[0].printerGroup;
                    //}


                    namek = items[0].namek;
                    if (customName != "") {
                        namek = customName;
                    }

                    printingArray.FoodItem.push({ Name: namek, Qty: r.order.item[i].qty[0], Printer: lastPrinter, Code: items[0].code, Time: itemOrderTime, Msg: vr, individualPrint: items[0].individualPrint, Type: type });
                }
            }

            if (itemType == "I") {
                lastIsServiceCharge = _sc;
                lastItemQty = parseInt(r.order.item[i].qty[0]);
                totalPrice += parseFloat((parseFloat(r.order.item[i].unitPrice[0]) / 100).toFixed(2)) * lastItemQty;
                if (_sc && lastIsServiceCharge) {
                    totalPriceForServiceCharge += parseFloat((parseFloat(r.order.item[i].unitPrice[0]) / 100).toFixed(2)) * lastItemQty;
                }
            } else {
                totalPrice += parseFloat((parseFloat(r.order.item[i].unitPrice[0]) / 100).toFixed(2)) * lastItemQty * r.order.item[i].qty[0];
                if (_sc && lastIsServiceCharge) {
                    totalPriceForServiceCharge += parseFloat((parseFloat(r.order.item[i].unitPrice[0]) / 100).toFixed(2)) * lastItemQty * r.order.item[i].qty[0];
                }


            }

            if (kds != null) {
                orderKds = kds;
            }

            if (type == "I" && args.insertType == "takeAway") {
                _sc = false;
            }

            _o = {
                "sql": "insert into transactionD (itemId,qty,price,voidIndex,voidRemark,time, staffId, type,seq,round,refNo, customName,approveStaffId,serviceCharge) values (@itemId,@qty,@price, @voidIndex,@voidRemark, @time ,@staffId,@type,@seq,@round,'##refNo##',@customName,@approveStaffId,@serviceCharge); \
                        insert into kds (itemId,qty,voidIndex,type,seq,time,round,printer,kdsgroup,kds, orderNo) values (@itemId,@qty,@voidIndex,@type,@seq,@time,@round,@printer,@kdsgroup,@kds,'##refNo##'); ",
                "args": [{ "c": "itemId", "t": TYPES.VarChar, "v": itemCode },
                            { "c": "qty", "t": TYPES.Int, "v": parseInt(r.order.item[i].qty[0]) },
                            { "c": "price", "t": TYPES.Decimal, "v": parseFloat((parseFloat(r.order.item[i].unitPrice[0]) / 100).toFixed(2)), "o": { "scale": 2 } },
                            { "c": "voidIndex", "t": TYPES.Int, "v": vi },
                            { "c": "voidRemark", "t": TYPES.NVarChar, "v": vr },
                            { "c": "type", "t": TYPES.VarChar, "v": type },
                            { "c": "seq", "t": TYPES.Int, "v": currentSeq },
                            { "c": "time", "t": TYPES.Time, "v": date },
                            { "c": "staffId", "t": TYPES.VarChar, "v": staff },
                            { "c": "round", "t": TYPES.Int, "v": currentRound },
                            { "c": "customName", "t": TYPES.NVarChar, "v": customName },
                            { "c": "approveStaffId", "t": TYPES.VarChar, "v": approveStaff },
                            { "c": "serviceCharge", "t": TYPES.Bit, "v": _sc },
                            { "c": "printer", "t": TYPES.VarChar, "v": lastPrinter },
                            { "c": "kdsgroup", "t": TYPES.BigInt, "v": '-1' },
                            { "c": "kds", "t": TYPES.VarChar, "v": lastKds }
                ]
            }

            //console.log(15610, r.order.item[i]);
            transactionDStatement.push(_o);


            try {
                checkStockArray.push({
                    sql: "select itemCode,qty , @dname as \"name\" from itemControl where itemCode = @itemCode and (qty < 0 or qty < @qty)"
                    , args: [{ c: 'dname', t: TYPES.NVarChar, v: r.order.item[i].desc1[0] }, { c: 'itemCode', t: TYPES.VarChar, v: itemCode }, { c: 'qty', t: TYPES.Int, v: parseInt(r.order.item[i].qty[0]) }]
                })

                //console.log(15618, r.order.item[i]);
                commitItemStockArray.push({ sql: "update itemControl set qty = qty - @qty where itemCode = @itemCode", args: [{ c: 'itemCode', t: TYPES.VarChar, v: itemCode }, { c: 'qty', t: TYPES.Int, v: parseInt(r.order.item[i].qty[0]) }] });
            } catch (e) {
                //console.log(15619,e);
            }

        }

        if (args.setPaymentForPayLater) checkStockArray = [];

        this.DM.executeStatement(checkStockArray, (err, result) => {

            //console.log(15625,err);
            var isValid = true;
            var errorMsg = ""; var outOfStockList = [];
            var remarkList = [];
            for (var i = 0; i < result.length; i++) {
                if (result[i].length != 0) {
                    isValid = false;
                    outOfStockList.push(result[i][0].itemCode);
                }
            }

            var refType = "";
            var progType = "";
            if (args.insertType == "takeAway") {
                refType = "-TO";
                progType = "takeout";
            }
            else if (args.insertType == "deliveryOrder") {
                refType = "-D";
                progType = "delivery";
            } else if (args.insertType == "fastFood") {
                if (args.isTakeout) refType = "-FT";
                else refType = "-FD";
                progType = "fastfood";
            }

            // generate seq
            var executeArr = [];

            function requestInvoiceSeq(cb) {
                if (args.retryRefNo && args.retryRefNo.$value) {
                    cb(null, { 'invoice': args.retryRefNo.$value, 'invoiceSeq': parseInt(args.retryRefNo.$value.substring(args.retryRefNo.$value.lastIndexOf('-') + 1)) });
                }
                else {
                    var query = {
                        "sql": "declare @seq int EXEC @seq = spGetNextSeq '" + progType + "' \
    declare @sseq varchar(10) = REPLICATE('0',6-LEN(RTRIM(@seq))) + RTRIM(@seq) \
    select 'invoice' = @refNo + @sseq, 'invoiceSeq' = @seq;",
                        "args": [
                            { "c": "refNo", "t": TYPES.VarChar, "v": config.shopCode + refType + moment().format("-YYMMDD-") }

                        ]
                    }
                    RAS.DM.executeStatement(query, (err, result) => {
                        //console.log(JSON.stringify(query));
                        //console.log('gen seq', result);
                        //console.log(13337, result[0]);
                        //callback();
                        //return;
                        cb(null, result[0]);

                    });
                }
            }

            function existingInvoiceSeqForPayLater(cb) {
                var tmp = {
                    invoice: "",
                    invoiceSeq: ""
                }

                tmp.invoice = args.payLaterRefNo;
                var tmpSplitList = tmp.invoice.split('-');
                tmp.invoiceSeq = Number(tmpSplitList[tmpSplitList.length - 1]);

                console.log(13362, tmp);
                cb(null, tmp)
            }

            if (args.setPaymentForPayLater) executeArr.push(existingInvoiceSeqForPayLater);
            else executeArr.push(requestInvoiceSeq);

            // check if has octopus
            var remark = "";

            args.payMethod.$value.forEach((payMethod, idx) => {
                if (payMethod.method === "octopus2") {
                    executeArr.push((_o, cb) => {
                        //var params = extend2({
                        //    till: args.till.$value,
                        //    totalPrice: payMethod.amount,
                        //    delay: args.delay ? args.delay : 3
                        //}, _o);
                        //this.DeductOctopus(params, (result) => {
                        //    if (result.octopusCode <= 100000) {
                        //        _o.octopusBalance = result.cardBalance;
                        //        remark = result.cardBalance + "|" + parseFloat(result.totalPrice).toFixed(2) + "|" + result.cardId + "|" + result.machineId;
                        //        remarkList[idx] = remark;

                        //        cb(null, _o);
                        //    }
                        //    else {

                        //        callback(result);
                        //    }
                        //});
                        var params = extend2({
                            service: this.Octopus.service.Octopus_deduct,
                            till: args.till.$value,
                            totalPrice: payMethod.amount
                        }, _o);
                        this.Octopus.connect(params, (result) => {
                            if (result.octopusCode <= 100000) {
                                _o.octopusBalance = result.cardBalance;
                                remark = result.cardBalance + "|" + parseFloat(result.totalPrice).toFixed(2) + "|" + result.cardId + "|" + result.machineId + "|" + result.lastAddValue + "|" + result.alertMessage;
                                remarkList[idx] = remark;

                                cb(null, _o);
                            }
                            else {

                                callback(result);
                            }
                        });
                    });
                }

                if (payMethod.tenderType == 'card') {

                    var cardService = ""
                    switch (payMethod.method) {
                        case "visa": case "master": default:
                            cardService = this.ECR.service.EDC_Sales;
                            break
                        case "unionpay":
                            cardService = this.ECR.service.CUP_Sales;
                            break;
                        case "eps":
                            cardService = this.ECR.service.EDC_EPS;
                            break;
                    }

                    if (args.isUseECR) {

                        executeArr.push((_o, cb) => {
                            var req = { totalPrice: payMethod.amount, service: cardService, till: args.till.$value };
                            extend2(req, _o);
                            this.ECR.checkECR(req, (result) => {
                                if (result.responseCode == 0) {
                                    callback({ result: 'failInCard' });


                                } else if (result.responseCode == 1) {


                                    this.ECR.insertECRTransaction(req, result, function (result2) {
                                        var remark = null;
                                        if (result.detail) remark = result.detail;
                                        if (result.detailInObject.Trace_Number) remark = result.detailInObject.Trace_Number + "##|##" + remark;
                                        remarkList[idx] = remark;
                                        cb(null, extend2(req, result));


                                    })


                                }
                            })
                        });


                    } else if (args.isUseKeypad) {

                        executeArr.push((_o, cb) => {

                            var res = {};
                            res.detailInObject = {};
                            res.detailInObject.ECR_Ref = null;
                            res.detailInObject.Amount = args.keypad.amount * 100;
                            res.detailInObject.Tips = 0;
                            res.detailInObject.Resp_Code = null;
                            res.detailInObject.Resp_Text = null;
                            res.detailInObject.DateTime = moment().format('YYMMDDHHmmss');
                            res.detailInObject.Card_Type = null;
                            res.detailInObject.Card_No = null;
                            res.detailInObject.Expiry_Date = null;
                            res.detailInObject.Card_Holder = null;
                            res.detailInObject.Terminal_Number = null;
                            res.detailInObject.Merchant_Number = null;
                            res.detailInObject.Trace_Number = args.keypad.traceNumber;
                            res.detailInObject.Batch_Number = args.keypad.batchNumber;
                            res.detailInObject.Retrieval_Reference = null;
                            res.commandCode = cardService;

                            var req = { totalPrice: payMethod.amount, service: cardService, till: args.till.$value }
                            extend2(req, _o);
                            RAS.ECR.insertECRTransaction(req, res, function (result2) {
                                var remark = "";
                                if (res.detailInObject.Trace_Number) remark = res.detailInObject.Trace_Number + "##|##" + remark;
                                remarkList[idx] = remark;
                                cb(null, extend2(req, res));
                            })
                        })

                    }

                }
            });

            if (isValid) {

                executeArr.push((_o, cb) => {
                    var query = [];
                    //                    var sql = "begin tran begin try \
                    //declare @seq int EXEC @seq = spGetNextSeq '" + progType + "' \
                    //declare @sseq varchar(10) = REPLICATE('0',6-LEN(RTRIM(@seq))) + RTRIM(@seq) \
                    //insert into [transaction]  (tableCode, tableSubCode, pax, staffId,  startTime, refNo, transTime, payAmount, price, serviceCharge, remainings,orderid) values(@tableCode, @tableSubCode, 0, @staffId, getDate(), @refNo+ @sseq, getDate(), @payAmount, @price, @serviceCharge, @remainings, @orderid); select *, 'invoiceSeq' = @seq from [transaction] where refNO = @refNo+ @sseq; ";

                    var sql = "begin tran begin try \
insert into [transaction]  (tableCode, tableSubCode, pax, staffId,  startTime, refNo, transTime, payAmount, price, serviceCharge, remainings,orderid , till) values(@tableCode, @tableSubCode, 0, @staffId, getDate(), @refNo, getDate(), @payAmount, @price, @serviceCharge, @remainings, @orderid, @till); select * from [transaction] where refNO = @refNo; insert into [kdsOrders] (orderNo, time, kdsStatus) VALUES (@refNo, getDate(), 0)";



                    var totalPrice = parseFloat(args.price.$value);



                    var sqlagrs = [{ "c": "tableCode", "t": TYPES.VarChar, "v": null },
                                    { "c": "tableSubCode", "t": TYPES.VarChar, "v": null },
                                    //{ "c": "refNo", "t": TYPES.VarChar, "v": config.shopCode + refType + moment().format("-YYMMDD-") },
                                    { "c": "refNo", "t": TYPES.VarChar, "v": _o.invoice },
                                    { "c": "serviceCharge", "t": TYPES.Decimal, "v": serviceCharge, "o": { "scale": 2 } },
                                    { "c": "remainings", "t": TYPES.Decimal, "v": remainings, "o": { "scale": 2 } },
                                    { "c": "staffId", "t": TYPES.VarChar, "v": args.user.$value },
                                    { "c": "orderid", "t": TYPES.VarChar, "v": moment().format("YYMMDDHHmmssSS") },
                                    { "c": "till", "t": TYPES.VarChar, "v": args.till.$value }

                    ];

                    if (args.isPayLater) {
                        sql = "begin tran begin try \
                            insert into [transaction]  (tableCode, tableSubCode, pax, staffId,  startTime, refNo, transTime, payAmount, price, serviceCharge, remainings,orderid , till, customerData, paymentLater, serveTime, isDelivery) values(@tableCode, @tableSubCode, 0, @staffId, getDate(), @refNo, getDate(), @payAmount, @price, @serviceCharge, @remainings, @orderid, @till, @customerData, 1,@serveTime, @isDelivery ); select * from [transaction] where refNO = @refNo; insert into [kdsOrders] (orderNo, time, kdsStatus) VALUES (@refNo, getDate(), 0); IF NOT EXISTS (SELECT  1 FROM customer  WHERE   phone = @phone  AND  address = @address) BEGIN INSERT into customer (phone, name, address) VALUES (@phone, @name, @address) END;";
                        //where Not exist ( select * from customer where phone=@phone and address=@address )

                        if (!args.customerData.time) args.customerData.time = moment().toDate();

                        args.customerData.lastUpdatedBy = args.user.$value;
                        args.customerData.lastUpdatedTime = moment().format('YYYY-MM-DD HH:mm:ss');
                        sqlagrs.push({ "c": "customerData", "t": TYPES.NVarChar, "v": JSON.stringify(args.customerData) })
                        sqlagrs.push({ "c": "serveTime", "t": TYPES.Time, "v": args.customerData.time });
                        sqlagrs.push({ "c": "isDelivery", "t": TYPES.Bit, "v": args.customerData.isDelivery });
                        sqlagrs.push({ "c": "phone", "t": TYPES.VarChar, "v": args.customerData.phone });
                        sqlagrs.push({ "c": "address", "t": TYPES.VarChar, "v": args.customerData.address });
                        sqlagrs.push({ "c": "name", "t": TYPES.VarChar, "v": args.customerData.name });
                    }
                    if (args.setPaymentForPayLater) sql = " begin tran begin try ";


                    var PaymentMethod = new Array;
                    var PaymentMethodItem = new Object;
                    var paymentDate = moment().format('YYYY-MM-DD HH:mm:ss');

                    for (var i = 0; i < args.payMethod.$value.length; i++) {
                        //console.log("paymentValue" + parseFloat(args.payMethod.$value[i].amount));
                        //console.log("paymentValue" + args.payMethod.$value[i].method);
                        var payseq = i + 1;
                        //sql += "insert into tenderpayment (paymentType,paymentValue,staffId,refNo,paymentDate,void,seq, till) values (@paymentType" + i.toString() + ",@paymentValue" + i.toString() + ",@staffId,@refNo + @sseq,getDate(),0," + payseq + ", @till" + i.toString() + ");";
                        sql += "insert into tenderpayment (paymentType,paymentValue, priceValue,staffId,refNo,paymentDate,void,seq, till, remark) values (@paymentType" + i.toString() + ",@paymentValue" + i.toString() + ", @priceValue" + i.toString() + ",@staffId,@refNo,getDate(),0," + payseq + ", @till" + i.toString() + ", @remark" + i.toString() + ");";
                        sqlagrs.push({ "c": "paymentType" + i.toString(), "t": TYPES.VarChar, "v": args.payMethod.$value[i].method });
                        sqlagrs.push({ "c": "paymentValue" + i.toString(), "t": TYPES.Decimal, "v": parseFloat(args.payMethod.$value[i].amount), "o": { "scale": 2 } });
                        var priceValue = args.payMethod.$value[i].totalAmount;
                        if (priceValue) priceValue = parseFloat(priceValue);
                        else priceValue = null;
                        sqlagrs.push({ "c": "priceValue" + i.toString(), "t": TYPES.Decimal, "v": priceValue });
                        sqlagrs.push({ "c": "till" + i.toString(), "t": TYPES.VarChar, "v": args.till.$value });
                        sqlagrs.push({ "c": "remark" + i.toString(), "t": TYPES.NVarChar, "v": remarkList[i] ? remarkList[i] : null });
                        totalPayment += parseFloat(args.payMethod.$value[i].amount);
                        PaymentMethodItem = new Object;
                        PaymentMethodItem.paymentType = args.payMethod.$value[i].method;
                        PaymentMethodItem.paymentValue = parseFloat(args.payMethod.$value[i].amount);
                        PaymentMethodItem.paymentDate = new Date();
                        PaymentMethodItem.paymentDate = paymentDate
                        PaymentMethodItem.void = 0;
                        PaymentMethodItem.seq = payseq;
                        PaymentMethod.push(PaymentMethodItem);
                        if (args.payMethod.$value[i].method == "cash") cash = true;
                    }

                    var tenderDiscount = new Array;
                    var tenderDiscountItem = new Object;

                    for (var i = 0; i < args.discountMethod.$value.length; i++) {
                        var disseq = i + 1;
                        //sql += "insert into tenderdiscount (discountType,discountValue,discountCode,staffId,refNo,discountDate,void,seq,discountAmount) values (@discountType" + i.toString() + ",@discountValue" + i.toString() + ",@discountCode" + i.toString() + ",@staffId,@refNo + @sseq,getDate(),0," + payseq + ",@discountAmount" + i.toString() + ");";
                        sql += "insert into tenderdiscount (discountType,discountValue,discountCode,staffId,refNo,discountDate,void,seq,discountAmount) values (@discountType" + i.toString() + ",@discountValue" + i.toString() + ",@discountCode" + i.toString() + ",@staffId,@refNo,getDate(),0," + disseq + ",@discountAmount" + i.toString() + ");";
                        sqlagrs.push({ "c": "discountType" + i.toString(), "t": TYPES.VarChar, "v": args.discountMethod.$value[i].type })
                        sqlagrs.push({ "c": "discountValue" + i.toString(), "t": TYPES.Decimal, "v": parseFloat(args.discountMethod.$value[i].price), "o": { "scale": 2 } })
                        sqlagrs.push({ "c": "discountCode" + i.toString(), "t": TYPES.VarChar, "v": args.discountMethod.$value[i].coupon_alias })
                        sqlagrs.push({ "c": "discountAmount" + i.toString(), "t": TYPES.Decimal, "v": parseFloat(args.discountMethod.$value[i].amount), "o": { "scale": 2 } })
                        //totalPrice += parseFloat(args.discountMethod.$value[i].price)
                        tenderDiscountItem = new Object;
                        tenderDiscountItem.discountType = args.discountMethod.$value[i].type;
                        tenderDiscountItem.discountValue = parseFloat(args.discountMethod.$value[i].price);
                        tenderDiscountItem.discountCode = args.discountMethod.$value[i].coupon_alias;
                        tenderDiscountItem.discountDate = paymentDate;
                        tenderDiscountItem.void = 0;
                        tenderDiscountItem.seq = disseq;
                        tenderDiscountItem.discountAmount = parseFloat(args.discountMethod.$value[i].amount);
                        tenderDiscount.push(tenderDiscountItem);
                    }


                    sqlagrs.push({ "c": "price", "t": TYPES.Decimal, "v": totalPrice, "o": { "scale": 2 } })
                    sqlagrs.push({ "c": "payAmount", "t": TYPES.Decimal, "v": totalPayment, "o": { "scale": 2 } })
                    sql += "commit tran end try begin catch rollback tran; THROW; end catch;";

                    //console.log(13573, sql);
                    query.push({ sql: sql, args: sqlagrs });

                    this.DM.executeStatement(query, (err, result) => {

                        var refNo = "";
                        if (args.setPaymentForPayLater) refNo = args.payLaterRefNo;
                        else refNo = result[0][0].refNo
                        //var invoiceSeq = result[0][0].invoiceSeq;
                        var invoiceSeq = _o.invoiceSeq;
                        var octopusBalance = _o.octopusBalance;

                        transactionDStatement.forEach(function (e) {
                            e.sql = e.sql.replace(/##refNo##/g, refNo);
                        })

                        //console.log(orderDList);
                        //        sql += "insert into transactionD (tableCode, tableSubCode, itemId, qty, price, discount, voidIndex, voidRemark, time, staffId, type, seq, round, refNo, customName, approveStaffId,serviceCharge ) select tableCode, tableSubCode, itemId, qty, price, discount, voidIndex, voidRemark, time, staffId, type, seq, round, @refNo + @sseq as refNo, customName, approveStaffId,serviceCharge from tableOrderD where tableCode = @tableCode and tableSubCode = @tableSubCode; \
                        //update tableOrder set status = 4 where  tableCode = @tableCode and tableSubCode = @tableSubCode;\
                        //delete tableOrder where tableCode = @tableCode and not exists (select * from tableorder where tableCode = @tableCode and (status != 4 or status is null)); \
                        //delete tableOrderD where tableCode = @tableCode and not exists (select * from tableorder where tableCode = @tableCode and (status != 4 or status is null)); \
                        //delete tablediscount where tableCode = @tableCode and not exists (select * from tableorder where tableCode = @tableCode and (status != 4 or status is null));\
                        //        update diningTable set status = 0 where name = @tableCode and not exists (select * from tableorder where tableCode = @tableCode and (status != 4 or status is null));\
                        //select @refNo + @sseq as refNo \
                        //commit tran end try begin catch rollback tran; THROW; end catch "
                        //        var query = [
                        //            {
                        //                "sql": "select * from tableOrderD where tableCode = @tableCode and tableSubCode = @tableSubCode;", "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": tableCode }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": tableSubCode }]
                        //            },
                        //            { "sql": "select * from tableOrder where tableCode = @tableCode and tableSubCode = @tableSubCode;", "args": [{ "c": "tableCode", "t": TYPES.VarChar, "v": tableCode }, { "c": "tableSubCode", "t": TYPES.VarChar, "v": tableSubCode }] }
                        //            ,
                        //            { "sql": sql, "args": sqlagrs }];
                        //        console.log('sql3', sql);
                        var AzureObject = new Object;
                        AzureObject.paymentArgs = new Object;
                        AzureObject.paymentArgs = args;
                        AzureObject.TransactionD = new Array;
                        AzureObject.TableOrder = new Array;
                        AzureObject.PayAmount = totalPayment;
                        AzureObject.Price = totalPrice;
                        AzureObject.ServiceCharge = serviceCharge;
                        AzureObject.Remainings = remainings;
                        AzureObject.PaymentMethod = new Array;
                        AzureObject.PaymentMethod = PaymentMethod;
                        AzureObject.tenderDiscount = new Array;
                        AzureObject.tenderDiscount = tenderDiscount;

                        if (args.setPaymentForPayLater) {
                            transactionDStatement = [];
                        }

                        this.DM.executeStatement(transactionDStatement, (err, result) => {

                            if (err == null) {

                                printingArray.TableNo = refNo;
                                printingArray.Pax = 0;
                                printingArray.isTakeAway = true;
                                printingArray.till = args.till.$value;
                                console.log("refNorefNo", refNo);
                                console.log("printingArray", printingArray);



                                var labelObject = {};
                                labelObject = JSON.parse(JSON.stringify(printingArray))
                                console.log("labelObject1", labelObject);

                                var labelPrinter = config.tills[args.till.$value].labelPrinter;

                                if (!args.isPayLater)
                                    this.PrintPaymentBill({ 'refNo': refNo, openCashDrawer: true, printer: args.printer.$value, till: args.till.$value, isCalledByTakeaway: true }, () => {
                                    })

                                if (!args.setPaymentForPayLater) {
                                    this.PrintFoodItem(printingArray);

                                    if (labelPrinter) {
                                        labelObject.FoodItem.forEach(function (food) {
                                            food.Printer = labelPrinter;
                                        })
                                        console.log("labelObject2", labelObject);
                                        RAS.PrintFoodItem(labelObject, true);
                                    }
                                } else {
                                    commitItemStockArray = [];
                                }


                                //fs.writeFile("./printer/" + moment().format("YYMMDDHHmmssSSSS") + ".json", JSON.stringify(printingArray), function (err) {
                                //    if (err) {
                                //        //console.log(err);
                                //    } else {
                                //        ////console.log("The file was saved!");
                                //    }

                                //});

                                //this.PrintPaymentBill({ 'refNo': refNo, openCashDrawer: true, printer: args.printer.$value }, () => {

                                //});



                                //if (cash)
                                //    this.OpenCashBox({ "till": args.till.$value }, () => { })


                                this.DM.executeStatement(commitItemStockArray, (err, result) => {
                                    //console.log(err);
                                    this.GetOutOfStock((err, result) => {
                                        io.sockets.emit('refreshItemQty', JSON.stringify(result));
                                        var _o = { "action": "refreshItemQty", "outOfStock": result }
                                        mqttClient.publish("wo/gm/all", JSON.stringify(_o));

                                        var _result = "OK";
                                        var si = [];
                                        var siq = [];
                                        result.forEach((item) => {
                                            si.push(item.code);
                                            siq.push(item.qty);
                                        });
                                        console.log("si", si);
                                        callbackObj = { result: _result, timestamp: new Date().getTime(), taskID: 1, note: '', suspendItems: si.toString(), suspendItemsQty: siq.toString(), totalPrice: totalPrice, invoiceSeq: invoiceSeq, invoice: refNo };
                                        //callback(null, callbackObj);
                                        cb(null, callbackObj);

                                        if (orderKds) {
                                            this.showKds(orderKds);
                                        }

                                    });
                                }, "parallel");
                            }
                            /*if (refNo != "") {
                                var temp = tableCode;
                                if (tableSubCode != "") {
                                    temp += "_" + tableSubCode;
                                }
                                var temp = refNo;
                                //'publish', { topic: "wo/gm/all", msg: "{\"action\":\"unlinkTable\",\"tableno\":\"" + $('#tno').val() + "\"}" 
                                mqttClient.publish("wo/gm/table/" + temp + "/", JSON.stringify({ "action": "unlinkTable", "tableno": temp }), { qos: 2 });
                                mqttClient.publish("wo/kds/all/", JSON.stringify({ "action": "reload" }), { qos: 2 });
                                //mqttClient.publish("wo/gm/tabletest/test" + temp + "/", JSON.stringify({ "action": "stressLoadOrder" }), { qos: 2 });
                                //mqttClient.publish("wo/gm/all", JSON.stringify({ "action": "stressLoadOrder" }), { qos: 2 });
                            }*/


                            //callback({ "result": refNo == "" ? "fail" : "OK", "refNo": refNo, "tableCode": tableCode, "tableSubCode": tableSubCode });
                            cb({ "result": refNo == "" ? "fail" : "OK", "refNo": refNo, "tableCode": tableCode, "tableSubCode": tableSubCode, "octopusBalance": octopusBalance });
                        }, 'series');

                    }, 'parallel');
                });
            }
            else {

                executeArr.push((_o, cb) => {
                    this.GetOutOfStock((err, result) => {
                        var note = "<note></note>";
                        var rItemSuspendInfo = "<suspendItems>" + this.OutOfStock + "</suspendItems>";
                        if (outOfStockList.length != 0) {
                            // note = "<note>outOfStock</note>";
                            note = "outOfStock";
                        }
                        var si = [];
                        var siq = [];
                        result.forEach((item) => {
                            si.push(item.code);
                            siq.push(item.qty);
                        });
                        console.log("si", si);
                        callbackObj = { result: 'ERROR', timestamp: new Date().getTime(), taskID: 1, note: note, suspendItems: si.toString(), suspendItemsQty: siq.toString() };
                        //callback(null, callbackObj);
                        cb(null, callbackObj);
                    });
                });
            }

            async.waterfall(executeArr, function (result) {
                if (args.setPaymentForPayLater) {
                    RAS.paymentSetForPayLater({ refNo: args.payLaterRefNo });
                }

                console.log(13777);
                if (args.isPayLater) result.paymentLater = true;
                callback(result);
            });

        }, 'parallel');

    }

    this.ECR = {
        checkECR: function (_obj, callback) {

            console.log('ECR', _obj.service, JSON.stringify(_obj));
            var obj = {};
            var actionCode = _obj.till + _obj.service + moment().format('YYMMDDHHmmss');

            switch (_obj.service) {
                case this.service.EDC_Refund:
                case this.service.EDC_Sales:
                case this.service.EDC_EPS:
                case this.service.CUP_Refund:
                case this.service.CUP_Sales:
                case this.service.CUP_EPS_Sales:
                    obj = { 'seq': _obj.invoiceSeq, 'amount': _obj.totalPrice, 'service': _obj.service };
                    break;
                case this.service.EDC_Settlement:
                case this.service.CUP_Batch:
                case this.service.CUP_Trace:
                case this.service.CUP_Settlement:
                    obj = { 'service': _obj.service }
                    break;
                case this.service.EDC_EPS_Void:
                case this.service.CUP_Void:
                case this.service.CUP_EPS_Void:
                    obj = { 'service': _obj.service, 'traceNumber': _obj.traceNumber };
                    break;


            }

            obj.actionCode = actionCode;
            fs.writeFile("./ecr/" + _obj.till + "/req/" + _obj.service + "_" + moment().format('YYMMDDHHmmss') + ".json", JSON.stringify(obj), function (err) {
                if (err) {
                    console.log(err);
                    callback();
                }
                else {
                    var path = "./ecr/" + _obj.till + "/res/";
                    var responseObj = {};
                    var haveRespone = false;

                    var checkTimeout;

                    var interval = setInterval(function () {
                        if (haveRespone) {
                            clearInterval(interval);
                            clearTimeout(checkTimeout);
                            callback(responseObj);
                        }
                        try {
                            fs.readdir(path, function (err, files) {
                                files = files.filter(function (filename) {
                                    return filename.indexOf('.json') != -1
                                });
                                if (files.length == 0) {
                                    return;
                                }
                                files.forEach(function (file) {
                                    var fullPath = path + file;
                                    var cpyFullPath = path + "backup/" + file;

                                    //var obj = fs.readFileSync(fullPath, { 'encoding': 'utf8' });
                                    var obj = fs.readFile(fullPath, { 'encoding': 'utf8' }, function (err, data) {
                                        var output = JSON.parse(data);
                                        if (output.actionCode != actionCode || output.date != moment().format('YYYYMMDD')) return;
                                        responseObj = output;
                                        fs.unlink(fullPath);
                                        fs.writeFile(cpyFullPath, data, function (err) {
                                        });
                                        haveRespone = true;
                                        return;
                                    });
                                });

                            });
                        }
                        catch (ex) {
                            return;
                        }
                    }, 1000);


                    checkTimeout = setTimeout(function () {
                        clearInterval(interval);
                        callback({ responseCode: "0" });
                    }, 180000) //180000

                    ////console.log("The file was saved!");
                }
            });

        },
        service: {

            EDC_Sales: "0",
            EDC_EPS: "5",
            EDC_Refund: "2",
            EDC_EPS_Void: "3",
            EDC_Settlement: "@",
            CUP_Sales: "a",
            CUP_Settlement: "f",
            CUP_Batch: "g",
            CUP_Trace: "j",
            CUP_Void: "d",
            CUP_Refund: "l",
            CUP_EPS_Sales: "J",
            CUP_EPS_Void: "O"



        },
        getVoidCommandByPaymentCode: function (code) {
            switch (code) {
                case "visa":
                case "master":
                case "eps":
                    return this.service.EDC_EPS_Void;
                case "unionpay":
                    return this.service.CUP_Void;
                case "ae_eps":
                    return this.service.CUP_EPS_Void;
            }

        },
        insertECRTransaction: function (req, resp, callback) {

            var cloud = {}
            var sql = "";
            var args = [];

            if (req.isSettlement) {
                var datetime = moment().toDate();
                sql = "insert into ecrTransaction(Resp_Code, Resp_Text, DateTime, Trans_Time, Command_Code, Archive, Till, Approval_Code) VALUES(@Resp_Code, @Resp_Text, @DateTime, @Trans_Time, @Command_Code, @Archive, @Till, @Approval_Code);"
                args = [
                    { "c": "Resp_Code", "t": TYPES.VarChar, "v": resp.detailInObject.Resp_Code },
                    { "c": "Resp_Text", "t": TYPES.VarChar, "v": resp.detailInObject.Resp_Text },
                    { "c": "DateTime", "t": TYPES.DateTime, "v": datetime },
                    { "c": "Trans_Time", "t": TYPES.DateTime, "v": datetime },
                    { "c": "Command_Code", "t": TYPES.VarChar, "v": req.service },
                    { "c": "Archive", "t": TYPES.Int, "v": 1 },
                    { "c": "Till", "t": TYPES.VarChar, "v": req.till },
                    { "c": "Approval_Code", "t": TYPES.VarChar, "v": resp.detailInObject.Batch_Upload }
                ]

                cloud.sql = sql;
                cloud.refNo = req.invoice;
                cloud.isSettlement = true;
                cloud.Trans_Time = datetime;
                cloud.service = req.service;
                cloud.till = req.till;
                cloud.resObj = resp;

            } else {

                sql = "insert into ecrTransaction VALUES(@refNo, @ECR_Ref, @Amount, @Tips, @Resp_Code, @Resp_Text, @DateTime, @Card_Type, @Card_No, @Expiry_Date, @Card_Holder, @Terminal_Number, @Merchant_Number, @Trace_Number, @Batch_Number, @Approval_Code, @Retrieval_Reference, @Trans_Time, @Command_Code, @Archive, @Till);"
                resp.Trans_Time = moment().toDate();
                args = [
                 { "c": "refNo", "t": TYPES.VarChar, "v": req.invoice ? req.invoice : null },
                 { "c": "ECR_Ref", "t": TYPES.VarChar, "v": resp.detailInObject.ECR_Ref },
                 { "c": "Amount", "t": TYPES.Decimal, "v": parseFloat(resp.detailInObject.Amount / 100), "o": { "scale": 2 } },
                 { "c": "Tips", "t": TYPES.Decimal, "v": parseFloat(resp.detailInObject.Tips / 100), "o": { "scale": 2 } },
                 { "c": "Resp_Code", "t": TYPES.VarChar, "v": resp.detailInObject.Resp_Code },
                 { "c": "Resp_Text", "t": TYPES.VarChar, "v": resp.detailInObject.Resp_Text },
                 { "c": "DateTime", "t": TYPES.DateTime, "v": moment(resp.detailInObject.DateTime, 'YYMMDDHHmmss').toDate() },
                 { "c": "Card_Type", "t": TYPES.VarChar, "v": resp.detailInObject.Card_Type },
                 { "c": "Card_No", "t": TYPES.VarChar, "v": resp.detailInObject.Card_No },
                 { "c": "Expiry_Date", "t": TYPES.VarChar, "v": resp.detailInObject.Expiry_Date },
                 { "c": "Card_Holder", "t": TYPES.VarChar, "v": resp.detailInObject.Card_Holder },
                 { "c": "Terminal_Number", "t": TYPES.VarChar, "v": resp.detailInObject.Terminal_Number },
                 { "c": "Merchant_Number", "t": TYPES.VarChar, "v": resp.detailInObject.Merchant_Number },
                 { "c": "Trace_Number", "t": TYPES.VarChar, "v": resp.detailInObject.Trace_Number },
                 { "c": "Batch_Number", "t": TYPES.VarChar, "v": resp.detailInObject.Batch_Number },
                 { "c": "Approval_Code", "t": TYPES.VarChar, "v": resp.detailInObject.Approval_Code },
                 { "c": "Retrieval_Reference", "t": TYPES.VarChar, "v": resp.detailInObject.Retrieval_Reference },
                 { "c": "Trans_Time", "t": TYPES.DateTime, "v": resp.Trans_Time },
                 { "c": "Command_Code", "t": TYPES.VarChar, "v": resp.commandCode },
                 { "c": "Archive", "t": TYPES.Int, "v": 0 },
                 { "c": "Till", "t": TYPES.VarChar, "v": req.till },
                ];

                cloud.refNo = req.invoice;
                cloud.till = req.till;
                cloud.resObj = resp;
            }



            RAS.DM.executeStatement({ sql: sql, args: args }, (err, result) => {



                var seq = req.till + req.service;

                if (req.invoice)
                    seq = req.invoice.substring((req.invoice.length - 6), req.invoice.length);

                fs.writeFile("./cloud/ecrtransaction/" + seq + moment().format("YYMMDDHHmmss") + ".json", JSON.stringify(cloud), function (err) {
                    if (err) {
                        //console.log(err);
                    } else {
                        ////console.log("The file was saved!");
                    }
                });
                //if (err != null) callback({ result: 'failInCard' });
                callback(result);
            })

        }

    }

    this.Octopus = {
        connect: function (args, callback) {
            console.log('Octopus', args.service, JSON.stringify(args));
            var obj = {};
            //var till = args.till.$value ? args.till.$value : args.till;
            var till = args.till;
            var octopusPath = __dirname + config.sysDirs.octopusDir + "/" + till;
            var actionCode = till + args.service + "_" + moment().format('YYMMDDHHmmss');

            switch (args.service) {
                case this.service.Octopus_addValue:
                case this.service.Octopus_deduct:
                    obj = { 'service': args.service, "receiptNo": args.invoice, "amount": parseInt(args.totalPrice * 10), "numRetry": args.numRetry };
                    break;
                case this.service.Octopus_dayend:
                case this.service.Octopus_enquiry:
                case this.service.Octopus_cancelIncomp:
                    obj = { 'service': args.service };
                    break;
            }

            obj.actionCode = actionCode;
            var reqFileName = args.service + "_" + moment().format('YYMMDDHHmmss') + ".json";
            var reqFilePath = octopusPath + "/req/" + reqFileName;
            fs.writeFile(reqFilePath, JSON.stringify(obj), function (err) {
                if (err) {
                    console.log(err);
                    callback({ "result": "ERROR", "note": err.stack });
                }

                var resFilePath = octopusPath + "/res/" + reqFileName;
                var resBackupFilePath = octopusPath + "/res/" + "backup/" + reqFileName;
                var resultCode = "ERROR";
                var responseObj = {};
                var haveRespone = false;

                var checkTimeout;

                var interval = setInterval(function () {
                    if (haveRespone) {
                        clearInterval(interval);
                        clearTimeout(checkTimeout);
                        callback(responseObj);
                    }
                    try {
                        fs.readFile(resFilePath, { 'encoding': 'utf8' }, function (err, data) {
                            if (err) return;
                            var output = JSON.parse(data);
                            if (output.actionCode != actionCode) return;
                            console.log("read octopus res file", output);

                            if (output.result <= 100000) resultCode = "OK";
                            responseObj = {
                                result: resultCode,
                                timestamp: new Date().getTime(),
                                note: output.result,
                                totalPrice: args.totalPrice,
                                invoice: args.invoice,

                                "machineId": output.deviceNo,
                                "octopusCode": output.result,
                                "cardId": output.cardId,
                                "txId": output.octopusId,
                                "payAmount": args.totalPrice,
                                "cardBalance": output.remaining ? output.remaining : null,
                                "receiptNo": args.invoiceSeq,
                                "transactionData": output.transactionData,
                                "alertMessage": output.alertMessage,
                                "lastAddValue": output.lastAddValue
                            };
                            fs.unlink(resFilePath);
                            fs.writeFile(resBackupFilePath, data, function (err) { });
                            haveRespone = true;
                            return;
                        });
                    }
                    catch (ex) {
                        console.log('Octopus', ex);
                        return;
                    }
                }, 500);

                checkTimeout = setTimeout(function () {
                    clearInterval(interval);
                    callback({ "result": "ERROR", "note": "100001" });
                }, 20 * 1000);
            });
        },

        CheckBlacklistStatus: (args, callback) => {
            var till = args.till;
            var octopusPath = __dirname + config.sysDirs.octopusDir + "/" + till;
            fs.readdir(octopusPath, function (err, files) {
                if (err) {
                    callback({ "result": "OK" });
                    return;
                }
                files = files.filter(function (filename) {
                    return filename.indexOf('blacklistStatus') != -1;
                });
                if (files.length == 0) {
                    callback({ "result": "OK" });
                    return;
                }
                var blacklistStatusFile = octopusPath + "/" + files[0];
                console.log("CheckBlacklistStatus", blacklistStatusFile);
                fs.stat(blacklistStatusFile, (err, stats) => {
                    if (err) {
                        callback({ "result": "OK" });
                        return;
                    }
                    console.log("CheckBlacklistStatus", stats);
                    var today = getToday();
                    //var lastModifiedDate = new Date(stats.mtime);
                    var lastModifiedDate = new Date(stats.mtime.getFullYear(), stats.mtime.getMonth(), stats.mtime.getDate());
                    if (today == lastModifiedDate) {
                        var status = files[0].substring(files[0].lastIndexOf("_") + 1);
                        if (status == 0) {
                            callback({ "result": "ERROR" });

                        } else {
                            callback({ "result": "OK" });
                        }
                    }
                    else {
                        callback({ "result": "ERROR" });

                    }
                });
            });
        },

        service: {
            Octopus_enquiry: "enquiry",
            Octopus_deduct: "deduct",
            Octopus_dayend: "dayend",
            Octopus_addValue: "addvalue",
            Octopus_cancelIncomp: "cancelincomp"
        }
    }

    this.getAllPayment = (args, callback) => {

        var refNo = args.refNo;
        var query = [
            {
                "sql": "select * from [tenderPayment] where void = 0 and refNo = @refNo order by seq;",
                "args": [
                    { "c": "refNo", "t": TYPES.VarChar, "v": refNo }
                ]
            },
            {
                "sql": "select * from [tenderPayment] where void = 1 and refNo = @refNo order by voidDate desc;",
                "args": [
                    { "c": "refNo", "t": TYPES.VarChar, "v": refNo }
                ]
            },
            {
                "sql": "select * from [ecrTransaction] where refNo = @refNo",
                "args": [
                    { "c": "refNo", "t": TYPES.VarChar, "v": refNo }
                ]
            }
        ]

        this.DM.executeStatement(query, (err, result) => {
            callback(result);
        })

    }

    this.deletePayment = (args, callback) => {
        var executeArr = [];
        function voidPayment(cb) {
            if (args.isCard) {
                var cardService = "";
                cardService = RAS.ECR.getVoidCommandByPaymentCode(args.tender.code);


                var query = {
                    sql: 'select * from ecrTransaction where refNo = @refNo and Archive = 0 and Trace_Number = @traceNumber;',
                    args: [
                        { "c": "refNo", "t": TYPES.VarChar, "v": args.refNo },
                        { "c": "traceNumber", "t": TYPES.VarChar, "v": args.tender.traceNumber }
                    ]
                };
                RAS.DM.executeStatement(query, (err, result) => {
                    var _o = result;
                    if (_o.length == 0) {
                        callback('failInCard');
                        return;
                    }

                    if (args.isUseECR) {
                        RAS.ECR.checkECR({ service: cardService, till: args.tender.till, traceNumber: _o[0].Trace_Number }, function (result) {
                            if (result.responseCode == 1) {
                                _o.invoice = args.refNo;
                                _o.till = args.tender.till;
                                RAS.ECR.insertECRTransaction(_o, result, function () {
                                    cb(null);
                                });
                            } else {
                                callback('failInCard'); //('failInCard');
                                //cb(null)
                            }
                        });
                    } else {
                        _o.invoice = args.refNo;
                        _o.till = args.tender.till;

                        var res = {};

                        res.detailInObject = {};
                        extend2(res.detailInObject, _o[0]);
                        res.detailInObject.Amount = res.detailInObject.Amount * 100;
                        res.detailInObject.DateTime = moment().format('YYMMDDHHmmss');
                        res.commandCode = cardService;

                        RAS.ECR.insertECRTransaction(_o, res, function () {
                            cb(null);
                        });
                    }
                });

            } else {
                cb(null);
            }

        }
        executeArr.push(voidPayment);
        executeArr.push((cb) => {
            var cloud = {};
            var voidDate = moment().toDate();
            var query = [
            {
                "sql": "update [tenderPayment] set void = 1, voidStaff = @voidStaff, voidDate = @voidDate where refNo=@refNo and seq=@seq;update [transaction] set payAmount = payAmount - (select paymentValue from [tenderPayment] where refNo = @refNo and seq=@seq) where refNo = @refNo; select * from [tenderPayment] where refNo = @refNo and void = 1 order by voidDate desc;",
                "args": [
                   { "c": "voidStaff", "t": TYPES.VarChar, "v": args.voidStaffId },
                   { "c": "refNo", "t": TYPES.VarChar, "v": args.refNo },
                   { "c": "seq", "t": TYPES.VarChar, "v": args.seq },
                   { "c": "voidDate", "t": TYPES.DateTime, "v": voidDate }
                ]
            }
            ]

            cloud.sql = query[0].sql;
            cloud.voidStaff = args.voidStaffId;
            cloud.voidDate = voidDate;
            cloud.seq = args.seq;
            cloud.refNo = args.refNo;

            this.DM.executeStatement(query, (err, result) => {
                if (err == null) {
                    fs.writeFile("./cloud/deletepayment/" + moment().format("YYMMDDHHmmss") + ".json", JSON.stringify(cloud), function (err) {
                        if (err) {
                            //console.log(err);
                        } else {
                            ////console.log("The file was saved!");
                        }
                    });
                    cb(null, result);
                }

            })
        })

        async.waterfall(executeArr, function (err, result) {
            callback(result);
        });
    }

    this.insertPayment = (args, callback) => {

        var executeArr = [];
        var remarkList = [];
        var seq = 0;
        var cloud = {};
        args.payMethod.$value.forEach((payMethod, idx) => {

            if (!payMethod.isNewAdd) return true;
            if (seq == 0 && (payMethod.tenderType == 'card' || payMethod.method === "octopus2")) {
                var progType = "fastfood"
                executeArr.push((cb) => {
                    var query = {
                        "sql": "declare @seq int EXEC @seq = spGetNextSeq '" + progType + "' \
                                select 'invoice' = @refNo, 'invoiceSeq' = @seq;",
                        "args": [
                            {
                                "c": "refNo", "t": TYPES.VarChar, "v": args.refNo.$value
                                //config.shopCode + refType + moment().format("-YYMMDD-")
                            }
                        ]
                    }
                    this.DM.executeStatement(query, (err, result) => {
                        seq = result.invoiceSeq;
                        cb(null, result[0]);
                    });
                });
            }


            if (payMethod.method === "octopus2") {
                executeArr.push((_o, cb) => {

                    //this.DeductOctopus(extend2({ till: args.till.$value, totalPrice: payMethod.amount }, _o), (result) => {
                    //    if (result.octopusCode <= 100000) {
                    //        remarkList[idx] = result.cardBalance + "|" + parseFloat(result.totalPrice).toFixed(2) + "|" + result.cardId + "|" + result.machineId;
                    //        //console.log('octopus2', JSON.stringify(result));
                    //        console.log('remark', remarkList[idx]);
                    //        console.log('octopus2 test', JSON.stringify(result));
                    //        cb(null);
                    //    }
                    //    else {
                    //        console.log('octopus2 test callback', JSON.stringify(result));
                    //        callback(result);
                    //    }
                    //});
                    var params = extend2({
                        service: this.Octopus.service.Octopus_deduct,
                        till: args.till.$value,
                        totalPrice: payMethod.amount
                    }, _o);
                    this.Octopus.connect(params, (result) => {
                        if (result.octopusCode <= 100000) {
                            _o.octopusBalance = result.cardBalance;
                            remark = result.cardBalance + "|" + parseFloat(result.totalPrice).toFixed(2) + "|" + result.cardId + "|" + result.machineId + "|" + result.lastAddValue + "|" + result.alertMessage;
                            remarkList[idx] = remark;
                            console.log('remark', remarkList[idx]);
                            console.log('octopus2 test', JSON.stringify(result));
                            cb(null);
                        }
                        else {
                            console.log('octopus2 test callback', JSON.stringify(result));
                            callback(result);
                        }
                    });
                });
                return true;
            }



            if (payMethod.tenderType == 'card') {
                var cardService = ""
                switch (payMethod.method) {
                    case "visa": case "master": default:
                        cardService = this.ECR.service.EDC_Sales;
                        break
                    case "unionpay":
                        cardService = this.ECR.service.CUP_Sales;
                        break;
                    case "eps":
                        cardService = this.ECR.service.EDC_EPS;
                        break;
                }

                console.log('insertPayment', args);




                if (args.isUseECR) {

                    executeArr.push((_o, cb) => {
                        var req = { totalPrice: payMethod.amount, service: cardService, till: args.till.$value, invoice: args.refNo.$value };
                        extend2(req, _o);
                        this.ECR.checkECR(req, (result) => {
                            if (result.responseCode == 0) {
                                callback({ result: 'failInCard' });


                            } else if (result.responseCode == 1) {


                                this.ECR.insertECRTransaction(req, result, function (result2) {
                                    var remark = null;
                                    if (result.detail) remark = result.detail;
                                    if (result.detailInObject.Trace_Number) remark = result.detailInObject.Trace_Number + "##|##" + remark;
                                    remarkList[idx] = remark;
                                    cb(null);


                                })


                            }
                        })
                    });


                } else if (args.isUseKeypad) {

                    executeArr.push((_o, cb) => {

                        var res = {};
                        res.detailInObject = {};
                        res.detailInObject.ECR_Ref = null;
                        res.detailInObject.Amount = args.keypad.amount * 100;
                        res.detailInObject.Tips = 0;
                        res.detailInObject.Resp_Code = null;
                        res.detailInObject.Resp_Text = null;
                        res.detailInObject.DateTime = moment().format('YYMMDDHHmmss');
                        res.detailInObject.Card_Type = null;
                        res.detailInObject.Card_No = null;
                        res.detailInObject.Expiry_Date = null;
                        res.detailInObject.Card_Holder = null;
                        res.detailInObject.Terminal_Number = null;
                        res.detailInObject.Merchant_Number = null;
                        res.detailInObject.Trace_Number = args.keypad.traceNumber;
                        res.detailInObject.Batch_Number = args.keypad.batchNumber;
                        res.detailInObject.Retrieval_Reference = null;
                        res.commandCode = cardService;

                        var req = { totalPrice: payMethod.amount, service: cardService, till: args.till.$value, invoice: args.refNo.$value };
                        extend2(req, _o);
                        RAS.ECR.insertECRTransaction(req, res, function (result2) {
                            var remark = "";
                            if (res.detailInObject.Trace_Number) remark = res.detailInObject.Trace_Number + "##|##" + remark;
                            remarkList[idx] = remark;
                            cb(null);
                        })
                    })

                }

                return true;
            }
        });



        executeArr.push((cb) => {

            var totalPayment = 0;
            var sql = ""
            var sqlargs = []
            sqlargs.push({ "c": "refNo", "t": TYPES.VarChar, "v": args.refNo.$value });
            sqlargs.push({ "c": "staffId", "t": TYPES.VarChar, "v": args.user.$value });
            sqlargs.push({ "c": "tillNum", "t": TYPES.VarChar, "v": args.till.$value });

            cloud.refNo = args.refNo.$value;
            cloud.staffId = args.user.$value;
            cloud.till = args.till.$value;

            cloud.paymentList = [];
            var paymentDate = moment().toDate();
            cloud.paymentDate = paymentDate;

            sqlargs.push({ "c": "paymentDate", "t": TYPES.DateTime, "v": paymentDate })
            try {
                for (var i = 0; i < args.payMethod.$value.length; i++) {
                    if (!args.payMethod.$value[i].isNewAdd) continue;
                    //var payseq = i + 1;
                    var cloudPaymentObj = {};
                    sql += " insert into tenderpayment (paymentType,paymentValue,staffId,refNo,paymentDate,void,seq, till, remark) values (@paymentType" + i.toString() + ",@paymentValue" + i.toString() + ",@staffId, @refNo ,@paymentDate,0," + "(SELECT CASE   WHEN (select Max(seq) from tenderPayment where refNo = @refNo) is null THEN 1  ELSE  (select Max(seq) from tenderPayment where refNo = @refNo) + 1 END)" + ", @tillNum, @remark" + i + "); ";

                    cloudPaymentObj.sql = " insert into tenderpayment (paymentType,paymentValue,staffId,refNo,paymentDate,void,seq, till, remark) values (@paymentType" + i.toString() + ",@paymentValue" + i.toString() + ",@staffId, @refNo ,@paymentDate,0," + "(SELECT CASE   WHEN (select Max(seq) from tenderPayment where refNo = @refNo) is null THEN 1  ELSE  (select Max(seq) from tenderPayment where refNo = @refNo) + 1 END)" + ", @tillNum, @remark" + i + "); ";

                    cloudPaymentObj.paymentTypeName = "paymentType" + i.toString();
                    cloudPaymentObj.paymentTypeValue = args.payMethod.$value[i].method;
                    cloudPaymentObj.paymentValueName = "paymentValue" + i.toString();
                    cloudPaymentObj.paymentValueValue = parseFloat(args.payMethod.$value[i].amount);

                    cloudPaymentObj.remarkName = "remark" + i.toString();
                    sqlargs.push({ "c": "paymentType" + i.toString(), "t": TYPES.VarChar, "v": args.payMethod.$value[i].method })
                    sqlargs.push({ "c": "paymentValue" + i.toString(), "t": TYPES.Decimal, "v": parseFloat(args.payMethod.$value[i].amount), "o": { "scale": 2 } })

                    if (args.payMethod.$value[i].tenderType == 'card' || args.payMethod.$value[i].method === "octopus2") {
                        cloudPaymentObj.remarkValue = remarkList[i] ? remarkList[i] : null;
                        sqlargs.push({ "c": "remark" + i, "t": TYPES.NVarChar, "v": remarkList[i] ? remarkList[i] : null });
                    }
                    else {
                        cloudPaymentObj.remarkValue = null;
                        sqlargs.push({ "c": "remark" + i, "t": TYPES.NVarChar, "v": null });
                    }

                    totalPayment += parseFloat(args.payMethod.$value[i].amount);
                    cloud.paymentList.push(cloudPaymentObj);
                }


                cloud.transaction = {};
                cloud.transaction.totalPayment = totalPayment;
                cloud.transaction.sql = "update [transaction] set payAmount = payAmount + @payAmount where refNo = @refNo;";
                console.log('payAmount', totalPayment);

                sql += " update [transaction] set payAmount = payAmount + @payAmount where refNo = @refNo; ";

                sqlargs.push({ "c": "payAmount", "t": TYPES.Decimal, "v": totalPayment, "o": { "scale": 2 } });

                sql += "select * from [tenderPayment] where void = 0 and refNo=@refNo order by seq;";

                //console.log(JSON.stringify(sqlargs));
                //console.log(sql);
            } catch (ex) {
                console.log(ex.toString());
            }


            //sqlargs.push({ "c": "remarks1", "t": TYPES.VarChar, "v": null });

            this.DM.executeStatement({ sql: sql, args: sqlargs }, (err, result) => {
                if (err == null) {
                    console.log('insertPayment', result);

                    this.PrintBill({ refNo: args.refNo.$value, till: args.till.$value, printer: config.tills[args.till.$value].printer }, function (r) {

                    });

                    fs.writeFile("./cloud/insertpayment/" + moment().format("YYMMDDHHmmss") + ".json", JSON.stringify(cloud), function (err) {
                        if (err) {
                            //console.log(err);
                        } else {
                            ////console.log("The file was saved!");
                        }
                    });

                    cb(null, { result: "OK", resultList: result })
                }

            })
        })

        async.waterfall(executeArr, function (err, result) {
            callback(result);
        });
    }

    this.SetPreOrder = (args, callback) => {
        var r = args.itemData.json;

        var preorderArray = [];
        var currentPreorderCode = "";
        try {
            console.log("SetPreOrder", r.order.item);
            for (var i = 0; i < r.order.item.length; i++) {
                preorderArray.push({
                    "sql": "insert into preorder (ticketId, itemId, qty, type) values (@ticketId, @itemId, @qty, @type)",
                    "args": [
                        { "c": "ticketId", "t": TYPES.VarChar, "v": args.ticketId.$value },
                        { "c": "itemId", "t": TYPES.VarChar, "v": r.order.item[i].code },
                        { "c": "qty", "t": TYPES.Int, "v": r.order.item[i].qty },
                        { "c": "type", "t": TYPES.VarChar, "v": r.order.item[i].type }
                    ]
                })
            }
        }
        catch (e) {
            console.log(e);
        }
        this.DM.executeStatement(preorderArray, (err, result) => {
            callbackObj = { result: "OK", timestamp: new Date().getTime(), taskID: 1, note: '' };
            callback(callbackObj);
        });
    }

    this.LoadPreOrder = (args, callback) => {
        var preorderId = args.preorderID.$value;
        var query = {
            "sql": "select * from preorder where ticketId = @ticketId",
            "args": [
                { "c": "ticketId", "t": TYPES.VarChar, "v": preorderId }
            ]
        }
        this.DM.executeStatement(query, (err, result) => {
            var order = { 'version': '1.0', 'preorder': [] };
            for (var i = 0; i < result.length; i++) {
                var contentObj = {
                    code: result[i].itemId,
                    qty: result[i].qty,
                    type: result[i].type
                };
                order.preorder.push(contentObj);
            }
            var orders = [order];
            var callbackObj = { result: "OK", timestamp: new Date().getTime(), taskID: 1, note: "", order: orders };
            callback(callbackObj);
        });
    }

    this.SwapMenuBoard = (swap, callback) => {
        var query = [];
        swap.forEach(function (s) {
            query.push({
                "sql": "update menuLayout set panel = @to + '_swapped' where panel = @from;",
                "args": [
                    { "c": "from", "t": TYPES.VarChar, "v": s.from },
                    { "c": "to", "t": TYPES.VarChar, "v": s.to }
                ]
            });
        });
        query.push({ "sql": "update menuLayout set panel = replace(panel, '_swapped', '') where panel like '%_swapped';" });
        this.DM.executeStatement(query, (err, result) => {
            callback();
        });
    }

    this.checkUserExist = (args, callback) => {

        var query = {
            sql: "select count(*) as isExist from staff where username=@username; ",
            args: [
                { "c": "username", "t": TYPES.VarChar, "v": args.username }
            ]
        }

        this.DM.executeStatement(query, (err, result) => {
            callback(result);
        });
    }

    this.registerStaffWatch = (args, callback) => {

        var exec = [];


        exec.push(function (cb) {
            var query = {
                sql: "select count(*) as isExist from [watch] where watchId=@watchId;",
                args: [
                    { "c": "watchId", "t": TYPES.VarChar, "v": args.watchTagId }
                ]
            }

            RAS.DM.executeStatement(query, (err, result) => {

                if (result[0].isExist) {

                    cb(null);
                } else {
                    callback({ "response": "fail1" });
                }
            });

        });

        exec.push(function (cb) {
            var query = {
                sql: "insert into staffWatch (staffId, watchTagId, update_time) VALUES ((select top 1 staffId from staff where username=@username), @watchTagId, @datetime); ",
                args: [
                    { "c": "username", "t": TYPES.VarChar, "v": args.username },
                    { "c": "watchTagId", "t": TYPES.VarChar, "v": args.watchTagId },
                    { "c": "datetime", "t": TYPES.DateTime, "v": new Date() },
                ]
            }

            RAS.DM.executeStatement(query, (err, result) => {
                if (err == null) cb(null);
            });
        })

        async.waterfall(exec, function (result) {
            callback("OK");
        });


    }

    this.checkPaymentPermission = (args, callback) => {

        var query = {
            sql: "SELECT count(*) as count from staff as s, staffGroup as g where s.staffGroupCode = g.staffGroupCode and p16 = 1 and username = @userName",
            args: [
                 { "c": "username", "t": TYPES.VarChar, "v": args.userName }
            ]
        }

        this.DM.executeStatement(query, (err, result) => {
            callback(result);
        });
    }

    this.changeCollectStatus = (args, callback) => {

        var exeArr = [];
        var remark = null;
        exeArr.push((cb) => {
            var query = { 'sql': 'select remark from [transaction] where refNo=@refNo and paymentLater != 1', 'args': [{ "c": "refNo", "t": TYPES.VarChar, "v": args.refNo }] };
            this.DM.executeStatement(query, (err, result) => {
                if (result.length == 0) {
                    callback({ error: "error1" });
                    return;
                }
                remark = result[0].remark;
                try {
                    remark = JSON.parse(remark);
                } catch (ex) {
                    remark = null;
                }

                cb(null);
            })
        })

        exeArr.push((cb) => {

            if (remark == null) remark = JSON.stringify({ isCollected: args.isCollected });
            else {
                remark.isCollected = args.isCollected;
                remark = JSON.stringify(remark);
            }

            var query = {
                sql: "update [transaction] set remark=@remark output inserted.* where refNo=@refNo;",
                args: [
                    { "c": "remark", "t": TYPES.VarChar, "v": remark },
                    { "c": "refNo", "t": TYPES.VarChar, "v": args.refNo },
                ]

            }

            this.DM.executeStatement(query, (err, result) => {
                if (err == null) cb(result[0]);
            });
        })

        async.waterfall(exeArr, function (result) {
            callback(result);
        });




    }

    this.saveDeviceLog = (args, callback) => {
        var query = {
            sql: "insert into deviceLog (ip, panelName, status, detail, androidID, packageName, appVersionCode, appVersionName, createDate) VALUES (@ip, @panelName, @status, @detail, @androidID, @packageName, @appVersionCode, @appVersionName, @createDate)",
            args: [
                { "c": "ip", "t": TYPES.NVarChar, "v": args.ip },
                { "c": "panelName", "t": TYPES.NVarChar, "v": args.panelName },
                { "c": "status", "t": TYPES.NVarChar, "v": args.status },
                { "c": "detail", "t": TYPES.NVarChar, "v": args.detail },
                { "c": "androidID", "t": TYPES.NVarChar, "v": args.androidID },
                { "c": "packageName", "t": TYPES.NVarChar, "v": args.packageName },
                { "c": "appVersionCode", "t": TYPES.Int, "v": args.appVersionCode },
                { "c": "appVersionName", "t": TYPES.NVarChar, "v": args.appVersionName },
                { "c": "createDate", "t": TYPES.DateTime, "v": new Date() },
            ]
        }

        RAS.DM.executeStatement(query, (err, result) => {
            callback("OK");
        });
    }

    this.GenerateLockerMap = (callback) => {
        try {
            var sql = new Array();
            sql.push({ "sql": "select * from locker" });
            sql.push({ "sql": "select * from staffLocker" });
            sql.push({ "sql": "select * from staff" });
            this.DM.executeStatement(sql, (err, obj) => {
                var locker = obj[0], staffLocker = obj[1], staff = obj[2];
                var _obj = { "lockers": [] };
                locker.forEach(function (l) {
                    var temp = l;
                    var mapping = [];
                    var tempMapping = staffLocker.filter(function (x) { return x.lockerCode == l.code; });
                    tempMapping.forEach(function (m) {
                        var staffObj = staff.filter(function (x) { return x.username == m.staffUsername; });
                        if (staffObj.length) {
                            m.password = staffObj[0].password;
                            m.card = staffObj[0].username;
                            m.name = staffObj[0].name;
                            mapping.push(m);
                        }
                    });
                    temp.mapping = tempMapping;
                    _obj.lockers.push(temp);
                });
                console.log("GenerateLockerMap", _obj);
                fs.writeFile("./public/data/lockerMap.json", JSON.stringify(_obj), function (err) {
                    if (err) {
                        //console.log(err);
                    } else {
                        ////console.log("The file was saved!");
                    }
                    if (callback) callback("OK");
                });
            });
        } catch (e) {
            console.log("Generate Locker Map error", e);
        }
    }

    this.paymentSetForPayLater = function (args, callback) {
        var query = {
            "sql": "update [transaction] set PaymentLater=0 where refNo in (select refNo from [transaction] where refNo = @refNo and PaymentLater = 1);",
            "args": [
                 { "c": "refNo", "t": TYPES.VarChar, "v": args.refNo },
            ]
        }

        this.DM.executeStatement(query, (err, result) => {
            if (callback) callback();
        })
    }
}

/**
 * Number.prototype.format(n, x)
 *
 * @param integer n: length of decimal
 * @param integer x: length of sections
 */
Number.prototype.format = function (n, x) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
};

function extend(from, to) {
    if (from == null || typeof from != "object") return from;
    if (from.constructor != Object && from.constructor != Array) return from;
    if (from.constructor == Date || from.constructor == RegExp || from.constructor == Function ||
        from.constructor == String || from.constructor == Number || from.constructor == Boolean)
        return new from.constructor(from);

    to = to || new from.constructor();

    for (var name in from) {
        to[name] = typeof to[name] == "undefined" ? extend(from[name], null) : to[name];
    }

    return to;
}

function pad(n, width, z) {
    z = z || '0';
    n = n + '';
    return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

function countDoubleByteChar(str) {
    var c = 0
    for (var i = 0, n = str.length; i < n; i++) {
        if (str.charCodeAt(i) > 255) { c++ }
    }
    return c;
}

function init() {
    RAS = new RASManager(config.sql)
    RAS.GenerateMenu();
    RAS.GenerateMenuLayout();
    RAS.GenerateLockerMap();
    RAS.LoadTableSchema(null, (err, result) => {
        io.sockets.emit('refreshTable', JSON.stringify(result));
    })
    RAS.InitLastOrderSchedule();
    PickUp = new PickUpManager(config.sql);
    PickUp.displayNumber();
    PickUp.removeExpiryNumber();
    setScheduleJob();
    resetCronJob();
}


function onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
}

init();

process.on('uncaughtException', function (err) {
    console.log('uncaughtException:' + err);
    console.log('uncaughtException:' + err.stack);
});

function setScheduleJob() {
    scheduleJobList.forEach(function (scheduleJob) {
        scheduleJob.cancel();
    });
    if (typeof goMenuConfig.scheduleReloadMenuTime != "undefined" && Array.isArray(goMenuConfig.scheduleReloadMenuTime)) {
        goMenuConfig.scheduleReloadMenuTime.forEach(function (scheduleTime) {
            var today = getToday();
            //var timeString = "" + today.getFullYear() + "-" + (today.getMonth() + 1) + "-" + today.getDate() + " " + scheduleTime;
            var timeString = moment(today).format("YYYY-MM-DD ") + scheduleTime;
            if (isNaN(Date.parse(timeString))) {
                console.log("schedule time config error", "[" + scheduleTime + "] is invalid time");
                // writeLog(": [setScheduleJob] [" + scheduleTime + "] is invalid time" + " \r\n");
                return;
            }

            var date = new Date(timeString);
            if (date <= new Date()) return;
            var j = schedule.scheduleJob(date, function (y) {
                var logMsg = "[ScheduleReloadMenu] scheduled job [" + y + "] was executed at [" + new Date().toString() + "].";
                // writeLog(": " + logMsg + " \r\n");
                console.log(logMsg);
                var _o = { action: "reloadMenu", "time": new Date().getTime() };
                mqttClient.publish("wo/gm/all", JSON.stringify(_o), { qos: 2 });
            }.bind(null, timeString));
            scheduleJobList.push(j);
        });
    }
    console.log("set swap menu board", config.scheduleSwapMenuBoardTime);
    if (typeof config.scheduleSwapMenuBoardTime != "undefined") {
        config.scheduleSwapMenuBoardTime.forEach(function (s) {
            var today = getToday();
            //var timeString = "" + today.getFullYear() + "-" + (today.getMonth() + 1) + "-" + today.getDate() + " " + s.time;
            var timeString = moment(today).format("YYYY-MM-DD ") + s.time;
            console.log("set swap menu board", timeString);
            if (isNaN(Date.parse(timeString))) {
                console.log("schedule time config error", "[" + s.time + "] is invalid time");
                // writeLog(": [setScheduleJob] [" + scheduleTime + "] is invalid time" + " \r\n");
                return;
            }

            var date = new Date(timeString);
            if (date <= new Date()) return;
            var j = schedule.scheduleJob(date, function (y) {
                var logMsg = "[ScheduleSwapMenuBoard] scheduled job [" + y + "] was executed at [" + new Date().toString() + "].";
                // writeLog(": " + logMsg + " \r\n");
                console.log(logMsg);
                RAS.SwapMenuBoard(s.swap, () => {
                    RAS.GenerateMenuLayout(() => {
                        var _o = { action: "reloadMenu", "time": new Date().getTime() };
                        mqttClient.publish("wo/gm/all", JSON.stringify(_o), { qos: 2 });
                        io.sockets.emit('reloadCurrentMenuLayout');
                    });
                });
            }.bind(null, timeString));
            scheduleJobList.push(j);
        });
    }
}

// dinning table status
/*
0 - can use
1 - ordering
2 - table have order
3 - order print

*/


// item type 
/*
 * I - item / open item
 * S - subItem
 * D - discount
 * K - Kitchen msg
 * M - modifier
 * O - option
 * T - time item
 * 
 */


//var connection = new sql.Connection(config.sql, function (err) {
//    //console.log(err);
//    var request = new sql.Request(connection); // or: var request = connection.request();
//    request.query('select * from item', function (err, recordset) {
//        //console.log(err);
//        // ... error checks
//        console.dir(recordset);
//    });
//});
