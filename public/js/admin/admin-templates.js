(function () {
    var app = angular.module('rms.templates', ['shared.templates', 'tmpl/top-bar.html', 'tmpl/status-bar.html', 'tmpl/order.html', 'tmpl/login.html', 'tmpl/login2.html', 'tmpl/managerLogin.html', 'tmpl/keypad.html', 'tmpl/keypad-free.html', 'tmpl/modifier.html', 'tmpl/cancelOptions.html', 'tmpl/reportScene.html', 'tmpl/calendar.html', 'tmpl/member.html', 'tmpl/pager-default-materialize.html', 'tmpl/gridMenuOption-cancel.html', 'tmpl/search-bill-menu.html', 'tmpl/main.html', 'tmpl/userScene.html', 'tmpl/item-scene.html', 'tmpl/category-scene.html', 'tmpl/gridMenuOption-order.html', 'tmpl/menu-layout-builder-scene.html','tmpl/menu-layout-list-scene.html', 'tmpl/menu-layout-template-scene.html', 'tmpl/time-range-scene.html', 'tmpl/foodControlOption.html', 'tmpl/optionsFoodControl.html', 'tmpl/options.html', 'tmpl/sidenav.html']);

// scene: search bill
// required: 
// templates: order.html + search-bill-menu.html

// scene: order
// required:
// controller: inputKeypadNoofpeople
// templates: order.html
  
  // swap 
    // templates
  angular.module('tmpl/sidenav.html', []).run(["$templateCache", function($templateCache) {
    $templateCache.put('tmpl/sidenav.html',
      '<md-sidenav class="md-sidenav-left md-whiteframe-z1" md-component-id="left">\
            <md-toolbar md-scroll-shrink>\
              <div class="md-toolbar-tools">\
                <h3>\
                  <span>WebOn RARe</span>\
                </h3>\
              </div>\
            </md-toolbar>\
            <md-content role="navigation">\
              <ul class="side-menu">\
                <li ng-repeat="section in vm.menu.mainMenuList" class="parent-list-item"\
                    ng-class="{\'parentActive\' : vm.isOpen(section)}">\
                  <menu-toggle section="section" ng-if="section.type === \'toggle\'"></menu-toggle>\
                  <menu-link section="section" ng-if="section.type === \'link\'"></menu-link>\
                  <menu-function section="section" ng-if="section.type === \'function\'"></menu-function>\
                </li>\
              </ul>\
            </md-content>\
          </md-sidenav>\
          <md-content flex>\
            <ui-view name="content"></ui-view>\
          </md-content>\
        ');
  }]);

  angular.module("tmpl/top-bar.html", []).run(["$templateCache", function($templateCache) {
    $templateCache.put("tmpl/top-bar.html",
      ' \
      <md-toolbar id="toolbar-top" class="md-hue-2 md-whiteframe-glow-z2 md-rms-theme toolbar-top" > \
        <div class="md-toolbar-tools" ng-controller="headerCtrl"> \
          <div layout="row" flex class="fill-height"> \
            <h1 class="logo md-toolbar-item">\
              <md-button md-no-ink="true" aria-label="menu" class="md-icon-button" ng-click="toggleRight()"> \
                <md-icon class="dp45" md-svg-src="assets/icon/hexagon.svg" style="color: orange;" alt="RMS"></md-icon> \
              </md-button> \
            </h1> \
            <!--<h1 class="logo md-toolbar-item"> \
              <md-menu> \
                  <md-button md-no-ink="true" aria-label="menu" class="md-icon-button" ng-click="$mdOpenMenu()"> \
                    <md-icon class="dp45" md-svg-src="assets/icon/hexagon.svg" style="color: orange;" alt="RMS"></md-icon> \
                  </md-button> \
                  <md-menu-content class="top-icon-menu" width="2"> \
                    <md-menu-item ng-repeat="menu in mainMenuList track by $index"> \
                      <md-button md-no-ink="true" ng-click="switchMode2(menu.mode, menu.requiredLogin);"> \
                        {{::ui[menu.label] || \'123\'}} \
                      </md-button> \
                    </md-menu-item> \
                  </md-menu-content> \
              </md-menu> \
            </h1>--> \
            <div>{{MainService.currentPageTitle}}</div>\
            <span flex=""></span> \
            <div class="md-toolbar-item" layout="row"> \
                <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active" title="{{::ui[\'topMenu_write_title\']}}" ng-click="writeFile()" ><strong>{{::ui[\'topMenu_write_title\']}}</strong></button> \
              \
              <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active" title="{{::ui[\'reload\']}}" onclick="location.reload();" ><strong>{{ui[\'reload\']}}</strong></button> \
              \
                <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button" title="{{::ui[\'member_info\']}}" ng-show="config.topMenu.memberInfo && modeCheckFloor()" ng-click="panelMember()"><strong>{{ui[\'member_info\']}}</strong></button>\
                <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active" title="{{::ui[\'login\']}}" ng-click="login()" ng-hide="isLogin()"><strong>{{ui[\'login\']}}</strong></button> \
                \
                <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active" title="{{::ui[\'logout\']}}" ng-click="logout($event)" ng-show="isLogin()"><strong>{{ui[\'logout\']}}</strong></button> \
                \
                <div class="edit-group" layout="row"> \
                    <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button delete" title="刪除"><strong>刪除</strong></button><button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button save proceed" title="儲存"><strong>儲存</strong></button><button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button abandon modal-trigger" title="放棄" data-target="abandon-dialog"><strong>放棄</strong></button> \
                </div> \
            </div> \
          </div> \
    </md-toolbar>');
  }]);

  angular.module("tmpl/status-bar.html", []).run(["$templateCache", function($templateCache) {
      $templateCache.put("tmpl/status-bar.html", 
      '<md-toolbar id="status-bar" class="md-hue-2 md-whiteframe-glow-z2 navbar-fixed-bottom md-rms-theme toolbar-bottom" layout="row"> \
        <!-- 100% - 95% --> \
        <!-- <div flex="80" class="messages"><i class="md-icon warn">expand_more</i><i class="md-icon">info_outline</i>收據已經列印到 *S4 打印機</div> --> \
        <div flex class="fill-height" style="height: 100%;"><div id="status-message"><div class="content message"></div></div></div> \
        <!-- <div flex class="fill-height" style="height: 100%;"><div id="warning-message"><div class="content message"></div></div></div> --> \
        <div flex></div> \
        <div flex="30" class="legend-container flex-center" ng-show="modeCheckFloor();"> \
          <ul class="legend"> \
            <li ng-repeat="(key, value) in config.statusBar.legend track by $index" class="{{key}}"><i class="md-icon">stop</i>{{ui[key]}}</li> \
          </ul> \
        </div> \
        <div class="employee flex-center" flex> \
          <!-- height 100% is for FF height: 100%; --> \
          <div class="" style="">服務員：<div class="name flex-center">{{MainService.UserManager.staffName || \'未登入/已登出\'}}</div></div> \
        </div> \
        </md-toolbar>');
  }]);

  angular.module("tmpl/login.html", []).run(["$templateCache", function($templateCache) {
    $templateCache.put("tmpl/login.html", 
      '<div id="login-box" class="keypad-container" ng-controller="DialogControllerLogin"> \
        <form name="loginForm"> \
            <md-dialog aria-label="Login" class="keypad" ng-app> \
                <md-dialog-content class="sticky-container"> \
                    <div class="message-container" style="padding: 0; max-width: 240px"> \
                        <div ng-show="loginError" ng-class="{warn: loginError}">{{ ui["password_fail"] || "密碼 或 用戶名稱錯誤"}}</div> \
                    </div> \
                    <md-input-container class="login"> \
                        <label>{{ui[modeName] || "用戶名稱 / 密碼"}}</label> \
                        <input name="loginField" class="login-input dialog-close" login-input login-input-enter="next()" ng-model="loginValue" ng-trim="false" ng-change="change()" type="password" custom-always-focus="true"> \
                    </md-input-container> \
                    <md-grid-list md-cols="6" md-row-height="1:1" md-gutter="1px" style="" class="keypad-grid"> \
                        <md-grid-tile ng-repeat="element in keypad" md-colspan="2" md-rowspan="2"> \
                            <md-button md-no-ink="true" class="md-raised" ng-click="input(element.text)" ng-if="!element.useIcon"> \
                                {{::element.text}} \
                            </md-button> \
                            <md-button md-no-ink="true" class="md-raised" ng-click="reset()" ng-if="element.useIcon && element.text==\'reset\'"> \
                                <i ng-show="loginValue && mode == \'username\'|| mode == \'password\'" class="md-icon dp40">replay</i> \
                                <i ng-hide="loginValue && mode == \'username\'|| mode != \'username\'" class="md-icon dp40">clear</i> \
                            </md-button> \
                            <md-button md-no-ink="true" class="md-raised" ng-click="backspace()" ng-if="element.useIcon && element.text==\'backspace\'"> \
                                <i class="md-icon dp40">backspace</i> \
                            </md-button> \
                        </md-grid-tile> \
                        <!-- finish button --> \
                        <!-- ng-show="loginValue"  --> \
                        <md-grid-tile md-rowspan="2" md-colspan="6" ng-show="loginValue"> \
                            <md-button md-no-ink="true" class="md-raised submit" ng-click="next()">{{::ui["done"] || "完成"}}</md-button> \
                        </md-grid-tile> \
                    </md-grid-list> \
                    <!-- </div> --> \
                </md-dialog-content> \
            </md-dialog> \
        </form> \
    </div>');
  }]);

  //<!-- flexible for specified or apply any controller, since $mdDialog.show can take a param for any controller -->
  angular.module("tmpl/login2.html", []).run(["$templateCache", function($templateCache) {
    $templateCache.put("tmpl/login2.html",
      '<div id="login-box" class="keypad-container"> \
        <form name="loginForm"> \
            <md-dialog aria-label="Login" class="keypad" ng-app> \
                <md-dialog-content class="sticky-container"> \
                    <div class="message-container" style="padding: 0; max-width: 240px"> \
                        <div ng-show="loginError" ng-class="{warn: loginError}">{{ ui["password_fail"] || "密碼 或 用戶名稱錯誤"}}</div> \
                    </div> \
                    <md-input-container class="login"> \
                        <label>{{ui[modeName] || "用戶名稱 / 密碼"}}</label> \
                        <input name="loginField" class="login-input dialog-close" login-input login-input-enter="next()" ng-model="loginValue" ng-trim="false" ng-change="change()" type="password" custom-always-focus="true" autocomplete="off"> \
                    </md-input-container> \
                    \
                    <div class="materialize-keypad col-5x3 keypad-container" style=""> \
                      <div class="row"> \
                        <div class="col s4" ng-repeat="element in keypad">\
                          <button class="btn md-button md-grid-button" ng-click="input(element.text)" ng-if="!element.useIcon">{{::element.text}}</button>\
                          <button class="btn md-button md-grid-button" ng-click="reset()" ng-if="element.useIcon && element.text==\'reset\'">\
                            <i ng-show="loginValue && mode == \'username\'|| mode == \'password\'" class="md-icon">replay</i> \
                            <i ng-hide="loginValue && mode == \'username\'|| mode != \'username\'" class="md-icon">clear</i> \
                          </button>\
                          <button class="btn md-button md-grid-button" ng-click="backspace()" ng-if="element.useIcon && element.text==\'backspace\'">\
                            <i class="md-icon">backspace</i> \
                          </button>\
                        </div> \
                        <div class="col s12">\
                          <button class="btn md-button md-grid-button submit" ng-click="next()">{{::ui["done"] || "完成"}}</button>\
                        </div> \
                      </div> \
                    </div> \
                    \
                    <!--<md-grid-list md-cols="6" md-row-height="1:1" md-gutter="1px" style="" class="keypad-grid"> \
                        <md-grid-tile ng-repeat="element in keypad" md-colspan="2" md-rowspan="2"> \
                            <md-button md-no-ink="true" class="md-raised" ng-click="input(element.text)" ng-if="!element.useIcon"> \
                                {{::element.text}} \
                            </md-button> \
                            <md-button md-no-ink="true" class="md-raised" ng-click="reset()" ng-if="element.useIcon && element.text==\'reset\'"> \
                                <i ng-show="loginValue && mode == \'username\'|| mode == \'password\'" class="md-icon dp40">replay</i> \
                                <i ng-hide="loginValue && mode == \'username\'|| mode != \'username\'" class="md-icon dp40">clear</i> \
                            </md-button> \
                            <md-button md-no-ink="true" class="md-raised" ng-click="backspace()" ng-if="element.useIcon && element.text==\'backspace\'"> \
                                <i class="md-icon dp40">backspace</i> \
                            </md-button> \
                        </md-grid-tile> \
                        <md-grid-tile md-rowspan="2" md-colspan="6" ng-show="loginValue"> \
                            <md-button md-no-ink="true" class="md-raised submit" ng-click="next()">{{::ui["done"] || "完成"}}</md-button> \
                        </md-grid-tile> \
                    </md-grid-list>--> \
                    <!-- </div> --> \
                </md-dialog-content> \
            </md-dialog> \
        </form> \
    </div>');
  }]);

  angular.module("tmpl/managerLogin.html", []).run(["$templateCache", function($templateCache) {
    $templateCache.put("tmpl/managerLogin.html", 
      '<div id="login-box" class="keypad-container" ng-controller="DialogControllerManagerLogin"> \
        <form name="loginForm"> \
            <md-dialog aria-label="Login" class="keypad" ng-app> \
                <md-dialog-content class="sticky-container"> \
                    <div class="message-container" style="padding: 0; max-width: 240px"> \
                        <div ng-show="loginError" ng-class="{warn: loginError}">{{ ui["password_fail"] || "密碼 或 用戶名稱錯誤"}}</div> \
                    </div> \
                    <md-input-container class="login"> \
                        <label>{{::ui[\'manager_confirmation\']}}/{{ui[modeName] || "用戶名稱 / 密碼"}}</label> \
                        <input name="loginField" class="login-input dialog-close" login-input login-input-enter="next()" ng-model="loginValue" ng-trim="false" ng-change="change()" type="text" custom-always-focus="true"> \
                    </md-input-container> \
                    <md-grid-list md-cols="6" md-row-height="1:1" md-gutter="1px" style="" class="keypad-grid"> \
                        <md-grid-tile ng-repeat="element in keypad" md-colspan="2" md-rowspan="2"> \
                            <md-button md-no-ink="true" class="md-raised" ng-click="input(element.text)" ng-if="!element.useIcon"> \
                                {{::element.text}} \
                            </md-button> \
                            <md-button md-no-ink="true" class="md-raised" ng-click="reset()" ng-if="element.useIcon && element.text==\'reset\'"> \
                                <i ng-show="loginValue && mode == \'username\'|| mode == \'password\'" class="md-icon dp40">replay</i> \
                                <i ng-hide="loginValue && mode == \'username\'|| mode != \'username\'" class="md-icon dp40">clear</i> \
                            </md-button> \
                            <md-button md-no-ink="true" class="md-raised" ng-click="backspace()" ng-if="element.useIcon && element.text==\'backspace\'"> \
                                <i class="md-icon dp40">backspace</i> \
                            </md-button> \
                        </md-grid-tile> \
                        <!-- finish button --> \
                        <!-- ng-show="loginValue"  --> \
                        <md-grid-tile md-rowspan="2" md-colspan="6" ng-show="loginValue"> \
                            <md-button md-no-ink="true" class="md-raised submit" ng-click="next()">{{::ui["done"] || "完成"}}</md-button> \
                        </md-grid-tile> \
                    </md-grid-list> \
                    <!-- </div> --> \
                </md-dialog-content> \
            </md-dialog> \
        </form> \
    </div>');
  }]);

  angular.module("tmpl/keypad.html", []).run(["$templateCache", function($templateCache) {
    $templateCache.put("tmpl/keypad.html", 
      '<div id="generic-keypad" class="keypad-container"> \
          <md-dialog aria-label="keypad" class="keypad" ng-app ng-keypress="keypressed($event)"> \
            \
            <md-toolbar> \
              <div class="md-toolbar-tools"> \
                <h2>{{title || "Keypad"}}</h2> \
                <span flex></span> \
                <!-- <md-button md-no-ink="true" class="md-icon-button" ng-click="closeDialog()"> \
                  <i class="md-icon dp45">clear</i> \
                </md-button> --> \
              </div> \
            </md-toolbar> \
              \
            <md-dialog-content class="sticky-container"> \
                     <md-input-container class="login"> \
                       <input class="dialog-close" ng-model="keypadInfo.value"  ng-trim="false" type="text"> \
                     </md-input-container> \
                            \
                     <md-grid-list md-cols="6" md-row-height="1:1" md-gutter="1px" style="" class="keypad-grid"> \
                         <md-grid-tile ng-repeat="element in keypad" md-colspan="2" md-rowspan="2"> \
                          <md-button md-no-ink="true" class="md-raised" ng-click="input(element.text)" ng-if="!element.useIcon"> \
                            {{::element.text}} \
                          </md-button> \
                            \
                          <md-button md-no-ink="true" class="md-raised" ng-click="reset()" ng-if="element.useIcon && element.text==\'reset\'"> \
                            <i ng-show="keypadInfo.value" class="md-icon dp40">replay</i> \
                            <i ng-hide="keypadInfo.value" class="md-icon dp40">clear</i> \
                          </md-button> \
                            \
                          <md-button md-no-ink="true" class="md-raised" ng-click="backspace()" ng-if="element.useIcon && element.text==\'backspace\'"> \
                            <i class="md-icon dp40">backspace</i> \
                          </md-button> \
                         </md-grid-tile> \
                            \
                         <!-- finish button --> \
                         <!-- ng-show="keypadInfo.value"  --> \
                         <md-grid-tile md-rowspan="2" md-colspan="6" ng-show="keypadInfo.value"> \
                           <md-button md-no-ink="true" class="md-raised submit" ng-click="next($event)">{{::ui["done"] || "完成"}}</md-button> \
                         </md-grid-tile> \
                     </md-grid-list> \
                   <!-- </div> --> \
            </md-dialog-content> \
        </md-dialog> \
      </div>');
  }]);

  angular.module("tmpl/keypad-free.html", []).run(["$templateCache", function($templateCache) {
    $templateCache.put("tmpl/keypad-free.html", 
      '<div id="free-item-keypad" class="keypad-container full-keyboard-container"> \
          <md-dialog aria-label="keypad" class="keypad" ng-app ng-keypress="keypressed($event)"> \
            \
            <md-toolbar> \
              <div class="md-toolbar-tools"> \
                <h2>{{::ui[\'custom\']}}</h2> \
                <span flex></span> \
              </div> \
            </md-toolbar> \
              \
            <md-dialog-content class="sticky-container"> \
               <div layout="row"> \
                 <md-input-container class="login" flex="30"> \
                   <label>{{ui[\'item_name\']}}</label> \
                   <input name="item-name" autofocus ng-model="keypadInfo.name" ng-class="{\'focused\' : currentFieldName == \'name\'}" ng-trim="false" type="text" ng-click="focusField(\'name\')"> \
                 </md-input-container> \
                 <md-input-container class="login" flex="30"> \
                   <label>{{ui[\'item_price\']}}</label> \
                   <input name="item-price" type="text" ng-model="keypadInfo.value" ng-class="{\'focused\' : currentFieldName == \'value\'}" ng-trim="false" ng-click="focusField(\'value\')"> \
                 </md-input-container> \
               </div> \
                      \
                <div class="md-grid-container keyboard-full" style="display: inline-block;"> \
                  <!-- keyboard --> \
                  <div class="materialize-keypad col-10x4" style=""> \
                    <div class="row"> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="inputFree(\'q\')">q</button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="inputFree(\'w\')">w</button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="inputFree(\'e\')">e</button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="inputFree(\'r\')">r</button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="inputFree(\'t\')">t</button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="inputFree(\'y\')">y</button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="inputFree(\'u\')">u</button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="inputFree(\'i\')">i</button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="inputFree(\'o\')">o</button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="inputFree(\'p\')">p</button></div> \
                    </div> \
                    <div class="row"> \
                      <div class="col" style="width:5%"></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="inputFree(\'a\')">a</button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="inputFree(\'s\')">s</button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="inputFree(\'d\')">d</button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="inputFree(\'f\')">f</button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="inputFree(\'g\')">g</button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="inputFree(\'h\')">h</button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="inputFree(\'j\')">j</button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="inputFree(\'k\')">k</button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="inputFree(\'l\')">l</button></div> \
                    </div> \
                    <div class="row"> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="inputFree(\'z\')">z</button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="inputFree(\'x\')">x</button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="inputFree(\'c\')">c</button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="inputFree(\'v\')">v</button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="inputFree(\'b\')">b</button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="inputFree(\'n\')">n</button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="inputFree(\'m\')">m</button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="inputFree(\',\')">,</button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="inputFree(\'.\')">.</button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="inputFree(\'#\')">#</button></div> \
                    </div> \
                    <div class="row"> \
                      <div class="col s10-2"><button class="btn md-button md-grid-button" ng-click="inputFree(\'tab\')">tab</button></div> \
                      <div class="col s10-5"><button class="btn md-button md-grid-button" ng-click="inputFree(\'space\')">space bar</button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button flex-center" ng-click="backspaceFree()"><i class="md-icon">backspace</i></button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="inputFree(\'-\')">-</button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="inputFree(\'*\')">*</button></div> \
                    </div> \
                  </div> \
                  <!-- keypad --> \
                  <div class="materialize-keypad col-3x4" style=""> \
                    <div class="row"> \
                      <div class="col s4 left-edge"><button class="btn md-button md-grid-button" ng-click="appendNumFree(1)">1</button></div> \
                      <div class="col s4"><button class="btn md-button md-grid-button" ng-click="appendNumFree(2)">2</button></div> \
                      <div class="col s4 right-edge"><button class="btn md-button md-grid-button" ng-click="appendNumFree(3)">3</button></div> \
                    </div> \
                    <div class="row"> \
                      <div class="col s4 left-edge"><button class="btn md-button md-grid-button" ng-click="appendNumFree(4)">4</button></div> \
                      <div class="col s4"><button class="btn md-button md-grid-button" ng-click="appendNumFree(5)">5</button></div> \
                      <div class="col s4 right-edge"><button class="btn md-button md-grid-button" ng-click="appendNumFree(6)">6</button></div> \
                    </div> \
                    <div class="row"> \
                      <div class="col s4 left-edge"><button class="btn md-button md-grid-button" ng-click="appendNumFree(7)">7</button></div> \
                      <div class="col s4"><button class="btn md-button md-grid-button" ng-click="appendNumFree(8)">8</button></div> \
                      <div class="col s4 right-edge"><button class="btn md-button md-grid-button" ng-click="appendNumFree(9)">9</button></div> \
                    </div> \
                    <div class="row"> \
                      <div class="col s4 left-edge"><button class="cancel btn md-button md-grid-button" ng-click="cancel()"><i class="md-icon dp40" aria-hidden="false" ng-show="lastActiveField == \'item-name\' && keypadInfo.name == \'\' || lastActiveField == \'item-price\' && keypadInfo.value == \'\'">clear</i> \
                      <i ng-show="lastActiveField == \'item-name\' && keypadInfo.name != \'\' || lastActiveField == \'item-price\' && keypadInfo.value != \'\'" class="md-icon dp40" aria-hidden="false">replay</i></button></div> \
                      <div class="col s4"><button class="btn md-button md-grid-button" ng-click="appendNumFree(0)">0</button></div> \
                      <div class="col s4 right-edge"><button class="proceed btn md-button md-grid-button" ng-click="nextFree($event)"><i class="md-icon dp45">done</i></button></div> \
                    </div> \
                  </div> \
                </div> \
            </md-dialog-content> \
        </md-dialog> \
      </div>');
  }]);

  angular.module("tmpl/member.html", []).run(["$templateCache", function($templateCache) {
    $templateCache.put("tmpl/member.html", 
      '<div id="member-box" class="keypad-container"> \
          <md-dialog aria-label="Member" class="keypad" ng-app> \
            <md-toolbar> \
               <div class="md-toolbar-tools"> \
                 <h2>{{::ui["search_member"] || "查詢會員"}}</h2> \
                 <span flex></span> \
               </div> \
             </md-toolbar> \
              <md-dialog-content class="sticky-container"> \
                  <div class="member-info" layout="row" style="margin: auto;"> \
                    <div  style="max-width: 250px;"> \
                      <md-input-container class="login"> \
                        <label>{{ui["member_no"]}} / {{ui["member_phone"]}}</label> \
                        <!-- ng-always-focus="true", will make it look buggy --> \
                          <input class="dialog-close" login-input login-input-enter="next()" ng-model="loginValue" ng-trim="false" ng-change="change()" ng-focus="true" type="text" custom-always-focus="true"> \
                      </md-input-container> \
                      \
                      <md-grid-list md-cols="6" md-row-height="1:1" md-gutter="1px" style="" class="keypad-grid"> \
                          <md-grid-tile ng-repeat="element in keypad" md-colspan="2" md-rowspan="2"> \
                           <md-button md-no-ink="true" class="md-raised" ng-click="input(element.text)" ng-if="!element.useIcon"> \
                             {{::element.text}} \
                           </md-button> \
                      \
                           <md-button md-no-ink="true" class="md-raised" ng-click="reset()" ng-if="element.useIcon && element.text==\'reset\'"> \
                             <i ng-show="loginValue" class="md-icon dp40">replay</i> \
                             <i ng-hide="loginValue" class="md-icon dp40">clear</i> \
                           </md-button> \
                      \
                           <md-button md-no-ink="true" class="md-raised" ng-click="backspace()" ng-if="element.useIcon && element.text==\'backspace\'"> \
                             <i class="md-icon dp40">backspace</i> \
                           </md-button> \
                          </md-grid-tile> \
                      \
                          <!-- finish button --> \
                          <!-- ng-show="loginValue"  --> \
                          <md-grid-tile md-rowspan="2" md-colspan="6" ng-show="loginValue"> \
                            <md-button md-no-ink="true" class="md-raised submit" ng-click="next()">{{::ui["done"] || "完成"}}</md-button> \
                          </md-grid-tile> \
                      </md-grid-list> \
                    </div> \
                    <div flex> \
                      <md-card> \
                       <!-- <img src="card-image.png" class="md-card-image" alt="image caption"> --> \
                          <md-card-content ng-show="MemberNotFound"> \
                              找不到會員 \
                          </md-card-content> \
                              <md-card-content ng-hide="MemberNotFound"> \
                                  <h2>卡號：{{Member.vipcode}}</h2> \
                                  <ul> \
                                      <li>會藉：{{Member.vipgroup.string[0]}}</li> \
                                      <!--<li>累積點數：10,000,000</li>--> \
                                      <li>名稱：{{Member.name()}}</li> \
                                      <li>生日：{{Member.dob()}}</li> \
                                      <!--<li>最後一次光臨日期：2013年3月16日</li>--> \
                                  </ul> \
                              </md-card-content> \
                              <div class="md-actions" layout="row" layout-align="end center"> \
                                  <md-button md-no-ink="true" class="no-margin md-raised md-warn" ng-click="cancel()">取消/關閉</md-button> \
                                  <md-button md-no-ink="true" class="no-margin md-raised proceed" ng-show="MainService.mode == MainService.schema.mode.order" ng-click="MainService.Cart.assignMemberToOrder(Member)">套用</md-button> \
                              </div> \
                              <!-- <md-card-footer> \
                  Card footer \
                </md-card-footer> --> \
        </md-card> \
                    </div> \
                  </div> \
          \
              </md-dialog-content> \
          </md-dialog> \
        </div>');
  }]);

  angular.module('tmpl/pager-default-materialize.html', []).run(["$templateCache", function($templateCache) {
    $templateCache.put('tmpl/pager-default-materialize.html',
      '<div class="md-custom-grid-container md-custom-grid col-1x2 md-grid-pager"> \
          <div class="col s10-5"><button class="flex-center md-button-arrow md-raised md-button md-grid-button" ng-class="{disabled: noPrevious(), previous: align}" ng-click="selectPage(page - 1, $event)" ng-disabled="noPrevious()"><i ng-hide="loginValue" class="md-icon {{::iconSize || \'dp70\'}}">keyboard_arrow_left</i></button></div> \
          <div class="col s10-5"><button class="flex-center md-raised md-button md-grid-button" ng-class="{disabled: noNext(), next: align}" ng-click="selectPage(page + 1, $event)" ng-disabled="noNext()"><i ng-hide="loginValue" class="md-icon {{::iconSize || \'dp70\'}}">keyboard_arrow_right</i></button></div> \
      </div>');
  }]);

  angular.module('tmpl/gridMenuOption-cancel.html', []).run(["$templateCache", function($templateCache) {
    $templateCache.put('tmpl/gridMenuOption-cancel.html',
      '<div> \
        <md-grid-list md-cols="{{::gridCol}}" md-row-height="{{::gridHeight}}" md-gutter="1" style=""> \
            <md-grid-tile> \
                <md-button md-no-ink="true" class="md-raised" ng-click="selectAllItem();">全選</md-button> \
            </md-grid-tile> \
            <md-grid-tile> \
                <md-button md-no-ink="true" class="md-raised" ng-click="unselectAllItem();">取消選擇</md-button> \
            </md-grid-tile> \
            <md-grid-tile class="dummy-grid" ng-repeat="i in $parent.$parent.numberToArray( 2 ) track by $index"><span><span></md-grid-tile> \
            <md-grid-tile> \
                <md-button md-no-ink="true" class="md-raised cancel" ng-click="MainService.cancelOption();"><i class="md-icon dp45">clear</i></md-button> \
            </md-grid-tile> \
        </md-grid-list> \
      </div>');
  }]);

  // based on billing-menu.html
  angular.module("tmpl/search-bill-menu.html", []).run(["$templateCache", function($templateCache) {
    $templateCache.put("tmpl/search-bill-menu.html", 
      '<div flex="100" id="search-bill-menu" class="menu-area" flex layout="row" ng-controller="searchBillController as searchBill" style=""> \
            <div class="md-grid-container" flex="100" style="height: 100%;"> \
            \
              <md-input-container class="login" style="height: 14%; width: 50%;"> \
                  <label>{{::ui[\'bill_no\']}}</label> \
                  <input name="billNo" class="dialog-close" ng-model="billNo" ng-trim="false" type="text" custom-always-focus="true"> \
              </md-input-container> \
            \
            <div style="height: 10.5%; width: 45%; min-width: 335px; position: absolute; top: 6px; right: 6px;"> \
              <div class="materialize-keypad col-1x4"> \
                <div class="row"> \
                  <div class="col s10-2"><button class="flex-center btn md-button md-grid-button" ng-click="browseBill(\'prev\')"><i class="flex-center md-icon dp40">keyboard_arrow_up</i></button></div> \
                  <div class="col s10-2"><button class="flex-center btn md-button md-grid-button" ng-click="browseBill(\'next\')"><i class="flex-center md-icon dp40">keyboard_arrow_down</i></button></div> \
                  <div class="col s10-6"><button class="icon-with-text btn md-button md-grid-button" ng-click="lastBill()"><span>{{::ui[\'last_bill\']}}</span></button></div> \
                </div> \
              </div> \
            </div> \
            \
              <!-- keypad --> \
              <h2 class="header right-edge-margin fill-height" style="height:6%">{{ ::ui[\'bill_search\'] }}</h2> \
              <div layout="row" style="height: 60%;"> \
                <div flex="50" style="height: 100%;width: 300px;"> \
                  <div class="materialize-keypad col-3x4" style=""> \
                    <div class="row"> \
                      <div class="col s4 left-edge"><button class="btn md-button md-grid-button" ng-click="inputFree(\'1\')">1</button></div> \
                      <div class="col s4"><button class="btn md-button md-grid-button" ng-click="inputFree(2)">2</button></div> \
                      <div class="col s4 right-edge"><button class="btn md-button md-grid-button" ng-click="inputFree(3)">3</button></div> \
                    </div> \
                    <div class="row"> \
                      <div class="col s4 left-edge"><button class="btn md-button md-grid-button" ng-click="inputFree(4)">4</button></div> \
                      <div class="col s4"><button class="btn md-button md-grid-button" ng-click="inputFree(5)">5</button></div> \
                      <div class="col s4 right-edge"><button class="btn md-button md-grid-button" ng-click="inputFree(6)">6</button></div> \
                    </div> \
                    <div class="row"> \
                      <div class="col s4 left-edge"><button class="btn md-button md-grid-button" ng-click="inputFree(7)">7</button></div> \
                      <div class="col s4"><button class="btn md-button md-grid-button" ng-click="inputFree(8)">8</button></div> \
                      <div class="col s4 right-edge"><button class="btn md-button md-grid-button" ng-click="inputFree(9)">9</button></div> \
                    </div> \
                    <div class="row"> \
                      <div class="col s4 left-edge"><button class="cancel btn md-button md-grid-button" ng-click="cancel()"><i class="md-icon dp40" aria-hidden="false" style="">clear</i></button></div> \
                      <div class="col s4"><button class="btn md-button md-grid-button" ng-click="inputFree(0)">0</button></div> \
                      <div class="col s4 right-edge"><button class="proceed2 btn md-button md-grid-button" ng-click="searchBill($event)"><i class="md-icon dp45">done</i></button></div> \
                    </div> \
                  </div> \
                </div> \
                <div flex="50" class="bill-info"> \
                  {{::ui[\'ref_no\'] + ui[\'colon\']}} {{MainService.Cart.refNo}} <br/>\
                  {{::ui[\'trans_date\'] + ui[\'colon\']}} {{MainService.Cart.transTime}} \
                </div> \
              </div> \
                \
              <!-- keypad for cash shortcut --> \
                <div style="height: 20%"> \
                  <div class="materialize-keypad col-1x4"> \
                    <div class="row"> \
                      <div class="col s8"><button class="btn md-button md-grid-button" ng-click="changeBillInfo()">更改付款資料</button></div> \
                      <!--<div class="col s4"><button class="btn md-button md-grid-button" ng-click="changeCreditCard()">更改信用卡</button></div>--> \
                      <div class="col s2"><button class="btn md-button md-grid-button" ng-click="printBill()">列印</button></div> \
                      <div class="col s2"><button class="icon-with-text proceed btn md-button md-grid-button" ng-click="MainService.mode = MainService.schema.mode.main"><i class="md-icon dp45">done</i><span>{{::ui["done"] || "完成"}}</span></button></div> \
                    </div> \
                  </div> \
                </div> \
            </div> \
        </div>')
  }]);

  // including the following templates: options, gridMenuOption-order, cancelOptions, modifier
  angular.module("tmpl/order.html", []).run(["$templateCache", function($templateCache) {
      $templateCache.put('tmpl/order.html',
        '<div id="order-panel" class="menu-area" layout="row" ng-hide="isModeOrder(\'split\')"> \
        <!-- sidebar column --> \
          <div flex="30" layout="column" class="sidebar" ng-hide=" modeCheckBill(\'summary\')"> \
            <!-- table information --> \
            <div layout="row" class="header fill-height table-info" style=""> \
            \
                <div class="table-summary" layout="column" ng-show="isModeOrder(\'foodControl\')"> \
                    <span class="inline-block">{{::ui[\'food_control\']}}</span> \
                </div> \
                <div layout="column" class="table-summary" ng-hide="isModeOrder(\'foodControl\')"> \
                    <!-- <span flex></span> --> \
                    <!-- <span class="inline-block no-of-people">4人</span> --><!-- 人 is unit, maybe use filter in the future --> \
                    <span class="inline-block table-no">{{MainService.Cart.tableNo}}</span> \
                    <div flex class="inline-block">\
                      <input-keypad-noofpeople title="No of People" class="inline-block no-of-people display-mode" maxlength="2"><input type="text" ngminlength="1" ngmaxlength="3" ng-model="keypadInfo.value" ng-click="callKeypad(dialogTest1)" custom-always-blur="true" /></input-keypad-noofpeople>\
                      \
                      <!--<input-keypad title="No of People" class="inline-block no-of-people display-mode" maxlength="2"><input type="text" ngminlength="1" ngmaxlength="3" ng-model="keypadInfo.value" ng-click="callKeypad(dialogTest1)" custom-always-blur="true" /></input-keypad> -->\
                      \
                      \
                      <!-- <input type="text" class="no-of-people display-mode" ngMinlength="1" ngMaxlength="3" name="no_of_people" value="{{loginValue}}" novalidate ng-click="MainService.changePeopleNo($event)"> --><span class="inline-block unit">人</span></div> \
                      \
                  \
                    <!-- this present button area  --> \
                </div> \
                <span flex></span> \
                <div style="position: absolute; top: 6px; right: 6px;" class="table-function" ng-controller="scrollController" layout="row"> \
                    <md-button md-no-ink="true" class="md-40p md-raised md-rounded-square md-densed" ng-click="scrollUp( \'#order-list\', 30 )"> \
                        <i class="md-icon dp40">keyboard_arrow_up</i> \
                    </md-button><md-button md-no-ink="true" class="md-40p md-raised md-rounded-square md-densed" ng-click="scrollDown( \'#order-list\', 30 )"> \
                        <i class="md-icon dp40">keyboard_arrow_down</i> \
                        <!-- ng-show="modeCheckOrderFood();"  -->\
                    </md-button><md-button md-no-ink="true" class="show-details md-40p md-raised md-rounded-square md-densed" ng-click="toggleItemDisplay1(\'detail\')" aria-label=""> \
                        <md-icon class="dp30" md-svg-src="assets/icon/identification24.svg"></md-icon> \
                    </md-button><md-button md-no-ink="true" class="show-details md-40p md-raised md-rounded-square md-densed" ng-click="toggleItemDisplay2(\'zeroItem\')" aria-label=""> \
                        <i class="md-icon dp48">exposure_zero</i> \
                    </md-button> \
                </div> \
            </div> \
                        \
            <!-- scrollable area only food list --> \
            <!-- class="active-item-list" --> \
            <md-sidenav id="order-list" flex class="order-list-wrapper" md-is-locked-open="$mdMedia(\'min-width: 0px\')" md-scroll-y style=""> \
                <!--<div class="order__list-order-list col order-list" ng-hide="modeCheckOrder(\'foodControl\')"> \
                  <ul class="list-group"> \
                    <li class="list-group-item" ng-repeat="item in MainService.Cart.orderList track by $index" ng-hide="item.getQtyWithVoid() <= 0 && !modeCheckOrder(\'detail\')" ng-click="item.editItem($event);" ng-class="{active : item.isEdit}"> \
                        \
                      <div layout="row" class="order-list-item"> \
                         <span flex="35" class="index" ng-show="modeCheckOrder(\'detail\')">#{{item.index}}</span> \
                         <span flex class="code" ng-show="modeCheckOrder(\'detail\')">{{item.code}}</span> \
                         <i class="md-icon delete" ng-click="item.deleteItem($event,\'{{$index}}\')" ng-if="modeCheckOrder(\'detailNormal\');">highlight_off</i> \
                        \
                      </div> \
                      \
                      <ul class="ordered__food__info-container no-padding"> \
                        \
                          <li layout="row" class="food-info"> \
                              \
                              <div flex="50" layout="column"> \
                                  <span class="name"><a href="#">{{item.getName()}}</a></span> \
                                  <ul class="modifier" ng-show="item.voidRemark != \'\'"> \
                                      <li>{{item.voidRemark}}</li> \
                                  </ul> \
                                  <ul class="modifier" ng-show="item.modifier.length != 0"> \
                                      <li ng-repeat="m in item.modifier">{{m.label}}</li> \
                                  </ul> \
                                  <ul class="kitchenMsg" ng-show="item.kitchenMsgOld.length != 0"> \
                                      <li ng-repeat="k in item.kitchenMsgOld track by $index">{{k.label}} {{item.getIndex()}}</li> \
                                  </ul> \
                                  <ul class="kitchenMsg" ng-show="item.kitchenMsg.length != 0"> \
                                      <li ng-repeat="k in item.kitchenMsg track by $index">{{k.label}}<a href="#" ng-click="MainService.Cart.deleteKitchenMsg(item.kitchenMsg, $index, $event)"><i class="md-icon remove">remove_circle</i></a></li> \
                                  </ul> \
                              </div> \
                              <div flex layout="column"> \
                                  <span class="qty">{{item.getQtyWithVoid()}}</span> \
                              </div> \
                              \
                              <div flex="35" layout="column"> \
                                  <span class="price">{{ item.getTotalPriceWithVoid() | currency : INFO.currency : 1 }}</span> \
                              </div> \
                          </li> \
                          <ul class="food_info-option-list">\
                            <li ng-repeat="optionList in item.option track by $index" ng-class="{active : option.isEdit}"> \
                              <ul class="food_info-option_list-option">\
                                \
                                  <div flex="50" layout="column"> \
                                      <span class="name"><a class="existing__order__list order_list-option-action" href="#" ng-click="MainService.Cart.cartListEditOption($event, option.getIndex(), item, optionList, option);">{{option.getName()}} {{option.getIndex()}}</a></span> \
                                      <ul class="option-modifier"></ul> \
                                      <ul class="option-modifier" ng-show="option.modifier.length != 0"> \
                                          <li ng-repeat="m in option.modifier">{{m.label}}</li> \
                                      </ul> \
                                      <ul class="option-kitchenMsg" ng-show="option.kitchenMsgOld.length != 0"> \
                                          <li ng-repeat="k in option.kitchenMsgOld">{{k.label}}</li> \
                                      </ul> \
                                      <ul class="option-kitchenMsg option-kitchenMsg-new" ng-show="option.kitchenMsg.length != 0"> \
                                          <li ng-repeat="k in option.kitchenMsg track by $index">{{k.label}}<a href="#" ng-click="MainService.Cart.deleteKitchenMsg(option.kitchenMsg, $index, $event)"><i class="md-icon remove">remove_circle</i></a></li> \
                                      </ul> \
                                  </div> \
                                  <div flex layout="column"> \
                                      <span class="qty">{{option.qty}}</span> \
                                  </div> \
                                  <div flex="35" layout="column"> \
                                      <span class="price"></span> \
                                  </div> \
                                </li> \
                                \
                                \
                              </ul>\
                            </li> \
                          </ul> \
                          <li layout="row" ng-repeat="subitemList in item.subitem track by $index" class="food-info" ng-if="subitemList.qty > 0"> \
                              <div flex="50" layout="column"> \
                                  <span class="name"><a href="#">{{subitemList.getName()}}</a></span> \
                                  \
                                  <ul class="modifier"></ul> \
                              </div> \
                              <div flex layout="column"> \
                                  <span class="qty">{{subitemList.qty}}</span> \
                              </div> \
                              <div flex="35" layout="column"> \
                                  <span class="price"></span> \
                              </div> \
                          </li> \
                            \
                      </ul> \
                      \
                      <ul class="details no-padding" layout="column" style="" ng-show="modeCheckOrder(\'detail\')"> \
                          <li class="order-trace"> \
                              <span class="employee">{{item.staff}}</span>@<span class="time">{{item.time}}</span> \
                          </li> \
                      </ul> \
                    </li> \
                    \
                    <li class="list-group-item new-item" ng-repeat="item in MainService.Cart.cartList track by $index" ng-if="!item.isDelete" ng-class="{active : item.isEdit}"> \
                        <div layout="row"> \
                            <span flex="35" class="index" ng-show="modeCheckOrder(\'detail\')">#0[New]</span> \
                            <span flex class="code" ng-show="modeCheckOrder(\'detail\')">{{item.code}}</span> \
                            <i class="md-icon delete" ng-click="item.deleteItem($event,\'{{$index}}\')">highlight_off</i> \
                        </div> \
                        <ul class="new__food__info-container no-padding"> \
                            <li layout="row" class="food-info" ng-click="item.editItem($event)"> \
                                <div flex="50" layout="column"> \
                                    <span class="name"><a href="#">{{item.getName()}}</a></span> \
                                    <ul class="modifier" ng-show="item.voidRemark != \'\'"> \
                                        <li>{{item.voidRemark}}</li> \
                                    </ul> \
                                    <ul class="modifier" ng-show="item.modifier.length != 0"> \
                                        <li ng-repeat="m in item.modifier">{{m.label}}<a href="#" ng-click="MainService.Cart.addModifier(m, $event)"><i class="md-icon remove">remove_circle</i></a></li> \
                                    </ul> \
                                    <ul class="kitchenMsg" ng-show="item.kitchenMsg.length != 0"> \
                                        <li ng-repeat="k in item.kitchenMsg">{{k.label}}<a href="#" ng-click="MainService.Cart.deleteKitchenMsg(item.kitchenMsg, $index, $event)"><i class="md-icon remove">remove_circle</i></a></li> \
                                    </ul> \
                                </div> \
                                <div flex layout="column"> \
                                    <span class="qty">{{item.getQty()}}</span> \
                                </div> \
                                <div flex="35" layout="column"> \
                                    <span class="price">{{ item.getTotalPrice() | currency : INFO.currency : 1 }}</span> \
                                </div> \
                            </li> \
                            <ul class="new__food__info-option-list">\
                            <li ng-repeat="optionList in item.option track by $index" ng-class="{active : option.isEdit}"> \
                                <li layout="row" ng-repeat="option in optionList.items track by $index" class="order_list-option food-info" ng-if="option.qty > 0" ng-class="{active : option.isEdit}" ng-init="$localIdx = counter();"> \
                                  <div flex="50" layout="column"> \
                                      <span class="name"><a href="#" ng-click="MainService.Cart.cartListSelectOption( $event, $localIdx, item, optionList, option );">{{option.getName()}}</a><a href="#" ng-click="option.edit($event, optionList.optionGroupCode)"><i class="md-icon remove">create</i></a></span> \
                                      <ul class="option-modifier" ng-show="option.modifier.length != 0"> \
                                          <li ng-repeat="m in option.modifier">{{m.label}}<a href="#" ng-click="MainService.Cart.deleteModifier(option.modifier, $index)"><i class="md-icon remove">remove_circle</i></a></li> \
                                      </ul> \
                                      <ul class="option-kitchenMsg" ng-show="option.kitchenMsg.length != 0"> \
                                          <li ng-repeat="k in option.kitchenMsg">{{k.label}}<a href="#" ng-click="MainService.Cart.deleteKitchenMsg(option.kitchenMsg, $index, $event)"><i class="md-icon remove">remove_circle</i></a></li> \
                                      </ul> \
                                  </div> \
                                  <div flex layout="column"> \
                                      <span class="qty">{{option.qty}}</span> \
                                  </div> \
                                  <div flex="35" layout="column"> \
                                      <span class="price"></span> \
                                  </div> \
                                </li>{{$totalQty}}\
                              </ul>\
                            </li> \
                          </ul> \
                              \
                            <li layout="row" ng-repeat="subitemList in item.subitem track by $index" class="food-info"> \
                                <div flex="50" layout="column"> \
                                    <span class="name"><a href="#">{{subitemList.getName()}}</a></span> \
                                    <ul class="modifier"></ul> \
                                </div> \
                                <div flex layout="column"> \
                                    <span class="qty">{{subitemList.qty}}</span> \
                                </div> \
                                <div flex="35" layout="column"> \
                                    <span class="price"></span> \
                                </div> \
                            </li> \
                        </ul> \
                    </li> \
                  </ul> \
                </div>--> \
                \
                <order-list ng-hide="isModeOrder(\'foodControl\')"></order-list>\
                \
                <div class="col food-control-list" ng-show="isModeOrder(\'foodControl\')"> \
                    <!-- <header class="text-center row-xs-1">食品控制</header> --> \
                    <ul class="list-group"> \
                        <!-- 套餐 --> \
                        <li class="list-item" ng-repeat="item in MainService.stock.list track by $index" ng-click="item.editItem();callKeypad($event, dialogTest1,item)" ng-controller="keypadControllerFoodControl" ng-class="{active : item.isEdit}" layout="row"> \
                            <span flex="75" class="name"><a href="#">{{item.getName()}}</a></span> \
                            <span flex class=""></span> \
                            <span class="qty" ng-if="item.qty >= 0 && item.qty != 999">{{item.qty}}</span> \
                            <span class="qty" ng-if="item.qty < 0">暫停</span> \
                            <span class="qty" ng-if="item.qty == 999">準備中</span> \
                        </li> \
                    </ul> \
                    <footer class="footer"> \
                        <div class="function"> \
                            <md-button md-no-ink="true" class="no-margin md-raised md-warn" ng-click="MainService.Cart.cancelOrder2();">關閉</md-button> \
                            <md-button md-no-ink="true" class="no-margin md-raised proceed" ng-click="MainService.stock.reset(item)">恢復使用所有食品</md-button> \
                            <!-- <button class="btn btn-default btn-block"></button> --> \
                            <!-- <button class="btn btn-danger btn-block" ng-click="panel.showOrderPanel()"><span class="glyphicon glyphicon-remove"></span> 關閉</button> --> \
                        </div> \
                    </footer> \
                </div> \
            </md-sidenav> \
            \
            <!-- total etc --> \
            <div id="" class="footer account-summary" ng-hide="isModeOrder(\'foodControl\')"> \
                <div class="account" class="row no-margin" ng-show="MainService.Cart.member.vipcode != null"> \
                    <div class=""><strong>{{ ::ui[\'member\'] + ui[\'colon\'] }}</strong><span>{{MainService.Cart.member.name()}} <span class="card-no">#{{MainService.Cart.member.vipcode}}</span></span></div> \
                </div> \
                <div layout="row" class="row no-margin" ng-show="MainService.Cart.member.vipcode != null"> \
                    <div class="inline-block">{{ ::ui[\'member_discount\'] }}</div><span flex></span><div class="inline-block">{{ MainService.Cart.price | currency : INFO.currency : 1 }}</div> \
                </div> \
                \
                <div layout="row" class="row no-margin"> \
                    <div class="inline-block">{{ ui[\'subtotal\'] }}</div><span flex></span><div class="inline-block">{{ MainService.Cart.price | currency : INFO.currency : 1 }}</div> \
                </div> \
                        \
                <div layout="row" class="row no-margin"> \
                    <div class="inline-block">{{ ::ui[\'service_charge\'] }}</div><span flex></span><div class="inline-block">{{ MainService.Cart.serviceCharge | currency : INFO.currency : 1 }}</div> \
                </div> \
                        \
                <div layout="row" class="row no-margin"> \
                    <!--<div class="inline-block discount">{{ ::ui[\'member_discount\'] }}</div><span flex></span><div class="inline-block">-$100.00</div>--> \
                </div> \
                        \
                <div layout="row" class="row no-margin"> \
                    <div class="inline-block">{{ ::ui[\'bill_decimal\'] }}</div><span flex></span><div class="inline-block">{{ MainService.Cart.remainings | currency : INFO.currency : 1 }}</div> \
                </div> \
                        \
                <div layout="row" class="row no-margin"> \
                    <div class="inline-block">{{ ::ui[\'total_amount\'] }}</div><span flex></span><div class="inline-block">{{ MainService.Cart.totalPrice | currency : INFO.currency : 1 }}</div> \
                </div> \
                \
                <!--<div layout="row" class="row no-margin discount-input row-custom" ng-if="modeCheckBill();" ng-repeat="discoutList in appliedDiscount() track by $index">-->\
                \
                <div layout="row" class="row no-margin discount-input row-custom" ng-repeat="discoutList in appliedDiscount() track by $index">\
                  <div class="inline-block">{{discoutList.name1}}<a ng-if="modeCheckBill(\'printBill\') && !modeCheckBill(\'amendBill\') && isNotModeOrder(\'searchBill\');" href="#" class="warn btn-delete" ng-click="deletDiscount($index)">刪除</a></div><span flex></span><div class="inline-block">{{discoutList.price | currency : INFO.currency : 1  }}</div> \
                </div> \
                <div layout="row" class="row no-margin price-input row-custom" ng-if="modeCheckBill();" ng-repeat="methodList in appliedPaymentMethod() track by $index"> \
                  <div class="inline-block">{{ui[methodList.method]}} <a ng-if="modeCheckBill(\'printAmendBill\');" href="#" class="warn btn-delete" ng-click="deletePaymentMethod($index)">刪除</a></div><span flex></span><div class="inline-block">{{methodList.amount | currency : INFO.currency : 1  }}</div> \
                </div> \
                \
                <div layout="row" class="row no-margin price-input row-custom" ng-if="(appliedPaymentMethod().length > 0 || appliedDiscount().length > 0) && modeCheckBill(\'searchAmendBill\');"> \
                    <div class="inline-block"><span ng-if="MainService.remainerAmount >= 0 && MainService.isCash">{{ ::ui[\'payment_change\'] }}</span><span ng-if="MainService.remainerAmount < 0">{{ ::ui[\'remainder\'] }}</span><span ng-if="MainService.remainerAmount >= 0 && MainService.isNotCash">{{ ::ui[\'tips\'] }}</span></div>\
                    <span flex></span><div class="inline-block">{{ MainService.remainerAmount | currency : INFO.currency : 1  }}</div> \
                </div> \
            </div> \
          </div> \
          \
          <!-- dummy control is for shortcut buttons demo display --> \
          <div id="food-menu" class="menu-area" flex layout="column" style="" ng-show="isModeOrder(\'foodMenu\') && isModeOrderMenu()" ng-controller="foodMenuController as foodMenu"> \
              <!-- category items --> \
              <div id="category" class="md-custom-grid col-2x5" style="height:25%;" ng-show="isModeItem(\'normal\')"> \
                  <div class="row" style=""> \
                    <div class="col s10-2" ng-repeat="category in MainService.food.category | startFrom:(currentCatsPage - 1) * MainService.catsPerPage | limitTo: MainService.catsPerPage" ng-if="category.items.length > 0"><button class="md-raised md-button md-grid-button {{::category.color}}" ng-click="MainService.selectCategory(category, $event)">{{::category.name[INFO.lang]}}</button></div> \
                     \
                    <div class="col s10-2 dummy-grid" ng-if="MainService.noOfCategoryPage == currentCatsPage && MainService.food.category.length % MainService.catsPerPage > 0" ng-repeat="i in MainService.getPageNumber( MainService.catsPerPage - MainService.food.category.length % MainService.catsPerPage ) track by $index"><span></span></div> \
                    <pager-custom icon-size="\'dp70\'" template="tmpl/pager-default-materialize.html" total-items="MainService.totalCats" items-per-page="MainService.catsPerPage" ng-model="currentCatsPage"></pager-custom> \
                  </div> \
              </div> \
              <div id="category-item" class="md-custom-grid col-3x5" style="height:50%;" ng-show="isModeItem(\'normal\')"> \
                  <div class="row" style=""> \
                  <!-- MainService.Cart.addItem(item); --> \
                     <div class="col s10-2" ng-repeat="item in MainService.currentCategory.items | startFrom:(currentPage - 1) * MainService.itemsPerPage | limitTo: MainService.itemsPerPage">\
                          <!-- food control -->\
                          <button class="md-raised md-button md-grid-button {{::item.color}}" ng-click="callKeypadFree($event, dialogTest1,item)" ng-controller="keypadControllerOpenItem" ng-class="{suspend: item.stock != undefined && (item.stock < 0 || item.stock === 0)}">\
                          \<!--<span class="badge -->\
                               <span class="flex-center badge suspend" ng-if="item.stock != undefined && (item.stock < 0 || item.stock === 0)">暫停</span> \
                               <span class="flex-center badge suspend" ng-if="item.stock != undefined && (item.stock === 999)">準備中</span>\
                               <div class="name">{{::MainService.fn.shorten(item.name[INFO.lang])}}</div> \
                             \
                               <span class="badge stock" ng-if="item.stock != undefined && item.stock > 0 && item.stock != 999">Q: {{item.stock}}</span> \
                               <small class="secondary"> \
                                   <span class="code" ng-show="modeCheckOrderItemDetail()">{{::item.code}}</span> \
                                   <span class="code" ng-show="modeCheckOrderItemDetail(\'printer\')">{{::item.printer}}</span> \
                                   <!--<span class="badge stock">15</span>--> \
                               </small> \
                               <div class="price">{{ ::item.unitprice / 100 | currency :INFO.currency :1 }}</div> \
                     </button></div> \
                      \
                      <!-- last page?{{ MainService.noOfItemPage - currentPage }} \
                      recoprocol dummy {{MainService.itemsPerPage - MainService.totalItems % MainService.itemsPerPage}} -->\
                     <div class="col s10-2 dummy-grid" ng-if="MainService.noOfItemPage - currentPage == 0" ng-repeat="i in MainService.getPageNumber( MainService.itemsPerPage - MainService.totalItems % MainService.itemsPerPage ) track by $index"><span></span></div> \
                    <pager-custom icon-size="\'dp70\'" template="tmpl/pager-default-materialize.html" total-items="MainService.totalItems" items-per-page="MainService.itemsPerPage" ng-model="currentPage"></pager-custom> \
                  </div> \
              </div> \
                  \
              <!--<ng-include include-replace src="\'tmpl/options.html\'"></ng-include>--> \
              <food-options style="height: 75%;" ng-show="isModeItem(\'option\')"></food-options>\
              <option-food-control ng-show="isModeItem(\'optionFoodControl\') && isModeOrder(\'foodControl\')"></option-food-control> \
              \
              <div id="shortcuts" class="md-custom-grid col-1x1" style="height:15%;" ng-controller="dummyCtrl as dummy" ng-show="isNotModeItem([\'optionFoodControl\', \'cancel\', \'modifier\'])"> \
                  <div class="row" style="margin-bottom: 0;"> \
                    <div class="col s8-1" ng-repeat="shortcut in ::dummy.shortcuts"><button class="md-raised md-button md-grid-button" ng-click="MainService.Cart.changeItemQty(shortcut.name)">X {{::shortcut.name}}</button></div> \
                     \
                    <!--<div class="col s8-1 dummy-grid"></div>--> \
                  </div> \
              </div> \
               \
              <grid-menu-option class="food_menu-menu-option menu-option md-grid-container" cancel-param="{}" grid-height="\'fit\'" grid-col="5" template="tmpl/gridMenuOption-order.html" flex="100" layout="column" style="height: 10%;" ng-if="isModeItem() && isNotModeOrder(\'billing\')"></grid-menu-option> \
              \
              <ng-include include-replace src="\'tmpl/cancelOptions.html\'"></ng-include> \
              <ng-include include-replace src="\'tmpl/modifier.html\'"></ng-include> \
          </div> \
            \
          <div class="order-mode-container" flex layout="column" ng-if="modeCheckBill(\'searchBill\')" ng-include="\'tmpl/search-bill-menu.html\'"></div> \
          \
          <div class="order-mode-container" flex layout="column" ng-if="modeCheckBill(\'amendBill\');" ng-include="\'tmpl/amend-bill-menu.html\'"></div> \
          \
          <!--<div class="order-mode-container" flex layout="column" ng-if="modeCheckOrder(\'transfer\');" ng-include="\'tmpl/transfer-item.html\'"></div>--> \
          <transfer-item-menu class="order-mode-container" flex layout="column" ng-if="modeCheckOrder(\'transfer\');"></transfer-item-menu>\
          \
          <div class="order-mode-container" flex layout="column" ng-if="modeCheckOrder(\'specialMessage\') || modeCheckFloor(\'kitchenMessage\');" ng-include="\'tmpl/kitchenMessage.html\'"></div> \
          \
          <div class="order-mode-container" flex layout="column" id="billing-container" ng-if="modeCheckBill(\'billPrint\');" ng-include="\'tmpl/billing-menu.html\'"></div> \
          <div flex layout="column" ng-if="modeCheckBill(\'summary\');" ng-include="\'tmpl/billing-summary.html\'"></div> \
        </div><!-- order-panel --> \
        <div class="divide-bill-container" ng-if="modeCheckFloor(\'split\')" ng-include="\'tmpl/divide-bill.html\'"></div>');
    }]);

  angular.module('tmpl/modifier.html', []).run(["$templateCache", function($templateCache) {
    $templateCache.put('tmpl/modifier.html',
      '<div id="modifier-menu" class="menu-area" flex layout="column" ng-controller="modifierCtrl as modifier" style="" ng-show="modeCheckOrderItem(\'modifier\')"> \
          <!-- option set menu, full page --> \
          <div class="md-custom-grid md-grid-container" style="height: calc(25% - 2px)" layout="column"> \
              <md-grid-list md-cols="6" md-row-height="fit" md-gutter="1" flex="100"> \
                  <!-- ng-if="category.items.length > 0" --> \
                  <md-grid-tile on-finish-render ng-repeat="option in ::modifier.modifierOption"> \
                      <md-button md-no-ink="true" class="md-raised button-dp40" ng-if="option.element == \'input\'"><input-keypad-modifier title="Number of Item" class="full-box inline-block display-mode" maxlength="3"><input type="text" ngminlength="1" ngmaxlength="3" ng-model="keypadInfo.value" ng-click="callKeypad($event, dialogTest1)" custom-always-blur="true" /></input-keypad-modifier><i class="md-icon dp80 md-watermark">keyboard</i></md-button> \
                      \
                      <md-button md-no-ink="true" class="md-raised button-dp40" ng-if="option.element == \'text\'" ng-click="MainService.Cart.changeItemQty(option.name)">{{::option.name}}</md-button> \
                  </md-grid-tile> \
                  <md-grid-tile class="dummy-grid" ng-repeat="i in ::numberToArray( 2 ) track by $index"><span><span></md-grid-tile> \
              </md-grid-list> \
          </div> \
            \
          <div class="modifier-option md-custom-grid col-3x5" style="height: 65%"> \
              <div class="row"> \
                <div class="col s10-2" ng-repeat="item in MainService.food.modifiers | startFrom:(currentPage - 1) * optionPerPage | limitTo: optionPerPage"><button class="md-raised md-button md-grid-button {{::category.color}}" ng-click="MainService.Cart.addModifier(item)">{{::item.label}}</button></div> \
                 \
                <div class="col s10-2 dummy-grid" ng-if="MainService.noOfModifierPage == currentPage && MainService.food.modifiers.length % optionPerPage > 0" ng-repeat="i in MainService.getPageNumber( optionPerPage - MainService.food.modifiers.length % optionPerPage ) track by $index"><span></span></div> \
                <pager-custom icon-size="\'dp56\'" template="tmpl/pager-default-materialize.html" total-items="MainService.food.modifiers.length" items-per-page="MainService.noOfModifierPage" ng-model="currentPage"></pager-custom> \
              </div> \
          </div> \
          \
          <div class="modifier-option md-custom-grid col-1x5" style="height: 10%"> \
              <div class="row no-padding-bottom"> \
                <div class="col s10-2 dummy-grid"><span></span></div> \
                <div class="col s10-2 dummy-grid"><span></span></div> \
                <div class="col s10-2 dummy-grid"><span></span></div> \
                <div class="col s10-2 dummy-grid"><span></span></div> \
                <!-- <div class="col s10-2"><button class="md-button md-grid-button" ng-click="MainService.cancelModifier()">{{::ui["food_modifier"]}}</button></div> \
                <div class="col s10-2"><button class="md-button md-grid-button" ng-click="MainService.cancelModifier()">{{::ui["drink_modifier"]}}</button></div> \
                <div class="col s10-2"><button class="md-button md-grid-button" ng-click="MainService.cancelModifier()">{{::ui["special_message"]}}</button></div> --> \
                <div class="col s10-2"><button class="cancel icon-with-text btn md-button md-button md-grid-button" ng-click="MainService.cancelModifier({modeOrder: true, modeItem: true, modeBase: false})"><i class="md-icon dp45">clear</i><span class="">{{::ui["cancel_back"] || "取消/返回"}}</span></button></div> \
              </div> \
          </div> \
          \
      </div>');
  }]);
// {{$parent.varName}}\
  angular.module('tmpl/gridMenuOption-order.html', []).run(["$templateCache", function($templateCache) {
    $templateCache.put('tmpl/gridMenuOption-order.html',
      '<div> \
        <md-grid-list md-cols="{{::gridCol}}" md-row-height="{{::gridHeight}}" md-gutter="1" style=""> \
            <md-grid-tile class="dummy-grid" ng-repeat="i in $parent.numberToArray( gridCol - 1 ) track by $index"><span></span></md-grid-tile> \
            <md-grid-tile> \
                <md-button md-no-ink="true" class="md-raised cancel icon-with-text" ng-click="MainService.Cart.cancelOrder2()" ng-hide="modeCheckOrderItem(\'option\')"><i class="md-icon dp45">clear</i>{{::ui["cancel_back"] || "取消/返回"}}</md-button> \
                <md-button md-no-ink="true" class="md-raised cancel icon-with-text" ng-click="MainService.Cart.cancelPackage();" ng-show="modeCheckOrderItem(\'option\')"><i class="md-icon dp45">clear</i>{{::ui["cancel_back"] || "取消/返回"}}</md-button> \
            </md-grid-tile> \
            <!-- <md-grid-tile> \
                <md-button md-no-ink="true" class="md-raised proceed icon-with-text" ng-click="MainService.submitCart();"><i class="md-icon dp45">done</i>{{::ui["done"] || "完成"}}</md-button> \
            </md-grid-tile> --> \
        </md-grid-list> \
      </div>');
  }]);

  angular.module('tmpl/cancelOptions.html', []).run(["$templateCache", function($templateCache) {
    $templateCache.put('tmpl/cancelOptions.html',
      '<div id="cancel-option-menu" class="menu-area" flex layout="column" ng-controller="cancelController as cancel" style="" ng-show="modeCheckOrderItem(\'cancel\')"> \
          <!-- option set menu, full page --> \
          <div class="cancel-option md-grid-container" style="height: 25%" layout="column"> \
              <md-grid-list md-cols="6" md-row-height="fit" md-gutter="1" style="" flex="100"> \
                  <!-- ng-if="category.items.length > 0" --> \
                  <md-grid-tile on-finish-render ng-repeat="option in cancel.cancelOption"> \
                      <md-button md-no-ink="true" class="md-raised button-dp40" ng-if="option.element == \'input\'"><input-keypad-cancel-item title="no_of_food_cancel" class="full-box inline-block display-mode" maxlength="3"><input type="text" ngminlength="1" ngmaxlength="3" ng-model="keypadInfo.value" ng-click="callKeypad($event, dialogTest1)" custom-always-blur="true" /></input-keypad-cancel-item><i class="md-icon dp80 md-watermark">keyboard</i></md-button> \
                      <md-button md-no-ink="true" class="md-raised button-dp40" ng-if="option.element == \'text\'" ng-click="MainService.Cart.voidItemCount = option.name">{{::option.name}}</md-button> \
                  </md-grid-tile> \
                  <md-grid-tile class="dummy-grid"><span><span></md-grid-tile> \
              </md-grid-list> \
            \
          </div> \
            \
          <div class="cancel-reason md-custom-grid col-3x5" style="height: 59%"> \
            <div class="row"> \
              <div class="col s10-2" style="margin-bottom: 0;" ng-repeat="option in INFO.cancelRemark | startFrom:(currentPage - 1) * MainService.cancelOptionPerPage | limitTo: MainService.cancelOptionPerPage"><button class="md-raised md-button md-grid-button" ng-click="MainService.Cart.voidItem(\'{{::option}}\')">{{::option}}</button></div> \
               \
              <div class="col s10-2 dummy-grid" ng-if="MainService.noOfCancelOptionPage == currentPage && MainService.cancelOptionTotalItems % MainService.cancelOptionPerPage > 0" ng-repeat="i in MainService.getPageNumber( MainService.cancelOptionPerPage - MainService.cancelOptionTotalItems % MainService.cancelOptionPerPage ) track by $index"><span></span></div> \
              <pager-custom icon-size="\'dp56\'" template="tmpl/pager-default-materialize.html" total-items="MainService.cancelOptionTotalItems" items-per-page="MainService.cancelOptionPerPage" ng-model="currentPage"></pager-custom> \
            </div> \
          </div> \
            \
          <grid-menu-option class="cancel_reason-menu-option menu-option md-grid-container" grid-height="\'fit\'" grid-col="5" template="tmpl/gridMenuOption-cancel.html" flex="100" layout="column" style="height: 16%;"></grid-menu-option> \
      </div>');
  }]);

  angular.module('tmpl/foodControlOption.html', []).run(["$templateCache", function($templateCache) {
    $templateCache.put('tmpl/foodControlOption.html',
      '<div id="bill-option-box" class=""> \
          <md-dialog aria-label="Bill Option" class="" ng-app> \
            <md-toolbar> \
               <div class="md-toolbar-tools"> \
                 <h2>{{::ui["please_select_item"] || "請選擇項目"}}</h2> \
                 <span flex></span> \
                 <md-button md-no-ink="true" class="md-icon-button" ng-click="closeDialog();"> \
                   <i class="md-icon dp45">clear</i> \
                 </md-button> \
               </div> \
             </md-toolbar> \
              <md-dialog-content> \
                  <div class="bill-option" layout="row" style="margin: auto;"> \
                    <div  style="max-width: 250px;"> \
                        <md-button md-no-ink="true" class="md-accent md-hue-3 md-mini md-raised md-button-text md-button-dp16 md-button md-default-theme" aria-label="" title="" ng-click="control(\'this\')"><span>{{::ui["control_this_item"]}}</span></md-button> \
                        <md-button md-no-ink="true" class="md-accent md-hue-3 md-mini md-raised md-button-text md-button-dp16 md-button md-default-theme" aria-label="" title="" ng-click="control(\'child\', dialogTest1, item)"><span>{{::ui["select_subitem"]}}</span></md-button> \
                    </div> \
                  </div> \
              </md-dialog-content> \
          </md-dialog> \
      </div>');
  }]);

  angular.module('tmpl/options.html', []).run(["$templateCache", function($templateCache) {
    $templateCache.put('tmpl/options.html',
      '<div id="option-menu" class="menu-area" flex layout="column"> \
          <!-- option set menu, full page --> \
          <div id="option" class="food__menu-option content md-custom-grid col-5x5" style="height: 100%; " ng-controller="keypadControllerOpenItem"> \
            <div class="row" style="position: relative;"> \
               <div class="col s10-2" ng-repeat="item in MainService.Cart.currentOptionList.items | startFrom:(currentPage - 1) * itemsPerPage | limitTo: itemsPerPage">\
                    <!-- food control -->\
                  <button class="md-raised md-button md-grid-button" ng-click="MainService.Cart.addOption(item, 1)" ng-class="{suspend: item.stock != undefined && (item.stock < 0 || item.stock === 0)}">\
                       <span class="flex-center badge suspend" ng-if="item.stock != undefined && (item.stock < 0 || item.stock === 0)">暫停</span> \
                       <span class="flex-center badge suspend" ng-if="item.stock != undefined && (item.stock === 999)">準備中</span>\
                       <div class="name">{{::MainService.fn.shorten(item.name[INFO.lang])}}</div> \
                     \
                       <span class="badge stock" ng-if="item.stock != undefined && item.stock > 0 && item.stock != 999">Q: {{item.stock}}</span> \
                       <div class="price">{{ item.getUnitPrice() }}</div> \
                  </button>\
                </div> \
                \
               <div class="col s10-2 dummy-grid" ng-if="noOfPage( MainService.Cart.currentOptionList.items.length, itemsPerPage ) == currentPage" ng-repeat="i in remainderPageArr( MainService.Cart.currentOptionList.items.length, itemsPerPage ) track by $index"><span></span></div> \
              <pager-custom icon-size="\'dp70\'" template="tmpl/pager-default-materialize.html" total-items="MainService.Cart.currentOptionList.items.length" items-per-page="itemsPerPage" ng-model="currentPage"></pager-custom> \
            </div> \
          </div> \
          \
          <!--<div id="option" class="food__menu-option content md-grid-container"> \
              <md-grid-list md-cols="5" md-row-height="99px" md-gutter="1"> \
                  <md-grid-tile ng-repeat="option in MainService.Cart.currentOptionList.items" ng-click="MainService.Cart.addOption(option)"> \
                      <md-button md-no-ink="true" class="md-raised">{{::option.name[INFO.lang]}}<br />{{::option.getUnitPrice()}}</md-button> \
                  </md-grid-tile> \
              </md-grid-list> \
                 \
              <div class="pager" style="position: absolute; bottom: 0; right: 0; width: calc((20% - 0.8px)*2 + 1px);"> \
                  <pager-custom grid-height="\'99px\'" total-items="totalItems" items-per-page="itemsPerPage" ng-model="currentPage"></pager-custom> \
              </div> \
          </div>--> \
      </div><!-- option-menu -->');
  }]);

  angular.module('tmpl/optionsFoodControl.html', []).run(["$templateCache", function($templateCache) {
    $templateCache.put('tmpl/optionsFoodControl.html',
      '<div id="option-control-menu" class="menu-area" flex layout="column" style="" style="height: 100%"> \
          <!-- option set menu, full page --> \
          <div id="option-control" class="food__menu-option content md-custom-grid col-5x5" style="height: 85%; " ng-controller="keypadControllerOpenItem"> \
            <div class="row" style="position: relative;"> \
               <div class="col s10-2" ng-repeat="item in MainService.Cart.optionControlList | startFrom:(currentPage - 1) * itemsPerPage | limitTo: itemsPerPage">\
                    <!-- food control -->\
                  <button class="md-raised md-button md-grid-button" ng-click="callKeypadFree($event, dialogTest1,item)" ng-class="{suspend: item.stock != undefined && (item.stock < 0 || item.stock === 0)}">\
                       <span class="flex-center badge suspend" ng-if="item.stock != undefined && (item.stock < 0 || item.stock === 0)">暫停</span> \
                       <span class="flex-center badge suspend" ng-if="item.stock != undefined && (item.stock === 999)">準備中</span>\
                       <div class="name">{{::MainService.fn.shorten(item.name[INFO.lang])}}</div> \
                     \
                       <span class="badge stock" ng-if="item.stock != undefined && item.stock > 0 && item.stock != 999">Q: {{item.stock}}</span> \
                       <div class="price">{{ item.getUnitPrice() }}</div> \
                  </button>\
                </div> \
                \
               <div class="col s10-2 dummy-grid" ng-if="noOfPage( MainService.Cart.optionControlList.length, itemsPerPage ) == currentPage" ng-repeat="i in remainderPageArr( MainService.Cart.optionControlList.length, itemsPerPage ) track by $index"><span></span></div> \
              <pager-custom icon-size="\'dp70\'" template="tmpl/pager-default-materialize.html" total-items="MainService.Cart.optionControlList.length" items-per-page="itemsPerPage" ng-model="currentPage"></pager-custom> \
            </div> \
              <!-- <div layout="column" style="position: relative; height: 85%">\
                <md-grid-list flex md-cols="5" md-row-height="fit" md-gutter="1" style="height: 100%"> \
                    <md-grid-tile ng-repeat="option in MainService.Cart.optionControlList | startFrom:(currentPage - 1) * itemsPerPage | limitTo: itemsPerPage" ng-click="callKeypadFree($event, dialogTest1,option)"> \
                        <md-button md-no-ink="true" class="md-raised">{{option.name[INFO.lang]}}<br />{{option.getUnitPrice()}}</md-button> \
                    </md-grid-tile> \
                </md-grid-list> \
                <div class="row option__control__menu-row" style="position:absolute; right: 0; bottom: 0; width: calc(40% - 1px); height: 20%; margin: 0;">\
                  <pager-custom icon-size="\'dp56\'" template="tmpl/pager-default-materialize.html" total-items="totalItems" items-per-page="itemsPerPage" ng-model="currentPage"></pager-custom> \
                </div>\
              </div> -->\
                   \
                   <!-- width: calc((20% - 0.8px)*2 + 1px);-->\
                \
          </div> \
          <div class="materialize-keypad col-1x5" style="height: 15%"> \
            <div class="row" style="margin-bottom: 0;"> \
              <div class="col s10-2 dummy-grid"><span></span></div> \
              <div class="col s10-2 dummy-grid"><span></span></div> \
              <div class="col s10-2 dummy-grid"><span></span></div> \
              <div class="col s10-2 dummy-grid"><span></span></div> \
              <div class="col s10-2"><button class="proceed btn md-button md-grid-button" ng-click="returnToOrder();">{{::ui[\'done\']}}</button></div> \
            </div><!-- row --> \
          </div> \
      </div><!-- option-control-menu -->');
  }]);

  angular.module("tmpl/main.html", []).run(["$templateCache", function ($templateCache) {
      $templateCache.put("tmpl/main.html",
        ' \
        <div class="menu-container md-custom-grid col-5x5 default" style="height: 100%;" ng-controller="mainMenuController as mainMenu"> \
          <div class="row"> \
            <!--  ng-repeat="(key, value) in mainMenu" --> \
            <div class="col s10-2" ng-repeat="menu in mainMenuList" ng-if="menu.label != \'main_page\'"> \
              <md-button md-no-ink="true" class="md-grid-button md-raised md-button" ng-click="switchMode2(menu.mode, menu.requiredLogin);">{{::ui[menu.label]}}</md-button> \
            </div> \
            <!--<div class="col s10-5"> \
              <md-button md-no-ink="true" class="md-raised" ng-click="switchMode2({mode: \'itemAdmin\'});">Item Admin</md-button> \
            </div>--> \
          </div> \
      ');
  }]);

  angular.module("tmpl/userScene.html", []).run(["$templateCache", function ($templateCache) {
      $templateCache.put("tmpl/userScene.html",
        '<div id="user-menu" class="menu-area" ng-controller="userCtrl as dummy"> \
          <!-- sidebar column --> \
                <table class="fix-head user-management-table data-table style1 tablesorter group-table userManagementTable" ng-show="currentMode == \'list\'"> \
                    <caption>員工列表 <a class="btn" ng-click="editUser(\'\')"><i class="md-icon" style="vertical-align: top;">note_add</i>{{::ui[\'add_user\']}}</a></caption><thead>\
                    <tr><th>員工編號</th><th>員工名稱</th><th>員工組別</th><th>&nbsp;</<th></tr></thead>\
                    <tbody><tr ng-repeat="staff in Staffs"><td>{{staff.username || \'&nbsp;\'}}</td><td>{{staff.name || \'&nbsp;\'}}</td><td>{{staff.staffGroupName || \'&nbsp;\'}}</td><td><a ng-click="editUser(\'{{staff.username}}\')"><i class="md-icon" style="vertical-align: top;">mode_edit</i>編輯</a></td></tr></tbody>\
                </table>\
                <div class="user-edit-container" ng-show="currentMode == \'user\'" style="width:100%">\
                    <div>{{ui[\'user_management\']}}</div>\
                    <md-input-container class="" flex="30"> \
                        <label ng-model="keypadInfo.itemName">{{ui[username] || "員工編號"}}</label> \
                        <input autofocus ng-model="currentUser.username" ng-class="{\'focused\' : currentFieldName == \'name\'}" ng-trim="false" type="text" ng-click="focusField(\'name\')"> \
                        </md-input-container> \
                        <md-input-container class="" flex="30"> \
                        <label>{{ui[usercode] || "員工名稱"}}</label> \
                        <input type="text" ng-model="currentUser.name" ng-class="{\'focused\' : currentFieldName == \'value\'}" ng-trim="false" ng-click="focusField(\'value\')"> \
                        </md-input-container> \
                        <md-input-container class="" flex="30"> \
                        <label>{{ui[usercode] || "員工密碼"}}</label> \
                        <input type="text" ng-model="currentUser.password" ng-class="{\'focused\' : currentFieldName == \'value\'}" ng-trim="false" ng-click="focusField(\'value\')"> \
                        </md-input-container> \
                        \
                        \
                        <!-- angular example -->\
                        <!-- <select ng-model="mySelectedValue" \
                          ng-options="myValue for myValue in someDynamicArrayOfValues" \
                          ng-change="myFunctionForDeterminingWhetherValueIsSelected()"> \
                          <option value="">--</option> \
                        </select> --> \
                        \
                        \
                        <md-input-container flex="30">\
                          <label>{{ui[\'staff_type\'] || "員工組別"}}</label>\
                          <md-select ng-model="currentUser.staffGroupCode">\
                            <md-option ng-repeat="type in staffTypeList" value="{{type.code}}">\
                              {{type.name}}\
                            </md-option>\
                          </md-select>\
                        </md-input-container>\
                        <!-- <div class="material-select input-field col s12">\
                            <select ng-model="currentUser.staffGroupCode">\
                              <option value="" disabled selected>{{ui[\'please_select\']}}</option>\
                              <option value="1">Option 1</option>\
                              <option value="2">Option 1</option>\
                              <option value="3">Option 1</option>\
                            </select>\
                            <label>{{ui[\'staff_type\'] || "員工組別"}}</label>\
                          </div> -->\
                        <!-- <md-input-container class="login" flex="30"> \
                        <label>{{ui[usercode] || "員工組別"}}</label> \
                        <input type="text" ng-model="currentUser.staffGroupName" ng-class="{\'focused\' : currentFieldName == \'value\'}" ng-trim="false" ng-click="focusField(\'value\')"> \
                        </md-input-container> --> \
                        <md-button md-no-ink="true" class="md-raised submit" ng-click="submitUser()">{{::ui["done"] || "完成"}}</md-button> \
          </div> \
      </div><!-- report-menu -->');
  }]);

  angular.module("tmpl/reportScene.html", []).run(["$templateCache", function($templateCache) {
    $templateCache.put("tmpl/reportScene.html", 
      '<div id="report-menu" class="menu-area" layout="row" ng-controller="reportController as report"> \
          <!-- sidebar column --> \
          \
            <div flex="45" class="report-content left-area" ng-hide="currentReport==\'\'"> \
              <!-- <div ng-show="currentReport==\'\'">{{::ui[\'please_select_report\']}}</div> --> \
              <ng-include include-replace src="\'tmpl/report_template_daily.html\'" ng-if="currentReport==\'daily_report\'"></ng-include> \
              <ng-include include-replace src="\'tmpl/report_template_sales.html\'" ng-if="currentReport==\'sales_report\'"></ng-include> \
            </div> \
            \
            <div id="report-option" flex class="right-area md-grid-container" style="position: relative; width: 100%; height: 100%;"> \
                <h2 class="header left-edge-margin fill-height ng-binding" style="height: 6%">{{::ui[\'report\']}}<div class="remark"><span ng-if="selectedDateRange.length > 1">{{::ui[\'selected_period\'] + ui[\'colon\']}}</span><span ng-if="selectedDateRange.length == 1">{{::ui[\'selected_day\'] + ui[\'colon\']}}</span>{{dateFrom}}<span ng-if="selectedDateRange.length > 1"> {{::ui[\'to\']}} </span>{{dateTo}}</div></h2> \
                <div class="materialize-keypad col-5x5" style="height: 82%"> \
                  <div class="row" style="margin-bottom: 0;"> \
                    <div class="col s10-2" ng-repeat="option in report.reportOption | startFrom:(currentPage - 1) * reportOptionPerPage | limitTo: reportOptionPerPage"><button class="btn md-button md-grid-button" ng-click="changeReport(\'{{::option.name}}\')">{{::ui[option.name]}}</button></div> \
                     \
                    <div class="col s10-2 dummy-grid" ng-if="noOfReportOptionPage - currentPage == 0" ng-repeat="i in MainService.getPageNumber( reportOptionPerPage - report.reportOption.length % reportOptionPerPage ) track by $index"><span></span></div> \
                    <div class="col s10-2 dummy-grid" ng-if="noOfReportOptionPage - currentPage == 0"><span></span></div> \
                    <div class="col s10-2 dummy-grid" ng-if="noOfReportOptionPage - currentPage == 0"><span></span></div> \
                    <!--<pager-custom icon-size="\'dp56\'" template="tmpl/pager-default-materialize.html" total-items="report.reportOption.length" items-per-page="reportOptionPerPage" ng-model="currentPage"></pager-custom>--> \
                  </div><!-- row --> \
                </div> \
                  <div class="materialize-keypad col-1x5" style="height: 12%"> \
                    <div class="row" style="margin-bottom: 0;"> \
                      <!-- <div class="col s10-2 dummy-grid"><span></span></div> --> \
                      <!-- <div class="col s10-3 dummy-grid"><span></span></div> --> \
                      <div class="col s10-2"><button class="btn md-button md-grid-button" ng-click="todayReport()">{{::ui[\'today_report\']}}</button></div> \
                      <div class="col s10-2"><button class="btn md-button md-grid-button" ng-click="callCalendar(dialogCtrl)">{{::ui[\'past_report\']}}</button></div> \
                      <div class="col s10-2"><button class="btn md-button md-grid-button" ng-click="printReport()">{{::ui[\'print_report\']}}</button></div> \
                      <div class="col s10-2"><button class="proceed2 btn md-button md-grid-button" ng-click="dailyClearance()">{{::ui[\'daily_clearance\']}}</button></div> \
                      <div class="col s10-2"><button class="proceed btn md-button md-grid-button" ng-click="MainService.mode = MainService.schema.mode.main">{{::ui[\'done\']}}</button></div> \
                    </div><!-- row --> \
                  </div> \
            </div> \
      </div><!-- report-menu -->');
  }]);

  angular.module("tmpl/calendar.html", []).run(["$templateCache", function ($templateCache) {
    $templateCache.put("tmpl/calendar.html",
      '<md-dialog aria-label="calendar" class="calendar"> \
            <md-dialog-content class="sticky-container"> \
                <div class="clearfix calendar-container" ng-class="{ \'extend-w\' : selectedDateRange.length > 0, \'extend-w\' : selectedDateRange.length > 0, \'extend-h\' : (ctrl.preDays.length + ctrl.days.length + ctrl.postDays.length) > 35}"> \
                  <calendar></calendar> \
                  <section class="date-range" ng-show="selectedDateRange.length > 0" ng-class="{\'show\' : selectedDateRange.length > 0}"> \
                      <h5 style="display: inline;"><span ng-if="selectedDateRange.length > 1">{{::ui[\'selected_period\']}}</span><span ng-show="selectedDateRange.length == 1">{{::ui[\'selected_day\']}}</</span></h5>\
                      <div class="date from flex-center"><span ng-show="selectedDateRange.length > 1">由：</span>{{dateFrom}}</div> \
                      <div class="date to flex-center" ng-show="selectedDateRange.length > 1"><span>至：</span>{{dateTo}}</div> \
                      <div class="materialize-keypad" style="height: 50px"> \
                        <div class="row" style="height: 100%"> \
                          <div class="col s10-10" style="height: 100%;"><button class="proceed btn md-button md-grid-button" ng-click="setReport()">{{::ui[\'done\']}}</button></div> \
                        </div><!-- row --> \
                      </div> \
                  </section> \
                </div> \
            </md-dialog-content> \
        </md-dialog>');
  }]);

  angular.module("tmpl/item-scene.html", []).run(["$templateCache", function ($templateCache) {
      $templateCache.put("tmpl/item-scene.html",'\
    <h4 id="page-title">餐品管理</h4>\
      <div id="item-scene-container" ng-controller="itemController as item" ng-init="load()" style="overflow-y:auto;overflow-x:auto;">\
        <div id="item-list-container" ng-show="true">\
        <ng-include include-replace src="\'../tmpl/EditItem.html\'"></ng-include> \
        </div>\
        <!-- 48px is tab heightin full version -->\
                <table id="FoodItem-list" class="tablesorter" data-sortlist="[[0,0]]" ng-show="itemList.length>0" style="margin-bottom:80px">\
                <thead>\
                <tr >\
                    <th style="text-align:center;"><button ng-click="AddItemPage($event);" style="color:black;">新增</button></th>\
                    <th>餐單</th>\
                    <th>食品編號</th>\
                    <th>plu</th>\
                    <th>中文名稱</th>\
                    <th>中文名稱2</th>\
                    <th>廚房名稱</th>\
                    <th>英文名稱</th>\
                    <th>價格</th>\
                    <th>類型</th>\
                    <th>項目顏色</th>\
                    <th>打印位置</th>\
                    <th>打印組別</th>\
                    <th>KDS</th>\
                    <th>收取服務費?</th>\
                    <th>有折扣?</th>\
                    <th>螢幕顯示?</th>\
                </tr>\
                </thead>\
                <tbody>\
                <tr ng-repeat="item in itemList" ng-click="ItemRowClick($event);" ng-style="{{item.rowStyle}}" rowCount="{{$index}}">\
                <td style="text-align:center"><button class="editMode" ng-click="EditFood($event)">修改</button></td>\
                <td class="category" category="{{item.categoryId}}">{{item.categoryname}}</td> \
                <td class="code">{{item.code}}</td> \
                <td class="plu">{{item.plu}}</td> \
                <td class="cname">{{item.name1}}</td> \
                <td class="cname2">{{item.name2}}</td> \
                <td class="kname">{{item.namek}}</td> \
                <td class="ename">{{item.name3}}</td> \
                <td class="price" price="{{item.price1}}">{{item.price1 | currency: INFO.currency : 1 }}</td> \
                <td class="type" type="{{item.type}}" > {{item.type}} </td> \
                <td class="color" color="{{item.color}}"><div class="color-box value {{item.color}}"></div></td> \
                <td class="printer" printer="{{item.printer}}">{{item.printer}}</td> \
                <td class="printerGroup" printerGroup="{{item.printerGroup}}">{{item.printerGroup}}</td> \
                <td class="kds" kds="{{item.kds}}">{{item.kds}}</td> \
                <td class="serviceCharge" serviceCharge="{{item.serviceCharge}}">{{item.serviceCharge===null || !item.serviceCharge?"否":"是"}}</td> \
                <td class="discount" discount="{{item.discount}}">{{item.discount == 1? "是" : "否"}}</td> \
                <td class="screenShow" screenShow="{{item.goMenu}}">{{item.goMenu?"是": "否"}}</td>\
                </tr>\
                </tbody>\
            </table>\
      </div>\
      ');
  }]);

  angular.module("tmpl/category-scene.html", []).run(["$templateCache", function ($templateCache) {
      $templateCache.put("tmpl/category-scene.html",'\
      <h4 id="page-title">餐品類別管理</h4>\
      <div id="category-scene-container" ng-controller="categoryController as category" ng-init="load()" style="overflow-y:auto;">\
        <div id="category-list-container">\
        <table ng-show="ShowBasic()" id="CategoryItem-list" style="margin-bottom:100px;"><thead>\
        <tr>\
        <th style="text-align:center;"><button ng-click="AddItemPage();" class="btn" >新增</button></th>\
        <th>中文名稱</th>\
        <th>中文名稱2</th>\
        <th>英文名稱</th>\
        <th>排序</th>\
        <th>ICON</th>\
        <th>顏色</th>\
        </tr>\
        </thead>\
            <tbody>\
            <tr ng-repeat="cate in categories" categoryId="{{cate.categoryId}}" \
            ng-click="ItemRowClick($event)" ng-style="{{cate.rowStyle}}" rowcount="{{$index}}">\
            <td style="text-align:center"><button class="editMode btn" ng-click="EditCategory($event, cate)">修改</button></td>\
            <td class="name1">{{cate.name1}}</td>\
            <td class="name3">{{cate.name3}}</td>\
            <td class="name2">{{cate.name2}}</td>\
            <td class="seq">{{cate.seq}}</td>\
            <td class="icon">{{cate.icon}}</td>\
            <td class="color" color="{{cate.color}}"><div class="color-box value {{cate.color}}"></div></td> \
            </tr>\
            </tbody>\
        <table>\
        </div>\
        <div id="category-details" class="form-container" style="overflow: auto; height: calc(100%);" ng-show="ShowEditPage()">\
            <ng-include include-replace src="\'../tmpl/EditCategory.html\'"></ng-include> \
        </div>\
      </div>\
      ');
  }]);

  angular.module("tmpl/menu-layout-list-scene.html",[]).run(["$templateCache", function($templateCache){
      $templateCache.put("tmpl/menu-layout-list-scene.html", '\
        <h4 id="page-title">餐牌顯示</h4>\
        <div class="row">\
            <div class="col s4">\
                <button class="btn waves-effect waves-light" ng-click="deploy()">Deploy</button>\
                <button class="btn waves-effect waves-light" ng-click="" style="display: none;">template</button>\
                <button class="btn waves-effect waves-light" ng-click="addLayout()">新增</button>\
            </div>\
        </div>\
        <div style="height: 100%;">\
            <div layout="row" style="height: inherit;"> \
                <div flex="" layout="column" class="menu-layout-builder-viewport" style="overflow-x:auto; margin-bottom:70px;">\
                    <table class="compact data-table style1 tablesorter item-table" style="margin-bottom:70px;">\
                        <thead>\
                            <tr>\
                                <th data-field="layoutId">#</th>\
                                <th data-field="layoutName">名稱</th>\
                                <th data-field="panel">面板</th>\
                                <th data-field="dayOfWeek">星期幾?</th>\
                                <th data-field="priority">優先</th>\
                                <th data-field="activateDate">啟用日期</th>\
                                <th data-field="expireDate">到期日</th>\
                                <th data-field="staffId">修訂</th>\
                                <th data-field="lastUpdateDate">更新</th>\
                                <th></th>\
                            </tr>\
                        </thead>\
                        <tbody>\
                            <tr ng-repeat="menuLayout in menuLayoutList" ng-model="menuLayout">\
                                <td>{{menuLayout.layoutId}}</td>\
                                <td>{{menuLayout.layoutName}}</td>\
                                <td>{{menuLayout.panel}}</td>\
                                <td>{{menuLayout.dayOfWeek}}</td>\
                                <td>{{menuLayout.priority}}</td>\
                                <td>{{menuLayout.activateDate}}</td>\
                                <td>{{menuLayout.expireDate}}</td>\
                                <td>{{menuLayout.staffId}}</td>\
                                <td>{{menuLayout.lastUpdateDate}}</td>\
                                <td style="text-align:center;">\
                                    <button class="btn waves-effect waves-light" ng-click="editLayout(menuLayout.layoutId)"><i class="md-icon">edit</i></button>\
                                    <button class="btn waves-effect waves-light light-green black-text" ng-click="editLayout(menuLayout.layoutId, true)"><i class="md-icon">filter_none</i></button>\
                                    <!--<button class="btn waves-effect waves-light red" ng-click="removeLayout(menuLayout.layoutId)"><i class="md-icon">delete</i></button>-->\
                                </td>\
                            </tr>\
                        </tbody>\
                    </table>\
                </div>\
            </div>\
        </div>');
  }]);

  angular.module("tmpl/menu-layout-builder-scene.html",[]).run(["$templateCache", function($templateCache){
      $templateCache.put("tmpl/menu-layout-builder-scene.html", '\
        <div class="panel" layout="row">\
            <div flex="10" class="menu-layout-builder-side-bar menu-layout-builder-left-side-bar">\
                <div class="row">\
                    <div class="col s12">\
                        <!--<a class="btn-floating waves-effect waves-teal" ng-click="addTileItem()"><i class="md-icon">picture_in_picture</i></a>-->\
                        <button class="btn-flat waves-effect waves-light" ng-click="addTileItem()"><i class="md-icon left">add</i>Tile</button>\
                    </div>\
                    <div class="col s12" ng-show="selectedFlow.hasNavBar">\
                        <!--<a class="btn-floating waves-effect waves-teal" ng-click="addNavItem()"><i class="md-icon">view_stream</i></a>-->\
                        <button class="btn-flat waves-effect waves-light" ng-click="addNavItem()"><i class="md-icon left">add</i>Nav</button>\
                    </div>\
                </div>\
                <hr/>\
                <div class="row" ng-show="selectedItem">\
                    <div class="col s12">\
                        <button class="btn-flat waves-effect waves-teal" ng-click="transformZ(\'backward\')">Send Backward</button>\
                    </div>\
                    <div class="col s12">\
                        <button class="btn-flat waves-effect waves-teal" ng-click="transformZ(\'back\')">Send to Back</button>\
                    </div>\
                    <div class="col s12">\
                        <button class="btn-flat waves-effect waves-teal" ng-click="transformZ(\'forward\')">Bring Forward</button>\
                    </div>\
                    <div class="col s12">\
                        <button class="btn-flat waves-effect waves-teal" ng-click="transformZ(\'front\')">Bring to Front</button>\
                    </div>\
                </div>\
            </div>\
            <div flex="70" class="menu-layout-builder-viewport">\
                <div class="menu-layout-container" ng-style="{width: menuLayout.width, height: menuLayout.height, backgroundColor: menuLayout.backgroundColor, color: menuLayout.fontColor}">\
                    <div class="nav-bar-container tile-group highlight inner-border" ng-show="selectedFlow.hasNavBar && selectedFlow.navigationBar"\
                            ng-style="{width: selectedFlow.navigationBar.width, height: menuLayout.height}">\
                        <div class="nav-bar-button tile-item highlight inner-border" ng-repeat="button in selectedFlow.navigationBar.layout.buttons track by $id(button)" ng-show="selectedFlow.hasNavBar" ng-model="::button" menu-layout link-data="::button" link-index="$index" link-type="\'tile\'">\
                        </div>\
                    </div>\
                    <div class="page-container tile-group highlight inner-border"\
                            ng-style="{width: menuLayout.width - (selectedFlow.hasNavBar ? selectedFlow.navigationBar.width : 0), height: menuLayout.height}">\
                        <div class="page-button tile-item highlight inner-border" ng-repeat="button in selectedLayout track by $id(button)" ng-hide="button.isDelete" ng-model="button" menu-layout link-data="button" link-index="$index" link-type="\'tile\'">\
                        </div>\
                        <div class="category-page" ng-show="selectedCategory">\
                            <p class="md-display-3">Category</p>\
                            <p class="md-display-2">{{selectedCategory}}</p>\
                        </div>\
                        <button class="btn nav-back-btn" ng-show="parentLayout.length" ng-click="back()">Back</button>\
                    </div>\
                    <div id="settings-panel" class="hoverable white black-text tile-item-panel">\
                        <form class="row">\
                            <div id="settings-panel-handle" class="row blue-grey lighten-5 z-depth-1 hoverable tile-item-panel-handle">\
                                <div class="col right">\
                                    <a class="close-btn icon-btn btn red"><i class="md-icon">close</i></a>\
                                </div>\
                                <div class="col right">\
                                    <a class="save-btn icon-btn btn blue"><i class="md-icon">save</i></a>\
                                </div>\
                            </div>\
                            <div class="row">\
                                <div class="input-field col s3">\
                                    <input id="item_x" type="number" name="x" class="validate" placeholder="X coord">\
                                    <label for="item_x">X</label>\
                                </div>\
                                <div class="input-field col s3">\
                                    <input id="item_y" type="number" name="y" class="validate" placeholder="Y coord">\
                                    <label for="item_y">Y</label>\
                                </div>\
                                <div class="input-field col s3">\
                                    <input id="item_width" type="number" name="width" class="validate">\
                                    <label for="item_width">Width</label>\
                                </div>\
                                <div class="input-field col s3">\
                                    <input id="item_height" type="number" name="height" class="validate">\
                                    <label for="item_height">Height</label>\
                                </div>\
                            </div>\
                            <div class="row layout-settings">\
                                <div class="col s12">\
                                    <label>Content Type</label>\
                                    <select class="browser-default content-type" name="type">\
                                        <option value="" disabled selected>Choose a content type</option>\
                                        <option value="item">Item</option>\
                                        <option value="image">Image</option>\
                                        <option value="text">Text</option>\
                                        <option value="pickup">PickUp Number Display</option>\
                                        <!--<option value="slideshow">Slide Show</option>-->\
                                    </select>\
                                </div>\
                            </div>\
                            <div class="row template-layout-settings content-input content-input-text">\
                                <div class="input-field col s3">\
                                    <label class="active" for="font_color">Font Color</label>\
                                    <input id="font_color" type="color" name="fontColor" class="validate">\
                                </div>\
                                <div class="input-field col s3">\
                                    <label for="font_size">Font Size</label>\
                                    <input id="font_size" type="number" name="fontSize" class="validate">\
                                </div>\
                                <div class="col s3">\
                                    <label for="font_style">Font Style</label>\
                                    <select id="font_style" name="fontStyle" class="browser-default">\
                                        <option value="" selected>No Style</option>\
                                        <option value="bold">bold</option>\
                                        <option value="italic">italic</option>\
                                        <option value="bold|italic">bold|italic</option>\
                                    </select>\
                                </div>\
                                <div class="col s3">\
                                    <label for="text_align">Text Align</label>\
                                    <select id="text_align" name="textAlign" class="browser-default">\
                                        <option value="" selected>default</option>\
                                        <option value="left">left</option>\
                                        <option value="center">center</option>\
                                        <option value="right">right</option>\
                                    </select>\
                                </div>\
                            </div>\
                            <div class="row template-layout-settings content-input content-input-text">\
                                <div class="input-field col s6">\
                                    <label for="lines">Number of Lines</label>\
                                    <input id="lines" type="number" name="lines" class="validate">\
                                </div>\
                                <div class="col s6">\
                                    <label for="font_family">Font Family</label>\
                                    <select id="font_family" name="fontFamily" class="browser-default">\
                                        <option value="" selected>system</option>\
                                        <option ng-repeat="f in fontFamily" value="{{f.key}}">{{f.text}}</option>\
                                    </select>\
                                </div>\
                                <div class="input-field col s12 text-string">\
                                    <label for="string">String</label>\
                                    <input id="string" type="text" name="string">\
                                </div>\
                            </div>\
                            <div class="row layout-settings item-selector content-input content-input-item">\
                                <div class="col s6">\
                                    <label>Category</label>\
                                    <select class="food-category browser-default"></select>\
                                </div>\
                                <div class="col s6">\
                                    <label>Item</label>\
                                    <select class="food-item browser-default">\
                                        <option value="" disabled selected>Choose any category</option>\
                                    </select>\
                                </div>\
                            </div>\
                            <div class="row layout-settings item-selector content-input content-input-item">\
                                <div class="col s12">\
                                    <label>Template</label>\
                                    <select class="template-list browser-default"></select>\
                                </div>\
                            </div>\
                            <div class="row layout-settings item-selector content-input content-input-item">\
                                <div class="col s6">\
                                    <button class="btn template-apply-btn" type="button">Apply Template</button>\
                                </div>\
                            </div>\
                            <div class="row layout-settings image-selector content-input content-input-image">\
                                <div class="col s12">\
                                    <label for="item_image">Image Path</label>\
                                    <input id="item_image" type="text" class="image-path s12" style="width: 100%" />\
                                </div>\
                            </div>\
                            <div class="row layout-settings item-selector content-input content-input-image">\
                                <div class="col s6">\
                                    <button class="btn fit-image-size-btn" type="button">Fit image size</button>\
                                </div>\
                            </div>\
                            <div class="row layout-settings text-input content-input content-input-text">\
                                <div class="input-field col s8">\
                                    <textarea id="pickup_title" name="text" class="text s12 materialize-textarea" style="width: 100%"></textarea>\
                                    <!--<input id="item_text" type="text" name="text" class="text s12" style="width: 100%" />-->\
                                    <label for="item_text">Text</label>\
                                </div>\
                                <!--<div class="input-field col s4">\
                                    <input id="item_text_font_size" type="number" name="fontSize" class="text-font-size s12" style="width: 100%" />\
                                    <label for="item_text_font_size">Font Size</label>\
                                </div>-->\
                            </div>\
                            <div class="row layout-settings text-input content-input content-input-pickup">\
                                <div class="col s12">\
                                    <h6>Queue Number</h6>\
                                </div>\
                                <div class="input-field col s4">\
                                    <label class="active" for="pickup_queue_font_color">Font Color</label>\
                                    <input id="pickup_queue_font_color" type="color" name="queueFontColor" class="validate">\
                                </div>\
                                <div class="input-field col s4">\
                                    <input id="pickup_queue_font_size" type="number" name="queueFontSize" class="queue-font-size s12" style="width: 100%" />\
                                    <label for="pickup_queue_font_size">Font size</label>\
                                </div>\
                                <div class="input-field col s4">\
                                    <input id="pickup_number_length" type="number" name="numberLength" class="number-length s12" style="width: 100%" />\
                                    <label for="pickup_number_length">Length</label>\
                                </div>\
                                <div class="input-field col s3">\
                                    <input id="pickup_row" type="number" name="row" class="limit-of-queue s12" style="width: 100%" />\
                                    <label for="pickup_row">Row</label>\
                                </div>\
                                <div class="input-field col s3">\
                                    <input id="pickup_col" type="number" name="col" class="limit-of-queue s12" style="width: 100%" />\
                                    <label for="pickup_col">Columns</label>\
                                </div>\
                                <div class="col s6">\
                                    <label>Direction</label>\
                                    <select class="browser-default" name="direction">\
                                        <option value="ttb">Top to Bottom</option>\
                                        <option value="ltr">Left to Right</option>\
                                    </select>\
                                </div>\
                                <div class="col s12">\
                                    <label>Call Number</label>\
                                    <select class="browser-default" name="callNumber">\
                                        <option value="false">No</option>\
                                        <option value="true">Yes</option>\
                                    </select>\
                                </div>\
                                <div class="col s12">\
                                    <h6>Title</h6>\
                                </div>\
                                <div class="input-field col s6">\
                                    <label class="active" for="pickup_title_font_color">Font Color</label>\
                                    <input id="pickup_title_font_color" type="color" name="titleFontColor" class="validate">\
                                </div>\
                                <div class="input-field col s6">\
                                    <input id="pickup_title_font_size" type="number" name="titleFontSize" class="title-font-size s12" style="width: 100%" />\
                                    <label for="pickup_title_font_size">Font size</label>\
                                </div>\
                                <div class="input-field col s12">\
                                    <textarea id="pickup_title" name="title" class="text s12 materialize-textarea" style="width: 100%"></textarea>\
                                    <!--<input id="pickup_title" type="text" name="title" class="text s12" style="width: 100%" />-->\
                                    <label for="pickup_title">Title</label>\
                                </div>\
                            </div>\
                        </form>\
                    </div>\
                    <div id="navigation-panel" class="z-depth-5 white black-text tile-item-panel">\
                        <form class="row">\
                            <div id="navigation-panel-handle" class="row blue-grey lighten-5 z-depth-1 hoverable tile-item-panel-handle">\
                                <div class="col right">\
                                    <a class="close-btn icon-btn btn red"><i class="md-icon">close</i></a>\
                                </div>\
                            </div>\
                            <div class="row layout-settings">\
                                <div class="col s12">\
                                    <label>Navigation Type</label>\
                                    <select class="browser-default nav-type" name="nav">\
                                        <option value="" disabled selected>Choose a navigation type</option>\
                                        <option value="link">Link</option>\
                                        <option value="category">Category</option>\
                                    </select>\
                                </div>\
                            </div>\
                            <div class="row page-nav page-nav-link">\
                                <div class="input-field col s12">\
                                    <input id="page_nav_link_edit" type="text" name="link" class="link" />\
                                    <label for="page_nav_link_edit">Link</label>\
                                </div>\
                            </div>\
                            <div class="row page-nav page-nav-link">\
                                <div class="col s12">\
                                    <button class="btn page-nav-link-create-btn" type="button">Create</button>\
                                    <button class="btn page-nav-link-update-btn" type="button">Update</button>\
                                    <button class="btn page-nav-link-unlink-btn red" type="button">Unlink</button>\
                                </div>\
                            </div>\
                            <div class="row page-nav page-nav-next-layout">\
                                <div class="col s12">\
                                    <h5>Next Page</h5>\
                                </div>\
                                <div class="col s12">\
                                    <button class="btn page-nav-next-layout-create-btn" type="button">Create</button>\
                                    <button class="btn page-nav-next-layout-delete-btn red" type="button">Delete</button>\
                                </div>\
                            </div>\
                            <div class="row page-nav page-nav-category">\
                                <div class="col s12">\
                                    <label>Category</label>\
                                    <input class="page-nav-category-select" name="category" style="width: 100%" multiple />\
                                </div>\
                            </div>\
                            <div class="row page-nav page-nav-category">\
                                <div class="input-field col s12">\
                                    <input id="page_nav_category_filter_key" type="text" name="filterKey" class="filter-key" />\
                                    <label for="page_nav_category_filter_key">Filter Key</label>\
                                </div>\
                                <div class="col s6">\
                                    <label>Disable Others</label>\
                                    <select class="browser-default" name="disableOthers">\
                                        <option value="false">No</option>\
                                        <option value="true">Yes</option>\
                                    </select>\
                                </div>\
                            </div>\
                            <div class="row page-nav page-nav-category">\
                                <div class="col s12">\
                                    <button class="btn page-nav-category-link-btn" type="button">Set</button>\
                                    <button class="btn page-nav-category-clear-btn red" type="button">Clear</button>\
                                </div>\
                            </div>\
                        </form>\
                    </div>\
                </div>\
            </div>\
            <div flex="20" class="menu-layout-builder-side-bar menu-layout-builder-right-side-bar">\
                <div flex class="menu-layout-side-bar-form">\
                    <div class="row">\
                        <div class="col s12">\
                            <ul class="tabs">\
                                <li class="tab col s6"><a class="active" href="#tab-setting">Settings</a></li>\
                                <li class="tab col s6"><a href="#tab-layout">Layout</a></li>\
                            </ul>\
                        </div>\
                        <div id="tab-setting" class="col s12">\
                            <form class="menu-layout-detail-form">\
                                <div class="row">\
                                    <div class="input-field col s6">\
                                        <label for="layoutName">Layout Name</label>\
                                        <input type="text" id="layoutName" placeholder="Layout Name" ng-model="menuLayout.layoutName" />\
                                    </div>\
                                    <div class="input-field col s6">\
                                        <label for="panel">Panel</label>\
                                        <input type="text" id="panel" placeholder="Leave blank for goMenu" ng-model="menuLayout.panel" />\
                                    </div>\
                                    <div class="input-field col s12">\
                                        <label for="activateDate">Activate Date</label>\
                                        <input type="date" id="activateDate" placeholder="Activate Date" ng-model="menuLayout.activateDate" />\
                                    </div>\
                                    <div class="input-field col s12">\
                                        <label for="expireDate">Expire Date</label>\
                                        <input type="date" id="expireDate" placeholder="Expire Date" ng-model="menuLayout.expireDate" />\
                                    </div>\
                                    <div class="input-field col s8">\
                                        <select ng-model="menuLayout.dayOfWeek" menu-layout link-type="\'select\'" multiple>\
                                            <option value="" disabled>Choose a day</option>\
                                            <option value="1">Sun</option>\
                                            <option value="2">Mon</option>\
                                            <option value="3">Tue</option>\
                                            <option value="4">Wed</option>\
                                            <option value="5">Thu</option>\
                                            <option value="6">Fri</option>\
                                            <option value="7">Sat</option>\
                                            <option value="excluding">Excluding</option>\
                                        </select>\
                                        <label>Day Of Week</label>\
                                    </div>\
                                    <div class="input-field col s4">\
                                        <label for="priority">Priority</label>\
                                        <input type="text" id="priority" placeholder="Priority of this" ng-model="menuLayout.priority" />\
                                    </div>\
                                    <div class="input-field col s6">\
                                        <label class="active" for="bgcolor">Background Color</label>\
                                        <input type="color" id="bgcolor" placeholder="color" ng-model="menuLayout.backgroundColor" menu-layout link-type="\'color\'" />\
                                    </div>\
                                    <div class="input-field col s6">\
                                        <label class="active" for="fontcolor">Font Color</label>\
                                        <input type="color" id="fontcolor" placeholder="color" ng-model="menuLayout.fontColor" menu-layout link-type="\'color\'" />\
                                    </div>\
                                    <div class="input-field col s6">\
                                        <select ng-model="menuLayout.orientation" ng-change="changeOrientation()" menu-layout link-type="\'select\'">\
                                            <option value="portrait">portrait</option>\
                                            <option value="landscape">landscape</option>\
                                        </select>\
                                        <label>Orientation</label>\
                                    </div>\
                                    <div class="input-field col s6">\
                                        <select ng-model="menuLayout.resId" ng-change="changeResolution()" menu-layout link-type="\'select\'">\
                                            <option value="{{resolution.resId}}" ng-repeat="resolution in resolutionList">{{resolution.name}}</option>\
                                        </select>\
                                        <label>Resolution</label>\
                                    </div>\
                                </div>\
                                <div class="row" ng-show="menuLayout.resId == 0">\
                                    <div class="input-field col s6">\
                                        <label class="active" for="layoutWidth">Width</label>\
                                        <input type="number" id="layoutWidth" placeholder="Layout Width" ng-model="menuLayout.width" />\
                                    </div>\
                                    <div class="input-field col s6">\
                                        <label class="active" for="layoutHeight">Height</label>\
                                        <input type="number" id="layoutHeight" placeholder="Layout Height" ng-model="menuLayout.height" />\
                                    </div></div>\
                            </form>\
                        </div>\
                        <div id="tab-layout" class="col s12">\
                            <!--<h1 class="md-title">layout</h1>-->\
                            <div class="row">\
                                <!--<div class="col s4">\
                                    <h5>Flow</h5>\
                                </div>-->\
                                <div class="col s12 align-center">\
                                    <button class="btn waves-effect waves-light" ng-click="addFlow()">\
                                        <i class="md-icon left">add</i>flow\
                                    </button>\
                                </div>\
                            </div>\
                            <div ng-repeat="flow in menuLayout.flow track by $index" ng-class="{\'yellow lighten-1\': flow == selectedFlow}">\
                                <div class="row">\
                                    <div class="input-field col s6"><input id="flow_{{$index}}" type="text" ng-model="flow.label" /><label class="active" for="flow_{{$index}}" ng-class="{\'black-text\': flow == selectedFlow}">Flow</label></div>\
                                    <div class="col s2"><button class="btn icon-btn" ng-click="editFlow(flow)"><i class="md-icon">edit</i></button></div>\
                                    <div class="col s2"><button class="btn icon-btn light-green black-text" ng-click="copyFlow(flow)"><i class="md-icon">filter_none</i></button></div>\
                                    <div class="col s2"><button class="btn icon-btn red" ng-click="removeFlow($index)"><i class="md-icon">delete</i></button></div>\
                                </div>\
                                <div class="row" ng-show="flow == selectedFlow && flow.pages.length > 1" ng-repeat="page in flow.pages track by $index" ng-class="{\'light-blue lighten-2\': page == selectedPage}">\
                                    <div class="input-field col s5"><input id="page_{{$index}}" type="text" ng-model="page.tag" /><label class="active" for="page_{{$index}}" ng-class="{\'black-text\': page == selectedPage}">Page</label></div>\
                                    <div class="col s1">({{page.layout.length}})</div>\
                                    <div class="col s2"><button class="btn icon-btn" ng-click="editPage(page)"><i class="md-icon">edit</i></button></div>\
                                    <div class="col s2"><button class="btn icon-btn light-green black-text" ng-click="copyPage(page)"><i class="md-icon">filter_none</i></button></div>\
                                    <div class="col s2"><button class="btn icon-btn red" ng-click="removePage($index)"><i class="md-icon">delete</i></button></div>\
                                </div>\
                            </div>\
                            <div class="row" ng-show="selectedFlow">\
                                <div class="row">\
                                    <div class="col s12">\
                                        <label>Time Range</label>\
                                        <select class="browser-default" ng-model="selectedFlow.timeRangeId" menu-layout link-type="\'select\'">\
                                            <option value="{{timeRange.timeRangeId}}" ng-repeat="timeRange in timeRangeList">{{timeRange.name}} {{timeRange.timeStart}}-{{timeRange.timeEnd}}</option>\
                                        </select>\
                                    </div>\
                                    <div class="input-field col s12">\
                                        <label>Duration (0 for disable swap)</label>\
                                        <input type="number" placeholder="0 for disable swap" ng-model="selectedFlow.duration">\
                                    </div>\
                                    <div class="input-field col s12">\
                                        <label>Sequence</label>\
                                        <input type="number" placeholder="" ng-model="selectedFlow.seq">\
                                    </div>\
                                </div>\
                                <div class="row">\
                                    <p>\
                                        <input type="checkbox" id="filled-in-box{{$index}}" ng-model="selectedFlow.hasNavBar" />\
                                        <label for="filled-in-box{{$index}}">Navigation Bar</label>\
                                    </p>\
                                </div>\
                                <div class="row">\
                                    <div class="input-field col s12">\
                                        <label>Navigation Bar Width</label>\
                                        <input type="number" placeholder="Navigation Bar Width" ng-model="selectedFlow.navigationBar.width">\
                                    </div>\
                                </div>\
                            </div>\
                        </div>\
                    </div>\
                </div>\
            </div>\
        </div>');
  }]);

  angular.module("tmpl/menu-layout-template-scene.html",[]).run(["$templateCache", function ($templateCache) {
      $templateCache.put("tmpl/menu-layout-template-scene.html",'\
      <h4 id="page-title">Menu Tile Template</h4>\
      <div id="time-range-scene-container"  ng-init="load()" style="overflow-y:auto;height:100%">\
        <table id="timeRange-list" class="compact data-table style1 tablesorter item-table" ng-show="showBasic();" style="margin-bottom:100px;"><thead>\
        <tr>\
        <th style="text-align:center;"><button ng-click="AddTimeRangePage();" class="btn">新增</button></th>\
        <th>時段名稱</th>\
        <th>開始時間</th>\
        <th>結束時間</th>\
        <th>日期</th>\
        </tr>\
        </thead>\
            <tbody>\
            <tr ng-repeat="timeRange in timeRangeList" \
            ng-click="ItemRowClick($event)" rowcount="{{$index}}"  ng-style="{{timeRange.rowStyle}}">\
            <td style="text-align:center"><button class="editMode btn" ng-click="EditTimeRange($event, timeRange)">修改</button></td>\
            <td class="name2">{{timeRange.name}}</td>\
            <td class="name1">{{timeRange.timeStart | date: \'HH:mm:ss\'}}</td>\
            <td class="name3">{{timeRange.timeEnd | date: \'HH:mm:ss\'}}</td>\
            <td class="seq">{{timeRange.date}}</td>\
            </tr>\
            </tbody>\
        <table>\
        <div id="timeRange-details" class="form-container" style="overflow: auto; height: calc(100%);" ng-show="showEdit();">\
            <ng-include include-replace src="\'../tmpl/EditTimeRange.html\'"></ng-include> \
        </div>\
      </div>\
      ');
  }]);
  //ng-controller="timeRangeController as timeRange"
  angular.module("tmpl/time-range-scene.html", []).run(["$templateCache", function ($templateCache) {
      $templateCache.put("tmpl/time-range-scene.html",'\
      <h4 id="page-title">時段設定</h4>\
      <div id="time-range-scene-container"  ng-init="load()" style="overflow-y:auto;height:100%">\
        <table id="timeRange-list" class="compact data-table style1 tablesorter item-table" ng-show="showBasic();" style="margin-bottom:100px;"><thead>\
        <tr>\
        <th style="text-align:center;"><button ng-click="AddTimeRangePage();" class="btn">新增</button></th>\
        <th>時段名稱</th>\
        <th>開始時間</th>\
        <th>結束時間</th>\
        <th>日期</th>\
        </tr>\
        </thead>\
            <tbody>\
            <tr ng-repeat="timeRange in timeRangeList" \
            ng-click="ItemRowClick($event)" rowcount="{{$index}}"  ng-style="{{timeRange.rowStyle}}">\
            <td style="text-align:center"><button class="editMode btn" ng-click="EditTimeRange($event, timeRange)">修改</button></td>\
            <td class="name2">{{timeRange.name}}</td>\
            <td class="name1">{{timeRange.timeStart | date: \'HH:mm:ss\'}}</td>\
            <td class="name3">{{timeRange.timeEnd | date: \'HH:mm:ss\'}}</td>\
            <td class="seq">{{timeRange.date}}</td>\
            </tr>\
            </tbody>\
        <table>\
        <div id="timeRange-details" class="form-container" style="overflow: auto; height: calc(100%);" ng-show="showEdit();">\
            <ng-include include-replace src="\'../tmpl/EditTimeRange.html\'"></ng-include> \
        </div>\
      </div>\
      ');
  }]);

})();