(function () {
    var webOn = {};
    webOn.RARE = {
        version: 0.12, // where 12 > 8 based on months in 2015
        update: 20151207
    }
    
    var app = angular.module('rms', ['ngTouch', 'ngMessages', 'ngMaterial', 'ui.bootstrap', 'rms.controllers', 'rms.directives', 'common.services']).run(function (MainService) {
        MainService.promise;
    });
    app.config(function ($mdThemingProvider, $locationProvider, $provide, $httpProvider) {
        $locationProvider.html5Mode({enabled : true, requireBase: false, rewriteLinks: true});
        $provide.constant('INFO', INFO);
        $provide.constant('uiLanguage', language);
        $provide.constant('keyMap', keyMap);
        $provide.constant('userConfig', userConfig);
        $httpProvider.useApplyAsync( true ); // put all http request into one digest cycle for speed optimization
    });

    app.filter('startFrom', function () {
        return function (input, start) {
            // console.log('starting');
            // console.log(input);
            // console.log(start);
            start = +start; //parse to int
            if (input == undefined) return 0;
            return input.slice(start);
        }
    })
    //take all whitespace out of string
    .filter('nospace', function () {
      return function (value) {
        return (!value) ? '' : value.replace(/ /g, '');
      };
    })
    //replace uppercase to regular case
    .filter('humanizeDoc', function () {
      return function (doc) {
        if (!doc) return;
        if (doc.type === 'directive') {
          return doc.name.replace(/([A-Z])/g, function ($1) {
            return '-' + $1.toLowerCase();
          });
        }
        
        return doc.label || doc.name;
      };
     });

    app.factory('SocketFactory', function ($rootScope, $timeout, INFO) {
        var socketConnection = 'http://' + location.hostname + ':' + INFO.socketPort;
        var socket = io.connect(socketConnection);
        io.Manager(socketConnection, { reconnect: true });
        var asyncAngularify = function (socket, callback) {
            return callback ? function () {
                var args = arguments;
                $timeout(function () {
                    callback.apply(socket, args);
                }, 0);
            } : angular.noop;
        };
        return {
            on: function (eventName, callback) {
                //console.log(eventName);
                socket.on(eventName, function () {
                    var args = arguments;
                    $rootScope.$apply(function () {
                        callback.apply(socket, args);
                    });
                });
                /*
                socket.io.on("connect_error", function (e) {
                    console.log('socket on connect_error');
                })
                socket.on('connect_failed', function (e) {
                    console.log('socket on connect_failed');
                    console.log(e);
                });
                socket.on('disconnect', function (e) {
                    console.log('socket on disconnect');
                    console.log(e);
                });
                socket.on('error', function (e) {
                    console.log('socket on error');
                    console.log(e);
                });
                */
            },
            emit: function (eventName, data, callback) {
                //console.log(eventName);
                var lastIndex = arguments.length - 1;
                var callback = arguments[lastIndex];
                if (typeof callback == 'function') {
                    callback = asyncAngularify(socket, callback);
                    arguments[lastIndex] = callback;
                }
                return socket.emit.apply(socket, arguments);
            }
        };
    });

    app.factory('MainService', function (SocketFactory, INFO, $http, $q, $mdDialog, $timeout, uiLanguage, userConfig, $rootScope) {
        SocketFactory.on('refreshItemQty', function (data) {
            console.log('refreshItemQty done');
            console.log(data);
            obj.stock.updateItem(JSON.parse(data));
            //initTableScene();
            // showMessage("#status-message", scope.RMSTable.messages.table_refresh, "", "warn", 30000);
        });

        var charArray = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".split('');
        var obj = {};
        obj.schema = {};
        obj.schema.mode = {};
        obj.schema.mode.floorPlan = 0;
        obj.schema.mode.order = 1;
        obj.schema.mode.main = 2;
        obj.schema.mode.report = 3;
        obj.schema.mode.login = 4;
        obj.schema.mode.userManagement = 5;
        obj.schema.mode.dailyClearance = 6;
        obj.schema.mode.cashManagement = 7;
        obj.schema.mode.userGroupManagement = 8;
        obj.schema.mode.userClockManagement = 9;
        obj.schema.mode.tableList = 10;
        obj.schema.mode.settings = 11;
        obj.schema.mode.itemAdmin = 30;
        obj.schema.mode.categoryAdmin = 31;
        obj.schema.mode.modifierAdmin = 32;
        obj.schema.mode.optionAdmin = 33;
        obj.schema.mode.subitemAdmin = 34;
        obj.schema.mode.priceAdmin = 35;
        obj.schema.mode.menuLayoutList = 50;
        obj.schema.mode.menuLayoutBuilder = 51;
        obj.schema.mode.timeRangeSetting = 52;

        obj.schema.modeMain = {};
        obj.schema.modeMain.normal = 0;
        obj.schema.modeMain.cashbox = 0;
        obj.schema.modeUserGroup = {};
        obj.schema.modeUserGroup.list = 0;
        obj.schema.modeUserGroup.detail = 1;
        obj.schema.modeCash = {};
        obj.schema.modeCash.normal = 0;
        obj.schema.modeCash.open = 1;
        obj.schema.modeCash.petty = 2;

        // floor plan function mode
        obj.schema.modeFloorPlan = {};
        obj.schema.modeFloorPlan.order = 0;
        obj.schema.modeFloorPlan.printOrder = 1;
        obj.schema.modeFloorPlan.changeTable = 2;
        obj.schema.modeFloorPlan.printBill = 3;
        obj.schema.modeFloorPlan.split = 4;
        // obj.schema.modeFloorPlan.discount = 5;
        obj.schema.modeFloorPlan.billing = 10;
        obj.schema.modeFloorPlan.kitchenMessage = 11;
        obj.schema.modeFloorPlan.editTable = 12;

        // top menu
        obj.schema.modeNavMenu = {};
        obj.schema.modeNavMenu.normal = 0;
        obj.schema.modeNavMenu.editTable = 1;

        // order item, option, menu mode
        obj.schema.modeItem = {};
        obj.schema.modeItem.normal = 0;
        obj.schema.modeItem.option = 1;
        obj.schema.modeItem.cancel = 2;
        obj.schema.modeItem.modifier = 3;
        obj.schema.modeItem.optionFoodControl = 4;
        obj.schema.modeItemDetail = {};
        obj.schema.modeItemDetail.code = 0;
        obj.schema.modeItemDetail.printer = 1;
        obj.schema.modeOption = {};
        obj.schema.modeOption.normal = 0;
        obj.schema.modeOption.cancel = 1;
        obj.schema.modeOrder = {};
        obj.schema.modeOrder.normal = 0;
        obj.schema.modeOrder.detail = 1; // deprecated => modeItemListDisplay
        obj.schema.modeOrder.foodControl = 2;
        obj.schema.modeOrder.transfer = 3;
        obj.schema.modeOrder.billing = 5;
        obj.schema.modeOrder.billingSummary = 51;
        obj.schema.modeOrder.searchBill = 6;
        obj.schema.modeOrder.amendBill = 7;
        obj.schema.modeOrder.amendBillSummary = 71; // sumamry after amending a bill
        obj.schema.modeOrder.printBill = 8; // for printing bill within order mode
        obj.schema.modeOrder.split = 9;
        obj.schema.modeOrder.specialMessage = 10;
        obj.schema.modeOrder.zeroItem = 11; // deprecated => modeItemListDisplay

        // because in order mode( right hand side menu can be food order or bill search/print bill...etc, menu is different)
        obj.schema.modeOrderMenu = {};
        obj.schema.modeOrderMenu.normal = 0; // food menu
        // obj.schema.modeOrderMenu.foodMenu = 1;
        obj.schema.modeOrderMenu.billMenu = 1;

        // mode item display mode, display, display2 like channel, if display is independent, use different channel, otherwise, put into the same group
        obj.schema.modeItemListDisplay = {};
        obj.schema.modeItemListDisplay.normal = 0;
        // obj.schema.modeItemListDisplay.detail = 1; // deprecated, moved to display2
        obj.schema.modeItemListDisplay.zeroItem = 1;

        obj.schema.modeItemListDisplay2 = {};
        obj.schema.modeItemListDisplay2.normal = 0;
        obj.schema.modeItemListDisplay2.detail = 1;

        obj.schema.permission = {};
        obj.schema.permission.openTableAndOrder = 'p1';
        obj.schema.permission.printBill = 'p2';
        obj.schema.permission.billing = 'p3';
        obj.schema.permission.voidItem = 'p4';
        obj.schema.permission.changeTender = 'p5';
        obj.schema.permission.report = 'p6';
        obj.schema.permission.userManagement = 'p7';
        obj.schema.permission.checkBill = 'p8';
        obj.schema.permission.dailyClearance = 'p9';
        obj.schema.permission.cashManagement = 'p10';
        obj.schema.permission.cashbox = 'p11';
        obj.schema.permission.changeBillAmount = 'p12';
        obj.schema.permission.tableManagement = 'p13';

        obj.resetTitle = function( title ) {
            var title = title ? title : '';
            this.currentPageTitle = uiLanguage[INFO.lang][title] ? uiLanguage[INFO.lang][title] : title;
        }

        obj.resetDefaultMode = function( option ) {
            var option = option ? option : 'reset';

            obj.voidIndex = -1;

            // first time only
            if( option === 'init' ) {
                obj.mode = obj.schema.mode.main;
            }
            
            obj.modeMain = obj.schema.modeMain.normal;
            obj.modeItem = obj.schema.modeItem.normal;
            obj.modeItemListDisplay = obj.schema.modeItemListDisplay.normal;
            obj.modeItemListDisplay2 = obj.schema.modeItemListDisplay2.normal;
            obj.modeNavMenu = obj.schema.modeNavMenu.normal;
            obj.modeOrderMenu = obj.schema.modeOrderMenu.normal;
            obj.modeCash = obj.schema.modeCash.normal;
            obj.modeOrder = obj.schema.modeOrder.normal;
            obj.modeFloorPlan = obj.schema.modeFloorPlan.order;
            obj.modeMultiSelectItem = false;
            obj.modeItemDetail = obj.schema.modeItemDetail.code;
            // this.goToMode2({modeItem: 'normal', mode: 'main', modeOrder: 'normal', modeFloorPlan: 'order'});
        }

        // page function
        obj.resetPage = {}; // then define in each controller for extra function such as cash management
        obj.sceneInitStatus = {}; // then define in each controller for extra function such as cash management

        obj.pettyReport = [{ name: 'Petty Cash' }, { name: 'P2' }]
        obj.pettyPerPage = 8;
        obj.noOfPettyPage = Math.ceil(obj.pettyReport.length / obj.pettyPerPage);
        obj.currentPettyPage = 1;

        // console.log(obj.schema.modeOrder.printBill);

        obj.schema.member = {
            name: function () { return this.surname +' '+ this.givenname },
            dob: function () { return this.dob_yyyy + '/'+  this.dob_mm + '/' + this.dob_dd },
        }

        obj.schema.unselectOption = {
            "name1": "未選擇菜單",
            "name2": "未選擇菜單",
            "name3": "未選擇菜單",
            "optionCode": "O9999",
            "unitprice": 0,
            "name": {
                "001": "未選擇菜單",
                "002": "未選擇菜單",
                "003": "未選擇菜單"
            },
            "code": "O9999",
            "qty": 0,
            "show": false
        };

        // for option extensibility
        obj.schema.baseOption = {
            edit: function ($event, optionGroupCode) {
                // obj.Cart.resetEditItem();
                this.isEdit = true;
                // alert('hahahahaah');
                console.log('optionGroupCode ' + optionGroupCode);

                if ($event) {
                    // console.log('baseOption');
                    $event.stopPropagation();
                }
            },
            delete: function ($event, index) {
                this.isEdit = !this.isEdit;
                obj.modeOption = obj.schema.modeOption.cancel;

                if ($event) {
                    $event.stopPropagation();
                }
            },
            isEdit: false
        }

        // for existing item
        obj.schema.baseItem = {
            init: function () {
                if (this.option != undefined) {
                    $.each(this.option, function (optionIndex, option) {
                        $.extend(true, option, obj.schema.baseItem);
                        $.each(option.items, function (optionItemIndex, optionItem) {
                            $.extend(true, optionItem, obj.schema.baseItem, { qty: 0, _qty: 0 });
                        })
                    });
                }
                if (this.subitem != undefined) {
                    $.each(this.subitem, function (idx, subitem) {
                        $.extend(true, subitem, obj.schema.baseItem);
                    });
                }
            },
            getName: function () {
                if (this.customName != undefined) {
                    if (this.customName != "") {
                        return obj.fn.shorten(this.customName, 15);
                    }
                }
                if (this.name != undefined) {
                    return obj.fn.shorten(this.name[INFO.lang], 15);
                }
                else {
                    // console.log('getName ar');
                    //console.log(this.desc1);
                    return obj.fn.shorten(this.desc1, 15);
                }
            },
            getIndex: function () {
                return this.index;
            },
            getQty: function () {
                return parseInt(this.qty);
            },
            getQtyWithVoid: function () {
                //console.log('qty:' + this.qty);
                //console.log('voidQty:' + this.voidQty);
                return parseInt(this.qty) + parseInt(this.voidQty);
            },
            getUnitPrice: function () {
                /*
                  unitPrice from db tableorderb
                  unitprice from db tableorderb
                */
                if (this.unitPrice != undefined) {
                    return parseFloat(this.unitPrice);
                }else
                if (this.unitprice != undefined) {
                    return (this.unitprice / 100);
                }
            },
            getItemUnitPrice: function () {
                var optionPrice = 0
                if (this.option != undefined && this.option.length > 0) {
                    $.each(this.option, function (optionIndex, option) {
                        $.each(option.items, function (itemIndex, item) {
                            if (item.qty != undefined) {
                                //console.log("qty:" + item.getQty);
                                //console.log("qty:" + item.getQty());
                                //console.log("getUnitPrice:" + item.getUnitPrice());
                                if (item.getQty() > 0) {
                                    optionPrice += item.getUnitPrice() * item.getQty();
                                }
                            }
                        })
                    });
                }
                return optionPrice + this.getUnitPrice();
            },
            getTotalPrice: function () {
                return this.getItemUnitPrice() * this.getQty();
                //return this.getUnitPrice() ;
            },
            getPriceForSC: function () {
                switch (this.serviceCharge) {
                    case undefined:
                    case true:
                        return this.getTotalPrice();
                    default:
                        return 0;

                }
            },
            getTotalPriceWithVoid: function () {
                var p = this.getTotalPrice();
                if (this.voidQty != 0) {
                    p -= - (this.getItemUnitPrice() * this.voidQty);
                }
                return p;
                },
            editItem: function ($event) {
                console.log('baseItem / editItem');
                // console.log(this);
                console.log(obj.modeMultiSelectItem);
                if (obj.modeOrder == obj.schema.modeOrder.amendBill || obj.modeOrder == obj.schema.modeOrder.amendBillSummary || obj.modeOrder == obj.schema.modeOrder.printBill ||
                     obj.modeOrder == obj.schema.modeOrder.searchBill) {
                    console.log('-1');
                    return;
                } else if (obj.modeMultiSelectItem) {
                    // console.log('1');
                    console.log('0');
                    this.isEdit = !this.isEdit;
                } else if (this.isEdit) {
                    // obj.Cart.currentOption = null;
                    console.log('1');
                    obj.Cart.currentOptionList = [];
                    obj.Cart.currentOptionIndex = -1;
                    obj.Cart.resetEditItem();
                    obj.modeItem = obj.schema.modeItem.normal;
                    return;
                } else {
                    console.log('2');
                    obj.Cart.currentOptionList = [];
                    obj.Cart.currentOptionIndex = -1;
                    obj.Cart.resetEditItem();
                    this.isEdit = true;
                }
                //obj.toggleActive($event, function () { });
                if ($event) {
                    $event.stopPropagation();
                }
            },
            selectAllItem: function ($event) {
                $.each(obj.Cart.orderList, function( idx, val ){
                    // console.log(this);
                    this.isEdit = true;
                })
            },
            toggleAllItem: function ($event) {
                $.each(obj.Cart.orderList, function( idx, val ){
                    // console.log(this);
                    this.isEdit = !this.isEdit;
                })
            },
            unselectAllItem: function ($event) {
                $.each(obj.Cart.orderList, function( idx, val ){
                    // console.log(this);
                    this.isEdit = false;
                })
            },
            deleteItem: function ($event, index) {
                console.log('baseItem / deleteItem');
                this.isEdit = !this.isEdit;
                obj.modeItem = obj.schema.modeItem.cancel;
                obj.modeMultiSelectItem = true;

                if ($event) {
                    $event.stopPropagation();
                }
            },
            qty: 1,
            voidQty: 0,
            voidIndex: -1,
            voidRemark: '',
            isDelete: false,
            isEdit: false,
            modifier: [],
            kitchenMsg: [],
            kitchenMsgOld: []
        }

        // for new item
        obj.schema.baseCartItem = $.extend(true, {}, obj.schema.baseItem, {
            deleteItem: function ($event, index) {
                obj.Cart.cartList.splice(index, 1);
                // when delete item, reset to default mode, such as modeOrder to normal while in option
                obj.modeOrder = obj.schema.modeOrder.normal;
                obj.modeItem = obj.schema.modeItem.normal;
                // obj.modeItemDetail = obj.schema.modeItemDetail.code;
                obj.Cart.currentOptionList = [];
                obj.Cart.currentOptionIndex = -1;

                if ($event) {
                    $event.stopPropagation();
                }
            },
            editItem: function ($event, focus) {
                console.log('baseCartItem / editItem');
                if (obj.modeMultiSelectItem) {
                    // console.log(1);
                    this.isEdit = !this.isEdit;
                } else if (this.isEdit) {
                    // deselect
                    // obj.Cart.currentOption = null;
                    obj.Cart.currentOptionList = [];
                    obj.Cart.currentOptionIndex = -1;
                    obj.Cart.resetEditItem();
                    obj.modeItem = obj.schema.modeItem.normal;
                    return;
                } else {
                    obj.Cart.currentOptionList = [];
                    obj.Cart.currentOptionIndex = -1;
                    obj.Cart.resetEditItem();
                    this.isEdit = true;
                }
                //obj.toggleActive($event, function () { });
                if ($event) {
                    $event.stopPropagation();
                }
                console.log("this.voidIndex", this.voidIndex);

                if (this.voidIndex == -1 && this.option != undefined && this.option) {
                    // if (this.option) {
                        obj.modeItem = obj.schema.modeItem.option;
                        obj.Cart.currentOption = this.option;
                        // obj.Cart.currentOptionIndex = 0;
                        // obj.Cart.currentOptionList = obj.Cart.currentOption[obj.Cart.currentOptionIndex];

                        // obj.Cart.showNextOption();

                        // show all option in item list
                        // console.log(item.option);
                        console.log('prepare extended option');

                        this.selectedOption = [];
                        $.each(this.option, function (idx, optionList) {
                            console.log(optionList.minQty);
                            for (var i = 0; i <= optionList.minQty; i++) {
                                var fillOption = $.extend(true, {}, obj.schema.unselectOption, obj.schema.baseItem, { qty: 1, _qty: 1 });
                                if (idx == 0 && i == 0) {
                                    fillOption.isEdit = true;
                                }
                                var exists = false;
                                angular.forEach(optionList.items, function(item) {
                                    if (item.optionCode === fillOption.optionCode) {
                                        exists = true;
                                    }
                                });
                                if (!exists) optionList.items.push(fillOption);
                            }
                            //$.each(optionList.items, function (oidx, option) {
                            //    newItem.selectedOption.push($.extend(true, {}, option, obj.schema.baseOption));
                            //});

                        });

                        obj.Cart.showNextOption();

                        //newItem.option = $.extend(true, {}, item.option , obj.schema.baseOption);
                        //newItem.option.edit(null, '');
                        //console.log(newItem.option);
                        // console.log(newItem);
                    // }


                    
                }
                else if (focus == undefined && this.qty > 0 && !obj.modeCheckOrder('transfer') && !obj.modeCheckOrder('specialMessage')) {
                    obj.modeItem = obj.schema.modeItem.modifier;
                }

                $timeout(function () {
                    obj.scrollToBottom();
                }, 10);
            }
        });

        obj.splitListUpdatePrice = function () {
            var price = 0;
            if (obj.Cart.allSplitList[obj.Cart.splitListLIndex].item) {
                //$.each(obj.Cart.allSplitList[obj.Cart.splitListLIndex].item, function (i, v) {
                //    //if (v.getQty() > 0 && !v.isDelete)
                //    price += v.getTotalPrice();
                //});
                var priceObj = obj.calcPrice([obj.Cart.allSplitList[obj.Cart.splitListLIndex].item]);

                obj.Cart.splitListLPrice = priceObj.price;
                obj.Cart.splitListLServiceCharge = priceObj.serviceCharge;
                obj.Cart.splitListLRemainings = priceObj.remainings;
                obj.Cart.splitListLTotalPrice = priceObj.totalPrice;

                //var scObj = obj.calcServiceCharge(price);
                //obj.Cart.splitListLServiceCharge = scObj.serviceCharge;
                //obj.Cart.splitListLRemainings = scObj.remainings;
                //obj.Cart.splitListLTotalPrice = price + obj.Cart.splitListLServiceCharge;
            }
            price = 0;
            if (obj.Cart.allSplitList[obj.Cart.splitListRIndex].item) {
                //$.each(obj.Cart.allSplitList[obj.Cart.splitListRIndex].item, function (i, v) {
                //    price += v.getTotalPrice();
                //});
                //obj.Cart.splitListRPrice = price;

                var priceObj = obj.calcPrice(obj.Cart.allSplitList[obj.Cart.splitListRIndex].item);
                obj.Cart.splitListRPrice = priceObj.price;
                obj.Cart.splitListRServiceCharge = priceObj.serviceCharge;
                obj.Cart.splitListRRemainings = priceObj.remainings;
                obj.Cart.splitListRTotalPrice = priceObj.totalPrice;

                //var scObj = obj.calcServiceCharge(price);
                //obj.Cart.splitListRServiceCharge = scObj.serviceCharge;
                //obj.Cart.splitListRRemainings = scObj.remainings;
                //obj.Cart.splitListRTotalPrice = price + obj.Cart.splitListRServiceCharge;
            }
        }

        obj.splitListClick = function (currentIdx,qty) {
            console.log(currentIdx);
            console.log(obj.Cart.allSplitList[obj.Cart.splitListLIndex].item);
            console.log(obj.Cart.allSplitList[obj.Cart.splitListRIndex].item);
            var checkList = obj.Cart.splitListLIndex == currentIdx ? obj.Cart.allSplitList[obj.Cart.splitListRIndex].item : obj.Cart.allSplitList[obj.Cart.splitListLIndex].item;
            var currentList = obj.Cart.allSplitList[currentIdx].item;
            var swapList = [];

            console.log(swapList);
            var checkFunction = function () {
                $.each(checkList, function (idx, item) {
                    if (item.isEdit) {
                        var cloneItem = {};
                        checkList[idx].isEdit = false;
                        angular.copy(checkList[idx], cloneItem);
                        swapList.push(cloneItem);
                        if(qty == undefined){
                            checkList.splice(idx, 1)
                        } else if (qty >= item.qty) {
                            checkList.splice(idx, 1)
                        } else {
                            item.qty -= qty;
                            swapList[swapList.length - 1].qty = qty;

                            var cloneVoidItem = {};
                            angular.copy(checkList[idx], cloneVoidItem);
                            cloneVoidItem.qty = -1 * qty;
                            cloneVoidItem.voidIndex = item.index;
                            cloneVoidItem.voidRemark = "Item transfer";
                            checkList.push(cloneVoidItem);
                        }
                        checkFunction();
                        return false;
                    }
                });
            }
            checkFunction();
            if (swapList.length != 0)
            {
                for (var i = 0; i < swapList.length; i++) {
                    swapList[i].isEdit = false;
                    currentList.push(swapList[i]);
                }

                currentList.sort(function (a, b) {
                    return  parseInt(a.index) > parseInt( b.index);
                });
                //obj.Cart.splitListR.concat(swapList)
                console.log(obj.Cart.splitListR.length);
                obj.splitListUpdatePrice();
                return true;
            } else {
                obj.Cart.currentSelectedSplitIndex = currentIdx;
                return false;
            }
        }

        obj.schema.baseSplitItem = $.extend(true, {}, obj.schema.baseItem, {
            editItem: function ($event, currentIdx) {
                console.log('obj.schema.baseSplitItem / editItem');
                if (obj.splitListClick(currentIdx)) {

                } else {
                    console.log(obj.modeMultiSelectItem);
                    if (obj.modeMultiSelectItem) {
                        // console.log(1);
                        this.isEdit = !this.isEdit;
                    } else if (this.isEdit) {
                        // obj.Cart.currentOption = null;
                        obj.Cart.currentOptionList = [];
                        obj.Cart.currentOptionIndex = -1;
                        obj.Cart.resetEditItem();
                        obj.modeItem = obj.schema.modeItem.normal;
                        return;
                    } else {
                        // console.log(2);
                        obj.Cart.currentOptionList = [];
                        obj.Cart.currentOptionIndex = -1;
                        obj.Cart.resetEditItem();
                        this.isEdit = true;
                    }
                };
                if ($event) {
                    $event.stopPropagation();
                }
            }
        });

        // default settings
        obj.mode = obj.schema.mode.main;
        obj.modeMain = obj.schema.modeMain.normal;
        obj.modeItem = obj.schema.modeItem.normal;
        obj.modeCash = obj.schema.modeCash.normal;
        //obj.mode = obj.schema.mode.login;
        //obj.mode = obj.schema.mode.floorPlan;
        //obj.mode = obj.schema.mode.order;
        obj.modeOrder = obj.schema.modeOrder.normal;

        obj.modeFloorPlan = obj.schema.modeFloorPlan.order;
        obj.modeMultiSelectItem = false;
        obj.itemsPerPage = 18-5;
        obj.catsPerPage = 8;
        obj.cancelOptionPerPage = 13;
        obj.cancelOptionTotalItems = INFO.cancelRemark.length;
        obj.noOfCancelOptionPage = Math.ceil( obj.cancelOptionTotalItems / obj.cancelOptionPerPage );
        obj.cancelOptionCurrentPage = 1;
        obj.modeItemDetail = obj.schema.modeItemDetail.code;

        obj.modeToggleItemDetail = function (element) {
            obj.modeItemDetail = obj.modeItemDetail == obj.schema.modeItemDetail.code ? obj.schema.modeItemDetail.printer : obj.schema.modeItemDetail.code;
        }

        obj.resizeOrderListHeight = function( element ) {
            var target = element ? element : "order-scene .order-list-wrapper";
            // total height of display area (exclude top and status bar)
            var maxHeight = angular.element(".menu-area").height();

            // total height of account summary
            var totalHeight = 0;
            angular.element(".account-summary .row").each(
             function(index, ele){
                 totalHeight = totalHeight + angular.element(ele).height();
                 console.log(angular.element(ele).height());
             }
            );
            // order list maximum height
            var orderListMaxHeight = maxHeight - totalHeight;
            angular.element(element).css("max-height", orderListMaxHeight);
        }

        obj.modeToggleModifier = function () {
            obj.modeItem = obj.modeItem == obj.schema.modeItem.normal ? obj.schema.modeItem.modifier : obj.schema.modeItem.normal
        }

        obj.modeToggleFoodControl = function () {
            obj.modeOrder = obj.modeOrder == obj.schema.modeOrder.normal ? obj.schema.modeOrder.foodControl : obj.schema.modeOrder.normal
        }

        // obj.modeToggleBilling = function () {
        //     console.log('toggle billing');
        //     obj.modeOrder = obj.modeOrder == obj.schema.modeOrder.billing ? obj.schema.modeOrder.billing : obj.schema.modeOrder.billing
        //     obj.schema.billing = true;
        // }

        obj.modeToggleOrder = function () {
            console.log('toggle order');
            obj.modeOrder = obj.modeOrder == obj.schema.modeOrder.normal ? obj.schema.modeOrder.detail : obj.schema.modeOrder.normal
        }

        // for legacy RMSTable function compatibility purpose, deprecated, please merge into better code later
        // deprecated
        obj.RMSTable = {
            editMode : "order",
            changeMode : function( mode ) {
                this.editMode = lowerFirstLetter( mode );
                console.log(lowerFirstLetter( mode ));
                console.log(this.editMode);
                showMessage('#status-message', uiLanguage[INFO.lang][lowerFirstLetter( mode )] + ' ' + uiLanguage[INFO.lang]['mode'], '', 'warn', 'stick');
            }
        };

        obj.showStatusMessage = function( message ) {
            showMessage('#status-message', message, '', 'warn', 'stick');
        }

        obj.UserManager = {};
        obj.UserManager.LoginRedirectToTable = '';
        obj.UserManager.LoginRedirectTo = {};
        obj.UserManager.staffName = '';
        obj.UserManager.staffCode = '';
        obj.UserManager.staffDetails = '';
        obj.UserManager.isLogin = false;
        obj.UserManager.loginCallback = function () {
            console.log('login callback');
            // var redirectMode = obj.UserManager.LoginRedirectToMode;
            // if ( angular.isDefined( redirectMode ) && Object.size( redirectMode ) != 0 ) {
            //     obj.goToMode( redirectMode.type, redirectMode.mode );
            //     // obj.goToMode2( redirectMode );

            //     // reset
            //     obj.UserManager.LoginRedirectTo = {};
            //     obj.UserManager.LoginRedirectToMode = {};
            // }

            var redirectMode2 = obj.UserManager.LoginRedirectToMode2;
            var eventEle = obj.UserManager.LoginRedirectToCaller;
            if ( angular.isDefined( redirectMode2 ) && Object.size( redirectMode2 ) != 0 ) {
                // console.log('redirect mode v2');
                obj.goToMode2( redirectMode2, eventEle );

                // reset
                obj.UserManager.LoginRedirectTo = {};
                obj.UserManager.LoginRedirectToMode2 = {};
                obj.UserManager.LoginRedirectToCaller = {};
            }

            if (obj.UserManager.LoginRedirectToTable != '') {
                //alert(MainService.modeFloorPlan)
                if (obj.modeFloorPlan == obj.schema.modeFloorPlan.billing) {
                    obj.billTable(obj.UserManager.LoginRedirectToTable);
                } else
                    if (obj.modeFloorPlan == obj.schema.modeFloorPlan.split) {
                        console.log('split table');
                        obj.splitTableNo(obj.UserManager.LoginRedirectToTable);
                    } else {
                        obj.assignTableNo(obj.UserManager.LoginRedirectToTable, function (cb) {
                            if (angular.element('md-dialog[aria-label="Bill Option"]').scope() == undefined) {
                                $mdDialog.show({
                                    controller: function ($scope) {
                                        $scope.order = cb;
                                        $scope.assignTableNo = function (t) {
                                            obj.assignTableNo(t);
                                            $scope.closeDialog();
                                        }
                                        $scope.closeDialog = function () {
                                            $mdDialog.hide();
                                        }
                                    },
                                    templateUrl: 'tmpl/billOption.html'
                                })
                            } else {
                                $mdDialog.hide();
                            }
                        });
                    }
                obj.UserManager.LoginRedirectToTable = '';
            }
        }
        obj.UserManager.checkLogin = function () {
            if (!obj.UserManager.isLogin){
                if (angular.element('md-dialog[aria-label="Login"]').scope() == undefined) {
                    $mdDialog.show({
                        templateUrl: 'tmpl/login.html'
                    })
                } else {
                    $mdDialog.hide();
                }
            }
        };
        obj.UserManager.logout = function (username, password) {
            // console.log(obj.modeOrder);
            console.log('logout ar!');
            obj.UserManager.isLogin = false;
            console.log(obj.UserManager.isLogin)
            obj.UserManager.staffName = '';
            console.log(obj.stopLogoutCountDown)
            obj.stopLogoutCountDown();
            console.log('after stop function')
            // for the following pages, redirect to floorplan
            // console.log(this.modeOrder);


            // console.log(obj.modeOrder);
            if (obj.modeOrder == obj.schema.modeOrder.searchBill) {
                console.log('search bill out?');
                obj.modeOrder = {};
                obj.mode = obj.schema.mode.main;
            }

            if ( obj.modeChecking( [{'mode': ['report', 'main', 'userManagement', 'dailyClearance', 'cashManagement', 'userClockManagement', 'userGroupManagement']}] ) || obj.modeCheckOrder('searchBill') ) {
                obj.mode = obj.schema.mode.main;
            }
            else {
                obj.mode = obj.schema.mode.floorPlan;
            }

            this.resetDefaultMode();
        }
        obj.UserManager.login = function (username, password) {
            var deferred = $q.defer();
            SocketFactory.emit('login', { u: username, p: password }, function (r) {
                //r = JSON.parse(r);
                if (r.success) {
                    obj.UserManager.staffName = r.staffName;
                    obj.UserManager.staffCode = username;
                    obj.UserManager.staffDetails = r.staffDetails;
                    obj.UserManager.isLogin = true;

                    // console.log('success?');
                    // obj.startLogoutCountDown();
                }
                deferred.resolve(r);
            });
            return deferred.promise;
        }
        obj.UserManager.requestApproval = function (username, password) {
            var deferred = $q.defer();
            SocketFactory.emit('login', { u: username, p: password }, function (r) {
                if (r.success) {
                    //return r;
                }
                deferred.resolve(r);
            });
            return deferred.promise;
        }


        obj.fn = {};
        obj.fn.shorten = function (text) {
            return text;
            if (text)
                return shortenText(text, 15);
            else
                return "";
        }

        obj.showAlert = function (content, title) {
            title = uiLanguage[INFO.lang][title] ? uiLanguage[INFO.lang][title] : title || uiLanguage[INFO.lang]['alert'],
            content = uiLanguage[INFO.lang][content] ? uiLanguage[INFO.lang][content] : content || 'Please provide content';

            $mdDialog.show(
              $mdDialog.alert()
                .parent(angular.element(document.body))
                .title(title)
                .content(content)
                .ok(uiLanguage[INFO.lang]["affirmative"])
            );
        }

        obj.showConfirm = function (content, title, okCallback, cancelCallback) {
            var title = uiLanguage[INFO.lang][title] ? uiLanguage[INFO.lang][title] : title || uiLanguage[INFO.lang]['alert'],
            content = uiLanguage[INFO.lang][content] ? uiLanguage[INFO.lang][content] : content || 'Please provide content';
            var confirm = $mdDialog.confirm()
                  .parent(angular.element(document.body))
                  .title(title)
                  .content(content)
                  // .content('Are you sure to delete the groups?')
                  .ok(uiLanguage[INFO.lang]['yes'])
                  .cancel(uiLanguage[INFO.lang]['cancel'])
                  //.targetEvent(ev);
            $mdDialog.show(confirm).then(function () {
                //alert('You decided to get rid of your debt.');
                //if()
                okCallback();
            }, function () {
                //alert('You decided to keep your debt.');

                cancelCallback();
            });
        }

        obj.clickTable = function (tableNo, status, callback) {
            console.log('click me');
            console.log(this.modeFloorPlan);


            switch (this.modeFloorPlan){
                case this.schema.modeFloorPlan.order:
                    console.log('this.schema.modeFloorPlan.order');
                    obj.modeItem = obj.schema.modeItem.normal;
                    this.assignTableNo(tableNo, function (cb) {
                        //callback(cb);
                        if (angular.element('md-dialog[aria-label="Bill Option"]').scope() == undefined) {
                            console.log('clickTable - assignTableNo');
                            $mdDialog.show({
                                controller: function ($scope) {
                                    $scope.order = cb;
                                    $scope.assignTableNo = function (t) {
                                        obj.assignTableNo(t);
                                        $scope.closeDialog();
                                    }
                                    $scope.closeDialog = function () {
                                        $mdDialog.hide();
                                    }
                                },
                                templateUrl: 'tmpl/billOption.html'
                            })
                        } else {
                            $mdDialog.hide();
                        }
                    });
                    break;
                case this.schema.modeFloorPlan.split:
                    this.splitTableNo(tableNo);
                    break;
                case this.schema.modeFloorPlan.billing:
                    console.log('test billing');
                    this.appliedPaymentMethod = [];

                    this.appliedDiscount = [];

                    // alert(status);
                    if( status == 2 ) {
                        // should print first
                        obj.showAlert('please_print_bill_first');
                        return;
                    }
                    if( status == 3 ) {
                        this.billTable(tableNo, 'billing');
                        return;
                    } else {
                        obj.showAlert('no_order');
                        return;
                    }
                    break;
                case this.schema.modeFloorPlan.printOrder :
                    this.printOrder(tableNo,'order');
                    break;
                case this.schema.modeFloorPlan.printBill:
                    console.log("printing bill");
                    // this.printOrder(tableNo,'bill'); // put in billTable() after yes
                    // return;
                    console.log(status)
                    // only table with order or print billed data is eligible to print
                    // obj.Cart.tableNo = tableNo;
                    // var status = Snap("#svg-canvas").select('#table_1418785047317').data().loadedData.status;
                    if( status == 2 || status == 3 ) {
                        //if( status == 2  ) {
                            this.billTable(tableNo, 'printBill');
                            // this.printOrder(tableNo,'bill'); // put in billTable() after yes
                        //}
                        //if( status == 3 ) {
                        //    // printed bill
                        //    var confirm = $mdDialog.confirm()
                        //          .parent(angular.element(document.body))
                        //          .title(uiLanguage[INFO.lang]['alert'])
                        //          .content('決定重覆打印賬單？')
                        //          .ok(uiLanguage[INFO.lang]['yes'])
                        //          .cancel(uiLanguage[INFO.lang]['cancel']);
                        //    $mdDialog.show(confirm).then(function() {
                        //      console.log('print again');
                        //    }, function() {
                        //      console.log('print again cancel');

                        //    });
                        //}
                    } else {
                        obj.showAlert('no_order');
                        return;
                    }
                    console.log('should not see me');
                    break;
                case this.schema.modeFloorPlan.kitchenMessage:
                    console.log('this.schema.modeFloorPlan.kitchenMessage');
                    this.tableKitchenMessage(tableNo, 'kitchenMessage');
                    break;
            }
        };

        obj.switchModeItem = function (modeChange) {

        }

        obj.resetPaymentSettings = function() {
            this.appliedPaymentMethod = []; // reset for next round
            this.remainerAmount = 0; // reset for next round
        }

        // logic:
        // add multiple payment method
        // when add once, payment method will gain one into array
        // default: cash + exact amount (if enter but not show in screen, avoid checking)
        /*
            default: store payment method into array (cash)
            [cash]
            if press enter -> submit assume exact amount with cash method (skip total amount input check)
            if press any number key -> assume custom input cash amount, when submit, check input amount
            if press cash on top of default with some amount input already (cash + amount then add cash again) and
            that amount < total required amount -> prompt to input/cannot proceed, if >= required amount, proceed
            and the new cash method is stored into array [cash, cash]
            assumed 2 or more method is added such as [cash, cash, visa]
            then these method will be displayed with method name and cash

            tips: use smaller font to show the payment method first to pass the deadline demo
        */

        // obj.paymentMethod = uiLanguage[INFO.lang]['cash'];
        // multiple payment method support
        obj.checkPaymentTips = function(forceAdd) {
            if (angular.isUndefined(this.appliedPaymentMethod) || this.appliedPaymentMethod.length === 0) {
                if (forceAdd == undefined) {
                    this.appliedPaymentMethod = [
                    // set default if no selected method
                    {
                        'method': 'cash',
                        'amount': this.Cart.totalPrice
                    }];
                    // this.isDefaultPaymentMethod = true;
                    // this.isNotDefaultPaymentMethod = false;
                    this.isCash = true;
                    this.isNotCash = false;
                }
            }
            console.log('checkPaymentTips');
            console.log(obj.remainerAmount >= 0 && obj.isCash);
            console.log(obj.remainerAmount < 0);
            console.log(obj.remainerAmount >= 0 && obj.isNotCash);

            if (this.appliedPaymentMethod) {
                if (this.appliedPaymentMethod.length == 1 && this.appliedPaymentMethod[0].method === 'cash') {
                    this.isCash = true;
                    this.isNotCash = false;
                } else {
                    this.isCash = false;
                    this.isNotCash = true;
                }
            }
            console.log('end checkPaymentTips')
        }
        obj.setPaymentMethod = function( methodName ) {
            // reset default payment method on first click
            // check if first click or not
            this.inputPrice = 0; // reset for each new method

            if( this.appliedPaymentMethod.length === 0 ) {
                // init method array
                 this.appliedPaymentMethod = [
                     {
                         method: methodName,
                         amount: 0 // change by input
                     }
                 ];
            } else {
                // add method to array if previous method have value, if not, replace current method
                if( this.appliedPaymentMethod[this.appliedPaymentMethod.length - 1].amount === 0 ) {
                    /*$mdDialog.show(
                        $mdDialog.alert()
                        .parent(angular.element(document.body))
                        .title(uiLanguage[INFO.lang]['alert'])
                        .content(uiLanguage[INFO.lang]['current_method_no_amount'])
                        .ok(uiLanguage[INFO.lang]['ok'])
                    );
                    return;*/
                    this.appliedPaymentMethod[ this.appliedPaymentMethod.length - 1 ] = {
                        method: methodName,
                        amount: 0 // change by input
                    };
                } else {
                    this.appliedPaymentMethod.push(
                        {
                            method: methodName,
                            amount: 0 // change by input
                        }
                    );
                }
            }

            this.checkPaymentTips();
        }

        obj.checkPaymentMethod = function() {
            // set default price for billing, set switch for extension
            if( this.appliedPaymentMethod.length === 0 ) {
                this.appliedPaymentMethod = [
                // set default if no selected method
                {
                    'method' : 'cash',
                    'amount' : this.Cart.totalPrice
                }];
                // this.isDefaultPaymentMethod = true;
                // this.isNotDefaultPaymentMethod = false;
                return true;
            } else {
                // this.isDefaultPaymentMethod = false;
                // this.isNotDefaultPaymentMethod = true;
                return false;
            }
        }

        obj.deletePaymentMethod = function (index) {
            console.log(this.appliedPaymentMethod);
            this.appliedPaymentMethod.splice(index, 1);
            this.remainder();
            this.checkPaymentTips(false);
        }

        obj.updatePaymentAmount = function( amount ) {
            if( this.appliedPaymentMethod.length > 0 ) {
                // last/latest method
                this.appliedPaymentMethod[this.appliedPaymentMethod.length - 1].amount = parseFloat(amount);
            }
        }

        // obj.appliedDiscount
        obj.availableDiscount = []; // by server?
        obj.availableDiscount = [
            //{
            //    coupon_alias: "cash_coupon_100",
            //    amount: 100,
            //    type: 'exact'
            //},
            //{
            //    coupon_alias: "cash_coupon_200",
            //    amount: 200,
            //    type: 'exact'
            //},
            //{
            //    coupon_alias: "cash_coupon_500",
            //    amount: 500,
            //    type: 'exact'
            //},
            {
                coupon_alias: "off_10",
                amount: 0.9,
                type: 'percentage'
            }
            ,
            {
                coupon_alias: "off_20",
                amount: 0.8,
                type: 'percentage'
            },
            {
                coupon_alias: "off_30",
                amount: 0.7,
                type: 'percentage'
            }

            // next phase
            /*,
            {
                coupon_alias: "deposit",
                amount: 0, // drawn from system, input reference code to load when click
                type: 'deposit' // will determine what to do for the "click" event
            },
            {
                coupon_alias: "custom",
                amount: 0, // drawn from system, input reference code to load when click
                type: 'custom' // will determine what to do for the "click" event
            }*/
        ];
        obj.discounPerPage = 13;
        obj.noOfPage_AvailableDiscount = Math.ceil( obj.availableDiscount.length / obj.discounPerPage );

        // return selected coupon obj with alias
        // @param string (coupon_alias)
        // @return object
        function getCouponObj( coupon_alias ) {
            for (var i = 0; i < obj.availableDiscount.length; i++) {
                if( obj.availableDiscount[i].coupon_alias === coupon_alias ) {
                    return obj.availableDiscount[i];
                }
            }
        }

        obj.setDiscount = function( coupon_alias ) {
            var couponObj = getCouponObj(coupon_alias);
            this.appliedDiscount = [couponObj];
            //this.appliedDiscount.push( couponObj );
            console.log(this.inputPrice);
            console.log(couponObj)
            //var priceObj = obj.calcPrice([obj.Cart.orderList])

            obj.Cart.couponObj = [couponObj];

            // exact amount add later
            // this.addAmount( couponObj.amount ) // is numeric
            this.remainder();
            this.checkPaymentTips(false);
        };

        obj.deletDiscount = function (index) {
            obj.Cart.couponObj.pop();
            //this.appliedDiscount.splice( index, 1 );
            this.appliedDiscount.pop();
            this.remainder();
        }

        // old method: change text only, no multiple
        // obj.selectPaymentMethod = function( methodName ) {
        //     console.log('methodName ' + methodName);
        //     if(angular.isUndefined( methodName ))
        //         this.paymentMethod = uiLanguage[INFO.lang]['no_selection'];
        //     this.paymentMethod = uiLanguage[INFO.lang][methodName];
        // }

        obj.addAmount = function (amount) {
            if (isNaN(this.inputPrice) || this.inputPrice == "") {
                this.inputPrice = 0;
            }
            //alert(this.inputPrice);
            console.log(this.inputPrice); // before modification
            this.inputPrice = String( parseFloat( this.inputPrice ) + amount );
            this.updatePaymentAmount( this.inputPrice );
            this.remainder();
        }

        obj.inputExact = function() {
            this.inputPrice = String(this.Cart.totalPrice);
            this.updatePaymentAmount( this.inputPrice );
            this.remainder();
        }

        obj.inputPrice = '';
        obj.floatingInput = false;
        obj.appendAmount = function( amount ) {
            console.log(amount);
            // do nothing if floating point is already input
            if( amount === '.' && this.inputPrice[this.inputPrice.length - 1] === '.') {
                console.log('0');
                return;
            }

            // if floating point is activated, not allow to add one character
            // this.inputPrice += '';
            this.inputPrice = String( this.inputPrice ); // type aware method and meaning is obvious without comment
            if( this.inputPrice.indexOf('.') === -1 && amount === '.' ) {
                console.log('1');
                this.floatingInput = true;
                // not necessary because " ", it will make it .# instead of 0.# when it is 0
                // if (this.inputPrice.trim() == "0") { // input 7, result: .7 instead of 0.7
                //     console.log('1-1');
                //     console.log(this.inputPrice.trim());
                //     this.inputPrice = amount;
                // }else{
                //     console.log('1-2');
                //     this.inputPrice = this.inputPrice.concat(amount);
                // }
                this.inputPrice = this.inputPrice.trim().concat( amount );
                return;
            }

            if( this.inputPrice.indexOf('.') != -1 && amount === '.' ) {
                console.log('2');
                return
            }

            // console.log(this.inputPrice.indexOf('.'));
            // console.log( !isNaN(parseInt(this.inputPrice[this.inputPrice.length - 1])));
            if( this.inputPrice.indexOf('.') != -1 && !isNaN(parseInt(this.inputPrice[this.inputPrice.length - 1])) ) {
                console.log("3");
                return;
            }
            // console.log(typeof this.inputPrice);
            // console.log(typeof this.inputPrice.trim());
            // console.log('when is this.inputPrice.trim() == 0 ' + this.inputPrice.trim());

            if (this.inputPrice.trim() == "0") { // 排除預設等值有space的障礙
                console.log("4");
                this.inputPrice = amount;
            } else {
                console.log("5");
                this.inputPrice = this.inputPrice.concat(amount);
            }
            //this.inputPrice = this.inputPrice.concat(amount);
            this.updatePaymentAmount( this.inputPrice );
            this.remainder();
            // default
        }

        obj.remainerAmount = '0.0';
        // obj.totalPriceInput = '0.0';
        obj.remainder = function() {
            // support multiple payment method
            var totalPriceInput = 0,
                totalCouponAmount = 0;

            // exact amount coupon add later
            /*// if after applied coupon, remainer >= 0, then no need for "payment change" and not require any payment method (cash will be set, 0 will be assumed)
            for (var i = 0; i < this.appliedDiscount.length; i++) {
                totalCouponAmount += this.appliedDiscount[i].amount;
            }
            this.remainerAmount = parseFloat(totalCouponAmount) - parseFloat(this.Cart.totalPrice);*/

            //alert(this.Cart.totalPrice)
            //if( this.remainerAmount < 0 ) {
            //console.log('.............');
            //console.log(this.appliedPaymentMethod);
            if( this.appliedPaymentMethod ) {
                for (var i = 0; i < this.appliedPaymentMethod.length; i++) {
                    obj.totalPriceInput = totalPriceInput += this.appliedPaymentMethod[i].amount;
                }
                this.remainerAmount = parseFloat(totalPriceInput) - parseFloat(this.Cart.totalPrice);
            }

            // this.totalPriceInput = String( totalPriceInput ); // for truncate...etc operation

            // old: only 1 inputPrice
            // this.remainerAmount = parseFloat(this.inputPrice) - parseFloat(this.Cart.totalPrice);

            //console.log(parseFloat(this.inputPrice));
            //console.log(parseFloat(this.Cart.price));
            //console.log(parseFloat(this.Cart.serviceCharge));

            if( isNaN(this.remainerAmount) )
                this.remainerAmount = 0;
        }

        obj.truncateAmount = function() {
            if( this.inputPrice[this.inputPrice.length - 1] === '.' ) {
                this.floatingInput = false;
            }
            // convert it to string or it will get error when it is 0 and use backspace
            this.inputPrice = String( this.inputPrice );
            this.inputPrice = this.inputPrice.substring(0, this.inputPrice.length - 1);

            if( isNaN(this.inputPrice) || this.inputPrice === '' ) {
                this.inputPrice = 0;
            }
            this.updatePaymentAmount( this.inputPrice );
            this.remainder();
        }



        // old method
        /*obj.truncateAmount = function() {
            if( this.inputPrice[this.inputPrice.length - 1] === '.' ) {
                this.floatingInput = false;
            }
            // convert it to string or it will get error when it is 0 and use backspace
            this.inputPrice = String( this.inputPrice );
            this.inputPrice = this.inputPrice.substring(0, this.inputPrice.length - 1);

            if( isNaN(this.inputPrice) || this.inputPrice === '' ) {
                this.inputPrice = 0;
            }
            this.remainder();
        }*/

        /*
            this newly created mode switcher is intended for generic purpose, lesser code and used in main.html first for
            login checking
            mode: main, floor, order, item
        */
        // deprecated, replaced by v2
        /*obj.goToMode = function( modeType, modeName ) {
            console.log('change to mode type ' + modeType);
            console.log('change to mode ' + modeName);

            if (modeName == "userManagement" || modeName == "report" || modeName == "order") {
                var p = modeName;
                switch (modeName) {
                    case "userManagement": p = 'userManagement'; break;
                    case "report": p = 'report'; break;
                    case "order": p = 'checkBill'; break;
                }
                obj.checkPermission(p, function () {
                    obj[modeType] = obj.schema[modeType][modeName];
                })
            } else {
                obj[modeType] = obj.schema[modeType][modeName];
            }

            // switch( modeType ) {
            //     case 'main': // mode (main mode)
            //         console.log('change mode');
            //         // this.mode = this.schema.mode[modeName];

            //         break;
            //     case 'floor': // modeFloorPlan

            //         break;
            //     case 'order': // modeOrder

            //         break;
            //     case 'item': // modeItem

            //         break;
            // }
        }*/
        // deprecated, replaced by v2
        /*obj.switchMode = function( modeType, modeName, requiredLogin ) {
            // all mode changed here required login by default
            //alert(1);
            var requiredLogin = ( angular.isDefined(requiredLogin) ) ? requiredLogin : true;

            if( requiredLogin && !obj.UserManager.isLogin ) {
                this.UserManager.checkLogin();
                obj.UserManager.LoginRedirectToMode = {
                    type: modeType,
                    mode: modeName
                }
            } else {
                this.goToMode( modeType, modeName );
            }
        }*/

        obj.resetTitle = function( title ) {
            var title = title ? title : '';
            this.currentPageTitle = uiLanguage[INFO.lang][title] ? uiLanguage[INFO.lang][title] : title;
        }

        obj.resetDefaultMode = function() {
            // obj.mode = obj.schema.mode.main;
            obj.modeMain = obj.schema.modeMain.normal;
            obj.modeItem = obj.schema.modeItem.normal;
            obj.modeCash = obj.schema.modeCash.normal;
            obj.modeOrder = obj.schema.modeOrder.normal;
            obj.modeFloorPlan = obj.schema.modeFloorPlan.order;
            obj.modeMultiSelectItem = false;
            obj.modeItemDetail = obj.schema.modeItemDetail.code;
            // this.goToMode2({modeItem: 'normal', mode: 'main', modeOrder: 'normal', modeFloorPlan: 'order'});
        }

        // test for multiple mode changing method, take modeOption with multiple modes at once inside
        // for future version
        // usage: obj.goToMode2({mode: 'save', mode: 'save2', mode4: 'save1'})
        // support change multiple mode at once
        obj.goToMode2 = function( modeOption, $event, setMessage ) {
            setMessage = setMessage ? setMessage : true;
            // console.log($event);
            // console.log(modeOption.mode == 'dailyClearance');
            // reset respective page to default
            // console.log(this.modeCheckBase('cashManagement'));
            

            // console.log(modeOption);

            if( modeOption.mode == 'cashManagement' && this.modeCash != this.schema.modeCash.normal ) {
                console.log(this.resetPage);
                this.initCashControl();
                this.resetPage['cashManagement']();
            }

            if( modeOption.mode == 'dailyClearance' && this.sceneInitStatus['dailyClearance'] ) {
                this.resetPage['dailyClearance']();
            }

            if( modeOption.mode == 'report' && this.sceneInitStatus['report'] ) {
                this.initReport();
                // console.log(this.resetPage);
            }

            if( modeOption.modeFloorPlan == 'editTable' ) {
                obj.setActive( angular.element('.btn-edit-table') );
            } else {
                // place here to control the nav menu, avoid too much settings in controller
                // obj.modeNavMenu = obj.schema.modeNavMenu.normal;
            }

            // if( obj.isModeNavMenu() ) {
            //     obj.modeNavMenu = obj.schema.modeNavMenu.normal;
            // }


            if( modeOption.modeFloorPlan == 'order' ) {
                obj.setActive( angular.element('.btn-order') );
            }

            // console.log(modeOption);
            // console.log('test test');
            if( modeOption.modeMain === 'cashbox' ) {
                obj.checkPermission('cashbox', function () {
                    SocketFactory.emit('openCashBox', {}, function (r) {});
                }, function(){obj.showAlert('no_permission');});
            }

            //     this.switchMode2({modeCash:'normal'});

            // console.log(modeOption);
            for( var key in modeOption) {
                // console.log('mode type ' + key);
                // console.log('mode name ' + modeOption[key]);
                obj[key] = obj.schema[key][modeOption[key]];
                obj[key] = obj.schema[key][modeOption[key]];
            }

            if( typeof modeOption.modeFloorPlan != 'undefined' && modeOption.modeFloorPlan === 'order' ) {
                this.setActive( angular.element('.btn-order') );
            }

            // only really switch after login or direct or execute here, title would be changed
            this.currentPageTitle = this.pageTitle;
            // console.log(this.statusMessage);
            this.showStatusMessage( this.statusMessage );

            // if( typeof $event != 'undefined' ) {
            //     this.setActive( $event );
            // }
        }

        obj.switchMode2 = function( modeOption, requiredLogin, pageTitle, message, $event ) {
            // all mode changed here required login by default
            var requiredLogin = ( angular.isDefined(requiredLogin) ) ? requiredLogin : true,
                pageTitle = ( angular.isDefined(pageTitle) ) ? pageTitle : '',
                message = ( angular.isDefined(message) && message != '' ) ? uiLanguage[INFO.lang][lowerFirstLetter( message )] : uiLanguage[INFO.lang][pageTitle];

            this.pageTitle = uiLanguage[INFO.lang][pageTitle];
            this.statusMessage = message;
            // console.log(pageTitle);
            // this.showStatusMessage( message );

            if( requiredLogin && !obj.UserManager.isLogin ) {
                this.UserManager.checkLogin();
                obj.UserManager.LoginRedirectToMode2 = modeOption;
                obj.UserManager.LoginRedirectToCaller = $event;
            } else {
                this.goToMode2( modeOption, $event );
            }

            // console.log(arguments);
            // console.log("message " + message);
            // show status

            // if( angular.isDefined( message ) ) {
                // console.log('show message?');
            //     this.showStatusMessage( message );
            // }
        }


        // display mode organizing in consolidated functions, try to group here to see how to generalize it later
        // eg modeChecking( [{'modeItem': ['mode', 'normal']}, {'modeOrder': ['normal', 'detail', 'foodControl']}] )
        obj.modeChecking = function( option ) {
            // this = obj
            // console.log(option);

            var eachModeCheck = [], // for || checking
                displayCheck = true; // for && checking
            for( var i=0; i < option.length; i++) {
                // console.log('mode testing inside array');
                // console.log(option[i]);

                // console.log('------------------------------------------------');
                for( var key in option[i] ) {
                    for( var m=0; m < option[i][key].length; m++) {
                        // console.log('current mode type ' + key);
                        // console.log('current mode name ' + option[i][key][m]);
                        eachModeCheck[i] = eachModeCheck[i] || this[key] == this.schema[key][option[i][key][m]];
                        // console.log('this[key] ' + this[key] + ' vs ' + this.schema[key][option[i][key][m]]);
                        // console.log(this[key] == this.schema[key][option[i][key][m]]);
                    }
                }
                // console.log('------------------------------------------------');
                for( var c=0; c < eachModeCheck.length; c++ ) {
                    displayCheck = displayCheck && eachModeCheck[c];
                }
            }
            // obj.modeCheck = displayCheck;
            // return displayCheck;
            // console.log(option);
            // console.log(displayCheck); // check
            return displayCheck;
        }

        obj.modeCheckUserGroup = function( option ) {
            var option = angular.isUndefined( option ) ? 'list' : option;
            // MainService.mode == MainService.schema.mode.floorPlan
            return this.modeChecking([{mode: [option]}]);
        }

        // default: floorplan, tablelist
        obj.modeCheckBase = function( option ) {
            var option = angular.isUndefined( option ) ? ['floorPlan', 'tableList'] : [option];
            // MainService.mode == MainService.schema.mode.floorPlan
            return this.modeChecking([{mode: option}]);
        }
        obj.isMode = obj.modeCheckBase;
        obj.isNotMode = function() {
            return !obj.modeCheckBase.apply( obj, arguments );
        }

        obj.isModeNavMenu = function( mode ) {
          var mode = angular.isUndefined( mode ) ? ['normal'] : mode,
              mode = angular.isArray( mode ) ? mode : [mode];
              
          return obj.modeChecking.apply( obj, [[{modeNavMenu: mode}]] );
        }

        obj.isNotModeNavMenu = function() {
          return !obj.isModeNavMenu.apply( $scope, arguments );
        }

        obj.modeCheckOrder = function( option ) {
            var option = angular.isUndefined( option ) ? 'normal' : option;
            // {\'mode\': [\'order\']},\'modeOrder\': [\'normal\']}
            // MainService.mode == MainService.schema.mode.order && MainService.modeOrder == MainService.schema.modeOrder.normal
            // MainService.modeOrder === MainService.schema.modeOrder.transfer
            // MainService.modeOrder == MainService.schema.modeOrder.detail
            switch( option ) {
                default:
                    option = angular.isArray( option ) ? option : [option];
                    return this.modeChecking([{mode: ['order']}, {modeOrder: option}]);
                    break;
                case 'order':
                    return this.modeChecking([{mode: ['order']}, {modeOrder: ['normal', 'detail', 'specialMessage', 'transfer']}]);
                    break;
                case 'detailNormal':
                    return this.modeChecking([{mode: ['order']}, {modeOrder: ['normal', 'detail']}]);
                    break;
                case 'foodMenu':
                    return this.modeChecking([{mode: ['order']}, {modeOrder: ['normal', 'detail', 'foodControl']}]);
                    break;
                case 'all':
                    return this.modeChecking([ {mode: ['order']} ]);
                    break;
            }
        }

        obj.isModeOrder = obj.modeCheckOrder; // for better semantic
        obj.isNotModeOrder = function() {
            return !obj.isModeOrder.apply( obj, arguments );
        }; // for better semantic

        obj.modeCheckOrderItem = function( option ) {
            var option = angular.isUndefined( option ) ? ['normal'] : option;
                option = angular.isArray( option ) ? option : [option];

            // {\'mode\': [\'order\']},\'modeOrder\': [\'normal\']}
            // MainService.mode == MainService.schema.mode.order && MainService.modeOrder == MainService.schema.modeOrder.normal
            // MainService.modeOrder === MainService.schema.modeOrder.transfer
            // MainService.modeOrder == MainService.schema.modeOrder.detail
            // return this.modeChecking([{mode: ['order']}, {modeItem: [option]}]);
            switch( option ) {
                default:
                    return this.modeChecking([{mode: ['order']}, {modeItem: option}]);
                    break;
                case 'normalOption':
                    // MainService.modeItem != MainService.schema.modeItem.cancel && MainService.modeItem != MainService.schema.modeItem.modifier
                    return this.modeChecking([{mode: ['order']}, {modeItem: ['option', 'normal']}]);
                    break;
            }
        }

        obj.modeCheckOrderItem2 = function( option ) {
            var option = angular.isUndefined( option ) ? ['normal', 'option'] : option;
                option = angular.isArray( option ) ? option : [option];

            // {\'mode\': [\'order\']},\'modeOrder\': [\'normal\']}
            // MainService.mode == MainService.schema.mode.order && MainService.modeOrder == MainService.schema.modeOrder.normal
            // MainService.modeOrder === MainService.schema.modeOrder.transfer
            // MainService.modeOrder == MainService.schema.modeOrder.detail
            // return this.modeChecking([{mode: ['order']}, {modeItem: [option]}]);
            return this.modeChecking([{mode: ['order']}, {modeItem: option}]);
        }

        obj.isModeItem = obj.modeCheckOrderItem2;
        obj.isNotModeItem = function() {
           return !obj.modeCheckOrderItem2.apply( obj, arguments );
        };

        obj.modeCheckOrderItemDetail = function( option ) {
            var option = angular.isUndefined( option ) ? 'code' : option;
            return this.modeChecking([{mode: ['order']}, {modeItemDetail: [option]}]);
        }

        obj.isModeItemDetail = obj.modeCheckOrderItemDetail;
        obj.isNotModeItemDetail = function() {
            return !obj.modeCheckOrderItemDetail.apply( obj, arguments );
        }

        obj.modeCheckBill = function( option ) {
            switch( option ) {
                default:
                case 'all':
                    return this.modeChecking([{'mode': ['order']}, {'modeOrder': ['amendBill', 'searchBill', 'printBill', 'billing']}]); //all
                    break;
                case 'searchAmendBill':
                    return this.modeChecking([{'mode': ['order']}, {'modeOrder': ['amendBill', 'searchBill', 'billing']}]); // searchAmendBill
                    break;
                case 'printAmendBill':
                    return this.modeChecking([{'mode': ['order']}, {'modeOrder': ['amendBill', 'printBill', 'billing']}]); // printAmendBill
                    break;
                case 'billPrint':
                    return this.modeChecking([{'mode': ['order']}, {'modeOrder': ['printBill', 'billing']}]); // printAmendBill
                    break;
                case 'billOnly':
                    return this.modeChecking([{'mode': ['order']}, {'modeOrder': ['billing']}]); // printAmendBill
                    break;
                case 'summary':
                    return this.modeChecking([{'mode': ['order']}, {'modeOrder': ['billingSummary']}]); // printAmendBill
                    break;
                case 'searchBill':
                    return this.modeChecking([{'mode': ['order']}, {'modeOrder': ['searchBill']}]); // printAmendBill
                    break;
                case 'amendBill':
                    return this.modeChecking([{'mode': ['order']}, {'modeOrder': ['amendBill']}]); // printAmendBill
                    break;
            }
            // return this.modeChecking([{'mode': ['order']}, {'modeOrder': ['printBill', 'billing']}]); // printBill
        }

        obj.isModeBill = obj.modeCheckBill;
        obj.isNotModeBill = function() {
           return !obj.modeCheckBill.apply( obj, arguments );
        };

        // replaced by modeOrderMenu
        obj.modeCheckOrderFood = function() {
            // this.schema.modeItem.normal,
            // this.schema.modeItem.option
            // ],
            // condArr2 = [
            // this.schema.modeOrder.normal,
            // this.schema.modeOrder.detail,
            // this.schema.modeOrder.foodControl
            // ];
            // conditions
            // console.log(typeof this.modeChecking([{'mode': ['report']}]));
            return this.modeChecking([{'mode': ['order']}, {'modeItem': ['option', 'normal', 'optionFoodControl']}, {'modeOrder': ['foodControl', 'detail', 'normal', 'zeroItem']}]);
        }

        obj.isModeOrderMenu = function( option ) {
            option = option ? option : 'normal';
            // console.log('isModeOrderMenu / ' + typeof option);
            // console.log('isModeOrderMenu / ' + option);
            // obj.modeOrderMenu = obj.schema.modeOrderMenu.normal;
            return this.modeChecking([{'mode': ['order']}, {'modeOrderMenu': [option]}]);
        }
        obj.isNotModeOrderMenu = function() {
            return !obj.isModeOrderMenu.apply( obj, arguments );
        }

        obj.modeCheckFloor = function( option ) {
            return this.modeChecking([{'modeFloorPlan': [option]}]);
        }

        obj.isModeFloor = obj.modeCheckFloor;
        obj.isNotModeFloor = function() {
           return !obj.modeCheckFloor.apply( obj, arguments );
        };

        // obj.checkItemDeleteDisplay2  = function() {
        //     return this.modeChecking([{'mode': ['order']}, {'modeOrder': ['detail', 'normal']}]);
        // }

        /*obj.checkItemDeleteDisplay  = function() {
            var conditions1 = this.mode == this.schema.mode.order,
                conditions2 = this.modeOrder == this.schema.modeOrder.normal,
                conditions3 = this.modeOrder == this.schema.modeOrder.detail

            return (conditions1) && (conditions2 || conditions3);
        }*/

        /*obj.checkFoodMenuDisplay = function() {
            // conditions
            // MainService.modeItem != MainService.schema.modeItem.cancel && MainService.modeItem != MainService.schema.modeItem.modifier && MainService.modeOrder == MainService.schema.modeOrder.normal

            var condArr1 = [
                this.schema.modeItem.normal,
                this.schema.modeItem.option
                ],
                condArr2 = [
                this.schema.modeOrder.normal,
                this.schema.modeOrder.detail,
                this.schema.modeOrder.foodControl
                ];

            if( condArr1.indexOf(this.modeItem) >= 0 && condArr2.indexOf(this.modeOrder) >= 0 ) {
                // console.log('yes');
                return true;
            }

            // console.log(this.schema.modeItem.normal);
            // console.log(this.schema.modeItem.option);
            // console.log(this.schema.modeOrder.normal);
            // console.log(this.schema.modeOrder.detail);
        }*/

        // for switch between floor plan/order functions and main menu functions
        /*obj.resetAllSubMode = function() {
            this.modeOrder = {};
            this.modeItem = {};
            this.modeItemDetail = {};
            this.modeFloorPlan = {};
        }*/

        obj.switchModeFloorPlan = function (modeChange) {
            // console.log('called me');
            obj.UserManager.checkLogin();
            this.modeFloorPlan = modeChange;
            console.log("%%% modeFloorPlan mode: "+this.modeFloorPlan);
            console.log("%%% modeOrder mode: "+this.schema.modeOrder);
            // reset payment method to nothing then add cash as default in submitcart calculation
        }

        obj.printOrder = function ( tableNo, type ) {
            console.log('print '+type+' here');
            console.log({ "tableNo": tableNo, "action": type });
            console.log(obj.appliedDiscount);

            // console.log({ "tableNo": tableNo, "action": type, discount: obj.appliedDiscount, "user": obj.UserManager.staffCode, printer: printerName });
            // alert(userConfig.page.printer)
            SocketFactory.emit('print', { "tableNo": tableNo, "action": type, discount: obj.appliedDiscount, "user": obj.UserManager.staffCode, printer: userConfig.page.printer });
        }

        obj.findItemFromFood = function(itemCode) {
            var found = {};
            $.each(obj.food.category, function (catIndex, cat) {
                $.each(cat.items, function (itemIndex, item) {
                    if (item.code == itemCode) {
                        $.extend(true, found, item);
                    }
                })
            });
            return found;
        }

        obj.Cart = {
            cartList: [], orderList: [], splitListRPrice: 0, splitListRServiceCharge: 0, splitListRTotalPrice: 0, splitListRNoofpeople: 0, splitListLPrice: 0, splitListLServiceCharge: 0, splitListLTotalPrice: 0, splitListLNoofpeople: 0, splitListRIndex: 1, splitListLIndex: 0, allSplitList: [], tableNo: '', noOfPeople: '', totalPrice: 0, serviceCharge: 0, price: 0,currentSelectedSplitIndex : 0,member:{}, refNo:'',
            currentOption: null, currentOptionIndex: -1, currentOptionList: {"items": []}, voidItemCount: uiLanguage[INFO.lang]['all'].toLowerCase(),
            cancelPackage: function () {
                obj.modeItem = obj.schema.modeItem.normal;
                obj.Cart.cartList.pop();
            },
            assignMemberToOrder: function(member){
                console.log(member);
                obj.Cart.member = member;
                $mdDialog.cancel();
            },
            addModifier: function (m, $event) {
                console.log(m);
                console.log(obj.Cart.cartList);
                $.each(obj.Cart.cartList, function (idx, item) {
                    var optionIsEdit = false;
                    if (item.option != undefined) {
                        $.each(item.option, function(idx, optionList) {
                            $.each(optionList.items, function(oidx, option) {
                                if (option.isEdit && option.code != 'O9999') {
                                    optionIsEdit = true;
                                    console.log(option);
                                    var foundModifierIdx = -1;
                                    $.each(option.modifier, function(midx, mitem) {
                                        if (mitem.code == m.code) {
                                            foundModifierIdx = midx
                                        }
                                    })
                                    if (foundModifierIdx == -1) {
                                        option.modifier.push(m);
                                    }
                                    else {
                                        option.modifier.splice(foundModifierIdx, 1);
                                    }
                                }
                            });
                            //alert(option.modifier.length)
                        });
                    }
                    if (!optionIsEdit) {
                        if (item.isEdit) {
                            var foundModifierIdx = -1;
                            $.each(item.modifier, function (midx, mitem) {
                                if (mitem.code == m.code) {
                                    foundModifierIdx = midx
                                }
                            })
                            if (foundModifierIdx == -1) {
                                item.modifier.push(m);
                            } else {
                                item.modifier.splice(foundModifierIdx, 1);
                            }
                        }
                    }
                });

                if ($event) {
                    $event.stopPropagation();
                }
            },
            deleteModifier: function(modifierList, mIdx) {
                modifierList.splice(mIdx, 1);
            },
            addKitchenMsg: function (k, $event) {
                // console.log(m);
                console.log('addKitchenMsg', [obj.Cart.cartList, obj.Cart.orderList]);
                $.each([obj.Cart.cartList, obj.Cart.orderList], function(i, list) {
                    $.each(list, function(idx, item) {
                        console.log('addKitchenMsg', item);
                        var optionIsEdit = false;
                        if (item.option != undefined) {
                            $.each(item.option, function (idx, optionList) {
                                $.each(optionList.items, function (oidx, option) {
                                    if (option.isEdit && option.code != 'O9999') {
                                        optionIsEdit = true;
                                        console.log(option);
                                        var foundModifierIdx = -1;
                                        $.each(option.kitchenMsg, function (midx, mitem) {
                                            if (mitem.code == k.code) {
                                                foundModifierIdx = midx
                                            }
                                        })
                                        if (foundModifierIdx == -1) {
                                            option.kitchenMsg.push(k);
                                        }
                                        // else {
                                        //     option.kitchenMsg.splice(foundModifierIdx, 1);
                                        // }
                                    }
                                });
                                //alert(option.kitchenMsg.length)
                            });
                        }
                        if (!optionIsEdit) {
                            if (item.isEdit) {
                                var foundIdx = -1;
                                $.each(item.kitchenMsg, function (kidx, kitem) {
                                    if (kitem.code == k.code) {
                                        foundIdx = kidx
                                    }
                                })
                                if (foundIdx == -1) {
                                    item.kitchenMsg.push(k);
                                }
                                // else {
                                //     item.kitchenMsg.splice(foundIdx, 1);
                                // }
                            }
                        }
                    });
                });
            },
            deleteKitchenMsg: function (kitchenMsgList, kIdx, $event) {
                kitchenMsgList.splice(kIdx, 1);
                if ($event) $event.stopPropagation();
            },
            clearKitchenMsg: function() {
                $.each([obj.Cart.cartList, obj.Cart.orderList], function(i, list) {
                    $.each(list, function(idx, item) {
                        console.log('clearKitchenMsg', item);
                        if (item.isEdit)
                            item.kitchenMsg = [];
                        if (item.option != undefined) {
                            $.each(item.option, function (idx, optionList) {
                                $.each(optionList.items, function (oidx, option) {
                                    if (option.isEdit) {
                                        option.kitchenMsg = [];
                                    }
                                });
                            });
                        }
                    });
                });
            },
            checkItemStock: function (item, qty, prevQty) {
                prevQty = prevQty ? prevQty : 0;
                var totalqty = qty == undefined ? 1 : qty;
                console.log('totalqty:' + totalqty);
                console.log('prevQty:' + prevQty);
                totalqty -= prevQty;
                var isValid = true;
                console.log('totalqty:' + totalqty);
                $.each(obj.Cart.cartList, function (idx, citem) {
                    if (citem.getQty() >= 0 && item.code == citem.code) {
                        totalqty += citem.getQty();
                    }
                });
                console.log('totalqty:'+totalqty);
                $.each(obj.stock.list, function (idx, sitem) {
                    if (sitem.code == item.code) {
                        if (sitem.stock <= 0) {
                            obj.showAlert('no_stock');
                            isValid = false;
                            return;
                        } else if (sitem.stock < totalqty) {
                            var message = uiLanguage[INFO.lang]['stock_reminder'].replace("[TOTAL_QTY]", totalqty).replace("[CURRENT_STOCK]", sitem.stock);

                            obj.showAlert(message);
                            isValid = false;
                            return;
                        }
                    }
                });
                return isValid;

            },
            addItem: function (item) {
                console.log('obj.Cart  / addItem');
                console.log('item.voidIndex', item.voidIndex);
                var newItem = $.extend(true, {}, item , obj.schema.baseCartItem, {"voidIndex": item.voidIndex});
                console.log('newItem.voidIndex', newItem.voidIndex);
                if (item.voidIndex > 0) {
                    this.cartList.push(newItem);
                    newItem.editItem(null, false);
                    return newItem; // void item
                }
                newItem.init();
                if (obj.modeOrder != obj.schema.modeOrder.foodControl) {
                    // not food control
                    if (!obj.Cart.checkItemStock(newItem,1)) {
                        // check cart stock
                        return false;
                    } else {
                        this.cartList.push(newItem);
                        newItem.editItem(null, false);


                        return newItem;
                    }
                } else if (obj.modeOrder == obj.schema.modeOrder.foodControl) {
                    // food control
                    var foundItem = null;
                    $.each(obj.stock.list, function (idx, item) {
                        if (item.code == newItem.code) {
                            foundItem = item;
                        }
                    });
                    if (foundItem != null) {
                        foundItem.qty = '';
                        foundItem.editItem();
                        $timeout(function () {
                            obj.scrollToBottom();
                        }, 10);
                        return foundItem;
                    } else {
                        newItem.qty = '';
                        obj.stock.list.push(newItem);
                        newItem.editItem();
                        $timeout(function () {
                            obj.scrollToBottom();
                        }, 10);
                        return newItem;
                    }
                }
            },
            loadOption: function(item) {
                obj.modeItem = obj.schema.modeItem.optionFoodControl;
                // obj.Cart.currentOption = item.option;

                // show all option in item list
                // console.log(item.option);
                console.log('prepare extended option');

                var allOptions = [],
                    tmpOption = {},
                    optionCodeList = [];
                $.each(item.option, function(idx, optionList){
                    if( $.inArray(optionList.optionGroupCode, optionCodeList) === -1 ) {
                        optionCodeList.push( optionList.optionGroupCode );
                        angular.forEach(optionList.items, function(item) {
                            // tmpOption = $.extend(true, {}, item, obj.schema.baseItem);
                            allOptions.push(item);
                        });
                    }
                })
                this.optionControlList = allOptions;
                console.log(allOptions);

                // this.selectedOption = [];
                // $.each(item.option, function (idx, optionList) {
                //     console.log(optionList.minQty);
                //     var fillOption = $.extend(true, {}, obj.schema.unselectOption, obj.schema.baseItem, { qty: 1, _qty: 1 });
                //     // if (idx == 0 && i == 0) {
                //     //     fillOption.isEdit = true;
                //     // }
                //     console.log(fillOption);
                //     var exists = false;
                //     angular.forEach(optionList.items, function(item) {
                //         if (item.optionCode === fillOption.optionCode) {
                //             exists = true;
                //         }
                //     });
                //     if (!exists) optionList.items.push(fillOption);
                //     console.log(this.currentOptionList);
                // });

                // var that = this;
                // var selectQty = this.selectQty ? this.selectQty : 0;
                // var unselectedCount = 0;
                // var callback = function(optionIndex, optionItem) {
                //     console.log("callback", that);
                //     if (that.currentOptionIndex == -1) {
                //         that.currentOptionIndex = optionIndex;
                //         that.currentOptionList = that.currentOption[optionIndex];
                //         optionItem.isEdit = true;
                //     }
                // }

                // $.each(this.currentOption, function(optionIndex, optionList) {
                //     $.each(optionList.items, function(optionItemIndex, optionItem) {
                //         // console.log(12345, optionItem.qty, optionItem.code);
                //         if (optionItem.qty > 0 && optionItem.code === 'O9999') {
                //             callback(optionIndex, optionItem);
                //             unselectedCount++;
                //         }
                //     });
                // });
                console.log(item);
                
            },
            cartListSelectOption: function ($event, idx, editItem, optionList, editOption) {
                if (editItem.voidIndex > -1) return;
                if (!editItem.isEdit) editItem.isEdit = true;
                angular.forEach(this.cartList, function(item) {
                    if (item != editItem) item.isEdit = false;
                    angular.forEach(item.option, function (option) {
                        if (option != editOption) {
                            option.isEdit = false;
                        }
                        angular.forEach(option.items, function (optItem) {
                            if (optItem != editOption)
                                optItem.isEdit = false;
                        });
                    });
                });
                //angular.forEach(editItem.option, function (option) {
                //    if (option != editOption) {
                //        option.isEdit = false;
                //    }
                //    angular.forEach(option.items, function(optItem) {
                //        if (optItem != editOption)
                //        optItem.isEdit = false;
                //    });
                //});
                // optionList.isEdit = !optionList.isEdit;
                editOption.isEdit = !editOption.isEdit;
                this.editOption = editOption.isEdit;
                if (editOption.isEdit) {

                    this.editOptionCode = editOption.code;

                    if (obj.modeItem != obj.schema.modeItem.modifier) {
                        //this.currentOption = option;
                        this.currentOptionIndex = idx;
                        this.currentOptionList = optionList;
                        obj.modeItem = obj.schema.modeItem.option;
                    }

                    if ($event) {
                        $event.stopPropagation();
                    }
                }
                else {
                    // this.currentOption = null;
                    this.editOptionCode = '';
                    this.currentOptionList = [];
                    this.currentOptionIndex = -1;
                    this.resetEditItem();
                    obj.modeItem = obj.schema.modeItem.normal;
                }
            },
            showNextOption: function (next) {
                console.log("showNextOption", next, this.currentOptionIndex, this.currentOption);
                // console.log("currentOptionIndex", this.currentOptionIndex, this.currentOption); // index option on the cart list

                if (this.currentOptionIndex > -1) {
                    // if there is anything selected, run here
                    // var selectQty = 0;
                    var selectQty = this.selectQty ? this.selectQty : 0;
                    $.each(this.currentOptionList.items, function(optionItemIndex, optionItem) {
                        if (optionItem.qty > 0) {
                            selectQty += optionItem.qty;
                        };
                    });
                    console.log(selectQty, this.currentOptionList);

                    if (next || !(selectQty < this.currentOptionList.maxQty)) {
                        this.currentOptionIndex++;
                        if (this.currentOptionIndex < this.currentOption.length) {
                            this.currentOptionList = this.currentOption[this.currentOptionIndex];
                            var selected = false;
                            $.each(this.currentOptionList.items, function(optionItemIndex, optionItem) {
                                if (optionItem.qty > 0 && optionItem.code === 'O9999') {
                                    optionItem.isEdit = true;
                                    selected = true;
                                };
                            });
                            if (!selected) {
                                obj.modeItem = obj.schema.modeItem.normal;
                            }
                        }
                        else {
                            obj.modeItem = obj.schema.modeItem.normal;
                        }
                    }
                    // this.selectQty = selectQty;
                }
                else {
                    // if there is nothing selected, run here
                    var that = this;
                    var selectQty = this.selectQty ? this.selectQty : 0;
                    var unselectedCount = 0;
                    var callback = function(optionIndex, optionItem) {
                        console.log("callback", that);
                        if (that.currentOptionIndex == -1) {
                            that.currentOptionIndex = optionIndex;
                            that.currentOptionList = that.currentOption[optionIndex];
                            optionItem.isEdit = true;
                        }
                    }


                    $.each(this.currentOption, function(optionIndex, optionList) {
                        $.each(optionList.items, function(optionItemIndex, optionItem) {
                            console.log(12345, optionItem.qty, optionItem.code);
                            if (optionItem.qty > 0 && optionItem.code === 'O9999') {
                                // obj.Cart.currentOptionIndex = optionIndex;
                                // obj.Cart.currentOptionList = obj.Cart.currentOption[optionIndex];
                                // optionItem.isEdit = true;
                                callback(optionIndex, optionItem);
                                // obj.modeItem = obj.schema.modeItem.option;
                                unselectedCount++;
                            }
                        });
                    });
                    // this.currentOptionIndex = 0;
                    // this.currentOptionList = this.currentOption[this.currentOptionIndex];
                    // $.each(this.currentOptionList.items, function (optionItemIndex, optionItem) {
                    //     if (optionItem.qty > 0) {
                    //         optionItem.isEdit = true;
                    //     }
                    // });
                }

                // console.log(unselectedCount);
                // all selected, and master/parent item is clicked
                if( this.currentOptionIndex == -1 && unselectedCount == 0) {
                    obj.modeItem = obj.schema.modeItem.normal;
                }
            },
            addOption : function (option) {
                // console.log(option)
                // var selectQty = 0;
                var selectQty = this.selectQty ? this.selectQty : 0;

                console.log("addOption currentOptionIndex", this.currentOptionIndex);
                console.log("addOption currentOptionList", this.currentOption);
                var selectOption = false;
                if (option.code != 'O9999') {
                    selectOption = true;
                }
                $.each(this.currentOptionList.items, function (optionItemIndex, optionItem) {
                    if (selectOption) {
                        if (optionItem.code == 'O9999') {
                            optionItem.qty = 0;
                            optionItem.isEdit = false;
                        } else {
                            selectQty += optionItem.qty;
                        }
                    } else if (optionItem.qty > 0) {
                        optionItem.qty = 0;
                    };
                });
                if (selectQty >= this.currentOptionList.maxQty) {
                    var eoc = this.editOptionCode;
                    $.each(this.currentOptionList.items, function (optionItemIndex, optionItem) {
                        if (eoc != '' && eoc == optionItem.code) {
                            optionItem.qty = 0;
                        } else if (eoc == '') {
                            optionItem.qty = 0;
                        }
                    });
                }
                var isAdd = true;
                // if (selectQty >= this.currentOptionList.minQty) {
                //     if (option.code == 'O9999') {
                //         isAdd = false;
                //     }
                // }
                if (isAdd) {
                    $.each(this.currentOptionList.items, function (optionItemIndex, optionItem) {
                        if (option.code == optionItem.code) {
                            option.qty += 1;
                            option.isEdit = false;
                        }
                    });
                    this.showNextOption();
                } else {
                    this.showNextOption(true);
                }

                // this.selectQty = selectQty;
                //console.log(obj.Cart.orderList);
            },
            selectOption: function( $event, idx, option, item ) {
                console.log(idx);
                console.log(option);
                console.log(item.option);
                console.log('MMMMMMMMMMMMMMMMMMMMMMMMM');

                obj.modeItem = obj.schema.modeItem.option;
                // console.log(idx);
                // console.log(option);
                 this.currentOption = item.option;
                 this.currentOptionIndex = 0;
                 this.currentOptionList = this.currentOption[this.currentOptionIndex];

                // show first option
                // this.showNextOption();
                if ($event) {
                    $event.stopPropagation();
                }
            },

            // deprecated
            cancelOrder: function ( option ) {
                this.cancelOrder2(arguments)
                /*console.log('can u see me?');
                console.log(option);
                var option = typeof option === 'object' ? option : {
                    modeOrder: false,
                    modeItem: false,
                    modeBase: true
                };
                var choices = $.extend(true, {}, option);
                console.log(choices);

                if(choices.modeOrder) {
                    console.log('restore modeOrder');
                    switch(obj.modeOrder) {
                        case obj.schema.modeOrder.normal:
                            obj.mode = obj.schema.mode.floorPlan;
                            break;
                        case obj.schema.modeOrder.amendBill:
                            obj.modeOrder = obj.schema.modeOrder.searchBill;
                            break;
                        case obj.schema.modeOrder.foodControl:
                            obj.modeOrder = obj.schema.modeOrder.normal;
                            break;
                    }
                }


                if(choices.modeItem) {
                    console.log('restore modeItem');
                    console.log(obj.modeItem);
                    console.log(obj.schema.modeItem.option);
                    switch(obj.modeItem) {
                        case obj.schema.modeItem.option:
                            // console.log('mode restore 2');
                            obj.modeItem = obj.schema.modeItem.normal;
                            break;
                    }
                }


                if(choices.mode) {
                    console.log('restore mode base');
                    obj.mode = obj.schema.mode.floorPlan;
                }



                // if (obj.modeOrder == obj.schema.modeOrder.amendBill) {
                //     obj.modeOrder = obj.schema.modeOrder.searchBill;

                // }else if (obj.modeOrder == obj.schema.modeOrder.foodControl) {
                //     obj.modeOrder = obj.schema.modeOrder.normal;
                // } else {
                //     obj.mode = obj.schema.mode.floorPlan;
                // }*/
            },
            cancelOrder2: function ( option ) {
                if( obj.modeCheckOrderItem('option') ) {
                    console.log('1');
                    obj.switchMode2({modeItem:'normal'});
                    return;
                }

                if( obj.modeCheckOrder('normal') && obj.modeCheckOrderItem('normal') ) {
                    console.log('2');
                    obj.switchMode2({mode:'floorPlan'});
                    return;
                }

                if( obj.modeCheckOrder('specialMessage')) {
                    obj.switchMode2({modeOrder:'normal'});
                    return;
                }

                if( obj.modeCheckBill('amendBill') ) {
                    console.log('3');
                    obj.switchMode2({modeOrder:'searchBill'});
                    return;
                }

                if( obj.modeCheckOrder('foodControl') ) {
                    console.log('4');
                    obj.switchMode2({mode:'main'});
                    return;
                }

                console.log('default cancel');
                // default
                obj.mode = obj.schema.mode.floorPlan;
                return;

                // if (obj.modeOrder == obj.schema.modeOrder.amendBill) {
                //     obj.modeOrder = obj.schema.modeOrder.searchBill;

                // }else if (obj.modeOrder == obj.schema.modeOrder.foodControl) {
                //     obj.modeOrder = obj.schema.modeOrder.normal;
                // } else {
                //     obj.mode = obj.schema.mode.floorPlan;
                // }
            },
            voidItem: function (voidRemark) {
                var that = this;
                var resultItemList = [];
                obj.pendingVoidItem = [];
                $.each(that.orderList, function (idx, item) {
                    if (item.isEdit && item.getQty() > 0) {
                        var finalCheckQty = item.getQty();
                        $.each(obj.Cart.orderList, function (checkidx, checkItem) {
                            console.log(checkItem.voidIndex);
                            if (checkItem.voidIndex == item.index) {
                                finalCheckQty += checkItem.getQty();
                            }
                        });
                        $.each(obj.Cart.cartList, function (checkidx, checkItem) {
                            if (checkItem.voidIndex == item.index) {
                                finalCheckQty += checkItem.getQty();
                            }
                        });
                        if (finalCheckQty > 0) {
                            var cloneItem = {};
                            angular.copy(item, cloneItem);
                            var _voidqty = obj.Cart.voidItemCount;
                            if (String(_voidqty).toLowerCase() == "all" || String(_voidqty) == "所有") {
                                _voidqty = finalCheckQty;
                            } else if (_voidqty > finalCheckQty) {
                                _voidqty = finalCheckQty;
                            }
                            cloneItem.qty = _voidqty * -1
                            cloneItem.voidIndex = item.index;
                            cloneItem.voidRemark = voidRemark;
                            resultItemList.push(cloneItem);
                            //resultItem = obj.Cart.addItem(cloneItem);
                            //resultItem.qty = _voidqty * -1
                            //resultItem.voidIndex = item.index;
                            //resultItem.voidRemark = voidRemark;
                        }
                    }
                });
                console.log(resultItemList)
                if (resultItemList.length == 0) {
                    obj.showAlert('please_select_food');
                    return;
                }


                obj.checkPermission('voidItem', function () {
                    for (var i = 0; i < resultItemList.length; i++) {
                        var oItem = {};
                        angular.copy(resultItemList[i], oItem);

                        var rItem = obj.Cart.addItem(resultItemList[i]);
                        rItem.qty = oItem.qty;
                        rItem.voidIndex = oItem.voidIndex;
                        rItem.voidRemark = oItem.voidRemark;
                    }
                    obj.Cart.resetEditItem();
                    obj.modeItem = obj.schema.modeItem.normal;
                    obj.modeMultiSelectItem = false;
                    $timeout(function () {
                        obj.scrollToBottom();
                    }, 10);
                }, function () {
                    // pop up username and password
                    // simon
                    obj.pendingVoidItem = resultItemList;
                    if (angular.element('md-dialog[aria-label="Manager Login"]').scope() == undefined) {
                        $mdDialog.show({
                            templateUrl: 'tmpl/voidItemApproval.html'
                        })
                    } else {
                        $mdDialog.hide();
                    }
                });


            },
            resetEditItem: function () {
                $.each(obj.Cart.orderList, function (i, v) {
                    v.isEdit = false;
                    if (v.option != undefined)
                    $.each(v.option, function(k, optionList){
                        $.each(optionList.items, function(l, optionItem) {
                            optionItem.isEdit = false;
                        })
                    });
                });
                $.each(obj.Cart.cartList, function (i, v) {
                    v.isEdit = false;
                    if (v.option != undefined)
                    $.each(v.option, function(k, optionList){
                        $.each(optionList.items, function(l, optionItem) {
                            optionItem.isEdit = false;
                        })
                    });
                });
                $.each(obj.stock.list, function (i, v) { v.isEdit = false; });
            },
            changeItemQty: function (qty) {
                console.log('changeItemQty');
                if (obj.modeOrder == obj.schema.modeOrder.foodControl) {
                    obj.stock.updateStock(qty);
                } else {
                    $.each(obj.Cart.cartList, function (i, v) {
                        if (v.isEdit && v.qty > 0) {
                            if (obj.Cart.checkItemStock(v, qty, v.getQty())) {
                                console.log(v);
                                v.qty = qty;
                            } else {
                                return;
                            }
                        }
                    });
                }
                obj.modeItem = obj.schema.modeItem.normal;
            },
            init: function () {
                this.tableNo = '';
                this.member = {};
                this.cartList = [];
                this.orderList = [];
                this.couponObj = [];
                this.noOfPeople = 0;
                this.member = {};
                this.refNo = {};
                this.currentOption = null;
                this.currentOptionIndex = -1;
                this.currentOptionList = {"items": []};
                obj.appliedPaymentMethod = [];
                obj.appliedDiscount = [];
            }
        };
        obj.pendingVoidItem = [];
        obj.showPendingVoidItemList = function () {
            var text = "";

            for (var i = 0; i < obj.pendingVoidItem.length; i++) {
                var item = obj.pendingVoidItem[i];
                text += item.getName() + ' ' + (item.qty * -1) + ' ' + item.voidRemark + '\n';
            }

            console.log(text);
            return text;
        }

        obj.scrollToBottom = function (element) {
            console.log('scrollToBottom');
            var target = element ? element : "order-scene .order-list-wrapper";
            var maxScrollable = angular.element(target)[0].scrollHeight - angular.element(target).height();
            angular.element( target ).scrollTop( maxScrollable );
        }

        // food control -> cancel food control
        // billing mode, amend bill -> submit bill
        obj.submitCart = function () {
            console.log('obj.mode beginning' );
            console.log(obj.mode );
            //alert(obj.modeOrder);
            if (obj.modeOrder == obj.schema.modeOrder.foodControl) {
                obj.modeOrder = obj.schema.modeOrder.normal;
            } else
                if (obj.modeOrder == obj.schema.modeOrder.billing || obj.modeOrder == obj.schema.modeOrder.amendBill) {
                    var priceObj = obj.calcPrice([obj.Cart.cartList, obj.Cart.orderList], obj.Cart.couponObj);

                    // needle
                    // alert(userConfig.page.printer)
                    // return;

                    this.checkPaymentMethod(); // will preset payment method if nothing is selected

                    // calculate total price for verification
                    var totalAmountOfAllPaymentMethod = 0;
                    for(var i=0; i < this.appliedPaymentMethod.length; i++ ) {
                        console.log(this.appliedPaymentMethod[i].amount);
                        console.log(this.appliedPaymentMethod[i]);
                        totalAmountOfAllPaymentMethod += this.appliedPaymentMethod[i].amount;
                    }
                    this.remainder(); // update remainder in case it is default

                    something = this.appliedPaymentMethod; // global debug needle
                    console.log('verification:');
                    console.log('total amount ' + priceObj.totalPrice);
                    console.log('sum of method ' + totalAmountOfAllPaymentMethod);

                    // if (obj.inputPrice < priceObj.totalPrice) {
                    //if (totalAmountOfAllPaymentMethod < priceObj.totalPrice) {
                    if (totalAmountOfAllPaymentMethod < obj.Cart.totalPrice) {
                        obj.showAlert('please_set_amount');
                        return;
                    }
                    // if (obj.paymentMethod == undefined) {
                    if (obj.appliedPaymentMethod.length === 0) {
                        obj.showAlert('please_select_payment_method');
                        return;
                    }
                    console.log(obj.appliedPaymentMethod);
                    console.log(obj.Cart.couponObj);
                    var postData = {}

                    postData = {
                        "user": {
                            "$value": obj.UserManager.staffCode
                        },
                        "tableNum": {
                            "$value": obj.Cart.tableNo
                        },
                        "peopleNum": {
                            "$value": obj.Cart.noOfPeople
                        },
                        "price": {
                            "$value": priceObj.price
                        },
                        "payAmount": {
                            "$value": obj.inputPrice
                        },
                        "payMethod": {
                            "$value": obj.appliedPaymentMethod
                        },
                        "discountMethod": {
                            "$value": obj.Cart.couponObj
                        },
                        "remainings": {
                            "$value": priceObj.remainings
                        },
                        "serviceCharge": {
                            "$value": priceObj.serviceCharge
                        },
                        "refNo": {
                            "$value": obj.Cart.refNo
                        },
                        "printer": {
                            "$value": userConfig.page.printer
                        }
                    };
                    //if (obj.Cart.member != null) {
                    //    postData.member = {};
                    //    postData.member.$value = obj.Cart.member;
                    //}
                    //alert(obj.inputPrice)

                    var socketAction = 'billTableOrder';
                    if (obj.modeOrder == obj.schema.modeOrder.amendBill) {
                        socketAction = 'amendBill';
                        postData = {
                            "user": {
                                "$value": obj.UserManager.staffCode
                            },
                            "payMethod": {
                                "$value": obj.appliedPaymentMethod
                            },
                            "refNo": {
                                "$value": obj.Cart.refNo
                            },
                            "printer": {
                                "$value": userConfig.page.printer
                            }
                        };
                    }
                    console.log(postData);
                    SocketFactory.emit(socketAction, postData, function (r) {
                        r = JSON.parse(r);
                        console.log(r);
                        console.log(r.refNo);
                        if (r.result === "OK") {

                            if (socketAction == 'amendBill') {
                                obj.modeOrder = obj.schema.modeOrder.searchBill;
                            } else {
                                obj.lastBillReference = r.refNo;
                                obj.lastTableCode = r.tableCode;
                                obj.lastTableSubCode = r.tableSubCode;
                                // jump to summary page instead of floor plan first
                                obj.modeOrder = obj.schema.modeOrder.billingSummary;
                            }
                            // obj.mode = obj.schema.mode.floorPlan;
                            console.log('obj.mode ' );
                            console.log(obj.mode );
                        }
                    });
                }
            else {
                var order = { item: [] };
                angular.forEach(obj.Cart.cartList, function (item) {
                    console.log('item');
                    console.log(item);
                    var itemType = "I"
                    if (item.type) {
                        if (item.type == "T") {
                            itemType = "T";
                        }
                    }
                    order.item.push({
                        code: [item.code], qty: [item.qty], unitPrice: [item.getUnitPrice() * 100], voidIndex: [item.voidIndex], voidRemark: [item.voidRemark], type: [itemType], desc1: [item.getName()]
                        , customName: [item.customName] ,voidApproveStaff: [item.voidApproveStaff],
                    });
                    if (item.option != undefined && item.option.length != 0) {
                        $.each(item.option, function (optionIndex, option) {
                            $.each(option.items, function (optionItemIndex, optionItem) {
                                if (optionItem.qty > 0) {
                                    order.item.push({
                                        desc1: [optionItem.getName()],
                                        code: [optionItem.code],
                                        qty: [optionItem.getQty()],
                                        unitPrice: [optionItem.getUnitPrice() * 100],
                                        voidIndex: [optionItem.voidIndex],
                                        voidRemark: [optionItem.voidRemark],
                                        voidApproveStaff: [optionItem.voidApproveStaff],
                                        type: "O"
                                    });
                                    $.each(optionItem.modifier, function (modifierIndex, modifier) {
                                        order.item.push({
                                            desc1: [modifier.label],
                                            code: [modifier.code],
                                            qty: [1],
                                            unitPrice: [0],
                                            voidIndex: [-1],
                                            voidRemark: [''],
                                            voidApproveStaff: [undefined],
                                            type: "M"
                                        });
                                    })
                                    $.each(optionItem.kitchenMsg, function (kitchenMsgIndex, kitchenMsg) {
                                        order.item.push({
                                            desc1: [kitchenMsg.label],
                                            code: [kitchenMsg.code],
                                            qty: [1],
                                            unitPrice: [0],
                                            voidIndex: [-1],
                                            voidRemark: [''],
                                            voidApproveStaff: [undefined],
                                            type: "K"
                                        });
                                    })
                                }
                            })
                        });
                    }
                    if (item.subitem != undefined && item.subitem.length != 0) {
                        $.each(item.subitem, function (subitemIndex, subitem) {
                            order.item.push({
                                desc1: [subitem.getName()],
                                code: [subitem.code],
                                qty: [subitem.getQty()],
                                unitPrice: [subitem.getUnitPrice() * 100],
                                voidIndex: [subitem.voidIndex],
                                voidRemark: [subitem.voidRemark],
                                voidApproveStaff: [subitem.voidApproveStaff],
                                type: "S"
                            });
                        });
                    }
                    if (item.modifier.length != 0) {

                        $.each(item.modifier, function (modifierIndex, modifier) {
                            order.item.push({
                                desc1: [modifier.label],
                                code: [modifier.code],
                                qty: [1],
                                unitPrice: [0],
                                voidIndex: [-1],
                                voidRemark: [''],
                                voidApproveStaff: [undefined],
                                type: "M"
                            });
                        })
                    }
                    if (item.kitchenMsg.length != 0) {

                        $.each(item.kitchenMsg, function (kitchenMsgIndex, kitchenMsg) {
                            order.item.push({
                                desc1: [kitchenMsg.label],
                                code: [kitchenMsg.code],
                                qty: [1],
                                unitPrice: [0],
                                voidIndex: [-1],
                                voidRemark: [''],
                                voidApproveStaff: [undefined],
                                type: "K"
                            });
                        })
                    }
                });
                //if (order.item.length > 0) {
                var postData = {
                    "action": "MDSaveTableOrder",
                    "user": {
                        "$value": obj.UserManager.staffCode
                    },
                    "tableNum": {
                        "$value": obj.Cart.tableNo
                    },
                    "peopleNum": {
                        "$value": obj.Cart.noOfPeople
                    },
                    "itemData": {
                        "json": {
                            "order": order
                        }
                    }
                };
                if (obj.Cart.member != null) {
                    postData.member = {};
                    postData.member.$value = obj.Cart.member;
                }
                console.log(order);
                SocketFactory.emit('saveTableOrder', postData, function (r) {
                    r = JSON.parse(r);
                    console.log(r);
                    if (r.result === "OK") {
                        obj.mode = obj.schema.mode.floorPlan;

                    } else {

                    }
                });
            }
        };

        obj.submitSplitTable = function () {
            var orders = [];
            $.each(obj.Cart.allSplitList, function (idx,list) {
                var order = { 'nop': obj.Cart.allSplitList[idx].nop, 'item': [] };
                $.each(obj.Cart.allSplitList[idx].item, function (index, item) {
                    console.log(item);
                    order.item.push({
                        code: [item.code], qty: [item.qty], unitPrice: [item.getUnitPrice() * 100], voidIndex: [item.voidIndex], voidRemark: [item.voidRemark], type: ['I'], desc1: [item.getName()],index:[item.index]
                    });
                    if (item.option != undefined && item.option.length != 0) {
                        $.each(item.option, function (optionIndex, option) {
                            $.each(option.items, function (optionItemIndex, optionItem) {
                                if (optionItem.qty > 0) {
                                    order.item.push({
                                        desc1: [optionItem.getName()],
                                        code: [optionItem.code],
                                        qty: [optionItem.getQty()],
                                        unitPrice: [optionItem.getUnitPrice() * 100],
                                        voidIndex: [optionItem.voidIndex],
                                        voidRemark: [optionItem.voidRemark],
                                        type: "S"
                                    });
                                }
                            })
                        });
                    }
                    if (item.modifier.length != 0) {

                        $.each(item.modifier, function (modifierIndex, modifier) {
                            order.item.push({
                                desc1: [modifier.label],
                                code: [modifier.code],
                                qty: [1],
                                unitPrice: [0],
                                voidIndex: [-1],
                                voidRemark: [''],
                                type: "M"
                            });
                        })
                    }
                });
                orders.push(order);
            });
            //if (order.item.length > 0) {
            var postData = {
                "action": "MDSaveTableOrder",
                "user": {
                    "$value": obj.UserManager.staffName
                },
                "tableNum": {
                    "$value": obj.Cart.tableNo
                },
                "peopleNum": {
                    "$value": obj.Cart.noOfPeople
                },
                "itemData": {
                    "json": {
                        "order": orders
                    }
                }
            };
            SocketFactory.emit('splitTableOrder', postData, function (r) {
                r = JSON.parse(r);
                console.log(r.result);
                if (r.result === "OK") {
                    obj.mode = obj.schema.mode.floorPlan;
                }
            });
        }

        obj.changeSplitList = function (index) {
            if (!(obj.Cart.currentSelectedSplitIndex === index)) {
                var targetIndex = obj.Cart.currentSelectedSplitIndex == obj.Cart.splitListLIndex ? obj.Cart.splitListRIndex : obj.Cart.splitListLIndex;
                if (obj.Cart.currentSelectedSplitIndex == obj.Cart.splitListLIndex) {
                    obj.Cart.splitListRIndex = index;
                    obj.Cart.splitListRNoofpeople = obj.Cart.allSplitList[index].nop;
                } else {
                    obj.Cart.splitListLIndex = index;
                    obj.Cart.splitListLNoofpeople = obj.Cart.allSplitList[index].nop;
                }
            }

        }

        obj.calcPrice = function (cartObjs,couponObj) {
            var price = 0;
            var priceWithoutDiscount = 0;
            var priceForSC = 0;
            for (var i = 0; i < cartObjs.length; i++) {
                var cartObj = cartObjs[i];
                for (var j = 0; j < cartObj.length; j++) {
                    price += cartObj[j].getTotalPrice();
                    priceForSC += cartObj[j].getPriceForSC();
                }
            }

            //console.log(price);
            //console.log('****************1');
            //console.log(couponObj);
            priceWithoutDiscount = price;
            var serviceCharge = (priceForSC / 10).toFixed(1);
            if (couponObj) {
                //alert(couponObj)
                for (var i = 0; i < couponObj.length; i++) {
                    var cobj = couponObj[i];
                    if (cobj.type == "percentage") {
                        cobj.price = price * (1 - cobj.amount)
                        price = parseFloat(parseFloat(price) * cobj.amount)
                    }
                }
            }

            //console.log(price);
            //console.log('****************2');

            var priceWithServiceCharge = price + parseFloat(serviceCharge);
            var r = (priceWithServiceCharge  % 1).toFixed(1);
            if (r > 0.4) {
                r = (1 - r).toFixed(1);
            } else if (r != 0) {
                r = '-' + r;
            }
            obj.remainder();

            console.log(price);
            console.log('****************');
            console.log(priceWithServiceCharge);
            console.log(r);
            return { serviceCharge: serviceCharge, remainings: r, totalPrice: Math.round(priceWithServiceCharge), price: price, priceWithoutDiscount: priceWithoutDiscount };
        }

        obj.removeSplitList = function (ev) {
            if (obj.Cart.allSplitList.length < 3) {
                obj.showAlert('group_reminder');
            } else {
                var lastgroup = obj.Cart.allSplitList[obj.Cart.allSplitList.length - 1];
                if (lastgroup.item.length != 0) {
                    var confirm = $mdDialog.confirm()
                   .parent(angular.element(document.body))
                   .title(uiLanguage[INFO.lang]['alert'])
                   .content(uiLanguage[INFO.lang]['group_delete_confirmation'])
                   // .content('Are you sure to delete the groups?')
                   .ok(uiLanguage[INFO.lang]['yes'])
                   .cancel(uiLanguage[INFO.lang]['cancel'])
                   .targetEvent(ev);
                    $mdDialog.show(confirm).then(function () {
                        //alert('You decided to get rid of your debt.');
                        //if()

                    }, function () {
                        //alert('You decided to keep your debt.');
                    });
                } else {
                    if (obj.Cart.splitListLIndex == obj.Cart.allSplitList.length - 1) {
                        if (obj.Cart.splitListRIndex == obj.Cart.allSplitList.length - 2) {
                            obj.Cart.splitListLIndex = obj.Cart.allSplitList.length - 3;
                        } else {
                            obj.Cart.splitListLIndex = obj.Cart.allSplitList.length - 2;
                        }
                    } else if (obj.Cart.splitListRIndex == obj.Cart.allSplitList.length - 1) {
                        if (obj.Cart.splitListLIndex == obj.Cart.allSplitList.length - 2) {
                            obj.Cart.splitListRIndex = obj.Cart.allSplitList.length - 3;
                        } else {
                            obj.Cart.splitListRIndex = obj.Cart.allSplitList.length - 2;
                        }
                    }
                    obj.Cart.allSplitList.pop();
                }
            }
        }

        obj.addSplitList = function () {
            var targetIndex = obj.Cart.currentSelectedSplitIndex == obj.Cart.splitListLIndex ? obj.Cart.splitListRIndex : obj.Cart.splitListLIndex;

            var lasttableno = obj.Cart.allSplitList[obj.Cart.allSplitList.length - 1].name;
            lasttableno = lasttableno.slice(0,-1);
            lasttableno += charArray[obj.Cart.allSplitList.length];

            obj.Cart.allSplitList[obj.Cart.allSplitList.length] = { 'name': lasttableno, 'nop': 1, 'item': [], 'onop': -1 };

            for (var i = 0; i < obj.Cart.allSplitList.length; i++) {
                if (obj.Cart.allSplitList[i].nop > 1) {
                    obj.Cart.allSplitList[i].nop = obj.Cart.allSplitList[i].nop - 1;
                    if (i == obj.Cart.splitListLIndex) {
                        obj.Cart.splitListLNoofpeople = obj.Cart.allSplitList[i].nop;
                    } else if (i == obj.Cart.splitListLIndex) {
                        obj.Cart.splitListRNoofpeople = obj.Cart.allSplitList[i].nop;
                    }
                    break;
                }
            }

            if (obj.Cart.currentSelectedSplitIndex == obj.Cart.splitListLIndex) {
                obj.Cart.splitListRIndex = obj.Cart.allSplitList.length - 1;
                obj.Cart.splitListRNoofpeople = 1;
            } else {
                obj.Cart.splitListLIndex = obj.Cart.allSplitList.length - 1;
                obj.Cart.splitListLNoofpeople = 1;
            }
        }

        obj.splitTableNo = function (tableNo) {

            if (!obj.UserManager.isLogin) {
                obj.UserManager.LoginRedirectToTable = tableNo;
                if (angular.element('md-dialog[aria-label="Login"]').scope() == undefined) {
                    $mdDialog.show({
                        templateUrl: 'tmpl/login.html'
                    })
                } else {
                    $mdDialog.hide();
                }
            } else {
                SocketFactory.emit('adminLoadTableOrder', { 'tableNum': { '$value': tableNo } }, function (r) {
                    console.log('adminLoadTableOrder r' + r)
                    r = JSON.parse(r);
                    console.log(r)
                    if (r.result === "OK") {
                        if (r.order[0].header.orderId == '')
                        {
                            obj.showAlert('no_order');
                        } else {
                            obj.Cart.orderList = [];
                            obj.Cart.splitListL = { 'name': '', 'nop': 0, 'item': [] };
                            obj.Cart.splitListR = { 'name': '', 'nop': 0, 'item': [] };
                            obj.Cart.splitListLIndex = 0;
                            obj.Cart.splitListRIndex = 1;
                            obj.Cart.currentSelectedSplitIndex = obj.Cart.splitListLIndex;
                            obj.Cart.allSplitList = [];
                            obj.Cart.allSplitList[0] = { 'name': tableNo + '_' + charArray[0], 'nop': 0, 'item': [], 'onop': -1 };
                            obj.Cart.allSplitList[1] = { 'name': tableNo + '_' + charArray[1], 'nop': 1, 'item': [], 'onop': -1 };
                            obj.mode = obj.schema.mode.order;
                            obj.modeOrder = obj.schema.modeOrder.split;
                            obj.modeMultiSelectItem = true;
                            obj.Cart.tableNo = tableNo;
                            for (var i = 0; i < r.order.length; i++) {
                                var order = r.order[i];
                                var itemlist = obj.Cart.allSplitList[i].item;
                                obj.Cart.allSplitList[i].nop = order.header.peopleNum;
                                obj.Cart.allSplitList[i].onop = order.header.peopleNum;
                                if (i == 0) {
                                    obj.Cart.splitListLNoofpeople = order.header.peopleNum;
                                } else if (i == 1) {
                                    obj.Cart.splitListRNoofpeople = order.header.peopleNum;
                                }
                                angular.forEach(order.item, function (v, k) {
                                    console.log(v);
                                    var item = $.extend(true, {}, obj.schema.baseSplitItem, v);
                                    if (item.type === "I") {
                                        item.init();
                                        if (v.voidIndex != undefined) {
                                            if (v.voidIndex != -1) {
                                                list[v.voidIndex].voidQty = v.qty;
                                            }
                                            item.voidRemark = v.voidRemark;
                                        }
                                        itemlist.push(item);
                                    } else if (item.type === "S") {
                                        if (itemlist[itemlist.length - 1].option == undefined)
                                            itemlist[itemlist.length - 1].option = [{ items: [] }];
                                        //obj.Cart.splitListL[obj.Cart.splitListL.length - 1].option[0].items.push(item);
                                        //obj.Cart.allSplitList[0] = [obj.Cart.allSplitList[0][obj.Cart.allSplitList[0].length - 1].option[0].items.push(item)];
                                        itemlist[itemlist.length - 1].option[0].items.push(item);
                                    } else if (item.type === "M") {
                                        var m = obj.food.modifiers.filter(function (matchItem) {
                                            return matchItem.code == item.code;
                                        });
                                        itemlist[itemlist.length - 1].push(m[0]);
                                    }
                                });
                            }
                        }
                        obj.splitListUpdatePrice();
                    }
                });
            }
            console.log('obj.mode ' + obj.mode);
        };

        obj.loadTableOrder = function (tableNo, tableMode) {
            obj.assignTableNo(tableNo, null, tableMode);
            /*
            SocketFactory.emit('adminLoadTableOrder', { 'tableNum': { '$value': tableNo } }, function (r) {
                console.log('adminLoadTableOrder r' + r)
                //alert(r)
                r = JSON.parse(r);
                console.log(r)
                if (r.result === "OK") {
                    if (r.order.length > 1) {
                        //alert(callback);
                        callback(r.order);
                    } else {
                        var order = r.order[0];
                        console.log(order);
                        obj.modeMultiSelectItem = false;
                        obj.modeOrder = obj.schema.modeOrder[tableMode];
                        obj.mode = obj.schema.mode.order;
                        // obj.modeOrder = obj.schema.modeOrder.normal;
                        console.log(obj.mode);
                        obj.Cart.tableNo = tableNo;
                        obj.Cart.noOfPeople = '';
                        obj.Cart.member = {};
                        //console.log(angular.element('input-keypad').scope());
                        obj.Cart.cartList = [];
                        obj.Cart.orderList = [];
                        obj.Cart.couponObj = [];
                        obj.appliedDiscount = [];
                        obj.appliedPaymentMethod = [];
                        obj.Cart.noOfPeople = order.header.peopleNum;
                        if (order.header.member != undefined) {
                            obj.Cart.member = $.extend(true, order.header.member, obj.schema.member);
                        }
                        angular.forEach(order.item, function (v, k) {
                            console.log(v);
                            //this.push(key + ': ' + value);
                            var item = $.extend(true, {}, obj.schema.baseItem, v);
                            if (item.type === "I") {
                                item.init();
                                console.log('v.voidIndex:' + v.voidIndex);
                                if (v.voidIndex != undefined) {
                                    if (v.voidIndex != -1) {
                                        //obj.Cart.orderList[v.voidIndex].voidQty = v.qty;
                                        $.each(obj.Cart.orderList, function (idx, vitem) {
                                            if (vitem.index == v.voidIndex) {
                                                vitem.voidQty += v.qty;
                                            }
                                        });
                                    }
                                    item.voidRemark = v.voidRemark;
                                }
                                obj.Cart.orderList.push(item);
                            } else if (item.type === "S") {
                                if (obj.Cart.orderList[obj.Cart.orderList.length - 1].option == undefined)
                                    obj.Cart.orderList[obj.Cart.orderList.length - 1].option = [{ items: [] }];
                                obj.Cart.orderList[obj.Cart.orderList.length - 1].option[0].items.push(item);
                            } else if (item.type === "M") {
                                var m = obj.food.modifier.filter(function (matchItem) {
                                    return matchItem.code == item.code;
                                });
                                item.modifier.push(m[0]);
                            }
                        });

                        for (var i = 0; i < order.discount.length; i++) {
                            obj.appliedDiscount.push({ 'coupon_alias': order.discount[i].discountCode, 'amount': order.discount[i].discountAmount, 'type': order.discount[i].discountType, 'price': order.discount[i].discountValue });
                            obj.Cart.couponObj = [{ 'coupon_alias': order.discount[i].discountCode, 'amount': order.discount[i].discountAmount, 'type': order.discount[i].discountType }];
                        }

                        console.log(order);
                        if (order.payment){
                            for (var i = 0; i < order.payment.length; i++) {
                                obj.appliedPaymentMethod.push({ 'method': order.payment[i].paymentType, 'amount': order.payment[i].paymentValue })
                            }
                        }

                        // print order if it is initiated by print order
                        if( obj.modeFloorPlan === obj.schema.modeFloorPlan.printBill ) {
                            console.log('please print bill');
                            // controlled by controller that show the discount page
                        }
                    }
                }
            });
            */
        }

        obj.assign2OrderList = function(order) {
            var lastInsertType = "",
                lastInsertIdx = 0,
                lastInsertOptIdx = -1;
            angular.forEach(order.item, function (v, k) {
                 var item = $.extend(true, {}, obj.schema.baseItem, v);
                //var item = $.extend(true, {}, obj.findItemFromFood(v.code), obj.schema.baseItem, v);
                console.log("index " + v.index, v.type, v);
                if (item.type === "I" || item.type === "T") {
                    item.init();
                    lastInsertOptIdx = -1;
                    if (v.voidIndex != undefined) {
                        if (v.voidIndex != -1) {
                            //obj.Cart.orderList[v.voidIndex].voidQty = v.qty;
                            $.each(obj.Cart.orderList, function (idx, vitem) {
                                if (vitem.index == v.voidIndex) {
                                    vitem.voidQty += v.qty;
                                }
                            });
                        }
                        item.voidRemark = v.voidRemark;
                    }
                    //console.log(item)
                    obj.Cart.orderList.push(item);
                } else if (item.type === "O") {
                    if (lastInsertIdx != item.index)
                        lastInsertOptIdx++;

                     //if (obj.Cart.orderList[obj.Cart.orderList.length - 1].option == undefined)
                     //    obj.Cart.orderList[obj.Cart.orderList.length - 1].option = [{ items: [] }];
                    //obj.Cart.orderList[obj.Cart.orderList.length - 1].option[0].items.push(item);
                    if (obj.Cart.orderList[obj.Cart.orderList.length - 1].option == undefined)
                        obj.Cart.orderList[obj.Cart.orderList.length - 1].option = [];
                    if (obj.Cart.orderList[obj.Cart.orderList.length - 1].option[lastInsertOptIdx] == undefined)
                        obj.Cart.orderList[obj.Cart.orderList.length - 1].option[lastInsertOptIdx] = { items: [] };
                     obj.Cart.orderList[obj.Cart.orderList.length - 1].option[lastInsertOptIdx].items.push(item);

                    //if (obj.Cart.orderList[obj.Cart.orderList.length - 1] != undefined) {
                    //    angular.forEach(obj.Cart.orderList[obj.Cart.orderList.length - 1].option[lastInsertOptIdx].items, function (optItem) {
                    //        if (optItem.code === item.code) {
                    //            $.extend(optItem, item);
                    //        }
                    //    });
                    //}
                } else if (item.type === "S") {
                    angular.forEach(obj.Cart.orderList[obj.Cart.orderList.length - 1].subitem, function (subitem) {
                        if (subitem.code === item.code) {
                            $.extend(subitem, item);
                        }
                    });
                } else if (item.type === "M") {
                    var m = obj.food.modifiers.filter(function (matchItem) {
                        return matchItem.code == item.code;
                    });
                    if (obj.Cart.orderList[obj.Cart.orderList.length - 1] != undefined && item.index > 1 && lastInsertIdx == item.index && lastInsertOptIdx > -1) {
                        //console.log("insert option", obj.Cart.orderList[obj.Cart.orderList.length - 1].option);
                        //console.log(obj.Cart.orderList[obj.Cart.orderList.length - 1].option[lastInsertOptIdx].items);
                        angular.forEach(obj.Cart.orderList[obj.Cart.orderList.length - 1].option[lastInsertOptIdx].items, function (optItem) {
                            if (optItem.index == item.index) {
                                optItem.modifier.push(m[0]);
                            }
                        });
                    }
                    else {
                        obj.Cart.orderList[obj.Cart.orderList.length - 1].modifier.push(m[0]);
                    }
                } else if (item.type === "K") {
                    var k = obj.food.kitchenmsg.filter(function (matchItem) {
                        return matchItem.code == item.code;
                    });
                    if (obj.Cart.orderList[obj.Cart.orderList.length - 1] != undefined && item.index > 1 && lastInsertIdx == item.index && lastInsertOptIdx > -1) {
                        //console.log("insert option", obj.Cart.orderList[obj.Cart.orderList.length - 1].option);
                        //console.log(obj.Cart.orderList[obj.Cart.orderList.length - 1].option[lastInsertOptIdx].items);
                        angular.forEach(obj.Cart.orderList[obj.Cart.orderList.length - 1].option[lastInsertOptIdx].items, function (optItem) {
                            if (optItem.index == item.index) {
                                optItem.kitchenMsgOld.push(k[0]);
                            }
                        });
                    }
                    else {
                        obj.Cart.orderList[obj.Cart.orderList.length - 1].kitchenMsgOld.push(k[0]);
                    }
                }
                lastInsertIdx = item.index;
            });
            angular.forEach(obj.Cart.orderList, function(order) {
                if (order.option != undefined && order.option) {
                    angular.forEach(order.option, function(optionList) {
                        var selectQty = 0;
                        angular.forEach(optionList.items, function(optItem) {
                            if (optItem.qty > 0) {
                                selectQty += optItem.qty
                            }
                        })
                        if (selectQty == 0) {
                            var fillOption = $.extend(true, {}, obj.schema.unselectOption, obj.schema.baseItem);
                            // var fillOption = $.extend(true, {}, obj.schema.unselectOption, {qty: 1, _qty: 1});
                            optionList.items.push(fillOption);
                        }
                    })
                }
            });

            if (order.discount) {
                for (var i = 0; i < order.discount.length; i++) {

                    obj.appliedDiscount.push({ 'coupon_alias': order.discount[i].discountCode, 'amount': order.discount[i].discountAmount, 'type': order.discount[i].discountType, 'price': order.discount[i].discountValue });
                    obj.Cart.couponObj = [{ 'coupon_alias': order.discount[i].discountCode, 'amount': order.discount[i].discountAmount, 'type': order.discount[i].discountType }];
                }
            }

            console.log('obj.appliedDiscount');
            console.log(obj.appliedDiscount);
            
            if (order.payment) {
                for (var i = 0; i < order.payment.length; i++) {
                    obj.appliedPaymentMethod.push({ 'method': order.payment[i].paymentType, 'amount': order.payment[i].paymentValue })
                }
            }
        }

        obj.assignTableNo = function (tableNo, callback, tableMode) {
            //$(angular.element.find('.orderPanelCtrl')).removeClass('ng-hide');
            console.log('assign table no');
            obj.removeActive('#order-list li');
            // console.log(obj.UserManager.isLogin);
            if (!obj.UserManager.isLogin) {
                obj.UserManager.LoginRedirectToTable = tableNo;
                if (angular.element('md-dialog[aria-label="Login"]').scope() == undefined) {
                    $mdDialog.show({
                        templateUrl: 'tmpl/login.html'
                    })
                } else {
                    $mdDialog.hide();
                }
            } else {
                SocketFactory.emit('adminLoadTableOrder', { 'tableNum': { '$value': tableNo } }, function (r) {
                    r = JSON.parse(r);
                    console.log(r)
                    if (r.result === "OK") {
                        if (r.order.length > 1) {
                            callback(r.order);
                        } else {
                            var order = r.order[0];
                            console.log(order);
                            var header = r.order[0].header;
                            console.log(header.peopleNum);
                            if (header.peopleNum == 0) {
                                console.log('huh?---------------------');
                                obj.Cart.noOfPeople = 0; // reset
                                obj.setNoOfPeople();
                            }

                            obj.mode = obj.schema.mode.order;
                            tableMode = tableMode || "normal";
                            obj.modeOrder = obj.schema.modeOrder[tableMode];

                            if( tableMode === 'kitchenMessage' )
                                obj.modeMultiSelectItem = true;
                            else
                                obj.modeMultiSelectItem = false;

                            obj.Cart.init();
                            obj.Cart.tableNo = tableNo;
                            obj.Cart.noOfPeople = order.header.peopleNum;
                            if (order.header.member != undefined) {
                                obj.Cart.member = $.extend(true, order.header.member, obj.schema.member);
                            }
                            // var lastInsertType = "",
                            //     lastInsertIdx = 0,
                            //     lastInsertOptIdx = -1;
                            // angular.forEach(order.item, function (v, k) {
                            //      var item = $.extend(true, {}, obj.schema.baseItem, v);
                            //     //var item = $.extend(true, {}, obj.findItemFromFood(v.code), obj.schema.baseItem, v);
                            //     console.log("index " + v.index, v.type, v);
                            //     if (item.type === "I" || item.type === "T") {
                            //         item.init();
                            //         lastInsertOptIdx = -1;
                            //         if (v.voidIndex != undefined) {
                            //             if (v.voidIndex != -1) {
                            //                 //obj.Cart.orderList[v.voidIndex].voidQty = v.qty;
                            //                 $.each(obj.Cart.orderList, function (idx, vitem) {
                            //                     if (vitem.index == v.voidIndex) {
                            //                         vitem.voidQty += v.qty;
                            //                     }
                            //                 });
                            //             }
                            //             item.voidRemark = v.voidRemark;
                            //         }
                            //         //console.log(item)
                            //         obj.Cart.orderList.push(item);
                            //     } else if (item.type === "O") {
                            //         if (lastInsertIdx != item.index)
                            //             lastInsertOptIdx++;
                            //
                            //          //if (obj.Cart.orderList[obj.Cart.orderList.length - 1].option == undefined)
                            //          //    obj.Cart.orderList[obj.Cart.orderList.length - 1].option = [{ items: [] }];
                            //         //obj.Cart.orderList[obj.Cart.orderList.length - 1].option[0].items.push(item);
                            //         if (obj.Cart.orderList[obj.Cart.orderList.length - 1].option == undefined)
                            //             obj.Cart.orderList[obj.Cart.orderList.length - 1].option = [];
                            //         if (obj.Cart.orderList[obj.Cart.orderList.length - 1].option[lastInsertOptIdx] == undefined)
                            //             obj.Cart.orderList[obj.Cart.orderList.length - 1].option[lastInsertOptIdx] = { items: [] };
                            //          obj.Cart.orderList[obj.Cart.orderList.length - 1].option[lastInsertOptIdx].items.push(item);
                            //
                            //         //if (obj.Cart.orderList[obj.Cart.orderList.length - 1] != undefined) {
                            //         //    angular.forEach(obj.Cart.orderList[obj.Cart.orderList.length - 1].option[lastInsertOptIdx].items, function (optItem) {
                            //         //        if (optItem.code === item.code) {
                            //         //            $.extend(optItem, item);
                            //         //        }
                            //         //    });
                            //         //}
                            //     } else if (item.type === "S") {
                            //         angular.forEach(obj.Cart.orderList[obj.Cart.orderList.length - 1].subitem, function (subitem) {
                            //             if (subitem.code === item.code) {
                            //                 $.extend(subitem, item);
                            //             }
                            //         });
                            //     } else if (item.type === "M") {
                            //         var m = obj.food.modifiers.filter(function (matchItem) {
                            //             return matchItem.code == item.code;
                            //         });
                            //         if (obj.Cart.orderList[obj.Cart.orderList.length - 1] != undefined && item.index > 1 && lastInsertIdx == item.index && lastInsertOptIdx > -1) {
                            //             //console.log("insert option", obj.Cart.orderList[obj.Cart.orderList.length - 1].option);
                            //             //console.log(obj.Cart.orderList[obj.Cart.orderList.length - 1].option[lastInsertOptIdx].items);
                            //             angular.forEach(obj.Cart.orderList[obj.Cart.orderList.length - 1].option[lastInsertOptIdx].items, function (optItem) {
                            //                 if (optItem.index == item.index) {
                            //                     optItem.modifier.push(m[0]);
                            //                 }
                            //             });
                            //         }
                            //         else {
                            //             obj.Cart.orderList[obj.Cart.orderList.length - 1].modifier.push(m[0]);
                            //         }
                            //     } else if (item.type === "K") {
                            //         var k = obj.food.kitchenmsg.filter(function (matchItem) {
                            //             return matchItem.code == item.code;
                            //         });
                            //         if (obj.Cart.orderList[obj.Cart.orderList.length - 1] != undefined && item.index > 1 && lastInsertIdx == item.index && lastInsertOptIdx > -1) {
                            //             //console.log("insert option", obj.Cart.orderList[obj.Cart.orderList.length - 1].option);
                            //             //console.log(obj.Cart.orderList[obj.Cart.orderList.length - 1].option[lastInsertOptIdx].items);
                            //             angular.forEach(obj.Cart.orderList[obj.Cart.orderList.length - 1].option[lastInsertOptIdx].items, function (optItem) {
                            //                 if (optItem.index == item.index) {
                            //                     optItem.kitchenMsgOld.push(k[0]);
                            //                 }
                            //             });
                            //         }
                            //         else {
                            //             obj.Cart.orderList[obj.Cart.orderList.length - 1].kitchenMsgOld.push(k[0]);
                            //         }
                            //     }
                            //     lastInsertIdx = item.index;
                            // });
                            // angular.forEach(obj.Cart.orderList, function(order) {
                            //     if (order.option != undefined && order.option) {
                            //         angular.forEach(order.option, function(optionList) {
                            //             var selectQty = 0;
                            //             angular.forEach(optionList.items, function(optItem) {
                            //                 if (optItem.qty > 0) {
                            //                     selectQty += optItem.qty
                            //                 }
                            //             })
                            //             if (selectQty == 0) {
                            //                 var fillOption = $.extend(true, {}, obj.schema.unselectOption, obj.schema.baseItem);
                            //                 // var fillOption = $.extend(true, {}, obj.schema.unselectOption, {qty: 1, _qty: 1});
                            //                 optionList.items.push(fillOption);
                            //             }
                            //         })
                            //     }
                            // });
                            //
                            // // console.log(obj.Cart.orderList);
                            //
                            // if (order.discount) {
                            //     for (var i = 0; i < order.discount.length; i++) {
                            //
                            //         obj.appliedDiscount.push({ 'coupon_alias': order.discount[i].discountCode, 'amount': order.discount[i].discountAmount, 'type': order.discount[i].discountType, 'price': order.discount[i].discountValue });
                            //         obj.Cart.couponObj = [{ 'coupon_alias': order.discount[i].discountCode, 'amount': order.discount[i].discountAmount, 'type': order.discount[i].discountType }];
                            //     }
                            // }
                            // console.log('obj.appliedDiscount');
                            // console.log(obj.appliedDiscount);
                            // if (order.payment) {
                            //     for (var i = 0; i < order.payment.length; i++) {
                            //         obj.appliedPaymentMethod.push({ 'method': order.payment[i].paymentType, 'amount': order.payment[i].paymentValue })
                            //     }
                            // }
                            obj.assign2OrderList(order);
                        }
                    }
                });
            }
        }

        obj.billTable = function ( tableNo, tableMode ) {
            if( angular.isUndefined( tableMode ) ) {
                console.log('tableMode is undefined');
                return;
            }

            if( angular.isUndefined( tableNo ) ) {
                console.log('tableNo is undefined');
                return;
            }

            if (!obj.UserManager.isLogin) {
                obj.UserManager.LoginRedirectToTable = tableNo;
                if (angular.element('md-dialog[aria-label="Login"]').scope() == undefined) {
                    $mdDialog.show({
                        templateUrl: 'tmpl/login.html'
                    })
                } else {
                    $mdDialog.hide();
                }
            } else {
                console.log("tableMode:" + tableMode)
                var p = 'printBill';
                switch (tableMode) {
                    case 'printBill': p = 'printBill'; break;
                    case 'billing': p = 'billing'; break;
                }
                console.log(p);
                obj.checkPermission(p, function () {
                    obj.inputPrice = 0;
                    obj.remainder();
                    console.log('obj.mode ' + obj.mode);
                    console.log('obj.modeOrder ' + obj.modeOrder);
                    obj.loadTableOrder(tableNo, tableMode);
                });
            }
        };

        obj.tableKitchenMessage = function ( tableNo, tableMode ) {
            if( angular.isUndefined( tableMode ) ) {
                console.log('tableMode is undefined');
                return;
            }

            if( angular.isUndefined( tableNo ) ) {
                console.log('tableNo is undefined');
                return;
            }

            if (!obj.UserManager.isLogin) {
                obj.UserManager.LoginRedirectToTable = tableNo;
                if (angular.element('md-dialog[aria-label="Login"]').scope() == undefined) {
                    $mdDialog.show({
                        templateUrl: 'tmpl/login.html'
                    })
                } else {
                    $mdDialog.hide();
                }
            } else {
                console.log("tableMode:" + tableMode)

                // console.log(p);
                // obj.checkPermission(p, function () {
                //     obj.inputPrice = 0;
                //     obj.remainder();
                //     console.log('obj.mode ' + obj.mode);
                //     console.log('obj.modeOrder ' + obj.modeOrder);

                    obj.loadTableOrder(tableNo, tableMode);
                // });
            }
        };

        obj.checkPermission = function (permission,callback,failCallback) {
            if (obj.UserManager.staffDetails[obj.schema.permission[permission]]) {
                callback();
            } else {
                if (failCallback) {
                    failCallback()
                }else{
                    obj.showAlert('no_permission');
                }
            }
        }

        obj.stock = {
            item: null,
            list: [], updateStock: function (qty) {
                $.each(this.list, function (idx, item) {
                    if (item.isEdit) {
                        //item.qty = qty;
                        console.log(qty);
                        qty = qty == 0 ? -1 : qty;
                        console.log(qty);

                        //var serviceUrl = 'service.ashx';
                        //var a =
                        SocketFactory.emit('setItemQty', { itemCode: item.code, qty: qty, remark: "", staffId: obj.UserManager.staffCode }, function (result) {
                            console.log(result);
                            obj.stock.updateItem(JSON.parse(result));
                        });
                        //$http.post(serviceUrl, { action: "MDSetItemQty", data: a })
                        //    .then(function (result) {
                        //        //console.log("Data Saved: ", result);
                        //        //$scope.stock.updateItem(result.data);
                        //    });
                    }
                });
            }, 
            updateStockItem: function (qty, item) {
                console.log(qty);
                qty = qty == 0 ? -1 : qty;
                console.log(qty);
                SocketFactory.emit('setItemQty', { itemCode: item.code, qty: qty, remark: "", staffId: obj.UserManager.staffCode }, function (result) {
                    console.log(result);
                    obj.stock.updateItem(JSON.parse(result));
                });
            },
            updateItem: function (data) {
                console.log('data / updateItem');
                console.log(data);
                var optionGroupCodeList = [],
                    optionCodeList = []; // for prevent duplication
                obj.stock.list = [];
                $.each(obj.food.category, function (catIndex, cat) {
                    $.each(cat.items, function (itemIndex, item) {
                        //item.stock = -2;
                        var found = false;
                        $.each(data, function (i, v) {
                            //console.log(item.code)
                            if (item.code == v.code) {
                                found = true;
                                item.stock = v.qty;
                                 console.log(item);
                                console.log(item.code)
                                obj.stock.list.push($.extend(true, {}, obj.schema.baseItem, v, item));
                            }
                        });
                        if (!found) {
                            item.stock = '';
                        }

                        // option control
                        // console.log(item);

                        if( angular.isDefined(item.option) ) {
                            
                            $.each(item.option, function(idx, optionList){
                                // if it is not yet in option code list
                                //if( $.inArray(optionList.optionGroupCode, optionGroupCodeList) === -1 ) {
                                    optionGroupCodeList.push( optionList.optionGroupCode );
                                    
                                    $.each(optionList.items, function (optionItemIndex, optionItem) {
                                        //if( $.inArray(optionItem.code, optionCodeList) === -1 ) {
                                            optionCodeList.push( optionItem.code );

                                            var foundOption = false;
                                            $.each(data, function (i, v) {
                                                // different group might have same option item, prevent duplication
                                                    // console.log('checking');
                                                    if (optionItem.code == v.code) {
                                                        // console.log('found item option?');
                                                        // console.log(optionItem);
                                                        // console.log('looking for ' + v.code);
                                                        // console.log('food control/option item code ' + optionItem.code)
                                                        foundOption = true;
                                                        optionItem.stock = v.qty;

                                                        console.log(optionItem)
                                                        obj.stock.list.push($.extend(true, {}, obj.schema.baseItem, v, optionItem));
                                                    }
                                                    if (!foundOption) {
                                                        optionItem.stock = '';
                                                    }
                                            });
                                            
                                        //}
                                        
                                    });
                                //}
                            })
                            
                        }
                    })
                });

                function uniqueBy(arr, fn) {
                    var unique = {};
                    var distinct = [];
                    arr.forEach(function (x) {
                        var key = fn(x);
                        console.log(x);
                        if (!unique[key]) {
                            distinct.push(x);
                            unique[key] = true;
                        }
                    });
                    return distinct;
                }

                obj.stock.list = uniqueBy(obj.stock.list, function (x) { return x.code; });
                // console.log(optionGroupCodeList);
                // console.log(optionCodeList);
                // console.log(obj.stock.list);
            },
            submitItem: function (qty) {
                console.log(qty);
                qty = qty == 0 ? -1 : qty;
                console.log(qty);
                console.log('submitItem')
                var a = { itemCode: obj.stock.item.code, qty: qty, remark: "" }
                $http.post(serviceUrl, { action: "MDSetItemQty", data: a })
                    .then(function (result) {
                        //console.log("Data Saved: ", result);
                        //$scope.stock.updateItem(result.data);
                    });
                $('.calc-modal').modal('hide');
            },
            resetItem: function (item) {
                obj.stock.item = item;
                obj.stock.submitItem(-2);
            },
            reset: function () {
                SocketFactory.emit('resetItemQty', {}, function (r) {
                    console.log(r);
                    obj.stock.list = JSON.parse(r);
                });
            }
        }

        obj.selectCategory = function (category) {
            //console.log(category);
            obj.currentCategory = category;
            obj.currentPage = 1;
            obj.noOfCategoryPage = Math.ceil( obj.food.category.length / obj.catsPerPage );
            obj.totalItems = obj.currentCategory.items.length;
            obj.noOfItemPage = Math.ceil( obj.totalItems / obj.itemsPerPage );
            // console.log('obj.noOfItemPage ' + obj.noOfItemPage);
            console.log('obj.totalItems ' + obj.totalItems);
            console.log('obj.itemsPerPage ' + obj.itemsPerPage);
            console.log('remainder ' + obj.totalItems % obj.itemsPerPage);
            // $scope.$emit('reRender');
        }

        obj.getPageNumber = function( pageNum ){
            // console.log(pageNum);
            // console.log(typeof pageNum === 'NaN');
            // console.log(typeof pageNum === 'number');
            // console.log(typeof pageNum);
            // console.log(isNaN(pageNum));
            if( isNaN(pageNum) )
                return new Array(3);
            // console.log(pageNum);
            return new Array( pageNum );
        }

        //obj.setModeMultiSelectItem = function (mode) {
        //    var selectMode = mode ? mode : "multi"; // default
        //    this.selectMode = selectMode;
        //    return selectMode;
        //};


        obj.toggleActive = function (element, callback) {
            if (typeof element === 'object') {
                // treat as event object
                var target = element.currentTarget || element.srcElement;
            } else {
                var target = element;
            }

            if (this.modeMultiSelectItem) {
                if (angular.element(target).hasClass("active")) {
                    angular.element(target).removeClass("active");
                } else {
                    angular.element(target).addClass("active");
                }
            } else {
                obj.setActive(element);
            }

            (callback || angular.noop)();
        }

        obj.setActive = function (element, callback) {
            // console.log(element);
            console.log('set active');
            if (typeof element === 'object' && (angular.isDefined(element.currentTarget) || angular.isDefined(element.srcElement) )) {
                // treat as event object
                var target = element.currentTarget || element.srcElement;
            } else {
                var target = element;
            }
            // console.log(element);
            // console.log(target);
            angular.element(target).parents('.md-toolbar-item').find('.active').removeClass("active");
            angular.element(target).addClass("active");
            (callback || angular.noop)();
        }

        obj.removeActive = function (element, callback) {
            if (typeof element === 'object') {
                // treat as event object
                var target = element.currentTarget || element.srcElement;
            } else {
                var target = element;
            }
            //console.log(target);
            angular.element(target).removeClass("active");

            (callback || angular.noop)();
        }

        obj.cancelDeleteItem = function () {
            obj.modeMultiSelectItem = false;
            obj.modeItem = obj.schema.modeItem.normal;
        }

        obj.unselectAllItem = function () {
            obj.removeActive('.order-list li', function () { });
        }

        obj.selectAllItem = function () {
            obj.setActive('.order-list li', function () { });
        }

        obj.cancelModifier = function () {
            obj.modeMultiSelectItem = false;
            obj.modeOrder = obj.schema.modeOrder.normal;
            obj.modeItem = obj.schema.modeItem.normal;
        }


        obj.cancelOption = function () {
            obj.modeMultiSelectItem = false;
            obj.modeOrder = obj.schema.modeOrder.normal;
            obj.modeItem = obj.schema.modeItem.normal;
        }


        obj.foodPromise = $http.get('data/food.json');
        //obj.loadStockPromise = $http.post(serviceUrl, { "action": "MDLoadItemQty" });
        obj.loadStockPromise = function () {
            var deferred = $q.defer();
            SocketFactory.emit('loadItemQty', '', function (r) {
                // console.log(4444);
                // console.log(r);
                r = JSON.parse(r);
                deferred.resolve(r);
            });
            return deferred.promise;
        }
        obj.tablePromise = $http.get('data/table_type.json');
        obj.fontPromise = $http.get('data/font.json');
        //obj.promise = $q.all([obj.foodPromise, obj.loadStockPromise(), obj.tablePromise]).then(function (values) {
        obj.promise = $q.all([obj.foodPromise, obj.loadStockPromise(), obj.tablePromise, obj.fontPromise]).then(function (values) {
            console.log("Promise", values);
            obj.table = values[2].data;
            obj.food = values[0].data;
            $rootScope.fontFamily = values[3].data;
            obj.stock.updateItem(values[1]);

            console.log(obj.food.category);
            obj.noOfModifierPage = Math.ceil(obj.food.modifiers.length / obj.cancelOptionPerPage);
            obj.currentCategory = obj.food.category[0];
            obj.noOfCategoryPage = Math.ceil(obj.food.category.length / obj.catsPerPage);
            // console.log('obj.noOfCategoryPage ' + obj.noOfCategoryPage);
            // console.log(obj.food.category);
            // console.log(obj.food.category.length);
            obj.totalItems = obj.currentCategory.items.length;
            obj.noOfItemPage = Math.ceil(obj.totalItems / obj.itemsPerPage);
            obj.totalCats = obj.food.category.length;
            //obj.stock = {};
            //obj.stock.updateItem(JSON.parse(values[1].data));
            window['MainService'] = obj;
        }, function (e) { console.log(e); })

        $rootScope.$on('returnToOrder', function(event, data){
            console.log('event / returnToOrder');
            if( obj.isModeItem('optionFoodControl') ) {
                // obj.switchMode2({modeItem: 'normal'});
                console.log(obj.modeItem);
                obj.goToMode2({modeItem: 'normal'}, null, false);
                console.log(obj.modeItem);
                return;
            }

            if( obj.isModeFloor('kitchenMessage') ) {
                // obj.switchMode2({modeItem: 'normal'});



                console.log(obj.modeFloorPlan);
                obj.goToMode2({mode: 'floorPlan', modeFloorPlan: 'order'}, null, false);
                console.log(obj.modeFloorPlan);
                return;
            }
            // alert();
            obj.switchMode2({modeOrder: 'normal', mode: 'order', modeItemListDisplay: 'normal', modeItemListDisplay2: 'normal'}, false, 'order_mode', 'order_mode');
        });

        $rootScope.listMenuTileTemplate = function () {
            SocketFactory.emit('listMenuTileTemplate', {}, function (result) {
                var _result = result;
                obj.menuTileTemplateList = _result.data[0];
                window['MainService'] = obj;
            });
        }

        obj.DateDuring = "";

        window['MainService'] = obj;
        return obj;
    })
})();