(function () {
    var app = angular.module('common.controllers', []);

    /*
        generic controllers
        $rootElement: for global event
    */
    
    function DialogControllerLogin($scope, $mdDialog, SocketFactory, MainService, uiLanguage, INFO) {
        // console.log("called once");
        // console.log($scope);
        $scope.mode = 'username';
        $scope.modeName = 'Login';
        $scope.username = '';
        $scope.password = '';
        $scope.tableNum = '';
        $scope.keypad = keypad;
        $scope.ui = uiLanguage[INFO.lang];

        $scope.input = function (val) {
            angular.element(".login").addClass("md-input-focused");
            $scope.loginValue += '' + val;
        }

        $scope.reset = function (val) {
            angular.element(".login").addClass("md-input-focused");
            if ($scope.loginValue == '' && $scope.mode == 'username') {
                $mdDialog.cancel();
                // angular.element(".login").addClass("md-input-focused");
            } else if ($scope.loginValue == '' && $scope.mode == 'password') {
                $scope.modeName = 'Login';
                $scope.mode = 'username';
                $scope.loginValue = '';
            } else {
                $scope.loginValue = '';
            }
        }

        $scope.backspace = function () {
            // console.log('here!');
            // $scope.loginValue = "12347";
            $scope.loginValue = $scope.loginValue.substring(0, $scope.loginValue.length - 1);
        };

        $scope.next = function (val) {
            //console.log('********');
            //console.log($scope.tableNum);
            //console.log($scope.mode);
            //console.log($scope.loginValue);
            switch ($scope.mode) {
                case 'username':
                    $scope.username = $scope.loginValue;
                    MainService.UserManager.login($scope.username, '').then(function (r) {
                        //console.log(r);
                        //console.log(r.success);
                        if (r.success) {
                            $scope.loginError = false;
                            $mdDialog.cancel();
                            MainService.UserManager.loginCallback();
                        } else {
                            $scope.mode = 'password';
                            $scope.modeName = 'Password';
                            $scope.resetLoginInputValue();
                        }
                    }, function (reason) {
                        console.log('Failed: ' + reason);
                    }, function (update) {
                        console.log('Got notification: ' + update);
                    });
                    break;
                case 'password':
                    $scope.password = $scope.loginValue;
                    MainService.UserManager.login($scope.username, $scope.password).then(function (r) {
                        console.log(r);
                        if (r.success) {
                            $scope.loginError = false;
                            $mdDialog.cancel();

                            MainService.UserManager.loginCallback();
                        } else {
                            $scope.mode = 'password';
                            $scope.resetLoginInputValue();
                            $scope.loginError = true;
                        }
                    }, function (reason) {
                        console.log('Failed: ' + reason);
                    }, function (update) {
                        console.log('Got notification: ' + update);
                    });
                    break;
            }
        }
    }

    // support permission checkup, eg report button => check permission first before redirect
    function DialogControllerLogin2($scope, $mdDialog, SocketFactory, MainService, uiLanguage, INFO) {
        // console.log("called once");
        // console.log($scope);
        $scope.mode = 'username';
        $scope.modeName = 'Login';
        $scope.username = '';
        $scope.password = '';
        $scope.tableNum = '';
        $scope.keypad = keypad;
        $scope.ui = uiLanguage[INFO.lang];

        $scope.input = function (val) {
            angular.element(".login").addClass("md-input-focused");
            $scope.loginValue += '' + val;
        }

        $scope.reset = function (val) {
            angular.element(".login").addClass("md-input-focused");
            if ($scope.loginValue == '' && $scope.mode == 'username') {
                $mdDialog.cancel();
                // angular.element(".login").addClass("md-input-focused");
            } else if ($scope.loginValue == '' && $scope.mode == 'password') {
                $scope.modeName = 'Login';
                $scope.mode = 'username';
                $scope.loginValue = '';
            } else {
                $scope.loginValue = '';
            }
        }

        $scope.backspace = function () {
            // console.log('here!');
            // $scope.loginValue = "12347";
            $scope.loginValue = $scope.loginValue.substring(0, $scope.loginValue.length - 1);
        };

        $scope.next = function (val) {
            //console.log('********');
            //console.log($scope.tableNum);
            //console.log($scope.mode);
            //console.log($scope.loginValue);
            switch ($scope.mode) {
                case 'username':
                    $scope.username = $scope.loginValue;
                    MainService.UserManager.login($scope.username, '').then(function (r) {
                        //console.log(r);
                        //console.log(r.success);
                        if (r.success) {
                            $scope.loginError = false;
                            $mdDialog.cancel();
                            MainService.UserManager.LoginRedirectToMode2 = $scope.modeSwitcherParam[0];
                            MainService.UserManager.loginCallback();
                        } else {
                            $scope.mode = 'password';
                            $scope.modeName = 'Password';
                            $scope.resetLoginInputValue();
                        }
                    }, function (reason) {
                        console.log('Failed: ' + reason);
                    }, function (update) {
                        console.log('Got notification: ' + update);
                    });
                    break;
                case 'password':
                    $scope.password = $scope.loginValue;
                    MainService.UserManager.login($scope.username, $scope.password).then(function (r) {
                        console.log(r);
                        if (r.success) {
                            $scope.loginError = false;
                            $mdDialog.cancel();

                            // check permission on login callback
                            // console.log($scope.modeSwitcherParam);
                            // permission
                            switch( $scope.modeSwitcherParam[4] ) {
                              case 'all':
                                // not check
                                MainService.UserManager.LoginRedirectToMode2 = $scope.modeSwitcherParam[0];
                                // MainService.UserManager.LoginRedirectToCaller = $event;
                                MainService.UserManager.loginCallback();
                                break;
                                default:
                                console.log($scope.modeSwitcherParam[4]);
                                MainService.checkPermission($scope.modeSwitcherParam[4], function () {
                                    MainService.UserManager.LoginRedirectToMode2 = $scope.modeSwitcherParam[0];
                                    MainService.UserManager.loginCallback();
                                })
                                break;
                            }
                        } else {
                            $scope.mode = 'password';
                            $scope.resetLoginInputValue();
                            $scope.loginError = true;
                        }
                    }, function (reason) {
                        console.log('Failed: ' + reason);
                    }, function (update) {
                        console.log('Got notification: ' + update);
                    });
                    break;
            }
        }
    }

    app.controller('DialogControllerLogin', DialogControllerLogin);

    app.controller('globalController', function($scope, $element, $location, $mdDialog, $controller, $rootElement, $interval, $timeout, MainService, uiLanguage, INFO, userConfig, keyMap, SocketFactory, menu) {
      // $controller('DialogControllerLogin',{$scope: $scope, $mdDialog: $mdDialog});
      console.log($location.search());

      // global inheritance for each controller
      var query = $location.search();
      userConfig.page = {
        cash: query.cash || userConfig.page.cash,
        printer: query.printer || userConfig.page.printer,
        bill: query.bill || userConfig.page.bill
      };
      $scope.uc = $scope.config = userConfig;
      $scope.ui = uiLanguage[INFO.lang];
      menu.userConfig = userConfig;

      $scope.returnStatus = function(status) {
          if( status == 4 ) {
            // console.log('*********************************** ' + status);
              return $scope.ui['billed'];
          }
      }

      $scope.toggleItemDisplay = function( mode ) {
        return MainService.singleModeToggle( 'modeItemListDisplay', mode );
      }

      $scope.toggleItemDisplay1 = $scope.toggleItemDisplay;

      $scope.toggleItemDisplay2 = function( mode ) {
        return MainService.singleModeToggle( 'modeItemListDisplay2', mode );
      }

      $scope.toggleItemOption = function( option ) {
        // modeObj = modeObj ? modeObj : {};
        // $scope.$emit('toggleItemOption', modeObj);
        switch( option ) {
          case 'transfer_item':
            $scope.switchMode2({modeOrder:'transfer', modeItem:'normal', modeFloorPlan: 'order'}, true, 'transfer_item');
            break;
          default:
            
            break;
        }
      }

      $scope.toggleFoodControlMode = function() {
        return MainService.modeToggleFoodControl();
      }

      $scope.toggleNavMode = function( mode ) {
        var mode = angular.isUndefined( mode ) ? ['normal'] : mode,
            mode = angular.isArray( mode ) ? mode : [mode];

        return MainService.singleModeToggle( 'modeNavMenu', mode );
      }

      //old alias
      $scope.modeToggleFoodControl = $scope.toggleFoodControlMode;

      $scope.returnToOrder = function () {
          MainService.modeMultiSelectItem = false;
          MainService.Cart.resetEditItem();
          $scope.$emit( 'returnToOrder', [] );
      }

      $scope.strConvertTo = function( option, str ){
        // return strConvertTo(option, str);
        return strConvertTo.apply( this, arguments );
      }

      // $scope.showConfirm = function() {
      //     MainService.showConfirm.apply( MainService, arguments );
      // }

      // common page function (shorten the writing in rms-templates), alias
      $scope.isLogin = function() {
        return MainService.UserManager.isLogin;
      };

      $scope.numberToArray = function( num ) {
          return new Array( num );
      }

      $scope.numberToArray2 = function( num ) {
          var tmp = [];
          for (var i = 0; i < num; i++) {
            tmp.push(i);
          }
          // console.log(tmp);
          return tmp;
      }

      $scope.repeatCount = 0;
      $scope.counter = function() {
        console.log($scope.repeatCount);
        return $scope.repeatCount++;
      }

      $scope.refreshScreen = function() {
        var confirm = $mdDialog.confirm()
           .title(uiLanguage[INFO.lang]['alert'])
           .content(uiLanguage[INFO.lang]['refresh_confirmation'])
           .ok(uiLanguage[INFO.lang]['yes'])
           .cancel(uiLanguage[INFO.lang]['cancel']);
           
        $mdDialog.show(confirm).then(function () {
              location.reload();
            }, function () {
            });
      }
      // location.reload();

      // alert($scope.config.page.cash || userConfig.page.cash || false)
      // $scope.mainMenuList = [
      //   { label: 'main_page', mode: { mode: 'main' }, requiredLogin: false, config: true, row: 2, permission: 'all' },
      //   { label: 'item_admin', mode: { mode: 'itemAdmin' }, requiredLogin: true, config: true, row: 2, permission: 'all' },
      //   { label: 'category_admin', mode: { mode: 'categoryAdmin' }, requiredLogin: true, config: true, row: 2, permission: 'all' },
      //   { label: 'modifier_admin', mode: { mode: 'modifierAdmin' }, requiredLogin: true, config: true, row: 2, permission: 'all' },
      //   { label: 'option_admin', mode: { mode: 'optionAdmin' }, requiredLogin: true, config: true, row: 2, permission: 'all' },
      //   { label: 'subitem_admin', mode: { mode: 'subitemAdmin' }, requiredLogin: true, config: true, row: 2, permission: 'all' },
      //   { label: 'menu_layout_admin', mode: { mode: 'menuLayoutList' }, requiredLogin: true, config: true, row: 2, permission: 'all' },
      //   //{ label: 'food_control', mode: { mode: 'order', modeOrder: 'foodControl' }, requiredLogin: true, config: true, row: 2, permission: 'all' },
      //   //{ label: 'food_control', menuGroup: '', mode: { mode: 'order', modeOrder: 'foodMenu' }, requiredLogin: true, config: true, col: 4, permission: 'checkBill' },
      //   { label: 'food_control', menuGroup: '', mode: { mode: 'order', modeOrder: 'foodControl', modeOrderMenu: 'normal', modeNavMenu: 'normal', modeFloorPlan: 'order', modeItem: 'normal' }, requiredLogin: true, config: true, col: 4, permission: 'all' },
      //   { label: 'time_range_setting', mode: { mode: 'timeRangeSetting' }, requiredLogin: true, config: true, row: 2, permission: 'all' },
      //   { label: 'user_management', mode: { mode: 'userManagement' }, requiredLogin: true, config: true, row: 2, permission: 'userManagement' },
      //   { label: 'user_group', mode: { mode: 'userGroupManagement' }, requiredLogin: true, config: true, row: 2, permission: 'userManagement' },
      //   { label: 'report', mode: { mode: 'report' }, requiredLogin: true, config: true, row: 2, permission: 'report' },
      //   { label: 'daily_clearance', mode: { mode: 'dailyClearance' }, requiredLogin: true, config: true, row: 2, permission: 'dailyClearance' },
      //   { label: 'search_bill', mode: { mode: 'order', modeOrder: 'searchBill' }, requiredLogin: true, config: true, row: 2, permission: 'checkBill' }
      //   // {label: 'cash_management', mode: {mode: 'cashManagement'}, requiredLogin: true, config: true, row: 2, permission: 'cashManagement'},
      //   // {label: 'open_cashbox', mode: {mode: 'main', modeMain: 'cashBox'}, requiredLogin: true, config: $scope.config.page.cash || userConfig.page.cash || false, row: 2, permission: 'cashManagement'},
      //   // {label: 'user_clock_management', mode: {mode: 'userClockManagement'}, requiredLogin: false, config: true, row: 1, permission: 'all'}
      //   // {label: 'kitchen_info', mode: {mode: 'order', modeItem: 'normal', modeOrder: 'kitchenMessage'}, requiredLogin: true, config: true}
      // ];

      $scope.mainMenuList = [];
        // $scope.mainMenuList = menu.mainMenuList;
        // console.log($scope.mainMenuList);
        // console.log($scope.mainMenuListOld);

        for (var i = 0; i < menu.mainMenuList.length; i++) {
          if(menu.mainMenuList[i].type === 'toggle') {
            for (var k = 0; k < menu.mainMenuList[i].pages.length; k++) {
              $scope.mainMenuList.push(menu.mainMenuList[i].pages[k]);
            }
          } else {
            $scope.mainMenuList.push(menu.mainMenuList[i]);
          }
        }

      // $scope.login = function() {
      //   var args = Array.prototype.slice.call(arguments).concat(); // same as arguments
      //   return MainService.UserManager.login.apply(MainService, args);
      //   // MainService.UserManager.login();
      // }

      $scope.logout = function() {
        // var args = Array.prototype.slice.call(arguments).concat(); // same as arguments
        return MainService.UserManager.logout.apply(MainService, arguments);
        // MainService.UserManager.logout();
      }

      // deprecated, replaced by v2
      $scope.switchMode = function( modeType, modeName, requiredLogin ) {
          MainService.switchMode( modeType, modeName, requiredLogin );
      }  

      // modeOption, requiredLogin, message
      // for generic purpose switching between modes
      $scope.switchMode2 = function() {
          // console.log(arguments);
          // put the data in the first arguments for working with custom render extensibility
          // var args = Array.prototype.slice.call(arguments).concat(); // same as arguments

          // automatically get all arguments from caller even not defined in above eg. "message" is not defined, but put in caller function will put here for use and extending the list freely like overloading

          // console.log(arguments);
          // check permission here before switching mode
          MainService.switchMode2.apply(MainService, arguments);

          // MainService.checkPermission(arguments[4], function () {
          //     console.log('ok ar');
          // })

          // MainService.switchMode2( modeOption, requiredLogin );
      }

      //  modeOption, requiredLogin, message, pageTitle, permission
      // for main menu or icon menu with permission check support 
      $scope.switchMode3 = function( modeOption, requiredLogin, message, pageTitle, permission ) {
        var requiredLogin = ( angular.isDefined(requiredLogin) ) ? requiredLogin : true,
            pageTitle = ( angular.isDefined(pageTitle) ) ? pageTitle : '',
            message = ( angular.isDefined(message) && message != '' ) ? uiLanguage[INFO.lang][lowerFirstLetter( message )] : ''

          MainService.pageTitle = uiLanguage[INFO.lang][pageTitle];
          MainService.statusMessage = message;
          $scope.modeSwitcherParam = arguments;
          // console.log(arguments);

          // required login
          if ( requiredLogin && !MainService.UserManager.isLogin){
              if (angular.element('md-dialog[aria-label="Login"]').scope() == undefined) {
                  $mdDialog.show({
                      controller: DialogControllerLogin2,
                      templateUrl: 'tmpl/login2.html',
                      bindToController: true,
                      scope: $scope,
                      preserveScope: true
                  })
              } else {
                  $mdDialog.hide();
              }
          } else {
            switch( arguments[4] ) {
              case 'all':
                // not check
                MainService.UserManager.LoginRedirectToMode2 = arguments[0];
                // MainService.UserManager.LoginRedirectToCaller = $event;
                MainService.UserManager.loginCallback();
                break;
                default:
                  console.log('login already redirect');
                    // console.log(arguments[4]);
                    var args = arguments;
                    MainService.checkPermission(arguments[4], function () {
                      // console.log('here?');
                      MainService.switchMode2.apply(MainService, args);
                  })

                    // MainService.checkPermission(arguments[4], function () {
                    //     MainService.UserManager.LoginRedirectToMode2 = arguments[0];
                    //     MainService.UserManager.loginCallback();
                    // })
                break;
            }
            // MainService.switchMode2.apply(MainService, arguments);
          }

          /*// all mode changed here required login by default
          var requiredLogin = ( angular.isDefined(requiredLogin) ) ? requiredLogin : true,
              pageTitle = ( angular.isDefined(pageTitle) ) ? pageTitle : '',
              message = ( angular.isDefined(message) && message != '' ) ? uiLanguage[INFO.lang][lowerFirstLetter( message )] : ''

          MainService.pageTitle = uiLanguage[INFO.lang][pageTitle];
          MainService.statusMessage = message;
          // console.log(pageTitle);
          // this.showStatusMessage( message );

          if( requiredLogin && !MainService.UserManager.isLogin ) {
              MainService.UserManager.checkLogin();
              console.log(permission);

              switch( permission ) {
                case 'all':
                    console.log('all / after login?');
                  break;
                default:
                      console.log(permission);
                  // MainService.checkPermission(permission, function () {
                  // })
                  break;
              }
              // MainService.UserManager.LoginRedirectToMode2 = modeOption;
              // MainService.UserManager.LoginRedirectToCaller = $event;
          } else {
            switch( permission ) {
              case 'all':
                console.log('all / no login required');
                MainService.goToMode2( modeOption );
                break;
              default:
                    console.log(permission);
                // MainService.checkPermission(permission, function () {
                //     // MainService.goToMode2( modeOption );
                // })
                break;
            }
          }*/
      }

      menu.switchMode3 = $scope.switchMode3;

      $scope.isMode = function() {
        return MainService.isMode.apply(MainService, arguments);
      }

      $scope.isNotMode = function() {
        return MainService.isNotMode.apply(MainService, arguments);
      }

      $scope.isModeBill = function() {
        return MainService.isModeBill.apply(MainService, arguments);
      }

      $scope.isNotModeBill = function() {
        return MainService.isNotModeBill.apply(MainService, arguments);
      }

      $scope.isModeFloor = function() {
        return MainService.isModeFloor.apply(MainService, arguments);
      }

      $scope.isNotModeFloor = function() {
        return MainService.isNotModeFloor.apply(MainService, arguments);
      }

      $scope.isModeOrderMenu = function() {
        return MainService.isModeOrderMenu.apply(MainService, arguments);
      }

      $scope.isNotModeOrderMenu = function() {
        return MainService.isNotModeOrderMenu.apply(MainService, arguments);
      }

      $scope.isModeNavMenu = function() {
        var mode = angular.isUndefined( mode ) ? ['normal'] : mode,
            mode = angular.isArray( mode ) ? mode : [mode];
            
        return MainService.modeChecking.apply( MainService, [[{modeNavMenu: mode}]] );
      }

      $scope.isNotModeNavMenu = function() {
        return !$scope.isModeNavMenu.apply( $scope, arguments );
      }

      $scope.isModeOrder = function() {
        return MainService.isModeOrder.apply(MainService, arguments);
      }

      $scope.isNotModeOrder = function() {
        return MainService.isNotModeOrder.apply(MainService, arguments);
      }

      $scope.isModeItem = function() {
        return MainService.isModeItem.apply(MainService, arguments);
      }

      $scope.isNotModeItem = function() {
        return MainService.isNotModeItem.apply(MainService, arguments);
      }

      /* 
        modeOrder > modeItem
        modeDisplay independent (in any order list mode)
        modeItem: control item mode, display option
        modeOrder: control order mode
        modeDisplay: control item display
      */
      $scope.modCheckItemDisplay = function( mode ) {
        // this.modeChecking([{mode: [option]}]);
        return MainService.modeChecking.apply( MainService, [[{modeItemListDisplay: [mode]}]] );
      }
      
      $scope.isModeItemDisplay = $scope.modCheckItemDisplay;
      $scope.isNotModeItemDisplay = function() {
        return !$scope.isModeItemDisplay.apply( MainService, arguments );
      }

      // multi channel
      $scope.isModeItemDisplayChannel = function(channel, mode) {
        var mode = angular.isArray( mode ) ? mode : [mode];
        switch(channel) {
          default:
          case 1:
            return MainService.modeChecking.apply( MainService, [[{'modeItemListDisplay': mode}]] );
            break;
          case 2:
            return MainService.modeChecking.apply( MainService, [[{'modeItemListDisplay2': mode}]] );
            break;
        }
      }
           $scope.isModeItemDisplay1 = function() {
             return $scope.isModeItemDisplayChannel.apply( MainService, [1, arguments[0]] )
           };

           $scope.isNotModeItemDisplay1 = function() {
             return !$scope.isModeItemDisplay1.apply( MainService, arguments );
           };

           $scope.isModeItemDisplay2 = function() {
             return $scope.isModeItemDisplayChannel.apply( MainService, [2, arguments[0]] )
           };

           $scope.isNotModeItemDisplay2 = function() {
             return !$scope.isModeItemDisplay2.apply( MainService, arguments );
           };

      // console.log('13-1293129-312-30123012-391239-')
      // console.log( $scope.isModeItemDisplay1('normal') )
      // console.log( $scope.isNotModeItemDisplay1('normal') )
      // console.log( $scope.isModeItemDisplay2('normal') )
      // console.log( $scope.isNotModeItemDisplay2('normal') )

      $scope.modeChecking = function() {
        // console.log('me ar....'); // run so many times from start up, not sure why, maybe watcher?.....
        // var args = Array.prototype.slice.call(arguments).concat(); // same as arguments
        return MainService.modeChecking.apply(MainService, arguments);
      }

      $scope.modeCheckBase = function() {
        return MainService.modeCheckBase.apply(MainService, arguments);
      }

      $scope.modeCheckUserGroup = function() {
        return MainService.modeCheckUserGroup.apply(MainService, arguments);
      }
      
      $scope.modeCheckFloor = function() {
        return MainService.modeCheckFloor.apply(MainService, arguments);
      }

      $scope.modeCheckBill = function() {
        return MainService.modeCheckBill.apply(MainService, arguments);
      }
      
      $scope.modeCheckOrderItem = function() {
        return MainService.modeCheckOrderItem.apply(MainService, arguments);
      }

      $scope.modeCheckOrder = function() {
        return MainService.modeCheckOrder.apply(MainService, arguments);
      }

      $scope.modeCheckOrderItemDetail = function() {
        return MainService.modeCheckOrderItemDetail.apply(MainService, arguments);
      }

      $scope.modeCheckOrderFood = function() {
        return MainService.modeCheckOrderFood.apply(MainService, arguments);
      }

      $scope.modeToggleItemDetail = function() {
        return MainService.modeToggleItemDetail.apply(MainService, arguments);
      }

      $scope.appliedDiscount = function() {
        // var args = Array.prototype.slice.call(arguments).concat(); // same as arguments
        return MainService.appliedDiscount;
      }

      $scope.deletDiscount = function() {
        // var args = Array.prototype.slice.call(arguments).concat(); // same as arguments
        return MainService.deletDiscount.apply(MainService, arguments);
      }

      $scope.appliedPaymentMethod = function() {
        // var args = Array.prototype.slice.call(arguments).concat(); // same as arguments
        return MainService.appliedPaymentMethod;
      }
      
      $scope.remainder = function() {
        // var args = Array.prototype.slice.call(arguments).concat(); // same as arguments
        return MainService.remainder;
      }

      $scope.deletePaymentMethod = function() {
        // var args = Array.prototype.slice.call(arguments).concat(); // same as arguments
        return MainService.deletePaymentMethod.apply(MainService, arguments);
      }

      $scope.cancelDeleteItem = function() {
        // var args = Array.prototype.slice.call(arguments).concat(); // same as arguments
        return MainService.cancelDeleteItem.apply(MainService, arguments);
      }

      $scope.selectAllItem = function() {
        // var args = Array.prototype.slice.call(arguments).concat(); // same as arguments
        return MainService.selectAllItem.apply(MainService, arguments);
      }

      $scope.unselectAllItem = function() {
        // var args = Array.prototype.slice.call(arguments).concat(); // same as arguments
        return MainService.unselectAllItem.apply(MainService, arguments);
      }

      // count down logout
      // store the interval promise in this variable
      var promise;
    
      // simulated items array
      $scope.items = [];
      $scope.count = 1;

      // starts the timeout counter
      // $scope.timeout = 2000; // put in settings
      $scope.timeout = angular.isDefined(userConfig.loginTimeout) ? userConfig.loginTimeout : "30000";
      $scope.interval = 500;
      // console.log('$scope.timeout ' + $scope.timeout);
      $scope.startLogoutCountDown = function() {
        // console.log('logout counter start');
        // stops any running interval to avoid two intervals running at the same time
        $scope.stopLogoutCountDown();

        // counter trace
        $scope.count = 1;
         //$scope.logoutTicker = $interval(ticker, $scope.interval);
         //$scope.logoutTicker = setInterval(ticker, $scope.interval);

        // store the timeout promise
          $scope.promise = $timeout(logout, $scope.timeout);
         //$scope.promise = setTimeout(logout, $scope.timeout);
      };

      function ticker() {
        // console.log('run ticker');
        // console.log($scope.count);
        // console.log($scope.interval * $scope.count);
        $scope.count++;
      }

      function logout() {
        // console.log($scope.interval * $scope.count);
        console.log('auto logout');
        // clearInterval($scope.logoutTicker);
        MainService.UserManager.logout();
          // $scope.stopLogoutCountDown();

        /*if (MainService.modeOrder == MainService.schema.modeOrder.searchBill) {

            console.log('after stop function 1')
            MainService.modeOrder = {};
            MainService.mode = MainService.schema.mode.main;
            console.log('after stop function 11')
        } else {
            console.log('after stop function 2')
            MainService.mode = MainService.schema.mode.floorPlan;
        }*/
      }
    
      // stops the interval
      $scope.stopLogoutCountDown = function() {
          // console.log('counter init/stop');
         // clearInterval($scope.logoutTicker);
          // console.log('after counter init/stop 1')
          //clearTimeout($scope.promise);
          // console.log('after counter init/stop 2')
        //$interval.cancel($scope.logoutTicker);
        $timeout.cancel($scope.promise);
      };
    
      // starting the interval by default
      // $scope.startLogoutCountDown();
      MainService.startLogoutCountDown = function(){$scope.startLogoutCountDown();};
      MainService.stopLogoutCountDown = function(){$scope.stopLogoutCountDown();};
    
      // stops the interval when the scope is destroyed,
      // this usually happens when a route is changed and 
      // the ItemsController $scope gets destroyed. The
      // destruction of the ItemsController scope does not
      // guarantee the stopping of any intervals, you must
      // be responsible of stopping it when the scope is
      // is destroyed.
      $scope.$on('$destroy', function() {
        console.log("scope distroyed");
        $scope.stopLogoutCountDown();
      });

      $rootElement.on('mousedown keyup touch', function(e){
          // if( $(e.target).hasClass('no-login') ) {
          //   console.log("not return?");
          //   return;
          // }

          if (MainService.UserManager.isLogin){
            // console.log(e.type);
            // console.log('counter reset');
            
            $scope.startLogoutCountDown(); // restart again, include stop
          }
      });

      $rootElement.on('mousedown keyup touch', function(e){
        // console.log(e.keyCode == keyMap.ESC);

        if( $scope.modeCheckFloor('changeTable') ) {
          if( e.keyCode == keyMap.ESC )
            MainService.cancelTransfer();
        }
      });
    });

    app.controller('scrollController', function($scope) {
      $scope.scrollDown = function( element, offset ) { //id or class
        var offsetHeight = offset ? offset : 25;
        var currentTop = angular.element( element ).scrollTop();
        angular.element( element ).scrollTop(currentTop + offsetHeight);
      }
      $scope.scrollUp = function( element, offset ) {
        var offsetHeight = offset ? offset : 25;
        var currentTop = angular.element( element ).scrollTop();
        angular.element( element ).scrollTop(currentTop - offsetHeight);
      } 
    });

    // instantiate this controller to call its functions
    /*
      eg.
      inject $controller into any caller controller
      $controller('ToastCtrl',{$scope: $scope}); // passing current scope to commmon controller
    */
    app.controller('ToastCtrl', function($scope, $mdToast, uiLanguage, INFO ) {
      $scope.closeToast = function() {
        $mdToast.hide();
      }

      $scope.showCustomToast = function( delay, msg ) {
        // $mdToast.simple().content('Hello!')
        var delay = delay ? delay : 3000;
        $scope.message = msg ? msg : uiLanguage[INFO.lang]['loading'];
        $scope.close = uiLanguage[INFO.lang]['closeMsg'];
        // alert(uiLanguage[INFO.lang]['loading'])

        if(this.message != '' && typeof this.message != 'undefined' ) {
            $mdToast.hide(); // hide previous
            $mdToast.show({
              controller: function($scope, $mdToast, message){$scope.message = message},
              templateUrl: 'tmpl/toast-template.html',
              hideDelay: delay,
              position: 'bottom left',
              scope: $scope,
              preserveScope: true,
              bindToController: true,
              locals: {
                  message: $scope.message,
                  close: $scope.close
              }
            });
        }

        // $mdToast.hide();
      };

      // disable toast for demo
      // $scope.showCustomToast = function(){}
    });

    app.controller('messageBoxController', function($scope, $mdDialog, uiLanguage, INFO) {
      $scope.alert = '';
      
      $scope.showAlert = function(title, content) {
        // Appending dialog to document.body to cover sidenav in docs app
        // Modal dialogs should fully cover application
        // to prevent interaction outside of dialog

        var title = title || 'Please provide title',
        content = content || 'Please provide content';

        $mdDialog.show(
          $mdDialog.alert()
            .parent(angular.element(document.body))
            .title(title)
            .content(content)
            .ok(uiLanguage[INFO.lang]["affirmative"])
        );
      };
    });
    //--------------------------------------------------------
})();