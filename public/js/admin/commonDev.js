// under development, not yet finailized common tools

// example to use callback in loadJSON
/*loadJSON("files/food.json", renderCB, renderHeaderHelper, "body");
var renderCB = function( data, helperFn, renderFn ){
 console.log(arguments);

@path: string
@callback: function
@callBackAfteRender(optional): function
@callBackAfterRenderOptions(optional): object // for performance, extensibility

};*/
function loadJSON(path, callback) {
    // console.log(arguments);
    var args = [];
    if( arguments.length > 2 ) {
      args = Array.prototype.slice.call(arguments).splice(2, arguments.length - 2);
      // console.log(args);
    }
    // console.log(args);
    
    $.ajax({
        url: path,
        dataType: "JSON",
        success: function (data) {
            console.log(Array.prototype.slice.call(arguments).splice(0, 1).concat(args));
            // put the data in the first arguments for working with custom render extensibility
            callback.apply(arguments[4], Array.prototype.slice.call(arguments).splice(0, 1).concat(args)) || noop;
        }
    });
}

// improved version, support $scope object so that data can be passed back for whole scope using
function loadJSON2(path, $scope, callback) {
    // console.log(arguments);
    var args = [], cutOffArg = 3;
    // console.log(arguments.length - cutOffArg);
    if( arguments.length > cutOffArg ) {
      args = Array.prototype.slice.call(arguments).splice(cutOffArg, arguments.length - cutOffArg);
      // console.log(args);
    }
    // console.log(args);
    
    $.ajax({
        url: path,
        dataType: "JSON",
        success: function (data) {
            // console.log($scope);
            // console.log(Array.prototype.slice.call(arguments).splice(0, 1).concat(args));
            // put the data in the first arguments for working with custom render extensibility
            $scope[callback].apply($scope, Array.prototype.slice.call(arguments).splice(0, 1).concat(args)) || noop;
        }
    });
}

function refreshPage() {
  location.reload();
}

function scrollToRow( container, row ) {
  // var rowpos = $(container).find(row).position();
  // console.log($(container).find(row));
  // console.log(rowpos);
  // if( typeof rowpos != 'undefined' && typeof rowpos.top != 'undefined' )
  //   $(container).scrollTop( rowpos.top );
  var headerRowHeight = $(container).find('table tr:first').height();
      rowpos = ( $(container).find(row).index() - 1 ) * $(container).find(row).height() + headerRowHeight;
  // console.log('rowpos is ' + rowpos);
  $(container).scrollTop( rowpos );
}

function removeAllData( target, inExList, listOnly ) {
  $target = typeof target === 'undefined' ? jQuery({}) : $( target );
  inExList = inExList ? inExList : [];
  listOnly = listOnly ? listOnly : false;
  // console.log('remove data');
  // console.log(target);
  // console.log($target);
  if( $target.length === 0 )
    return;
  // console.log($('#item-details form #code').data());
  // console.log($target.data());
  // console.log("clear data");
  $.each($target.data(), function(i){
    // $element.removeAttr('data-'+i.replace(/([a-z])([A-Z])/g, '$1-$2').toLowerCase()); 
    // console.log(i)
    // camel case to hyphen format then compare in inExList
    // console.log(i);
    if( listOnly ) {
      // console.log("in only?");
      // console.log('list name ' + i.replace(/([a-z])([A-Z])/g, '$1-$2').toLowerCase());
      // console.log($.inArray(i.toLowerCase(), inExList));
      if( $.inArray(i.toLowerCase(), inExList) != -1 ) {
        
          $target.removeData(i); 
      }
    } else {
      // console.log("out only?");
      // if( $.inArray(i.replace(/([a-z])([A-Z])/g, '$1-$2').toLowerCase(), inExList) === -1 )
      //   $target.removeData(i); 
    }
  });
};

function removeDataAttributes( target, inExList, listOnly ) {
    var $target = $(target);
    if( $target.length === 0 )
      return;

    var i,
        attrName,
        dataAttrsToDelete = [],
        dataAttrs = $target.get(0).attributes ? $target.get(0).attributes : [],
        dataAttrsLen = dataAttrs === 0 ? 0 : dataAttrs.length;

    inExList = inExList ? inExList : [];
    listOnly = listOnly ? listOnly : false;

    // loop through attributes and make a list of those
    // that begin with 'data-'
    for (i=0; i<dataAttrsLen; i++) {
        if ( 'data-' === dataAttrs[i].name.substring(0,5) ) {
            // Why don't you just delete the attributes here?
            // Deleting an attribute changes the indices of the
            // others wreaking havoc on the loop we are inside
            // b/c dataAttrs is a NamedNodeMap (not an array or obj)

            // if it is not in inExList list
            // inclusion
            if( listOnly ) {
              // console.log("in only?");
                // console.log(dataAttrs[i].name.substr(5));
                // console.log($.inArray(dataAttrs[i].name.substr(5), inExList));
              // console.log("inclusion, include listed item");
              if( $.inArray(dataAttrs[i].name.substr(5), inExList) === -1 ) {
                
              } else {
                dataAttrsToDelete.push(dataAttrs[i].name);
              }
            } else {
              // exclusion or All
              // console.log(dataAttrs[i].name.substr(5));
              if( $.inArray(dataAttrs[i].name.substr(5), inExList) === -1 ) {
                // console.log("out of list? all others");
                dataAttrsToDelete.push(dataAttrs[i].name);
              } else {
                // console.log("exclusion, exclude listed item");
              }
            }
        }
    }
    // delete each of the attributes we found above
    // i.e. those that start with "data-"
    // console.log(dataAttrsToDelete);
    $.each( dataAttrsToDelete, function( index, attrName ) {
        $target.removeAttr( attrName );
    })
};

function getCheckValue( $target ) {
  if( $target.prop('checked') )
    return 1;
  else
    return 0;
}

function getCheckBool( val ) {
  if( val == 1 )
    return true;
  else
    return false;
}

function setCheckValue( $target, val ) {
  if( val == 1 )
    $target.prop('checked', true);
  else
    $target.prop('checked', false);
}

// do not need getCheckField since value is read from data object to item details field in direct
function setCheckField( value ) {
  if( typeof value === 'undefined')
    return;

  if( value == 1 )
    return 'done';
  else
    return 'clear';
}

function DataBinder( object_id, dataObject, $parentObj ) {
  // console.log(arguments);
  // console.log($parentObj);
  // console.log(dataObject);
  // Use a jQuery object as simple PubSub
  var pubSub = jQuery({});
  // We expect a `data` element specifying the binding
  // in the form: data-bind-<object_id>="<property_name>"
  var data_attr = "bind-" + object_id,
      message = object_id + ":change";
      // console.log(object_id);
      // console.log(dataObject);
      // console.log($parentObj);
      // console.log('message '+ message);

  // Listen to change events on elements with the data-binding attribute and proxy
  // them to the PubSub, so that the change is "broadcasted" to all connected objects
  // if use "document" as parent, then it is hard to unbind only specified
  // dataObject is given by Item.set while type is given by $(DOM).trigger('eventName', [type])
  // type is use to distinguish if it is a TD cell with dynamic input and default events
  // dataObject is used to bind data value to object after DOM eg input is updated
  $parentObj.on( 'keyup fieldChange', "[data-" + data_attr + "]", {}, function( evt, type ) {
    // for just previous value

    console.log('keyup fieldChange');
    // console.log("trigger update on " + evt.data.object_id);
    var type = type ? type : 'default', // if the trigger is not coming from makeEditable plugin, no type will be there
        $input = jQuery( this ),
        new_value = '',
        fieldType = $input.attr('type');
    console.log("type is " + type);

    if ( $input.is("input, textarea, select") ) {
      console.log('is input, textarea, select?');
      switch( fieldType ) {
        case 'checkbox':
          break;
        default:
          new_value = $input.val();
          break;
      }
    } else {
      new_value = $input.find('.value:eq(0)').attr('title') || $input.find('input.select-dropdown').val() || $input.find('.value:eq(0)').text() || $input.text();
    }

    // value checking
    // console.log($input.find('input.select-dropdown').val());
    // // console.log($input);
    // console.log("new value from input");
    // console.log($input.find('.value'));
    // console.log($input.find('.value:eq(0)').html());
    // console.log($input.find('.value:eq(0)').text());
    // console.log(new_value);
    // console.log(message);
    console.log('message '+ message);
    pubSub.trigger( message, [ $input.data( data_attr ), new_value, type, $input ] );
  });

  // for custom form object, materializecss, with td wrapper
  $parentObj.on( 'checkboxChange', "[data-" + data_attr + "]", {}, function( evt, type ) {
    console.log('click event, for TD wrapper');
    var $wrapper = jQuery( this ),
        type = type ? type : 'default',
        fieldType = $wrapper.attr('data-field-type');

    switch( fieldType ) {
      case 'checkbox':
        new_value = getCheckValue( $wrapper.find('input') );
        console.log('checkbox new value ' + new_value);
        pubSub.trigger( message, [ $wrapper.data( data_attr ), new_value, type, $wrapper ] );
        break;
      default:
        break;
    }
  });

  // for custom form object, materializecss, non-td wrapper
  $parentObj.on( 'click', "[data-" + data_attr + "]", {}, function( evt ) {
    // console.log('click event (non-td wrapper)');
    var $input = jQuery( this ),
        type = type ? type : 'default',
        fieldType = $input.attr('data-field-type');

    switch( fieldType ) {
      case 'checkbox':
        new_value = getCheckValue( $input );
        console.log('checkbox new value ' + new_value);
        pubSub.trigger( message, [ $input.data( data_attr ), new_value, type, $input ] );
        break;
      default:
        break;
    }
  });

  // PubSub propagates changes to all bound elements, setting value of
  // input tags or HTML content of other tags
  // init
  // type and bindkey is only for item.set
  pubSub.on( message, {}, function( evt, prop_name, new_val, type, $currentObj ) {
    $currentObj ? $currentObj : ''
    // console.log('new_val ' + new_val);
    console.log('type ' + type);
    console.log(prop_name);
    console.log("[data-" + data_attr + "=" + prop_name + "]");
    console.log(jQuery( "[data-" + data_attr + "=" + prop_name + "]" ));
    console.log(jQuery( "[data-" + data_attr + "=" + prop_name + "]" ).length + ' matched object(s)');
    // console.log(arguments);
    // console.log(message);
    // console.log(type);
    // console.log("init trigger type inside on " + type);
    // console.log("triggered");
    jQuery( "[data-" + data_attr + "=" + prop_name + "]" ).each( function() {
      var $bound = jQuery( this ),
          fieldType = $bound.data('field-type') ? $bound.data('field-type') : '',
          fieldStyle = $bound.data('field-style') ? $bound.data('field-style') : '';
      
      // var bindKey = bindKey ? bindKey : $bound.data('bind-'+object_id); // either item.set or get from object
      console.log('--------------------looping object--------------');
      console.log($bound);
      console.log('update field type ' + fieldType);
      console.log('update field style ' + fieldStyle);
      // console.log($bound.is('td')); // as a cell checker
      // console.log( $bound );
      // console.log( $currentObj );
      console.log(dataObject);
      bindValToObj( dataObject, prop_name, new_val );
      
      // when $currentObj is jQuery object and their id is same, then they are the same object, suppose ID is not duplicatedly used
      if( $currentObj instanceof $ && $($bound).attr('id') === $($currentObj).attr('id') ) {
        console.log('will do nothing on the same DOM object after bind to data object. Save Performance');
        return;
      }

      // if it is triggered by cell, no need to update since when blur, value is copied to cell from dynamic input by makeEditable plugin
      // this can save some performance
      // console.log(type != 'default');
      if( type != 'default' ){
        console.log('will do nothing, leave it for cell default event handler');
        return;
      };

      // original idea is doing the switch case for cell update but it is redundant since cell have dynamic input and respective event to update when blur or click in body
      // normal form element
      if ( $bound.is( 'input, textarea, select' ) ) {
        $bound.val( new_val );
      } 

      // materializecss element
      // if element is in TD, form field is manipulated by makeEditable plugin, to make it compatible, handle it separately
      if ( $bound.is( 'td' ) ) {
        console.log( 'bind to TD element, not necessary in most of the case unless set value to TD element using Item.set(), when initiated by input field other than fields in TD, will trigger to update binded TD fields as well' );
        switch( fieldType ) {
          case 'dropdown':
            switch( fieldStyle ) {
              case 'color':
                console.log('bind color to td color');
                $bound.html( '<div class="color-box value '+new_val+'" title="'+new_val+'"></div>' );
                break;
              default:
                $bound.html( '<span class="value">'+new_val+'</span>' );
                break;
            }
            break;
          case 'checkbox':
            console.log('set td checkbox?');

            $bound.html( '<i class="md-icon value" title="'+ String( getCheckBool( new_val ) ) +'">'+setCheckField( new_val )+'</i>' );
            break;
          default:
            $bound.html( new_val );
            break;
        } // switch
      }
      // if element is ordinary materialzecss form element without using makeEditable plugin
      else {
        switch( fieldType ) {
          case 'dropdown':
            switch( fieldStyle ) {
              case 'color':
                // update method for TD cell but it is not necessary since there is event taking care of table input field when they are blur
                // $bound.html( '<div class="color-box value '+new_val+'" title="'+new_val+'"></div>' );
                // $bound.html( '<div class="color-box value '+new_val+'" title="'+new_val+'"></div>' );
                updateDisplayField( $bound.find('.select-wrapper input.select-dropdown'), new_val );
                break;
              default:
                // $bound.html( '<span class="value">'+new_val+'</span>' );
                console.log( new_val );
                console.log($bound.find( 'input' ));
                $bound.find( 'input' ).val( new_val );
                break;
            }
            break;
          case 'checkbox':
            console.log('set other checkbox?');
            setCheckValue( $bound, new_val );
            break;
          default:
            $bound.html( new_val );
            break;
        } // switch
      }
    });
  });

  // $parentObj.off( 'keyup fieldChange' );
  return pubSub;
}

function Item( uid, dataObject, $parentObj ) {
  var $parentObj = $parentObj ? $parentObj : $('#item-list-container'),
      dataObject = dataObject ? dataObject : {},
      binder = new DataBinder( uid, dataObject, $parentObj ),

      item = {
        attributes: {},

        // The attribute setter publish changes using the DataBinder PubSub
        set: function( attr_name, val ) {
          var type = type ? type : 'default';
          console.log('set data');
          this.attributes[ attr_name ] = val;
          console.log('>>>>============================set ' + attr_name);
          console.log(val);
          binder.trigger( uid + ":change", [ attr_name, val, type, this ] );
        },

        get: function( attr_name ) {
          return this.attributes[ attr_name ];
        },

        _binder: binder
      };

  // Subscribe to the PubSub, seems no use
  /*binder.on( uid + ":change", function( evt, attr_name, new_val, initiator ) {
    // console.log(new_val);
    console.log("second on");
    console.log(uid + ":change");
    console.log('bind name ' + attr_name);
    // console.log(initiator);
    // console.log(arguments);
    // console.log(item);
    // console.log(initiator); // undefined if from keyboard instead of calling user.set()
    // console.log(typeof initiator === 'undefined');
    if ( initiator !== item && typeof initiator !== 'undefined' ) {
      item.set( attr_name, new_val ); // trigger set if it is not by input nor user set function
    }
  });*/

  return item;
}

// place an object with reference object and key
// eg data = {test1: 1234, test2: 1234, test3: 1234} => bindValToObj( {dataObj: data, key: test1} )
// similar: helper.bindData2Way (write value to DOM), DataBinder(write value from DOM to object or vice versa)
function bindValToObj( obj, key, val ) {
  console.log('bind value to object');
  // console.log(obj);
  console.log('================================');
  console.log('bind key: ' + key);
  console.log('bind value: ' + val);
  console.log(obj);
  console.log('================================');
  // return;
  if( typeof obj != 'object' )
      return;
  switch( key ) {
    case '001': // english
    case 'name2': // english
      console.log("name2222222222222222222222222222222222222");
      console.log(val);
      obj.name['001'] = val;
      break;
    case '003': // chinese
    case 'name1': // chinese
      console.log('name1 here?');
      obj.name['003'] = val;
      break;
    case 'category': // chinese
      console.log('binding category');
      obj.group = val;
      break;
    default:
      console.log('binding default');
      console.log(obj[key]);
      obj[key] = val;
      break;
  }
  // console.log(obj);
  // if(typeof obj.dataObj === 'object' && obj.key != 'undefined')
  //   dataObj[key] = val;
}

