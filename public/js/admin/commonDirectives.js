(function () {var app = angular.module('common.directives', []);

    /*
        generic directives
    */
    app.directive('pagerCustom', ['pagerConfig', function(pagerConfig) {
      return {
        restrict: 'EA',
        scope: {
          gridHeight: '=?gridHeight', // optional
          iconSize: '=?iconSize', // optional
          colNo: '=?colNo', // optional
          totalItems: '=',
          previousText: '@',
          nextText: '@'
        },
        require: ['pagerCustom', '?ngModel'],
        controller: 'PaginationController',
        // templateUrl: 'tmpl/pager.html',
        templateUrl: function(elem, attr){
        // console.log(attr.template)
          return attr.template || 'tmpl/pager-default.html';
        },
        replace: true,
        link: function(scope, element, attrs, ctrls) {
          var paginationCtrl = ctrls[0], ngModelCtrl = ctrls[1];
          // scope.gridHeight = scope.gridHeight ? scope.gridHeight : "88px"; //default
          // scope.iconSize = scope.iconSize ? scope.iconSize : "70px"; //default
          // console.log(angular.element(element).find("md-grid-list").attr("md-row-height", "88px"));

          if (!ngModelCtrl) {
             return; // do nothing if no ng-model
          }

          scope.align = angular.isDefined(attrs.align) ? scope.$parent.$eval(attrs.align) : pagerConfig.align;
          paginationCtrl.init(ngModelCtrl, pagerConfig);
        }
      };
    }]);

    app.directive('customAlwaysBlur', function ($timeout, $parse) {
        return {
            //scope: true,   // optionally create a child scope
            link: function (scope, element, attrs) {
                var model = $parse(attrs.customAlwaysBlur);
                // console.log(attrs.customAlwaysBlur);
                scope.$watch(model, function (value) {
                    //console.log('value=', value);
                    if (value === true) {
                        $timeout(function () {
                            element[0].blur();
                            // console.log(element[0]);
                        });
                    }
                });
                element.bind('focus', function () {
                    //console.log('blur');
                    //scope.$apply(model.assign(scope, false));
                    element[0].blur();
                });
            }
        };
    });

    app.directive('customAlwaysFocus', function ($timeout, $parse) {
        return {
            //scope: true,   // optionally create a child scope
            link: function (scope, element, attrs) {
                var model = $parse(attrs.ngAlwaysFocus);
                scope.$watch(model, function (value) {
                    //console.log('value=', value);
                    if (value === true) {
                        $timeout(function () {
                            element[0].focus();
                        });
                    }
                });
                element.bind('blur', function () {
                    //console.log('blur');
                    //scope.$apply(model.assign(scope, false));
                    element[0].focus();
                });
            }
        };
    });

    /**
     * the HTML5 autofocus property can be finicky when it comes to dynamically loaded
     * templates and such with AngularJS. Use this simple directive to
     * tame this beast once and for all.
     *
     * Usage:
     * <input type="text" autofocus>
     * 
     * License: MIT
     */
    angular.module('utils.autofocus', [])

    .directive('autofocus', ['$timeout', function($timeout) {
      return {
        restrict: 'A',
        link : function($scope, $element) {
          $timeout(function() {
            $element[0].focus();
          });
        }
      }
    }]);

    // custom repeat watch source - inform change notification
    app.directive('onFinishRender', function ($timeout) {
        return {
            restrict: 'A',
            link: function (scope, element, attr) {
                // console.log('called me?');
                // console.log('make false');
                // console.log(element.scope());
                // console.log(element.parent().scope());

                if (scope.$first === true) {
                    element.parent().hide();
                    element.parent().siblings().hide();
                    scope.$emit('reRender');
                }

                if (scope.$last === true) {
                    $timeout(function () {
                        element.parent().show();
                        element.parent().siblings().show();
                        scope.$emit('LastElem');
                    });
                }
            },
            controller: function ($scope) {
                // $scope.renderCompleted = false;
            }
        }
    });

    // custom repeat watch target - receive change notification
    app.controller('customNgRepeatWatcher', function($scope){
        $scope.$on('reRender', function(event){
          $scope.renderCompleted = false;
        });
        $scope.$on('reRender', function(event){
          $scope.renderCompleted = true;
        });
    });

    /*
     * Checks every $digest for height changes
     */
    app.directive( 'emHeightSource', function() {

        return {
            link: function( scope, elem, attrs ) {

                scope.$watch( function() {
                    scope.__height = elem.height();
                    console.log('test');
                } );
            }
        }

    });

    app.directive('includeReplace', function () {
        return {
            require: '^ngInclude',
            restrict: 'A', /* optional */
            link: function (scope, el, attrs) {
                el.replaceWith(el.children());
            }
        };
    });

    app.directive('finishOption', function() {
      return {
        restrict: 'E',
        replace: true,
        // transclude: true,
        scope: {
          gridHeight: '=?gridHeight',
          gridCol: '=?gridCol',
          back: '=?back',
          cancel: '=?cancel',
          done: '=?done'
        },
        templateUrl: 'tmpl/finish.html',
        link: function(scope, element, attrs, transclude) {
            // console.log(typeof scope.gridHeight);
            console.log(scope);
            if( typeof scope.gridCol == 'undefined')
                scope.gridCol = 2;

            if( typeof scope.gridHeight == 'undefined')
                scope.gridHeight = "100px";

            if( typeof scope.back == 'undefined')
                scope.back = false;

            if( typeof scope.cancel == 'undefined')
                scope.cancel = false;

            if( typeof scope.done == 'undefined')
                scope.done = true;
            // console.log(scope.title);
            // transclude(scope,function(clone) {
            //     // copy attributes to transcluded item
            //     for(var k=0; k < element[0].attributes.length; k++) {
            //         clone.attr( element[0].attributes[k].name, element[0].attributes[k].value );
            //     }
            //     element.append(clone);
            // });
        }
      };
    });

	  app.directive('customDraggable', function ($document) {
        return {
            link: function (scope, element, attr) {
                var startX = 0, startY = 0, x = 0, y = 0;

                element.css({
                    position: 'absolute',
                    border: '1px solid red',
                    backgroundColor: 'lightgrey',
                    cursor: 'pointer',
                    zIndex: '1000'
                });

                element.on('mousedown', function (event) {
                    // Prevent default dragging of selected content
                    event.preventDefault();
                    startX = event.pageX - x;
                    startY = event.pageY - y;
                    $document.on('mousemove', mousemove);
                    $document.on('mouseup', mouseup);
                });

                function mousemove(event) {
                    y = event.pageY - startY;
                    x = event.pageX - startX;
                    element.css({
                        top: y + 'px',
                        left: x + 'px'
                    });
                }

                function mouseup() {
                    $document.off('mousemove', mousemove);
                    $document.off('mouseup', mouseup); // unregister mouseup, clean up memory
                }
            }
        };
    });

    app.directive('gridMenuOption', function(MainService) {
      return {
        restrict: 'E',
        replace: true,
        // transclude: true,
        scope: {
          gridHeight: '=?gridHeight',
          gridCol: '=?gridCol',
          back: '=?back',
          cancel: '=?cancel',
          done: '=?done',
          flex: '=?flex',
          layout: '=?layout'
        },
        // templateUrl: 'tmpl/gridMenuOption.html',
        templateUrl: function(elem, attr){
        // console.log(attr)
          return attr.template;
        },
        link: function(scope, elem, attrs, transclude) {
          // scope.hahaha = '1234';
            // console.log(typeof scope.gridHeight);
            // console.log(scope);
            scope.MainService = MainService;
            if( typeof scope.gridCol == 'undefined')
                scope.gridCol = 2;

            if( typeof scope.gridHeight == 'undefined')
                scope.gridHeight = "100px";

            if( typeof scope.back == 'undefined')
                scope.back = false;

            if( typeof scope.cancel == 'undefined')
                scope.cancel = false;

            if( typeof scope.done == 'undefined')
                scope.done = true;

            // for "fit" grid-row of md-grid-list, the manual bug-fixed version work well with parent having "layout" and itself having "flex"
            if( angular.isDefined( scope.layout ) )
                elem.attr('layout', scope.layout);

            if( angular.isDefined( scope.flex ) ) {
                elem.find('md-grid-list').attr('flex', scope.flex);
                elem.removeAttr( 'flex' ); // if parent have "flex", "fit" won't work properly since it cannot get the height of parent
            }

            // for generating an array to repeat dummy
            scope.numberToArray = function( num ) {
                return new Array( num );
            }
        },
        // ,
        controller: 'gridMenuOptionController'
      };
    });

    app.controller('gridMenuOptionController', function($scope, $mdToast, uiLanguage, INFO ) {
      console.log($scope);
    });

    // performance tweak
    /*
      ng-include lite
      author: http://zachsnow.com/#!/blog/2014/angularjs-faster-ng-include/
      demo: http://plnkr.co/edit/dTUUPhsVrCbeSIjZzqmx?p=preview
    */
    app.directive('ngCompiledInclude', [
      '$compile',
      '$templateCache',
      function($compile, $templateCache) {
        console.log($templateCache);
        return {
          restrict: 'A',
          priority: 400,
          compile: function(element, attrs){
            var templateName = attrs.ngCompiledInclude;
            var template = $templateCache.get(templateName);
            // console.log(attrs.ngCompiledInclude);
            // console.log(template);
            element.html(template);
          }
        };
      }
    ]);

    app.directive('optionFoodControl', function( MainService ) {
        return {
            restrict: "E",
            templateUrl: 'tmpl/optionsFoodControl.html',
            link: function (scope, element, attrs) {
                scope.itemsPerPage = 5*5 - 2;
                scope.currentPage = 1;
                scope.totalItems = 1;
                
            },
            controller: function( $scope ) {
                
            }
        }
    });

    app.directive('foodOptions', function( MainService ) {
        return {
            restrict: "E",
            templateUrl: 'tmpl/options.html',
            link: function (scope, element, attrs) {
                scope.itemsPerPage = 5*5 - 2;
                scope.currentPage = 1;
                scope.totalItems = 1;
                
            },
            controller: function( $scope ) {
                
            }
        }
    });

    /*
      based on angular material 0.10.0
      make a lite version of mdCheckbox with Materialize CSS 0.97.0 (css based material design solution)
      removed: aria, ripple, theme
    */
    app.directive('mcssCheckbox', McssCheckboxDirective);
    /**
     * @ngdoc directive
     * @name mdCheckbox
     * @restrict E
     *
     * @description
     * The checkbox directive is used like the normal [angular checkbox](https://docs.angularjs.org/api/ng/input/input%5Bcheckbox%5D).
     *
     * As per the [material design spec](http://www.google.com/design/spec/style/color.html#color-ui-color-application)
     * the checkbox is in the accent color by default. The primary color palette may be used with
     * the `md-primary` class.
     *
     * @param {string} ng-model Assignable angular expression to data-bind to.
     * @param {string=} name Property name of the form under which the control is published.
     * @param {expression=} ng-true-value The value to which the expression should be set when selected.
     * @param {expression=} ng-false-value The value to which the expression should be set when not selected.
     * @param {string=} ng-change Angular expression to be executed when input changes due to user interaction with the input element.
     * @param {boolean=} md-no-ink Use of attribute indicates use of ripple ink effects
     * Defaults to checkbox's text. If no default text is found, a warning will be logged.
     *
     * @usage
     * <hljs lang="html">
     * <md-checkbox ng-model="isChecked" aria-label="Finished?">
     *   Finished ?
     * </md-checkbox>
     *
     *
     * <md-checkbox ng-disabled="true" ng-model="isDisabled" aria-label="Disabled">
     *   Disabled
     * </md-checkbox>
     *
     * </hljs>
     *
     */
    function McssCheckboxDirective(inputDirective, $mdConstant, $mdUtil, $timeout) {
      inputDirective = inputDirective[0];
      var CHECKED_CSS = 'md-checked';

      return {
        restrict: 'E',
        transclude: true,
        require: '?ngModel',
        priority:210, // Run before ngAria
        template: 
            '<div class="mcss-checkbox">' +
              '<input type="checkbox" class="filled-in"/>' +
                '<label ng-transclude></label>' +
            '</div>',

          // '<div class="md-container" md-ink-ripple md-ink-ripple-checkbox>' +
          //   '<div class="md-icon"></div>' +
          // '</div>' +
          // '<div ng-transclude class="md-label"></div>',
        compile: compile
      };

      // **********************************************************
      // Private Methods
      // **********************************************************

      function compile (tElement, tAttrs) {

        tAttrs.type = 'checkbox';
        tAttrs.tabindex = tAttrs.tabindex || '0';
        tElement.attr('role', tAttrs.type);

        return function postLink(scope, element, attr, ngModelCtrl) {
          ngModelCtrl = ngModelCtrl || $mdUtil.fakeNgModel();
          // $mdTheming(element);

          if (attr.ngChecked) {
            scope.$watch(
                scope.$eval.bind(scope, attr.ngChecked),
                ngModelCtrl.$setViewValue.bind(ngModelCtrl)
            );
          }
          $$watchExpr('ngDisabled', 'tabindex', {
            true: '-1',
            false: attr.tabindex
          });
          // $mdAria.expectWithText(element, 'aria-label');

          // Reuse the original input[type=checkbox] directive from Angular core.
          // This is a bit hacky as we need our own event listener and own render
          // function.
          inputDirective.link.pre(scope, {
            on: angular.noop,
            0: {}
          }, attr, [ngModelCtrl]);

          scope.mouseActive = false;
          element.on('click', listener)
            .on('keypress', keypressHandler)
            .on('mousedown', function() {
              scope.mouseActive = true;
              $timeout(function(){
                scope.mouseActive = false;
              }, 100);
            })
            .on('focus', function() {
              if(scope.mouseActive === false) { element.addClass('md-focused'); }
            })
            .on('blur', function() { element.removeClass('md-focused'); });

          ngModelCtrl.$render = render;

          function $$watchExpr(expr, htmlAttr, valueOpts) {
            if (attr[expr]) {
              scope.$watch(attr[expr], function(val) {
                if (valueOpts[val]) {
                  element.attr(htmlAttr, valueOpts[val]);
                }
              });
            }
          }

          function keypressHandler(ev) {
            var keyCode = ev.which || ev.keyCode;
            if (keyCode === $mdConstant.KEY_CODE.SPACE || keyCode === $mdConstant.KEY_CODE.ENTER) {
              ev.preventDefault();
              if (!element.hasClass('md-focused')) { element.addClass('md-focused'); }
              listener(ev);
            }
          }
          function listener(ev) {
            if (element[0].hasAttribute('disabled')) return;

            scope.$apply(function() {
              // Toggle the checkbox value...
              var viewValue = attr.ngChecked ? attr.checked : !ngModelCtrl.$viewValue;

              ngModelCtrl.$setViewValue( viewValue, ev && ev.type);
              ngModelCtrl.$render();
            });
          }

          function render() {
            if(ngModelCtrl.$viewValue) {
              element.addClass(CHECKED_CSS);
              element.find("input[type=checkbox]").prop("checked", true)
              console.log(element.find("checkbox"));
            } else {
              element.removeClass(CHECKED_CSS);
              element.find("input[type=checkbox]").prop("checked", false)
            }
          }
        };
      }
    }
    McssCheckboxDirective.$inject = ["inputDirective", "$mdConstant", "$mdUtil", "$timeout"];
    // McssCheckboxDirective.$inject = ["inputDirective", "$mdAria", "$mdConstant", "$mdTheming", "$mdUtil", "$timeout"];

    // performance check
    // limitation: cannot time ui-grid correctly if there is no timing in other directive to delay the loading because it is not watch, due to ngInclude
    app.directive('ngTimeit', [
      '$parse',
      '$timeout',
      function($parse, $timeout){
        return {
          restrict: 'A',
          link: function(scope, element, attrs){
            var modelFn = $parse(attrs.ngTimeit);
            var then = new Date();
            $timeout(function(){
              var now = new Date();
              modelFn.assign(scope, (now - then) / 1000.0); // copy the time(eg data.timing) to scope object
              console.log( modelFn.assign(scope, (now - then) / 1000.0) );
            }, 100);
          }
        };
      }
    ]);

    app.controller('calendarCtrl', calendarCtrl)
    app.directive('calendar', calendar)
    app.directive('day', day);

    function calendarCtrl($scope, $timeout, $element) {
      console.log($scope);
      // console.log('called once');
      var ctrl = $scope.ctrl = this;
      $scope.$watch('ctrl.day', function () { ctrl.updateDate(ctrl); });
      
      this.months = 'January February March April May June July August September October November December'.split(' ');
      this.day = new Date();
      this.preDays = [];
      this.days = [];
      this.postDays = [];
      this.headings = [''];
      this.monthName = '';
      this.back = false;
      this.locked = false;
      // this.lockTime = 500 + 350 * 2;
      this.lockTime = 0;
      // console.log(ctrl);
      // $scope.test = 1;

      
      this.next = function () {
        if (this.locked) return;
        this.locked = true;
        $timeout(function () { this.locked = false; }.bind(this), this.lockTime);
        this.day.setMonth(this.day.getMonth() + 1);
        this.back = false;
        $element.removeClass('back');
        this.updateDate();

        console.log(ctrl.preDays.length);
        console.log(ctrl.days.length);
        console.log(ctrl.postDays.length);
      };
      this.prev = function () {
        if (this.locked) return;
        this.locked = true;
        $timeout(function () { this.locked = false; }.bind(this), this.lockTime);
        this.day.setMonth(this.day.getMonth() - 1);
        this.back = true;
        $element.addClass('back');
        this.updateDate();
      };
      this.updateDate = function (day) {
        this.day.setDate(1);
        this.preDays = this.getDays(this.day.getDay(), 0);
        this.days = this.getDays(this.getDaysInMonth(), this.preDays.length);
        this.postDays = this.getDays(
          (7 - (this.preDays.length + this.days.length) % 7) % 7,
          this.preDays.length + this.days.length
        );
        this.monthName = this.months[this.day.getMonth()];
        this.headings = [{ month: this.monthName, year: this.day.getFullYear() }];
        ctrl.month = this.day.getMonth() + 1;
        ctrl.year = this.day.getFullYear();
      };
      this.setDelay = function (item) {
        // console.log('delay');
        var left = item.prop('offsetLeft') - $element.prop('offsetLeft'),
            top = item.prop('offsetTop') - $element.prop('offsetTop'),
            dist = Math.sqrt(left * left + top * top),
            delay = dist * 0.75;
        item.css('transition-delay', delay + 'ms');
      };
      this.getDays = function (count, start) {
        var days = [], i;
        for (i = start; i < count + start; i++) {
          days.push({ col: i % 7, row: Math.floor(i / 7) });
        }
        return days;
      };
      this.getDaysInMonth = function () {
        switch (this.day.getMonth()) {
          case 1: return new Date(this.day.getFullYear(), 1, 29).getMonth() == 1 ? 29 : 28;
          case 3: case 5: case 8: case 10: return 30;
          default: return 31;
        }
      };
    }
    function calendar() {
      return {
        template: '\
          <header>\
            <a ng-click="ctrl.prev()" class="back" ng-class="{ disabled: ctrl.locked }"></a>\
            <div class="date-wrapper">\
              <span class="title" ng-repeat="heading in ctrl.headings">{{heading.month}} {{heading.year}}</span>\
            </div>\
            <a ng-click="ctrl.next()" class="forward" ng-class="{ disabled: ctrl.locked }"></a>\
          </header>\
          <section class="day-header">\
            <div class="day">Sun</div>\
            <div class="day">Mon</div>\
            <div class="day">Tue</div>\
            <div class="day">Wed</div>\
            <div class="day">Thu</div>\
            <div class="day">Fri</div>\
            <div class="day">Sat</div>\
          </section>\
          <section class="clearfix day-content" ng-class="{\'row6\' : (ctrl.preDays.length + ctrl.days.length + ctrl.postDays.length) > 35 }">\
            <day ng-repeat="day in ctrl.preDays" class="inactive" data-col="{{day.col}}" data-row="{{day.row}}"></day>\
            <day ng-repeat="day in ctrl.days" data-col="{{day.col}}" data-row="{{day.row}}" ng-click="setReportDate(\'{{ctrl.year}}\',\'{{ctrl.month}}\',\'{{$index + 1}}\', $event);">{{$index + 1}}</day>\
            <day ng-repeat="day in ctrl.postDays" class="inactive" data-col="{{day.col}}" data-row="{{day.row}}"></day>\
          </section>\
        ',
        controller: 'calendarCtrl'
      };
    }
    function day($timeout, $window) {
      return { require: '^?calendar', link: link };
      function link(scope, element, attr, ctrl) {}
    }

    // deprecated, reserved for 1-2 version, removed later for compatibility reason, not suggested to use "ng" as initial to prevent future official changes
    /*app.directive('ngAlwaysFocus', function ($timeout, $parse) {
        return {
            //scope: true,   // optionally create a child scope
            link: function (scope, element, attrs) {
                var model = $parse(attrs.ngAlwaysFocus);
                scope.$watch(model, function (value) {
                    //console.log('value=', value);
                    if (value === true) {
                        $timeout(function () {
                            element[0].focus();
                        });
                    }
                });
                element.bind('blur', function () {
                    //console.log('blur');
                    //scope.$apply(model.assign(scope, false));
                    element[0].focus();
                });
            }
        };
    });*/
    //------------------------------------------------------------

})();