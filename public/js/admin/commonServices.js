(function(){

'use strict';

  angular.module('common.services', [])
  .factory('menu', function ($location, $rootScope, $mdSidenav, $log, uiLanguage, INFO) {
// console.log('called menu');
  
        // var sections = [{
        //   name: 'Getting Started',
        //   state: 'home.gettingstarted',
        //   type: 'link'
        // }];
        var self = {};
        var mainMenuList = [];
        var ui = uiLanguage[INFO.lang];

        // settings
        // order
        // order management
        // table management (edit)
        // staff management

        mainMenuList.push({
          label: 'main_page',
          type: 'function',
          mode: {
            mode: 'main', 
            modeNavMenu: 'normal'
          }, 
          requiredLogin: false, 
          config: true, 
          col: 3, 
          permission: 'all'
        });

        // item_settings
        mainMenuList.push({
          label: 'item_settings',
          type: 'toggle',
          pages: [{ 
              label: 'item_admin', 
              type: 'function',
              mode: { 
                mode: 'itemAdmin' 
              }, 
              requiredLogin: true, 
              config: true, 
              row: 2, 
              permission: 'all' 
            }, { 
              label: 'category_admin', 
              type: 'function',
              mode: { 
                mode: 'categoryAdmin' 
              }, 
                requiredLogin: true, 
                config: true, 
                row: 2, 
                permission: 'all' 
            }, { 
              label: 'modifier_admin', 
              type: 'function',
              mode: { 
                mode: 'modifierAdmin' }, 
              requiredLogin: true, 
              config: true, 
              row: 2, 
              permission: 'all' 
            }, { 
              label: 'option_admin', 
              type: 'function',
              mode: { 
                mode: 'optionAdmin' 
              }, 
              requiredLogin: true, 
              config: true, 
              row: 2, 
              permission: 'all' 
            },
            {
              label: 'subitem_admin', 
              type: 'function',
              mode: { 
                mode: 'subitemAdmin' 
              }, 
              requiredLogin: true, 
              config: true, 
              row: 2, 
              permission: 'all' 
            },
            {
                label: 'price_admin',
                type: 'function',
                mode: {
                    mode: 'priceAdmin'
                },
                requiredLogin: true,
                config: true,
                row: 2,
                permission: 'all'
            },
            { label: 'food_control', type: 'function', menuGroup: '', mode: { mode: 'order', modeOrder: 'foodControl', modeOrderMenu: 'normal', modeNavMenu: 'normal', modeFloorPlan: 'order', modeItem: 'normal' }, requiredLogin: true, config: true, col: 4, permission: 'all' },
               { label: 'time_range_setting', type: 'function', mode: { mode: 'timeRangeSetting' }, requiredLogin: true, config: true, row: 2, permission: 'all' },
            { 
              label: 'menu_layout_admin', 
              type: 'function',
              mode: { 
                mode: 'menuLayoutList'
              }, 
              requiredLogin: true, 
              config: true,
              row: 2, 
              permission: 'all' 
          }]});

        // mainMenuList.push({
        //   label: 'order_management',
        //   type: 'toggle',
        //   pages: [
        //   {
        //     label: 'search_bill', 
        //     type: 'function',
        //     mode: {
        //       mode: 'order', 
        //       modeOrder: 'searchBill', 
        //       modeOrderMenu: 'billMenu', 
        //       modeNavMenu: 'normal'
        //     }, 
        //     requiredLogin: true, 
        //     config: true, 
        //     col: 4, 
        //     permission: 'checkBill'
        //   }, 
        //   {
        //     label: 'report', 
        //     type: 'function',
        //     mode: {
        //       mode: 'report', 
        //       modeNavMenu: 'normal'
        //     }, 
        //     requiredLogin: true, 
        //     config: true, 
        //     col: 2, 
        //     permission: 'report'
        //   }
        //   // , 
        //   // {
        //   //   label: 'daily_clearance', type: 'function', mode: {mode: 'dailyClearance', modeNavMenu: 'normal'}, requiredLogin: true, config: true, col: 2, permission: 'dailyClearance'}
        //   ]});

        // mainMenuList.push({
        //   label: 'system_settings',
        //   type: 'toggle',
        //   pages: [
        //   { label: 'settings', type: 'function', mode: { mode: 'settings' }, requiredLogin: true, config: true, col: 4, permission: 'all' },
        //   { label: 'Download_Data', type: 'function', mode: { mode: 'testing', modeFloorPlan: '', modeOrderMenu: '', modeNavMenu: '' }, requiredLogin: true, config: true, col: 2, permission: 'tableManagement' }
        // ]});

        // user_management
        // mainMenuList.push({
        //   label: 'user_management',
        //   type: 'toggle',
        //   pages: [
        //   {label: 'user_management', type: 'function', mode: {mode: 'userManagement', modeNavMenu: 'normal'}, requiredLogin: true, config: true, col: 5, permission: 'userManagement'},
        //   {label: 'user_group', type: 'function', mode: {mode: 'userGroupManagement', modeNavMenu: 'normal'}, requiredLogin: true, config: true, col: 5, permission: 'userManagement'}
        // ]});

        // mainMenuList.push({
        //   label: 'group template',
        //   type: 'toggle',
        //   pages: [
        // ]});

        // mainMenuList.push({
        //   label: 'group template',
        //   type: 'toggle',
        //   pages: [
        // ]});
        // mainMenuList.push();

        // console.log(self);
          
        // dynamic adding function if action function is not explicitly added in obove list
        for (var i = 0; i < mainMenuList.length; i++) {
          if( mainMenuList[i].type != 'toggle' ) {
            if( angular.isUndefined( mainMenuList[i].func ) ) {
              // console.log('yeah undefined');
              mainMenuList[i].func = function() {
                $mdSidenav('left').close();

                self.switchMode3(this.mode, this.requiredLogin, this.label, this.label, this.permission);
              }
            } else {
              // console.log('yeah defined');
              
            }
          } else {
            for (var p = 0; p < mainMenuList[i].pages.length; p++) {
              mainMenuList[i].pages[p].func = function() {
                $mdSidenav('left').close();
                //   .then(function () {
                //     $log.debug("close LEFT is done");
                // });

                  console.log(self.switchMode3);

                self.switchMode3(this.mode, this.requiredLogin, this.label, this.label, this.permission);
              }
            }
          }
        }

        // console.log(mainMenuList);

          /*
          {label: 'user_clock_management', menuGroup: '', mode: {mode: 'userClockManagement', modeNavMenu: 'normal'}, requiredLogin: false, config: true, col: 1, permission: 'all'}*/

        // sections.push({
        //   name: 'Beers',
        //   type: 'toggle',
        //   pages: [{
        //     name: 'IPAs',
        //     type: 'link',
        //     state: 'home.beers.ipas',
        //     icon: 'fa fa-group'
        //   }, 
        //   {
        //     name: 'Porters',
        //     state: 'home.beers.porters',
        //     type: 'link',
        //     icon: 'fa fa-map-marker'
        //   },
        //   {
        //     name: 'Wheat',
        //     state: 'home.beers.wheat',
        //     type: 'link',
        //     icon: 'fa fa-plus'
        //   }]
        // });

        // sections.push({
        //   name: 'Munchies',
        //   type: 'toggle',
        //   pages: [{
        //     name: 'Cheetos',
        //     type: 'link',
        //     state: 'munchies.cheetos',
        //     icon: 'fa fa-group'
        //   }, {
        //     name: 'Banana Chips',
        //     state: 'munchies.bananachips',
        //     type: 'link',
        //     icon: 'fa fa-map-marker'
        //   },
        //     {
        //       name: 'Donuts',
        //       state: 'munchies.donuts',
        //       type: 'link',
        //       icon: 'fa fa-map-marker'
        //     }]
        // });

        // sections.push({
        //   name: 'test',
        //   type: 'heading',
        //   pages: []
        // });

        

        return self = {
          // sections: sections,
          mainMenuList: mainMenuList,

          toggleSelectSection: function (section) {
            self.openedSection = (self.openedSection === section ? null : section);
          },
          isSectionSelected: function (section) {
            return self.openedSection === section;
          },

          selectPage: function (section, page) {
            page && page.url && $location.path(page.url);
            self.currentSection = section;
            self.currentPage = page;
          }
        };

        function sortByHumanName(a, b) {
          return (a.humanName < b.humanName) ? -1 :
            (a.humanName > b.humanName) ? 1 : 0;
        }
      });
      
})();