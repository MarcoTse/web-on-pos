﻿(function () {
    var app = angular.module('rms.controllers', ['common.controllers']);

    /*
        dummy controllers to create prototype layout
    */
    var dummyCategories = [
        {name: 'Sushi-好味道'}
    ];
    for(var i=0; i<7; i++) {
        dummyCategories.push({name: dummyCategories[0].name + 'x ' + i});
    }
    // console.log(dummyCategories);

    var dummyCategoryItems = [
        {name: 'Sushi かつお'}
    ];
    for(var i=0; i<17; i++) {
        dummyCategoryItems.push({name: dummyCategoryItems[0].name + 'x ' + i});
    }

    var dummyShortcuts = [
        //{name: '加'}
        //{ name: '' }
    ];
    for(var i=1; i<=8; i++) {
        dummyShortcuts.push({name: i});
    }

    var dummyOptions = [
        {name: '加'}
    ];
    for(var i=0; i<7; i++) {
        dummyOptions.push({name: dummyOptions[0].name + 'x ' + i});
    }

    var dummyDeleteOptions= [];
    for(var i=0; i<7; i++) {
        // String(i)
        dummyDeleteOptions.push({name: '1234' });
    }

    var dummyFoodOptions= [];
    for(var i=0; i<(5*5-3); i++) {
        // String(i)
        dummyFoodOptions.push({name: '1234 Set' });
    }

    // var keypadText = [];
    // var keypadText = [
    //      {text: 'q', useIcon: false}
    // ];



    var modifierOption = [];
    modifierOption.push.apply(modifierOption, [
        { name: 'Show value', element: 'input', type: 'text' }
    ]); // option 1 is showing number
    for (var n = 1; n < 10; n++) {
        modifierOption.push({ name: n, element: 'text', type: '' });
    }
    // modifierOption.push({ name: '', element: 'dummy', type: '' });
    // modifierOption.push({ name: '', element: 'dummy', type: '' });

    var cancelReason = [];
    for(var n=0; n<18; n++) {
        cancelReason.push({name: 'Reason'+n});
    }
    
    var foodTransfer = [];
    for(var n=0; n<6; n++) {
        foodTransfer.push({name: 'Food'+n});
    }

    var tableTransfer = [];
    for(var n=0; n<6; n++) {
        tableTransfer.push({name: 'Table'+n});
    }
    
    var reportOption = [];
    var reportOption = [
        { name: 'daily_report' }
        //,{name: 'sales_report'}
    ];
    // for(var n=0; n<28; n++) {
    //     reportOption.push({name: 'Report'+n});
    // }
    
    app.controller('dummyCtrl', function ($scope, $timeout, uiLanguage, INFO, $http, SocketFactory, MainService, $mdDialog) {
        ctrl = this;
        this.categories = dummyCategories;
        this.categoryItems = dummyCategoryItems;
        this.shortcuts = dummyShortcuts;
        this.deleteOptions = dummyDeleteOptions;
        this.foodOptions = dummyFoodOptions;
        // this.cancelOption = cancelOption;
        this.modifierOption = modifierOption;
        this.cancelReason = cancelReason;
        this.foodTransfer = foodTransfer;
        this.tableTransfer = tableTransfer;
        // this.reportOption = reportOption;
        $scope.MainService = MainService;

        // consolidated to 'reportController'
        /*$http.get('data/dummy_report.json').success(function (data) {
            $scope.dailyReport = data.daily;
            console.log($scope.dailyReport);
            // console.log(data);
        });

        $scope.reportData = {
            NO_OF_TOTAL_AMOUNT: "12345"
        }

        $scope.reportOptionPerPage = 25 - 2;
        $scope.noOfReportOptionPage = Math.ceil( ctrl.reportOption.length / $scope.reportOptionPerPage );
        // console.log($scope.reportOptionPerPage);
        // console.log(ctrl.reportOption.length);
        // console.log($scope.noOfReportOptionPage);
        $scope.currentReport = '';
        $scope.changeReport = function( code_name ) {
            console.log('current report ' + code_name);
            SocketFactory.emit('loadReport', { membercode: $scope.loginValue,isPrint:false }, function (r) {
                console.log(r);
                $scope.reportData = JSON.parse(r);
                $scope.currentReport = code_name;
            })
            // replace report info here
        }
        $scope.printReport = function () {

            SocketFactory.emit('loadReport', { membercode: $scope.loginValue,isPrint:true }, function (r) {
                //console.log(r);
                $scope.reportData = JSON.parse(r);
                $scope.currentReport = code_name;
            })
        }

        $scope.dailyClearance = function () {
            SocketFactory.emit('dailyClearance', { usercode: $scope.MainService.UserManager.staffCode, isPrint: true }, function (r) {
                console.log(r);
                r = JSON.parse(r);
                if (r.result == 'OK') {
                    $scope.reportData = r.resultObj;
                } else {
                    var msg = "請完成所有訂單";
                    if (r.msg == "please_complete_orders") {
                        msg = "請完成所有訂單";
                    } else if (r.msg == "no_order") {
                        msg = "沒有訂單";
                    }
                    $mdDialog.show(
                     $mdDialog.alert()
                       .parent(angular.element(document.body))
                       .title(uiLanguage[INFO.lang]['alert'])
                       //.content(uiLanguage[INFO.lang]['no_order'])
                        .content(msg)
                       // .content('There is no order')
                       .ok(uiLanguage[INFO.lang]['ok'])
                   );
                }
                //$scope.currentReport = code_name;
            })
        }*/

        $scope.ui = uiLanguage[INFO.lang];
        $scope.lang = INFO.lang;
        // console.log(this.ui);

        $scope.triggerKeypad = function( element ){
            $timeout(function() {
                angular.element( element ).trigger('click');
           }, 1000);
        }

        // for generating an array to repeat dummy
        $scope.numberToArray = function( num ) {
            return new Array( num );
        }
    });

    app.controller('modifierCtrl', function ($scope, uiLanguage, INFO, MainService) {
        this.modifierOption = modifierOption;

        // console.log($scope);
        $scope.cancel = function () {
            console.log(98)
            MainService.modeOrder = MainService.schema.modeOrder.normal;
        };

        // console.log('calling modifierCtrl');

        $scope.optionPerPage = 3 * 5 - 2; // row * col - pagination
        $scope.currentPage = 1;
        // if( typeof MainService.food != 'undefined' ) {
        //     // MainService.food.modifiers = MainService.food.modifiers ? MainService.food.modifiers : [];
        //     $scope.noOfPage = Math.ceil( MainService.food.modifiers.length / $scope.optionPerPage );
        //     $scope.reminderPage = MainService.getPageNumber( $scope.optionPerPage - MainService.food.kitchenmsg.length % $scope.optionPerPage );
        // }
        //
        // console.log(typeof MainService.food);
        // console.log(typeof MainService.food.modifiers);
        // $scope.noOfPage = Math.ceil( MainService.food.modifiers.length / $scope.optionPerPage );
        // $scope.reminderPage = MainService.getPageNumber( $scope.optionPerPage - MainService.food.kitchenmsg.length % $scope.optionPerPage );
    });

    app.controller('cancelController', function ($scope, uiLanguage, INFO, MainService) {
        $scope.ui = uiLanguage[INFO.lang];

        var cancelOption = [];
        cancelOption.push.apply(cancelOption, [
            {name: 'Show value', element: 'input', type: 'text'},
            {name: uiLanguage[INFO.lang]['all'], element: 'text', type: ''}
        ]); // option 1 is showing number
        for(var n=1; n<10; n++) {
            cancelOption.push({name: n, element: 'text', type: ''});
        }
        // cancelOption.push({name: 'keyboard', element: 'icon', type: ''});

        this.cancelOption = cancelOption;

        $scope.cancelDeleteItem = function () {
            MainService.modeOrder = MainService.schema.modeOrder.normal;
        }
    });

    app.controller('reportController', function ($scope, $controller, $timeout, $element, $rootElement, uiLanguage, INFO, $http, SocketFactory, MainService, $mdDialog) {
        // $controller('headerCtrl',{$scope: $scope, $element: $element}); // passing current scope to commmon controller

        // $controller('calendarCtrl',{$scope: $scope, $timeout: $timeout, $element: $element}); // not neces...
        ctrl = this;
        this.reportOption = reportOption;

        /*$http.get('data/dummy_report.json').success(function (data) {
            $scope.dailyReport = data.daily;
            console.log($scope.dailyReport);
            // console.log(data);
        });*/

        $scope.currentReport = reportOption[0].name; // default, first report
        SocketFactory.emit('loadReport', { membercode: $scope.loginValue,isPrint:false }, function (r) {
            // console.log(r);
            $scope.reportData = JSON.parse(r);
        })

        // $scope.reportData = {
        //     NO_OF_TOTAL_AMOUNT: "12345"
        // }

        $scope.reportOptionPerPage = 5*5 - 2;
        $scope.noOfReportOptionPage = Math.ceil( ctrl.reportOption.length / $scope.reportOptionPerPage );
        // console.log($scope.reportOptionPerPage);
        // console.log(ctrl.reportOption.length);
        // console.log($scope.noOfReportOptionPage);
        
        $scope.changeReport = function( code_name ) {
            console.log('current report ' + code_name);
            SocketFactory.emit('loadReport', { membercode: $scope.loginValue,isPrint:false }, function (r) {
                console.log(r);
                $scope.reportData = JSON.parse(r);
                $scope.currentReport = code_name;
            })
            // replace report info here
        }

        $scope.printReport = function () {
            if ($scope.year != "") {
                SocketFactory.emit('loadReport', { membercode: $scope.loginValue, isPrint: true, year: $scope.year, month: $scope.month, day: $scope.day }, function (r) {
                    //console.log(r);
                    $scope.reportData = JSON.parse(r);
                    $scope.currentReport = code_name;
                })
            } else {
                SocketFactory.emit('loadReport', { membercode: $scope.loginValue, isPrint: true }, function (r) {
                    //console.log(r);
                    $scope.reportData = JSON.parse(r);
                    // $scope.currentReport = code_name;
                })

            }
        }

        $scope.dailyClearance = function () {
            SocketFactory.emit('dailyClearance', { usercode: $scope.MainService.UserManager.staffCode, isPrint: true }, function (r) {
                console.log(r);
                r = JSON.parse(r);
                if (r.result == 'OK') {
                    $scope.reportData = r.resultObj;
                } else {
                    var msg = "請完成所有訂單";
                    if (r.msg == "please_complete_orders") {
                        msg = "請完成所有訂單";
                    } else if (r.msg == "no_order") {
                        msg = "沒有訂單";
                    }
                    $mdDialog.show(
                     $mdDialog.alert()
                       .parent(angular.element(document.body))
                       .title(uiLanguage[INFO.lang]['alert'])
                       //.content(uiLanguage[INFO.lang]['no_order'])
                        .content(msg)
                       // .content('There is no order')
                       .ok(uiLanguage[INFO.lang]['ok'])
                   );
                }
                //$scope.currentReport = code_name;
            })
        }

        $scope.setActive = function (element, callback) {
            console.log('set active report date');
            // console.log(element);
            if (typeof element === 'object') {
                // treat as event object
                var target = element.currentTarget || element.srcElement;
            } else {
                var target = element;
            }
            // console.log(element);
            // console.log(target);
            if($scope.selectedDateRange.length <= 2) {
             angular.element(target).addClass("selected");
            }

            if($scope.selectedDateRange.length > 2) {
                // console.log('reset active');
             angular.element(target).parents('.day-content').find('.selected').removeClass("selected");
             angular.element(target).addClass("selected");
            }
            
            (callback || angular.noop)();
        }

        /*
            version1: params: (year, month, day)
            version2: support range selection

            1. first click, determine first range,
            2. 2nd click, determine range boundary (starts over again if click again afterward)
            3. click again, go to first click again

            {from: [2015,10,1], to: [2015,10,16]}
        */
        $scope.selectedDateRange = [];
        $scope.dateFrom = $scope.dateTo = '';
        $scope.setReportDate = function( year, month, day, element ) {
            console.log('oh my god');
            console.log($scope);
            console.log(day);

            $scope.selectedDateRange.push({year: year, month: month, day:day});
            if($scope.selectedDateRange.length == 1 || $scope.selectedDateRange.length > 2) {
                // assume from date
                $scope.dateFrom = year + '-' + month + '-' + day;
                $scope.dateTo = '';
            }
            $scope.setActive( element );

            if($scope.selectedDateRange.length == 2) {
                // sort and compare before display
                var d1 = $scope.selectedDateRange[0],
                    d2 = $scope.selectedDateRange[1]
                // console.log(new Date(d1.year, d1.month - 1, d1.day).getTime() > new Date(d2.year, d2.month - 1, d2.day).getTime() );
                if( new Date(d1.year, d1.month - 1, d1.day).getTime() > new Date(d2.year, d2.month - 1, d2.day).getTime() ) {
                    // display d2 in the second place
                    $scope.dateTo = d1.year + '-' + d1.month + '-' + d1.day;
                    $scope.dateFrom = d2.year + '-' + d2.month + '-' + d2.day;
                } else {
                    // display d2 in first and d1 in second
                    $scope.dateFrom = d1.year + '-' + d1.month + '-' + d1.day;
                    $scope.dateTo = d2.year + '-' + d2.month + '-' + d2.day;
                }
            }

            if( $scope.selectedDateRange.length > 2 ) {
                // reset from step 1
                $scope.selectedDateRange = [{year: year, month: month, day:day}];
            };
            // console.log($scope.dateFrom);
            // console.log($scope.dateTo);
            // $mdDialog.hide(); // use confirm button instead

            console.log($scope.dateFrom);
            console.log($scope.dateTo);
            console.log($scope.selectedDateRange);
            // $scope.setHeaderRemark('選擇範圍：2015-10-23 至 2015-10-31');

            

            // $scope.fromYear = option.from[0];
            // $scope.fromMonth = month;
            // $scope.fromDay = day;

            // $scope.toYear = year;
            // $scope.toMonth = month;
            // $scope.toDay = day;
            

            // SocketFactory.emit('loadReport', { membercode: $scope.loginValue, isPrint: false, year: $scope.year, month: $scope.month, day: $scope.day }, function (r) {
            //     console.log(r);
            //     $scope.reportData = JSON.parse(r);
            //     // $scope.currentReport = code_name;
            // })
        }

        $scope.setReport = function() {
            // submit date and close dialog
            console.log($scope.dateFrom);
            console.log($scope.dateTo);
            $mdDialog.hide();
            var postObj = { membercode: $scope.loginValue, isPrint: false };
            if ($scope.selectedDateRange.length > 0) {
                var dateFromSplit = $scope.dateFrom.split('-');
                postObj.year = dateFromSplit[0];
                postObj.month = dateFromSplit[1];
                postObj.day = dateFromSplit[2];
                if ($scope.selectedDateRange.length > 1) {
                    var dateToSplit = $scope.dateTo.split('-');
                    postObj.toYear = dateToSplit[0];
                    postObj.toMonth = dateToSplit[1];
                    postObj.toDay = dateToSplit[2];
                }
            }
            SocketFactory.emit('loadReport', postObj, function (r) {
                console.log(r);
                $scope.reportData = JSON.parse(r);
                // $scope.currentReport = code_name;
            })

        }

        // $scope.year = '';
        // $scope.month = '';
        // $scope.day = '';

        $scope.calendarInfo = {
            reportType: ''
        };

        $scope.closeDialog = function ($mdDialog) {
            // $scope.calendarInfo.reportType = $scope.calendarInfo.lastReportType;
            $mdDialog.cancel();
        };

        $scope.callCalendar = function (dialogCtrl) {
            console.log('call calendar');
            //console.log(event);
            $scope.calendarInfo.lastReportType = $scope.calendarInfo.reportType;
            //console.log(angular.element('md-dialog[aria-label="keypad"]').scope());
            $scope.calendarInfo.value = '';
            if (angular.element('md-dialog[aria-label="calendar"]').scope() == undefined) {
                $mdDialog.show({
                    controller: dialogCtrl,
                    templateUrl: 'tmpl/calendar.html',
                    // targetEvent: event,
                    escapeToClose: false,
                    focusOnOpen: true,
                    bindToController: true,
                    scope: $scope,
                    preserveScope: true,
                    // for generic purpose, each instance only affect local property keypad value
                    locals: {
                        calendarInfo: $scope.calendarInfo,
                    }
                })

                $rootElement.on('keyup', function(e){
                    // console.log("its's me");
                    if (e.keyCode === 27) {
                      $timeout($mdDialog.hide);
                      $scope.calendarInfo.reportType = $scope.calendarInfo.lastReportType;
                    }
                });
            } else {
                $mdDialog.hide();
            }
        };

        $scope.todayReport = function() {
            console.log('called');
            $scope.selectedDateRange.length = 0;
            $scope.dateFrom = '';
            $scope.dateTo = '';

            var day = new Date;
            //this.setReport( day.getFullYear(), day.getMonth() + 1, day.getDate());
            this.setReport();
        }

        $scope.dialogCtrl = function ($scope, $mdDialog, calendarInfo) {
            $scope.calendarInfo = calendarInfo;
        };
    });

    app.controller('searchBillController', function ($scope, MainService, uiLanguage, INFO, $http, SocketFactory) {
        $scope.printBill = function () {
            //alert(MainService.Cart.refNo);
            SocketFactory.emit('printBill', { "refNo": MainService.Cart.refNo }, function (r) {

            })
        }

        $scope.loadBill = function(args){
            SocketFactory.emit('loadBill', args, function (r) {
                r = JSON.parse(r);
                console.log(r);
                if (r.order[0].header.orderId == '') {
                    $mdDialog.show(
                     $mdDialog.alert()
                       .parent(angular.element(document.body))
                       .title(uiLanguage[INFO.lang]['alert'])
                       .content(uiLanguage[INFO.lang]['no_order'])
                       // .content('There is no order')
                       .ok(uiLanguage[INFO.lang]['ok'])
                   );
                } else {
                    var order = r.order[0];
                    MainService.modeMultiSelectItem = false;
                    //MainService.mode = MainService.schema.mode.order;
                    //MainService.modeOrder = MainService.schema.modeOrder.normal;
                    console.log(order);
                    MainService.Cart.tableNo = order.header.tableNum;
                    MainService.Cart.noOfPeople = '';
                    MainService.Cart.refNo = order.header.refNo;
                    MainService.Cart.transTime = order.header.transTime;
                    MainService.Cart.member = {};
                    //console.log(angular.element('input-keypad').scope());
                    MainService.Cart.cartList = [];
                    MainService.Cart.orderList = [];
                    MainService.appliedPaymentMethod = [];
                    MainService.appliedDiscount = [];
                    MainService.Cart.couponMainService = [];
                    MainService.Cart.noOfPeople = order.header.peopleNum;
                    if (order.header.member != undefined) {
                        MainService.Cart.member = $.extend(true, order.header.member, MainService.schema.member);
                    }
                    angular.forEach(order.item, function (v, k) {
                        console.log(v);
                        //this.push(key + ': ' + value);
                        var item = $.extend(true, {}, MainService.schema.baseItem, v);
                        if (item.type === "I") {
                            item.init();
                            if (v.voidIndex != undefined) {
                                if (v.voidIndex != -1) {
                                    //MainService.Cart.orderList[v.voidIndex].voidQty = v.qty;
                                    $.each(MainService.Cart.orderList, function (idx, vitem) {
                                        if (vitem.index == v.voidIndex) {
                                            vitem.voidQty = v.qty;
                                        }
                                    });
                                }
                                item.voidRemark = v.voidRemark;
                            }
                            MainService.Cart.orderList.push(item);
                        } else if (item.type === "S") {
                            if (MainService.Cart.orderList[MainService.Cart.orderList.length - 1].option == undefined)
                                MainService.Cart.orderList[MainService.Cart.orderList.length - 1].option = [{ items: [] }];
                            MainService.Cart.orderList[MainService.Cart.orderList.length - 1].option[0].items.push(item);
                        } else if (item.type === "M") {
                            var m = MainService.food.modifier.filter(function (matchItem) {
                                return matchItem.code == item.code;
                            });
                            item.modifier.push(m[0]);
                        }
                    });

                    for (var i = 0; i < order.discount.length; i++) {
                    
                        //coupon_alias: "off_20",
                        //amount: 0.8,
                        //type: 'percentage'
                        MainService.appliedDiscount.push({ 'coupon_alias': order.discount[i].discountCode, 'amount': order.discount[i].discountAmount, 'type': order.discount[i].discountType, 'price': order.discount[i].discountValue });
                        MainService.Cart.couponObj = [{ 'coupon_alias': order.discount[i].discountCode, 'amount': order.discount[i].discountAmount, 'type': order.discount[i].discountType }];
                    }

                    for (var i = 0; i < order.payment.length; i++) {
                        MainService.appliedPaymentMethod.push({ 'method': order.payment[i].paymentType, 'amount': order.payment[i].paymentValue })
                    }
                    MainService.calcPrice([MainService.Cart.orderList], MainService.appliedDiscount);

                    MainService.remainder();
                    MainService.checkPaymentTips();
                }
            })
        }
        $scope.loadBill(null);

        $scope.lastBill = function () {
            $scope.loadBill(null);
        }

        $scope.browseBill = function( direction ) {
            // prev
            $scope.loadBill({ "refNo": MainService.Cart.refNo , "direction": direction});
            // next
            //alert(MainService.Cart.refNo)

        }

        $scope.searchBill = function() {
            $scope.loadBill({ "searchNo": this.billNo});
        }

        $scope.cancel = function() {
            this.billNo = '';
        }

        $scope.inputFree = function ( val ) {
            console.log($scope.billNo);
            this.appendNumFree( val );
        };

        $scope.appendNumFree = function( val ) {

            // do nothing if floating point is already input
            var inputValue = String( this.billNo )
            if( inputValue === 'undefined')
                inputValue = '';

            this.billNo = inputValue.concat(val);
            // default
        }

        $scope.changeBillInfo = function() {
            // MainService.mode = MainService.schema.mode.order; 
            MainService.checkPermission('changeTender', function () {
                MainService.modeOrder = MainService.schema.modeOrder.amendBill;
            })
        }

        $scope.changeCreditCard = function() {

        }
    });

    app.controller('mainMenuController', function ($scope, $controller, uiLanguage, INFO, SocketFactory, MainService) {
        // just an alias to call, for lesser code in template purpose
        // $controller('gridMenuOptionController',{$scope: $scope}); // passing current scope to commmon controller
        

        // even object is work from template ng-click
        $scope.test = function( arg ) {
            console.log('hahahahaahah');
            console.log(arg);
        }

        $scope.varName = 1234;
    });

    //--------------------------------------------------------

    // prototype test
    app.controller('selectableController', function($scope){
        console.log('selectable controller');
        $scope.setActive = function( $event, callback ) {
            console.log($event);
            var element = $event.currentTarget || $event.srcElement;
            // console.log('setactive????');
            // console.log( element );

            // can pass any params to callback if needed using arguments[2], [3]....
            if(angular.element( element ).hasClass("active")) {
                angular.element( element ).removeClass("active");
                (callback || angular.noop)()
                // $scope.removeFromBasket( callback || angular.noop );
            } else {
                angular.element( element ).addClass("active");
                (callback || angular.noop)()
                // $scope.addToBasket( callback || angular.noop );
            }
        };

        // $scope.addToBasket = function( callback ){
        //     callback();
        // };

        // $scope.removeFromBasket = function( callback ) {
        //     callback();
        // };
    });

    // controllers created by Daniel for merge and test here
    app.controller('headerCtrl', function ($scope, $log, $mdDialog, $rootScope, $element, MainService, $mdToast, $controller, $mdSidenav, uiLanguage, SocketFactory) {
        $controller('tableController',{$scope: $scope}); // passing current scope to commmon controller
        // $controller('reportController',{$scope: $scope, $element: $element}); // passing current scope to commmon controller
        // console.log($scope);

        // $scope.headerRemark = reportService.remark;

        $scope.user = '';
        $scope.MainService = MainService;

        // just an alias to call, for lesser code in template purpose
        $scope.switchMode = function( modeType, modeName, requiredLogin ) {
            MainService.switchMode( modeType, modeName, requiredLogin );
        }

        // legacy method, change mode name and call svg methods, combine later with latest functions
        $scope.changeMode = function( mode, modeNo ) {
            // MainService.switchModeFloorPlan( modeNo );
            MainService.RMSTable.changeMode( mode );
            this.cancelTransfer();
        }


        $scope.writeFile = function () {
            var confirm = $mdDialog.confirm()
               .title(uiLanguage[INFO.lang]['alert'])
               .content(uiLanguage[INFO.lang]['dialog_write_content'])
               .ok(uiLanguage[INFO.lang]['yes'])
               .cancel(uiLanguage[INFO.lang]['cancel']);

            $mdDialog.show(confirm).then(function () {
                SocketFactory.emit('GenerateMenu', function (result) {
                    if (result === 'OK') {
                        $mdDialog.show(
                            $mdDialog.alert()
                              .parent(angular.element(document.body))
                              .title(uiLanguage[INFO.lang]['alert'])
                               .content(uiLanguage[INFO.lang]['dialog_write_success'])
                              .ok(uiLanguage[INFO.lang]['yes'])
                          );
                    }
                });
            }, function () {
            });

        }

        // console.log($scope.MainService);
        SocketFactory.on('test', function (data) {
            console.log('socket connected 1' + data);
        });

        $scope.nextZone = function(){
            // console.log("please chagne to next zone");
            $scope.$emit('nextZone', '');
        }

        $scope.prevZone = function(){
            // console.log("please change tp prev zone");
            $scope.$emit('prevZone', '');
        }

        $scope.login = function (ev) {
            if (angular.element('md-dialog[aria-label="Login"]').scope() == undefined) {
                $mdDialog.show({
                    templateUrl: 'tmpl/login.html',
                    targetEvent: ev,
                })
            } else {
                $mdDialog.hide();
            }
        }
        $scope.next = function () { console.log('headerctrl') }

        $scope.submitOrder = function (ev) {
            console.log('emit submit order');
            $scope.$emit('submitOrder', {});
        }

        $scope.modeFoodControl = function (ev) {
            //angular.element('order-ctrl').scope().mode = angular.element('order-ctrl').scope().mode === 'detail' ? '' : 'detail';
            //angular.element('order-ctrl').scope().$element.removeClass('ng-hide');
            angular.element('order-scene').scope().toggleFoodControl();

        }

        $scope.modeToggleOrder = function (ev) {
            console.log('toggle order?');
            angular.element('order-ctrl').scope().mode = angular.element('order-ctrl').scope().mode === angular.element('order-ctrl').scope().modeOrderDetail ? angular.element('order-ctrl').scope().modeOrder : angular.element('order-ctrl').scope().modeOrderDetail;
        }

        $scope.panelMember = function (ev) {
            if (angular.element('md-dialog[aria-label="Member"]').scope() == undefined) {
                $mdDialog.show({
                    controller: DialogControllerMember,
                    templateUrl: 'tmpl/member.html',
                    targetEvent: ev,
                })
            } else {
                $mdDialog.hide();
            }
        }

        $scope.toggleRight = buildToggler('left');

        function buildToggler(navID) {
          return function() {
            // default action for toggler
            // angular.element('.side-menu > li:eq(3) .md-button-toggle').click()
            $mdSidenav(navID)
              .toggle()
              // .then(function () {
              //   $log.debug("toggle " + navID + " is done");
              // });
          }
        }

        // event
        // put it on respective controller to call MainService without doing it over and over again in rms-templates
        $element.find('.btn:not(".no-active")').on('mousedown keyup touch', function(e){
            //console.log(this);
            //console.log(e);
            //MainService.setActive( e );
        });


        //$rootScope.$on('modeTableOrder', function (o) {
        //    $element.find('.orderPanelCtrl').removeClass('ng-hide');
        //});
    });


    app.controller('DialogControllerManagerLogin', function ($scope, $mdDialog, SocketFactory, MainService, uiLanguage, INFO) {
        // console.log("called once");
        $scope.mode = 'username';
        $scope.modeName = 'Login';
        $scope.username = '';
        $scope.password = '';
        $scope.tableNum = '';
        $scope.keypad = keypad;
        $scope.ui = uiLanguage[INFO.lang];

        $scope.input = function (val) {
            angular.element(".login").addClass("md-input-focused");
            $scope.loginValue += '' + val;
        }

        $scope.reset = function (val) {
            angular.element(".login").addClass("md-input-focused");
            if ($scope.loginValue == '' && $scope.mode == 'username') {
                $mdDialog.cancel();
                // angular.element(".login").addClass("md-input-focused");
            } else if ($scope.loginValue == '' && $scope.mode == 'password') {
                $scope.modeName = 'Login';
                $scope.mode = 'username';
                $scope.loginValue = '';
            } else {
                $scope.loginValue = '';
            }
        }

        $scope.backspace = function () {
            // console.log('here!');
            // $scope.loginValue = "12347";
            $scope.loginValue = $scope.loginValue.substring(0, $scope.loginValue.length - 1);
        };

        $scope.next = function (val) {
            //console.log('********');
            //console.log($scope.tableNum);
            //console.log($scope.mode);
            //console.log($scope.loginValue);
            switch ($scope.mode) {
                case 'username':
                    $scope.username = $scope.loginValue;
                    MainService.UserManager.login($scope.username, '').then(function (r) {
                        //console.log(r);
                        //console.log(r.success);
                        if (r.success) {
                            $scope.loginError = false;
                            $mdDialog.cancel();
                            MainService.UserManager.loginCallback();
                        } else {
                            $scope.mode = 'password';
                            $scope.modeName = 'Password';
                            $scope.resetLoginInputValue();
                        }
                    }, function (reason) {
                        console.log('Failed: ' + reason);
                    }, function (update) {
                        console.log('Got notification: ' + update);
                    });
                    break;
                case 'password':
                    $scope.password = $scope.loginValue;
                    MainService.UserManager.login($scope.username, $scope.password).then(function (r) {
                        console.log(r);
                        if (r.success) {
                            $scope.loginError = false;
                            $mdDialog.cancel();
                            MainService.UserManager.loginCallback();
                        } else {
                            $scope.mode = 'password';
                            $scope.resetLoginInputValue();
                            $scope.loginError = true;
                        }
                    }, function (reason) {
                        console.log('Failed: ' + reason);
                    }, function (update) {
                        console.log('Got notification: ' + update);
                    });
                    break;
            }
        }
    });

    // controllers for headCtrl->login
    function DialogControllerMember($scope, $mdDialog, SocketFactory, MainService, uiLanguage, INFO, keyMap) {
        // $scope.calcNumberGroup = [[1, 2, 3], [4, 5, 6], [7, 8, 9]];
        $scope.MainService = MainService;
        $scope.keypad = keypad;
        $scope.MemberNotFound = false;
        $scope.input = function (val) {
            $scope.loginValue += '' + val;
        }
        $scope.reset = function (val) {
            if ($scope.loginValue == '') {
                $mdDialog.cancel();
            } else {
                $scope.loginValue = '';
            }
        }
        $scope.cancel = function () {
            $mdDialog.cancel();
        }

        $scope.next = function (val) {
            console.log($scope.loginValue);
            console.log("next");
            //console.log(angular.element('order-ctrl').scope().cart.tableNo);
            //SocketFactory.emit('linkMemberToTable', { membercode: $scope.loginValue, tableno: MainService.Cart.tableNo }, function (r) {
            SocketFactory.emit('searchMember', { membercode: $scope.loginValue }, function (r) {
                console.log(r);
                if (r) {
                    $scope.Member = $.extend(true, r, MainService.schema.member);
                    $scope.MemberNotFound = false;
                } else {
                    $scope.MemberNotFound = true;
                }
                //if (r.success) {

                //}
            });
        }

        $scope.backspace = function () {
            // console.log('here!');
            // $scope.loginValue = "12347";
            $scope.loginValue = $scope.loginValue.substring(0, $scope.loginValue.length-1);
        };

        $scope.ui = uiLanguage[INFO.lang];
    };

    app.controller('userCtrl', function ($scope, $timeout, uiLanguage, INFO, $http, SocketFactory) {
        $scope.ui = uiLanguage[INFO.lang];
        $scope.lang = INFO.lang;
        $scope.currentMode = 'list';
        $scope.editUser = function (username) {
            $scope.currentMode = 'user';
            var foundStaff = $scope.Staffs.filter(function (s) {
                return s.username == username;
            })
            if (foundStaff.length == 0) {
                $scope.currentUser = {'oldusername':'new'};
            } else {
                $scope.currentUser = foundStaff[0];
                $scope.currentUser.oldusername = $scope.currentUser.username;
            }
        }

        $scope.submitUser = function () {
            console.log($scope.currentUser)
            SocketFactory.emit('editStaff', $scope.currentUser, function (r) {
                r = JSON.parse(r);
                if (r.result == "OK") {
                    SocketFactory.emit('loadStaff', null, function (r) {
                        $scope.Staffs = JSON.parse(r);
                        $scope.currentMode = 'list';
                    })
                } else {

                }
            })
        }

        SocketFactory.emit('loadStaff',null, function (r) {
            //alert(r);
            $scope.Staffs = JSON.parse(r);
        })
    });

    app.controller('itemController', function ($scope, $timeout, uiLanguage, INFO, $mdDialog, $http, SocketFactory, $compile) {
        // console.log('run me');
        // load data -> after loaded -> render table
        $scope.mode = 'basic';
        $scope.categories = [];

        $scope.initFoodItem = function () {

            $scope.FoodItem = {};
            $scope.FoodItem.serviceCharge = "0";
            $scope.FoodItem.discount = "0";
            $scope.FoodItem.screenShow = "0";
            $scope.FoodItem.ItemType = "null";
        }


        $scope.colors = [
            { val: "rms-palette-orange", name: "Orange" },
            { val: "rms-palette-gold", name: "Gold" },
            { val: "rms-palette-grass", name: "Grass" },
            { val: "rms-palette-sky", name: "Sky" },
            { val: "rms-palette-lemon", name: "Lemon" },
            { val: "rms-palette-jade", name: "Jade" },
            { val: "rms-palette-navy", name: "Navy" }
        ]
        $scope.ItemType = [{ val: "null", name: "沒有" }, { val: "T", name: 'T' }, { val: "o", name: 'o' }]

        $scope.load = function () {
            var currentScope = this;
            var rootScope = $scope;
            SocketFactory.emit('GetCategory', null, function (result) {
                result = JSON.parse(result);
                $scope.categories = result;
            });


            $http({
                method: 'GET',
                url: './service.ashx?action=GetAllItem'
            }).then(function successCallback(response) {
                $scope.itemList = [];
                $scope.itemList = response.data;
            }, function errorCallback(response) {
            });
        }

        $scope.EditFood = function (e) {
            var elem = e.currentTarget;
            $scope.initFoodItem();
            $scope.FoodItem.rowCount = $(elem).closest('tr').attr('rowcount');
            $scope.FoodItem.code = $(elem).closest('tr').children('td.code').html();
            $scope.FoodItem.ocode = $scope.FoodItem.code;
            $scope.FoodItem.plu = $(elem).closest('tr').children('td.plu').html();
            $scope.FoodItem.oplu = $scope.FoodItem.plu;
            $scope.FoodItem.cname = $(elem).closest('tr').children('td.cname').html();
            $scope.FoodItem.cname2 = $(elem).closest('tr').children('td.cname2').html();
            $scope.FoodItem.ename = $(elem).closest('tr').children('td.ename').html();
            $scope.FoodItem.kname = $(elem).closest('tr').children('td.kname').html();
            $scope.FoodItem.price = parseFloat($(elem).closest('tr').children('td.price').attr('price'));
            $scope.FoodItem.printer = $(elem).closest('tr').children('td.printer').attr('printer');
            $scope.FoodItem.printerGroup = $(elem).closest('tr').children('td.printerGroup').attr('printerGroup');
            $scope.FoodItem.kds = $(elem).closest('tr').children('td.kds').attr('kds');
            $scope.FoodItem.serviceCharge = $(elem).closest('tr').children('td.serviceCharge').attr('serviceCharge') === "true" ? "1" : "0";
            $scope.FoodItem.discount = $(elem).closest('tr').children('td.discount').attr('discount');
            $scope.FoodItem.screenShow = $(elem).closest('tr').children('td.screenShow').attr('screenShow') === "true" ? "1" : "0";;
            $scope.FoodItem.category = $(elem).closest('tr').children('td.category').attr('category');
            $scope.FoodItem.color = $(elem).closest('tr').children('td.color').attr('color');
            $scope.FoodItem.ItemType = $(elem).closest('tr').children('td.type').attr('type') == "" ? "null" : $(elem).closest('tr').children('td.type').attr('type');
            console.log($scope.FoodItem)
            Materialize.updateTextFields();
            angular.element('#item-details').css('display', 'block')
            angular.element('#FoodItem-list').css('display', 'none');
            $scope.mode = 'change'

        }

        $scope.removeErrorStyle = function () {
            angular.element('.missing-data').removeClass('missing-data');
        }

        $scope.submitAdd = function (e) {
            e.preventDefault();
            $scope.removeErrorStyle();
            var checking = true;
            if ($scope.FoodItem.category == undefined) {
                angular.element('#select_value_label_0').addClass('missing-data')
                checking = false;
            }

            if ($scope.FoodItem.color == undefined) {
                angular.element('#select_value_label_1').addClass('missing-data')
                checking = false;

            }

            if ($scope.FoodItem.code == undefined || $scope.FoodItem.code == "") {
                angular.element('#code').addClass('missing-data')
                checking = false;
            }

            if ($scope.FoodItem.plu == undefined || $scope.FoodItem.plu == "") {
                angular.element('#plu').addClass('missing-data')
                checking = false;
            }

            if ($scope.FoodItem.printer == undefined) {
                $scope.FoodItem.printer = null;
            }

            if ($scope.FoodItem.printerGroup == undefined) {
                $scope.FoodItem.printerGroup = null;
            }

            if ($scope.FoodItem.kds == undefined) {
                $scope.FoodItem.kds = null;
            }

            if ($scope.FoodItem.ename == undefined || $scope.FoodItem.ename == "") {
                angular.element('#ename').addClass('missing-data')
                checking = false;
            }

            if ($scope.FoodItem.cname == undefined || $scope.FoodItem.cname == "") {
                angular.element('#cname').addClass('missing-data')
                checking = false;
            }

            if ($scope.FoodItem.cname2 == undefined || $scope.FoodItem.cname2 == "") {
                angular.element('#cname2').addClass('missing-data')
                checking = false;
            }

            if ($scope.FoodItem.kname == undefined || $scope.FoodItem.kname == "") {
                angular.element('#kname').addClass('missing-data')
                checking = false;
            }

            if ($scope.FoodItem.price == undefined || $scope.FoodItem.price == "") {
                angular.element('#price').addClass('missing-data')
                checking = false;
            }
            if (checking) {
                console.log($scope.FoodItem);
                $scope.ItemFunc();
            }

        }

        $scope.ItemFunc = function () {
            if ($scope.mode === 'add') {
                SocketFactory.emit('SearchItemIsExist', $scope.FoodItem.code, function (result) {
                    result = parseInt(result)
                    if (result != 0) {
                        $scope.FoodItemIsExist();
                    }
                    else {
                        //console.log($scope.FoodItem);
                        $scope.SubmitScreen();
                    }
                })
            }
            else if ($scope.mode === 'change') {
                if ($scope.FoodItem.code === $scope.FoodItem.ocode)
                    $scope.SubmitScreen();
                else {

                    SocketFactory.emit('SearchItemIsExist', $scope.FoodItem.code, function (result) {
                        result = parseInt(result)
                        if (result != 0) {
                            $scope.FoodItemIsExist();
                        }
                        else {
                            //console.log($scope.FoodItem);
                            $scope.SubmitScreen();
                        }
                    })


                }
            }
        }


        $scope.BackItemListPage = function (e) {
            if (e != undefined) e.preventDefault();
            $scope.mode = 'basic';
            $scope.removeErrorStyle();
            angular.element('#item-details').css('display', 'none');
            angular.element('#FoodItem-list').css('display', 'block');
        }

        $scope.AddItemPage = function (e) {
            e.preventDefault();
            $scope.mode = 'add';
            $scope.initFoodItem();
            Materialize.updateTextFields();
            angular.element('#item-details').css('display', 'block');
            angular.element('#FoodItem-list').css('display', 'none');
        }

        $scope.ItemRowClick = function (e) {
            angular.element('#FoodItem-list tbody tr').css('background-color', 'white');
            angular.element(e.currentTarget).css('background-color', 'yellow');
        }

        $scope.AfterFuncToShow = function (ItemCode, itemIndex) {

            $.ajax({
                url: "./service.ashx?action=GetAllItem&itemId=" + ItemCode,
                dataType: "JSON",
                success: function (data) {

                    angular.element('#FoodItem-list tbody tr').css('background-color', 'white');

                    if (itemIndex === 0) {
                        $scope.itemList.splice(0, 0, data[0]);
                        $scope.itemList[0].rowStyle = "{'background-color':'yellow'}";
                    }
                    else {
                        itemIndex = parseInt(itemIndex);
                        console.log($scope.itemList[itemIndex]);
                        console.log(data[0]);
                        $scope.itemList[itemIndex] = data[0];
                        $scope.itemList[itemIndex].rowStyle = "{'background-color':'yellow'}";

                    }
                },
                complete: function () {
                    $scope.BackItemListPage();
                }, async: false
            });

        }

        $scope.SubmitScreen = function () {
            var confirm = $mdDialog.confirm()
               .title(uiLanguage[INFO.lang]['alert'])
               .content('確定要輸入?')
               .ok(uiLanguage[INFO.lang]['yes'])
               .cancel(uiLanguage[INFO.lang]['cancel']);

            $mdDialog.show(confirm).then(function () {

                if ($scope.mode === 'add') {
                    SocketFactory.emit('InsertItem', JSON.stringify($scope.FoodItem), function (result) {
                        if (result === "OK") {
                            //$scope.refreshPage();
                            $scope.AfterFuncToShow($scope.FoodItem.code, 0);
                        }
                    });
                }
                else if ($scope.mode === 'change') {
                    SocketFactory.emit('UpdateItem', JSON.stringify($scope.FoodItem), function (result) {
                        if (result === "OK") {
                            $scope.AfterFuncToShow($scope.FoodItem.code, $scope.FoodItem.rowCount)
                        }
                    });
                }






            }, function () {
            });
        }

        $scope.FoodItemIsExist = function () {
            $mdDialog.show(
                $mdDialog.alert()
                  .parent(angular.element(document.body))
                  .title(uiLanguage[INFO.lang]['alert'])
                   .content("食品編號已存在!")
                  .ok(uiLanguage[INFO.lang]['yes'])
              );
        }

        $scope.renderTable = function (data) {
            // table food list

            HandlebarsIntl.registerWith(Handlebars);
            var intlData = {
                "locales": "en-US"
            };
            var contextItemList = {};
            contextItemList.tableHead = [
              { "name": "食品編號", "fieldType": "textfield", "widthClass": "item-code" },
              { "name": "中文名稱", "fieldType": "textfield", "widthClass": "name1" },
              { "name": "英文名稱", "fieldType": "textfield", "widthClass": "name2" },
              { "name": "螢幕", "fieldType": "dropdown", "widthClass": "display" },
              { "name": "排列#", "fieldType": "textfield", "widthClass": "sort-order" },
              { "name": "顏色", "fieldType": "dropdown", "widthClass": "color" },
              { "name": "PLU", "fieldType": "textfield", "widthClass": "plu" },
              { "name": "價錢#0", "fieldType": "textfield", "widthClass": "price" },
              { "name": "積分", "fieldType": "textfield", "widthClass": "point" },
              { "name": "列印到", "fieldType": "textfield", "widthClass": "print-to" },
              { "name": "獨立列印", "fieldType": "checkbox", "widthClass": "separate-print" },
              { "name": "廚房名稱", "fieldType": "textfield", "widthClass": "kitchen-name" },
              { "name": "計服務費", "fieldType": "checkbox", "widthClass": "service-charge" },
              { "name": "套餐", "fieldType": "checkbox", "widthClass": "combo" },
              { "name": "細項", "fieldType": "textfield", "widthClass": "detail-item" },
              { "name": "禮券樣式", "fieldType": "textfield", "widthClass": "discount-template" },
              { "name": "備註", "fieldType": "textfield", "widthClass": "remark" }];

            contextItemList.tableContent = data.category;

            content = Handlebars.compile($("#tpl-table-itemlist").html())(contextItemList);
            // console.log(content);
            $('#item-list-container').append(content);

            var contextSubitemList = {};
            contextSubitemList.tableHead = [
               { "name": "排列", "fieldType": "textfield", "widthClass": "sort-order" },
               { "name": "子項目類別", "fieldType": "textfield", "widthClass": "sub-item-type" },
               { "name": "套餐食品", "fieldType": "textfield", "widthClass": "combo" },
               { "name": "數量", "fieldType": "textfield", "widthClass": "qty" },
               { "name": "最少數量", "fieldType": "textfield", "widthClass": "minoq" },
               { "name": "最多數量", "fieldType": "textfield", "widthClass": "maxoq" },
               { "name": "自由更改控制金額", "fieldType": "textfield", "widthClass": "free-price" },
               { "name": "最大項目金額", "fieldType": "textfield", "widthClass": "max-price" },
               { "name": "我的細項", "fieldType": "textfield", "widthClass": "detail-item" },
               { "name": "金額到主項目", "fieldType": "checkbox", "widthClass": "amount-to-main-item" },
               { "name": "自訂名稱1", "fieldType": "textfield", "widthClass": "name1" },
               { "name": "自訂名稱2", "fieldType": "textfield", "widthClass": "name2" },
               { "name": "金額", "fieldType": "textfield", "widthClass": "amount" },
               { "name": "列印到", "fieldType": "textfield", "widthClass": "print-to" },
               { "name": "自訂廚房名稱", "fieldType": "textfield", "widthClass": "custom-kitchen-name" },
               { "name": "獨立列印", "fieldType": "checkbox", "widthClass": "separate-print" },
               { "name": "可更改為", "fieldType": "textfield", "widthClass": "changeable" },
               { "name": "顯示螢幕組別", "fieldType": "textfield", "widthClass": "category-display" }];
            content = Handlebars.compile($("#tpl-table-subitem").html())(contextSubitemList);
            // console.log(content);
            $('#subitem-list').append(content);

            // content = Handlebars.compile($("#tpl-table-subitem-content").html())(subitemArr);
            // console.log(content);
            // $('#subitem-list tbody').html(content);
        }


        //$scope.renderTable();
    });
    app.controller('categoryController', function ($scope, $timeout, uiLanguage, INFO, $http, SocketFactory, $mdDialog, $anchorScroll) {
        $scope.mode = 'basic';
 
        $scope.Category = {};
        $scope.colors = [
            { val: "rms-palette-orange", name: "Orange" },
            { val: "rms-palette-gold", name: "Gold" },
            { val: "rms-palette-grass", name: "Grass" },
            { val: "rms-palette-sky", name: "Sky" },
            { val: "rms-palette-lemon", name: "Lemon" },
            { val: "rms-palette-jade", name: "Jade" },
            { val: "rms-palette-navy", name: "Navy" }
        ]

        $scope.icons = [
            { val: "null", name: "沒有" },
            { val: 2, name: "2" },
            { val: 4, name: "4" },
            { val: 6, name: "6" },
            { val: 8, name: 8 }
        ]

        $scope.ShowBasic = function () {
            return $scope.mode === 'basic';
        }

        $scope.ShowEditPage = function () {
            return $scope.mode === 'add' || $scope.mode === 'change';
        }


        $scope.load = function () {

            SocketFactory.emit('GetCategory', null, function (result) {
                result = JSON.parse(result);
                $scope.categories = result;
            });
        }

        $scope.AddItemPage = function () {
            $scope.mode = 'add';
            Materialize.updateTextFields();
    

        }


        $scope.BackItemListPage = function (e) {
            if (e !== undefined) e.preventDefault();
      
            $scope.Category = {};
            $scope.mode = 'basic';
        }

        $scope.ItemRowClick = function (e) {
            angular.element('#CategoryItem-list tbody tr').css('background-color', 'white');
            angular.element(e.currentTarget).css('background-color', 'yellow');
        }

        $scope.removeErrorStyle = function () {
            angular.element('.missing-data').removeClass('missing-data');
        }

        $scope.submitAdd = function (e) {
            e.preventDefault();
            $scope.removeErrorStyle();
            var checking = true;
            if ($scope.Category.color == undefined) {
                angular.element('#select_value_label_0').addClass('missing-data')
                checking = false;
            }

            if ($scope.Category.icon == undefined) {
                angular.element('#select_value_label_1').addClass('missing-data')
                checking = false;

            }

            if ($scope.Category.ename == undefined || $scope.Category.ename == "") {
                angular.element('#ename').addClass('missing-data')
                checking = false;
            }

            if ($scope.Category.cname == undefined || $scope.Category.cname == "") {
                angular.element('#cname').addClass('missing-data')
                checking = false;
            }

            if ($scope.Category.cname2 == undefined || $scope.Category.cname2 == "") {
                angular.element('#cname2').addClass('missing-data')
            }

            if ($scope.Category.seq == undefined || $scope.Category.seq == "") {
                angular.element('#seq').addClass('missing-data')
            }


            if (checking) {
                $scope.CategoryFunc();
            }

        }


        $scope.CategoryFunc = function () {
            $scope.SubmitScreen();
        }


        $scope.SubmitScreen = function () {
            var confirm = $mdDialog.confirm()
               .title(uiLanguage[INFO.lang]['alert'])
               .content('確定要輸入?')
               .ok(uiLanguage[INFO.lang]['yes'])
               .cancel(uiLanguage[INFO.lang]['cancel']);

            $mdDialog.show(confirm).then(function () {

                if ($scope.mode === 'add') {
                    SocketFactory.emit('InsertCategory', JSON.stringify($scope.Category), function (result) {
                        if (result !== undefined || result !== null) {

                            SocketFactory.emit('GetCategory', result, function (newCategory) {
                                newCategory = JSON.parse(newCategory);
                                newCategory[0].rowStyle = "{'background-color':'yellow'}";
                                $scope.categories.splice(0, 0, newCategory[0])
                                $scope.BackItemListPage();

                            });


                            //$scope.refreshPage();
                            //$scope.AfterFuncToShow($scope.FoodItem.code, 0);
                        }
                    });
                }
                else if ($scope.mode === 'change') {
                    SocketFactory.emit('UpdateCategory', JSON.stringify($scope.Category), function (result) {
                        if (result !== null && result !== undefined) {
                            result = JSON.parse(result);
                            console.log(result[0]);
                            result[0].rowStyle = "{'background-color':'yellow'}";
                            $scope.categories[$scope.Category.rowCount] = result[0];
                            $scope.BackItemListPage();
                        }
                    });
                }
            }, function () {
            });
        }

        $scope.EditCategory = function (e, cateItem) {
            console.log(cateItem);
            Materialize.updateTextFields();
            $scope.Category.categoryId = cateItem.categoryId;
            $scope.Category.seq = cateItem.seq;
            $scope.Category.cname = cateItem.name1;
            $scope.Category.ename = cateItem.name2;
            $scope.Category.cname2 = cateItem.name3;
            $scope.Category.color = cateItem.color;
            $scope.Category.icon = cateItem.icon == null ? "null" : cateItem.icon;
            $scope.Category.rowCount = $(e.currentTarget).closest('tr').attr('rowcount');
            $scope.mode = 'change';
        }



        $scope.renderTable = function () {
            // table food list
            HandlebarsIntl.registerWith(Handlebars);
            var intlData = {
                "locales": "en-US"
            };
            var contextCategoryList = {};
            contextCategoryList.tableHead = [
              { "name": "螢幕", "fieldType": "textfield", "widthClass": "display" },
              { "name": "排列", "fieldType": "textfield", "widthClass": "sort-order" },
              { "name": "中文名稱", "fieldType": "textfield", "widthClass": "name1" },
              { "name": "英文名稱", "fieldType": "textfield", "widthClass": "name2" },
              { "name": "可使用", "fieldType": "checkbox", "widthClass": "usable" },
              { "name": "顏色", "fieldType": "dropdown", "widthClass": "color" }];
            // contextCategoryList.tableContent = data.category;
            content = Handlebars.compile($("#tpl-table-category").html())(contextCategoryList);
            $('#category-list-container').append(content);
        }
        // $scope.renderTable();
    });

    app.controller("keypadControllerOpenItem", function ( $scope, $mdDialog, $element, $attrs, $timeout, MainService ) {
        $scope.keypad = keypad;
        $scope.keypadInfo = {
            value: '0.0',
            name: ''
        };
        // $scope.ui = uiLanguage[INFO.lang];
        $scope.title = $attrs.title ? $attrs.title : 'Keypad';
        $scope.input = function (val) {
            var maxlength = $attrs.maxlength ? $attrs.maxlength : 3;
            if ($scope.keypadInfo.value.length < maxlength)
                $scope.keypadInfo.value += '' + val;
        };

        $scope.currentFieldName = "name";
        $scope.focusField = function (fieldName) {
            this.currentFieldName = fieldName;
            return true;
        }

        // for non-numerical character
        $scope.inputFree = function (val) {
            console.log($scope.keypadInfo);
            // simpler:
            // number type field
            if (this.currentFieldName === 'value') {
                switch (val) {
                    case '.':
                        console.log('. ar');
                        this.appendNumFree(val);
                        break;
                    case 'tab':
                    case 'space':
                        break;
                    default:
                        /*$scope.keypadInfo[this.currentFieldName] = parseFloat( String( $scope.keypadInfo[this.currentFieldName] ) + val );
                        break;*/
                };
            } else {
                // text field (exclude date, time and so on... bare text only)
                switch (val) {
                    case 'space':
                        val = " ";
                        break;
                    case 'tab':
                        val = '';
                        for (var i = 0; i < 3; i++) {
                            val += " ";
                        }
                        break;
                    default:
                        break;
                };
                $scope.keypadInfo[this.currentFieldName] += '' + val;
            }

            // switch( this.currentFieldName ) {
            //     case "value":
            //         $scope.keypadInfo.value += '' + val;
            //         break;
            //     default:
            //     case "name":
            //         $scope.keypadInfo.name += '' + val;
            //         break;
            // }
        };

        // for numerical character, support append, decimal points
        $scope.appendNumFree = function (amount) {

            console.log(amount);
            // do nothing if floating point is already input
            var inputValue = $scope.keypadInfo[this.currentFieldName];
            if (amount === '.' && $scope.keypadInfo[this.currentFieldName][inputValue.length - 1] === '.') {
                console.log('0');
                return;
            }

            // if floating point is activated, not allow to add one character
            // this.inputPrice += '';
            $scope.keypadInfo[this.currentFieldName] = String(inputValue); // type aware method and meaning is obvious without comment
            if (inputValue.indexOf('.') === -1 && amount === '.') {
                console.log('1');
                this.floatingInput = true;
                // not necessary because " ", it will make it .# instead of 0.# when it is 0
                // if (this.inputPrice.trim() == "0") { // input 7, result: .7 instead of 0.7
                //     console.log('1-1');
                //     console.log(this.inputPrice.trim());
                //     this.inputPrice = amount;
                // }else{
                //     console.log('1-2');
                //     this.inputPrice = this.inputPrice.concat(amount);
                // }
                $scope.keypadInfo[this.currentFieldName] = inputValue.trim().concat(amount);
                return;
            }

            if (inputValue.indexOf('.') != -1 && amount === '.') {
                console.log('2');
                return
            }

            // console.log(this.inputPrice.indexOf('.'));
            // console.log( !isNaN(parseInt(this.inputPrice[this.inputPrice.length - 1])));
            if (inputValue.indexOf('.') != -1 && !isNaN(parseInt($scope.keypadInfo[this.currentFieldName][inputValue.length - 1]))) {
                console.log("3");
                return;
            }
            // console.log(typeof this.inputPrice);
            // console.log(typeof this.inputPrice.trim());
            // console.log('when is this.inputPrice.trim() == 0 ' + this.inputPrice.trim());

            if (inputValue.trim() == "0") { // 排除預設等值有space的障礙
                console.log("4");
                $scope.keypadInfo[this.currentFieldName] = amount;
            } else {
                console.log("5");
                // append 0 if prev is "."
                if (inputValue[0] === '.') {
                    console.log("0" + inputValue);
                    $scope.keypadInfo[this.currentFieldName] = "0" + inputValue.concat(amount);
                } else {
                    $scope.keypadInfo[this.currentFieldName] = inputValue.concat(amount);
                }

            }
            // default
        }

        $scope.backspaceFree = function () {
            console.log($scope.keypadInfo);
            // convert it to string or it will get error when it is 0 and use backspace

            if (this.currentFieldName === 'value') {
                var inputValue = String($scope.keypadInfo[this.currentFieldName]);
                /*var tmpStr = String( $scope.keypadInfo[this.currentFieldName] );
                if( tmpStr.length > 1) {
                    $scope.keypadInfo[this.currentFieldName] = parseFloat( tmpStr.substring(0, tmpStr.length - 1) );
                }

                if( tmpStr.length == 1) {
                    $scope.keypadInfo[this.currentFieldName] = 0;
                }*/

                if (inputValue.length === 0)
                    return;

                console.log(inputValue);
                if (inputValue[inputValue.length - 1] === '.') {
                    this.floatingInput = false;
                }

                $scope.keypadInfo[this.currentFieldName] = inputValue.substring(0, inputValue.length - 1);

                if (isNaN(inputValue) || inputValue === '') {
                    console.log('nan');
                    $scope.keypadInfo[this.currentFieldName] = '';
                }
            } else {
                $scope.keypadInfo[this.currentFieldName] = $scope.keypadInfo[this.currentFieldName].substring(0, $scope.keypadInfo[this.currentFieldName].length - 1);
            }
        }

        $scope.backspace = function () {
            $scope.keypadInfo.value = $scope.keypadInfo.value.substring(0, $scope.keypadInfo.value.length - 1);
        };

        $scope.reset = function (val) {
            console.log($scope.keypadInfo.lastValue);
            console.log($scope.keypadInfo.lastName);
            if ($scope.keypadInfo.name == '') {
                $scope.keypadInfo.name = $scope.keypadInfo.lastName;
                $mdDialog.cancel();
            } else {
                $scope.keypadInfo.name = '';
            }

            if ($scope.keypadInfo.value == '') {
                $scope.keypadInfo.value = $scope.keypadInfo.lastValue;
                $mdDialog.cancel();
            } else {
                $scope.keypadInfo.value = '';
            }
        };

        $scope.cancel = function () {
            // var focusedElement = document.activeElement;
            // console.log(focusedElement);
            // console.log($scope.lastActiveField);
            console.log('cancel');
            console.log($scope.lastActiveField == "item-name" && $scope.keypadInfo.name != "");
            console.log($scope.lastActiveField == "item-price" && $scope.keypadInfo.value != "");
            console.log($scope.lastActiveField);
            console.log($scope.keypadInfo.value);
            if ($scope.lastActiveField == "item-name" && $scope.keypadInfo.name != "") {
                $scope.keypadInfo.name = "";
                return;
            }

            if ($scope.lastActiveField == "item-price" && $scope.keypadInfo.value != "") {
                $scope.keypadInfo.value = "";
                return;
            }

            $mdDialog.cancel();
        }

        $scope.keypressed = function (event) {
            console.log(event);
        };

        $scope.next = function (event) {
            $mdDialog.hide();
            //MainService.Cart.noOfPeople = $scope.keypadInfo.value;
            //alert($scope.keypadInfo.value);
            if( MainService.modeCheckOrder('foodControl') ) {
                // food control operation
                MainService.stock.updateStockItem($scope.keypadInfo.value, $scope.currentItem);
            } else {
                angular.element($element).blur();
                // alert($scope.keypadInfo.value);
            }
        };

        $scope.checkDecimal = function (amount) {
            if (amount[amount.length - 1] === '.' || amount[0] === '.')
                return false; // input is incorrect
            else
                return true;
        }

        $scope.nextFree = function (event) {
            if (!this.checkDecimal($scope.keypadInfo.value) || $scope.keypadInfo.value === '') {
                return; // input is not completed
            }

            $mdDialog.hide();
            //MainService.Cart.noOfPeople = $scope.keypadInfo.value;
            //MainService.stock.updateStock($scope.keypadInfo.value);
            angular.element($element).blur();
            //alert($scope.keypadInfo.value);
            //alert($scope.keypadInfo.name);
            console.log($scope.currentItem);
            //$scope.currentItem.name = $scope.keypadInfo.name;

            var customItem = {};
            $.extend(true, customItem, $scope.currentItem);
            customItem.desc1 = $scope.keypadInfo.name;
            customItem.name['001'] = $scope.keypadInfo.name;
            customItem.customName = $scope.keypadInfo.name;
            customItem.unitprice = parseFloat($scope.keypadInfo.value) * 100;
            console.log(customItem)
            MainService.Cart.addItem(customItem);
        };

        $scope.closeDialog = function ($mdDialog) {
            $scope.keypadInfo.value = $scope.keypadInfo.lastValue;
            $scope.keypadInfo.name = $scope.keypadInfo.lastName;
            $mdDialog.cancel();
        };

        $scope.callKeypad = function (event, dialogCtrl, item) {
            console.log('call callKeypad right keypadControllerFoodControl');
            // console.log($scope.keypadInfo.value);
            // console.log(angular.element('md-dialog[aria-label="keypad"]').scope());
            $scope.keypadInfo.lastValue = $scope.keypadInfo.value;
            $scope.keypadInfo.lastName = $scope.keypadInfo.name;
            $scope.keypadInfo.value = ''; // demo mode
            $scope.currentItem = item;
            if (MainService.modeOrder == MainService.schema.modeOrder.normal) {
                if (item.stock == -1) {
                    MainService.stock.updateStock(-2);
                } else {
                    if (angular.element('md-dialog[aria-label="keypad"]').scope() == undefined) {
                        $mdDialog.show({
                            controller: dialogCtrl,
                            // templateUrl: 'tmpl/keypad.html',
                            templateUrl: 'tmpl/keypad.html',
                            targetEvent: event,
                            focusOnOpen: true,
                            bindToController: true,
                            scope: $scope,
                            preserveScope: true,

                            // for generic purpose, each instance only affect local property keypad value
                            locals: {
                                keypadInfo: $scope.keypadInfo,
                            }
                        })
                    } else {
                        $mdDialog.hide();
                    }

                }
            }
        };

        $scope.currentItem = {};

        $scope.callKeypadFree = function (event, dialogCtrl, item) {
            console.log('callKeypadFree / keypadControllerOpenItem');

            // usage cases
            // case 1: not food control, add item to order
            // case 2: in foodcontrol, popup the dialog and ask for control item with control number
            console.log(item.type);
            // console.log($element);
            if( $scope.modeCheckOrder('detailNormal') && MainService.modeOrder != MainService.schema.modeOrder.foodControl ) {
                console.log('for food ordering');
                $scope.loadSubitem( item, dialogCtrl );
            } else {
                console.log('for food control');
                console.log(item);
                $scope.keypadInfo.lastValue = $scope.keypadInfo.value;
                $scope.keypadInfo.lastName = $scope.keypadInfo.name;
                $scope.keypadInfo.name = '';
                $scope.keypadInfo.value = '';
                $scope.currentItem = item;
                $scope.title = $scope.ui['food_control'] + ' / ' + item.name[INFO.lang];

                if( item.option != null && item.option.length > 0 ) {
                    // provide option for selection (either option or itself)
                    if (angular.element('md-dialog[aria-label="Food Control Option"]').scope() == undefined) {
                        console.log('food control option');
                        $mdDialog.show({
                            controller: dialogCtrl,
                            bindToController: true,
                            scope: $scope,
                            preserveScope: true,
                            controller: function ($scope) {
                                $scope.closeDialog = function () {
                                    $mdDialog.hide();
                                }
                            },
                            templateUrl: 'tmpl/foodControlOption.html'
                        })
                    } else {
                        $mdDialog.hide();
                    }
                } else {
                    // direct control current item
                    $scope.foodControlDirect( dialogCtrl );
                }
            }
        };

        $scope.loadSubitem = function( item, dialogCtrl ) {
            if (item.type === "o") {
                console.log('call callKeypad right keypadControllerFoodControl');
                console.log(item);
                // console.log(angular.element('md-dialog[aria-label="keypad"]').scope());
                $scope.keypadInfo.lastValue = $scope.keypadInfo.value;
                $scope.keypadInfo.lastName = $scope.keypadInfo.name;
                //$scope.keypadInfo.name = item.name1;
                $scope.keypadInfo.name = '';
                $scope.keypadInfo.value = '';
                $scope.currentItem = item;
                    if (item.stock == -1) {
                        console.log('-----------------------------1');
                        MainService.stock.updateStock(-2);
                    } else {
                        console.log('-----------------------------2');
                        if (angular.element('md-dialog[aria-label="keypad"]').scope() == undefined) {
                            $mdDialog.show({
                                controller: dialogCtrl,
                                // templateUrl: 'tmpl/keypad.html',
                                templateUrl: 'tmpl/keypad-free.html',
                                targetEvent: event,
                                focusOnOpen: true,
                                bindToController: true,
                                scope: $scope,
                                preserveScope: true,
                                onComplete: function () {
                                    // determine which is last active field for cancel button to discriminate
                                    $scope.lastActiveField = angular.element('#free-item-keypad input[name="item-name"]').attr('name');
                                    angular.element('#free-item-keypad input').on('focus', function () {
                                        // console.log(angular.element(this).attr('name'));
                                        $scope.lastActiveField = angular.element(this).attr('name');
                                    })
                                },

                                // for generic purpose, each instance only affect local property keypad value
                                locals: {
                                    keypadInfo: $scope.keypadInfo
                                }
                            })
                        } else {
                            $mdDialog.hide();
                        }

                    }
                // if (MainService.modeOrder == MainService.schema.modeOrder.normal) {
                // }
            } else {
                // console.log(item);
                MainService.Cart.addItem(item);
            }
        }

        $scope.foodControlDirect = function( dialogCtrl ) {
            if (angular.element('md-dialog[aria-label="keypad"]').scope() == undefined) {
                $mdDialog.show({
                    controller: dialogCtrl,
                    // templateUrl: 'tmpl/keypad.html',
                    templateUrl: 'tmpl/keypad.html',
                    targetEvent: event,
                    focusOnOpen: true,
                    bindToController: true,
                    scope: $scope,
                    preserveScope: true,
                    onComplete: function () {
                        // determine which is last active field for cancel button to discriminate
                        $scope.lastActiveField = angular.element('#free-item-keypad input[name="item-name"]').attr('name');
                        angular.element('#free-item-keypad input').on('focus', function () {
                            // console.log(angular.element(this).attr('name'));
                            $scope.lastActiveField = angular.element(this).attr('name');
                        })
                    },

                    // for generic purpose, each instance only affect local property keypad value
                    locals: {
                        keypadInfo: $scope.keypadInfo
                    }
                })
            } else {
                $mdDialog.hide();
            }
        }

        $scope.control = function( option, dialogCtrl ) {
            dialogCtrl = dialogCtrl ? dialogCtrl : angular.noop;
            switch( option ) {
                default:
                case 'this':
                    $scope.foodControlDirect( dialogCtrl );
                    break;
                case 'child':
                    // load option
                    console.log('load subitem');
                    // $scope.loadSubitem( $scope.currentItem );
                    MainService.Cart.loadOption($scope.currentItem );
                    $mdDialog.hide();
                    break;
            }
        }

        $scope.dialogTest1 = function ($scope, $mdDialog, keypadInfo) {
            console.log('dialogTest1');
            // console.log($scope.title);
            // console.log($scope.keypad);
            // console.log($scope.keypadInfo.value);
            console.log(keypadInfo);
            $scope.keypadInfo = keypadInfo;
        };

        $scope.triggerKeypad = function (element) {
            $timeout(function () {
                angular.element(element).trigger("click");
            }, 1);
        };
    });

    app.controller("foodMenuController", function ($scope, $controller) {
        // $controller('gridMenuOptionController', {$scope: $scope});

        // console.log($scope);
        // $scope.foodMenuCancel = function() {
        //     console.log('test');
        //     // alert('haha');
        // }

        // $scope.testtesttest = function(){}
    });
})();