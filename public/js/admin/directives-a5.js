(function () {
    var app = angular.module('rms.directives', ['rms.directives.dummy', 'common.directives', 'rms.templates']);
    // 'ngMaterial'
    app.config(function ($mdThemingProvider, $provide, $httpProvider) {
        $mdThemingProvider.theme('default')
          .primaryPalette("brown")
          .accentPalette('yellow')
          .warnPalette('red');
        // .backgroundPalette('lime')
        $httpProvider.useApplyAsync(true)
        // console.log($httpProvider.useApplyAsync( true ));
    });

    //------------------------------------------- update@2015/6/18
    // added angular-momentum-scroll in RMSAPP
    // added angular-momentum-scroll settings in order3 for left scrollbar

    app.directive('inputKeypad', function () {
        return {
            restrict: 'E',
            controller: 'keypadController',
            transclude: true,
            scope: {
                // customerInfo: '=info'
                keypadInfo: '@'
            },
            template: '',
            // <input class="{{class}}" ngMinlength="1" ngMaxlength="3" ng-model="keypadInfo.value" ng-click="callKeypad($event, dialogTest1)"/>
            // template: '<div class="{{class}}" ngMinlength="1" ngMaxlength="3" ng-click="callKeypad($event, dialogTest1)">{{keypadInfo.value}}</div>',
            link: function (scope, element, attrs, ctrl, transclude) {
                // console.log(attrs);
                // console.log(scope.keypadInfo);
                // console.log(scope.title);
                transclude(scope, function (clone) {
                    // copy attributes to transcluded item
                    for (var k = 0; k < element[0].attributes.length; k++) {
                        clone.attr(element[0].attributes[k].name, element[0].attributes[k].value);
                    }
                    element.append(clone);

                    // clone.filter('div').addClass('reddos');
                    // console.log(clone.attr("maxlength", scope.maxlength));
                    // for(var key in attrs) {
                    //     if(key.indexOf("$") === -1)
                    //         // console.log(key);
                    //         console.log(element[0].attributes);
                    //         // clone.attr(key, element[0].)
                    // }
                    // clone.attr("maxlength", scope.maxlength)

                    // console.log(element[0].attributes);
                    // console.log(element[0].attributes[0]);
                    // console.log(element[0].attributes[1]);
                });
                // copy attribute
                // for(attr in scope.list){
                //     elem.attr(scope.list[attr].attr, scope.list[attr].value);   
                // }
            }
            // variable template
            // templateUrl: function (elem, attr) {
            //     // console.log(attr)
            //     return attr.template;
            // },
            // replace:true, work but require at least one valid DOM native container like input, div etc
        };
    });

    app.directive('inputKeypadNoofpeople', function () {
        return {
            restrict: 'E',
            controller: 'keypadControllerNoofpeople',
            transclude: true,
            scope: {
                keypadInfo: '@'
            },
            template: '',
            link: function (scope, element, attrs, ctrl, transclude) {
                transclude(scope, function (clone) {
                    for (var k = 0; k < element[0].attributes.length; k++) {
                        clone.attr(element[0].attributes[k].name, element[0].attributes[k].value);
                    }
                    element.append(clone);
                });
            }
        };
    })

    app.controller("keypadControllerNoofpeople", function ($scope, $mdDialog, $element, $attrs, $timeout, MainService, $rootElement, $rootScope) {


        $scope.keypad = keypad;
        $scope.keypadInfo = {
            value: ''
        };
        $scope.title = $attrs.title ? $attrs.title : 'Keypad';

        $scope.$watch(function (scope) { return MainService.Cart.noOfPeople },
            function () {
                $scope.keypadInfo.value = MainService.Cart.noOfPeople;
            }
        );

        $scope.input = function (val) {
            var maxlength = $attrs.maxlength ? $attrs.maxlength : 3;
            console.log(String($scope.keypadInfo.value).length);

            // reset value if 0, when zero operate as string, it will become 01, 02...
            if (String($scope.keypadInfo.value).length == 1 && $scope.keypadInfo.value == 0)
                $scope.keypadInfo.value = '';

            if (String($scope.keypadInfo.value).length < maxlength)
                $scope.keypadInfo.value += val;
        };

        // $scope.backspace = function () {
        //     console.log('me?');
        //     $scope.keypadInfo.value = $scope.keypadInfo.value.substring(0, $scope.keypadInfo.value.length - 1)
        // };

        $scope.backspace = function () {
            var inputValue = String($scope.keypadInfo.value);
            $scope.keypadInfo.value = inputValue.substring(0, inputValue.length - 1)
        };

        $scope.reset = function (val) {
            console.log($scope.keypadInfo.last);
            if ($scope.keypadInfo.value == '') {
                $scope.keypadInfo.value = $scope.keypadInfo.last;
                $mdDialog.cancel();
            } else {
                $scope.keypadInfo.value = '';
            }

            // cancel order if not input any people
            if ($scope.keypadInfo.value == 0)
                MainService.Cart.cancelOrder();
        };

        $scope.keypressed = function (event) {
            console.log(event);
        };

        $scope.next = function (event) {
            $mdDialog.hide();
            MainService.Cart.noOfPeople = $scope.keypadInfo.value;
            angular.element($element).blur();
        };

        $scope.closeDialog = function ($mdDialog) {
            $scope.keypadInfo.value = $scope.keypadInfo.last;
            $mdDialog.cancel();
        };

        $scope.callKeypad = function (dialogCtrl) {
            console.log('call callKeypad');
            //console.log(event);
            // console.log($scope.keypadInfo.value);
            // .Cart.tableNo
            // console.log('MainService.Cart.noOfPeople');
            // console.log(MainService.Cart.noOfPeople);
            console.log($scope.keypadInfo.value);

            console.log(MainService.mode);
            console.log(MainService.modeOrder);
            console.log(MainService.modeItem);

            if (MainService.modeOrder == MainService.schema.modeOrder.searchBill || MainService.modeOrder == MainService.schema.modeOrder.amendBill) {
                return;
            }

            $scope.keypadInfo.last = MainService.Cart.noOfPeople;
            $scope.keypadInfo.value = '';
            //console.log(angular.element('md-dialog[aria-label="keypad"]').scope());
            //console.log(123);
            console.log('$scope.keypadInfo.value');
            console.log($scope.keypadInfo.value);
            if (angular.element('md-dialog[aria-label="keypad"]').scope() == undefined) {
                $mdDialog.show({
                    controller: dialogCtrl,
                    templateUrl: 'tmpl/keypad.html',
                    // targetEvent: event,
                    escapeToClose: false,
                    focusOnOpen: true,
                    bindToController: true,
                    scope: $scope,
                    preserveScope: true,
                    // for generic purpose, each instance only affect local property keypad value
                    locals: {
                        keypadInfo: $scope.keypadInfo,
                    }
                })

                $rootElement.on('keyup', function (e) {
                    // console.log("its's me");
                    console.log($scope.keypadInfo);
                    if (e.keyCode === 27) {
                        $timeout($mdDialog.hide);
                        $scope.keypadInfo.value = $scope.keypadInfo.last;
                    }
                });
            } else {
                $mdDialog.hide();
            }
        };

        $scope.dialogTest1 = function ($scope, $mdDialog, keypadInfo) {
            //console.log('dialogTest1');
            // console.log($scope.title);
            // console.log($scope.keypad);
            // console.log($scope.keypadInfo.value);
            // console.log(keypadInfo);
            $scope.keypadInfo = keypadInfo;
        };

        // MainService.setNoOfPeople = $scope.callKeypad;
        // MainService.keypadDialog = $scope.dialogTest1;
        // MainService.input = $scope.input;
        MainService.setNoOfPeople = function () {
            $scope.callKeypad($scope.dialogTest1);
        }

        // callKeypad($event, dialogTest1)
        // $scope.callKeypad($scope.dialogTest1);
        // $rootScope.$on('assignTable', function(event, args) {
        //     console.log(event);
        //     console.log('table is assigned');
        //     $scope.callKeypad($scope.dialogTest1);
        // });
    });

    app.controller("keypadController", function ($scope, $mdDialog, $element, $attrs, $timeout, MainService) {
        // console.log($scope.title);
        // console.log($scope.keypadInfo.value);
        console.log($attrs);

        $scope.keypad = keypad;
        $scope.keypadInfo = {
            value: ''
        };
        $scope.title = $attrs.title ? $attrs.title : 'Keypad';

        //$scope.$watch(function (scope) { return MainService.Cart.noOfPeople },
        //    function () {
        //        $scope.keypadInfo.value = MainService.Cart.noOfPeople;
        //    }
        //);

        $scope.input = function (val) {
            var maxlength = $attrs.maxlength ? $attrs.maxlength : 3;
            if ($scope.keypadInfo.value.length < maxlength)
                $scope.keypadInfo.value += '' + val;
        };

        $scope.backspace = function () {
            $scope.keypadInfo.value = $scope.keypadInfo.value.substring(0, $scope.keypadInfo.value.length - 1)
        };

        $scope.reset = function (val) {
            console.log($scope.keypadInfo.last);
            if ($scope.keypadInfo.value == '') {
                $scope.keypadInfo.value = $scope.keypadInfo.last;
                $mdDialog.cancel();
            } else {
                $scope.keypadInfo.value = '';
            }
        };

        $scope.keypressed = function (event) {
            console.log(event);
        };

        $scope.next = function (event) {
            $mdDialog.hide();
            //MainService.Cart.noOfPeople = $scope.keypadInfo.value;
            angular.element($element).blur();
        };

        $scope.closeDialog = function ($mdDialog) {
            $scope.keypadInfo.value = $scope.keypadInfo.last;
            $mdDialog.cancel();
        };

        $scope.callKeypad = function (event, dialogCtrl) {
            // console.log('call callKeypad');
            // console.log($scope.keypadInfo.value);
            $scope.keypadInfo.last = $scope.keypadInfo.value;
            // console.log(angular.element('md-dialog[aria-label="keypad"]').scope());
            $scope.keypadInfo.value = '';
            if (angular.element('md-dialog[aria-label="keypad"]').scope() == undefined) {
                $mdDialog.show({
                    controller: dialogCtrl,
                    templateUrl: 'tmpl/keypad.html',
                    targetEvent: event,
                    focusOnOpen: true,
                    bindToController: true,
                    scope: $scope,
                    preserveScope: true,

                    // for generic purpose, each instance only affect local property keypad value
                    locals: {
                        keypadInfo: $scope.keypadInfo,
                    }
                })
            } else {
                $mdDialog.hide();
            }
        };

        $scope.dialogTest1 = function ($scope, $mdDialog, keypadInfo) {
            console.log('dialogTest1');
            // console.log($scope.title);
            // console.log($scope.keypad);
            // console.log($scope.keypadInfo.value);
            console.log(keypadInfo);
            $scope.keypadInfo = keypadInfo;
        };

        $scope.triggerKeypad = function (element) {
            $timeout(function () {
                angular.element(element).trigger("click");
            }, 1);
        };
    });

    app.controller("keypadControllerFoodControl", function ($scope, $mdDialog, $element, $attrs, $timeout, MainService) {
        // console.log($scope.title);
        // console.log($scope.keypadInfo.value);
        //console.log($attrs);
        $scope.keypad = keypad;
        $scope.keypadInfo = {
            value: ''
        };
        $scope.title = $attrs.title ? $attrs.title : 'Keypad';

        //$scope.$watch(function (scope) { return MainService.Cart.noOfPeople },
        //    function () {
        //        $scope.keypadInfo.value = MainService.Cart.noOfPeople;
        //    }
        //);

        $scope.input = function (val) {
            var maxlength = $attrs.maxlength ? $attrs.maxlength : 3;
            if ($scope.keypadInfo.value.length < maxlength)
                $scope.keypadInfo.value += '' + val;
        };

        $scope.backspace = function () {
            $scope.keypadInfo.value = $scope.keypadInfo.value.substring(0, $scope.keypadInfo.value.length - 1)
        };

        $scope.reset = function (val) {
            console.log($scope.keypadInfo.last);
            if ($scope.keypadInfo.value == '') {
                $scope.keypadInfo.value = $scope.keypadInfo.last;
                $mdDialog.cancel();
            } else {
                $scope.keypadInfo.value = '';
            }
        };

        $scope.keypressed = function (event) {
            console.log(event);
        };

        $scope.next = function (event) {
            $mdDialog.hide();
            //MainService.Cart.noOfPeople = $scope.keypadInfo.value;
            MainService.stock.updateStock($scope.keypadInfo.value);
            angular.element($element).blur();
        };

        $scope.closeDialog = function ($mdDialog) {
            $scope.keypadInfo.value = $scope.keypadInfo.last;
            $mdDialog.cancel();
        };

        $scope.callKeypad = function (event, dialogCtrl, item) {
            // console.log('call callKeypad');
            // console.log($scope.keypadInfo.value);
            // console.log(angular.element('md-dialog[aria-label="keypad"]').scope());
            $scope.keypadInfo.last = $scope.keypadInfo.value;
            $scope.keypadInfo.value = '';
            if (MainService.modeOrder == MainService.schema.modeOrder.foodControl) {
                console.log('item.stock ' + item.stock);
                if (item.stock == -1 || item.stock == 999) {
                    console.log('update stock number');
                    MainService.stock.updateStock(-2);
                } else {
                    if (angular.element('md-dialog[aria-label="keypad"]').scope() == undefined) {
                        $mdDialog.show({
                            controller: dialogCtrl,
                            templateUrl: 'tmpl/keypad.html',
                            targetEvent: event,
                            focusOnOpen: true,
                            bindToController: true,
                            scope: $scope,
                            preserveScope: true,

                            // for generic purpose, each instance only affect local property keypad value
                            locals: {
                                keypadInfo: $scope.keypadInfo,
                            }
                        })
                    } else {
                        $mdDialog.hide();
                    }

                }
            }
        };

        $scope.dialogTest1 = function ($scope, $mdDialog, keypadInfo) {
            console.log('dialogTest1');
            // console.log($scope.title);
            // console.log($scope.keypad);
            // console.log($scope.keypadInfo.value);
            console.log(keypadInfo);
            $scope.keypadInfo = keypadInfo;
        };

        $scope.triggerKeypad = function (element) {
            $timeout(function () {
                angular.element(element).trigger("click");
            }, 1);
        };
    });

    // app.controller("keypadControllerOpenItem", function ($scope, $mdDialog, $element, $attrs, $timeout, MainService) {
    //     $scope.keypad = keypad;
    //     $scope.keypadInfo = {
    //         value: '0.0',
    //         name: ''
    //     };
    //     $scope.title = $attrs.title ? $attrs.title : 'Keypad';
    //     $scope.input = function (val) {
    //         var maxlength = $attrs.maxlength ? $attrs.maxlength : 3;
    //         if ($scope.keypadInfo.value.length < maxlength)
    //             $scope.keypadInfo.value += '' + val;
    //     };

    //     $scope.currentFieldName = "name";
    //     $scope.focusField = function (fieldName) {
    //         this.currentFieldName = fieldName;
    //         return true;
    //     }

    //     // for non-numerical character
    //     $scope.inputFree = function (val) {
    //         console.log($scope.keypadInfo);
    //         // simpler:
    //         // number type field
    //         if (this.currentFieldName === 'value') {
    //             switch (val) {
    //                 case '.':
    //                     console.log('. ar');
    //                     this.appendNumFree(val);
    //                     break;
    //                 case 'tab':
    //                 case 'space':
    //                     break;
    //                 default:
    //                     /*$scope.keypadInfo[this.currentFieldName] = parseFloat( String( $scope.keypadInfo[this.currentFieldName] ) + val );
    //                     break;*/
    //             };
    //         } else {
    //             // text field (exclude date, time and so on... bare text only)
    //             switch (val) {
    //                 case 'space':
    //                     val = " ";
    //                     break;
    //                 case 'tab':
    //                     val = '';
    //                     for (var i = 0; i < 3; i++) {
    //                         val += " ";
    //                     }
    //                     break;
    //                 default:
    //                     break;
    //             };
    //             $scope.keypadInfo[this.currentFieldName] += '' + val;
    //         }

    //         // switch( this.currentFieldName ) {
    //         //     case "value":
    //         //         $scope.keypadInfo.value += '' + val;
    //         //         break;
    //         //     default:
    //         //     case "name":
    //         //         $scope.keypadInfo.name += '' + val;
    //         //         break;
    //         // }
    //     };

    //     // for numerical character, support append, decimal points
    //     $scope.appendNumFree = function (amount) {

    //         console.log(amount);
    //         // do nothing if floating point is already input
    //         var inputValue = $scope.keypadInfo[this.currentFieldName];
    //         if (amount === '.' && $scope.keypadInfo[this.currentFieldName][inputValue.length - 1] === '.') {
    //             console.log('0');
    //             return;
    //         }

    //         // if floating point is activated, not allow to add one character
    //         // this.inputPrice += '';
    //         $scope.keypadInfo[this.currentFieldName] = String(inputValue); // type aware method and meaning is obvious without comment
    //         if (inputValue.indexOf('.') === -1 && amount === '.') {
    //             console.log('1');
    //             this.floatingInput = true;
    //             // not necessary because " ", it will make it .# instead of 0.# when it is 0
    //             // if (this.inputPrice.trim() == "0") { // input 7, result: .7 instead of 0.7
    //             //     console.log('1-1');
    //             //     console.log(this.inputPrice.trim());
    //             //     this.inputPrice = amount;
    //             // }else{
    //             //     console.log('1-2');
    //             //     this.inputPrice = this.inputPrice.concat(amount);
    //             // }
    //             $scope.keypadInfo[this.currentFieldName] = inputValue.trim().concat(amount);
    //             return;
    //         }

    //         if (inputValue.indexOf('.') != -1 && amount === '.') {
    //             console.log('2');
    //             return
    //         }

    //         // console.log(this.inputPrice.indexOf('.'));
    //         // console.log( !isNaN(parseInt(this.inputPrice[this.inputPrice.length - 1])));
    //         if (inputValue.indexOf('.') != -1 && !isNaN(parseInt($scope.keypadInfo[this.currentFieldName][inputValue.length - 1]))) {
    //             console.log("3");
    //             return;
    //         }
    //         // console.log(typeof this.inputPrice);
    //         // console.log(typeof this.inputPrice.trim());
    //         // console.log('when is this.inputPrice.trim() == 0 ' + this.inputPrice.trim());

    //         if (inputValue.trim() == "0") { // 排除預設等值有space的障礙
    //             console.log("4");
    //             $scope.keypadInfo[this.currentFieldName] = amount;
    //         } else {
    //             console.log("5");
    //             // append 0 if prev is "."
    //             if (inputValue[0] === '.') {
    //                 console.log("0" + inputValue);
    //                 $scope.keypadInfo[this.currentFieldName] = "0" + inputValue.concat(amount);
    //             } else {
    //                 $scope.keypadInfo[this.currentFieldName] = inputValue.concat(amount);
    //             }

    //         }
    //         // default
    //     }

    //     $scope.backspaceFree = function () {
    //         console.log($scope.keypadInfo);
    //         // convert it to string or it will get error when it is 0 and use backspace

    //         if (this.currentFieldName === 'value') {
    //             var inputValue = String($scope.keypadInfo[this.currentFieldName]);
    //             /*var tmpStr = String( $scope.keypadInfo[this.currentFieldName] );
    //             if( tmpStr.length > 1) {
    //                 $scope.keypadInfo[this.currentFieldName] = parseFloat( tmpStr.substring(0, tmpStr.length - 1) );
    //             }

    //             if( tmpStr.length == 1) {
    //                 $scope.keypadInfo[this.currentFieldName] = 0;
    //             }*/

    //             if (inputValue.length === 0)
    //                 return;

    //             console.log(inputValue);
    //             if (inputValue[inputValue.length - 1] === '.') {
    //                 this.floatingInput = false;
    //             }

    //             $scope.keypadInfo[this.currentFieldName] = inputValue.substring(0, inputValue.length - 1);

    //             if (isNaN(inputValue) || inputValue === '') {
    //                 console.log('nan');
    //                 $scope.keypadInfo[this.currentFieldName] = '';
    //             }
    //         } else {
    //             $scope.keypadInfo[this.currentFieldName] = $scope.keypadInfo[this.currentFieldName].substring(0, $scope.keypadInfo[this.currentFieldName].length - 1);
    //         }
    //     }

    //     $scope.backspace = function () {
    //         $scope.keypadInfo.value = $scope.keypadInfo.value.substring(0, $scope.keypadInfo.value.length - 1);
    //     };

    //     $scope.reset = function (val) {
    //         console.log($scope.keypadInfo.lastValue);
    //         console.log($scope.keypadInfo.lastName);
    //         if ($scope.keypadInfo.name == '') {
    //             $scope.keypadInfo.name = $scope.keypadInfo.lastName;
    //             $mdDialog.cancel();
    //         } else {
    //             $scope.keypadInfo.name = '';
    //         }

    //         if ($scope.keypadInfo.value == '') {
    //             $scope.keypadInfo.value = $scope.keypadInfo.lastValue;
    //             $mdDialog.cancel();
    //         } else {
    //             $scope.keypadInfo.value = '';
    //         }
    //     };

    //     $scope.cancel = function () {
    //         // var focusedElement = document.activeElement;
    //         // console.log(focusedElement);
    //         // console.log($scope.lastActiveField);
    //         console.log('cancel');
    //         console.log($scope.lastActiveField == "item-name" && $scope.keypadInfo.name != "");
    //         console.log($scope.lastActiveField == "item-price" && $scope.keypadInfo.value != "");
    //         console.log($scope.lastActiveField);
    //         console.log($scope.keypadInfo.value);
    //         if ($scope.lastActiveField == "item-name" && $scope.keypadInfo.name != "") {
    //             $scope.keypadInfo.name = "";
    //             return;
    //         }

    //         if ($scope.lastActiveField == "item-price" && $scope.keypadInfo.value != "") {
    //             $scope.keypadInfo.value = "";
    //             return;
    //         }

    //         $mdDialog.cancel();
    //     }

    //     $scope.keypressed = function (event) {
    //         console.log(event);
    //     };

    //     $scope.next = function (event) {
    //         $mdDialog.hide();
    //         //MainService.Cart.noOfPeople = $scope.keypadInfo.value;
    //         MainService.stock.updateStock($scope.keypadInfo.value);
    //         angular.element($element).blur();
    //         alert($scope.keypadInfo.value);
    //     };

    //     $scope.checkDecimal = function (amount) {
    //         if (amount[amount.length - 1] === '.' || amount[0] === '.')
    //             return false; // input is incorrect
    //         else
    //             return true;
    //     }

    //     $scope.nextFree = function (event) {
    //         if (!this.checkDecimal($scope.keypadInfo.value) || $scope.keypadInfo.value === '') {
    //             return; // input is not completed
    //         }

    //         $mdDialog.hide();
    //         //MainService.Cart.noOfPeople = $scope.keypadInfo.value;
    //         //MainService.stock.updateStock($scope.keypadInfo.value);
    //         angular.element($element).blur();
    //         //alert($scope.keypadInfo.value);
    //         //alert($scope.keypadInfo.name);
    //         console.log($scope.currentItem);
    //         //$scope.currentItem.name = $scope.keypadInfo.name;

    //         var customItem = {};
    //         $.extend(true, customItem, $scope.currentItem);
    //         customItem.desc1 = $scope.keypadInfo.name;
    //         customItem.name['001'] = $scope.keypadInfo.name;
    //         customItem.customName = $scope.keypadInfo.name;
    //         customItem.unitprice = parseFloat($scope.keypadInfo.value) * 100;
    //         console.log(customItem)
    //         MainService.Cart.addItem(customItem);
    //     };

    //     $scope.closeDialog = function ($mdDialog) {
    //         $scope.keypadInfo.value = $scope.keypadInfo.lastValue;
    //         $scope.keypadInfo.name = $scope.keypadInfo.lastName;
    //         $mdDialog.cancel();
    //     };

    //     $scope.callKeypad = function (event, dialogCtrl, item) {
    //         console.log('call callKeypad right keypadControllerFoodControl');
    //         // console.log($scope.keypadInfo.value);
    //         // console.log(angular.element('md-dialog[aria-label="keypad"]').scope());
    //         $scope.keypadInfo.lastValue = $scope.keypadInfo.value;
    //         $scope.keypadInfo.lastName = $scope.keypadInfo.name;
    //         $scope.keypadInfo.value = '';
    //         if (MainService.modeOrder == MainService.schema.modeOrder.normal) {
    //             if (item.stock == -1) {
    //                 MainService.stock.updateStock(-2);
    //             } else {
    //                 if (angular.element('md-dialog[aria-label="keypad"]').scope() == undefined) {
    //                     $mdDialog.show({
    //                         controller: dialogCtrl,
    //                         // templateUrl: 'tmpl/keypad.html',
    //                         templateUrl: 'tmpl/keypad.html',
    //                         targetEvent: event,
    //                         focusOnOpen: true,
    //                         bindToController: true,
    //                         scope: $scope,
    //                         preserveScope: true,

    //                         // for generic purpose, each instance only affect local property keypad value
    //                         locals: {
    //                             keypadInfo: $scope.keypadInfo,
    //                         }
    //                     })
    //                 } else {
    //                     $mdDialog.hide();
    //                 }

    //             }
    //         }
    //     };

    //     $scope.currentItem = {};

    //     $scope.callKeypadFree = function (event, dialogCtrl, item) {
    //         $timeout(function () {
    //             // $element[0].focus();
    //         });
    //         console.log(item.type);
    //         if (item.type === "o" && MainService.modeOrder != MainService.schema.modeOrder.foodControl) {
    //             console.log($element);
    //             console.log('call callKeypad right keypadControllerFoodControl');
    //             console.log(item);
    //             // console.log(angular.element('md-dialog[aria-label="keypad"]').scope());
    //             $scope.keypadInfo.lastValue = $scope.keypadInfo.value;
    //             $scope.keypadInfo.lastName = $scope.keypadInfo.name;
    //             //$scope.keypadInfo.name = item.name1;
    //             $scope.keypadInfo.name = '';
    //             $scope.keypadInfo.value = '';
    //             $scope.currentItem = item;
    //             if (MainService.modeOrder == MainService.schema.modeOrder.normal) {
    //                 if (item.stock == -1) {
    //                     MainService.stock.updateStock(-2);
    //                 } else {
    //                     if (angular.element('md-dialog[aria-label="keypad"]').scope() == undefined) {
    //                         $mdDialog.show({
    //                             controller: dialogCtrl,
    //                             // templateUrl: 'tmpl/keypad.html',
    //                             templateUrl: 'tmpl/keypad-free.html',
    //                             targetEvent: event,
    //                             focusOnOpen: true,
    //                             bindToController: true,
    //                             scope: $scope,
    //                             preserveScope: true,
    //                             onComplete: function () {
    //                                 // determine which is last active field for cancel button to discriminate
    //                                 $scope.lastActiveField = angular.element('#free-item-keypad input[name="item-name"]').attr('name');
    //                                 angular.element('#free-item-keypad input').on('focus', function () {
    //                                     // console.log(angular.element(this).attr('name'));
    //                                     $scope.lastActiveField = angular.element(this).attr('name');
    //                                 })
    //                             },

    //                             // for generic purpose, each instance only affect local property keypad value
    //                             locals: {
    //                                 keypadInfo: $scope.keypadInfo
    //                             }
    //                         })
    //                     } else {
    //                         $mdDialog.hide();
    //                     }

    //                 }
    //             }
    //         } else {

    //             MainService.Cart.addItem(item);
    //         }
    //     };

    //     $scope.dialogTest1 = function ($scope, $mdDialog, keypadInfo) {
    //         console.log('dialogTest1');
    //         // console.log($scope.title);
    //         // console.log($scope.keypad);
    //         // console.log($scope.keypadInfo.value);
    //         console.log(keypadInfo);
    //         $scope.keypadInfo = keypadInfo;
    //     };

    //     $scope.triggerKeypad = function (element) {
    //         $timeout(function () {
    //             angular.element(element).trigger("click");
    //         }, 1);
    //     };
    // });

    app.directive('rmsTop', function (INFO, uiLanguage, userConfig) {
        return {
            restrict: "E",
            templateUrl: 'tmpl/top-bar.html',
            controller: function ($scope, INFO, uiLanguage, userConfig) {
                // console.log('called me?'); // answer: once
                $scope.config = userConfig;
                $scope.ui = uiLanguage[INFO.lang];
            }
        }
    })

    app.directive('rmsStatus', function (INFO, uiLanguage, userConfig) {
        return {
            restrict: "E",
            templateUrl: 'tmpl/status-bar.html',
            controller: function ($scope, INFO, uiLanguage, userConfig) {
                $scope.config = userConfig;
                $scope.ui = uiLanguage[INFO.lang];
            }
        }
    })

    app.controller('tableController', function ($scope) {
        // the scope inside this controller
        // default table icon settings

        // console.log($scope.RMSTable);
        // console.log($scope.svgScene);

        // console.log($element);
        // $http.get('data/table_type.json').success(function (data) {
        //     $scope.tableTypeJSON = data;
        //     // console.log(data);
        // });
        // share with other controllers or change state of the directive such as active/inactive
        $scope.RMSTable = {};
        $scope.cancelTransfer = function cancelTransfer() {
            this.removeAllTransferBB();
            var svgScene = Snap("#svg-canvas");
            svgScene.selectAll(".transfer").forEach(function (element, index, array) {
                element.removeClass("transfer");
            });
            $scope.RMSTable.transfer_to = 9999;
            $scope.RMSTable.transfer_from = 9999;
        }

        $scope.removeAllTransferBB = function removeAllTransferBB() {
            var svgScene = Snap("#svg-canvas");
            if ((activeTransferItem = svgScene.selectAll(".transfer")).length != 0) {
                // console.log(activeTransferItem);
                activeTransferItem.forEach(this.removeBB);
            }
        }

        $scope.removeBB = function removeBB(element, index, array) {
            // console.log(element);
            element.ftRemoveBB();
        }
    });

    app.directive('reportScene', function (MainService) {
        return {
            restrict: "E",
            templateUrl: 'tmpl/reportScene.html'
        }
    });

    app.directive('searchBillScene', function (MainService) {
        return {
            restrict: "E",
            templateUrl: 'tmpl/searchBillScene.html'
        }
    });

    app.directive('orderScene', function ($http, INFO, MainService, uiLanguage) {
        // console.log(MainService.selectPaymentMethod);
        return {
            restrict: "E",
            templateUrl: 'tmpl/order.html',
            link: function (scope, element) {
            },
            controller: 'orderController'
        }
    });

    app.controller('orderController', function ($scope, INFO, SocketFactory, $element, MainService, $http, $rootScope, uiLanguage, $mdDialog) {
        // $mdDialog.show();
        $scope.ui = uiLanguage[INFO.lang];
        var watchFood = $scope.$watch(function (scope) { return MainService.food },
              function () {
                  if (MainService.food) {
                      // console.log(MainService.food);
                      watchFood();
                  }
              }
             );
        $scope.MainService = MainService;
        $scope.$on('submitOrder', function (o) {
            $scope.cart.submit(SocketFactory);
        });

        $scope.$watch('MainService.Cart', function (newValue, oldValue) {
            if (newValue === oldValue) return;
            //var totalPrice = 0;
            //var price = 0;
            //$.each(newValue.cartList, function (i, v) {
            //    //if (v.getQty() > 0 && !v.isDelete)
            //        price += v.getTotalPrice();
            //});
            //$.each(newValue.orderList, function (i, v) {
            //    //if (v.getQty() > 0)
            //        price += v.getTotalPrice();
            //});
            //newValue.price = price;
            // console.log(price)
            var couponObj;
            //console.log(scObj);
            //console.log(newValue.couponObj);
            if (MainService.appliedDiscount) {
                if (MainService.appliedDiscount.length != 0) {
                    couponObj = MainService.appliedDiscount;
                }
            }
            //if (newValue.couponObj) {
            //    if (newValue.couponObj.length != 0) {
            //        couponObj = newValue.couponObj;
            //    }
            //}

            var priceObj = MainService.calcPrice([newValue.cartList, newValue.orderList], couponObj);

            newValue.price = priceObj.price;

            newValue.serviceCharge = priceObj.serviceCharge;
            newValue.remainings = priceObj.remainings;
            newValue.totalPrice = priceObj.totalPrice;

        }, true);

        $scope.INFO = INFO;
        $scope.currentPage = 1;
        $scope.currentCatsPage = 1;
        $scope.itemsPerPage = 20;
        $scope.modeOrder = 0;
        $scope.modeOrderDetail = 1;
        $scope.modeFoodControl = 2;


        $scope.cart = {
            tableNo: '',
            cartList: [],
            orderList: [],
            outofstockList: [],
            setOutofstock: function (obj) {
                outofstockList = obj;
                if ($scope.food) {

                }
            },
            assignTableNo: function (tableNo) {
                this.tableNo = tableNo;
                SocketFactory.emit('loadTableOrder', { 'tableNum': { '$value': tableNo } }, function (r) {
                    r = JSON.parse(r);
                    console.log(r.result)
                    if (r.result === "OK") {
                        //console.log(r)
                        $scope.cart.orderList = [];
                        angular.forEach(r.order.item, function (v, k) {
                            //this.push(key + ': ' + value);
                            var item = $.extend(true, {}, $scope.baseItem, v);
                            $scope.cart.orderList.push(item);
                        });
                    }
                })
            },
            submit: function (SocketFactory) {
                console.log('submit');
                console.log(this.cartList.length);
                var order = { item: [] };
                angular.forEach(this.cartList, function (item) {
                    order.item.push({
                        code: [item.code], qty: [item.qty], unitPrice: [item.unitprice], voidIndex: [item.voidIndex], voidRemark: [item.voidRemark], type: ['I'],
                        desc1: [item.getName()], customName: [item.customName]
                    });
                });
                if (order.item.length > 0) {
                    var postData = {
                        "action": "MDSaveTableOrder",
                        "tableNum": {
                            "$value": this.tableNo
                        },
                        "itemData": {
                            "json": {
                                "order": order
                            }
                        }
                    };
                    SocketFactory.emit('saveTableOrder', postData, function (r) {
                        r = JSON.parse(r);
                        //console.log(r.result);
                        SocketFactory.emit('loadTableOrder', { 'tableNum': { '$value': $scope.cart.tableNo } }, function (r) {
                            r = JSON.parse(r);
                            console.log(r.result)
                            if (r.result === "OK") {
                                //console.log(r)
                                $scope.cart.orderList = [];
                                $scope.cart.cartList = [];
                                angular.forEach(r.order.item, function (v, k) {
                                    //this.push(key + ': ' + value);
                                    var item = $.extend(true, {}, $scope.baseItem, v);
                                    $scope.cart.orderList.push(item);
                                });
                            }
                            //console.log(111111);
                            //console.log(r);
                            //$scope.cart
                        })
                    });
                }
            }
        };

        SocketFactory.emit('loadItemQty', '', function (r) {
            // console.log("debug000");
            // console.log(r);
            //angular.forEach($scope.cart)
        });

        $scope.selectCategory = function (category) {
            //console.log(category);
            // $scope.previousCategory = $scope.currentCategory;
            $scope.currentCategory = category;
            $scope.currentPage = 1;
            $scope.totalItems = $scope.currentCategory.items.length;
        }

        $scope.changeQty = function (number) {

        };

        $scope.selectItem = function (item) {
            //console.log(item);
        };

        $scope.editItem = function (item) {

        };

        $scope.baseItem = {
            getName: function () {
                if (this.name != undefined) {
                    return this.name[$scope.INFO.lang];
                }
                else {
                    return this.desc1;
                }
            },
            qty: 1,
            voidIndex: -1,
            voidRemark: ''
        }

        $scope.toggleFoodControl = function () {
            if ($element.hasClass('ng-hide')) {
                angular.element('table-scene').scope().Hide();
                $element.removeClass('ng-hide');
            }

            if ($scope.mode != angular.element('order-scene').scope().modeFoodControl) {
                $scope.mode = $scope.modeFoodControl;
            } else {
                $scope.mode = $scope.modeOrder;
                if ($scope.cart.tableNo == '') {
                    angular.element('rms-svg').scope().Show();
                    $element.addClass('ng-hide');
                }
            }
            //angular.element('order-ctrl').scope().mode = angular.element('order-ctrl').scope().modeFoodControl;
        }
    });

    app.directive('mainScene', function ($http, INFO, MainService, uiLanguage) {
        // console.log(MainService.selectPaymentMethod);
        return {
            restrict: "E",
            templateUrl: 'tmpl/main.html',
            link: function (scope, element) {
            },
            controller: 'mainController'
        }
    });

    app.controller('mainController', function ($scope, INFO, SocketFactory, $element, MainService, $http, $rootScope, uiLanguage, $mdDialog) {
        $scope.MainService = MainService;
    })

    app.directive('loginInput', function ($timeout, $parse, userConfig) {
        return {
            require: '?ngModel',
            link: function (scope, elem, attrs, ngModel) {
                elem.bind("keydown keypress", function (event) {
                    if (event.which === 13) {
                        scope.$apply(attrs.loginInputEnter);
                        event.preventDefault();
                    }
                });
            },
            controller: function ($scope) {
                $scope.loginValue = userConfig.dev ? userConfig.devParam.password : ''; // test

                $scope.change = function (v) {
                    //$scope.counter++;
                    console.log($scope.loginValue);
                };

                $scope.$watch('loginValue', function (newValue, oldValue) {
                    // access new and old value here
                    //console.log("Your former user.name was " + oldValue + ", you're current user name is " + newValue + ".");
                    if (isNaN(newValue) || newValue.indexOf('.') != -1 || newValue.indexOf('-') != -1) {
                        //console.log('debug' + attrs.ngModel);
                        //console.log('debug' + scope.login.value);
                        $scope.loginValue = oldValue ? oldValue : '';
                    }
                    $scope.loginValue = $scope.loginValue.trim();
                }, true);

                $scope.resetLoginInputValue = function () {
                    $scope.loginValue = userConfig.dev ? userConfig.devParam.password : ''; // test
                    //ngModel.$setViewValue('');
                    //ngModel.$render();
                }
            }
        };
    });

    app.directive('menuLayout', function ($timeout, $sce, MainService, $rootScope) {
        return {
            scope: {
                linkData: '=?',
                linkIndex: "=?",
                linkResource: "=?",
                linkType: '=?'
            },
            restrict: 'A',
            require: '?ngModel',
            link: function (scope, element, attr, ngModel) {
                //if (!ngModel) return;
                switch (scope.linkType) {
                    case "tile": {
                        ngModel.$render = function () {
                            $(element).data({"layout": $.extend(scope.linkData, {"index": scope.linkIndex}), "res": scope.linkResource});
                            $(element).tileItem({
                                complete: function (event, data) {
                                },
                                active: function (event, element) {
                                    scope.$apply(function () {
                                        console.log('active', $(element).data("layout"));
                                        $rootScope.selectItem($(element).data("layout"));
                                    });
                                },
                                navigate: function (event, element) {
                                    console.log("navigate", element);
                                    scope.$apply(function () {
                                        $rootScope.beginLayoutTransaction($(element).data("layout"));
                                    });
                                },
                                copy: function (event, element) {
                                    console.log("copy", element);
                                    scope.$apply(function () {
                                        $rootScope.copyTileItem(element.data("layout"))
                                    });
                                },
                                delete: function (event, element) {
                                    $.extend($(element).data("layout"), { isDelete: true });
                                    console.log("delete!", element.data());
                                }
                            });
                        }
                        break;
                    }

                    case "collapsible": {
                        ngModel.$render = function () {
                            $(element).collapsible({
                                accordion: true // A setting that changes the collapsible behavior to expandable instead of the default accordion style
                            });
                            Materialize.updateTextFields();
                        }
                        break;
                    }

                    case "select": {
                        ngModel.$render = function () {
                            //console.log("$render", ngModel.$viewValue, element.val(), element, element.html());
                            if (ngModel.$viewValue != null && ngModel.$viewValue != undefined) {
                                if (attr.multiple) {
                                    var value = $.isArray(ngModel.$viewValue) ? ngModel.$viewValue : ngModel.$viewValue.split(",");
                                    console.log("multiple select", value, attr, attr.multiple)
                                    if (value.length) {
                                        var hasValue = false
                                        $.each(value, function (i, e) {
                                            //element.find("option[value=" + e + "]").attr("selected", "selected");
                                            if (e.length) {
                                                hasValue = true;
                                                element.find("option[value=" + e + "]").prop("selected", true);
                                                console.log(element.find("option[value=" + e + "]").prop("selected"));
                                            }
                                        });
                                        if (!hasValue) value = [];
                                        element.val(value);
                                        ngModel.$setViewValue(value);
                                    }
                                }
                                else {
                                    console.log("single select", ngModel.$viewValue, attr);
                                    element.val(ngModel.$viewValue);
                                }
                                element.material_select('destroy')
                                element.material_select();
                            }
                        }
                        break;
                    }

                    case "color": {
                        ngModel.$render = function () {
                            if (ngModel.$viewValue) {
                                element.val(ngModel.$viewValue);
                                var $spectrum = $(element).spectrum({
                                    showInitial: true,
                                    showInput: true,
                                    preferredFormat: "hex",
                                    hide: function (color) {
                                        console.log("color", color.toHexString(), element, attr);
                                        setModelValue(color.toHexString());
                                    }
                                });
                                console.log("spectrum", $spectrum);
                            }
                        }
                        break;
                    }
                }
                var setModelValue = function (value) {
                    ngModel.$setViewValue(value);
                    window['MainService'] = MainService;
                }
                //if (scope.$last === true) {
                //    $timeout(function () {
                //        //console.log("ngRepeatFinished", scope, element, attr);
                //        scope.$emit('ngRepeatFinished', element);
                //    });
                //}
            }
        }
    });

    app.directive('userScene', function (MainService, $timeout, uiLanguage, INFO) {
        return {
            restrict: "E",
            templateUrl: 'tmpl/userScene.html',
            link: function (scope, element) {
            },
            controller: function ($scope, MainService, $timeout, uiLanguage, INFO) {
                // $scope.reportData = MainService.reportData;
                // reportData.NO_OF_TOTAL_AMOUNT;
                // $scope.reportData = {
                //     NO_OF_TOTAL_AMOUNT: "12345"
                // }
                // console.log(MainService.schema.modeOrder.billing);
                var ui = $scope.ui = uiLanguage[INFO.lang];
                $scope.staffTypeList = [
                    { code: 1, name: ui['manager'] },
                    { code: 2, name: ui['cashier'] },
                    { code: 3, name: ui['staff'] }
                ];
                $timeout(function () {

                    $('select').material_select();
                }, 100)
            }
            // controller: 'mainController'
            // controller: function($scope, MainService) {
            //     $scope.MainService = MainService;
            // }
        }
    });

    app.directive('categoryScene', function (MainService, $timeout, uiLanguage, INFO) {
        return {
            restrict: "E",
            templateUrl: 'tmpl/category-scene.html',
            link: function (scope, element) {
            },
            controller: function ($scope, MainService, $timeout, uiLanguage, INFO) {
                var ui = $scope.ui = uiLanguage[INFO.lang];
            }
        }
    });

    app.directive('menuLayoutBuilderScene', function (MainService, $timeout, uiLanguage, INFO) {
        return {
            restrict: "E",
            templateUrl: 'tmpl/menu-layout-builder-scene.html',
            link: function (scope, element) {
                scope.$watch('MainService.food', function (newValue, oldValue) {
                    $('body').data("food", newValue);
                });
            },
            controller: function ($scope, MainService, $timeout, uiLanguage, INFO, SocketFactory, $rootScope) {
                $scope.$watch('selectedPage', function (newValue, oldValue) {
                    if (newValue == oldValue) return;
                    $('body').data('selectedPage', newValue);
                });

                var ui = $scope.ui = uiLanguage[INFO.lang];
                $scope.menuLayoutList = [];
                $scope.menuLayout = {};
                $scope.selectedFlow = {};
                $scope.selectedPage = {};
                $scope.selectedLayout = {};
                $scope.selectedItem = null;
                $scope.selectedCategory = null;
                $scope.parentLayout = [];
                $scope.baseFlow = {
                }
                $scope.newTiemslot = {
                    hasNavBar: false,
                    label: "New Flow",
                    navigationBar: { layout: { buttons: [], logo: {} }, width: 100 },
                    pages: [{ layout: [] }],
                    time: "00:00-23:00",
                    timeRangeId: 0
                }
                $scope.newTileItem = {
                    x: 0,
                    y: 0,
                    width: 100,
                    height: 100
                }
                $scope.defaultLayout = {
                    layoutId: -1,
                    layoutName: "",
                    activateDate: new Date(),
                    backgroundColor: "#212121",
                    dayOfWeek: [],
                    expireDate: new Date(),
                    flow: [],
                    fontColor: "#cfd8dc",
                    orientation: "landscape",
                    priority: 50,
                    width: 1024,
                    height: 768,
                    resId: 1
                };

                $scope.transformZ = function (type) {
                    console.log($scope.selectedItem.index);
                    if (!$scope.selectedItem) return;
                    switch (type) {
                        case "forward": {
                            if ($scope.selectedItem.index + 1 == $scope.selectedLayout.length) return;
                            $scope.selectedLayout.move($scope.selectedItem.index, $scope.selectedItem.index + 1);
                            break;
                        }
                        case "front": {
                            $scope.selectedLayout.move($scope.selectedItem.index, $scope.selectedLayout.length - 1);
                            break;
                        }
                        case "backward": {
                            if ($scope.selectedItem.index - 1 < 0) return;
                            $scope.selectedLayout.move($scope.selectedItem.index, $scope.selectedItem.index - 1);
                            break;
                        }
                        case "back": {
                            $scope.selectedLayout.move($scope.selectedItem.index, 0);
                            break;
                        }
                    }
                    $scope.selectedLayout.forEach(function (l, i) {
                        $.extend(l, { "index": i });
                    });
                }

                $scope.addFlow = function () {
                    var newFlow = $.extend(true, {}, $scope.newTiemslot, $scope.baseFlow);
                    $scope.menuLayout.flow.push(newFlow);
                    $scope.editFlow(newFlow);

                    console.log("idx", ($scope.menuLayout.flow.length - 1));
                    $timeout(function () {
                        $('#tab-layout .collapsible').find('> li > .collapsible-header:eq(' + ($scope.menuLayout.flow.length - 1) + ')').trigger('click.collapse')
                    }, 400);
                }

                $scope.editFlow = function (flow) {
                    console.log("editFlow", flow);
                    $scope.selectedFlow = flow;
                    $('body').data('selectedFlow', $scope.selectedFlow);
                    if ($scope.selectedFlow.pages && $scope.selectedFlow.pages.length) {
                        $scope.selectedPage = $scope.selectedFlow.pages[0];
                        $scope.selectedLayout = $scope.selectedPage.layout;
                        $scope.selectedCategory = null;
                    }
                    else {
                        $scope.selectedPage = null;
                        $scope.selectedLayout = null;
                        $scope.selectedCategory = null;
                    }
                }

                $scope.copyFlow = function (flow) {
                    console.log("copyFlow", flow);
                    var clonedFlow = angular.copy(flow);
                    $scope.menuLayout.flow.push(clonedFlow);
                    $timeout(function () {
                        $scope.editFlow(clonedFlow);
                    })
                }

                $scope.removeFlow = function (idx) {
                    console.log("removeFlow", idx);
                    $scope.menuLayout.flow.splice(idx, 1);
                    $scope.editFlow($scope.menuLayout.flow[0]);
                }

                $scope.editPage = function (page) {
                    console.log("editPage", page);
                    $scope.selectedPage = page;
                    $scope.selectedLayout = $scope.selectedPage.layout;
                    $scope.selectedCategory = null;
                }

                $scope.copyPage = function (page) {
                    console.log("copyPage", page);
                    var clonedPage = angular.copy(page);
                    $scope.selectedFlow.pages.push(clonedPage);
                    $timeout(function () {
                        $scope.editPage(clonedPage);
                    })
                }

                $scope.removePage = function (idx) {
                    console.log("removePage", idx);
                    $scope.selectedFlow.pages.splice(idx, 1);
                }

                $scope.addTileItem = function () {
                    $scope.selectedLayout.push($.extend(true, {}, $scope.newTileItem));
                }

                $rootScope.copyTileItem = function (tileItem) {
                    console.log("copyItem", tileItem);
                    var clonedTileItem = angular.copy(tileItem);
                    clonedTileItem.x += 20;
                    clonedTileItem.y += 20;
                    $scope.selectedLayout.push(clonedTileItem);
                }

                $scope.addNavItem = function () {
                    if (!$scope.selectedFlow.navigationBar.layout) {
                        $scope.selectedFlow.navigationBar.layout = { "buttons": [] };
                    }
                    $scope.selectedFlow.navigationBar.layout.buttons.push($.extend(true, {}, $scope.newTileItem, {isNav: true}));
                }

                $rootScope.beginLayoutTransaction = function (data) {
                    console.log("beginLayoutTransaction", data);
                    if (data.link) {
                        if (data.isNav) $scope.parentLayout = [];
                        else $scope.parentLayout.push($scope.selectedLayout);

                        $scope.selectedPage = $scope.selectedFlow.pages.filter(function (x) { return x.tag === data.link })[0];                        
                        $scope.selectedLayout = $scope.selectedPage.layout;
                        $scope.selectedCategory = null;
                    }
                    else if (data.nextLayout) {
                        $scope.parentLayout.push($scope.selectedLayout);
                        $scope.selectedLayout = data.nextLayout.layout;
                        $scope.selectedCategory = null;
                    }
                    else if (data.category) {
                        if (data.isNav) $scope.parentLayout = [];
                        else $scope.parentLayout.push($scope.selectedLayout);

                        //$scope.parentLayout.push($scope.selectedLayout);
                        $scope.selectedLayout = [];
                        $scope.selectedCategory = data.category;
                    }
                }                

                $rootScope.selectItem = function (data) {
                    $scope.selectedItem = data;
                }

                $scope.back = function () {
                    console.log('back');
                    $scope.selectedCategory = null;
                    if ($scope.parentLayout.length) {
                        $scope.selectedLayout = $scope.parentLayout.pop();
                        //this._setOptions({ layout: this.options.parentLayout.pop(), category: null });
                    }
                },

                $scope.changeOrientation = function () {
                    console.log("changeOrientation")
                    var w = $scope.menuLayout.width,
                        h = $scope.menuLayout.height,
                        o = $scope.menuLayout.orientation;

                    if (o === "portrait" && w > h) {
                        $scope.menuLayout.height = w;
                        $scope.menuLayout.width = h;
                    }
                    else if (o === "landscape" && h > w) {
                        $scope.menuLayout.height = w;
                        $scope.menuLayout.width = h;
                    }
                }

                $scope.changeResolution = function () {
                    console.log("changeResolution");
                    var res = $scope.resolutionList.filter(function (x) { return x.resId == $scope.menuLayout.resId; })[0];
                    console.log($scope.resolutionList, res);
                    var w = res.width,
                        h = res.height,
                        o = $scope.menuLayout.orientation;
                    if (o === "portrait") {
                        $scope.menuLayout.height = w;
                        $scope.menuLayout.width = h;
                    }
                    else if (o === "landscape") {
                        $scope.menuLayout.height = h;
                        $scope.menuLayout.width = w;
                    }
                }

                $rootScope.$on('menuLayout.submit', function () {
                    console.log("menuLayout.submit");
                    if (MainService.mode === MainService.schema.mode.menuLayoutBuilder) {
                        console.log($scope.menuLayout.dayOfWeek)
                        $scope.menuLayout.dayOfWeek = $scope.menuLayout.dayOfWeek.join(",")
                        $scope.menuLayout.staffId = MainService.UserManager.staffCode;
                        console.log('saveMenuLayout', $scope.menuLayout)
                        //$scope.cleanUpMenuLayout($scope.menuLayout);
                        cleanupMenuLayout($scope.menuLayout);
                        SocketFactory.emit('saveMenuLayout', $scope.menuLayout, function () {
                            $scope.resetMenuLayout();
                        })
                    }
                });

                $rootScope.$on('menuLayout.abandon', function () {
                    console.log("menuLayout.abandon");
                    $rootScope.$apply(function () {
                        $scope.resetMenuLayout();
                    });
                });

                $rootScope.changeTopToolbarAction = function () {
                    $('#toolbar-top .save').bind('click.wo-menuLayout', function () {
                        $rootScope.$broadcast('menuLayout.submit');
                    });
                    $('#toolbar-top .abandon').bind('click.wo-menuLayout', function () {
                        $rootScope.$broadcast('menuLayout.abandon');
                    });
                }
                $rootScope.resetMenuLayout = function () {
                    $scope.menuLayout = {};
                    $scope.selectedFlow = null;
                    $scope.selectedPage = null;
                    $scope.selectedItem = null;
                    $('body').data('selectedFlow', $scope.selectedFlow);
                    $('body').data('selectedPage', $scope.selectedPage);
                    $scope.selectedLayout = null;
                    $scope.selectedCategory = null;
                    MainService.mode = MainService.schema.mode.menuLayoutList;
                    $('.md-toolbar-item .btn').unbind('.wo-menuLayout')
                    $scope.listMenuLayout();
                }
            }
        }
    });

    app.directive('menuLayoutListScene', function (MainService, $timeout, uiLanguage, INFO) {
        return {
            restrict: "E",
            templateUrl: 'tmpl/menu-layout-list-scene.html',
            link: function ($scope, element) {
                $scope.listMenuLayout();
            },
            controller: function ($scope, MainService, $timeout, uiLanguage, INFO, SocketFactory, $rootScope) {
                var ui = $scope.ui = uiLanguage[INFO.lang];

                $scope.addLayout = function () {
                    $scope.menuLayout = $.extend(true, {}, $scope.defaultLayout);
                    var newFlow = $.extend(true, {}, $scope.newTiemslot, $scope.baseFlow);
                    $scope.menuLayout.flow.push(newFlow);
                    $scope.editFlow($scope.menuLayout.flow[0]);
                    MainService.mode = MainService.schema.mode.menuLayoutBuilder;
                    $timeout(function () {
                        stickySidebar();
                        $('select').material_select('destroy');
                        $('select').material_select();
                        $rootScope.changeTopToolbarAction();
                    })
                    console.log("addLayout", $scope.menuLayout);
                }

                $scope.editLayout = function (id, isCopy) {
                    SocketFactory.emit("loadMenuLayout", { id: id }, function (result) {
                        console.log("loadMenuLayout", result);
                        var temp = result.data[0];
                        if (isCopy) temp.layoutId = -1;
                        temp.flow = JSON.parse(temp.flow);
                        temp.activateDate = new Date(temp.activateDate);
                        temp.expireDate = new Date(temp.expireDate);
                        //$scope.cleanUpMenuLayout(temp);
                        cleanupMenuLayout(temp);
                        if (!temp.flow) {
                            $.extend(true, temp, { flow: [] });
                        }
                        if (!temp.flow.length) {
                            var newFlow = $.extend(true, {}, $scope.newTiemslot);
                            temp.flow.push(newFlow);
                        }
                        temp.flow.forEach(function (flow) {
                            $.extend(true, flow, $scope.baseFlow);
                        });
                        $scope.menuLayout = temp;

                        $scope.editFlow($scope.menuLayout.flow[0]);
                        MainService.mode = MainService.schema.mode.menuLayoutBuilder;
                        $timeout(function () {
                            stickySidebar();
                            $rootScope.changeTopToolbarAction();
                        })
                        console.log("loadMenuLayout", $scope.menuLayout);
                    });
                }

                $rootScope.listMenuLayout = function () {
                    SocketFactory.emit('listMenuLayout', {}, function (_result) {
                        //var _result = JSON.parse(result);
                        console.log('listMenuLayout', _result);
                        $scope.menuLayoutList = _result.data[0];
                        $.each($scope.menuLayoutList, function (idx, menuLayout) {
                            var tempDay = menuLayout.dayOfWeek.split(',');
                            for (key in tempDay) {
                                tempDay[key] = getDayString(tempDay[key]);
                            }
                            menuLayout.dayOfWeek = tempDay.join(", ");
                        });

                        $scope.resolutionList = _result.data[1];
                        $scope.timeRangeList = _result.data[2];
                        _result.data[3].forEach(function (tmpl) {
                            try {
                                tmpl.template = JSON.parse(tmpl.template);
                            }
                            finally {

                            }
                        });
                        $scope.menuTileTemplateList = _result.data[3];
                        $scope.imageList = _result.data[_result.data.length - 1];
                        $('body').data("imageList", $scope.imageList);
                        $('body').data("templateList", $scope.menuTileTemplateList);
                    });
                }
                $scope.deploy = function () {
                    SocketFactory.emit('GenerateMenuLayout', function (result) {
                        console.log(result)
                    });
                }
            }
        }
    });

    app.directive('timeRangeScene', function (MainService, $timeout, uiLanguage, INFO, $mdDialog) {
        return {
            restrict: "E",
            templateUrl: 'tmpl/time-range-scene.html',
            link: function (scope, element) {
            },
            controller: function ($scope, MainService, $timeout, uiLanguage, INFO, SocketFactory, $filter) {
                var ui = $scope.ui = uiLanguage[INFO.lang];


                $scope.mode = "basic";
                console.log("MainService", MainService.DateDuring);


                $scope.initTimeslot = function () {
                    $scope.timeslot = { name: "", startTime: "00:00:00", endTime: "00:00:00", date: [], dateString: "" };
                }

                $scope.initTimeslot();
                $scope.AddTimeRangePage = function () {
                    $scope.mode = "add";
                    Materialize.updateTextFields();
                }

                $scope.showBasic = function () {
                    return $scope.mode === "basic";
                }

                $scope.showEdit = function () {
                    return $scope.mode === "add" || $scope.mode === "change";
                }

                $scope.BackItemListPage = function ($event) {
                    if ($event != undefined && $event != null)
                        $event.preventDefault();
                    $scope.initTimeslot();
                    $scope.mode = "basic";
                    MainService.DateDuring = "";
                }

                $scope.submitAdd = function ($event) {
                    $event.preventDefault();

                    $scope.removeErrorStyle();
                    var checking = true;
                    if ($scope.timeslot.name == undefined || $scope.timeslot.name == "") {
                        angular.element('#name').addClass('missing-data')
                        checking = false;
                    }

                    var regexp = new RegExp("^([0-9]{2})\:([0-9]{2})$");
                    var regexp2 = new RegExp("^([0-9]{2})\:([0-9]{2})\:([0-9]{2})$");
                    //var regexp = new RegExp("^([0-9]{4})$");
                    if (!regexp.test($scope.timeslot.startTime) && !regexp2.test($scope.timeslot.startTime)) {
                        angular.element('#startTime').addClass('missing-data')
                        checking = false;
                    }

                    var tempStartTime = $scope.timeslot.startTime.replace(new RegExp(':', 'g'), "");

                    if ($scope.timeslot.startTime.trim().length == 8) {
                        if (parseInt(tempStartTime) >= 240000) {
                            angular.element('#startTime').addClass('missing-data')
                            checking = false;
                        }
                    }
                    else if ($scope.timeslot.startTime.trim().length == 5) {
                        if (parseInt(tempStartTime) >= 2400) {
                            angular.element('#startTime').addClass('missing-data')
                            checking = false;
                        }
                    }





                    if (!regexp.test($scope.timeslot.endTime) && !regexp2.test($scope.timeslot.endTime)) {
                        angular.element('#endTime').addClass('missing-data')
                        checking = false;
                    }


                    var tempEndTime = $scope.timeslot.endTime.replace(new RegExp(':', 'g'), "");

                    if ($scope.timeslot.endTime.trim().length == 8) {
                        if (parseInt(tempEndTime) >= 240000) {
                            angular.element('#endTime').addClass('missing-data')
                            checking = false;
                        }
                    }
                    else if ($scope.timeslot.endTime.trim().length == 5) {
                        if (parseInt(tempEndTime) >= 2400) {
                            angular.element('#endTime').addClass('missing-data')
                            checking = false;
                        }
                    }

                    if (checking) {

                        console.log($scope.timeslot.endTime.trim().length)
                        $scope.SubmitScreen();
                    }


                }


                $scope.SubmitScreen = function () {
                    var confirm = $mdDialog.confirm()
                       .title(uiLanguage[INFO.lang]['alert'])
                       .content('確定要輸入?')
                       .ok(uiLanguage[INFO.lang]['yes'])
                       .cancel(uiLanguage[INFO.lang]['cancel']);

                    $mdDialog.show(confirm).then(function () {
                        $scope.timeslot.date = [];
                        if (angular.element('#date').val() != null) {

                            $scope.timeslot.date = angular.element('#date').val();
                            $scope.timeslot.dateString = "";
                            $scope.timeslot.date.forEach(function (date, index) {
                                $scope.timeslot.dateString += date;
                                if (index != $scope.timeslot.date.length - 1)
                                    $scope.timeslot.dateString += ",";

                            })



                        }




                        if ($scope.mode === 'add') {

                            //console.log(JSON.stringify($scope.timeslot));

                            SocketFactory.emit('InsertTimeRange', JSON.stringify($scope.timeslot), function (result) {
                                if (result != undefined && result != null) {

                                    if (result[0].date != null && result[0].date != "") {
                                        var tempDate = result[0].date.split(',');
                                        result[0].odate = result[0].date;
                                        result[0].date = "";
                                        result[0].date = $scope.DBvalueToString(tempDate);

                                    }

                                    result[0].rowStyle = "{'background-color':'yellow'}";
                                    console.log(result[0]);
                                    $scope.timeRangeList.splice(0, 0, result[0]);
                                    $scope.BackItemListPage();

                                }
                            });
                        }
                        else if ($scope.mode === 'change') {

                            SocketFactory.emit('UpdateTimeRange', JSON.stringify($scope.timeslot), function (result) {

                                if (result != undefined && result != null) {
                                    result[0].rowStyle = "{'background-color':'yellow'}";

                                    result[0].odate = result[0].date;
                                    if (result[0].date != null && result[0].date != "") {
                                        var tempDate = result[0].date.split(',');
                                        result[0].date = "";
                                        result[0].date = $scope.DBvalueToString(tempDate);
                                    }

                                    console.log("updaet result 0", result[0])
                                    $scope.timeRangeList[$scope.timeslot.rowCount] = result[0];
                                    $scope.BackItemListPage();
                                }
                            });

                        }
                    }, function () {
                    });
                }

                $scope.EditTimeRange = function ($event, timeRange) {
                    $event.preventDefault();
                    $scope.mode = "change";
                    $scope.timeslot.rowCount = $($event.currentTarget).closest('tr').attr('rowcount');
                    $scope.timeslot.name = timeRange.name;
                    $scope.timeslot.timeRangeId = timeRange.timeRangeId;
                    $scope.timeslot.startTime = $filter('date')(new Date(timeRange.timeStart), "HH:mm:ss");
                    $scope.timeslot.endTime = $filter('date')(new Date(timeRange.timeEnd), "HH:mm:ss");
                    $scope.timeslot.dateString = timeRange.odate;
                    MainService.DateDuring = timeRange.odate;
                    Materialize.updateTextFields();
                }


                $scope.removeErrorStyle = function () {
                    angular.element('.missing-data').removeClass('missing-data');
                }



                $scope.DBvalueToString = function (tempDate) {
                    var tempDateString = "";
                    var Timeslot = ["empty", "Sun", "Mon", "Tue", "Wed", "Thus", "Fri", "Sat"];
                    tempDate.forEach(function (date, index) {
                        if (Timeslot[date] !== undefined)
                            tempDateString += Timeslot[date]
                        else
                            tempDateString += date
                        if (index != tempDate.length - 1)
                            tempDateString += ","
                    })
                    return tempDateString;
                }


                SocketFactory.emit('listTimeRange', {}, function (result) {
                    var TimeList = JSON.parse(result);

                    $scope.timeRangeList = [];

                    TimeList.forEach(function (item) {
                        item.odate = item.date;
                        if (item.date != null && item.date != "") {
                            var tempDate = item.date.split(',');
                            item.date = "";
                            item.date = $scope.DBvalueToString(tempDate);
                        }

                        $scope.timeRangeList.push(item);
                    })
                });
            }
        }
    });



    app.directive('modifierScene', function (MainService, $timeout, uiLanguage, INFO) {
        return {
            restrict: "E",
            templateUrl: '../tmpl/modifier.html',
            link: function (scope, element) {
                //element.find('#modifier-tabs').tabs();


                $('ul.tabs').each(function () {
                    var $active, $content, $links = $(this).find('a');
                    $(this).children('li:eq(0)').css('border-bottom', '1px solid #ee6e73');
                    $active = $($links.filter('[href="' + location.hash + '"]')[0] || $links[0]);
                    $active.addClass('active');

                    $content = $($active[0].hash);
                    $links.not($active).each(function () {
                        $(this.hash).hide();
                    });

                    $(this).on('click', 'a', function (e) {
                        var mainThis = $(this);
                        var mainContent = $(this.hash);
                        e.preventDefault();

                        $(this).closest('ul').children('li').find('a').removeClass('active');
                        $(this).closest('ul').children('li').find('a').each(function () {
                            $(this).closest('li').css('border-bottom-width', '0px');
                            $(this.hash).hide();
                        })
                        $(this).closest('li').css('border-bottom', '1px solid #ee6e73');
                        $(this).addClass('active');
                        $(this.hash).show();



                    });


                });

                Materialize.updateTextFields();
            },
            controller: function ($scope, MainService, $timeout, uiLanguage, INFO, SocketFactory, $mdDialog) {
                var ui = $scope.ui = uiLanguage[INFO.lang];


                $scope.InitObject = function () {
                    $scope.ModifierItem = { modifierCode: "MD", name1: null, name2: null, name3: null, namek: null, goMenu: "0", CurrentModifierItemCode: null };
                }

                $scope.InitObject();
                $scope.ItemManager = new ItemManager();
                $scope.GroupManager = new GroupManager();
                $scope.AddModifierGroupItemPage = false;
                $scope.EditGroupPage = false;

                function ItemManager() {
                    this.Mode = 'basic';

                    SocketFactory.emit('GetAllModifierItem', null, function (result) {
                        $scope.modifierItemList = [];
                        $scope.modifierItemList = JSON.parse(result);

                    });

                    ItemManager.prototype.BackBasicList = function () {
                        this.Mode = 'basic';
                        $scope.removeErrorStyle();
                        $scope.InitObject();

                    }

                    ItemManager.prototype.EditBtnClick = function (item, index) {
                        this.Mode = 'change';
                        $scope.ModifierItem = item;
                        $scope.ModifierItem.index = index;
                        $scope.ModifierItem.goMenu = item.goMenu == "1" ? "1" : "0";

                    }

                    ItemManager.prototype.EditSubmit = function () {

                        var checking = true;
                        $scope.removeErrorStyle();

                        $scope.removeErrorStyle();
                        var ModifierCodeCheck = /[M][D][a-zA-Z0-9]{4}$/;

                        if (!ModifierCodeCheck.test($scope.ModifierItem.modifierCode)) {
                            angular.element('#code').addClass('missing-data');
                            checking = false;
                        }

                        if ($scope.ModifierItem.name1 == null || $scope.ModifierItem.name1 == "") {
                            angular.element('#name1').addClass('missing-data')
                            checking = false;
                        }
                        if ($scope.ModifierItem.namek == null || $scope.ModifierItem.namek == "") {
                            angular.element('#namek').addClass('missing-data')
                            checking = false;
                        }

                        if (checking) {
                            var GlobalMode = this.Mode;
                            var confirm = $mdDialog.confirm()
                           .title(uiLanguage[INFO.lang]['alert'])
                           .content('確定要輸入?')
                           .ok(uiLanguage[INFO.lang]['yes'])
                           .cancel(uiLanguage[INFO.lang]['cancel']);

                            $mdDialog.show(confirm).then(function () {


                                if (GlobalMode == 'add') {

                                    SocketFactory.emit('GetAllModifierItem', $scope.ModifierItem.modifierCode, function (result) {
                                        result = JSON.parse(result);
                                        if (result.length != 0) {
                                            $scope.itemIsExist();

                                        }
                                        else {

                                            SocketFactory.emit('InsertModifierItem', JSON.stringify($scope.ModifierItem), function (result) {
                                                if (result == "OK") {
                                                    $scope.SuccessOperation();
                                                    $scope.modifierItemList.push($scope.ModifierItem);
                                                    $scope.InitObject();
                                                }

                                            })

                                        }

                                    });


                                } else {

                                    SocketFactory.emit('UpdateModifierItem', JSON.stringify($scope.ModifierItem), function (result) {
                                        if (result == "OK") {
                                            $scope.SuccessOperation();
                                            $scope.modifierItemList[$scope.ModifierItem.index] = $scope.ModifierItem;
                                            $scope.InitObject();

                                        }

                                    })

                                }

                            }, function () {
                            });
                        }


                    }
                }

                function GroupManager() {
                    this.Mode = 'basic';
                    this.CurrentGroupItem = null;

                    $scope.ModifierGroup = { "Code": 'MDG', "Name": null, "goMenu": "0", "currentGroupCode": null };



                    SocketFactory.emit('GetAllModifierGroup', null, function (result) {
                        $scope.modifierGroupList = [];
                        $scope.modifierGroupList = JSON.parse(result);
                        console.log($scope.modifierGroupList);
                    });

                    GroupManager.prototype.AddItemToModifierGroup = function (itemId) {

                        SocketFactory.emit('InsertItemToModifierGroup', { "GroupId": $scope.ModifierGroup.currentGroupCode, "ItemCode": itemId }, function (result) {

                            if (result == "OK") {
                                SocketFactory.emit('GetAllModifierItemByGroup', $scope.ModifierGroup.currentGroupCode, function (result) {
                                    $scope.modifierItemListByGroup = [];
                                    $scope.modifierItemListByGroup = JSON.parse(result)[0];
                                    $scope.modifierItemListNotInGroup = [];
                                    $scope.modifierItemListNotInGroup = JSON.parse(result)[1];
                                });
                            }
                        });
                    }

                    GroupManager.prototype.RemoveItemInModifierGroup = function (itemId) {

                        SocketFactory.emit('RemoveItemInModifierGroup', { "GroupId": $scope.ModifierGroup.currentGroupCode, "ItemCode": itemId }, function (result) {

                            if (result == "OK") {
                                SocketFactory.emit('GetAllModifierItemByGroup', $scope.ModifierGroup.currentGroupCode, function (result) {
                                    $scope.modifierItemListByGroup = [];
                                    $scope.modifierItemListByGroup = JSON.parse(result)[0];
                                    $scope.modifierItemListNotInGroup = [];
                                    $scope.modifierItemListNotInGroup = JSON.parse(result)[1];
                                });
                            }
                        });


                    }

                    GroupManager.prototype.submitAddGroup = function () {
                        var checking = true;
                        $scope.removeErrorStyle();
                        var GroupCodeCheck = /[M][D][G][a-zA-Z0-9]{4}$/;

                        if (!GroupCodeCheck.test($scope.ModifierGroup.Code)) {
                            angular.element('#group-code').addClass('missing-data');
                            checking = false;
                        }

                        if ($scope.ModifierGroup.Name == null || $scope.ModifierGroup.Name == "") {
                            angular.element('#group-name').addClass('missing-data')
                            checking = false;
                        }

                        if (checking) {
                            var GlobalMode = this.Mode;

                            var confirm = $mdDialog.confirm()
                            .title(uiLanguage[INFO.lang]['alert'])
                            .content('確定要輸入?')
                            .ok(uiLanguage[INFO.lang]['yes'])
                            .cancel(uiLanguage[INFO.lang]['cancel']);

                            $mdDialog.show(confirm).then(function () {
                                if (GlobalMode == "add") {
                                    SocketFactory.emit('GetAllModifierGroup', $scope.ModifierGroup.Code, function (result) {
                                        result = JSON.parse(result);
                                        if (result.length != 0) {
                                            $scope.itemIsExist();
                                        }
                                        else {
                                            SocketFactory.emit('InsertModifierGroup', JSON.stringify($scope.ModifierGroup), function (result) {
                                                if (result == "OK") {
                                                    $scope.SuccessOperation();
                                                    $scope.modifierGroupList.push({ modifierGroupCode: $scope.ModifierGroup.Code, modifierGroupName: $scope.ModifierGroup.Name, goMenu: $scope.ModifierGroup.goMenu });
                                                    $scope.ModifierGroup = { "Code": 'MDG', "Name": null, "goMenu": "0", "currentGroupCode": null };
                                                }

                                            })

                                        }

                                    });
                                }
                                else {
                                    SocketFactory.emit('UpdateModifierGroup', JSON.stringify($scope.ModifierGroup), function (result) {
                                        if (result == "OK") {
                                            $scope.modifierGroupList[$scope.ModifierGroup.index].modifierCode = $scope.ModifierGroup.Code;
                                            $scope.modifierGroupList[$scope.ModifierGroup.index].modifierGroupName = $scope.ModifierGroup.Name;
                                            $scope.modifierGroupList[$scope.ModifierGroup.index].goMenu = $scope.ModifierGroup.goMenu;
                                            $scope.ModifierGroup = { "Code": 'MDG', "Name": null, "goMenu": "0", "currentGroupCode": null };
                                        }

                                    });
                                }
                            }, function () {
                            });
                        }
                    }

                    GroupManager.prototype.GroupInsertClick = function () {
                        $scope.ModifierGroup = { "Code": 'MDG', "Name": null, "goMenu": "0", "currentGroupCode": null };
                        this.Mode = 'add';
                    }

                    GroupManager.prototype.BackBasicList = function () {
                        this.Mode = 'basic';
                        $scope.removeErrorStyle();
                        angular.element('.group-list-table').closest('table').find('tbody tr').css('background-color', 'white');
                        $scope.modifierItemListByGroup = [];
                        $scope.modifierItemListNotInGroup = [];

                    }

                    GroupManager.prototype.GroupListRowClick = function (item, $event, index) {
                        if (this.Mode == 'add') this.Mode = 'basic';
                        angular.element($event.currentTarget).closest('table').find('tbody tr').css('background-color', 'white');
                        angular.element($event.currentTarget).css('background-color', 'yellow');

                        $scope.ModifierGroup.currentGroupCode = item.modifierGroupCode;
                        $scope.ModifierGroup.Code = item.modifierGroupCode;
                        $scope.ModifierGroup.Name = item.modifierGroupName;
                        $scope.ModifierGroup.goMenu = item.goMenu == "1" ? "1" : "0";
                        $scope.ModifierGroup.index = index;

                        console.log('GetAllModifierItemByGroup');
                        SocketFactory.emit('GetAllModifierItemByGroup', $scope.ModifierGroup.currentGroupCode, function (result) {
                            console.log('GetAllModifierItemByGroup', result);
                            $scope.modifierItemListByGroup = [];
                            $scope.modifierItemListByGroup = JSON.parse(result)[0];
                            $scope.modifierItemListNotInGroup = [];
                            $scope.modifierItemListNotInGroup = JSON.parse(result)[1];
                        });
                    }

                    GroupManager.prototype.GroupUpdateClick = function (item, index) {
                        this.Mode = 'change';
                        $scope.ModifierGroup = item;
                        $scope.ModifierGroup.index = index;
                    }
                }



                $scope.itemIsExist = function () {

                    $mdDialog.show(
                       $mdDialog.alert()
                         .parent(angular.element(document.body))
                         .title(uiLanguage[INFO.lang]['alert'])
                          .content("Code已存在!")
                         .ok(uiLanguage[INFO.lang]['yes'])
                     );
                }

                $scope.emptySelectItem = function () {

                    $mdDialog.show(
                       $mdDialog.alert()
                         .parent(angular.element(document.body))
                         .title(uiLanguage[INFO.lang]['alert'])
                          .content("請選擇Item!")
                         .ok(uiLanguage[INFO.lang]['yes'])
                     );
                }

                $scope.SuccessOperation = function () {
                    $mdDialog.show(
                        $mdDialog.alert()
                          .parent(angular.element(document.body))
                          .title(uiLanguage[INFO.lang]['alert'])
                           .content("完成!")
                          .ok(uiLanguage[INFO.lang]['yes'])
                      );
                }

                $scope.removeErrorStyle = function () {
                    angular.element('.missing-data').removeClass('missing-data');
                }


            }
        }
    });

    app.directive('optionScene', function (MainService, $timeout, uiLanguage, INFO) {
        return {
            restrict: "E",
            templateUrl: '../tmpl/option.html',
            link: function (scope, element) {
                //element.find('ul li a').removeClass('active');
                //element.find('ul li a[herf=#item-info]').addClass('active');
                //element.find('#option-tabs').tabs();

                $('ul.tabs.basic-tabs').each(function () {
                    var $active, $content, $links = $(this).find('a');
                    $(this).children('li:eq(0)').css('border-bottom', '1px solid #ee6e73');
                    $active = $($links.filter('[href="' + location.hash + '"]')[0] || $links[0]);
                    $active.addClass('active');

                    $content = $($active[0].hash);
                    $links.not($active).each(function () {
                        $(this.hash).hide();
                    });

                    $(this).on('click', 'a', function (e) {
                        var mainThis = $(this);
                        var mainContent = $(this.hash);
                        e.preventDefault();

                        $(this).closest('ul').children('li').find('a').removeClass('active');
                        $(this).closest('ul').children('li').find('a').each(function () {
                            $(this).closest('li').css('border-bottom-width', '0px');
                            $(this.hash).hide();
                        })
                        $(this).closest('li').css('border-bottom', '1px solid #ee6e73');
                        $(this).addClass('active');
                        $(this.hash).show();



                    });


                });
                Materialize.updateTextFields();


            },
            controller: function ($scope, MainService, $timeout, uiLanguage, INFO, SocketFactory, $mdDialog) {

                var ui = $scope.ui = uiLanguage[INFO.lang];
                $scope.ItemManager = new ItemManager();
                $scope.GroupManager = new GroupManager();


                $scope.tabsOperation = function () {

                    $('ul.tabs.Edit-tabs').each(function () {
                        var $active, $content, $links = $(this).find('a');
                        $(this).children('li:eq(0)').css('border-bottom', '1px solid #ee6e73');
                        $active = $($links.filter('[href="' + location.hash + '"]')[0] || $links[0]);
                        $active.addClass('active');

                        $content = $($active[0].hash);
                        $links.not($active).each(function () {
                            $(this.hash).hide();
                        });

                        $(this).on('click', 'a', function (e) {
                            var mainThis = $(this);
                            var mainContent = $(this.hash);
                            e.preventDefault();

                            if ($(this).attr('href') != '#option-info') {
                                $scope.removeErrorStyle();
                                if (!$scope.ItemManager.ItemInfoChecking()) return;
                                else {
                                    if ($scope.ItemManager.Mode == 'add') {
                                        SocketFactory.emit('GetAllOptionItem', $scope.ItemObject.optionCode, function (result) {
                                            result = JSON.parse(result);
                                            if (result.length != 0) {
                                                $scope.itemIsExist();
                                            }
                                            else {

                                                $(mainThis).closest('ul').children('li').find('a').removeClass('active');
                                                $(mainThis).closest('ul').children('li').find('a').each(function () {
                                                    $(this).closest('li').css('border-bottom-width', '0px');
                                                    $(this.hash).hide();
                                                })
                                                $(mainThis).closest('li').css('border-bottom', '1px solid #ee6e73');
                                                $(mainThis).addClass('active');
                                                $(mainContent).show();



                                                //console.log($scope.FoodItem);
                                                //scope.SubmitScreen();
                                            }
                                        })
                                    } else {

                                        $(mainThis).closest('ul').children('li').find('a').removeClass('active');
                                        $(mainThis).closest('ul').children('li').find('a').each(function () {
                                            $(this).closest('li').css('border-bottom-width', '0px');
                                            $(this.hash).hide();
                                        })
                                        $(mainThis).closest('li').css('border-bottom', '1px solid #ee6e73');
                                        $(mainThis).addClass('active');
                                        $(mainContent).show();

                                    }
                                }
                            }
                            else {

                                $(this).closest('ul').children('li').find('a').removeClass('active');
                                $(this).closest('ul').children('li').find('a').each(function () {
                                    $(this).closest('li').css('border-bottom-width', '0px');
                                    $(this.hash).hide();
                                })
                                $(this).closest('li').css('border-bottom', '1px solid #ee6e73');
                                $(this).addClass('active');
                                $(this.hash).show();
                            }


                        });


                    });

                    $('#back-basic').on('click', function () {
                        $scope.tabsBackFirstItem();
                    })

                }
                $scope.tabsBackFirstItem = function () {
                    var $active, $content, $links = $('ul.tabs.Edit-tabs').find('a');
                    $active = $($links.filter('[href="' + location.hash + '"]')[0] || $links[0]);

                    $links.not($active).each(function () {
                        $(this).closest('li').css('border-bottom-width', '0px');
                        $(this).removeClass('active');
                        $(this.hash).hide();
                    });

                    $($active[0]).closest('li').css('border-bottom', '1px solid #ee6e73');
                    $($active[0].hash).show();
                }
                $scope.tabsOperation();





                function ItemManager() {

                    this.Mode = 'basic';
                    this.colors = [
                   { val: "rms-palette-orange", name: "Orange" },
                   { val: "rms-palette-gold", name: "Gold" },
                   { val: "rms-palette-grass", name: "Grass" },
                   { val: "rms-palette-sky", name: "Sky" },
                   { val: "rms-palette-lemon", name: "Lemon" },
                   { val: "rms-palette-jade", name: "Jade" },
                   { val: "rms-palette-navy", name: "Navy" }
                    ]
                    ItemManager.prototype.modifierList = [];
                    ItemManager.prototype.selectedModifierList = [];
                    ItemManager.prototype.AllModifierInGroupList = [];

                    $scope.ItemObject = { "index": null, "optionCode": "OP", "name1": null, "name2": null, "name3": null, "namek": null, "printer": null, "printerGroup": null, "kds": null, "goMenu": "0", "individualPrint": "0", "isDefault": "0", "color":"" };

                    ItemManager.prototype.GetItemList = function () {
                        SocketFactory.emit('GetAllOptionItem', null, function (result) {
                            $scope.ItemList = JSON.parse(result);
                            console.log(JSON.parse(result));
                            //$('#optionItemList').floatThead(); 
                            //$("#optionItemList").fixMe();
                        });
                    }



                    this.GetItemList();

                    ItemManager.prototype.BackBasicList = function () {
                        this.Mode = 'basic';
                        $scope.removeErrorStyle();
                        $scope.ItemObject = { "index": null, "optionCode": "OP", "name1": null, "name2": null, "name3": null, "namek": null, "printer": null, "printerGroup": null, "kds": null, "goMenu": "0", "individualPrint": "0", "isDefault": "0", "color": "" };

                        ItemManager.prototype.modifierGroupList = [];
                        ItemManager.prototype.selectedModifierList = [];
                        ItemManager.prototype.AllModifierInGroupList = [];
                        $scope.tabsBackFirstItem();
                        this.GetAllModifierGroup();
                    }

                    ItemManager.prototype.EditBtnClick = function (item, index) {
                        this.Mode = 'change';
                        $scope.ItemObject = item;
                        $scope.ItemObject.index = index;
                        $scope.ItemObject.goMenu = item.goMenu == "1" ? "1" : "0";
                        $scope.ItemObject.individualPrint = item.individualPrint == "1" ? "1" : "0";
                        $scope.ItemObject.isDefault = item.isDefault == "1" ? "1" : "0";
                        this.GetAllModifierGroup();
                        this.GetAllModifierGroupByItemCode(item.optionCode);
                    }

                    ItemManager.prototype.ItemInfoChecking = function () {
                        var checking = true;
                        $scope.removeErrorStyle();
                        var optionCodeCheck = /[O][P][a-zA-Z0-9]{5}$/;
                        //if ($scope.ItemObject.optionCode.charAt(0) != "O" || $scope.ItemObject.optionCode.charAt(1) != "P" || $scope.ItemObject.optionCode.trim().length != 7) {
                        //    angular.element('#code').addClass('missing-data');
                        //    checking = false;
                        //}

                        if (!optionCodeCheck.test($scope.ItemObject.optionCode)) {
                            angular.element('#code').addClass('missing-data');
                            checking = false;
                        }

                        if ($scope.ItemObject.name1 == null || $scope.ItemObject.name1 == "") {
                            angular.element('#name1').addClass('missing-data');
                            checking = false;
                        }

                        if ($scope.ItemObject.name2 == null || $scope.ItemObject.name2 == "") {
                            angular.element('#name2').addClass('missing-data');
                            checking = false;
                        }

                        //if ($scope.ItemObject.name3 == null || $scope.ItemObject.name3 == "") {
                        //    angular.element('#name3').addClass('missing-data');
                        //    checking = false;
                        //}

                        if ($scope.ItemObject.namek == null || $scope.ItemObject.namek == "") {
                            angular.element('#namek').addClass('missing-data');
                            checking = false;
                        }

                        return checking;
                    }

                    ItemManager.prototype.EditSubmit = function () {


                        if (this.ItemInfoChecking()) {
                            var GlobalMode = this.Mode;
                            var confirm = $mdDialog.confirm()
                            .title(uiLanguage[INFO.lang]['alert'])
                            .content('確定要輸入?')
                            .ok(uiLanguage[INFO.lang]['yes'])
                            .cancel(uiLanguage[INFO.lang]['cancel']);

                            $mdDialog.show(confirm).then(function () {
                                if (GlobalMode == 'add') {
                                    SocketFactory.emit('GetAllOptionItem', $scope.ItemObject.optionCode, function (result) {
                                        result = JSON.parse(result);
                                        if (result.length != 0) {
                                            $scope.itemIsExist();
                                        }
                                        else {
                                            var InsertArgs = { "option": $scope.ItemObject, "modifier": $scope.ItemManager.selectedModifierList };
                                            SocketFactory.emit('InsertOptionItem', JSON.stringify(InsertArgs), function (result) {
                                                if (result == "OK") {
                                                    $scope.SuccessOperation();
                                                    $scope.ItemList.push($scope.ItemObject);
                                                    $scope.ItemManager.BackBasicList();

                                                }
                                            });
                                        }
                                    });
                                } else if (GlobalMode == 'change') {
                                    var InsertArgs = { "option": $scope.ItemObject, "modifier": $scope.ItemManager.selectedModifierList };
                                    SocketFactory.emit('UpdateOptionItem', JSON.stringify(InsertArgs), function (result) {
                                        if (result == "OK") {
                                            $scope.SuccessOperation();
                                            $scope.ItemList[$scope.ItemObject.index] = $scope.ItemObject;
                                            $scope.ItemManager.BackBasicList();
                                        }
                                    });

                                }



                            }, function () {
                            });
                        }

                    }

                    ItemManager.prototype.InsertBtnClick = function () {
                        this.Mode = 'add';
                        this.GetAllModifierGroup();
                    }

                    ItemManager.prototype.GetAllModifierGroup = function () {
                        SocketFactory.emit('GetAllModifierGroup', null, function (result) {
                            ItemManager.prototype.modifierList = [];
                            ItemManager.prototype.modifierList = JSON.parse(result);

                        });
                    }

                    ItemManager.prototype.AddModifierGroup = function (item, index) {
                        ItemManager.prototype.selectedModifierList.push(item);
                        ItemManager.prototype.modifierList.splice(index, 1);
                    }

                    ItemManager.prototype.RemoveModifierGroup = function (item, index) {
                        ItemManager.prototype.selectedModifierList.splice(index, 1);
                        ItemManager.prototype.modifierList.push(item);
                    }

                    ItemManager.prototype.GetAllModifierInGroup = function (GroupCode) {
                        SocketFactory.emit('GetAllModifierItemByGroup', GroupCode, function (result) {
                            ItemManager.prototype.AllModifierInGroupList = [];
                            ItemManager.prototype.AllModifierInGroupList = JSON.parse(result)[0];
                            $('#modifier-in-group').openModal();
                        });

                    }

                    ItemManager.prototype.GetAllModifierGroupByItemCode = function (itemCode) {
                        SocketFactory.emit('GetAllModifierByItemCode', itemCode, function (result) {
                            ItemManager.prototype.selectedModifierList = [];
                            ItemManager.prototype.selectedModifierList = JSON.parse(result);

                            ItemManager.prototype.selectedModifierList.forEach(function (selected) {
                                ItemManager.prototype.modifierList = ItemManager.prototype.modifierList.filter(function (Md) {
                                    return Md.modifierGroupCode != selected.modifierGroupCode;
                                })
                            })

                        });

                    }

                    ItemManager.prototype.CloseAllModifierInGroup = function () {
                        $('#modifier-in-group').closeModal();
                    }

                }

                function GroupManager() {
                    this.Mode = 'basic';
                    this.OptionMode = 'basic';
                    this.CurrentGroupItem = null;
                    $scope.GroupObject = { index: null, optionGroupCode: "OP", name1: null, name2: null, name3: null, goMenu: "0" };

                    GroupManager.prototype.GetAllOptionGroup = function () {
                        SocketFactory.emit('GetAllOptionGroup', null, function (result) {
                            $scope.GroupList = JSON.parse(result);
                        });
                    }

                    this.GetAllOptionGroup();

                    GroupManager.prototype.GroupListRowClick = function (item, $event, index) {

                        if (this.Mode == 'add') this.Mode = 'basic';

                        angular.element($event.currentTarget).closest('table').find('tbody tr').css('background-color', 'white');
                        angular.element($event.currentTarget).css('background-color', 'yellow');

                        $scope.GroupObject.optionGroupCode = item.optionGroupCode;
                        $scope.GroupObject.name1 = item.name1;
                        $scope.GroupObject.name2 = item.name2;
                        $scope.GroupObject.name3 = item.name3;
                        $scope.GroupObject.goMenu = item.goMenu == 1 ? "1" : "0";
                        $scope.GroupObject.index = index;

                        SocketFactory.emit('GetAllOptionItemByGroup', $scope.GroupObject.optionGroupCode, function (result) {
                            $scope.OptionItemListByGroup = [];
                            $scope.OptionItemListByGroup = JSON.parse(result)[0];

                            $scope.OptionItemListNotInGroup = [];
                            $scope.OptionItemListNotInGroup = JSON.parse(result)[1];
                            console.log($scope.OptionItemListByGroup);
                        });

                    }

                    GroupManager.prototype.EditSubmit = function () {
                        var checking = true;
                        $scope.removeErrorStyle();
                        var GroupCodeCheck = /[O][P][a-zA-Z0-9]{2}$/;

                        if (!GroupCodeCheck.test($scope.GroupObject.optionGroupCode)) {
                            angular.element('#group-code').addClass('missing-data');
                            checking = false;
                        }

                        if ($scope.GroupObject.name1 == null || $scope.GroupObject.name1 == "") {
                            angular.element('#group-name1').addClass('missing-data');
                            checking = false;
                        }

                        if (checking) {
                            var GlobalMode = this.Mode;
                            var confirm = $mdDialog.confirm()
                            .title(uiLanguage[INFO.lang]['alert'])
                            .content('確定要輸入?')
                            .ok(uiLanguage[INFO.lang]['yes'])
                            .cancel(uiLanguage[INFO.lang]['cancel']);

                            $mdDialog.show(confirm).then(function () {
                                if (GlobalMode == 'add') {
                                    SocketFactory.emit('GetAllOptionGroup', $scope.GroupObject.optionGroupCode, function (result) {
                                        result = JSON.parse(result);
                                        if (result.length != 0) {
                                            $scope.itemIsExist();
                                        }
                                        else {

                                            SocketFactory.emit('InsertOptionGroup', JSON.stringify($scope.GroupObject), function (result) {
                                                if (result == "OK") {
                                                    $scope.SuccessOperation();
                                                    $scope.GroupList.push($scope.GroupObject);
                                                    $scope.GroupObject = { index: null, optionGroupCode: "OP", name1: null, name2: null, name3: null, goMenu: "0" };

                                                }
                                            });

                                        }
                                    });
                                }
                                else if (GlobalMode == 'change') {
                                    SocketFactory.emit('UpdateOptionGroup', JSON.stringify($scope.GroupObject), function (result) {
                                        if (result == "OK") {
                                            $scope.SuccessOperation();
                                            $scope.GroupList[$scope.GroupObject.index] = $scope.GroupObject;
                                            $scope.GroupObject = { index: null, optionGroupCode: "OP", name1: null, name2: null, name3: null, goMenu: "0" };
                                        }
                                    });

                                }



                            }, function () {
                            });

                        }

                    }

                    GroupManager.prototype.BackBasicList = function () {
                        this.Mode = 'basic';
                        $scope.removeErrorStyle();
                        angular.element('.group-list-table').closest('table').find('tbody tr').css('background-color', 'white');
                        $scope.OptionItemListByGroup = [];
                        $scope.OptionItemListNotInGroup = [];
                        //$scope.GroupObject = { index: null, optionGroupCode: "OP", name1: null, name2: null, name3: null, goMenu: "0" };
                        //$scope.GroupObject = this.CurrentGroupItem;
                    }


                    GroupManager.prototype.GroupInsertClick = function ($event) {
                        $scope.GroupObject = { index: null, optionGroupCode: "OP", name1: null, name2: null, name3: null, goMenu: "0" };
                        this.Mode = 'add';
                    }

                    GroupManager.prototype.GroupUpdateClick = function (item, index) {
                        this.Mode = 'change';
                        $scope.GroupObject.optionGroupCode = item.optionGroupCode;
                        $scope.GroupObject.name1 = item.name1;
                        $scope.GroupObject.name2 = item.name2;
                        $scope.GroupObject.name3 = item.name3;
                        $scope.GroupObject.index = index;
                        $scope.GroupObject.goMenu = item.goMenu ? "1" : "0";

                    }

                    GroupManager.prototype.addOptionToGroup = function (optionCode) {
                        SocketFactory.emit('AddOptionInOptionGroup', JSON.stringify({ groupCode: $scope.GroupObject.optionGroupCode, optionCode: optionCode }), function (result) {
                            if (result == "OK") {
                                SocketFactory.emit('GetAllOptionItemByGroup', $scope.GroupObject.optionGroupCode, function (result) {
                                    $scope.OptionItemListByGroup = [];
                                    $scope.OptionItemListByGroup = JSON.parse(result)[0];
                                    console.log($scope.OptionItemListByGroup);
                                    $scope.OptionItemListNotInGroup = [];
                                    $scope.OptionItemListNotInGroup = JSON.parse(result)[1];

                                });
                            }
                        });
                    }

                    GroupManager.prototype.RemoveOptionFromGroup = function (optionCode) {
                        SocketFactory.emit('RemoveOptionFromOptionGroup', JSON.stringify({ groupCode: $scope.GroupObject.optionGroupCode, optionCode: optionCode }), function (result) {
                            if (result == "OK") {
                                SocketFactory.emit('GetAllOptionItemByGroup', $scope.GroupObject.optionGroupCode, function (result) {
                                    $scope.OptionItemListByGroup = [];
                                    $scope.OptionItemListByGroup = JSON.parse(result)[0];
                                    console.log($scope.OptionItemListByGroup);
                                    $scope.OptionItemListNotInGroup = [];
                                    $scope.OptionItemListNotInGroup = JSON.parse(result)[1];

                                });
                            }
                        });

                    }


                    GroupManager.prototype.UpdateSeq = function ($event, code, oseq) {
                        if (oseq != $event.currentTarget.value) {
                            console.log(oseq, $event.currentTarget.value);
                            SocketFactory.emit('UpdateOptionItemSeqInGroup', JSON.stringify({ groupCode: $scope.GroupObject.optionGroupCode, optionCode: code, seq: $event.currentTarget.value }), function (result) {
                                $scope.OptionItemListByGroup = JSON.parse(result);
                            });
                        }

                    }


                }

                $scope.itemIsExist = function () {
                    $mdDialog.show(
                       $mdDialog.alert()
                         .parent(angular.element(document.body))
                         .title(uiLanguage[INFO.lang]['alert'])
                          .content("Code已存在!")
                         .ok(uiLanguage[INFO.lang]['yes'])
                     );
                }

                $scope.SuccessOperation = function () {
                    $mdDialog.show(
                        $mdDialog.alert()
                          .parent(angular.element(document.body))
                          .title(uiLanguage[INFO.lang]['alert'])
                           .content("完成!")
                          .ok(uiLanguage[INFO.lang]['yes'])
                      );
                }


                $scope.removeErrorStyle = function () {
                    angular.element('.missing-data').removeClass('missing-data');
                }
            }
        }
    });

    app.directive('itemScene', function (MainService, $timeout, uiLanguage, INFO) {
        return {
            restrict: "E",
            templateUrl: '../tmpl/item.html',
            link: function (scope, element, SocketFactory) {
                Materialize.updateTextFields();
            },
            controller: function ($scope, MainService, $timeout, uiLanguage, INFO, SocketFactory, $mdDialog, $http) {

                var ui = $scope.ui = uiLanguage[INFO.lang];
                //
                $scope.tabsOperation = function () {

                    $('ul.tabs').each(function () {
                        var $active, $content, $links = $(this).find('a');
                        $(this).children('li:eq(0)').css('border-bottom', '1px solid #ee6e73');
                        $active = $($links.filter('[href="' + location.hash + '"]')[0] || $links[0]);
                        $active.addClass('active');

                        $content = $($active[0].hash);
                        $links.not($active).each(function () {
                            $(this.hash).hide();
                        });

                        $(this).on('click', 'a', function (e) {
                            var mainThis = $(this);
                            var mainContent = $(this.hash);
                            e.preventDefault();

                            if ($(this).attr('href') != '#item-info') {
                                $scope.removeErrorStyle();
                                if (!$scope.ItemManager.ItemInfoChecking()) return;
                                else {
                                    if ($scope.ItemManager.Mode == 'add') {
                                        SocketFactory.emit('SearchItemIsExist', $scope.ItemManager.FoodItem.code, function (result) {
                                            result = parseInt(result)
                                            if (result != 0) {
                                                $scope.ItemManager.FoodItemIsExist();
                                                return;
                                            }
                                            else {

                                                $(mainThis).closest('ul').children('li').find('a').removeClass('active');
                                                $(mainThis).closest('ul').children('li').find('a').each(function () {
                                                    $(this).closest('li').css('border-bottom-width', '0px');
                                                    $(this.hash).hide();
                                                })
                                                $(mainThis).closest('li').css('border-bottom', '1px solid #ee6e73');
                                                $(mainThis).addClass('active');
                                                $(mainContent).show();



                                                //console.log($scope.FoodItem);
                                                //scope.SubmitScreen();
                                            }
                                        })
                                    } else {

                                        $(mainThis).closest('ul').children('li').find('a').removeClass('active');
                                        $(mainThis).closest('ul').children('li').find('a').each(function () {
                                            $(this).closest('li').css('border-bottom-width', '0px');
                                            $(this.hash).hide();
                                        })
                                        $(mainThis).closest('li').css('border-bottom', '1px solid #ee6e73');
                                        $(mainThis).addClass('active');
                                        $(mainContent).show();

                                    }
                                }
                            }
                            else {

                                $(this).closest('ul').children('li').find('a').removeClass('active');
                                $(this).closest('ul').children('li').find('a').each(function () {
                                    $(this).closest('li').css('border-bottom-width', '0px');
                                    $(this.hash).hide();
                                })
                                $(this).closest('li').css('border-bottom', '1px solid #ee6e73');
                                $(this).addClass('active');
                                $(this.hash).show();
                            }


                        });


                    });

                    $('#back-basic').on('click', function () {
                        $scope.tabsBackFirstItem();
                    })

                }
                $scope.tabsBackFirstItem = function () {
                    var $active, $content, $links = $('ul.tabs').find('a');
                    $active = $($links.filter('[href="' + location.hash + '"]')[0] || $links[0]);

                    $links.not($active).each(function () {
                        $(this).closest('li').css('border-bottom-width', '0px');
                        $(this).removeClass('active');
                        $(this.hash).hide();
                    });

                    $($active[0]).closest('li').css('border-bottom', '1px solid #ee6e73');
                    $($active[0].hash).show();
                }
                $scope.tabsOperation();

                $scope.removeErrorStyle = function () {
                    angular.element('.missing-data').removeClass('missing-data');
                }

                function Category() {

                    Category.prototype.GetAllCategory = function () {


                    }

                }

                function Item() {
                    this.Mode = 'basic';
                    Item.prototype.categories = [];
                    Item.prototype.ItemList = [];
                    Item.prototype.FoodItem = {};
                    this.colors = [
                    { val: "rms-palette-orange", name: "Orange" },
                    { val: "rms-palette-gold", name: "Gold" },
                    { val: "rms-palette-grass", name: "Grass" },
                    { val: "rms-palette-sky", name: "Sky" },
                    { val: "rms-palette-lemon", name: "Lemon" },
                    { val: "rms-palette-jade", name: "Jade" },
                    { val: "rms-palette-navy", name: "Navy" }
                    ]

                    this.ItemType = [{ val: "null", name: "普通" }, { val: "T", name: '時間' }, { val: "o", name: 'Open Key' }]


                    Item.prototype.FoodItemIsExist = function () {
                        $mdDialog.show(
                            $mdDialog.alert()
                              .parent(angular.element(document.body))
                              .title(uiLanguage[INFO.lang]['alert'])
                               .content("食品編號已存在!")
                              .ok(uiLanguage[INFO.lang]['yes'])
                          );
                    }

                    Item.prototype.ItemInfoChecking = function () {
                        var checking = true;
                        if ($scope.ItemManager.FoodItem.category == undefined) {
                            $('#select_value_label_0').addClass('missing-data')
                            checking = false;
                        }

                        if ($scope.ItemManager.FoodItem.color == undefined) {
                            $('#select_value_label_1').addClass('missing-data')
                            checking = false;

                        }

                        if ($scope.ItemManager.FoodItem.code == undefined || $scope.ItemManager.FoodItem.code == "") {
                            $('#code').addClass('missing-data')
                            checking = false;
                        }
           

                        if ($scope.ItemManager.FoodItem.cname == undefined || $scope.ItemManager.FoodItem.cname == "") {
                            angular.element('#cname').addClass('missing-data')
                            checking = false;
                        }

                        if ($scope.ItemManager.FoodItem.plu == undefined || $scope.ItemManager.FoodItem.plu == "") {
                            angular.element('#plu').addClass('missing-data')
                            checking = false;
                        }

                        if ($scope.ItemManager.FoodItem.ename == undefined || $scope.ItemManager.FoodItem.ename == "") {
                            angular.element('#ename').addClass('missing-data')
                            checking = false;
                        }

                        if ($scope.ItemManager.FoodItem.kname == undefined || $scope.ItemManager.FoodItem.kname == "") {
                            angular.element('#kname').addClass('missing-data')
                            checking = false;
                        }

                        if ($scope.ItemManager.FoodItem.printer == undefined || $scope.ItemManager.FoodItem.printer == "") {
                            angular.element('#printer').addClass('missing-data')
                            checking = false;
                        }

                        return checking;

                    }


                    Item.prototype.BackItemListPage = function () {
                        this.Mode = 'basic';
                        $scope.removeErrorStyle();
                        this.initFoodItem();
                        $scope.ModifierManager = new Modifier();
                        $scope.OptionManager = new Option();
                        $scope.SubItemManager = new SubItem();
                        $scope.TimeRangeManager = new TimeRange();
                    }

                    Item.prototype.AddItemPage = function (e) {
                        this.Mode = 'add';
                    }

                    Item.prototype.EditPage = function (e, index) {
                        var elem = e.currentTarget;
                        this.initFoodItem();
                        this.FoodItem.rowCount = index;
                        this.FoodItem.code = $(elem).closest('tr').children('td.code').html();
                        this.FoodItem.ocode = this.FoodItem.code;
                        this.FoodItem.plu = $(elem).closest('tr').children('td.plu').html();
                        this.FoodItem.oplu = this.FoodItem.plu;
                        this.FoodItem.cname = $(elem).closest('tr').children('td.cname').html();
                        this.FoodItem.cname2 = $(elem).closest('tr').children('td.cname2').html();
                        this.FoodItem.ename = $(elem).closest('tr').children('td.ename').html();
                        this.FoodItem.kname = $(elem).closest('tr').children('td.kname').html();
                        this.FoodItem.price = parseFloat($(elem).closest('tr').children('td.price').attr('price'));
                        this.FoodItem.printer = $(elem).closest('tr').children('td.printer').attr('printer');
                        this.FoodItem.printerGroup = $(elem).closest('tr').children('td.printerGroup').attr('printerGroup');
                        this.FoodItem.kds = $(elem).closest('tr').children('td.kds').attr('kds');
                        this.FoodItem.serviceCharge = $(elem).closest('tr').children('td.serviceCharge').attr('serviceCharge') === "true" ? "1" : "0";
                        this.FoodItem.discount = $(elem).closest('tr').children('td.discount').attr('discount');
                        this.FoodItem.screenShow = $(elem).closest('tr').children('td.screenShow').attr('screenShow') === "true" ? "1" : "0";;
                        this.FoodItem.category = $(elem).closest('tr').children('td.category').attr('category');
                        this.FoodItem.color = $(elem).closest('tr').children('td.color').attr('color');
                        this.FoodItem.individualPrint = $(elem).closest('tr').children('td.individualPrint').attr('individualPrint') === "true" ? "1" : "0";

                        this.FoodItem.ItemType = $(elem).closest('tr').children('td.type').attr('type') == "" ? "null" : $(elem).closest('tr').children('td.type').attr('type');

                        this.FoodItem.suspend = $(elem).closest('tr').children('td.suspend').attr('suspend');

                        this.Mode = 'change';
                        $scope.ModifierManager.GetAllModifierGroupByItemCode(this.FoodItem.code);
                        $scope.SubItemManager.GetAllSubItemByItemCode(this.FoodItem.code);
                        $scope.OptionManager.GetAllOptionGroupByItemCode(this.FoodItem.code);
                        $scope.TimeRangeManager.GetAllTimeRangeByItemCode(this.FoodItem.code);
                    }

                    Item.prototype.GetAllItem = function () {
                        SocketFactory.emit('GetCategory', null, function (result) {
                            Item.prototype.categories = JSON.parse(result);
                        });


                        $http({
                            method: 'GET',
                            url: './service.ashx?action=GetAllItem'
                        }).then(function successCallback(response) {
                            $("#preLoading").hide();
                            Item.prototype.ItemList = response.data;
                            console.log(response.data);
                            //$('#FoodItem-list').floatThead();

                        }, function errorCallback(response) {
                        });
                    }

                    Item.prototype.initFoodItem = function () {
                        Item.prototype.FoodItem = {};
                        Item.prototype.FoodItem.serviceCharge = "0";
                        Item.prototype.FoodItem.discount = "0";
                        Item.prototype.FoodItem.screenShow = "0";
                        Item.prototype.FoodItem.individualPrint = "0";
                        Item.prototype.FoodItem.suspend = "0";
                        Item.prototype.FoodItem.ItemType = "null";
                        Item.prototype.FoodItem.price = 0;
                    }

                    Item.prototype.EditSubmit = function () {

                        $scope.removeErrorStyle();

                        if (!this.ItemInfoChecking()) return;
                        else {

                            if (this.Mode == 'add') {
                                SocketFactory.emit('SearchItemIsExist', $scope.ItemManager.FoodItem.code, function (result) {
                                    result = parseInt(result)
                                    if (result != 0) {
                                        Item.prototype.FoodItemIsExist();
                                        return;
                                    }
                                    else {

                                        var confirm = $mdDialog.confirm()
                                       .title(uiLanguage[INFO.lang]['alert'])
                                       .content('確定要輸入?')
                                       .ok(uiLanguage[INFO.lang]['yes'])
                                       .cancel(uiLanguage[INFO.lang]['cancel']);

                                        $mdDialog.show(confirm).then(function () {

                                            var InsertArgs = { "ItemInfo": $scope.ItemManager.FoodItem, "Option": $scope.OptionManager.selectedGroupList, "Modifier": $scope.ModifierManager.selectedModifierGroupList, "SubItem": $scope.SubItemManager.selectedSubitemList, "TimeRange": $scope.TimeRangeManager.selectedTimeRangeList };


                                            SocketFactory.emit('InsertItem', JSON.stringify(InsertArgs), function (result) {
                                                if (result === "OK") {
                                                    //$.ajax({
                                                    //    url: "./service.ashx?action=GetAllItem&itemId=" + ItemCode,
                                                    //    dataType: "JSON",
                                                    //    success: function (data) {
                                                    //        angular.element('#FoodItem-list tbody tr').css('background-color', 'white');
                                                    //        if (itemIndex === 0) {
                                                    //            $scope.itemList.splice(0, 0, data[0]);
                                                    //            $scope.itemList[0].rowStyle = "{'background-color':'yellow'}";
                                                    //        }
                                                    //        else {
                                                    //            itemIndex = parseInt(itemIndex);
                                                    //            console.log($scope.itemList[itemIndex]);
                                                    //            console.log(data[0]);
                                                    //            $scope.itemList[itemIndex] = data[0];
                                                    //            $scope.itemList[itemIndex].rowStyle = "{'background-color':'yellow'}";
                                                    //        }
                                                    //    },
                                                    //    complete: function () {
                                                    //        $scope.BackItemListPage();
                                                    //    }, async: false
                                                    //});

                                                    $http({
                                                        method: 'GET',
                                                        url: './service.ashx?action=GetAllItem&itemId=' + $scope.ItemManager.FoodItem.code,
                                                    }).then(function successCallback(response) {
                                                        Item.prototype.ItemList.splice(0, 0, response.data[0]);
                                                        $scope.ItemManager.Mode = 'basic';
                                                        $scope.ItemManager.BackItemListPage();
                                                        $scope.tabsBackFirstItem();
                                                    }, function errorCallback(response) {
                                                    });


                                                    //return true;
                                                }
                                            });



                                        }, function () {
                                        });

                                    }
                                })



                            }
                            else {


                                var confirm = $mdDialog.confirm()
                                 .title(uiLanguage[INFO.lang]['alert'])
                                 .content('確定要輸入?')
                                 .ok(uiLanguage[INFO.lang]['yes'])
                                 .cancel(uiLanguage[INFO.lang]['cancel']);

                                $mdDialog.show(confirm).then(function () {
                                    console.log('item', $scope.ItemManager.FoodItem);
                                    var InsertArgs = { "ItemInfo": $scope.ItemManager.FoodItem, "Option": $scope.OptionManager.selectedGroupList, "Modifier": $scope.ModifierManager.selectedModifierGroupList, "SubItem": $scope.SubItemManager.selectedSubitemList, "TimeRange": $scope.TimeRangeManager.selectedTimeRangeList };

                                    if (isNaN(InsertArgs.ItemInfo.price)) InsertArgs.ItemInfo.price = null;
                                    console.log(InsertArgs);

                                    SocketFactory.emit('UpdateItem', JSON.stringify(InsertArgs), function (result) {
                                        if (result === "OK") {
                                            $http({
                                                method: 'GET',
                                                url: './service.ashx?action=GetAllItem&itemId=' + $scope.ItemManager.FoodItem.code,
                                            }).then(function successCallback(response) {
                                                Item.prototype.ItemList[$scope.ItemManager.FoodItem.rowCount] = response.data[0];
                                                $scope.ItemManager.Mode = 'basic';
                                                $scope.ItemManager.BackItemListPage();
                                                $scope.tabsBackFirstItem();
                                            }, function errorCallback(response) {
                                            });
                                        }
                                    });


                                }, function () {
                                });

                            }
                        }

                    }


                    this.GetAllItem();
                    this.initFoodItem();

                }



                function Modifier() {
                    Modifier.prototype.modifierGroupList = [];
                    Modifier.prototype.selectedModifierGroupList = [];
                    Modifier.prototype.AllModifierInGroupList = [];
                    Modifier.prototype.GetAllModifierGroup = function () {
                        SocketFactory.emit('GetAllModifierGroup', null, function (result) {
                            Modifier.prototype.modifierGroupList = JSON.parse(result);

                        });
                    }

                    Modifier.prototype.AddModifierGroup = function (item, index) {
                        Modifier.prototype.selectedModifierGroupList.push(item);
                        Modifier.prototype.modifierGroupList.splice(index, 1);
                    }

                    Modifier.prototype.RemoveModifierGroup = function (item, index) {
                        Modifier.prototype.selectedModifierGroupList.splice(index, 1);
                        Modifier.prototype.modifierGroupList.push(item);
                    }

                    Modifier.prototype.GetAllModifierGroupByItemCode = function (itemCode) {
                        SocketFactory.emit('GetAllModifierByItemCode', itemCode, function (result) {
                            Modifier.prototype.selectedModifierGroupList = [];
                            Modifier.prototype.selectedModifierGroupList = JSON.parse(result);

                            Modifier.prototype.selectedModifierGroupList.forEach(function (selected) {
                                Modifier.prototype.modifierGroupList = Modifier.prototype.modifierGroupList.filter(function (Md) {
                                    return Md.modifierGroupCode != selected.modifierGroupCode;
                                })
                            })

                        });

                    }

                    Modifier.prototype.GetAllModifierInGroup = function (GroupCode) {
                        SocketFactory.emit('GetAllModifierItemByGroup', GroupCode, function (result) {
                            Modifier.prototype.AllModifierInGroupList = [];
                            Modifier.prototype.AllModifierInGroupList = JSON.parse(result)[0];
                            console.log(Modifier.prototype.AllModifierInGroupList);
                            $('#modifier-in-group').openModal();
                        });

                    }

                    Modifier.prototype.CloseAllModifierInGroup = function () {
                        $('#modifier-in-group').closeModal();
                    }

                    this.GetAllModifierGroup();

                }

                function Option() {
                    Option.prototype.GroupList = [];
                    Option.prototype.selectedGroupList = [];
                    Option.prototype.AllOptionInGroupList = [];
                    Option.prototype.GetAllOptionGroup = function () {
                        SocketFactory.emit('GetAllOptionGroup', null, function (result) {
                            Option.prototype.GroupList = JSON.parse(result);
                        });
                    }

                    Option.prototype.AddGroup = function (item, index) {
                        var temp = { "goMenu": item.goMenu, "name1": item.name1, "name2": item.name2, "name3": item.name3, "optionGroupCode": item.optionGroupCode, "minQty": 0, "maxQty": 0 }
                        /** prevent Duplicate Key in ngRepeat **/
                        Option.prototype.selectedGroupList.push(temp);
                    }

                    Option.prototype.RemoveGroup = function (item, index) {
                        Option.prototype.selectedGroupList.splice(index, 1);
                    }

                    Option.prototype.UpdateQty = function ($event, $index, type) {

                        if (type == 'min') {
                            Option.prototype.selectedGroupList[$index].minQty = $event.currentTarget.value;
                        } else {
                            Option.prototype.selectedGroupList[$index].maxQty = $event.currentTarget.value;
                        }

                    }

                    Option.prototype.GetAllOptionGroupByItemCode = function (itemCode) {

                        SocketFactory.emit('GetAllOptionGroupByItemCode', itemCode, function (result) {
                            Option.prototype.selectedGroupList = [];
                            Option.prototype.selectedGroupList = JSON.parse(result);


                        });

                    }

                    Option.prototype.GetAllOptionInGroup = function (GroupCode) {
                        SocketFactory.emit('GetAllOptionItemByGroup', GroupCode, function (result) {
                            Option.prototype.AllOptionInGroupList = [];
                            Option.prototype.AllOptionInGroupList = JSON.parse(result)[0];
                            $('#option-in-group').openModal();
                        });
                    }


                    Option.prototype.CloseAllOptionInGroup = function () {
                        $('#option-in-group').closeModal();
                    }
                    this.GetAllOptionGroup();




                }



                function SubItem() {

                    SubItem.prototype.SubitemList = [];
                    SubItem.prototype.selectedSubitemList = [];
                    SubItem.prototype.GetAllSubItem = function () {

                        SocketFactory.emit('GetAllSubItem', null, function (result) {
                            SubItem.prototype.SubitemList = JSON.parse(result);
                        });
                    }

                    SubItem.prototype.AddSubitem = function (item, $index) {
                        SubItem.prototype.selectedSubitemList.push(item);
                        SubItem.prototype.SubitemList.splice($index, 1);

                    }

                    SubItem.prototype.RemoveSubitem = function (item, $index) {
                        SubItem.prototype.selectedSubitemList.splice($index, 1);
                        SubItem.prototype.SubitemList.push(item);
                    }

                    SubItem.prototype.GetAllSubItemByItemCode = function (itemCode) {

                        SocketFactory.emit('GetAllSubItemByItemCode', itemCode, function (result) {
                            SubItem.prototype.selectedSubitemList = [];
                            SubItem.prototype.selectedSubitemList = JSON.parse(result);

                            SubItem.prototype.selectedSubitemList.forEach(function (selected) {
                                SubItem.prototype.SubitemList = SubItem.prototype.SubitemList.filter(function (sub) {
                                    return sub.subitemCode != selected.subitemCode;
                                })
                            })

                        });
                    }

                    this.GetAllSubItem();
                }

                function TimeRange() {
                    TimeRange.prototype.TimeRangeList = [];
                    TimeRange.prototype.selectedTimeRangeList = [];
                    TimeRange.prototype.GetAllTimeRange = function () {
                        SocketFactory.emit('listTimeRange', null, function (result) {
                            TimeRange.prototype.TimeRangeList = JSON.parse(result);
                        });
                    }

                    TimeRange.prototype.AddTimeRange = function (item, index) {
                        item.ActiveDate = $.datepicker.formatDate('yy-mm-dd', new Date());
                        TimeRange.prototype.selectedTimeRangeList.push(item);
                        TimeRange.prototype.TimeRangeList.splice(index, 1);
                    }

                    TimeRange.prototype.RemoveTimeRange = function (item, index) {

                        TimeRange.prototype.TimeRangeList.push(item);
                        TimeRange.prototype.selectedTimeRangeList.splice(index, 1);
                    }

                    TimeRange.prototype.UpdateDate = function ($event, $index) {


                        if ($event.currentTarget.value == "" || $event.currentTarget.value == undefined) {
                            $event.currentTarget.value = $.datepicker.formatDate('yy-mm-dd', new Date());
                        } else {
                            TimeRange.prototype.selectedTimeRangeList[$index].ActiveDate = $event.currentTarget.value;
                        }



                    }

                    TimeRange.prototype.GetAllTimeRangeByItemCode = function (itemCode) {

                        SocketFactory.emit('GetAllTimeRangeByItemCode', itemCode, function (result) {
                            TimeRange.prototype.selectedTimeRangeList = [];
                            TimeRange.prototype.selectedTimeRangeList = JSON.parse(result);

                            TimeRange.prototype.selectedTimeRangeList.forEach(function (selected) {
                                TimeRange.prototype.TimeRangeList = TimeRange.prototype.TimeRangeList.filter(function (tr) {
                                    return tr.timeRangeId != selected.timeRangeId;
                                })
                            })

                        });

                    }

                    this.GetAllTimeRange();

                }

                $scope.ItemManager = new Item();
                $scope.ModifierManager = new Modifier();
                $scope.OptionManager = new Option();
                $scope.SubItemManager = new SubItem();
                $scope.TimeRangeManager = new TimeRange();
            }
        }
    });


    app.directive('subitemScene', function (MainService, $timeout, uiLanguage, INFO) {
        return {
            restrict: "E",
            templateUrl: '../tmpl/subitem.html',
            link: function (scope, element) {
                Materialize.updateTextFields();
            },
            controller: function ($scope, MainService, $timeout, uiLanguage, INFO, SocketFactory, $mdDialog) {
                var ui = $scope.ui = uiLanguage[INFO.lang];
                $scope.Mode = 'basic';
                $scope.ItemObject = { "index": null, "subitemCode": "SB", "name1": null, "name2": null, "name3": null, "namek": null, "printer": null, "printerGroup": null, "individualPrint": "0" };

                $scope.GetAllSubitem = function () {
                    SocketFactory.emit('GetAllSubItem', null, function (result) {
                        $scope.SubitemList = [];
                        $scope.SubitemList = JSON.parse(result);


                    });
                }


                $scope.BackBasicList = function () {
                    $scope.Mode = 'basic';
                    $scope.removeErrorStyle();
                    $scope.ItemObject = { "index": null, "subitemCode": "SB", "name1": null, "name2": null, "name3": null, "namek": null, "printer": null, "printerGroup": null, "individualPrint": "0" };
                }

                $scope.EditBtnClick = function (item, index) {
                    $scope.Mode = 'change';
                    console.log(item);
                    $scope.ItemObject = item;
                    if (item.individualPrint == null) item.individualPrint = "0";
                    else if (item.individualPrint) item.individualPrint = "1";
                    else item.individualPrint = "0";
                    $scope.ItemObject.index = index;
                }


                $scope.EditSubmit = function () {
                    var checking = true;
                    $scope.removeErrorStyle();
                    var optionCodeCheck = /[S][B][a-zA-Z0-9]{5}$/;
                    //if ($scope.ItemObject.optionCode.charAt(0) != "O" || $scope.ItemObject.optionCode.charAt(1) != "P" || $scope.ItemObject.optionCode.trim().length != 7) {
                    //    angular.element('#code').addClass('missing-data');
                    //    checking = false;
                    //}

                    if (!optionCodeCheck.test($scope.ItemObject.subitemCode)) {
                        angular.element('#code').addClass('missing-data');
                        checking = false;
                    }

                    if ($scope.ItemObject.name1 == null || $scope.ItemObject.name1 == "") {
                        angular.element('#name1').addClass('missing-data');
                        checking = false;
                    }

                    if ($scope.ItemObject.name2 == null || $scope.ItemObject.name2 == "") {
                        angular.element('#name2').addClass('missing-data');
                        checking = false;
                    }

                    //if ($scope.ItemObject.name3 == null || $scope.ItemObject.name3 == "") {
                    //    angular.element('#name3').addClass('missing-data');
                    //    checking = false;
                    //}

                    if ($scope.ItemObject.namek == null || $scope.ItemObject.namek == "") {
                        angular.element('#namek').addClass('missing-data');
                        checking = false;
                    }

                    if (checking) {
                        var GlobalMode = $scope.Mode;
                        var confirm = $mdDialog.confirm()
                        .title(uiLanguage[INFO.lang]['alert'])
                        .content('確定要輸入?')
                        .ok(uiLanguage[INFO.lang]['yes'])
                        .cancel(uiLanguage[INFO.lang]['cancel']);

                        $mdDialog.show(confirm).then(function () {
                            if (GlobalMode == 'add') {
                                SocketFactory.emit('GetAllSubItem', $scope.ItemObject.subitemCode, function (result) {
                                    result = JSON.parse(result);
                                    if (result.length != 0) {
                                        $scope.itemIsExist();
                                    }
                                    else {

                                        SocketFactory.emit('InsertSubitem', JSON.stringify($scope.ItemObject), function (result) {
                                            if (result == "OK") {
                                                $scope.SuccessOperation();
                                                $scope.SubitemList.push($scope.ItemObject);
                                                $scope.ItemObject = { "index": null, "subitemCode": "SB", "name1": null, "name2": null, "name3": null, "namek": null, "printer": null, "printerGroup": null, "individualPrint": "0" };
                                                $scope.GetAllSubitem();

                                            }
                                        });
                                    }
                                });
                            } else if (GlobalMode == 'change') {

                                SocketFactory.emit('UpdateSubitem', JSON.stringify($scope.ItemObject), function (result) {
                                    if (result == "OK") {
                                        $scope.SuccessOperation();
                                        $scope.SubitemList[$scope.ItemObject.index] = $scope.ItemObject;
                                        $scope.ItemObject = { "index": null, "subitemCode": "SB", "name1": null, "name2": null, "name3": null, "namek": null, "printer": null, "printerGroup": null, "individualPrint": "0" };
                                        $scope.GetAllSubitem();
                                    }
                                });

                            }



                        }, function () {
                        });
                    }

                }


                $scope.removeErrorStyle = function () {
                    angular.element('.missing-data').removeClass('missing-data');
                }

                $scope.itemIsExist = function () {
                    $mdDialog.show(
                       $mdDialog.alert()
                         .parent(angular.element(document.body))
                         .title(uiLanguage[INFO.lang]['alert'])
                          .content("Code已存在!")
                         .ok(uiLanguage[INFO.lang]['yes'])
                     );
                }

                $scope.SuccessOperation = function () {
                    $mdDialog.show(
                        $mdDialog.alert()
                          .parent(angular.element(document.body))
                          .title(uiLanguage[INFO.lang]['alert'])
                           .content("完成!")
                          .ok(uiLanguage[INFO.lang]['yes'])
                      );
                }
                $scope.GetAllSubitem();

            }
        }
    });

    app.directive('sideNavLeft', function (MainService, uiLanguage, INFO, userConfig, $log) {
        return {
            restrict: "E",
            templateUrl: 'tmpl/sidenav.html',
            link: function (scope, element, attrs) {
                // console.log($log);
                // console.log(element.controller());
                scope.ui = uiLanguage[INFO.lang];
                scope.lang = INFO.lang;
                scope.permissionList = userConfig.permissionList;
            }
            // controller: 'navLeftCtrl as vm'
        }
    });

    app.controller('sideMenuCtrl', function ($scope, $rootScope, $log, $timeout, $location, menu) {
        $scope.close = function () {
            $mdSidenav('left').close()
              .then(function () {
                  $log.debug("close LEFT is done");
              });
        };

        // console.log('sideMenuCtrl');
        var vm = this;
        // var aboutMeArr = ['Family', 'Location', 'Lifestyle'];
        // var budgetArr = ['Housing', 'LivingExpenses', 'Healthcare', 'Travel'];
        // var incomeArr = ['SocialSecurity', 'Savings', 'Pension', 'PartTimeJob'];
        // var advancedArr = ['Assumptions', 'BudgetGraph', 'AccountBalanceGraph', 'IncomeBalanceGraph'];

        //functions for menu-link and menu-toggle
        vm.isOpen = isOpen;
        vm.toggleOpen = toggleOpen;
        vm.autoFocusContent = false;
        vm.menu = menu;

        vm.status = {
            isFirstOpen: true,
            isFirstDisabled: false
        };

        function isOpen(section) {
            return menu.isSectionSelected(section);
        }

        function toggleOpen(section) {
            menu.toggleSelectSection(section);
        }

        // console.log(vm);
    });


    app.directive('priceScene', function (MainService, $timeout, uiLanguage, INFO) {
        return {
            restrict: "E",
            templateUrl: '../tmpl/price.html',
            link: function (scope, element) {
                Materialize.updateTextFields();
            },
            controller: function ($scope, MainService, $timeout, uiLanguage, INFO, SocketFactory, $mdDialog) {
                var ui = $scope.ui = uiLanguage[INFO.lang];
                $scope.Mode = 'basic';


                $scope.removeErrorStyle = function () {
                    angular.element('.missing-data').removeClass('missing-data');
                }

                $scope.SuccessOperation = function () {
                    $mdDialog.show(
                        $mdDialog.alert()
                          .parent(angular.element(document.body))
                          .title(uiLanguage[INFO.lang]['alert'])
                           .content("完成!")
                          .ok(uiLanguage[INFO.lang]['yes'])
                      );
                }

                $scope.priceBatch = {
                    batchList: [],
                    mode: 'basic',
                    currentPriceBatch: {},
                    getAllBatch: function () {
                        SocketFactory.emit('getAllPriceBatch', function (result) {
                            $scope.priceBatch.batchList = result;
                        })
                    },
                    insertBatch: function () {
                        this.mode = 'add';
                        this.currentPriceBatch = { priceBatchId: null, name: null, activateDate: null, expireDate:null};
                    },
                    updateBatch: function (item) {
                        this.mode = 'change';
                        //angular.copy(item, this.currentPriceBatch);
                        $.extend(true, this.currentPriceBatch, item);
                        //this.currentPriceBatch.name = item.name;
                        this.currentPriceBatch.activateDate = moment(item.activateDate).toDate();
                        //this.currentPriceBatch.expireDate = moment(this.currentPriceBatch.expireDate).toDate();
                        this.currentPriceBatch.expireDate = this.currentPriceBatch.expireDate != null ? moment(item.expireDate).toDate() : null;
                    },
                    backBasicMode: function () {
                        this.mode = 'basic';
                        $scope.batchDetail.mode = 'basic'
                        this.currentPriceBatch = { priceBatchId: null, name: null, activateDate: null, expireDate: null };
                        $scope.batchDetail.batchDetailList = [];
                        $scope.removeErrorStyle();
                    },
                    submitClick: function () {


                        var checking = true;
                        $scope.removeErrorStyle();

                        if (this.currentPriceBatch.name == null || this.currentPriceBatch.name == "") {
                            angular.element('#batch-name').addClass('missing-data');
                            checking = false;
                        }

                        if (this.currentPriceBatch.activateDate == null || this.currentPriceBatch.activateDate == undefined) {
                            $('#Date1').addClass('missing-data');
                            $('#activateTime').addClass('missing-data');
                            checking = false;
                        }
              
                        if (checking) {
                            var confirm = $mdDialog.confirm()
                            .title(uiLanguage[INFO.lang]['alert'])
                            .content('確定要輸入?')
                            .ok(uiLanguage[INFO.lang]['yes'])
                            .cancel(uiLanguage[INFO.lang]['cancel']);
                            $mdDialog.show(confirm).then(function () {
                                if ($scope.priceBatch.mode == 'change') {

                                    SocketFactory.emit('updatePriceBatch', $scope.priceBatch.currentPriceBatch, function (result) {
                                        if (result == 'OK') {
                                            $scope.SuccessOperation();
                                            var templ = $.grep($scope.priceBatch.batchList, function (item) {
                                                return item.priceBatchId == $scope.priceBatch.currentPriceBatch.priceBatchId;
                                            });
                                            if (templ.length > 0) {
                                                templ[0] = $scope.priceBatch.currentPriceBatch;
                                            }
                                            $scope.priceBatch.backBasicMode();
                                        }
                                    })
                                }
                                else if ($scope.priceBatch.mode == 'add') {
                                    SocketFactory.emit('insertPriceBatch', $scope.priceBatch.currentPriceBatch, function (result) {
                                        if (result == 'OK') {
                                            $scope.SuccessOperation();
                                            $scope.priceBatch.batchList.push($scope.priceBatch.currentPriceBatch);
                                            $scope.priceBatch.getAllBatch();
                                            $scope.priceBatch.backBasicMode();
                                        }
                                    })
                                }
                            }, function () {
                            });

                        }





                    }



                }

                $scope.batchDetail = {
                    batchDetailList: [],
                    notInBatchDetailList: [],
                    currentPriceBatch:{},
                    mode: "basic",
                    currentSelectDetailItem: {},
                    getBatchDetailByBatch: function (batch) {
                        if ($scope.priceBatch.mode != 'basic') return;
                        $scope.batchDetail.currentPriceBatch = batch;
                        SocketFactory.emit('getBatchDetailByBatch', batch.priceBatchId, function (result) {
                            $scope.batchDetail.batchDetailList = result;
                        });
                    },
                    updateBatchDetailItem: function (batchDetailItem) {
                        this.mode = 'change';
                        this.currentSelectDetailItem = batchDetailItem;
                        this.currentSelectDetailItem.timeStart = moment(this.currentSelectDetailItem.timeStart).toDate();
                        this.currentSelectDetailItem.timeEnd = moment(this.currentSelectDetailItem.timeEnd).toDate();
                    },
                    backBasicMode: function () {
                        this.mode = 'basic';
                        this.currentSelectDetailItem = {};
                        $scope.removeErrorStyle();
                    },
                    btnAddBatchDetailClick: function () {
                        console.log($scope.batchDetail.currentPriceBatch)
                        if ($scope.batchDetail.currentPriceBatch.priceBatchId == undefined) return;
                        this.mode = 'add';
                        SocketFactory.emit('getAllNotInBatchDetailItem', $scope.batchDetail.currentPriceBatch.priceBatchId, function (result) {
                            $scope.batchDetail.notInBatchDetailList = result;
                            console.log(result);
                        });

                    },
                    submitUpdateBatchDetailItem: function () {

                        var checking = true;
                        $scope.removeErrorStyle();

                        if (this.currentSelectDetailItem.price1 == null) {
                            angular.element('#price').addClass('missing-data');
                            checking = false;
                        }

                        if (checking) {
                            var confirm = $mdDialog.confirm()
                            .title(uiLanguage[INFO.lang]['alert'])
                            .content('確定要輸入?')
                            .ok(uiLanguage[INFO.lang]['yes'])
                            .cancel(uiLanguage[INFO.lang]['cancel']);
                            $mdDialog.show(confirm).then(function () {
                                var args = {
                                    'detail': $scope.batchDetail.currentSelectDetailItem,
                                    'batch': $scope.batchDetail.currentPriceBatch
                                }

                                SocketFactory.emit('updateBatchDetailItem', args, function (result) {
                                    if (result == 'OK') {
                                        $scope.SuccessOperation();
                                        $scope.batchDetail.backBasicMode();
                                    }
                                })


                            }, function () {
                            });

                        }



                    },
                    addDetailItem: function (detailItem, index) {
                        console.log(detailItem);
                        var templItem = {
                            Item: detailItem,
                            itemId: detailItem.code,
                            price1: detailItem.price,
                            price2: null,
                            price3: null,
                            price4: null,
                            price5: null,
                            priceBatchId: $scope.batchDetail.currentPriceBatch.priceBatchId,
                            priceZoneId: $scope.batchDetail.currentPriceBatch.priceBatchId,
                            timeEnd: null,
                            timeStart: null
                        }

                        SocketFactory.emit('insertDetailItemByBatch', templItem, function (result) {
                            if (result == 'OK') {
                                $scope.batchDetail.batchDetailList.push(templItem);
                                $scope.batchDetail.notInBatchDetailList.splice(index, 1);
                            }
                        })


                    },
                    removeBatchDetailItem: function (detailItem, index) {
                        var args = {
                            priceBatchId: $scope.batchDetail.currentPriceBatch.priceBatchId,
                            itemId: detailItem.itemId
                        }

                        SocketFactory.emit('removeDetailItemByBatch', args, function (result) {
                            if (result == "OK") {
                                $scope.batchDetail.batchDetailList.splice(index, 1);
                                $scope.batchDetail.notInBatchDetailList.push({ code: detailItem.itemId, name1: detailItem.Item.name1, price: "" });
                            }
                        });

                        //batchDetailList: [],
                        //notInBatchDetailList:[],
                    }

                }

                $scope.priceBatch.getAllBatch();

            }
        }
    });

})();