(function () {
    var app = angular.module('rms.directives.dummy', []);
    // 'ngMaterial'
    
    // app.run(function ($templateCache){
    //   // override is working with material directive tag
    //   // $templateCache.put('template/pagination/pager.html', '<md-button class="md-raised md-button md-mini md-button" style="color: black">haha</md-button>');
    //   $templateCache.put('template/pagination/pager.html', '<md-grid-list md-cols-sm="1" md-cols-md="2" md-cols-gt-md="6" md-row-height-gt-md="1:1" md-row-height="2:2" md-gutter="12px" md-gutter-gt-sm="8px" ><md-grid-tile><md-grid-tile-header><h3>This is a header</h3></md-grid-tile-header><md-grid-tile class="green"><md-grid-tile-footer><h3>#2: (1r x 1c)</h3></md-grid-tile-footer></md-grid-tile></md-grid-tile></md-grid-list>');
    // });

    // test
    /*app.directive('mdButtonTest', function () {
        return {
          restrict: 'EA',
          scope: {
            totalItems: '=',
            previousText: '@',
            nextText: '@'
          },
          require: ['pager', '?ngModel'],
          controller: 'PaginationController',
          templateUrl: 'template/pagination/pager.html',
          replace: true,
          link: function(scope, element, attrs, ctrls) {
            var paginationCtrl = ctrls[0], ngModelCtrl = ctrls[1];

            if (!ngModelCtrl) {
               return; // do nothing if no ng-model
            }

            scope.align = angular.isDefined(attrs.align) ? scope.$parent.$eval(attrs.align) : pagerConfig.align;
            paginationCtrl.init(ngModelCtrl, pagerConfig);
          }
        };

        // return {
        //     restrict: 'E',
        //     templateUrl: 'template/pagination/pager.html',
        //     link: function (scope, element, attrs) {
                
        //     },
        //     controller: function( $templateCache ) {
        //         console.log($templateCache.get('template/pagination/pager.html'));
        //     }
        // };
    });*/

    //------------------------------------------- update end

    /*
        page/scene directives
    */
    // app.directive('kitchenInfoScene', function (MainService) {
    //     return {
    //         restrict: "E",
    //         templateUrl: 'tmpl/kitchenInfo.html',
    //         controller: function($scope, MainService) {
    //             $scope.MainService = MainService;
    //         }
    //     }
    // })

    app.directive('cancelOptionScene', function (MainService) {
        return {
            restrict: "E",
            templateUrl: 'tmpl/cancelOptions.html',
            controller: function($scope, MainService,INFO) {
                $scope.MainService = MainService;
                $scope.INFO = INFO;
                $scope.itemsPerPage = 13;
                $scope.totalItems = INFO.cancelRemark.length;
                $scope.currentPage = 1;
            }
        }
    });

    app.directive('divideBill', function (MainService) {
        return {
            restrict: "E",
            templateUrl: 'tmpl/divideBill.html',
            controller: function($scope, MainService) {
                $scope.MainService = MainService;
            }
        }
    });

    // app.directive('reportScene', function (MainService) {
    //     return {
    //         restrict: "E",
    //         templateUrl: 'tmpl/reportScene.html',
    //         controller: function($scope, MainService) {
    //             $scope.MainService = MainService;
    //         }
    //     }
    // });

    // directives created by Daniel for merge and test here
    // app.controller('tableController', function ($scope, INFO, SocketFactory, $element, $q, $http){
    //     console.log($q.all);
    //  // console.log(MainService.table);

    //     // console.log("here?22222");
    //     // console.log(MainService.food);
    //     // console.log(MainService.totalItems);
    //     $scope.Hide = function () {
    //         $element.addClass('ng-hide');
    //     }
    //     $scope.Show = function () {
    //         $element.removeClass('ng-hide');
    //     }
    //     // $scope.MainService = MainService;
    // })

})();