/*
  function to make cell editable
  step
  make it editable
    double click to trigger, single click for development phase
  after change of data
    save to cell for display
    save to JSON and pass to DB

  objective: 
    make one time editable without listening to anything, lightweight
    support input(text, checkbox), select, textarea
  dependency: jQuery
*/

/*
  sortable table
  make table sortable
  dependency: jQuery
*/
/*
  jQuery plugin to make fields editable with class "editable"
  field need to equip with attribute: data-field-type="" where type = "textfield", "select" is supported so far
  more field type: checkbox, dropdown, number and so on
  usage: $(jQuery object).makeEditable();
  support single or multiple jQuery objects

  options(optional): object
  {
    optionData: {}/object
  }

  Notes for event
  nature input support focus, blur, click, select(IE), mousedown...
  nature select support focus, blur, change, click, select(IE) mousedown...
  normal DOM support click, mousedown...

  event flow:
  body click -> trigger blur/custom blur to element -> remove form element from TD cell
*/
$.fn.makeEditable = function ( options ) {
  'use strict' // write more secured code

  // bind object function to any context scope
  function bind( context, name ){
    return function(){
    return context[name].apply(context,arguments);
    };
  }

  function triggerCustomEvents( fieldType ) {
    // console.log('triggerCustomEvents');
    // console.log(fieldType);
    var fieldType = fieldType ? fieldType : '',
        element = this;
    // console.log(this);

    // console.log('trigger: ' + fieldType);
    switch( fieldType ) {
      case 'textfield':
      case 'dropdown':
        console.log('trigger fieldChange when blur');
        $( element ).trigger('fieldChange', [ 'fieldChange' ]);
        break;
      case 'checkbox':
        // console.log(options); // available
        // var rowID = $( element ).parent('tr').data('rowID');
        // var bindKey = $( element ).data('bind-key') ? $( element ).data('bind-key') : '';
        // $( element ).trigger('fieldChange', [type, bindKey]);
        //     // name[001]
        console.log('trigger checkboxChange when click to materializecss checkbox');
        $( element ).trigger('checkboxChange', [ 'checkboxChange' ]);
        break;
      // global change
      default :
        // when body or a new TD element is clicked, will call customBlur for previous existing elements
        $( element ).parents('tbody').find('.select-dropdown, label.editable-checkbox').trigger('customBlur');
        $( element ).parents('tbody').find('input').trigger('blur');
        break;
    }
    // order does matter, when blur occur, element is being removed, any event after will not work
    // $( element ).parents('tbody').find('.editable-checkbox').trigger('customKeydown');
    // $( element ).parents('tbody').find('.select-dropdown, label.editable-checkbox').trigger('customBlur');
    // $( element ).parents('tbody').find('input').trigger('blur');
    // console.log($( element ).parents('tbody').find('label.editable-checkbox'));
    // console.log($( element ).parents('tbody').find('.editable-checkbox'));
    // $( element ).parents('tbody').find('input').trigger('blur');
    // console.log($( element ).parents('tbody').find('.select-dropdown, .editable-checkbox, input'));
    // console.log( $( element ).parents('tbody').find('input') );
  }


  function addEventForGlobal2( element ) {

    var editor = this;
    // console.log(this);
    $('body').click(function(e){
      // e.stopPropagation();
      // console.log(e);
      // // restore the current cell, because current cell click will not propagate to td parent with stopPropagation()
      // // $(element).html(editor._originalElement);
      // // trigger blur to custom drop down
      triggerCustomEvents.apply( element );

      $( element ).find('.color-box').removeClass('hidden');
      $( element ).parents('table tbody tr').removeClass('active');
    });
  }

  function isModified( oval, newVal ) {
    // console.log(newVal);
    // console.log(oval);
    return oval != newVal;
  }

  // TODO: need to check filtered elements
  function addKeyEventToElement() {
    // console.log(this);
    var element = $(this),
        editorElement = this.editor.editorElement,
        editor = this.editor,
        keymap = this.keymap;
    // if( editor.fieldType === 'checkbox' )
    //   editorElement = this.editor.editorElement.find('input');

    // console.log(editorElement);

    // if( typeof $target === 'undefined' || $($target).length === 0 )
    //   return;

    // console.log( 'filtered? ' + editor.isFiltered() );

    editorElement
    .on('keydown', function (e) {
      console.log('key down');
      var keys = editor.keyComb,
          $row = $(element).parents('tr'),
          $prevRow = editor.isFiltered() ? $row.prevAll(':not(".filtered"):eq(0)') : $row.prev(),
          $nextRow = editor.isFiltered() ? $row.nextAll(':not(".filtered"):eq(0)') : $row.next();
      // console.log(e);
      // console.log($row);
      
      // console.log('======================================');
      // console.log(e.which);
      // console.log(typeof e.which); // number
      if( keys.indexOf(e.which) === -1 ) {
        // see if "shift" is hold
        // console.log("shift hold?--------------");
        // console.log($.fn.makeEditable.keyHold);
        if( $.fn.makeEditable.keyHold && keys.indexOf(keymap.SHIFT) === -1 ) {
          console.log('yes key is hold');
          editor.keyComb.unshift( keymap.SHIFT );
        }

        editor.keyComb.push(e.which);
        // console.log('add to keyComb');
      }
      // console.log(editor.keyComb);
      // console.log('======================================');
        

      // following key, prevent default behavior
      if( e.which === keymap.TAB ) {
        e.preventDefault();
      }

      // key control for table navigation
      switch( keys.length ) {
        case 1:
          if( keys[0] === keymap.TAB || keys[0] === keymap.ARROW_RIGHT ) {
            console.log('tab down');
            $( element ).next().click();
          }
          if( keys[0] === keymap.SHIFT ) {
            console.log('shift down');
            $.fn.makeEditable.keyHold = true;
            console.log('key is unhold');
          }
          if( keys[0] === keymap.ARROW_DOWN ) {
            console.log('arrow down');
            // console.log($(element).index());
            var colIdx = $(element).index();
            $nextRow.find('td:eq('+colIdx+')').click();
          }
          if( keys[0] === keymap.ARROW_UP ) {
            console.log('arrow up');
            // console.log($(element).index());
            console.log($prevRow);
            var colIdx = $(element).index();
            // console.log($row.prev().find('td:eq('+colIdx+')'));
            $prevRow.find('td:eq('+colIdx+')').click();
          }
          if( keys[0] === keymap.ARROW_LEFT ) {
            console.log('arrow left');
            // console.log($(element).index());
            $( element ).prev().click();
          }
          if( keys[0] === keymap.INSERT ) {
            console.log('insert');
            // console.log(typeof options.$scope.addFood);
          }
          break;
        case 2:
          if( keys[0] === keymap.SHIFT && keys[1] === keymap.TAB ) {
            console.log('shift tab');
            $( element ).prev().click();
          }
          break;
      }
      // console.log(editor.keyComb);
    })
    .on('keyup', function (e) {
      // console.log('key up common');
      // console.log(e.which);
      // order of key is matter

      // switch( keys.length ) {
      //   case 1:
      //     if( keys[0] === keymap.TAB ) {
      //       console.log('tab up');
      //     }
      //     break;
      //   case 2:
      //     if( keys[0] === keymap.SHIFT && keys[1] === keymap.TAB ) {
      //       console.log('shift tab');
      //     }
      //     break;
      // }
      var keys = editor.keyComb;
      editor.keyComb = []; // reset key combination store
      // console.log(editor.keyComb);
      
      e.stopPropagation();
      // switch can reduce redundant event response
      switch( e.which ) {
        case keymap.SHIFT:
        console.log('shift up');
          $.fn.makeEditable.keyHold = false;
          break;
      }
      // console.log(e.which);
      console.log('after switch');
    })
  }
  
  // console.log($.fn.makeEditable == this);
  // return jQuery element(s) 
  return $(this).each(function(){
    // console.log(this);
    var element = $(this),
        keymap = {
          ESC: 27,
          ENTER: 13,
          TAB: 9,
          SHIFT: 16, // left and right are same
          CTRL: 17,
          ALT: 18,
          F5: 116,
          F8: 119,
          F11: 122,
          ARROW_LEFT: 37,
          ARROW_UP: 38,
          ARROW_RIGHT: 39,
          ARROW_DOWN: 40,
          INSERT: 45
        },
        // buildOptions = function(){
        //   var opts = $.extend( $.fn.editableTableWidget.defaultOptions, options);
        //   return opts;
        // },
        editor = {
          keyComb: [],
          isActive: function() {
            return $(element).hasClass( 'active' );
          },
          isFiltered: function() {
            if( $( element ).parents('table').find('.filtered').length > 0 )
              return true;
            else
              return false;
          },
          _chkModified: function( newValue ) {
            // console.log($(element).data('oval'));
            if( !isModified( $(element).data('oval'), newValue) ) {
              console.log("not modified");
              $( element ).removeClass('modified');
              $( element ).parents('tr').removeClass('modified');
            }
          },
          _init: function( e ) {
            console.log('-----------------------------------------------------');
            // the jquery object then store in element i.e. editor.element and bind related event
            console.log("create input editor if there isn't any for editable");
            console.log(element.data());
            // stop propagation to child, prevent weird UI response
            e.stopPropagation();
            // console.log(e);
            // console.log(element);
            // console.log(this);
            // console.log(options);
            // console.log(options.$scope);
            var editor = this,
                fieldType = element.data("field-type");
            this.dropdownTitle = element.data("dropdown-title") ? element.data("dropdown-title") : '';
            this.fieldType = fieldType; // new way is put it here, and reduce variables amongs each function
            console.log(fieldType + '-----------------------------');
            if(options && options.rowCallback)
              options.rowCallback.apply(options.$scope, [element]) || noop();


            /*
              these object creation function based on HTML tag and its attribute
              <select>
              <input>
              <textarea>
            */
            function createSelect() {
              // console.log($( element ).parents('table').find('.select-wrapper.'+dropdownType));
              $( element ).parents('table').find('.select-wrapper.'+dropdownType + ', select').remove();
              // if( editorElement.length === 0) {
              console.log("newly create");

              // materialcss will copy these css classes to its input tag
              var cssClass = options.dropdown.cssClass,
                  type = options.dropdown.type,
                  optionData = options.dropdown.optionData,
                  optionKey = options.dropdown.optionKey ? options.dropdown.optionKey : null;
              // console.log(optionData);

              if( typeof optionData != 'undefined' && typeof optionData.length != 'undefined') {
                var option = "";
                if( this.dropdownTitle )
                  option = "<option value=\"\" disabled selected>"+this.dropdownTitle+"</option>";
                
                // console.log(this);
                console.log(this.dropdownTitle);

                if( optionKey === null ) {
                  for(var i = 0; i < optionData.length; i++) {
                      option += '<option value="'+optionData[i]+'">'+optionData[i] + '</option>';
                  }
                } else {
                  for(var i = 0; i < optionData.length; i++) {
                      option += '<option value="'+optionData[i][optionKey]+'">'+optionData[i][optionKey]+'</option>';
                  }
                }
              }

              // up to this point is normal select
              var editorElement = $('<select class="' + [cssClass, type, 'editable-field'].join(" ") + '">' + option + '</select>');
              $(element).append(editorElement); 
              // console.log(editorElement);
              return editorElement;
            }; // createSelect

            /*
              default: textfield
              type: @string (email, checkbox, radio, file, color, date...)
              reference: https://developer.mozilla.org/en/docs/Web/HTML/Element/Input
            */
            function createInput( fieldType ) {
              console.log("editable " + fieldType + " is created");
              var fieldType = fieldType ? fieldType : 'textfield';
              var editorElement = '';
              if( typeof $.fn.material_select != 'undefined' ) {
                // console.log(fieldType);
                // if materialcss is avaiable
                switch( fieldType ) {
                  default:
                  // case 'checkbox':
                  case 'textfield':
                  editorElement = $('<input class=\"editable-field editable-' + fieldType + '\" type="text" value=\"testing default\" />');
                    break;
                  case 'checkbox':
                    // editorElement = $('<div class=\"editable-field input-field checkbox-container editable-' + fieldType + '\"><input type=\"checkbox\" class=\"filled-in\" id=\"table-checkbox\" /><label for=\"table-checkbox\"></label></div>');
                    editorElement = $('<input type=\"checkbox\" class=\"filled-in editable-' + fieldType + '\" id=\"table-checkbox\" /><label class=\"editable-'+fieldType+'\" for=\"table-checkbox\"></label>');
                    // editorElement = $('<input class=\"editable-field editable-' + fieldType + '\" type='checkbox' value=\"testing default\" />');
                    break;
                  case 'email':
                    
                    break;
                }
              } else {
                // normal browser input style
                editorElement = $('<input class=\"editable-' + fieldType + '\" type="text" value=\"\" />');
              }
              // return jQuery object
              return editorElement;
            } // createInput

            function createTextArea() {

            } // createTextArea

            /*if( $('.editable-container').length === 0 ) {
              console.log("editable container is created");
              $("body").prepend('<div class=\"editable-container\"></div>');
            }*/
            // restore previous for custom input object by DOM micmic
            // because color dropbox is not a select object, no focus or blur event, need to mimic
            // will only appear in cell with select that is select, not active normally, will not affect normal operation
            // show previous hidden cell and hide current original
            $( element ).parents('table').find('.hidden').removeClass('hidden');
            if( $( element ).parents('table').find('.select-wrapper') ) {
              $( element ).parents('table').find('.select-wrapper').hide();
            }

            switch( fieldType ) {
              case 'checkbox':
                var editorElement = createInput( fieldType );
                this.editorElement = editorElement;
                this._addElement();
                // console.log($(element).data());
                // console.log(editorElement);

                // event handlers
                // focus is based on label since materialcss used label as display, input is hidden, need to twist style as well, see admin.css
                $(editorElement[1]).focus().select()
                .click(function(e){
                })

                // value is getting from the input
                $(editorElement[0])
                .click(function(e){
                  // console.log($(this).siblings("input"));
                  // console.log($(this).prop('checked'));
                  editor._setFieldValue( $(this).prop('checked') );

                  editor._setStatus( 'modified' );
                  editor._chkModified( $(this).prop('checked') );

                  console.log("checkbox clicked");
                  $(element).trigger('checkboxChange');
                })

                // for both, disable propagation and remove when blur
                $(editorElement)
                .click(function(e){
                  // console.log('checkbox label, input and label, disable propagation for both');
                  e.stopPropagation();
                })
                .on('customBlur', function () {
                  console.log('custom blur checkbox');
                  editor._remove2();
                });
                break;
              //case 'checkbox':
              case "textfield":
                var editorElement = createInput( fieldType );
                this.editorElement = editorElement;
                this._addElement();

                // event handlers
                editorElement.focus().select()
                .click(function(e){
                  e.stopPropagation();
                })
                .keyup(function (e) {
                  console.log('defined inside element');
                  e.stopPropagation();
                  
                  // switch can reduce redundant event response
                  switch( e.which ) {
                    case keymap.ESC:
                      // reset value
                      console.log('ESC');
                      // reset value to previous
                      // true means reset value, 
                      // false means update
                      editor._remove( $(this) );
                      // restore original with ESC
                      // console.log($(this).val());
                      editor._setFieldValue( editor._originalValue );
                      editor._setElemValue( editor._originalValue ); // bind back
                      // $(element).find('.value').text( editor._originalValue );
                      // $(this).val( editor._originalValue ); // bind back

                      // not reset if not same as original
                      editor._chkModified( $(this).val() );
                      // $( element ).removeClass('modified');
                      break;
                    default:
                      // console.log("default");
                      editor._setStatus( 'modified' );
                      editor._setFieldValue( $(this).val() );
                      editor._chkModified( $(this).val() );
                      break;
                  }
                })
                .blur(function () {
                  console.log(editor._originalValue + ' textfield blur');
                  // editor._remove( editorElement );
                  editor._setPrevVal( editor._getOValue() ); // set the value to other related binded form objects
                  editor._remove2();
                })
                // this._position( editorElement );

                // console.log("add global event for textfield");
                // addEventForGlobal( element, editorElement );
                // addKeyEventToElement.apply( element )
                // addEventForGlobal2.apply( this, [element] ); // "this" is editor
                break;
              case "dropdown":
              var dropdownType = options.dropdown.type,
                  editor = this,
                  fieldStyle = element.data("field-style"),
                  editorElement = createSelect.apply( this ); // to read any variable in "editor"

              // console.log(editorElement);
              // for color selection
              if( fieldStyle === 'color') {
                console.log("color select");
                // must run before materialize and html update
                // console.log(element);
                this._originalValue = $( element ).find('.color-box').attr('title');
                // if( typeof $( element ).data('oval') === 'undefined') {
                //   $( element ).data('oval', this._originalValue);
                // }
                this._setOVal( this._originalValue );

                $( element ).find('.color-box.value').addClass('hidden');
                
                // update value
                if( typeof $.fn.material_select != 'undefined' ) {
                  console.log( 'materialize select for color');
                  // materialize it if available
                  editorElement.material_select_color();
                  editorElement = $( element ).parents('table').find('.select-wrapper.'+dropdownType);

                  // update data from materialized select to original display box
                  // editorElement.show();

                  $(element).append(editorElement); // up to this point is normal select
                  this._setElemValue( this._originalValue );
                  // updateDisplayField( editorElement.find('input.select-dropdown'), this._originalValue );

                  // prevent from propagating to parent element
                  editorElement.find(".select-dropdown").focus().select()
                  .on('click', function (e) {
                    // console.log("I am select dropdown");
                    e.stopPropagation();
                  })
                  // .on('customChange', function (e) {
                  //   console.log("I am dropdown color, changed!");
                    
                  // })
                  .on('customBlur', function (e) {
                    console.log("I am dropdown color, blurred!");
                    editor._remove2();
                  });

                  // when color is changed, copy to original
                  // ***if use mousedown/click together, will generate twice
                  editorElement.find('li')
                  .on('click', function (e) {
                    e.stopPropagation();
                    editor._setStatus( 'modified' );
                    editorElement.siblings('.color-box.value').attr('title', $(this).text());
                    editorElement.siblings('.color-box.value').attr('class', 'value color-box hidden ' + $(this).text());

                    editorElement.find("input.select-dropdown").trigger('fieldChange');

                    // console.log($(element).data('oval'));
                    editor._chkModified( $(this).text() );
                  });

                  // console.log("add global event for dropdown color");
                  // addEventForGlobal( element, editorElement );
                  this.editorElement = editorElement;
                  // addEventForGlobal2.apply( this, [element] );
                } else {
                // for browser select to update value
                }
                // because when dropdown is blur, it will cancel out the selection effect
                // .on('blur', function (e) {
                //   console.log("leaved me alone");
                //   // console.log(this);
                //   e.stopPropagation();
                //   $(element).html(editor._originalElement);
                // });

                // input box is the trigger point for materialized CSS select
                
              } else {
                // create normal materialcss select
                this._originalValue = $( element ).find('.value').text();
                // console.log(editorElement);

                console.log("create normal select");
                if( typeof $.fn.material_select != 'undefined' ) {
                  this._hideOriginalElement(); // store oval must be done before creation of new element

                  console.log( 'materialize select for normal');
                  // materialize it if available
                  editorElement.material_select();
                  var editorElement = $( element ).parents('table').find('.select-wrapper.'+dropdownType),
                      clickSelectItem = false;

                      console.log(dropdownType);
                      console.log($( element ).parents('table').find('.select-wrapper.'+dropdownType));

                  // $( element ).find('.value').addClass('hidden');
                  

                  // update data from materialized select to original display box

                  $(element).append(editorElement); // up to this point is normal select
                  // updateDisplayField( editorElement.find('input.select-dropdown'), this._originalValue );

                  // prevent from propagating to parent element
                  // console.log(editorElement.find("input.select-dropdown"));
                  editorElement.find("input.select-dropdown").focus().select()
                  .on('click', function (e) {
                    // console.log("I am select dropdown");
                    e.stopPropagation();
                  })
                  // .on('customChange', function (e) {
                  //   console.log("I am select dropdown, changed!");
                  //   // console.log(editor._getOValue());
                    
                  // })
                  .on('customBlur', function (e) {
                    console.log("I am select dropdown, blurred!");
                    editor._remove2();
                  });
                  
                  // when color is changed, copy to original
                  // ***if use mousedown/click together, will generate twice
                  editorElement.find('li')
                  .on('click', function (e) {
                    e.stopPropagation();
                    clickSelectItem = true;

                    editor._setStatus( 'modified' );
                    // console.log(editorElement.siblings());
                    editorElement.siblings('.value')
                    .text($(this).text());

                    // editorElement.find(".select-dropdown").trigger('customChange');
                    editorElement.find("input.select-dropdown").trigger('fieldChange');

                    // editorElement.siblings('.color-box.value').attr('title', $(this).text());
                    // editorElement.siblings('.color-box.value').attr('class', 'value color-box hidden ' + $(this).text());

                    // console.log($(element).data('oval')); // oval
                    if( !isModified( $(element).data('oval'), $(this).text()) ) {
                      $( element ).removeClass('modified');
                      $( element ).parents('tr').removeClass('modified');
                    }
                  });

                  console.log("add global event for dropdown normal");
                  // addEventForGlobal( element, editorElement );
                  this.editorElement = editorElement;
                  // addEventForGlobal2.apply( this, [element] );
                } else {
                // for normal select to update value
                }
              }

              

                // test
                // console.log($(editorElement).clone().addClass("test"));
                // $('.editable-container').append($(editorElement).clone().addClass("test"))
                // $('.test').material_select()
                // $('.editable-container .select-dropdown').on('click mousedown', function (e) {
                //   console.log(this);
                //   e.stopPropagation();
                // });

                // prevent weird UI response for normal select
                // editorElement.focus().select()
                // .on('click mousedown', function (e) {
                //   // console.log(this);
                //   e.stopPropagation();
                // });
                
                break;
              default:
                break;
            } // switch

            addKeyEventToElement.apply( element )
            addEventForGlobal2.apply( this, [element] ); // "this" is editor
          },
          // set value to input element
          _setElemValue: function( value ) {
            if( typeof value === 'undefined')
              return;

            if( typeof $.fn.material_select != 'undefined' ) {
              // for materialcss
              switch( this.fieldType ) {
                case 'dropdown':
                  if( this.fieldType === 'color' ) {
                    updateDisplayField( editorElement.find('input.select-dropdown'), value );
                  } else {

                  }
                  break;
                case 'textfield':
                  $( this.editorElement ).val( value );
                  break;
                case 'checkbox':
                  console.log('set checkbox initial');
                  console.log(value);
                  console.log(typeof value);
                  if( value === true || value === 'true' ) {
                    console.log('true?');
                    console.log(this.editorElement);
                    $( this.editorElement ).prop('checked', true)
                  } else {
                    $( this.editorElement ).prop('checked', false)
                  }
                  break;
              }
            } else {
              // normal browser input style
            }
          },
          /*
            get original field value before modified status
          */
          _getOValue: function() {
            // console.log( element.text() || element.find('.value').text() || element.find('.value').attr('title'));
            // console.log(element.text());
            // console.log(element.find('.value:eq(0)').text());
            // console.log(element.find('.value:eq(0)').attr('title'));
            var oval = element.find('.value:eq(0)').attr('title') || element.find('.value:eq(0)').text() || element.text();
            // var oval = element.text() || element.find('.value:eq(0)').text() || element.find('.value:eq(0)').attr('title');
            if( this.fieldType === 'dropdown') {
              return element.find('.value:eq(0)').text() || element.text();
            }
            if( this.fieldType === 'checkbox') {
              console.log('get check value');
              // console.log( oval );
              return this._getCheckValue( oval );
            } else {
              return oval;
            }
          },
          /*
            set original value to data as a tempoary value for checking in any related binded form element
          */
          _setPrevVal: function( val ) {
            $(element).data('_originalValue', val);
          },
          /*
            set original value to data for restoration when save is abandoned
          */
          _setOVal: function( val ) {
            if( typeof $( element ).data('oval') === 'undefined') {
              $( element ).data( 'oval', val );
              $( element ).attr( 'data-oval', val );
              // console.log("set ovalue");
              // console.log($(element).data());
            }
          },
          /*
            set original field value
          */
          _setFieldValue: function( value ) {
            console.log("set field value");
            switch( this.fieldType ) {
              case 'checkbox':
                console.log('_setCheckValue');
                // console.log(this.fieldType);
                // console.log( this._getCheckValue( value ) );
                $(element).find('.value').text( this._setCheckValue( value ) );
                $(element).find('.value').attr( 'title', value );
                break;
              case 'dropdown':
                
                break;
              default:
                $(element).find('.value').text( value );
                return;
                break;
            }
            // console.log("here?");
          },
          _getCheckValue: function( value ) {
            console.log(value);
            if( typeof value === 'undefined')
              return;

            if( value === 'done' || value === true || value === 'true' )
              return true;
            else
              return false;
          },
          _setCheckValue: function( value ) {
            if( typeof value === 'undefined')
              return;

            if( value === true || value === 'true' )
              return 'done';
            else
              return 'clear';
          },
          // _position: function( editorElement ){
          // },
          /* 
            for input object
            textfield
          */
          _addElement: function(){
            console.log('add element');
            // console.log(this._getOValue());
            // create original value container and hide original value
            this._hideOriginalElement();
            this._originalValue = this._getOValue();
            this._setOVal( this._originalValue );
            $(element).append( this.editorElement );
            this._setElemValue( this._originalValue );
            this._setStatus( 'active' );
            
            // console.log(this._originalValue);
            // console.log($(element).data());
            // $( this.editorElement ).val( this._originalValue );
            // this._bindValue( editorElement );
          },
          // plan for select object
          _addElementSelect: function( editorElement ) {
            
          },
          _setStatus: function( status ) {
            if( typeof status != 'undefined' && status === 'active') {
              // $(element).parent().find('.active').removeClass('active'); // done in restore
              $(element).addClass('active');

              // make row active
              $( element ).parents('tr').siblings().removeClass('active');
              $( element ).parents('tr').addClass('active');
              // console.log($( element ).parents('tr').addClass('active'));
            } else {
              $(element).addClass( status );
            }

            if( typeof status != 'undefined' && status === 'modified') {
              $( element ).addClass('modified');
              $( element ).parents('tr').addClass('modified');
            }
          },
          _unsetStatus: function( status ) {
            if( typeof status != 'undefined' && status === 'modified') {
              $( element ).removeClass('modified');
              $( element ).parents('tr').removeClass('modified');
            }
          },
          /*_bindValue: function( editorElement ) {
            // any bind operation here
            // console.log(element.text());
            // console.log($( editorElement ).val(element.text()));

            switch( this.fieldType ) {
              case "textfield":
                // save original value if no original data exist, destroy it when save
                if( typeof $( element ).data('oval') === 'undefined') {
                  $( element ).data('oval', element.text());
                }

                // for textfield
                this._originalValue = element.text();
                $( editorElement ).val(element.text())
                break;
              case "dropdown":
                // seems too redundant, refine the function later

                // for dropdown
                // save original value if no original data exist, destroy it when save

                // console.log(editorElement);
                break;
            }
          },*/
          _restoreField2: function(){
            $( element ).removeClass( 'active' );
            $( element ).find('.value.hidden').removeClass( "hidden" );
          },
          _hideOriginalElement: function(){
            console.log("hide original");
            // replacement is suitable to use in natural input DOM with focus/blur to fire immediately
            // hide/show is suitable to use in custom selection list that micmic select input
            // restore is placing at head for custom select object

          //   element.html('<span class=\"sr-only\">' + element.text() + '</span>');
            /*
              create container for original value
              it should be workable for any field
              textfield
                email
                password
              checkbox
              select dropdown
            */

            // store original value in TD element "data-oval"
            // consolidated to _setOVal()
            /*if( typeof $( element ).data('oval') === 'undefined') {
              // console.log("set ovalue");
              // console.log( this._getOValue() );
              $( element ).data( 'oval', this._getOValue() );
              // console.log($(element).data());
            }*/

            var $valEle = $(element).find('.value');
            // var $valEle = $(element).find('.value');
            if( $valEle.length === 0 ) {
              this._originalElement = $('<span class="value">'+element.text()+'</span>').addClass('hidden');
              $(element).html(this._originalElement);
            } else {
              $valEle.addClass('hidden')
            }
          },
          /*
            show element
            unhide input object if created
            create new if not exist
          */
          _remove: function ( editorElement ) {
            editorElement.remove();
            // console.log("editor is hidden, created input editor will not be removed for reusing");
            // this._restoreField( editorElement, resetValue );
            this._restoreField2();
            // editorElement.hide();
          },
          _remove2: function () {
            console.log('remove element ' + this.fieldType);
            triggerCustomEvents.apply( element, [this.fieldType] ); // tell the binder to do a customFieldChange after
            this.editorElement.remove();
            // console.log("editor is hidden, created input editor will not be removed for reusing");
            // this._restoreField( editorElement, resetValue );
            this._restoreField2();
            // editorElement.hide();
          }
        };

      element.editor = editor;
      element.keymap = keymap;

      // console.log(test);
      /*
      function(e){
                  // console.log(editor);
                  // console.log(bind);
                  console.log(e);
                  // bind(editor, "show");
                  return editor["show"].apply(editor,arguments);
                }
      */
      // without event: bind(editor, "show")
    element
    .on('click', function(e){
      // $( element ).parents('.table-container')
      // console.log(options.$parentLock);
      var $parentLock = options.$parentLock ? options.$parentLock : $( element ).parents('.table-container');
      console.log( options.$parentLock);
      console.log($( element ).parents('.table-container'));
      console.log( options.$parentLock.attr('id') === $( element ).parents('.table-container').attr('id') );

      if( options.$parentLock.attr('id') === $( element ).parents('.table-container').attr('id') ) {
        // console.log($.fn.makeEditable.defaultOptions);
        if( $.fn.makeEditable.editable ) {
          // trigger custom blur for custom element in other cell on the same table
          triggerCustomEvents.apply( element );

          // console.log( $( element ).parents('.table-container') );

          // console.log($( element ).parents('table').find('tbody tr td').length);
          return editor["_init"].apply( editor, arguments );
        }
        // else
          // return;
      }
        // return;
    })
    .keydown(function (e) {
      // e.preventDefault();
      // e.stopPropagation();
    });
  }); // each
}
$.fn.makeEditable.editable = false;
$.fn.makeEditable.setEditMode = function ( mode ) {
  $.fn.makeEditable.editable = mode ? mode : false;
}
$.fn.makeEditable.toggleEditMode = function () {
  if( $.fn.makeEditable.editable )
    $.fn.makeEditable.editable = false;
  else
    $.fn.makeEditable.editable = true
  return $.fn.makeEditable.editable;
}
$.fn.makeEditable.keyHold = false;


// Select Plugin
/*
  based on materialize CSS material_select, modify to create color option, not generic
*/
$.fn.material_select_color = function (callback) {
  $(this).each(function(){
    $select = $(this);

    // console.log( $select.siblings() );

    if ( $select.hasClass('browser-default')) {
      return; // Continue to next (return false breaks out of entire loop)
    }

    // Tear down structure if Select needs to be rebuilt
    var lastID = $select.data('select-id');
    if (lastID) {
      $select.parent().find('i').remove();
      $select.parent().find('input').remove();

      $select.unwrap();
      $('ul#select-options-'+lastID).remove();
    }

    // If destroying the select, remove the selelct-id and reset it to it's uninitialized state.
    if(callback === 'destroy') {
        $select.data('select-id', null).removeClass('initialized');
        return;
    }

    var uniqueID = Materialize.guid();
    $select.data('select-id', uniqueID);
    var wrapper = $('<div class="select-wrapper"></div>');
    wrapper.addClass($select.attr('class'));
    var options = $('<ul id="select-options-' + uniqueID+'" class="dropdown-content select-dropdown"></ul>');
    var selectOptions = $select.children('option');

    var label;
    if ($select.find('option:selected') !== undefined) {
      label = $select.find('option:selected');
    }
    else {
      label = options.first();
    }


    // Create Dropdown structure
    selectOptions.each(function () {
      // console.log($(this).text());
      // Add disabled attr if disabled
      options.append($('<li class="' + (($(this).is(':disabled')) ? 'disabled' : '') + '"><div class="color-box '+$(this).text()+'"><span class="sr-only">' + $(this).html() + '</span></div></li>'));
    });


    options.find('li').each(function (i) {
      var $curr_select = $select;
      $(this).click(function () {
        // console.log("click");
        // Check if option element is disabled
        if (!$(this).hasClass('disabled')) {
          $curr_select.find('option').eq(i).prop('selected', true);
          // Trigger onchange() event
          $curr_select.trigger('change');
          // console.log($curr_select);
          // console.log($curr_select.siblings('input.select-dropdown'));
          // console.log("new " +$(this).text());
          // console.log($curr_select.siblings());
          // console.log("prev " +$curr_select.siblings('input.select-dropdown').val());

          // $curr_select.siblings('input.select-dropdown').removeClass($curr_select.siblings('input.select-dropdown').val());
          // $curr_select.siblings('input.select-dropdown').addClass($(this).text());
          // $curr_select.siblings('input.select-dropdown').val($(this).text());
          // write a separate function for calling outside
          updateDisplayField( $curr_select.siblings('input.select-dropdown'), $(this).text() );
          // $curr_select.siblings('input.select-dropdown').removeClass($curr_select.siblings('input.select-dropdown').val());
          if (typeof callback !== 'undefined') callback();
        }
      });
    });

    updateDisplayField = function( displayBox, value ) {
      // console.log(displayBox);
      var curClassArr = displayBox.attr("class").split(" ").splice(0,2);
      curClassArr.push( value );
      displayBox.attr( "class", curClassArr.join(" "));

      // support input object and non-input object
      if( displayBox.is('input') || displayBox.is('select') ) {
        displayBox.val( value );
      } else {
        // displayBox.text( value ); // not necessary
      }
      // console.log(displayBox.is('input'));
    }

    // Wrap Elements
    $select.wrap(wrapper);
    // Add Select Display Element
    var dropdownIcon = $('<span class="caret">&#9660;</span>');
    if ( $select.is(':disabled') )
      dropdownIcon.addClass('disabled');
    var $newSelect = $('<input type="text" class="select-dropdown color-box '+label.text()+'" readonly="true" ' + (($select.is(':disabled')) ? 'disabled' : '') + ' data-activates="select-options-' + uniqueID +'" value="'+ label.html() +'"/>');
    $select.before($newSelect);
    $newSelect.before(dropdownIcon);
    // console.log($newSelect);

    $('body').append(options);
    // Check if section element is disabled
    if (!$select.is(':disabled')) {
      $newSelect.dropdown({"hover": false});
    }

    // Copy tabindex
    if ($select.attr('tabindex')) {
      $($newSelect[0]).attr('tabindex', $select.attr('tabindex'));
    }

    $select.addClass('initialized');

    $newSelect.on('focus', function(){
      $(this).trigger('open');
      label = $(this).val();
      selectedOption = options.find('li').filter(function() {
        // console.log($(this).text().toLowerCase());
        // console.log(label.toLowerCase());
        // console.log($(this).text().toLowerCase() === label.toLowerCase());
        return $(this).text().toLowerCase() === label.toLowerCase();
      })[0];

      // console.log("called");
      // console.log(selectedOption);
      activateOption(options, selectedOption);

      // console.log(options);
      // console.log(selectedOption);
    });

    $newSelect.on('blur', function(){
      $(this).trigger('close');
    });

    // Make option as selected and scroll to selected position
    activateOption = function(collection, newOption) {
      // console.log(collection);
      // console.log(newOption);
      collection.find('li.active').removeClass('active');
      $(newOption).addClass('active');
      collection.scrollTo(newOption);
    };

    // added for binding existing value to an external list
    // return the option
    findOptionByValue = function( collection, value ) {
      var foundOption = {};
      // based on its own template
      collection.find('li span').each(function(){
        console.log($(this).text() === value);
        if( $(this).text() === value ) {
          foundOption = $(this).parents('li')[0];
        }
      });
      return foundOption;
    }

    // updateSelect = function( collection, value ) {
    //   activateOption( collection, findOptionByValue( collection, 'rms-palette-grass' ) )
    //   updateDisplayField( $('.item-color-option.select-wrapper input.select-dropdown'), 'rms-palette-grass' );
    // }

    

    // Allow user to search by typing
    // this array is cleared after 1 second
    filterQuery = [];

    onKeyDown = function(event){
      // TAB - switch to another input
      if(event.which == 9){
        $newSelect.trigger('close');
        return;
      }

      // ARROW DOWN WHEN SELECT IS CLOSED - open select options
      if(event.which == 40 && !options.is(":visible")){
        $newSelect.trigger('open');
        return;
      }

      // ENTER WHEN SELECT IS CLOSED - submit form
      if(event.which == 13 && !options.is(":visible")){
        return;
      }

      event.preventDefault();

      // CASE WHEN USER TYPE LETTERS
      letter = String.fromCharCode(event.which).toLowerCase();
      var nonLetters = [9,13,27,38,40];
      if (letter && (nonLetters.indexOf(event.which) === -1)){
        filterQuery.push(letter);

        string = filterQuery.join("");

        newOption = options.find('li').filter(function() {
          return $(this).text().toLowerCase().indexOf(string) === 0;
        })[0];

        if(newOption){
          // console.log("called");
          activateOption(options, newOption);
        }
      }

      // ENTER - select option and close when select options are opened
      if(event.which == 13){
        activeOption = options.find('li.active:not(.disabled)')[0];
        if(activeOption){
          $(activeOption).trigger('click');
          $newSelect.trigger('close');
        }
      }

      // ARROW DOWN - move to next not disabled option
      if(event.which == 40){
        newOption = options.find('li.active').next('li:not(.disabled)')[0];
        if(newOption){
          activateOption(options, newOption);
        }
      }

      // ESC - close options
      if(event.which == 27){
        $newSelect.trigger('close');
      }

      // ARROW UP - move to previous not disabled option
      if(event.which == 38){
        newOption = options.find('li.active').prev('li:not(.disabled)')[0];
        if(newOption){
          activateOption(options, newOption);
        }
      }

      // Automaticaly clean filter query so user can search again by starting letters
      setTimeout(function(){ filterQuery = []; }, 1000);
    };

    $newSelect.on('keydown', onKeyDown);
  });
};

// Make option as selected and scroll to selected position
// trigger change in materialized CSS
// var activateOption = function(collection, newOption) {
//   collection.find('li.active').removeClass('active');
//   $(newOption).addClass('active');
//   collection.scrollTo(newOption);
// };

// var options = 

