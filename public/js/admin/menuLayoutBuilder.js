﻿$(function () {
    var $viewport = null,
        selectedFlow = [],
        seelctedPage = null;
    $.widget("wo.tileItem", {

        options: {
            type: "image",
            itemData: { "templateId": 1 },
            foodControl: false
        },

        //data: {
        //    itemData: null
        //},

        setData: function (data) {
            $.extend(this.data, data);
        },

        activate: function () {
            this._activate();
        },

        _create: function () {
            var that = this;
            //this.options.resetData = $.extend({}, this.options.itemData);
            if (!this.options.foodControl) {
                var panels = ['$settingsPanel', '$navigationPanel'];
                panels.forEach(function (panel) {
                    if (!that[panel].element) {
                        that[panel]._create();
                    }
                    else {
                        that[panel]._toggle(false);
                    }
                });
            }
            if (this.element.data("layout") && !this.element.data("layout").reset) {
                $.extend(this.element.data("layout"), { "reset": $.extend({}, this.element.data("layout")) });
            }
            console.log("_create", this.element.data("layout"));

            var data = this.element.data("layout"),
                dragging = false,

                $handle = $('<div />')
                .addClass("tile-handle tile-item-handle white"),

                //$toolRow = $('<div />')
                $toolRow = $('<div><a class="icon-btn settings-btn btn waves-effect waves-light"><i class="md-icon">settings</i></a><a class="icon-btn copy-btn btn light-green black-text waves-effect waves-light"><i class="md-icon">filter_none</i></a><a class="icon-btn reset-btn btn amber black-text waves-effect waves-light"><i class="md-icon">undo</i></a><a class="icon-btn next-page-btn btn"><i class="md-icon">forward</i></a><a class="icon-btn delete-btn btn red"><i class="md-icon">delete</i></a><span style="width: .6em; display: inline-block;"></span><a class="icon-btn close-btn btn red"><i class="md-icon">close</i></a></div>')
                .appendTo($handle),

                $foodControlHandle = $('<div />')
                .addClass("tile-handle tile-item-handle tile-item-handle-food-control white");
            if (!data) return;

            // init
            if (typeof data.templateId == "undefined") {
                data.templateId = this.options.itemData.templateId;
            }
            if (!data.template && typeof data.templateId != "undefined") {
                var templateList = $('body').data('templateList');
                var template = templateList.filter(function (x) { return x.templateId === data.templateId; });
                console.log('init template', data.templateId, template);
                if (template.length) {
                    $.extend(true, data, $.extend(true, {}, template[0]));
                }
            }
            console.log('_create', 'create virtual element start');
            if (!this.options.foodControl) {
                $toolRow
                    .find('.settings-btn')
                    .on("click", function (e) {
                        e.stopPropagation();
                        that._activate();
                        if (!that.options.foodControl) {
                            that.$navigationPanel._toggle(false);
                            that.$settingsPanel._toggle();
                            that.$settingsPanel._reposition();
                        }
                    });

                $toolRow
                    .find('.copy-btn')
                    .on("click", function (e) {
                        e.stopPropagation();
                        // widget event 'copy'
                        that._trigger("copy", null, that.element);
                    });

                $toolRow
                    .find('.reset-btn')
                    .on("click", function (e) {
                        e.stopPropagation();
                        if (confirm("Are you sure to RESET this item?")) {
                            that._reset();
                            Materialize.toast('Item is reset!', 2000)
                        }
                    });

                $toolRow
                    .find('.next-page-btn')
                    .on("click", function (e) {
                        e.stopPropagation();
                        that._activate();
                        if (!that.options.foodControl) {
                            that.$settingsPanel._toggle(false);
                            that.$navigationPanel._updateData();
                            that.$navigationPanel._toggle();
                        }
                    });

                $toolRow
                    .find('.delete-btn')
                    .on("click", function (e) {
                        e.stopPropagation();
                        if (confirm("Are you sure to REMOVE this item?")) {
                            // widget event 'delete'
                            that._trigger("delete", null, that.element);
                            that.element.remove();
                            if (!that.options.foodControl) {
                                that.$settingsPanel._toggle(false);
                            }
                            Materialize.toast('Item is removed!', 2000)
                        }
                    });

                $toolRow
                    .find('.close-btn')
                    .on('click', function (e) {
                        e.stopPropagation();
                        that._inactive();
                    });

                $handle.on("click", function (e) { e.stopPropagation(); });
            }
            else {
                if (data.type === "item") {
                    var $itemList = $('<select />')
                        .addClass('browser-default black-text')
                        .append($('<option />').attr('value', '').prop('disabled', 'disabled').text("更換餐品"))
                        .appendTo($foodControlHandle);

                    var itemList = [];
                    $('body').data('food').category.forEach(function (cat) {
                        cat.items.forEach(function (item) {
                            if (item.code === data.itemCode) {
                                itemList = cat.items;
                            }
                        });
                    });

                    itemList.forEach(function (item) {
                        $('<option />')
                            .attr('value', item.code)
                            .data('item', item)
                            .text(item.name1)
                            .appendTo($itemList);
                    });
                    $itemList.on('change', function () {
                        $.extend(data, { "itemCode": $(this).val(), "item": $(this).find('option:selected').data('item') });
                        $foodControlHandle.hide();
                        that._updateContent();
                        // widget event 'datachange'
                        that._trigger('datachange', null, data);
                    });

                    var triggerUpdateStocket = function (itemCode, type) {
                        // widget event 'updateStock'
                        that._trigger("updateStock", null, { "itemCode": itemCode, "type": type });
                    }
                    $('<button />')
                        .addClass('btn white black-text food-control food-control-prepare')
                        .text("準備")
                        .on('click', function (e) {
                            e.stopPropagation();
                            triggerUpdateStocket(data.itemCode, 'prepare');
                        })
                        .on('tap', function (e) {
                            e.stopPropagation();
                            triggerUpdateStocket(data.itemCode, 'prepare');
                        })
                        .appendTo($foodControlHandle);
                    $('<button />')
                        .addClass('btn white black-text food-control food-control-soldout')
                        .text("暫停")
                        .on('click', function (e) {
                            e.stopPropagation();
                            triggerUpdateStocket(data.itemCode, 'soldout');
                        })
                        .on('tap', function (e) {
                            e.stopPropagation();
                            triggerUpdateStocket(data.itemCode, 'soldout');
                        })
                        .appendTo($foodControlHandle);
                    $('<button />')
                        .addClass('btn blue food-control food-control-clear')
                        .text("恢復")
                        .on('click', function (e) {
                            e.stopPropagation();
                            triggerUpdateStocket(data.itemCode, 'clear');
                        })
                        .on('tap', function (e) {
                            e.stopPropagation();
                            triggerUpdateStocket(data.itemCode, 'clear');
                        })
                        .appendTo($foodControlHandle);

                    $('<span />')
                        .addClass('food-control-badge food-control-badge-prepare')
                        //.text("準備中")
                        .append($('<img />').attr("src", "/eMenuImages/flow_preparing.png"))
                        .appendTo(this.element)
                        .css({ 'visibility': 'hidden' });
                    $('<span />')
                        .addClass('food-control-badge food-control-badge-soldout')
                        //.text("暫停")
                        .append($('<img />').attr("src", "/eMenuImages/flow_soldout.png"))
                        .appendTo(this.element)
                        .css({ 'visibility': 'hidden' });
                }

                $foodControlHandle.on("click", function (e) { e.stopPropagation(); });
            }
            console.log('_create', 'create virtual element end');

            this.element
                .addClass("tile-item hoverable")
                .css({ "position": "absolute", "top": data.y, "left": data.x, "width": data.width, "height": data.height })
                .on("click", function (e) {
                    e.stopPropagation();
                    if (!that.options.foodControl || (that.options.foodControl && data.type === "item")) {
                        that._activate();
                        that._trigger("active", null, that.element);
                    }
                })
                .on('dblclick', function (e) {
                    e.stopPropagation();
                    that._trigger("navigate", null, that.element);
                });
            this.element.on('datachange', function (event, data) {
                console.log("datachange", data);
                $.extend($(this).data("layout"), data);
                that._update();
            });
            if (!this.options.foodControl) {
                this.element
                .draggable({
                    containment: "parent",
                    snap: true,
                    snapTolerance: 10,
                    //snapMode: "outer",
                    //handle: "div.tile-item-handle",
                    stop: function (event, ui) {
                        dragging = false;
                        console.log("stop", ui.helper.data("layout"));
                        $.extend(ui.helper.data("layout"), { "x": ui.position.left, "y": ui.position.top });
                        // widget event 'complete'
                        that._trigger("complete", null, { "x": ui.position.left, "y": ui.position.top });
                        if (!that.options.foodControl) {
                            that.$settingsPanel._updateData(ui.helper.data("layout"));
                        }
                        that.$settingsPanel._hover(false);
                        $(this).draggable("option", "snap", false);
                    },
                    drag: function (event, ui) {
                        that._activate();
                        if (event.ctrlKey) {
                            $(this).draggable("option", "snap", false);
                        } else {
                            $(this).draggable("option", "snap", true);
                        }
                    },
                    start: function (event, ui) {
                        dragging = true;
                        if (event.ctrlKey) {
                            $(this).draggable("option", "snap", false);
                        } else {
                            $(this).draggable("option", "snap", true);
                        }
                        event.stopPropagation();
                    }
                })
                .resizable({
                    containment: 'parent',
                    stop: function (event, ui) {
                        dragging = false;
                        $.extend(ui.helper.data("layout"), { "width": ui.size.width, "height": ui.size.height });
                        // widget event 'complete'
                        that._trigger("complete", null, { "width": ui.size.width, "height": ui.size.height });
                        if (!that.options.foodControl) {
                            that.$settingsPanel._updateData(ui.helper.data("layout"));
                        }
                    },
                    start: function (event, ui) {
                        dragging = true;
                        event.stopPropagation();
                    }
                })
                //.on('keydown', function (event) {
                //    if (event.shiftKey) {
                //        $(this).resizable("option", "aspectRatio", true);
                //    }
                //})
                //.on('keyup', function (event) {
                //    $(this).resizable("option", "aspectRatio", false);
                //})
                .append($handle);
            }
            else {
                this.element
                .append($foodControlHandle)
            }

            console.log("_create end");
            this._updateContent();
            console.log("_updateContent end");
        },

        _update: function () {
            var data = this.element.data("layout");
            this.element.css({ "top": data.y, "left": data.x, "width": data.width, "height": data.height });
            this._updateContent();
        },

        _renderTileTemplate: function (item, template) {
            if (!template) {
                template = {
                    "width": 800,
                    "height": 90,
                    "layouts": [
                        { "type": "text", "fontSize": 36, "mapping": "name1", "x": 200, "y": 0, "width": 500, "height": 60 },
                        { "type": "text", "fontSize": 18, "mapping": "name2", "x": 200, "y": 60, "width": 500, "height": 30 },
                        { "type": "image", "fontSize": 18, "x": 0, "y": 0, "width": 200, "height": 90 },
                        { "type": "text", "fontSize": 36, "mapping": "unitprice", "x": 700, "y": 0, "width": 100, "height": 90, "textAlign": "right" }
                    ]
                };
            }
            var that = this,
                $item = $('<div />')
                .css({ "position": "relative", "width": template.width, "height": template.height }),

                updateTemplateLayout = function ($l, l) {
                    if (l.align) {
                        //$text.css({ "textAlign": l.textAlign });
                        l.textAlign = l.align;
                        delete l.align;
                    }

                    if (l.mapping === "unitprice+optprice") {
                        l.mapping = "comboprice";
                    }

                    var fontWeight = "normal", fontStyle = "normal";
                    var lineHeight = (l.height / l.lines);

                    if (l.fontStyle && l.fontStyle.indexOf("bold") > -1) {
                        fontWeight = "bold";
                    }
                    if (l.fontStyle && l.fontStyle.indexOf("italic") > -1) {
                        fontStyle = "italic";
                    }

                    if (l.type === "optionlist") {
                        lineHeight = (l.fontSize + 4)
                    }

                    $l.css({
                        "position": "absolute",
                        "top": l.y,
                        "left": l.x,
                        "width": l.width,
                        "height": l.height,
                        "lineHeight": lineHeight + "px",
                        "color": l.fontColor,
                        "fontSize": l.fontSize,
                        "fontWeight": fontWeight,
                        "fontStyle": fontStyle,
                        "textAlign": l.textAlign,
                        "fontFamily": l.fontFamily
                    });

                    if (l.string) {
                        $l.find('.text-string').text(l.string);
                    }
                };

            template.layouts.forEach(function (l) {
                var $l = $('<div />')
                    .addClass('tile-template-item inner-border hoverable')
                    .data("templateLayout", l),

                    $c = $('<div />')
                    .addClass('tile-template-item-content')
                    .css({ "overflow": "hidden", "width": "100%", "height": "100%" }),

                    $handle = $('<div />')
                    .addClass("tile-handle tile-template-item-handle white"),

                    //$toolRow = $('<div />')
                    $toolRow = $('<div><a class="icon-btn settings-btn btn waves-effect waves-light"><i class="md-icon">settings</i></a><a class="icon-btn up-btn btn amber black-text waves-effect waves-light"><i class="md-icon">keyboard_arrow_up</i></a><span style="width: .6em; display: inline-block;"></span><a class="icon-btn close-btn btn red"><i class="md-icon">close</i></a></div>')
                    .appendTo($handle),

                    dragging = false;

                updateTemplateLayout($l, l);

                $toolRow.find('.settings-btn').bind('click', function (e) {
                    e.stopPropagation();

                    that.$settingsPanel._toggle();
                    that.$settingsPanel._reposition();
                });

                $toolRow.find('.up-btn').bind('click', function (e) {
                    e.stopPropagation();

                    var $parent = $(this).parents('.tile-item');
                    $parent.tileItem('activate');
                    console.log($parent);
                });

                $toolRow.find('.close-btn').bind('click', function (e) {
                    e.stopPropagation();

                    //that._inactive();
                    $l.removeClass('active');
                    $handle.hide();
                });

                if (l.fontSize)
                    $l.css({ "fontSize": l.fontSize });
                if (l.type === "text") {
                    if (l.mapping) {
                        var value = item[l.mapping];
                        console.log('_renderTileTemplate', item);
                        if (l.mapping === "unitprice") {
                            value = "$" + (parseInt(value) / 100).toFixed();
                        }
                        if (l.mapping === "comboprice") {
                            var optPirce = 0;
                            if (item.option) {
                                item.option.forEach(function (opt) {
                                    opt.items.forEach(function (optItem) {
                                        if ((optItem.unitprice > 0 && optItem.unitprice < optPirce) | optPirce == 0) {
                                            optPirce = optItem.unitprice;
                                        }
                                    });
                                });
                            }
                            value = "$" + ((parseInt(item["unitprice"]) + optPirce) / 100).toFixed();
                        }
                        var $text = $('<div />').addClass("text-mapping").text(value).appendTo($c);
                    }
                    else if (l.string) {
                        var $text = $('<div />').addClass("text-string").text(l.string).appendTo($c)
                    }
                }
                else if (l.type === "image") {
                    $('<img />')
                        .attr('src', './eMenuImages/' + item.code + '.png')
                        //.css({ "width": l.width, "height": l.height })
                        .css({ "maxWidth": "100%", "height": "auto" })
                        .addClass("tile-item-content-image")
                        .appendTo($c);
                }
                else if (l.type === "optionlist") {
                    console.log('optionlist', item, l);
                    if (item.option) {
                        item.option.forEach(function (opt) {
                            var $opt = $("<div />");
                            var text = "";
                            var optItemName = [];
                            opt.items.forEach(function (optItem) {
                                optItemName.push(optItem.name1);
                            });
                            if (l.replace) {
                                text = l.replace.replace("#options#", optItemName.join(l.separator));
                            }
                            else {
                                text = optItemName.join(l.separator);
                            }
                            $opt.text(text).appendTo($c);
                        });
                    }

                }
                if (!that.options.foodControl) {
                    $l
                        .bind('mouseenter', function () {
                            $(this).draggable("option", "disabled", false).resizable("option", "disabled", false)

                        })
                        .bind('mouseleave', function () {
                            if (!dragging)
                                $(this).draggable("option", "disabled", true).resizable("option", "disabled", true)
                        })
                        .bind('click', function (e) {
                            e.stopPropagation();
                            console.log('template item click');
                            that._inactive();
                            $('.tile-template-item').not(this).removeClass('active').find('.tile-template-item-handle').hide();;
                            $(this).addClass('active');
                            $handle.show();
                            that.$settingsPanel._link($l, "templateLayout");
                            that.$settingsPanel._reposition();
                        })
                        .on('datachange', function (event, l) {
                            console.log("temple layout datachange", l);
                            $.extend($(this).data("templateLayout"), l);
                            updateTemplateLayout($(this), $(this).data("templateLayout"));
                            //that._update();
                        })
                        .draggable({
                            //containment: "parent",
                            snap: true,
                            snapTolerance: 10,
                            disabled: true,
                            //snapMode: "outer",
                            //handle: "div.tile-item-handle",
                            stop: function (event, ui) {
                                dragging = false;
                                console.log("draggable sstop", ui.helper.data("templateLayout"));
                                $.extend(ui.helper.data("templateLayout"), { "x": ui.position.left, "y": ui.position.top });
                                //// widget event 'complete'
                                //that._trigger("complete", null, { "x": ui.position.left, "y": ui.position.top });
                                if (!that.options.foodControl) {
                                    that.$settingsPanel._updateData(ui.helper.data("templateLayout"));
                                    updateTemplateLayout(ui.helper, ui.helper.data("templateLayout"));
                                }
                                that.$settingsPanel._hover(false);
                                $(this).draggable("option", "snap", true);
                            },
                            drag: function (event, ui) {
                                //that._activate();
                                if (event.ctrlKey) {
                                    $(this).draggable("option", "snap", false);
                                } else {
                                    $(this).draggable("option", "snap", true);
                                }
                            },
                            start: function (event, ui) {
                                that.$settingsPanel._link(ui.helper, "templateLayout");
                                dragging = true;
                                if (event.ctrlKey) {
                                    $(this).draggable("option", "snap", false);
                                } else {
                                    $(this).draggable("option", "snap", true);
                                }
                                event.stopPropagation();
                            }
                        })
                        .resizable({
                            //containment: 'parent',
                            disabled: true,
                            stop: function (event, ui) {
                                dragging = false;
                                console.log("resizable stop", ui.helper.data("templateLayout"));
                                $.extend(ui.helper.data("templateLayout"), { "width": ui.size.width, "height": ui.size.height });
                                //// widget event 'complete'
                                //that._trigger("complete", null, { "width": ui.size.width, "height": ui.size.height });
                                if (!that.options.foodControl) {
                                    that.$settingsPanel._updateData(ui.helper.data("templateLayout"));
                                    updateTemplateLayout(ui.helper, ui.helper.data("templateLayout"));
                                }
                            },
                            start: function (event, ui) {
                                that.$settingsPanel._link($l, "templateLayout");
                                dragging = true;
                                event.stopPropagation();
                            }
                        });
                }
                $l.append($handle).append($c).appendTo($item);
            });
            return $item;

        },

        _updateContent: function () {
            var data = this.element.data("layout");

            if (!data) return;
            if (!this.element.find(".tile-item-content").length) {
                var $tileItemContent = $('<div />')
                    .addClass("tile-item-content")
                    .css({ "width": "100%", "height": "100%" })
                    .appendTo(this.element);
            }
            else {
                var $tileItemContent = this.element.find('.tile-item-content').empty();
            }
            switch (data.type) {
                case "item": {
                    if (!data.item) {
                        data.item = this._findItem(data.itemCode);
                    }
                    if (data.item) {
                        data.template = $.extend(true, {}, data.template);
                        var $item = this._renderTileTemplate(data.item, data.template);
                    }
                    else {
                        var $item = $('<div />')
                            .addClass('row')
                            .append($('<div />')
                                .addClass('col s12')
                                .append(
                                    $('<div />').addClass('heading1').text("Item not exists")
                                )
                            );
                    }
                    $tileItemContent
                        .append(
                            $('<div />')
                                .addClass("tile-item-content-item")
                                .append($item)
                                .css({ "width": "100%", "height": "100%" })
                        );
                    break;
                }
                case "image": {
                    $tileItemContent
                        .append(
                            $('<img />')
                                .addClass("tile-item-content-image lazy")
                                .attr("src", data.imageTC)
                                .css({ "width": "100%", "height": "100%" })
                        );
                    break;
                }
                case "text": {
                    console.log('data.fontSize', data.fontSize);
                    var fontWeight = "normal", fontStyle = "normal";
                    var lineHeight = (data.height / data.lines);

                    if (data.fontStyle && data.fontStyle.indexOf("bold") > -1) {
                        fontWeight = "bold";
                    }
                    if (data.fontStyle && data.fontStyle.indexOf("italic") > -1) {
                        fontStyle = "italic";
                    }

                    $tileItemContent
                    .css({ "textAlign": data.textAlign, "lineHeight": lineHeight + "px" })
                        .append(
                            $('<span />')
                                .addClass("tile-item-content-text")
                                .css({
                                    "fontSize": data.fontSize + "px",
                                    "lineHeight": lineHeight + "px",
                                    "white-space": "pre",
                                    "color": data.fontColor,
                                    "fontSize": data.fontSize,
                                    "fontWeight": fontWeight,
                                    "fontStyle": fontStyle,
                                    "textAlign": data.textAlign,
                                    "fontFamily": data.fontFamily
                                })
                                .text(data.text)
                        );
                    break;
                }
                case "pickup": {
                    $tileItemContent
                        .css({ "textAlign": "center" })
                        .append(
                            $('<span />')
                                .addClass("tile-item-content-text")
                                .css({ "textAlign": "center", "color": data.titleFontColor, "fontSize": (data.titleFontSize + 4) + "px", "lineHeight": (data.titleFontSize + 12) + "px" })
                                .text(data.title)
                        );
                    var number = data.row * data.col;
                    if (data.direction === "ttb") { // top to bottom
                        var $numberDisplay = $('<div />').addClass('row').css({ "display": "inline-block", "margin": "0 auto", "width": "auto", "height": "auto" });
                        for (i = 0; i < data.col; i++) {
                            var $row = $('<p />').addClass('col').css({ "margin": 0 });
                            var content = [];
                            for (j = 0; j < data.row; j++) {
                                content.push(pad(number, data.numberLength, "0"));
                                number--;
                            }
                            $numberDisplay.append(
                                $row.append(
                                        $('<span />').text(content.join('\n')).css({ "color": data.queueFontColor, "fontSize": (data.queueFontSize + 4) + "px", "lineHeight": (data.queueFontSize + 16) + "px" })
                                    )
                                );
                        }
                        $tileItemContent.append($numberDisplay);
                    }
                    else if (data.direction === "ltr") { // left to right
                        var $numberDisplay = $('<div />').addClass('row').css({ "width": "auto", "height": "auto", "text-align": "left" });
                        for (i = 0; i < data.row; i++) {
                            var $row = $('<p />').addClass('row').css({ "margin": 0 });
                            var content = [];
                            for (j = 0; j < data.col; j++) {
                                content.push(pad(number, data.numberLength, "0"));
                                number--;
                            }
                            $numberDisplay.append(
                                $row.append(
                                        $('<span />').text(content.join('\t\t\t')).css({ "color": data.queueFontColor, "fontSize": (data.queueFontSize + 4) + "px", "lineHeight": (data.queueFontSize + 16) + "px", "white-space": "pre", "tab-size": "1" })
                                    )
                                );
                        }
                        $tileItemContent.append($numberDisplay);
                    }
                    console.log("pickup")
                    break;
                }
                default: {
                    $tileItemContent
                        .append(
                            $('<div />')
                                .addClass("tile-item-content-empty")
                                .css({ "width": "100%", "height": "100%" })
                        );
                }
            }
        },

        _reset: function () {
            var data = $.extend(this.element.data("layout"), this.element.data("layout").reset);
            this._update();
            if (!this.options.foodControl) {
                this.$settingsPanel._reposition();
            }
        },

        _activate: function () {
            $('.tile-item .tile-template-item-handle').hide();
            $(".tile-item.active").not(this.element).removeClass("active").find(".tile-item-handle, .tile-item-handle-food-control").hide();
            this.element.addClass("active");

            if (!this.options.foodControl) {
                if (this.element.find(".tile-item-handle").is(":hidden"))
                    this.element.find(".tile-item-handle").show();
                this.$settingsPanel._link(this.element);
                this.$settingsPanel._reposition();
                //this.$navigationPanel._toggle(false);
                this.$navigationPanel._link(this.element);
            }
            else {
                this.element.find(".tile-item-handle-food-control").toggle(this.element.find(".tile-item-handle-food-control").is(":hidden"));
            }
        },

        _inactive: function () {
            $(".tile-item.active").removeClass("active").find(".tile-item-handle, .tile-item-handle-food-control").hide();
        },

        _findItem: function (code) {
            //var food = this.$settingsPanel.element.data("food"),
            var food = $('body').data("food"),
                matchItem = null;
            $.each(food.category, function (idx, cat) {
                $.each(cat.items, function (itemIdx, item) {
                    if (!matchItem && item.code === code) {
                        matchItem = item;
                    }
                });
            });
            return matchItem;
        },

        $settingsPanel: {
            element: null,

            target: null,

            type: "layout",

            _create: function () {
                var that = this,
                    $pageContainer = $('.page-container');

                $('.menu-layout-builder-viewport').on('click.sp', function (e) {
                    //console.log('viewport on click', e);
                    var target = $(e.target);
                    if (target.is(this))
                        that.element.hide();
                });

                this.element = $('#settings-panel')
                    .appendTo('.menu-layout-container');
                this.element.draggable({
                    handle: "div#settings-panel-handle"
                });
                this.element.find('.content-type').on('change', function () {
                    var typeClass = '.content-input-' + $(this).val()
                    that.element.find('.content-input').hide();
                    that.element.find('.content-input').not(typeClass).find('input, select').prop('disabled', true);
                    that.element.find(typeClass).show();
                    that.element.find(typeClass).find('input, select').prop('disabled', false);
                })
                this.element.find('.content-input').hide();
                var food = $('body').data("food"),
                    imageList = $('body').data("imageList"),
                    templateList = $('body').data('templateList'),
                    $foodCatSelect = this.element.find('select.food-category'),
                    $foodItemSelect = this.element.find('select.food-item'),
                    $imageSelect = this.element.find('.image-path'),
                    $tmplSelect = this.element.find('select.template-list'),
                    triggerDataChange = function (_o) {
                        // element event 'datachange', i.e. this.element.on('datachange')
                        //that.target.element.triggerHandler('datachange', [_o]);
                        that.target.triggerHandler('datachange', [_o]);
                        that._updateData(_o);
                        Materialize.toast('Item is saved!', 2000);
                        console.log('Item is saved!', _o);
                    };

                // initialize the "item-selector"
                $foodCatSelect.on('change', function (event) {
                    $foodItemSelect.empty().append('<option value="" disabled selected>Choose an item</option>');
                    var items = $(this).find(':selected').data("items");
                    $.each(items, function (idx, item) {
                        var $itemOpt = $('<option />').attr('value', item.code).data("item", item).text(item.name1);
                        if (that.target.data("layout") && that.target.data("layout").item && that.target.data("layout").item.code === item.code) {
                            $itemOpt.attr('selected', 'selected');
                        }
                        $foodItemSelect.append($itemOpt);
                    });
                });

                this.element.find('[type=color]').spectrum({
                    showInitial: true,
                    showInput: true,
                    preferredFormat: "hex",
                    hide: function (color) {
                        console.log("color", color.toHexString());
                        that.element.find('#font_color').val(color.toHexString());
                    }
                });

                this.element.find('select:not(.image-path)').material_select();

                this.element.find('.save-btn').on('click', function (e) {
                    e.stopPropagation();
                    //var _o = that.element.find('form').serializeObject();
                    var _o = $.extend(true, {}, that.target.data(that.type), that.element.find('form').serializeObject());
                    console.log('Before item be saved!', that.type, _o);
                    if (that.type === "layout") {
                        if (_o.type === "item") {
                            var selectedItem = $foodItemSelect.find('option:selected').data("item");
                            if (selectedItem) {
                                $.extend(_o, {
                                    "item": selectedItem,
                                    "itemCode": selectedItem.code
                                });
                            }
                            triggerDataChange(_o);
                        }
                        else if (_o.type === "image") {
                            var imageSize = { "width": 0, "height": 0 },
                                $imagePath = that.element.find('.image-path'),
                                imageTC = $imagePath.select2("val");

                            $.extend(_o, {
                                "imageTC": imageTC,
                                "image": $imagePath.select2("data")
                            });
                            triggerDataChange(_o);
                        }
                        else if (_o.type === "text" || _o.type === "pickup") {
                            triggerDataChange(_o);
                        }
                    }
                    else if (that.type === "templateLayout") {
                        triggerDataChange(_o);
                    }
                });

                this.element.find('.close-btn').on('click', function (e) {
                    e.stopPropagation();
                    that._toggle(false);
                });

                this.element.find('.template-apply-btn').on('click', function (e) {
                    e.stopPropagation();
                    var _o = $.extend(true, {}, that.target.data(that.type), that.element.find('form').serializeObject());
                    var selectedItem = $foodItemSelect.find('option:selected').data("item");
                    if (selectedItem) {
                        $.extend(_o, {
                            "item": selectedItem,
                            "itemCode": selectedItem.code
                        });
                    }
                    var templateId = parseInt($tmplSelect.val());
                    var tileTmpl = $('body').data("templateList").filter(function (x) { return x.templateId == templateId; });
                    if (tileTmpl.length) {
                        _o.width = tileTmpl[0].template.width;
                        _o.height = tileTmpl[0].template.height;
                        $.extend(_o, tileTmpl[0]);
                    }
                    triggerDataChange(_o);
                });

                this.element.find('.fit-image-size-btn').on('click', function (e) {
                    e.stopPropagation();
                    var _o = $.extend(true, {}, that.target.data(that.type), that.element.find('form').serializeObject());
                    var imageSize = { "width": 0, "height": 0 },
                        $imagePath = that.element.find('.image-path'),
                        imageTC = $imagePath.select2("val");

                    $.extend(_o, {
                        "imageTC": imageTC,
                        "image": $imagePath.select2("data")
                    });
                    console.log('fit-image', _o);
                    $("<img/>") // Make in memory copy of image to avoid css issues
                        .attr("src", imageTC)
                        .load(function () {
                            imageSize.width = this.width;   // Note: $(this).width() will not
                            imageSize.height = this.height; // work for in memory images.
                            var pageSize = { "width": $('.page-container').width(), "height": $('.page-container').height() };
                            console.log('pageSize', pageSize, 'imageSize', imageSize, pageSize.width < imageSize.width, pageSize.height < imageSize.height);
                            if (pageSize.width < imageSize.width || pageSize.height < imageSize.height) {
                                var originSize = { "width": imageSize.width, "height": imageSize.height };
                                if (imageSize.width > imageSize.height) {
                                    var newWidth = pageSize.width,
                                        newHeight = Math.round((imageSize.height / imageSize.width) * newWidth);
                                    imageSize.width = newWidth;
                                    imageSize.height = newHeight;
                                }
                                else {
                                    var newHeight = pageSize.height,
                                        newWidth = Math.round((imageSize.width / imageSize.height) * newHeight);
                                    imageSize.width = newWidth;
                                    imageSize.height = newHeight;
                                }
                                Materialize.toast('Image oversize! Resized from (' + originSize.width + ',' + originSize.height + ') to (' + imageSize.width + ',' + imageSize.height + ')', 6000);
                            }
                            $.extend(_o, imageSize);
                            triggerDataChange(_o);
                        })
                        .error(function () {
                            triggerDataChange(_o);
                        });
                });

                this.element.find('input[type=text]').on('change', function () {
                    Materialize.updateTextFields();
                });

            },

            _updateData: function (data) {
                if (!data) data = this.target.data(this.type);
                //console.log('_updateData',this.type, (this.type === "templateLayout" && typeof data.string != "undefined"), data);
                this.element.find('.layout-settings').toggle(this.type === "layout");
                this.element.find('.layout-settings').find('input, select').prop('disabled', this.type !== "layout" );
                this.element.find('.template-layout-settings').toggle((this.type === "templateLayout" && data.type !== "image") || data.type === "text");
                this.element.find('.template-layout-settings').find('input, select').prop('disabled', !((this.type === "templateLayout" && data.type !== "image") || data.type === "text"));
                this.element.find('.text-string').toggle((this.type === "templateLayout" && typeof data.string != "undefined")).find('input').prop('disabled', !(this.type === "templateLayout" && typeof data.string != "undefined"));
                this.element.find('input[type=number]').val(0);
                this.element.find('input[type=text]').val('');
                this.element.find('input[type=color]').val(this.target.css('color'));
                this.element.find('input[type=color]').spectrum("set", this.target.css('color'));
                this.element.find('select').val('');

                var that = this,
                    food = $('body').data("food"),
                    imageList = $('body').data("imageList"),
                    templateList = $('body').data('templateList'),
                    $foodCatSelect = this.element.find('select.food-category'),
                    $foodItemSelect = this.element.find('select.food-item'),
                    $imageSelect = this.element.find('.image-path'),
                    $tmplSelect = this.element.find('select.template-list');
                if (this.type === "layout") {
                    if ($foodCatSelect.find('option').length <= 1) {
                        $foodCatSelect.empty().append('<option value="" disabled selected>Choose a category</option>');
                        $.each(food.category, function (idx, cat) {
                            $foodCatSelect.append($('<option />').data("items", cat.items).attr('value', cat.code).text(cat.name1));
                        });
                    }
                    if (!$tmplSelect.find('option').length) {
                        $tmplSelect.empty();
                        templateList.forEach(function (template) {
                            $tmplSelect.append($('<option />').data('template', template.template).attr('value', template.templateId).text(template.templateName));
                        });
                    }
                    if (!$imageSelect.find('option').length) {

                        var formatImage = function (image) {
                            if (!image.id) return image.text;
                            //var $img = $('<img />').attr({ 'src': image.id }).css({ "display": "block", "maxWidth": "100%", "height": "auto" }); // dynamic height, fix width
                            var $img = $('<img />').attr({ 'src': image.id }).css({ "display": "block", "maxHeight": "100%", "width": "auto" }); // dynamic width, fix height
                            var $image = $('<span />')
                                //.prepend($('<div />').css({ "minHeight": "100", "width": "150" }).append($img))
                                .prepend($('<div />').css({ "height": "80" }).append($img))
                                .append($('<div />').css({ "wordWrap": "break-word" }).text(image.text));
                            //console.log("formatImage", $image.html())
                            return $image.html();
                        }
                        this.element.find('input.image-path').select2({
                            placeholder: "Select a image",
                            //dropdownAutoWidth: true,
                            data: imageList,
                            formatResult: formatImage,
                            escapeMarkup: function (m) { return m; }
                        });
                    }
                    this.element.find('select.content-type').val(data.type).change();
                    //$($foodCatSelect, $foodItemSelect, $imageSelect, 'input.text', 'input.link').val('');
                    $($foodCatSelect, $foodItemSelect, $imageSelect).val('');
                    $tmplSelect.val('1');


                    if (data.type && data.type === "item") {

                        var matchCategory = null;
                        $.each(food.category, function (idx, cat) {
                            $.each(cat.items, function (itemIdx, item) {
                                if (!matchCategory && item.code === data.itemCode) {
                                    matchCategory = cat;
                                }
                            });
                        });

                        if (matchCategory) {
                            $foodCatSelect.val(matchCategory.code).change();
                            // trigger food-category on change event
                        }

                        $tmplSelect.val(data.templateId);
                    }
                    else if (data.type && data.type === "image") {
                        //this.element.find('.image-path').val(data.imageTC).change();
                        if (data.image) {
                            $imageSelect.select2('data', data.image);
                        }
                        else if (data.imageTC) {
                            var temp = data.imageTC.split('/');
                            var imageName = temp[temp.length - 1];
                            $imageSelect.select2('data', { "id": data.imageTC, "text": imageName });
                        }
                    }
                }

                for (key in data) {
                    try {
                        this.element.find('[name=' + key + ']').val(data[key]);
                        if (this.element.find('[name=' + key + ']').attr("type") == "color") {
                            this.element.find('[name=' + key + ']').spectrum("set", data[key]);
                            console.log(key, data[key], this.element.find('[name=' + key + ']').spectrum("get"));
                        }
                    } catch (e) {
                        console.log(e);
                    }
                }

                // fix text input label overlay bug
                this.element.find('input[type=text]').change();
            },

            _reposition: function () {
                if (this.element.is(':hidden')) return;
                var $viewport = $('.menu-layout-builder-viewport:not(:hidden)'),
                    $container = $('.menu-layout-container'),
                    $leftSideBar = $('.menu-layout-builder-left-side-bar'),
                    //$handler = this.target.element.find('.tile-item-handle');
                    $handler = this.target.find('.tile-handle');
                //var elementBoundaryX = this.target.element.outerWidth(true) + this.target.element.offset().left + this.element.outerWidth(true) + 30;
                //var elementBoundaryY = this.target.element.outerHeight(true) + this.target.element.offset().top + this.element.outerHeight(true) - $('.toolbar-top').height();
                var elementBoundaryX = this.target.outerWidth(true) + this.target.offset().left + this.element.outerWidth(true) + 30;
                var elementBoundaryY = this.target.outerHeight(true) + this.target.offset().top + this.element.outerHeight(true) - $('.toolbar-top').height();
                var viewportBoundaryX = $viewport.width() + $viewport.scrollLeft();
                var viewportBoundaryY = $viewport.height() + $viewport.scrollTop();
                var containerMarginLeft = ($container.outerWidth(true) - $container.outerWidth()) / 2;
                if (containerMarginLeft < 0) containerMarginLeft = 0;
                var containerMarginTop = ($container.outerHeight(true) - $container.outerHeight()) / 2;
                if (containerMarginTop < 0) containerMarginTop = 0;
                var tmp_x = $handler.offset().left + $handler.outerWidth(true) - containerMarginLeft - $leftSideBar.width();
                var tmp_y = $handler.offset().top - $('.toolbar-top').height() - containerMarginTop;
                tmp_x += $viewport.scrollLeft();
                tmp_y += $viewport.scrollTop();
                if (tmp_x + this.element.outerWidth(true) > viewportBoundaryX) {
                    tmp_x = $handler.offset().left - this.element.outerWidth(true) - containerMarginLeft + $viewport.scrollLeft() - $leftSideBar.width();
                }
                console.log("tmp_y + this.element.outerHeight(true)", tmp_y + this.element.outerHeight(true));
                if (tmp_y + this.element.outerHeight(true) > viewportBoundaryY) {
                    tmp_y = $handler.offset().top + $handler.outerHeight(true) - this.element.outerHeight(true) - $('.toolbar-top').height() - containerMarginTop + $viewport.scrollTop();
                }

                this.element.css({
                    'left': tmp_x,
                    'top': tmp_y
                });
            },

            _link: function (target, type) {
                if (!type) type = "layout";
                this.type = type;
                if (this.target !== target) {
                    this.target = target;
                    //this.element.data({ "linkedItem": { "uuid": this.target.uuid } });
                    //this._updateData(this.target.element.data(type));
                    this._updateData(this.target.data(type));
                }
            },

            _toggle: function (display) {
                this.element.toggle(display);
            },

            _hover: function (display) {
                if (display)
                    this.element.addClass('hover');
                else
                    this.element.removeClass('hover');
            }
        },

        $navigationPanel: {
            element: null,

            target: null,

            _create: function () {
                var that = this;
                this.element = $('#navigation-panel')
                    .appendTo('.menu-layout-container');

                $('.menu-layout-builder-viewport').on('click.np', function (e) {
                    //console.log('viewport on click', e);
                    var target = $(e.target);
                    if (target.is(this))
                        that.element.hide();
                });
                this.element.find('.nav-type').on('change', function () {
                    var typeClass = '.page-nav-' + $(this).val()
                    that.element.find('.page-nav').hide();
                    that.element.find('.page-nav').not(typeClass).find('input, select').prop('disabled', true);
                    that.element.find(typeClass).show();
                    that.element.find(typeClass).find('input, select').prop('disabled', false);
                })

                this.element.find('.close-btn').on('click', function (e) {
                    e.stopPropagation();
                    that._toggle(false);
                });

                this.element.find('.page-nav-link-update-btn').on('click', function (e) {
                    e.stopPropagation();
                    var linkTag = $('input.link').val();
                    if (linkTag.length) {
                        var linkedPage = $('body').data('selectedFlow').pages.filter(function (x) { return x.tag === that.target.data('layout').link; });
                        if (linkedPage.length) {
                            //$.extend(linkedPage[0], { tag: linkTag });
                            linkedPage.forEach(function (p) {
                                $.extend(p, { tag: linkTag });
                            });
                        }
                        //$.extend(that.target.data('layout'), { link: linkTag });
                        $.extend(that.target.data('layout'), that.element.find('form').serializeObject());
                        that.target.triggerHandler("navigate", null, that.target);
                        //$viewport.tileItems("navigate", that.target.element.data('layout'));
                    }
                    that._toggle(false);
                });

                this.element.find('.page-nav-link-create-btn').on('click', function (e) {
                    e.stopPropagation();
                    var linkTag = $('input.link').val();
                    if (linkTag.length) {
                        //var linkedPage = $('body').data('selectedFlow').pages.filter(function (x) { return x.tag === linkTag; });
                        //var existsLink = $('body').data('selectedFlow').navigationBar.layout.buttons.filter(function (x) { return x.link === linkTag; });
                        //console.log('create', linkedPage, existsLink);
                        //if (linkedPage.length && existsLink.length) {
                        ////if (linkedPage.length) {
                        //    Materialize.toast('"' + linkTag + '" is not unique!', 2000);
                        //    return;
                        //}
                        var linkedPage = $('body').data('selectedFlow').pages.filter(function (x) { return x.tag === linkTag; });
                        if (linkedPage.length) {
                            linkedPage.forEach(function (p) {
                                $.extend(p, { tag: linkTag });
                            });
                        }
                        else {
                            $('body').data('selectedFlow').pages.push({ layout: [], tag: linkTag });
                        }
                        //$.extend(that.target.data('layout'), { link: linkTag });
                        $.extend(that.target.data('layout'), that.element.find('form').serializeObject());
                        //if (!$('body').data('selectedPage').tag || !$('body').data('selectedPage').tag.length) {
                        //    $.extend($('body').data('selectedPage'), { tag: linkTag });
                        //}
                        //else {
                        //    $('body').data('selectedFlow').pages.push({ layout: [], tag: linkTag });
                        //}
                        that.target.triggerHandler("navigate", null, that.target);
                        //$viewport.tileItems("navigate", that.target.element.data('layout'));
                    }
                    that._toggle(false);
                });

                this.element.find('.page-nav-link-unlink-btn').on('click', function (e) {
                    e.stopPropagation();
                    $.extend(that.target.data('layout'), { link: '', type: '' });
                    delete that.target.data('layout').link;
                    //var linkTag = $('input.link').val();
                    //if (linkTag.length) {
                    //    var linkedPage = $('body').data('selectedFlow').pages.filter(function (x) { return x.tag === that.target.data('layout').link; });
                    //    if (linkedPage.length) {
                    //        console.log('unlink', linkedPage);
                    //        linkedPage.forEach(function (p) { delete p });
                    //        $.extend(that.target.data('layout'), { link: '' });
                    //    }
                    //    //$viewport.tileItems("navigate", that.target.element.data('layout'));
                    //}
                    that._toggle(false);
                });

                this.element.find('.page-nav-next-layout-create-btn').on('click', function (e) {
                    e.stopPropagation();
                    $.extend(that.target.data('layout'), { 'nextLayout': { 'layout': [] } });
                    that.target.triggerHandler("navigate", null, that.target);
                    that._toggle(false);
                });

                this.element.find('.page-nav-next-layout-delete-btn').on('click', function (e) {
                    e.stopPropagation();
                    delete that.target.data('layout').nextLayout;
                    that._toggle(false);
                });

                this.element.find('.page-nav-category-link-btn').on('click', function (e) {
                    e.stopPropagation();
                    //var _o = $.extend(true, {}, that.target.data('layout'), that.element.find('form').serializeObject());
                    //console.log('link set', that.element.find('form').serializeObject());
                    var category = that.element.find('.page-nav-category-select').select2('val');
                    if (category.length) {
                        //$.extend(that.target.data('layout'), { 'category': category });
                        $.extend(that.target.data('layout'), that.element.find('form').serializeObject(), { 'category': category });
                    }
                    else {
                        delete that.target.data('layout').category;
                        that.target.data('layout').type = "";
                    }
                    that.target.triggerHandler("navigate", null, that.target);
                    that._toggle(false);
                });

                this.element.find('.page-nav-category-clear-btn').on('click', function (e) {
                    e.stopPropagation();
                    delete that.target.data('layout').category;
                    that.target.data('layout').type = "";
                    that.target.triggerHandler("navigate", null, that.target);
                    that._toggle(false);
                });

                this.element.find('input.link').autocomplete({
                    source: function (request, response) {
                        var data = [];
                        $('body').data('selectedFlow').pages.forEach(function (p) {
                            //var linked = $('body').data('selectedFlow').navigationBar.layout.buttons.filter(function (x) { return x.link && x.link === p.tag; });
                            if (p.tag && data.indexOf(p.tag) == -1) {
                                data.push(p.tag);
                            }
                        });
                        console.log("autocomplete", data, $('body').data('selectedFlow').navigationBar.layout.buttons, $('body').data('selectedFlow').pages);
                        response(data);
                    },
                    minLenght: 0
                });
            },

            _updateData: function (data) {
                if (!data) data = this.target.data("layout");
                this.element.find('input[type=number]').val(0);
                this.element.find('input[type=text]').val('');
                this.element.find('select').val('');

                console.log('data', data);
                var that = this,
                    food = $('body').data("food"),
                    $foodCatSelect = this.element.find('input.page-nav-category-select');

                //$('input.link').val('');

                var catData = [];
                food.category.forEach(function (cat) {
                    catData.push({ "id": cat.code, "text": cat.name1 });
                });
                $foodCatSelect.select2({
                    placeholder: "Choose a category",
                    data: catData,
                    multiple: true
                });

                this.element.find('select.nav-type').val(data.nav || "").change();
                $foodCatSelect.select2('val', '');

                if (data.category) {
                    $foodCatSelect.select2('val', data.category, true);
                }
                //$('.page-nav-next-layout').hide();
                //$('.page-nav-category').hide();
                //$('.page-nav-link').show();
                //$('.page-nav-link-unlink-btn').show();

                //$('input.link').val(data.link).change();
                if (!data.link || !data.link.length) {
                    $('.page-nav-link-create-btn').show();
                    $('.page-nav-link-update-btn').hide();
                }
                else {
                    $('.page-nav-link-create-btn').hide();
                    $('.page-nav-link-update-btn').show();
                }
                if (data.category) {
                    //$('.page-nav-link').hide();
                    $('.page-nav-category, .page-nav-category-clear-btn').show();
                }
                else {
                    //$('.page-nav-link').show();
                    //$('.page-nav-category').hide();
                }

                if (!data.link && !data.category) {
                    //$('.page-nav-link').show();
                    //$('.page-nav-category').show();
                    $('.page-nav-link-unlink-btn').hide();
                }

                for (key in data) {
                    try {
                        this.element.find('[name=' + key + ']').val(data[key]);
                        if (this.element.find('[name=' + key + ']').attr("type") == "color") {
                            this.element.find('[name=' + key + ']').spectrum("set", data[key]);
                            console.log(key, data[key], this.element.find('[name=' + key + ']').spectrum("get"));
                        }
                    } catch (e) {
                        console.log(e);
                    }
                }

                // fix text input label overlay bug
                this.element.find('input[type=text]').change();
            },

            _reposition: function () {
                var tmp_x = this.element.width() / 2;
                var tmp_y = this.element.height() / 2;

                this.element.css({
                    'marginLeft': -tmp_x,
                    'marginTop': -tmp_y
                });
            },

            _link: function (target, forceUpdate) {
                if (forceUpdate || this.target !== target) {
                    this.target = target;
                    //this.element.data({ "linkedItem": { "uuid": this.target.uuid } });
                    //this._updateData(this.target.element.data("layout"));
                    this._updateData(this.target.data("layout"));
                    this._reposition();
                }
            },

            _toggle: function (display) {
                this.element.toggle(display);
            }
        }
    });

    $.widget("wo.tileItems", {

        options: {
            flow: null,
            page: null,
            layout: null,
            parentLayout: [],
            category: null,
            highlighted: true,
            settings: {
                height: 800,
                width: 1280
            }
        },

        data: {
        },

        reload: function () {
            this._update();
        },

        setData: function (data) {
            $.extend(this.data, data);
            this._cleanup();
            this._update();
        },

        _create: function () {
            console.log('_create');
            $viewport = this.element;
            this._update();
        },

        //_setOption: function (key, value) {
        //    this.options[key] = value;
        //    this._cleanup();
        //    this._update();
        //},

        _setOptions: function () {
            console.log(arguments);
            this._superApply(arguments);
            this._update();
        },

        _update: function () {
            console.log(1);
            this.element.find('.tile-group').remove();
            selectedFlow = this.options.flow;
            selectedPage = this.options.page;
            var that = this,
                pageContainerWidth = this.options.settings.width,
                hasNavBar = this.options.flow.hasNavBar,
                navBarObj = this.options.flow.navigationBar,
                pagesObj = this.options.flow.pages,
                $navBar = $('<div />').addClass("nav-bar-container tile-group inner-border"),
                $page = $('<div />').addClass('page-container tile-group inner-border'),
                $back = $('<button />').addClass('btn nav-back-btn').text('Back'),
                $toolBar = $('<div />').addClass('page-tool-bar').append();

            if (hasNavBar && navBarObj) {
                var buttons = navBarObj.layout.buttons;

                $navBar.css({ "width": navBarObj.width, "height": this.options.settings.height });
                pageContainerWidth -= navBarObj.width;

                if (buttons) {
                    var navArr = buttons.filter(function (x) { return !x.isDelete; });
                    var $navArr = [];
                    $.each(navArr, function (btnIdx, btnObj) {
                        $navArr.push(
                            $("<div />")
                                .data("layout", btnObj)
                                .addClass("nav-bar-button tile-item highlight inner-border")
                            );
                    });
                    if ($navArr.length) {
                        $navBar.append($navArr);
                    }
                }
                $navBar.appendTo(this.element);
                $navBar.find('.tile-item').tileItem({
                    complete: function (event, data) {
                    },
                    navigate: function (event, element) {
                        that.navigate($(element).data("layout"));
                    },
                    copy: function (event, element) {
                        that._copyNavItem(element.data("layout"));
                    },
                    delete: function (event, element) {
                        $.extend($(element).data("layout"), { isDelete: true });
                    }
                })
            }
            //if (pagesObj) {
                $page.css({ "width": pageContainerWidth, "height": this.options.settings.height });
            //}
            if (this.options.layout && this.options.layout.length) {
                var layoutArr = this.options.layout.filter(function (x) { return !x.isDelete; });
                var $layoutArr = [];
                $.each(layoutArr, function (btnIdx, btnObj) {
                    $layoutArr.push(
                        $("<div />")
                            .data("layout", btnObj)
                            .addClass("page-button tile-item highlight inner-border")
                        );
                });
                if ($layoutArr.length) {
                    $page.append($layoutArr);
                }
            }
            else if (this.options.category && this.options.category.length) {
                $("<div />")
                    .addClass("category-page")
                    .appendTo($page)
                    .append($("<p />").addClass("md-display-3").text("Category"))
                    .append($("<p />").addClass("md-display-2").text(this.options.category));
            }
            $page.appendTo(this.element);
            $page.find('.tile-item').tileItem({
                complete: function (event, data) {
                },
                navigate: function (event, element) {
                    that.navigate($(element).data("layout"));
                },
                copy: function (event, element) {
                    that._copyTileItem(element.data("layout"));
                },
                delete: function (event, element) {
                    $.extend($(element).data("layout"), { isDelete: true });
                }
            });
            if (this.options.parentLayout.length) {
                $back.appendTo($page);
                $back.on('click', function (e) {
                    e.stopPropagation();
                    that.back();
                });
            }
            this._trigger('complete');
        },

        highlight: function () {
            this.options.highlighted = !this.options.highlighted;
            this._setHighlight();
        },

        navigate: function (data) {
            console.log("beginLayoutTransaction", data);
            if (data.link) {
                this.options.parentLayout = [];
                var selectedPage = this.options.flow.pages.filter(function (x) { return x.tag === data.link })[0];
                var selectedLayout = selectedPage.layout;
                var selectedCategory = null;
            }
            else if (data.nextLayout) {
                this.options.parentLayout.push(this.options.layout);
                var selectedPage = this.options.page;
                var selectedLayout = data.nextLayout.layout;
                var selectedCategory = null;
            }
            else if (data.category) {
                this.options.parentLayout.push(this.options.layout);
                var selectedPage = this.options.page;
                var selectedLayout = [];
                var selectedCategory = data.category;
            }
            if (selectedCategory || selectedLayout) {
                this._setOptions({ page: selectedPage, layout: selectedLayout, category: selectedCategory });
            }
        },

        back: function () {
            console.log('back');
            var resetObj = { category: null };
            if (this.options.parentLayout.length) {
                $.extend(resetObj, { layout: this.options.parentLayout.pop() });
            }
            this._setOptions(resetObj);
        },

        addTileItem: function (data) {
            this.options.layout.push(data);
            this._update();
        },
        
        _copyTileItem: function (data) {
            console.log("copyItem", data);
            var clonedItem = $.extend(true, {}, data);
            clonedItem.x += 20;
            clonedItem.y += 20;
            this.options.layout.push(clonedItem);
            this._update();
        },
                
        addNavItem: function (data) {
            this.options.flow.navigationBar.layout.buttons.push(data);
            this._update();
        },

        _copyNavItem: function (data) {
            console.log("copyItem", data);
            var clonedItem = $.extend(true, {}, data);
            clonedItem.x += 20;
            clonedItem.y += 20;
            this.options.flow.navigationBar.layout.buttons.push(clonedItem);
            this._update();
        },

        _setHighlight: function () {
            if (this.options.highlighted) {
                this.element.addClass("highlight");
                this.element.find(".tile-item").addClass("highlight");
            }
            else {
                this.element.removeClass("highlight");
                this.element.find(".tile-item").removeClass("highlight");
            }
        },

        _cleanup: function () {
            this.element.empty();
        }
    });
});

(function ($) {
    $.fn.serializeObject = function () {
        var that = this;
        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            console.log(this);
            var type = that.find('[name=' + this.name + ']').attr('type');
            this.value = this.value || '';
            if (type === 'number' && this.value.length && parseFloat(this.value) !== NaN) {
                this.value = parseFloat(this.value);
            }
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value);
            } else {
                o[this.name] = this.value;
            }
        });
        return o;
    };
})(jQuery);

$(function () {
    $('img.lazy').lazyload({
        effect: "fadeIn",
        failure_limit: 40
    });
    Array.prototype.move = function (old_index, new_index) {
        if (new_index >= this.length) {
            var k = new_index - this.length;
            while ((k--) + 1) {
                this.push(undefined);
            }
        }
        this.splice(new_index, 0, this.splice(old_index, 1)[0]);
        return this; // for testing purposes
    };
});

function pad(n, width, z) {
    z = z || '0';
    n = n + '';
    return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

var stickySidebar = function () {
    var h = $('.menu-layout-builder-side-bar').height() - $('.menu-layout-side-bar-header').height();
    $(".menu-layout-side-bar-form").height(h);
}

var getDayString = function (day) {
    //var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    //var days = ['日', '一', '二', '三', '四', '五', '六'];
    var days = { "1": "日", "2": "一", "3": "二", "4": "三", "5": "四", "6": "五", "7": "六", "excluding": "特別日子" };
    //return days[day - 1];
    return days[day];
}

var cleanupMenuLayout = function (data) {
    $.each(data.flow, function (idx, menulayout) {
        //$.extend(true, menulayout, $scope.baseFlow);
        if (!menulayout.timeRangeId) {
            $.extend(true, menulayout, { timeRangeId: 0 });
        }
        if (!menulayout.duration) {
            $.extend(true, menulayout, { duration: 0 });
        }
        if (!menulayout.seq) {
            $.extend(true, menulayout, { seq: -1 });
        }
        //if (menulayout.navigationBar && menulayout.navigationBar.layout.logo) {
        //    cleanupTileItem(menulayout.navigationBar.layout.logo);
        //}
        if (!menulayout.hasNavBar) {
            menulayout.navigationBar = null;
        }
        if (menulayout.navigationBar && menulayout.navigationBar.layout.buttons) {
            menulayout.navigationBar.layout.buttons.forEach(function (btn, btnIdx, object) {
                if (btn.isDelete) {
                    object.splice(btnIdx, 1);
                }
                else {
                    cleanupTileItem($.extend(btn, { isNav: true }));
                }
            })
        }
        if (menulayout.pages)
            menulayout.pages.forEach(function (page) {
                page.layout.forEach(function (btn, btnIdx, object) {
                    if (btn.isDelete) {
                        object.splice(btnIdx, 1);
                    }
                    else {
                        cleanupTileItem(btn);
                        cleanupNavLayout(btn);
                    }
                });
            });
    });
}

var cleanupNavLayout = function (data) {
    if (data.nextLayout) {
        data.nextLayout.layout.forEach(function (layout) {
            cleanupTileItem(layout);
            cleanupNavLayout(layout);
        });
    }
}

var cleanupTileItem = function (data) {
    console.log("start", $.extend(true, {}, data));
    if (data.isDelete) {
        delete data;
        console.log("end", "data deleted");
        return;
    }
    if (data.$$hashKey || data.$$hashKey == null) {
        delete data.$$hashKey;
    }
    if (data.reset || data.reset == null) {
        delete data.reset;
    }
    if (data.src) {
        $.extend(data, { "imageTC": data.src });
        delete data.src;
    }
    if (data.src == null) {
        delete data.src;
    }
    if (typeof data.nav == 'undefined') {
        if (data.link)
            data.nav = "link";
        if (data.category)
            data.nav = "category";
    }
    if (data.category && !$.isArray(data.category)) {
        data.category = [data.category];
    }
    if (data.filterKey && !$.isArray(data.filterKey)) {
        data.filterKey = data.filterKey.split(",");
    }
    if (data.item && !data.itemCode) {
        data.itemCode = data.item.code;
    }
    delete data.item;
    if (data.type === 'image' || data.isNav) {
        delete data.template;
        delete data.templateId;
        delete data.templateName;
        delete data.text;
        delete data.title;
    }
    if (data.type === "item") {
        delete data.text;
        delete data.title;
        delete data.image;
        delete data.imageTC;
    }
    if (data.type === 'text') {
        delete data.template;
        delete data.templateId;
        delete data.templateName;
        delete data.image;
        delete data.imageTC;
    }
    if (data.nav === "link") {
        delete data.category;
        delete data.filterKey;
        delete data.disableOthers;
    }
    if (data.nav === "category") {
        delete data.link;
    }
    console.log("end", $.extend(true, {}, data));
}