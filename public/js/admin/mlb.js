﻿$.widget("wo.tileItem", {

    options: {
        type: "image",
        itemData: null
    },

    data: {
        itemData: null
    },

    setData: function (data) {
        $.extend(this.data, data);
    },

    _create: function () {
        this.options.resetData = $.extend({}, this.options.itemData);
        if (!this.$settingsContainer.element) {
            this.$settingsContainer._create(this);
        } else {
            this.$settingsContainer._toggle(false);
        }
        if (this.element.data("layout") && !this.element.data("layout").reset) {
            $.extend(this.element.data("layout"), { "reset": $.extend({}, this.element.data("layout")) });
        }

        var that = this,
            data = $.extend(this.element.data("layout"), this.options.itemData),
            dragging = false,
            //$tileItemContent = $('<div />')
            //.addClass("tile-item-content")
            //.css({ "width": "100%", "height": "100%" }),

            //$itemImg = $('<img />')
            //.addClass("tile-item-content")
            //.attr("src", data.src)
            //.css({ "width": "100%", "height": "100%" }),

            $handle = $('<div />')
            .addClass("tile-item-handle white"),

            $toolRow = $('<div />')
            .addClass("row")
            .appendTo($handle);

        //if (data.src) {
        //    $tileItemContent.append($('<img />')
        //    .addClass("tile-item-content-image")
        //    .attr("src", data.src)
        //    .css({ "width": "100%", "height": "100%" })
        //    );
        //}
        //else {
        //    $tileItemContent.append($('<div />')
        //    .addClass("tile-item-content-empty")
        //    .css({ "width": "100%", "height": "100%" })
        //    );
        //}

        $('<div />')
            .addClass("col s3")
            .append(
                $('<a />')
                .addClass('icon-btn settings-btn btn')
                .append($("<i />").addClass("md-icon").text("settings"))
                .bind("click", function (event) {
                    event.stopPropagation();
                    that._activate();
                    that.$settingsContainer._toggle();
                })
            )
            .appendTo($toolRow);

        $('<div />')
            .addClass("col s3")
            .append(
                $('<a />')
                .addClass('icon-btn reset-btn btn')
                .append($("<i />").addClass("md-icon").text("replay"))
                .bind("click", function (event) {
                    event.stopPropagation();
                    if (confirm("Are you sure to RESET this item?")) {
                        that._reset();
                        Materialize.toast('Item is reset!', 2000)
                    }
                })
            )
            .appendTo($toolRow);

        $('<div />')
            .addClass("col s3")
            .append(
                $('<a />')
                .addClass('icon-btn next-page-btn btn')
                .append($("<i />").addClass("md-icon").text("forward"))
                .bind("click", function (event) {
                    event.stopPropagation();
                    that._trigger("navigate", null, that.element);
                    that.$settingsContainer._toggle(false);
                })
            )
            .appendTo($toolRow);

        $('<div />')
            .addClass("col s3")
            .append(
                $('<a />')
                .addClass('icon-btn delete-btn btn')
                .append($("<i />").addClass("md-icon").text("delete"))
                .bind("click", function (event) {
                    event.stopPropagation();
                    //var confirm = confirm("Are you sure to REMOVE this item?");
                    if (confirm("Are you sure to REMOVE this item?")) {
                        that._trigger("delete", null, that.element);
                        that.element.remove();
                        that.$settingsContainer._toggle(false);
                        Materialize.toast('Item is removed!', 2000)
                    }
                })
            )
            .appendTo($toolRow);

        $handle.bind("click", function (event) { event.stopPropagation(); });
        //if (!this.element.find(".tile-item-content")) {
        //    this.element
        //    .append($tileItemContent)
        //}
        this.element
            .addClass("tile-item hoverable")
            .css({ "position": "absolute", "top": data.y, "left": data.x, "width": data.width, "height": data.height })
            .append($handle)
            //.append($tileItemContent)
            .draggable({
                containment: "parent",
                snap: true,
                //snapMode: "outer",
                //handle: "div.tile-item-handle",
                stop: function (event, ui) {
                    dragging = false;
                    console.log(ui.helper.data("layout"));
                    $.extend(ui.helper.data("layout"), { "x": ui.position.left, "y": ui.position.top });
                    that._trigger("complete", null, { "x": ui.position.left, "y": ui.position.top });
                    that.$settingsContainer._updateData(ui.helper.data("layout"));
                },
                drag: function (event, ui) {
                    //that.$settingsContainer._reposition();
                    that._activate();
                    $.extend(ui.helper.data("layout"), { "x": ui.position.left, "y": ui.position.top });
                    that.$settingsContainer._updateData(ui.helper.data("layout"));
                },
                start: function (event, ui) {
                    dragging = true;
                    event.stopPropagation();
                }
            })
            .resizable({
                containment: 'parent',
                stop: function (event, ui) {
                    dragging = false;
                    $.extend(ui.helper.data("layout"), { "width": ui.size.width, "height": ui.size.height });
                    that._trigger("complete", null, { "width": ui.size.width, "height": ui.size.height });
                    that.$settingsContainer._updateData(ui.helper.data("layout"));
                },
                resize: function (event, ui) {
                    that._activate();
                    $.extend(ui.helper.data("layout"), { "width": ui.size.width, "height": ui.size.height });
                    that.$settingsContainer._updateData(ui.helper.data("layout"));
                },
                start: function (event, ui) {
                    dragging = true;
                    event.stopPropagation();
                }
            })
            .bind("click", function () {
                that._activate();
            })
            .bind("mousemove", function () {
                if (dragging)
                    that._activate();
            });
        //.bind("mouseleave", function () {
        //    if (!dragging)
        //        $handle.hide();
        //})
        //.data({ "layout": data });
        //console.log("reset data", this.element.data("reset"));
        //if (!this.element.data("reset")) {
        //    this.element.data({ "reset": $.extend({}, this.options.itemData) });
        //}
        this.element.on('datachange', function (event, data) {
            console.log("datachange");
            $.extend($(this).data("layout"), data);
            that._update();
        });

        this._updateContent();
    },

    _update: function () {
        var data = this.element.data("layout");
        this.element.css({ "top": data.y, "left": data.x, "width": data.width, "height": data.height });
        this._updateContent();
    },

    _updateContent: function () {
        var data = this.element.data("layout");
        if (!data) return;
        if (!this.element.find(".tile-item-content").length) {
            var $tileItemContent = $('<div />')
            .addClass("tile-item-content")
            .css({ "width": "100%", "height": "100%" })
            .appendTo(this.element);
            //console.log('test');
        }
        else {
            var $tileItemContent = this.element.find('.tile-item-content').empty();
        }
        //console.log('_updateContent', data);
        switch (data.type) {
            case "item": {
                var fontSize = 36;
                var $item = $('<div />')
                    .addClass('row')
                    .append($('<div />')
                        .addClass('col s8')
                        .append($('<div />').addClass('heading1').text(data.item.name1))
                        .append($('<div />').addClass('heading2').css({ "fontSize": "50%" }).text(data.item.name2))
                    )
                    //.append($('<div class="col s2" />')
                    //    .append($('<span />').addClass('price2').text(' '))
                    //)
                    .append($('<div />')
                        .addClass('col s2')
                        .addClass('offset-s2')
                        .css({ "textAlign": "center" })
                        .append($('<span />').addClass('price1').text((data.item.unitprice / 100).toFixed(1)))
                    )
                $tileItemContent.append($('<div />')
                .addClass("tile-item-content-item")
                .css({ "fontSize": fontSize })
                .append($item)
                .css({ "width": "100%", "height": "100%" })
                );
                break;
            }
            case "image": {
                $tileItemContent.append($('<img />')
                .addClass("tile-item-content-image")
                .attr("src", data.imageTC)
                .css({ "width": "100%", "height": "100%" })
                );
                break;
            }
            default: {
                $tileItemContent.append($('<div />')
                .addClass("tile-item-content-empty")
                .css({ "width": "100%", "height": "100%" })
                );
            }
        }
        //if (data.type === "item") {

        //}
        //else if (data.type === "image") {

        //}
    },

    _reset: function () {
        var data = $.extend(this.element.data("layout"), this.element.data("layout").reset);
        //this.element.css({ "top": data.y, "left": data.x, "width": data.width, "height": data.height });
        this._update();
        this.$settingsContainer._reposition();
    },

    _activate: function () {
        $(".tile-item.active").removeClass("active").find(".tile-item-handle").hide()
        this.element.addClass("active");
        this.element.find(".tile-item-handle").show();
        this.$settingsContainer._link(this);
        this.$settingsContainer._reposition();
    },

    //_reposition: function () {
    //    var data = this.element.data("layout");
    //    this.element.css({ "top": data.y, "left": data.x, "width": data.width, "height": data.height });
    //},

    $settingsContainer: {
        element: null,
        target: null,
        _create: function () {
            var that = this;
            //    this.element = $('<div id="settings-container" class="row card-panel hoverable white">\
            //  <form class="row">\
            //    <div class="row blue-grey lighten-5 z-depth-1 hoverable setting-panel-handle">\
            //        <div class="col right">\
            //            <a class="close-btn icon-btn btn red"><i class="md-icon">close</i></a>\
            //        </div>\
            //        <div class="col right">\
            //            <a class="save-btn icon-btn btn blue"><i class="md-icon">save</i></a>\
            //        </div>\
            //    </div>\
            //    <div class="row">\
            //        <div class="input-field col s3">\
            //          <input id="item_x" type="number" class="validate" placeholder="X coord">\
            //          <label for="item_x">X</label>\
            //        </div>\
            //        <div class="input-field col s3">\
            //          <input id="item_y" type="number" class="validate" placeholder="Y coord">\
            //          <label for="item_y">Y</label>\
            //        </div>\
            //        <div class="input-field col s3">\
            //          <input id="item_width" type="number" class="validate">\
            //          <label for="item_width">Width</label>\
            //        </div>\
            //        <div class="input-field col s3">\
            //          <input id="item_height" type="number" class="validate">\
            //          <label for="item_height">Height</label>\
            //        </div>\
            //      </div>\
            //    <div class="row">\
            //        <div class="col s4">\
            //            <label>Content Type</label>\
            //            <select class="browser-default content-type">\
            //              <option value="" disabled selected>Choose a content type</option>\
            //              <option value="item">Item</option>\
            //              <option value="image">Image</option>\
            //            </select>\
            //          </div>\
            //        <!--<div class="input-field col s8">\
            //            <select>\
            //              <option value="" disabled selected>Choose your option</option>\
            //              <option value="1">Option 1</option>\
            //              <option value="2">Option 2</option>\
            //              <option value="3">Option 3</option>\
            //            </select>\
            //            <label>Content</label>\
            //        </div>-->\
            //    </div>\
            //    <div class="row item-selector">\
            //        <div class="col s6">\
            //            <label>Category</label>\
            //            <select class="food-category browser-default">\
            //            </select>\
            //          </div>\
            //        <div class="col s6">\
            //            <label>Item</label>\
            //            <select class="food-item browser-default">\
            //              <option value="" disabled selected>Choose any category</option>\
            //            </select>\
            //        </div>\
            //    </div>\
            //    <!--<div class="row image-selector">\
            //        <div class="file-field input-field col s12">\
            //            <div class="btn">\
            //                <span>File</span>\
            //                <input type="file">\
            //            </div>\
            //            <div class="file-path-wrapper">\
            //                <input class="file-path validate image-path" type="text">\
            //            </div>\
            //        </div>\
            //    </div>-->\
            //    <div class="row image-selector">\
            //        <div class="input-field col s12">\
            //            <input id="item_image" type="text" class="image-path validate" placeholder="Image at server location. e.g. \"/eMenuImages/example.jpg\"">\
            //            <label for="item_image">Image Path</label>\
            //        </div>\
            //    </div>\
            //  </form>\
            //</div>')
            this.element = $('#settings-container')
                .css({ "zIndex": 500, "position": "absolute", "top": 0, "left": 0, "display": "none", "width": 420 })
            //.appendTo($("body"));
            this.element.draggable({
                handle: "div.setting-panel-handle"
            });
            this.element.find('.content-type').bind('change', function () {
                that.element.find('.image-selector, .item-selector').hide();
                that.element.find('.' + $(this).val() + '-selector').show();
            })
            this.element.find('.image-selector, .item-selector').hide();
            var food = this.element.data("food");

            // initialize the "item-selector"
            if (food) {
                this.element.find('select.food-category').empty().append('<option value="" disabled selected>Choose a category</option>');
                $.each(food.category, function (idx, cat) {
                    that.element.find('select.food-category').append($('<option />').data("items", cat.items).attr('value', cat.code).text(cat.name1));
                });
            }
            this.element.find('select.food-category').bind('change', function (event) {
                that.element.find('select.food-item').empty().append('<option value="" disabled selected>Choose an item</option>');
                var items = $(this).find(':selected').data("items");
                //console.log("items", items);
                $.each(items, function (idx, item) {
                    var $itemOpt = $('<option />').attr('value', item.code).data("item", item).text(item.name1);
                    if (that.target.element.data("layout") && that.target.element.data("layout").item && that.target.element.data("layout").item.code === item.code) {
                        $itemOpt.attr('selected', 'selected');
                    }
                    that.element.find('select.food-item').append($itemOpt);
                });
            })

            this.element.find('select').material_select();

            this.element.find('.save-btn').bind('click', function () {
                var _o = {
                    "x": parseInt(that.element.find("#item_x").val()),
                    "y": parseInt(that.element.find("#item_y").val()),
                    "width": parseInt(that.element.find("#item_width").val()),
                    "height": parseInt(that.element.find("#item_height").val()),
                    "type": that.element.find('.content-type').val()
                };
                if (_o.type === "item") {
                    $.extend(_o, { "item": that.element.find('select.food-item option:selected').data("item") });
                }
                else if (_o.type === "image") {
                    $.extend(_o, { "imageTC": that.element.find('.image-path').val() });
                }
                //$.extend(that.target.element.data("layout"), _o);
                //that.target._reposition();
                that.target.element.triggerHandler('datachange', [_o]);
                //console.log(that.target.element);
                Materialize.toast('Item is saved!', 2000);
            });

            this.element.find('.close-btn').bind('click', function () {
                that._toggle(false);
            });

            this.element.find('input[type=text]').bind('change', function () {
                Materialize.updateTextFields();
                ////console.log("change", $(this).val());
                //if ($(this).val().length)
                //    $(this).siblings('label').addClass("active")
                //else {
                //    $(this).siblings('label').removeClass("active")
                //}
            });

        },

        _updateData: function (data) {
            var that = this,
                food = this.element.data("food");
            if (this.element.find('select.food-category option').length <= 1) {
                this.element.find('select.food-category').empty().append('<option value="" disabled selected>Choose a category</option>');
                $.each(food.category, function (idx, cat) {
                    that.element.find('select.food-category').append($('<option />').data("items", cat.items).attr('value', cat.code).text(cat.name1));
                });
            }

            this.element.find('#item_x').val(data.x).change();
            this.element.find('#item_y').val(data.y).change();
            this.element.find('#item_width').val(data.width).change();
            this.element.find('#item_height').val(data.height).change();
            this.element.find('select.content-type').val(data.type).change();
            this.element.find('select.food-category, select.food-item, .image-path').val('')
            //this.element.find('select.food-category').val('');
            //this.element.find('select.food-item').val('');
            if (data.type && data.type === "item") {

                var matchCategory = null;
                $.each(food.category, function (idx, cat) {
                    $.each(cat.items, function (itemIdx, item) {
                        if (!matchCategory && item.code === data.item.code) {
                            matchCategory = cat;
                        }
                    });
                });

                if (matchCategory) {
                    this.element.find('select.food-category').val(matchCategory.code).change();
                }
            }
            else if (data.type && data.type === "image") {
                this.element.find('.image-path').val(data.imageTC);
            }
        },

        _reposition: function () {
            //var patch_id = (typeof patch_id === 'undefined') ? current_selected_patch_id : patch_id;
            //console.log("reposition", this.target);
            var tmp_x = this.target.element.width() + this.target.element.offset().left + 10;
            //var tmp_x = this.target.element.offset().left - this.element.width() - 5;
            var tmp_y = this.target.element.offset().top - 50;

            this.element.css({
                'left': tmp_x,
                'top': tmp_y
            });

            this._updateData(this.target.element.data("layout"));
        },

        _link: function (target) {
            this.target = target;
            this.element.data({ "linkedItem": { "uuid": this.target.uuid } });
            this._updateData(this.target.element.data("layout"));
        },

        _toggle: function (display) {
            this.element.toggle(display);
        }
    }
});

$.widget("wo.tileGroup", {

    options: {
        period: null,
        layout: null,
        categoryId: null,
        highlighted: true
    },

    data: {
    },

    setData: function (data) {
        $.extend(this.data, data);
        this._cleanup();
        this._update();
    },

    _create: function () {
        //this.options.layout = this.options.period.pages[0].layout;
        this._update();
    },

    _setOption: function (key, value) {
        this.options[key] = value;
        this._cleanup();
        this._update();
    },

    _update: function () {
        var that = this,
            settings = {
                height: 800,
                width: 1280,
                backgroundColor: "#212121",
                color: "grey"
            },
            pageContainerWidth = settings.width,
            navBarObj = this.options.period.navigationBar,
            pagesObj = this.options.period.pages,
            $container = $('.menu-layout-container').css(settings),
            $page = $('<div />').addClass("page-container tile-group");

        if (navBarObj) {
            var $navBar = $('<div />')
                .addClass("nav-bar-container tile-group")
                .css({ "width": navBarObj.width, "height": settings.height }),
                logoObj = navBarObj.layout.logo,
                buttons = navBarObj.layout.buttons;
            pageContainerWidth -= navBarObj.width;

            if (logoObj) {
                $("<div />").addClass("nav-bar-logo")
                    //.tileItem()
                    //.tileItem("setData", { "itemData": logoObj })
                    .tileItem({
                        itemData: logoObj,
                        complete: function (event, data) {
                            console.log("Callbacks are great!");
                            $.extend(logoObj, data);
                            that._trigger("complete", null, that.options.period);
                        },
                        delete: function (event, element) {
                            console.log("delete!", element);
                            logoObj.isDelete = true;
                        }
                    })
                    .data({ "layout": logoObj, "reset": $.extend({}, logoObj) })
                    .appendTo($navBar);
            }
            if (buttons) {
                $.each(buttons, function (btnIdx, btnObj) {
                    $("<div />")
                        .addClass("nav-bar-button")
                        //.tileItem()
                        //.tileItem("setData", { "itemData": btnObj }) // edit the origin object
                        .tileItem({
                            itemData: btnObj,
                            complete: function (event, data) {
                                console.log("Callbacks are great!");
                                $.extend(btnObj, data);
                                that._trigger("complete", null, that.options.period);
                            },
                            delete: function (event, element) {
                                console.log("delete!", element);
                                btnObj.isDelete = true;
                            }
                        })
                        .data({ "layout": btnObj, "reset": $.extend({}, btnObj) })
                        .appendTo($navBar)
                        .bind("click", function () {
                            var pageFilter = pagesObj.filter(function (x) { return x.tag === btnObj.link; });
                            if (pageFilter) {
                                //that.setData({ "layout": pageFilter[0].layout, "categoryId": null  });
                                that._setOptions({ "layout": pageFilter[0].layout, "categoryId": null });
                            }
                        });
                });
            }
            $container.append($navBar);
        }
        if (pagesObj) {
            $page.css({ "width": pageContainerWidth, "height": settings.height })
                .appendTo($container);
        }
        if (this.options.layout) {
            var layoutArr = this.options.layout.filter(function (x) { return !x.isDelete; });
            $.each(layoutArr, function (btnIdx, btnObj) {
                $("<div />")
                .addClass("page-button")
                //.tileItem()
                //.tileItem("setData", { "itemData": btnObj }) // edit the origin object
                .tileItem({
                    itemData: btnObj,
                    complete: function (event, data) {
                        console.log("Callbacks are great!");
                        $.extend(btnObj, data);
                        that._trigger("complete", null, that.options.period);
                    },
                    delete: function (event, element) {
                        console.log("delete!", element);
                        btnObj.isDelete = true;
                    }
                })
                .data({ "layout": btnObj, "reset": $.extend({}, btnObj) })
                .appendTo($page)
                .bind("click", function () {
                    if (btnObj.nextLayout) {
                        //that.setData({ "layout": btnObj.nextLayout.layout, "categoryId": null });
                        that._setOptions({ "layout": btnObj.nextLayout.layout, "categoryId": null });
                    }
                    else if (btnObj.category) {
                        //that.setData({ "layout": null, "categoryId": btnObj.category });
                        that._setOptions({ "layout": null, "categoryId": btnObj.category });
                    }
                });
            });
        }
        else if (this.options.categoryId) {
            $("<div />")
            .addClass("category-page")
            .appendTo($page)
            .append($("<p />").addClass("md-display-3").text("Category"))
            .append($("<p />").addClass("md-display-2").text(this.options.categoryId));
        }
        this._setHighlight();
    },

    highlight: function () {
        this.options.highlighted = !this.options.highlighted;
        this._setHighlight();
    },

    _setHighlight: function () {
        if (this.options.highlighted) {
            this.element.addClass("highlight");
            this.element.find(".tile-item").addClass("highlight");
        }
        else {
            this.element.removeClass("highlight");
            this.element.find(".tile-item").removeClass("highlight");
        }
    },

    _cleanup: function () {
        this.element.empty();
    }
});

var obj = {
    flow: {},
    reset: {},
    change: false
},
    socket = io.connect('http://' + location.hostname + ':5000');
socket.on('news', function (data) {
    console.log(data);
    socket.emit('my other event', { my: 'data' });
});
socket.on('connect', function () {
    console.log('connected');
});
$.getJSON("/data/layoutFlow2.json", function (data) {
    console.log("loaded", data);
    obj.flow = data.flow;
    obj.reset = $.extend({}, data.flow);
    console.log(obj.flow == obj.reset);
    //$('.menu-layout-container').tileGroup({
    //    period: obj.flow[0],
    //    layout: obj.flow[0].pages[0].layout,
    //    complete: function (event, data) {
    //        console.log("init complete!");
    //    }
    //});
    ////$('.menu-layout-container').tileGroup()
    ////    .tileGroup("setData", { period: obj.flow[0], layout: obj.flow[0].pages[0].layout }); // edit the origin object
    var $accordion = $("<div />");
    var $sideBar = $(".menu-layout-builder-side-bar");
    $.each(obj.flow, function (periodIdx, period) {
        $("<h3 />").text(period.time)
            .appendTo($accordion)
            .bind("click", function () {
                $('.menu-layout-container')
                    //.tileGroup("setData": { period: period, layout: period.pages.filter(function (x) { return x.tag === period.navigationBar.layout.buttons[0].link })[0].layout }); // edit the origin object
                    .tileGroup({
                        period: period,
                        layout: period.pages.filter(function (x) { return x.tag === period.navigationBar.layout.buttons[0].link })[0].layout,
                        complete: function (event, data) {
                            console.log("change  complete!");
                        }
                    });
            });

        var $detail = $("<div />")
            .attr("layout", "column")
            .addClass("md-inline-form")
        //.append($("<p/>").text(period.time))


        $("<div />").appendTo($accordion).append($detail)
            .addClass("menu-layout-detail")

        $("<md-content layout-padding />")
            .appendTo($detail)
            .append($("<form />")
                .addClass("menu-layout-detail-form")
                .append('<div layout layout-sm="column"> \
        <md-input-container flex> \
          <label>First name</label>\
          <input ng-model="user.firstName">\
        </md-input-container>\
      </div>\
        <div layout layout-sm="column"> \
        <md-input-container flex>\
          <label>Last Name</label>\
          <input ng-model="theMax">\
        </md-input-container>\
      </div>')
            );
        //$accordion
        ////.attr("layout", "row")
        //.append($("<h3 />").text(period.time))
        //.append()
    });
    $("<md-sidenav flex />")
        .addClass("menu-layout-period-list md-sidenav-right md-locked-open")
    //.appendTo($sideBar).append($accordion)
    $accordion
        .accordion({
            animate: false,
            heightStyle: "fill",
            collapsible: true
        })
    //.appendTo($sideBar)
    stickySidebar();
    $(window).resize(function () {
        stickySidebar();
    })
});
var stickySidebar = function () {
    var h = $('.menu-layout-builder-side-bar').height() - $('.menu-layout-side-bar-header').height();
    $(".menu-layout-side-bar-form").height(h);
}

var getDayString = function (day) {
    //var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    var days = ['日', '一', '二', '三', '四', '五', '六'];
    return days[day - 1];
}