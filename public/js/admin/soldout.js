//a = [{ "image_filename": "/files/signage/media/image/fairwood/chinese_s2.jpg", "patch_box": [{ "patch_image": "/files/signage/media/image/patch_image/Not_Available_Icon_01.png", "x": 237, "y": 0, "w": 176, "h": 176 }] }];

//socket.emit('generaterule', a);

//socket.emit('boxstatuschangerequest', {});
//socket.emit('applyrule', [{ "img": "chinese_s2.jpg" }]);


//a = [{ "image_filename": "/files/signage/media/image/fairwood/chinese_s2.jpg", "patch_box": [] }];

var socket;
//var configFile = 'files/signage/media/fairwood_demo/config.xml';
var configFile = 'data/food.json';

var app = angular.module('fdm', ['ngTouch']);
app.config(function ($locationProvider, $provide, $httpProvider) {
    $locationProvider.html5Mode({ enabled: true, requireBase: false, rewriteLinks: true });
    $provide.constant('INFO', INFO);
    $httpProvider.useApplyAsync(true); // put all http request into one digest cycle for speed optimization
});

app.factory('SocketFactory', function ($rootScope, $timeout, INFO) {
    var socketConnection = 'http://' + location.hostname + ':' + INFO.socketPort;
    var socket = io.connect(socketConnection);
    io.Manager(socketConnection, { reconnect: true });
    var asyncAngularify = function (socket, callback) {
        return callback ? function () {
            var args = arguments;
            $timeout(function () {
                callback.apply(socket, args);
            }, 0);
        } : angular.noop;
    };
    return {
        on: function (eventName, callback) {
            //console.log(eventName);
            socket.on(eventName, function () {
                var args = arguments;
                $rootScope.$apply(function () {
                    callback.apply(socket, args);
                });
            });
            /*
            socket.io.on("connect_error", function (e) {
                console.log('socket on connect_error');
            })
            socket.on('connect_failed', function (e) {
                console.log('socket on connect_failed');
                console.log(e);
            });
            socket.on('disconnect', function (e) {
                console.log('socket on disconnect');
                console.log(e);
            });
            socket.on('error', function (e) {
                console.log('socket on error');
                console.log(e);
            });
            */
        },
        emit: function (eventName, data, callback) {
            //console.log(eventName);
            var lastIndex = arguments.length - 1;
            var callback = arguments[lastIndex];
            if (typeof callback == 'function') {
                callback = asyncAngularify(socket, callback);
                arguments[lastIndex] = callback;
            }
            return socket.emit.apply(socket, arguments);
        }
    };
});
app.controller('FDMController', function (SocketFactory, INFO, $scope, $window, $http, $q) {
    SocketFactory.on('connect', function (msg) {
        console.log('receive');
    });
    SocketFactory.on('refreshItemQty', function (data) {
        console.log('refreshItemQty', data)
        obj.stock.updateItem(JSON.parse(data));
    });
    var obj = {};
    obj.foodPromise = $http.get('data/food.json');
    obj.loadStockPromise = function () {
        var deferred = $q.defer();
        SocketFactory.emit('loadItemQty', '', function (r) {
            r = JSON.parse(r);
            deferred.resolve(r);
        });
        return deferred.promise;
    }
    obj.loadMenuLayoutPromise = function () {
        var deferred = $q.defer();
        SocketFactory.emit('getCurrentMenuLayout', {}, function (r) {
            console.log('listMenuLayout', r);
            deferred.resolve(r);
        });
        return deferred.promise;
    }
    obj.promise = $q.all([obj.foodPromise, obj.loadStockPromise(), obj.loadMenuLayoutPromise()]).then(function (values) {
        console.log(values);
        $scope.food = values[0].data;
        obj.stock.updateItem(values[1]);
        $scope.menuLayoutList = values[2];
        delete $scope.menuLayoutList.goMenu;

    }, function (e) { console.log(e); })

    obj.stock = {
        updateItem: function (data) {
            $.each($scope.food.category, function (idx, cat) {
                $.each(cat.items, function (itemIdx, item) {
                    var found = false;
                    $.each(data, function (i, v) {
                        if (item.code == v.code) {
                            found = true;
                            item.stock = v.qty;
                            console.log(item);
                            console.log(item.code)
                        }
                    });
                    if (!found) {
                        item.stock = '';
                    }
                })
            });
        }
    }

    $scope.deployMenu = function () {
        SocketFactory.emit('GenerateMenuLayout', {}, function (result) {
            console.log(result)
        });
    }

    $scope.changeTime = function (time) {
        SocketFactory.emit('changetime', { 'time': time }, function (result) {
            console.log("changeTime", result);
        });
    }

    $scope.reset = function () {
        var obj = [];
        $scope.sectionList.forEach(function (section) {
            section.imageList.forEach(function (image) {
                image.enable = true;
                var fileNameSplit = image.img.split("/");
                var fileName = fileNameSplit[fileNameSplit.length - 1];
                obj.push({ "img": fileName, "enable": image.enable });
            });
        });
        console.log(obj);
        SocketFactory.emit('applysoldout', { "list": obj });
    }

    $scope.foodControl = function (type, item) {
        switch (type) {
            case "prepare":
                var qty = -99;
                break;
            case "soldout":
                var qty = -1;
                break;
            case "clear":
                var qty = -2;
                break;
        }
        SocketFactory.emit('setItemQty', { itemCode: item.code, qty: qty }, function (result) {
            console.log("setItemQty soldout", result)
        });
    }
});