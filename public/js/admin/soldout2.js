//a = [{ "image_filename": "/files/signage/media/image/fairwood/chinese_s2.jpg", "patch_box": [{ "patch_image": "/files/signage/media/image/patch_image/Not_Available_Icon_01.png", "x": 237, "y": 0, "w": 176, "h": 176 }] }];

//socket.emit('generaterule', a);

//socket.emit('boxstatuschangerequest', {});
//socket.emit('applyrule', [{ "img": "chinese_s2.jpg" }]);


//a = [{ "image_filename": "/files/signage/media/image/fairwood/chinese_s2.jpg", "patch_box": [] }];

var socket;
//var configFile = 'files/signage/media/fairwood_demo/config.xml';
var configFile = 'data/food.json';

var app = angular.module('fdm', ['ngTouch']);
app.config(function ($locationProvider, $provide, $httpProvider) {
    $locationProvider.html5Mode({ enabled: true, requireBase: false, rewriteLinks: true });
    $provide.constant('INFO', INFO);
    $httpProvider.useApplyAsync(true); // put all http request into one digest cycle for speed optimization
});

app.factory('SocketFactory', function ($rootScope, $timeout, INFO) {
    var socketConnection = 'http://' + location.hostname + ':' + INFO.socketPort;
    var socket = io.connect(socketConnection);
    io.Manager(socketConnection, { reconnect: true });
    var asyncAngularify = function (socket, callback) {
        return callback ? function () {
            var args = arguments;
            $timeout(function () {
                callback.apply(socket, args);
            }, 0);
        } : angular.noop;
    };
    return {
        on: function (eventName, callback) {
            //console.log(eventName);
            socket.on(eventName, function () {
                var args = arguments;
                $rootScope.$apply(function () {
                    callback.apply(socket, args);
                });
            });
            /*
            socket.io.on("connect_error", function (e) {
                console.log('socket on connect_error');
            })
            socket.on('connect_failed', function (e) {
                console.log('socket on connect_failed');
                console.log(e);
            });
            socket.on('disconnect', function (e) {
                console.log('socket on disconnect');
                console.log(e);
            });
            socket.on('error', function (e) {
                console.log('socket on error');
                console.log(e);
            });
            */
        },
        emit: function (eventName, data, callback) {
            //console.log(eventName);
            var lastIndex = arguments.length - 1;
            var callback = arguments[lastIndex];
            if (typeof callback == 'function') {
                callback = asyncAngularify(socket, callback);
                arguments[lastIndex] = callback;
            }
            return socket.emit.apply(socket, arguments);
        }
    };
});
app.controller('FDMController', function (SocketFactory, INFO, $scope, $window, $http, $q) {
    SocketFactory.on('connect', function (msg) {
        console.log('receive');
    });
    SocketFactory.on('refreshItemQty', function (data) {
        console.log('refreshItemQty', data)
        $scope.stockList = JSON.parse(data);
        obj.stock.updateItem($scope.stockList);
        updateStock($scope.stockList);
    });
    SocketFactory.on('reloadCurrentMenuLayout', function () {
        console.log('reloadCurrentMenuLayout')
        obj.promise();
    });
    var obj = {};
    obj.foodPromise = $http.get('data/food.json');
    obj.loadStockPromise = function () {
        var deferred = $q.defer();
        SocketFactory.emit('loadItemQty', '', function (r) {
            r = JSON.parse(r);
            deferred.resolve(r);
        });
        return deferred.promise;
    }
    obj.loadMenuLayoutPromise = function () {
        var deferred = $q.defer();
        SocketFactory.emit('getCurrentMenuLayout', {}, function (r) {
            console.log('getCurrentMenuLayout', r);
            deferred.resolve(r);
        });
        return deferred.promise;
    }
    obj.promise = function () {
        return $q.all([obj.foodPromise, obj.loadStockPromise(), obj.loadMenuLayoutPromise()]).then(
            function (values) {
                console.log(values);
                $scope.food = values[0].data;
                $scope.stockList = values[1];
                obj.stock.updateItem($scope.stockList);
                $scope.menuLayoutList = values[2].layouts;
                $scope.serverTime = values[2].serverTime;
                $scope.serverLocalInterval = $scope.serverTime - new Date();
                delete $scope.menuLayoutList.goMenu;

                $('body').data("food", $scope.food);
                $('body').data("imageList", values[2].imageList);
                $('body').data("templateList", values[2].templateList);

                for (var first in $scope.menuLayoutList) if ($scope.menuLayoutList.hasOwnProperty(first)) break;

                $scope.buildMenuLayout(first, $scope.menuLayoutList[first]);
            },
            function (e) { console.log(e); }
        );
    }

    obj.stock = {
        updateItem: function (data) {
            $.each($scope.food.category, function (idx, cat) {
                $.each(cat.items, function (itemIdx, item) {
                    var found = false;
                    $.each(data, function (i, v) {
                        if (item.code == v.code) {
                            found = true;
                            item.stock = v.qty;
                            console.log(item);
                            console.log(item.code)
                        }
                    });
                    if (!found) {
                        item.stock = '';
                    }
                })
            });
        }
    }

    $scope.selectedPanel = "";

    $scope.buildMenuLayout = function (k, m) {
        $scope.selectedPanel = k;
        buildMenuLayout(m);
        updateStock($scope.stockList);
    }

    $scope.deployMenu = function (callback) {
        SocketFactory.emit('GenerateMenuLayout', {}, function (result) {
            console.log(result);
            callback();
        });
    }

    $scope.changeTime = function (time) {
        SocketFactory.emit('changetime', { 'time': time }, function (result) {
            console.log("changeTime", result);
        });
    }

    $scope.reset = function () {
        var obj = [];
        $scope.sectionList.forEach(function (section) {
            section.imageList.forEach(function (image) {
                image.enable = true;
                var fileNameSplit = image.img.split("/");
                var fileName = fileNameSplit[fileNameSplit.length - 1];
                obj.push({ "img": fileName, "enable": image.enable });
            });
        });
        console.log(obj);
        SocketFactory.emit('applysoldout', { "list": obj });
    }

    $scope.foodControl = function (type, item) {
        switch (type) {
            case "soldout":
                var qty = -1;
                break;
            case "clear":
                var qty = -2;
                break;
            case "prepare":
                var qty = -3;
                break;
        }
        if (typeof item === 'object')
            var itemCode = item.code;
        else if (typeof item === "string")
            var itemCode = item;
        SocketFactory.emit('setItemQty', { itemCode: itemCode, qty: qty }, function (result) {
            console.log("setItemQty soldout", result)
        });
    }

    $scope.cleanUpMenuLayout = function (data) {
        $.each(data.flow, function (idx, menulayout) {
            $.extend(true, menulayout, $scope.baseFlow);
            if (!menulayout.timeRangeId) {
                $.extend(true, menulayout, { timeRangeId: 0 });
            }
            if (menulayout.navigationBar && menulayout.navigationBar.layout.logo) {
                cleanupTileItem(menulayout.navigationBar.layout.logo);
            }
            if (menulayout.navigationBar && menulayout.navigationBar.layout.buttons)
                $.each(menulayout.navigationBar.layout.buttons, function (btnIdx, btn) {
                    cleanupTileItem(btn);
                })
            if (menulayout.pages)
                $.each(menulayout.pages, function (pageIdx, page) {
                    $.each(page.layout, function (btnIdx, btn) {
                        cleanupTileItem(btn);
                    });
                });
        });
    }

    $scope.save = function () {
        var layout = $('.main-canvas').data("layout");
        layout.flowOnly = true;
        layout.staffId = 'foodControl';
        console.log('saveMenuLayout', layout)
        $scope.cleanUpMenuLayout(layout);
        SocketFactory.emit('saveMenuLayout', layout, function () {
            $scope.deployMenu(function () {
                console.log('deploy update');
                hideNavbar('#bs-example-navbar-collapse-1');
            })
        })
    }

    $scope.reload = function () {
        location.reload();
    }

    $scope.downloadCloudContent = function () {
        SocketFactory.emit('downloadCloudContent', function () {
            console.log('downloadCloudContent finish');
            location.reload();
        });
        hideNavbar('#bs-example-navbar-collapse-1');
    }

    obj.promise();
});

$(function () {
    $('.ui-loader').hide();
})

$(window).resize(function () {
    buildMenuLayout();
    updateStock();
});

function buildMenuLayout(m) {
    var $target = $('.main-canvas');
    if (typeof m == 'undefined') {
        m = $target.data("layout");
    }
    else {
        $target.data("layout", m);
    }
    $target.empty();

    var windowW = $(window).width();
    var windowH = $(window).height();
    var maxCanvasW = windowW - 20;
    var maxCanvasH = windowH - 50 - $('#tool-bar').outerHeight(true) - 20;
    if (windowW > windowH) {
        //var canvasW = 960;
        //var canvasW = $(window).width() - 500;
        //var canvasH = canvasW * (m.height / m.width);
        var canvasH = maxCanvasH;
        var canvasW = canvasH * (m.width / m.height);
        if (maxCanvasW < canvasW) {
            canvasH = canvasH * (maxCanvasW / canvasW);
            canvasW = maxCanvasW;
        }
        console.log(canvasW, canvasH);
        var ratioX = canvasW / m.width;
        var ratioY = canvasH / m.height;
        console.log(ratioX, ratioY);
    }
    else {
        if (m.orientation == "portrait") {
            var canvasH = maxCanvasH;
            var canvasW = canvasH * (m.width / m.height);
        }
        else {
            var canvasW = windowW;
            var canvasH = canvasW * (m.height / m.width);
            if (maxCanvasW < canvasW) {
                canvasH = canvasH * (maxCanvasW / canvasW);
                canvasW = maxCanvasW;
            }
        }
        console.log(canvasW, canvasH);
        var ratioX = canvasW / m.width;
        var ratioY = canvasH / m.height;
        console.log(ratioX, ratioY);
    }
    //$target.css({ "width": canvasW, "height": canvasH, "background": m.backgroundColor, "color": m.fontColor });
    $target.css({ "width": m.width, "height": m.height, "background": m.backgroundColor, "color": m.fontColor, "zoom": ratioX });
    //$target.css({ "width": m.width, "height": m.height, "background": m.backgroundColor, "color": m.fontColor});

    var currentFlow = null;
    m.flow.forEach(function (flow) {
        var time = flow.time.split('-');
        var now = new Date();
        var today = now.getFullYear() + "/" + (now.getMonth() + 1) + "/" + now.getDate()
        var start = new Date(today + " " + time[0]);
        var end = new Date(today + " " + time[1]);
        if (now >= start && now <= end) {
            currentFlow = flow;
        }
    });

    if (currentFlow) {
        console.log('currentFlow', currentFlow);
        currentFlow.pages.forEach(function (p) {
            p.layout.forEach(function (l) {
                //if (l.isDelete) return;
                var $item = $('<div />')
                    .css({ "position": "absolute", "top": (l.y * ratioY), "left": (l.x * ratioX), "width": (l.width * ratioX), "height": (l.height * ratioY) })
                    .data("layout", l)
                    .appendTo($target);
                $item.tileItem({
                    "foodControl": true,
                    "updateStock": function (event, data) {
                        console.log('foodControl', data);
                        $('body').scope().foodControl(data.type, data.itemCode);
                    },
                    "datachange": function (event, data) {
                        console.log('soldout', 'datachange')
                        updateStock();
                    }
                });
            });
        });
    }
}

function updateStock(s) {
    var $target = $('.main-canvas');
    if (s == undefined) {
        s = $target.data("stock");
    }
    else {
        $target.data("stock", s);
    }
    $('.tile-item').each(function () {
        var data = $(this).data('layout');
        if (data.type === "item") {
            var stock = s.filter(function (x) { return x.code == data.itemCode; });
            $(this).find('.food-control-badge').hide();
            if (stock.length) {
                $(this).find('.food-control-clear').removeClass('disabled').show();
                $(this).find('.food-control-soldout, .food-control-prepare').removeClass('disabled red').show();
                if (stock[0].qty == -3) {
                    $(this).find('.food-control-prepare').addClass('disabled red').hide();
                    $(this).find('.food-control-badge-prepare').show();
                }
                else if (stock[0].qty == -1) {
                    $(this).find('.food-control-soldout').addClass('disabled red').hide();
                    $(this).find('.food-control-badge-soldout').show();
                }
            }
            else {
                $(this).find('.food-control-clear').addClass('disabled').hide();
                $(this).find('.food-control-soldout, .food-control-prepare').removeClass('disabled red').show();
            }
            $(this).find('.tile-item-handle').hide();
        }
    });
}

function hideNavbar(selector) {
    $(selector).collapse('hide');
}

function toggleFullScreen() {
    if ((document.fullScreenElement && document.fullScreenElement !== null) ||    // alternative standard method
        (!document.mozFullScreen && !document.webkitIsFullScreen)) {               // current working methods
        if (document.documentElement.requestFullScreen) {
            document.documentElement.requestFullScreen();
        } else if (document.documentElement.mozRequestFullScreen) {
            document.documentElement.mozRequestFullScreen();
        } else if (document.documentElement.webkitRequestFullScreen) {
            document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
        }
    } else {
        if (document.cancelFullScreen) {
            document.cancelFullScreen();
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if (document.webkitCancelFullScreen) {
            document.webkitCancelFullScreen();
        }
    }
}