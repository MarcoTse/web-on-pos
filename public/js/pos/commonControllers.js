(function () {
    var app = angular.module('common.controllers', []);

    function DialogControllerLogin($scope, $mdDialog, SocketFactory, MainService, uiLanguage, INFO) {
        // console.log("called once");
        // console.log($scope);
        $scope.mode = 'username';
        $scope.modeName = 'Login';
        $scope.username = '';
        $scope.password = '';
        $scope.tableNum = '';
        $scope.keypad = keypad;
        $scope.ui = uiLanguage[INFO.lang];

        $scope.input = function (val) {
            angular.element(".login").addClass("md-input-focused");
            $scope.loginValue += '' + val;
        }

        $scope.reset = function (val) {
            angular.element(".login").addClass("md-input-focused");
            if ($scope.loginValue == '' && $scope.mode == 'username') {
                $mdDialog.cancel();
                // angular.element(".login").addClass("md-input-focused");
            } else if ($scope.loginValue == '' && $scope.mode == 'password') {
                $scope.modeName = 'Login';
                $scope.mode = 'username';
                $scope.loginValue = '';
            } else {
                $scope.loginValue = '';
            }
        }

        $scope.backspace = function () {
            // console.log('here!');
            // $scope.loginValue = "12347";
            $scope.loginValue = $scope.loginValue.substring(0, $scope.loginValue.length - 1);
        };

        $scope.next = function (val) {
            //console.log('********');
            //console.log($scope.tableNum);
            //console.log($scope.mode);
            //console.log($scope.loginValue);
            switch ($scope.mode) {
                case 'username':
                    $scope.username = $scope.loginValue;
                    MainService.UserManager.login($scope.username, '').then(function (r) {
                        //console.log(r);
                        //console.log(r.success);
                        if (r.success) {
                            $scope.loginError = false;
                            $mdDialog.cancel();
                            MainService.UserManager.loginCallback();
                        } else {
                            $scope.mode = 'password';
                            $scope.modeName = 'Password';
                            $scope.resetLoginInputValue();
                        }
                    }, function (reason) {
                        console.log('Failed: ' + reason);
                    }, function (update) {
                        console.log('Got notification: ' + update);
                    });
                    break;
                case 'password':
                    $scope.password = $scope.loginValue;
                    MainService.UserManager.login($scope.username, $scope.password).then(function (r) {
                        console.log(r);
                        if (r.success) {
                            $scope.loginError = false;
                            $mdDialog.cancel();

                            MainService.UserManager.loginCallback();
                            if (MainService.mode == MainService.schema.mode.home) {
                                MainService.switchMode2({ mode: 'main' }, false);
                                MainService.mode = MainService.schema.mode.main;
                                MainService.currentPageTitle = uiLanguage[INFO.lang]['main_page'];
                            }
                        } else {
                            $scope.mode = 'password';
                            $scope.resetLoginInputValue();
                            $scope.loginError = true;
                        }
                    }, function (reason) {
                        console.log('Failed: ' + reason);
                    }, function (update) {
                        console.log('Got notification: ' + update);
                    });
                    break;
            }
        }
    }

    // support permission checkup, eg report button => check permission first before redirect
    function DialogControllerLogin2($scope, $mdDialog, SocketFactory, MainService, uiLanguage, INFO) {
        // console.log("called once");
        // console.log($scope);
        $scope.mode = 'username';
        $scope.modeName = 'Login';
        $scope.username = '';
        $scope.password = '';
        $scope.tableNum = '';
        $scope.keypad = keypad;
        $scope.ui = uiLanguage[INFO.lang];

        $scope.input = function (val) {
            angular.element(".login").addClass("md-input-focused");
            $scope.loginValue += '' + val;
        }

        $scope.reset = function (val) {
            angular.element(".login").addClass("md-input-focused");
            if ($scope.loginValue == '' && $scope.mode == 'username') {
                $mdDialog.cancel();
                // angular.element(".login").addClass("md-input-focused");
            } else if ($scope.loginValue == '' && $scope.mode == 'password') {
                $scope.modeName = 'Login';
                $scope.mode = 'username';
                $scope.loginValue = '';
            } else {
                $scope.loginValue = '';
            }
        }

        $scope.backspace = function () {
            // console.log('here!');
            // $scope.loginValue = "12347";
            $scope.loginValue = $scope.loginValue.substring(0, $scope.loginValue.length - 1);
        };

        $scope.next = function (val) {
            //console.log('********');
            //console.log($scope.tableNum);
            //console.log($scope.mode);
            //console.log($scope.loginValue);
            switch ($scope.mode) {
                case 'username':
                    $scope.username = $scope.loginValue;
                    MainService.UserManager.login($scope.username, '').then(function (r) {
                        //console.log(r);
                        //console.log(r.success);
                        if (r.success) {
                            $scope.loginError = false;
                            $mdDialog.cancel();
                            MainService.UserManager.LoginRedirectToMode2 = $scope.modeSwitcherParam[0];
                            MainService.UserManager.loginCallback();
                        } else {
                            $scope.mode = 'password';
                            $scope.modeName = 'Password';
                            $scope.resetLoginInputValue();
                        }
                    }, function (reason) {
                        console.log('Failed: ' + reason);
                    }, function (update) {
                        console.log('Got notification: ' + update);
                    });
                    break;
                case 'password':
                    $scope.password = $scope.loginValue;
                    MainService.UserManager.login($scope.username, $scope.password).then(function (r) {
                        console.log(r);
                        if (r.success) {
                            $scope.loginError = false;
                            $mdDialog.cancel();

                            // check permission on login callback
                            // console.log($scope.modeSwitcherParam);
                            // permission
                            switch ($scope.modeSwitcherParam[4]) {
                                case 'all':
                                    // not check
                                    MainService.UserManager.LoginRedirectToMode2 = $scope.modeSwitcherParam[0];
                                    // MainService.UserManager.LoginRedirectToCaller = $event;
                                    MainService.UserManager.loginCallback();
                                    break;
                                default:
                                    console.log($scope.modeSwitcherParam[4]);
                                    MainService.checkPermission($scope.modeSwitcherParam[4], function () {
                                        MainService.UserManager.LoginRedirectToMode2 = $scope.modeSwitcherParam[0];
                                        MainService.UserManager.loginCallback();
                                    })
                                    break;
                            }
                        } else {
                            $scope.mode = 'password';
                            $scope.resetLoginInputValue();
                            $scope.loginError = true;
                        }
                    }, function (reason) {
                        console.log('Failed: ' + reason);
                    }, function (update) {
                        console.log('Got notification: ' + update);
                    });
                    break;
            }
        }
    }

    app.controller('DialogControllerLogin', DialogControllerLogin);

    /*
        generic controllers
        $rootElement: for global event
    */
    // app.factory('SocketFactoryFree', function ($rootScope, $timeout, INFO) {
    //     var socketConnection = 'http://' + location.hostname + ':5001';
    //     var socket = io.connect(socketConnection);
    //     io.Manager(socketConnection, { reconnect: true });
    //     var asyncAngularify = function (socket, callback) {
    //         return callback ? function () {
    //             var args = arguments;
    //             $timeout(function () {
    //                 callback.apply(socket, args);
    //             }, 0);
    //         } : angular.noop;
    //     };
    //     return {
    //         on: function (eventName, callback) {
    //             //console.log(eventName);
    //             socket.on(eventName, function () {
    //                 var args = arguments;
    //                 $rootScope.$apply(function () {
    //                     callback.apply(socket, args);
    //                 });
    //             });
    //         },
    //         emit: function (eventName, data, callback) {
    //             //console.log(eventName);
    //             var lastIndex = arguments.length - 1;
    //             var callback = arguments[lastIndex];
    //             if (typeof callback == 'function') {
    //                 callback = asyncAngularify(socket, callback);
    //                 arguments[lastIndex] = callback;
    //             }
    //             return socket.emit.apply(socket, arguments);
    //         }
    //     };
    // });

    app.controller('globalController', function ($scope, $element, $location, $mdDialog, $controller, $rootElement, $interval, $timeout, MainService, uiLanguage, INFO, userConfig, keyMap, SocketFactory, menu, $rootScope) {
        // $controller('DialogControllerLogin',{$scope: $scope, $mdDialog: $mdDialog});
        // console.log($location.search());

        // global inheritance for each controller
        var query = $location.search();
        //if (!query.till && !userConfig.dev) {
        //    angular.element('body').html('');
        //    return;
        //}



        userConfig.page = {
            cash: query.cash || userConfig.page.cash,
            printer: query.printer || userConfig.page.printer,
            bill: query.bill || userConfig.page.bill,
            till: query.till || userConfig.page.till,
            duty: query.duty || userConfig.page.duty,
            takeaway: query.takeaway || userConfig.page.takeaway,
            
        };

        $scope.$watch('MainService.lockTable.tableNum', function () {
            //alert(MainService.lockTable.tableNum);
            if (MainService.lockTable.tableNum == "") return;
            //alert('lock table ' + MainService.lockTable.tableNum);
            MainService.lockTable.toActiveLockTable();
            //MainService.lockTable.tableNum = "";

        });

        $scope.uc = $scope.config = userConfig;
        $scope.ui = uiLanguage[INFO.lang];
        menu.userConfig = userConfig;

        // development test
        $scope.autoLogin = function (val) {
            MainService.UserManager.login(userConfig.devParam.password, userConfig.devParam.password).then(function (r) {
                // console.log(r);
                if (r.success) {
                    $scope.loginError = false;
                    $mdDialog.cancel();

                    // , modeNavMenu: 'editTable', modeFloorPlan: 'order'
                    // MainService.UserManager.LoginRedirectToMode2 = {mode: 'floorPlan', modeFloorPlan: 'split'};

                    // edit table
                    // MainService.UserManager.LoginRedirectToMode2 = {mode: 'floorPlan', modeFloorPlan: 'order', modeNavMenu: 'editTable'};

                    // MainService.UserManager.LoginRedirectToMode2 = {mode: 'takeAway', modeOrder: 'normal', modeNavMenu: 'takeAway'};

                    // order
                    MainService.UserManager.LoginRedirectToMode2 = { mode: 'floorPlan', modeFloorPlan: 'order', modeOrderMenu: 'normal', modeNavMenu: 'normal' };

                    MainService.pageTitle = uiLanguage[INFO.lang]['edit_table_mode'];
                    // MainService.UserManager.LoginRedirectToCaller = $event;
                    MainService.UserManager.loginCallback();
                } else {
                    $scope.mode = 'password';
                    $scope.resetLoginInputValue();
                    $scope.loginError = true;
                }
            }, function (reason) {
                console.log('Failed: ' + reason);
            }, function (update) {
                console.log('Got notification: ' + update);
            });
        }

        function editTableAux() {
            if (Snap('#grid-helper') != null) {
                Snap('#grid-helper').removeClass('ng-hide');
                $interval.cancel(waitForRendering);
                $('.btn-grid').addClass('active');
            }
        }
        if (userConfig.dev) {
            if (query.autologin || userConfig.devParam.autologin) {
                $scope.autoLogin();

                var waitForRendering = $interval(function () {
                    // editTableAux();
                    // var target = '.btn-kitchen';
                    // angular.element(target).click();
                    // MainService.setActive( angular.element(target) );
                }, 1500)

                $timeout(function () {
                    // var target = '.btn-kitchen';
                    // angular.element(target).click();
                    // MainService.setActive( angular.element(target) );
                }, 10)
            }
        }

        $scope.returnStatus = function (status) {
            if (status == 4) {
                // console.log('*********************************** ' + status);
                return $scope.ui['billed'];
            }
        }

        $scope.toggleEditTable = function () {
            $('#new-table-shape-container').toggleClass('ng-hide');
        }

        $scope.toggleID = function (ID) {
            if ($('#' + ID).length > 0) {
                $('#' + ID).toggleClass('ng-hide');
                return;
            }

            if ($('.' + ID).length > 0) {
                $('.' + ID).toggleClass('ng-hide');
                return;
            }
        }

        $scope.toggleSVG = function (ID) {
            if (MainService.RMSTable.svgScene.select("#" + ID) != null) {
                MainService.RMSTable.svgScene.select("#" + ID).toggleClass('ng-hide');
                return;
            }

            if (MainService.RMSTable.svgScene.select("." + ID) != null) {
                MainService.RMSTable.svgScene.select("." + ID).toggleClass('ng-hide');
                return;
            }

            // MainService.RMSTable.svgScene.select("#"+ID).toggleClass('ng-hide');
            // Snap("#svg-canvas").select("#"+ID).toggleClass('ng-hide');
            // $('#'+ID).toggleClass('ng-hide');
        }

        $scope.removeAllSelectionBB = function () {
            console.log('removeAllSelectionBB');
            var svgScene = Snap("#svg-canvas");
            if ((activeItem = svgScene.selectAll(".selected")).length != 0) {
                activeItem.forEach(function (element) {
                    element.ftRemoveBB();
                    element.data("bbT") && element.data("bbT").remove();
                    element.data("bb") && element.data("bb").remove();
                    element.removeClass('selected');
                });
            }
        }

        $scope.toggleGroupEdit = function () {
            MainService.RMSTable.groupEdit = !MainService.RMSTable.groupEdit;
            if (MainService.RMSTable.groupEdit) {
                $('#current-table-label').attr('disabled', true);
                Snap("#svg-canvas").selectAll(".transform").forEach(function (element) {
                    if (element.freeTransform) {
                        element.freeTransform.hideHandles();
                    }
                });
            } else {
                // unselect all
                $('#current-table-label').attr('disabled', false);
                $scope.removeAllSelectionBB();
            }
        }

        $scope.toggleGrid = function () {
            Snap('#grid-helper').toggleClass('ng-hide');
        }

        $scope.toggleActive = function (e) {
            console.log('toggle active');
            if (typeof e === 'object' && (angular.isDefined(e.currentTarget) || angular.isDefined(e.srcElement))) {
                // treat as event object
                var target = e.currentTarget || e.srcElement;
            } else {
                var target = e;
            }

            // angular.element(target).parents('.md-toolbar-item').find('.active').removeClass("active");
            angular.element(target).toggleClass("active");
        }

        $scope.toggleItemDisplay = function (mode) {
            return MainService.singleModeToggle('modeItemListDisplay', mode);
        }

        $scope.toggleItemDisplay1 = $scope.toggleItemDisplay;

        $scope.toggleItemDisplay2 = function (mode) {
            return MainService.singleModeToggle('modeItemListDisplay2', mode);
        }

        $scope.toggleItemOption = function (option) {
            // modeObj = modeObj ? modeObj : {};
            // $scope.$emit('toggleItemOption', modeObj);
            switch (option) {
                case 'transfer_item':
                    $scope.switchMode2({ modeOrder: 'transfer', modeItem: 'normal', modeFloorPlan: 'order' }, true, 'transfer_item');
                    break;
                default:

                    break;
            }
        }

        $scope.toggleFoodControlMode = function () {
            return MainService.modeToggleFoodControl();
        }

        $scope.toggleNavMode = function (mode) {
            var mode = angular.isUndefined(mode) ? ['normal'] : mode,
                mode = angular.isArray(mode) ? mode : [mode];

            return MainService.singleModeToggle('modeNavMenu', mode);
        }

        //old alias
        $scope.modeToggleFoodControl = $scope.toggleFoodControlMode;

        $scope.returnToOrder = function () {
            MainService.modeMultiSelectItem = false;
            MainService.Cart.resetEditItem();
            $scope.$emit('returnToOrder', []);
        }

        $scope.strConvertTo = function (option, str) {
            // return strConvertTo(option, str);
            return strConvertTo.apply(this, arguments);
        }

        // $scope.showConfirm = function() {
        //     MainService.showConfirm.apply( MainService, arguments );
        // }

        // common page function (shorten the writing in rms-templates), alias
        $scope.isLogin = function () {
            return MainService.UserManager.isLogin;
        };

        $scope.numberToArray = function (num) {
            return new Array(num);
        }

        $scope.numberToArray2 = function (num) {
            var tmp = [];
            for (var i = 0; i < num; i++) {
                tmp.push(i);
            }
            // console.log(tmp);
            return tmp;
        }

        // for pagination
        // remainder is for dummy generation count
        $scope.remainderPageArr = function (arrayLength, optionPerPage) {
            // console.log('a;sdkas;dkas;dkasl;dka================================');
            // console.log(arrayLength);
            // console.log(optionPerPage);
            // console.log(optionPerPage - arrayLength % optionPerPage);
            // console.log(optionPerPage);
            // console.log(arrayLength % optionPerPage);
            // console.log(MainService.getPageNumber( optionPerPage - arrayLength % optionPerPage ));

            return arrayLength % optionPerPage == 0 ? 0 : $scope.numberToArray2(optionPerPage - arrayLength % optionPerPage);
        }

        // MainService.getPageNumber( optionPerPage - MainService.tender.length % optionPerPage )
        // MainService.getPageNumber( MainService.discounPerPage - MainService.availableDiscount.length % MainService.discounPerPage )
        $scope.noOfPage = function (arrayLength, optionPerPage) {
            // console.log('no of page ' + Math.ceil( arrayLength / optionPerPage ));
            return Math.ceil(arrayLength / optionPerPage);
        }

        $scope.repeatCount = 0;
        $scope.counter = function () {
            console.log($scope.repeatCount);
            return $scope.repeatCount++;
        }

        $scope.refreshScreen = function () {
            var confirm = $mdDialog.confirm()
               .title(uiLanguage[INFO.lang]['alert'])
               .content(uiLanguage[INFO.lang]['refresh_confirmation'])
               .ok(uiLanguage[INFO.lang]['yes'])
               .cancel(uiLanguage[INFO.lang]['cancel']);

            $mdDialog.show(confirm).then(function () {
                location.reload();
            }, function () {
            });
        }


        $scope.takeOut = {
            oriColor: null,
            topBarInit: function () {
                var oriColor = $('.fast-food-in-order-group .color-show:eq(0)').css('background-color');
                this.oriColor = oriColor;
                //$('.fast-food-in-order-group .color-show').on('click', function () {
                //    $('.fast-food-in-order-group .color-show').css('background-color', oriColor);
                //    $(this).css('background-color', 'rgb(255,249,196');
                //})
            },
            getBtnBackgroundColor: function (bool) {
                if (bool) {
                    return { "background": "#DE7047" };
                } else {
                    return { "background": this.oriColor };
                }
            },
            takeOrder: function () {
                //MainService.modeOrderMenu = MainService.schema.modeOrderMenu.normal;
                //MainService.modeNavMenu = MainService.schema.modeNavMenu.takeAwayInOrder;
                //MainService.setActive(angular.element('.btn-takeaway-order'));      
                //MainService.assignTableNo(MainService.getOtherWriteOrderService.till, null, null, false, null);

                if (!MainService.UserManager.isLogin) {
                    //MainService.UserManager.LoginRedirectToTable = tableNo;
                    if (angular.element('md-dialog[aria-label="Login"]').scope() == undefined) {
                        $mdDialog.show({
                            templateUrl: 'tmpl/login.html'
                        })
                    } else {
                        $mdDialog.hide();
                    }
                } else {

                    $timeout(function () {

                        MainService.getOtherWriteOrderService().backTakeAwayOrderMode();
                        MainService.getOtherWriteOrderServiceFullDetail().isCardManually = 0;
                    }, 100)

                }

                //angular.element('#food-menu').addClass("ng-hide");
            },
            viewOrder: function () {
                if (MainService.getOtherWriteOrderService().modeSetting.mode.value == "takeAway") $rootScope.$emit('initAllOrderInTakeOut');
                MainService.getOtherWriteOrderService().backTakeAwayViewMode();
                MainService.getOtherWriteOrderServiceFullDetail().isCardManually = 0;
            },
            searchBill: function () {
                MainService.modeOrder = MainService.schema.modeOrder.searchBill;
            },
            changeTakeoutOrDinning: function () {
                if (MainService.schema.modeOrder.billingSummary == MainService.modeOrder) return;
                MainService.getOtherWriteOrderService().modeSetting.isTakeout ^= 1;
            },
            changeSwipeOrManually: function () {
                if (MainService.schema.modeOrder.billingSummary == MainService.modeOrder) return;
                MainService.getOtherWriteOrderServiceFullDetail().isCardManually ^= 1;
            },
            checkTakeOutAndDeliveryByPhone: function () {
                console.debug("593");
                MainService.getOtherWriteOrderService().takeOutAndDelivery.showCheckBillByPhone();
            }

        }

        $scope.deliveryOrder = {
            viewOrder: function () {
                MainService.deliveryOrderService.viewOrderMode();
            },
            takeOrder: function () {
                if (!MainService.UserManager.isLogin) {
                    if (angular.element('md-dialog[aria-label="Login"]').scope() == undefined) {
                        $mdDialog.show({
                            templateUrl: 'tmpl/login.html'
                        })
                    } else {
                        $mdDialog.hide();
                    }
                } else {
                    MainService.deliveryOrderService.takeOrderMode();
                }
            }
            ,
            searchBill: function () {
                MainService.modeOrder = MainService.schema.modeOrder.searchBill;
            }
        }


        // location.reload();

        // alert($scope.config.page.cash || userConfig.page.cash || false)
        /*$scope.mainMenuListOld = [
          {label: 'main_page', menuGroup: '', mode: {mode: 'main', modeNavMenu: 'normal'}, requiredLogin: false, config: true, col: 3, permission: 'all'},
  
          {label: 'order_mode', menuGroup: '', mode: {mode: 'floorPlan', modeFloorPlan: 'order', modeOrderMenu: 'normal', modeNavMenu: 'normal'}, requiredLogin: true, config: true, col: 4, permission: 'all'},
          {label: 'search_bill', menuGroup: '', mode: {mode: 'order', modeOrder: 'searchBill', modeOrderMenu: 'billMenu', modeNavMenu: 'normal'}, requiredLogin: true, config: true, col: 4, permission: 'checkBill'},
  
        // , modeOrder: 'normal'
          {label: 'take_away', menuGroup: '', mode: {mode: 'takeAway', modeOrderMenu: 'takeAwayMenu', modeNavMenu: 'takeAway'}, requiredLogin: true, config: true, col: 4, permission: 'all', func: function(){
              // $scope.switchMode3({mode: 'takeAway', modeOrder: 'takeAway', modeOrderMenu: 'takeAwayMenu', modeNavMenu: 'takeAway'}, true, 'take_away', 'take_away', 'all');
              // MainService.assignTableNo();
              $scope.switchMode3(this.mode, this.requiredLogin, this.label, this.label, this.permission);
            }
          },
          { label: 'Scan_QRcode', menuGroup: '', mode: { mode: 'scanQrcode', modeNavMenu: 'normal' }, requiredLogin: true, config: true, col: 4, permission: 'all' },
  
          {label: 'report', menuGroup: '', mode: {mode: 'report', modeNavMenu: 'normal'}, requiredLogin: true, config: true, col: 2, permission: 'report'},
  
          {label: 'cash_management', menuGroup: '', mode: {mode: 'cashManagement', modeNavMenu: 'normal'}, requiredLogin: true, config: true, col: 4, permission: 'cashManagement'},
          { label: 'edit_table_mode', menuGroup: '', mode: { mode: 'floorPlan', modeFloorPlan: 'editTable', modeOrderMenu: 'normal', modeNavMenu: 'editTable' }, requiredLogin: true, config: false , col: 4, permission: 'tableManagement' },
          {label: 'daily_clearance', menuGroup: '', mode: {mode: 'dailyClearance', modeNavMenu: 'normal'}, requiredLogin: true, config: true, col: 2, permission: 'dailyClearance'},
  
          {label: 'open_cashbox', menuGroup: '', mode: {mode: 'main', modeMain: 'cashbox', modeNavMenu: 'normal'}, requiredLogin: true, config: $scope.config.page.cash || userConfig.page.cash || false, col: 4, permission: 'cashManagement'},
  
          { label: 'settings', menuGroup: '', mode: { mode: 'settings' }, requiredLogin: true, config: true, col: 4, permission: 'all' },
  
          { label: 'Download_Data', menuGroup: '', mode: { mode: 'testing', modeFloorPlan: '', modeOrderMenu: '', modeNavMenu: '' }, requiredLogin: true, config: true, col: 2, permission: 'tableManagement' },
  
          {label: 'user_management', menuGroup: '', mode: {mode: 'userManagement', modeNavMenu: 'normal'}, requiredLogin: true, config: true, col: 5, permission: 'userManagement'},
          {label: 'user_group', menuGroup: '', mode: {mode: 'userGroupManagement', modeNavMenu: 'normal'}, requiredLogin: true, config: true, col: 5, permission: 'userManagement'},
  
          {label: 'user_clock_management', menuGroup: '', mode: {mode: 'userClockManagement', modeNavMenu: 'normal'}, requiredLogin: false, config: true, col: 1, permission: 'all'}
  
  
          // {label: 'kitchen_info', mode: {mode: 'order', modeItem: 'normal', modeOrder: 'kitchenMessage'}, requiredLogin: true, config: true}
        ];*/

        // console.log(menu.mainMenuList);
        $scope.mainMenuList = [];
        // $scope.mainMenuList = menu.mainMenuList;
        // console.log($scope.mainMenuList);
        // console.log($scope.mainMenuListOld);

        SocketFactory.emit('loadSettings', {}, function (r) {
            r = JSON.parse(r);
            console.log('loadSettings', r, new Date(r.serverTime).getTime());
            $scope.uc.serverTimeInterval = new Date().getTime() - new Date(r.serverTime).getTime();
            // settings
            // order
            // order management
            // table management (edit)
            // staff management
            // till managment
            var till = r.tills[userConfig.page.till];
            if (till) {
                console.log('till', till);
                userConfig.page = {
                    cash: till.cash || userConfig.page.cash,
                    printer: till.printer || userConfig.page.printer,
                    bill: till.bill || userConfig.page.bill,
                    duty: till.duty || userConfig.page.duty,
                    till: userConfig.page.till,
                    takeaway: till.takeaway || false,
                    individualPayment: till.individualPayment || false,
                    additionalMessageAll: till.additionalMessageAll || false,
                    multiPaymentPanel: till.multiPaymentPanel || false,
                    dailyClearance: till.dailyClearance || false,
                };
                $.extend($scope.config, userConfig);
                $.extend($scope.uc, $scope.config);
                //$scope.uc = $scope.config = userConfig;
                //menu.userConfig = userConfig;
            }

            userConfig.takeOutAndDelivery = r.takeOutAndDelivery;
            userConfig.discountPiority = r.discountPiority;
            userConfig.deposit = r.deposit;
            if (userConfig.discountPiority == undefined) userConfig.discountPiority = [];
            //console.log('r.takeAwayMode', r.takeAwayMode)
            //r.takeAwayMode
            //if (false) {
            //    if (userConfig.page.takeaway == "yes") { 
            //        var mainMenuTmp = menu.mainMenuList.filter(function (item) {
            //            return item.takeAwayMode == true;
            //        })

            //        mainMenuTmp.forEach(function (item) {
            //            if (item.pages != undefined)
            //                item.pages = item.pages.filter(function (opts) {
            //                    return opts.takeAwayMode != undefined && opts.takeAwayMode == true;
            //                })
            //        })

            //        menu.mainMenuList = mainMenuTmp;
            //        MainService.resetDefaultMode('takeAway');
            //    }
            //}

            console.log('mainMenuList after filter ', menu.mainMenuList);

            //menu.mainMenuList = menu.buildMainMenuList($scope.uc);
            menu.buildMainMenuList($scope.uc);
            //console.log("djfsh kdshf kdhjf kdsjfh ");
            console.log(menu.mainMenuList);

            $scope.mainMenuList = [];
            for (var i = 0; i < menu.mainMenuList.length; i++) {
                if (menu.mainMenuList[i].type === 'toggle') {
                    for (var k = 0; k < menu.mainMenuList[i].pages.length; k++) {
                        //if(menu.mainMenuList[i].pages[k].takeAwayMode != undefined && menu.mainMenuList[i].pages[k].takeAwayMode == true)
                        $scope.mainMenuList.push(menu.mainMenuList[i].pages[k]);
                    }
                } else {
                    //menu.mainMenuList[i].pages = menu.mainMenuList[i].pages.filter(function (item) {
                    //    return item.takeAwayMode != undefined && item.takeAwayMode == true;
                    //})
                    $scope.mainMenuList.push(menu.mainMenuList[i]);
                }
            }

            SocketFactory.emit('checkBlacklistStatus', { 'till': userConfig.page.till }, function (r) {
                r = JSON.parse(r);
                if (r.result === "ERROR") {
                    MainService.octopusBlacklistOutdated = true;
                }
            });
        });



        // console.log($scope.mainMenuList);

        // dynamic adding function if action function is not explicitly added in obove list
        // for (var i = 0; i < $scope.mainMenuList.length; i++) {
        //   if( angular.isUndefined( $scope.mainMenuList[i].func ) ) {
        //     // console.log('yeah undefined');
        //     $scope.mainMenuList[i].func = function() {
        //       $scope.switchMode3(this.mode, this.requiredLogin, this.label, this.label, this.permission);
        //     }
        //   } else {
        //     // console.log('yeah defined');
        //   }
        // }

        // $scope.login = function() {
        //   var args = Array.prototype.slice.call(arguments).concat(); // same as arguments
        //   return MainService.UserManager.login.apply(MainService, args);
        //   // MainService.UserManager.login();
        // }

        $scope.logout = function () {
            // var args = Array.prototype.slice.call(arguments).concat(); // same as arguments
            return MainService.UserManager.logout.apply(MainService, arguments);
            // MainService.UserManager.logout();
        }

        // deprecated, replaced by v2
        $scope.switchMode = function (modeType, modeName, requiredLogin) {
            MainService.switchMode(modeType, modeName, requiredLogin);
        }

        // modeOption, requiredLogin, message
        // for generic purpose switching between modes
        $scope.switchMode2 = function () {
            // console.log(arguments);
            // put the data in the first arguments for working with custom render extensibility
            // var args = Array.prototype.slice.call(arguments).concat(); // same as arguments

            // automatically get all arguments from caller even not defined in above eg. "message" is not defined, but put in caller function will put here for use and extending the list freely like overloading

            // console.log(arguments);
            // check permission here before switching mode
            MainService.switchMode2.apply(MainService, arguments);

            // MainService.checkPermission(arguments[4], function () {
            //     console.log('ok ar');
            // })

            // MainService.switchMode2( modeOption, requiredLogin );
        }

        $scope.downloadData = function (modeOption) {

            if (modeOption.mode === "testing") {
                console.log('DownloadData testing')
                $scope.$emit('DownloadData', []);
                SocketFactory.emit('DownloadData', []);
            }
        }

        $scope.testing = function () {
            console.log('I am testing')
        }

        //  modeOption, requiredLogin, message, pageTitle, permission
        // for main menu or icon menu with permission check support 
        $scope.switchMode3 = function (modeOption, requiredLogin, pageTitle, message, permission, callback) {

            MainService.ResetFirstInTakeAway();

            if (MainService.lockTable.tableNum != "" && (MainService.lockTable.tableNum == MainService.Cart.tableNo || MainService.lockTable.orderNo == MainService.Cart.tableNo)) {
                MainService.lockTable.toReleaseLockTable(MainService.lockTable.tableNum);
                MainService.lockTable.tableNum = "";
                MainService.lockTable.orderNo = "";
            }
            //MainService.lockTable.getAllLockTable();


            var requiredLogin = (angular.isDefined(requiredLogin)) ? requiredLogin : true,
                pageTitle = (angular.isDefined(pageTitle)) ? pageTitle : '',
                message = (angular.isDefined(message) && message != '') ? uiLanguage[INFO.lang][lowerFirstLetter(message)] : ''

            MainService.pageTitle = uiLanguage[INFO.lang][pageTitle];
            MainService.statusMessage = message;
            $scope.modeSwitcherParam = arguments;
            // console.log(arguments);

            if (callback) {
                callback(modeOption);
            }

            // required login
            if (requiredLogin && !MainService.UserManager.isLogin) {
                if (angular.element('md-dialog[aria-label="Login"]').scope() == undefined) {
                    $mdDialog.show({
                        controller: DialogControllerLogin2,
                        templateUrl: 'tmpl/login2.html',
                        bindToController: true,
                        scope: $scope,
                        preserveScope: true
                    })
                } else {
                    $mdDialog.hide();
                }
            } else {
                switch (arguments[4]) {
                    case 'all':
                        // not check
                        MainService.UserManager.LoginRedirectToMode2 = arguments[0];
                        // MainService.UserManager.LoginRedirectToCaller = $event;
                        MainService.UserManager.loginCallback();
                        break;
                    default:
                        console.log('login already redirect');
                        // console.log(arguments[4]);
                        var args = arguments;
                        MainService.checkPermission(arguments[4], function () {
                            // console.log('here?');
                            MainService.switchMode2.apply(MainService, args);
                        })

                        // MainService.checkPermission(arguments[4], function () {
                        //     MainService.UserManager.LoginRedirectToMode2 = arguments[0];
                        //     MainService.UserManager.loginCallback();
                        // })
                        break;
                }
                // MainService.switchMode2.apply(MainService, arguments);
            }

            /*// all mode changed here required login by default
            var requiredLogin = ( angular.isDefined(requiredLogin) ) ? requiredLogin : true,
                pageTitle = ( angular.isDefined(pageTitle) ) ? pageTitle : '',
                message = ( angular.isDefined(message) && message != '' ) ? uiLanguage[INFO.lang][lowerFirstLetter( message )] : ''
  
            MainService.pageTitle = uiLanguage[INFO.lang][pageTitle];
            MainService.statusMessage = message;
            // console.log(pageTitle);
            // this.showStatusMessage( message );
  
            if( requiredLogin && !MainService.UserManager.isLogin ) {
                MainService.UserManager.checkLogin();
                console.log(permission);
  
                switch( permission ) {
                  case 'all':
                      console.log('all / after login?');
                    break;
                  default:
                        console.log(permission);
                    // MainService.checkPermission(permission, function () {
                    // })
                    break;
                }
                // MainService.UserManager.LoginRedirectToMode2 = modeOption;
                // MainService.UserManager.LoginRedirectToCaller = $event;
            } else {
              switch( permission ) {
                case 'all':
                  console.log('all / no login required');
                  MainService.goToMode2( modeOption );
                  break;
                default:
                      console.log(permission);
                  // MainService.checkPermission(permission, function () {
                  //     // MainService.goToMode2( modeOption );
                  // })
                  break;
              }
            }*/
        }

        // console.log(menu);
        menu.switchMode3 = $scope.switchMode3;

        $scope.isMode = function () {
            return MainService.isMode.apply(MainService, arguments);
        }

        $scope.isNotMode = function () {
            return MainService.isNotMode.apply(MainService, arguments);
        }

        $scope.isModeBill = function () {
            return MainService.isModeBill.apply(MainService, arguments);
        }

        $scope.isNotModeBill = function () {
            return MainService.isNotModeBill.apply(MainService, arguments);
        }

        $scope.isModeFloor = function () {
            return MainService.isModeFloor.apply(MainService, arguments);
        }

        $scope.isNotModeFloor = function () {
            return MainService.isNotModeFloor.apply(MainService, arguments);
        }

        $scope.isModeOrderMenu = function () {
            return MainService.isModeOrderMenu.apply(MainService, arguments);
        }

        $scope.isNotModeOrderMenu = function () {
            return MainService.isNotModeOrderMenu.apply(MainService, arguments);
        }

        $scope.isModeCash = function (mode) {
            var mode = angular.isUndefined(mode) ? ['normal'] : mode,
                mode = angular.isArray(mode) ? mode : [mode];

            return MainService.modeChecking.apply(MainService, [[{ modeCash: mode }]]);
        }

        $scope.isNotModeCash = function () {
            return !$scope.isModeCash.apply($scope, arguments);
        }

        $scope.isModeNavMenu = function (mode) {
            var mode = angular.isUndefined(mode) ? ['normal'] : mode,
                mode = angular.isArray(mode) ? mode : [mode];

            return MainService.modeChecking.apply(MainService, [[{ modeNavMenu: mode }]]);
            // [{modeNavMenu: [\'normal\']}]
        }

        $scope.isNotModeNavMenu = function () {
            return !$scope.isModeNavMenu.apply($scope, arguments);
        }

        $scope.isModeOrder = function () {
            return MainService.isModeOrder.apply(MainService, arguments);
        }

        $scope.isNotModeOrder = function () {
            return MainService.isNotModeOrder.apply(MainService, arguments);
        }

        $scope.isModeItem = function () {
            return MainService.isModeItem.apply(MainService, arguments);
        }

        $scope.isNotModeItem = function () {
            return MainService.isNotModeItem.apply(MainService, arguments);
        }

        /* 
          modeOrder > modeItem
          modeDisplay independent (in any order list mode)
          modeItem: control item mode, display option
          modeOrder: control order mode
          modeDisplay: control item display
        */
        $scope.modCheckItemDisplay = function (mode) {
            // this.modeChecking([{mode: [option]}]);
            return MainService.modeChecking.apply(MainService, [[{ modeItemListDisplay: [mode] }]]);
        }

        $scope.isModeItemDisplay = $scope.modCheckItemDisplay;
        $scope.isNotModeItemDisplay = function () {
            return !$scope.isModeItemDisplay.apply(MainService, arguments);
        }

        // multi channel
        $scope.isModeItemDisplayChannel = function (channel, mode) {
            var mode = angular.isArray(mode) ? mode : [mode];
            switch (channel) {
                default:
                case 1:
                    return MainService.modeChecking.apply(MainService, [[{ 'modeItemListDisplay': mode }]]);
                    break;
                case 2:
                    return MainService.modeChecking.apply(MainService, [[{ 'modeItemListDisplay2': mode }]]);
                    break;
            }
        }
        $scope.isModeItemDisplay1 = function () {
            return $scope.isModeItemDisplayChannel.apply(MainService, [1, arguments[0]])
        };

        $scope.isNotModeItemDisplay1 = function () {
            return !$scope.isModeItemDisplay1.apply(MainService, arguments);
        };

        $scope.isModeItemDisplay2 = function () {
            return $scope.isModeItemDisplayChannel.apply(MainService, [2, arguments[0]])
        };

        $scope.isNotModeItemDisplay2 = function () {
            return !$scope.isModeItemDisplay2.apply(MainService, arguments);
        };

        // console.log('13-1293129-312-30123012-391239-')
        // console.log( $scope.isModeItemDisplay1('normal') )
        // console.log( $scope.isNotModeItemDisplay1('normal') )
        // console.log( $scope.isModeItemDisplay2('normal') )
        // console.log( $scope.isNotModeItemDisplay2('normal') )

        $scope.modeChecking = function () {
            // console.log('me ar....'); // run so many times from start up, not sure why, maybe watcher?.....
            // var args = Array.prototype.slice.call(arguments).concat(); // same as arguments
            return MainService.modeChecking.apply(MainService, arguments);
        }

        $scope.modeCheckBase = function () {
            return MainService.modeCheckBase.apply(MainService, arguments);
        }

        $scope.modeCheckUserGroup = function () {
            return MainService.modeCheckUserGroup.apply(MainService, arguments);
        }

        $scope.modeCheckFloor = function () {
            return MainService.modeCheckFloor.apply(MainService, arguments);
        }

        $scope.modeCheckBill = function () {
            return MainService.modeCheckBill.apply(MainService, arguments);
        }

        $scope.modeCheckOrderItem = function () {
            return MainService.modeCheckOrderItem.apply(MainService, arguments);
        }

        $scope.modeCheckOrder = function () {
            return MainService.modeCheckOrder.apply(MainService, arguments);
        }

        $scope.modeCheckOrderItemDetail = function () {
            return MainService.modeCheckOrderItemDetail.apply(MainService, arguments);
        }

        $scope.modeCheckOrderFood = function () {
            return MainService.modeCheckOrderFood.apply(MainService, arguments);
        }

        $scope.modeToggleItemDetail = function () {
            return MainService.modeToggleItemDetail.apply(MainService, arguments);
        }

        $scope.appliedDiscount = function () {
            //return MainService.appliedDiscount;
            return MainService.Cart.couponObj;
        }

        $scope.deleteDiscount = function () {
            return MainService.deleteDiscount.apply(MainService, arguments);
        }

        $scope.appliedPaymentMethod = function () {
            return MainService.appliedPaymentMethod;
        }

        $scope.remainder = function () {
            return MainService.remainder;
        }

        $scope.deletePaymentMethod = function () {
            return MainService.deletePaymentMethod.apply(MainService, arguments);
        }

        $scope.cancelDeleteItem = function () {
            return MainService.cancelDeleteItem.apply(MainService, arguments);
        }

        $scope.selectAllItem = function () {
            return MainService.selectAllItem.apply(MainService, arguments);
        }

        $scope.unselectAllItem = function () {
            return MainService.unselectAllItem.apply(MainService, arguments);
        }

        $scope.getTenderName = function (paymentMethod) {
            var tender = MainService.allTenderForDisplay.filter(function (t) {
                return t.code == paymentMethod.method;
            });
            var result = "";
            if(tender.length != 0){
                switch(INFO.lang){
                    case "001":
                        result = tender[0].name1;
                        break;
                    case "003":
                        result = tender[0].name2;
                        break;
                }
            }

            return result;
        }
        // count down logout
        // store the interval promise in this variable
        var promise;

        // simulated items array
        $scope.items = [];
        $scope.count = 1;

        // starts the timeout counter
        // $scope.timeout = 2000; // put in settings
        $scope.timeout = angular.isDefined(userConfig.loginTimeout) ? userConfig.loginTimeout : "30000";
        $scope.interval = 500;
        // console.log('$scope.timeout ' + $scope.timeout);
        $scope.startLogoutCountDown = function () {
            // console.log('logout counter start');
            // stops any running interval to avoid two intervals running at the same time
            $scope.stopLogoutCountDown();

            // counter trace
            $scope.count = 1;
            //$scope.logoutTicker = $interval(ticker, $scope.interval);
            //$scope.logoutTicker = setInterval(ticker, $scope.interval);

            // store the timeout promise
            $scope.promise = $timeout(logout, $scope.timeout);
            //$scope.promise = setTimeout(logout, $scope.timeout);
        };

        function ticker() {
            // console.log('run ticker');
            // console.log($scope.count);
            // console.log($scope.interval * $scope.count);
            $scope.count++;
        }

        function logout() {
            // console.log($scope.interval * $scope.count);
            // console.log('logout');
            // clearInterval($scope.logoutTicker);
            MainService.UserManager.logout();
            // $scope.stopLogoutCountDown();

            /*if (MainService.modeOrder == MainService.schema.modeOrder.searchBill) {
    
                console.log('after stop function 1')
                MainService.modeOrder = {};
                MainService.mode = MainService.schema.mode.main;
                console.log('after stop function 11')
            } else {
                console.log('after stop function 2')
                MainService.mode = MainService.schema.mode.floorPlan;
            }*/
        }

        // stops the interval
        $scope.stopLogoutCountDown = function () {
            // console.log('counter init/stop');
            // clearInterval($scope.logoutTicker);
            // console.log('after counter init/stop 1')
            //clearTimeout($scope.promise);
            // console.log('after counter init/stop 2')
            //$interval.cancel($scope.logoutTicker);
            $timeout.cancel($scope.promise);
        };

        // starting the interval by default
        // $scope.startLogoutCountDown();
        MainService.startLogoutCountDown = function () { $scope.startLogoutCountDown(); };
        MainService.stopLogoutCountDown = function () { $scope.stopLogoutCountDown(); };

        // stops the interval when the scope is destroyed,
        // this usually happens when a route is changed and 
        // the ItemsController $scope gets destroyed. The
        // destruction of the ItemsController scope does not
        // guarantee the stopping of any intervals, you must
        // be responsible of stopping it when the scope is
        // is destroyed.
        $scope.$on('$destroy', function () {
            console.log("scope distroyed");
            $scope.stopLogoutCountDown();
        });

        $rootElement.on('mousedown keyup touch', function (e) {
            // if( $(e.target).hasClass('no-login') ) {
            //   console.log("not return?");
            //   return;
            // }

            if (MainService.UserManager.isLogin) {
                // console.log(e.type);
                // console.log('counter reset');

                $scope.startLogoutCountDown(); // restart again, include stop
            }
        });

        $rootElement.on('mousedown keyup touch', function (e) {
            // console.log(e.keyCode == keyMap.ESC);

            if ($scope.modeCheckFloor('changeTable')) {
                if (e.keyCode == keyMap.ESC)
                    MainService.cancelTransfer();
            }
        });
    });

    app.controller('scrollController', function ($scope) {
        $scope.scrollDown = function (element, offset) { //id or class
            var offsetHeight = offset ? offset : 25;
            var currentTop = angular.element(element).scrollTop();
            angular.element(element).scrollTop(currentTop + offsetHeight);
        }
        $scope.scrollUp = function (element, offset) {
            var offsetHeight = offset ? offset : 25;
            var currentTop = angular.element(element).scrollTop();
            angular.element(element).scrollTop(currentTop - offsetHeight);
        }
    });

    // instantiate this controller to call its functions
    /*
      eg.
      inject $controller into any caller controller
      $controller('ToastCtrl',{$scope: $scope}); // passing current scope to commmon controller
    */
    app.controller('ToastCtrl', function ($scope, $mdToast, uiLanguage, INFO) {
        $scope.closeToast = function () {
            $mdToast.hide();
        }

        $scope.showCustomToast = function (delay, msg) {
            // $mdToast.simple().content('Hello!')
            var delay = delay ? delay : 3000;
            $scope.message = msg ? msg : uiLanguage[INFO.lang]['loading'];
            $scope.close = uiLanguage[INFO.lang]['closeMsg'];
            // alert(uiLanguage[INFO.lang]['loading'])

            if (this.message != '' && typeof this.message != 'undefined') {
                $mdToast.hide(); // hide previous
                $mdToast.show({
                    controller: function ($scope, $mdToast, message) { $scope.message = message },
                    templateUrl: 'tmpl/toast-template.html',
                    hideDelay: delay,
                    position: 'bottom left',
                    scope: $scope,
                    preserveScope: true,
                    bindToController: true,
                    locals: {
                        message: $scope.message,
                        close: $scope.close
                    }
                });
            }

            // $mdToast.hide();
        };

        // disable toast for demo
        // $scope.showCustomToast = function(){}
    });

    app.controller('messageBoxController', function ($scope, $mdDialog, uiLanguage, INFO) {
        $scope.alert = '';

        $scope.showAlert = function (title, content) {
            // Appending dialog to document.body to cover sidenav in docs app
            // Modal dialogs should fully cover application
            // to prevent interaction outside of dialog

            var title = title || 'Please provide title',
            content = content || 'Please provide content';

            $mdDialog.show(
              $mdDialog.alert({
                  clickOutsideToClose: true,
                  escapeToClose: true,
                  focusOnOpen: true
              })
                .parent(angular.element(document.body))
                .title(title)
                .content(content)
                .ok(uiLanguage[INFO.lang]["affirmative"])
            );
        };
    });
    //--------------------------------------------------------
})();