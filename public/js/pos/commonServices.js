(function () {

    'use strict';

    angular.module('common.services', [])
    .factory('menu', function ($location, $rootScope, $mdSidenav, $log, uiLanguage, INFO, userConfig) {
        // console.log('called menu');

        var self = {};
        var mainMenuList = [];
        var ui = uiLanguage[INFO.lang];

        var buildMainMenuList = function (userConfig) {
            console.log("menu", userConfig);
            //var query = $location.search(),
            //    uc = {};
            var uc = userConfig;


            //uc.mainMenu = userConfig.mainMenu;
            //uc.takeAwayMenu = userConfig.takeAwayMenu;
            // alert(query.cash || userConfig.page.cash || false)

            //var takeAwayMenu = query.takeaway || false
            var takeAwayMenu = uc.page.takeaway || false
            var menuSettings = uc.mainMenu;
            if (takeAwayMenu == "yes") {
                menuSettings = uc.takeAwayMenu;
            }

            console.debug(uc);
            // var sections = [{
            //   name: 'Getting Started',
            //   state: 'home.gettingstarted',
            //   type: 'link'
            // }];

            // settings
            // order
            // order management
            // table management (edit)
            // staff management

            mainMenuList.push({
                label: 'main_page',
                type: 'function',
                config: true,
                mode: {
                    mode: 'main',
                    modeNavMenu: 'normal'
                },
                requiredLogin: false,
                col: 3,
                permission: 'all'
            });

            mainMenuList.push({
                label: 'order',
                type: 'toggle',
                config: menuSettings.group['order'],
                pages: [{
                    label: 'order_mode',
                    type: 'function',
                    mode: {
                        mode: 'floorPlan',
                        modeFloorPlan: 'order',
                        modeOrderMenu: 'normal',
                        modeNavMenu: 'normal'
                    },
                    requiredLogin: true,
                    config: menuSettings['table_order'],
                    col: 4,
                    permission: 'all',
                    func: function () {
                        $mdSidenav('left').close();
                        self.switchMode3(this.mode, this.requiredLogin, this.label, this.label, this.permission);
                        Snap('#grid-helper').addClass('ng-hide');
                    }
                },
                {
                    label: 'fast_food',
                    type: 'function',
                    mode: {
                        mode: 'fastFood',
                        modeOrderMenu: 'normal',
                        modeNavMenu: 'fastFood'
                    },
                    requiredLogin: true,
                    config: menuSettings['fast_food'],
                    col: 4,
                    permission: 'all'
                },
                {
                    label: 'take_away',
                    type: 'function',
                    mode: {
                        mode: 'takeAway',
                        modeOrderMenu: 'takeAwayMenu',
                        modeNavMenu: 'takeAway'
                    },
                    requiredLogin: true,
                    config: menuSettings['take_away'],
                    col: 4,
                    permission: 'all'
                },
                {
                    label: 'delivery_order',
                    type: 'function',
                    mode: {
                        mode: 'deliveryOrder',
                        modeOrderMenu: 'normal',
                        modeNavMenu: 'deliveryOrder'
                    },
                    requiredLogin: true,
                    config: menuSettings['delivery_order'],
                    col: 4,
                    permission: 'all'
                },
                {
                    label: 'txt_deposit',
                    type: 'function',
                    mode: {
                        mode: 'deposit',
                        modeMain: 'normal',
                        modeNavMenu: 'depositNavBar',
                        modeDeposit: 'input'
                    },
                    requiredLogin: true,
                    config: menuSettings['deposit_management'],
                    col: 4,
                    permission: 'changeTender'
                }
                ]
            });

            mainMenuList.push({
                label: 'order_management',
                type: 'toggle',
                config: menuSettings.group['order_management'],
                pages: [
                {
                    label: 'search_bill',
                    type: 'function',
                    mode: {
                        mode: 'order',
                        modeOrder: 'searchBill',
                        modeOrderMenu: 'billMenu',
                        modeNavMenu: 'normal'
                    },
                    requiredLogin: true,
                    config: menuSettings['search_bill'],
                    col: 4,
                    permission: 'checkBill'
                },
                {
                    label: 'report',
                    type: 'function',
                    mode: {
                        mode: 'report',
                        modeNavMenu: 'normal'
                    },
                    requiredLogin: true,
                    config: menuSettings['report'],
                    col: 2,
                    permission: 'report'
                },
                {
                    label: 'cash_management',
                    type: 'function',
                    mode: {
                        mode: 'cashManagement',
                        modeNavMenu: 'normal'
                    },
                    requiredLogin: true,
                    config: menuSettings['cash_management'],
                    col: 4,
                    permission: 'cashManagement'
                },
                {
                    label: 'daily_clearance',
                    type: 'function',
                    mode: {
                        mode: 'dailyClearance',
                        modeNavMenu: 'normal'
                    },
                    requiredLogin: true,
                    config: uc.page.dailyClearance && menuSettings['daily_clearance'],
                    col: 2,
                    permission: 'dailyClearance'
                },
                {
                    label: 'open_cashbox',
                    type: 'function',
                    mode: {
                        mode: 'main',
                        modeMain: 'cashbox',
                        modeNavMenu: 'normal'
                    },
                    requiredLogin: true,
                    config: uc.page.cash || menuSettings['cash_box'] || false,
                    col: 4,
                    permission: 'cashManagement'
                }

                ]
            });

            mainMenuList.push({
                label: 'system_settings',
                type: 'toggle',
                config: menuSettings.group['system_management'],
                pages: [
                {
                    label: 'settings',
                    type: 'function',
                    mode: {
                        mode: 'settings'
                    },
                    requiredLogin: true,
                    config: menuSettings['settings'],
                    col: 4,
                    permission: 'all'
                },
                {
                    label: 'Category_setting',
                    type: 'function',
                    mode: {
                        mode: 'categorySetting'
                    },
                    requiredLogin: true,
                    config: menuSettings['category_management'],
                    col: 4,
                    permission: 'foodManagement'
                },
                {
                    label: 'Download_Data',
                    type: 'function', mode:
                    {
                        mode: 'testing',
                        modeFloorPlan: '',
                        modeOrderMenu: '',
                        modeNavMenu: ''
                    },
                    requiredLogin: true,
                    config: menuSettings['download'],
                    col: 2,
                    permission: 'tableManagement'
                }
                ]
            });

            mainMenuList.push({
                label: 'table_management',
                type: 'toggle',
                config: menuSettings.group['table_management'],
                pages: [{
                    label: 'edit_table_mode',
                    type: 'function', mode: {
                        mode: 'floorPlan',
                        modeFloorPlan: 'editTable',
                        modeOrderMenu: 'normal',
                        modeNavMenu: 'editTable'
                    },
                    requiredLogin: true,
                    config: menuSettings['edit_table'],
                    col: 4,
                    permission: 'tableManagement',
                    func: function () {
                        $mdSidenav('left').close();
                        self.switchMode3(this.mode, this.requiredLogin, this.label, this.label, this.permission);
                        Snap('#grid-helper').removeClass('ng-hide');
                    }
                }
                ]
            });

            mainMenuList.push({
                label: 'user_management',
                type: 'toggle',
                config: menuSettings.group['user_management'],
                pages: [{
                    label: 'user_management',
                    type: 'function',
                    mode: {
                        mode: 'userManagement',
                        modeNavMenu: 'normal'
                    },
                    requiredLogin: true,
                    config: menuSettings['user_management'],
                    col: 5,
                    permission: 'userManagement'
                },
                {
                    label: 'user_group',
                    type: 'function',
                    mode: {
                        mode: 'userGroupManagement',
                        modeNavMenu: 'normal'
                    },
                    requiredLogin: true,
                    config: menuSettings['user_group_management'],
                    col: 5,
                    permission: 'userManagement'
                },
                {
                    label: 'watch_management',
                    type: 'function',
                    mode: {
                        mode: 'main',
                        modeMain: 'watch',
                        modeNavMenu: 'normal'
                    },
                    requiredLogin: false,
                    config: menuSettings["watch_management"],
                    col: 5,
                    permission: 'all'
                },
                {
                    label: 'txt_build_bill_from_void',
                    type: 'function',
                    mode: {
                        mode: 'buildBillFromVoid',
                        modeOrderMenu: 'buildBillFromVoidMenu',
                        modeNavMenu: 'buildBillFromVoid'
                    },
                    requiredLogin: true,
                    config: menuSettings["build_bill_from_void"],
                    col: 5,
                    permission: 'all'
                },
                {
                    label: 'user_clock_management', menuGroup: '',
                    type: 'function',
                    mode: { mode: 'userClockManagement', modeNavMenu: 'normal' },
                    requiredLogin: false,
                    //config: menuSettings["user_clock_management"],
                    config: uc.page.duty === 'yes' || uc.page.duty === true || false,
                    col: 1,
                    permission: 'all'
                }
                ]
            });

            console.log("menuSettings['user_group_management']" + menuSettings['user_group_management'])
            console.log('userConfig.page.takeAway', takeAwayMenu)
            //if (takeAwayMenu == "yes") {
            //    mainMenuList = [];
            //    mainMenuList.push({
            //        label: 'main_page',
            //        type: 'function',
            //        config: true,
            //        mode: {
            //            mode: 'main',
            //            modeNavMenu: 'normal'
            //        },
            //        requiredLogin: false,
            //        col: 3,
            //        permission: 'all'
            //    });
            //    mainMenuList.push({
            //        label: 'order',
            //        type: 'toggle',
            //        config: uc.takeAwayMenu.group['order'],
            //        pages: [{
            //            label: 'order_mode',
            //            type: 'function',
            //            mode: {
            //                mode: 'floorPlan',
            //                modeFloorPlan: 'order',
            //                modeOrderMenu: 'normal',
            //                modeNavMenu: 'normal'
            //            },
            //            requiredLogin: true,
            //            config: uc.takeAwayMenu['table_order'],
            //            col: 4,
            //            permission: 'all',
            //            func: function () {
            //                $mdSidenav('left').close();
            //                self.switchMode3(this.mode, this.requiredLogin, this.label, this.label, this.permission);
            //                Snap('#grid-helper').addClass('ng-hide');
            //            }
            //        }, {
            //            label: 'take_away',
            //            type: 'function',
            //            mode: {
            //                mode: 'takeAway',
            //                modeOrderMenu: 'takeAwayMenu',
            //                modeNavMenu: 'takeAway'
            //            },
            //            requiredLogin: true,
            //            config: uc.takeAwayMenu['take_away'],
            //            col: 4,
            //            permission: 'all'
            //        },
            //        {
            //            label: 'delivery_order',
            //            type: 'function',
            //            mode: {
            //                mode: 'deliveryOrder',
            //                modeOrderMenu: 'normal',
            //                modeNavMenu: 'deliveryOrder'
            //            },
            //            requiredLogin: true,
            //            config: uc.takeAwayMenu['delivery_order'],
            //            col: 4,
            //            permission: 'all'
            //        }
            //        ]
            //    });
            //    mainMenuList.push({
            //        label: 'order_management',
            //        type: 'toggle',
            //        config: uc.takeAwayMenu.group['order_management'],
            //        pages: [
            //        {
            //            label: 'search_bill',
            //            type: 'function',
            //            mode: {
            //                mode: 'order',
            //                modeOrder: 'searchBill',
            //                modeOrderMenu: 'billMenu',
            //                modeNavMenu: 'normal'
            //            },
            //            requiredLogin: true,
            //            config: uc.takeAwayMenu['search_bill'],
            //            col: 4,
            //            permission: 'checkBill'
            //        },
            //        {
            //            label: 'report',
            //            type: 'function',
            //            mode: {
            //                mode: 'report',
            //                modeNavMenu: 'normal'
            //            },
            //            requiredLogin: true,
            //            config: uc.takeAwayMenu['report'],
            //            col: 2,
            //            permission: 'report'
            //        },
            //        {
            //            label: 'cash_management',
            //            type: 'function',
            //            mode: {
            //                mode: 'cashManagement',
            //                modeNavMenu: 'normal'
            //            },
            //            requiredLogin: true,
            //            config: uc.takeAwayMenu['cash_management'],
            //            col: 4,
            //            permission: 'cashManagement'
            //        },
            //        {
            //            label: 'daily_clearance',
            //            type: 'function',
            //            mode: {
            //                mode: 'dailyClearance',
            //                modeNavMenu: 'normal'
            //            },
            //            requiredLogin: true,
            //            config: uc.takeAwayMenu['daily_clearance'],
            //            col: 2,
            //            permission: 'dailyClearance'
            //        },
            //        {
            //            label: 'open_cashbox',
            //            type: 'function',
            //            mode: {
            //                mode: 'main',
            //                modeMain: 'cashbox',
            //                modeNavMenu: 'normal'
            //            },
            //            requiredLogin: true,
            //            config: uc.page.cash || uc.takeAwayMenu['cash_box'] || false,
            //            col: 4,
            //            permission: 'cashManagement'
            //        }
            //        ]
            //    });
            //    mainMenuList.push({
            //        label: 'system_settings',
            //        type: 'toggle',
            //        config: uc.takeAwayMenu.group['system_management'],
            //        pages: [
            //        {
            //            label: 'settings',
            //            type: 'function',
            //            mode: {
            //                mode: 'settings'
            //            },
            //            requiredLogin: true,
            //            config: uc.takeAwayMenu['settings'],
            //            col: 4,
            //            permission: 'all'
            //        },
            //        {
            //            label: 'Category_setting',
            //            type: 'function',
            //            mode: {
            //                mode: 'categorySetting'
            //            },
            //            requiredLogin: true,
            //            config: uc.takeAwayMenu['category_management'],
            //            col: 4,
            //            permission: 'foodManagement'
            //        },
            //        {
            //            label: 'Download_Data',
            //            type: 'function', mode:
            //            {
            //                mode: 'testing',
            //                modeFloorPlan: '',
            //                modeOrderMenu: '',
            //                modeNavMenu: ''
            //            },
            //            requiredLogin: true,
            //            config: uc.takeAwayMenu['download'],
            //            col: 2,
            //            permission: 'tableManagement'
            //        }
            //        ]
            //    });
            //    mainMenuList.push({
            //        label: 'table_management',
            //        type: 'toggle',
            //        config: uc.takeAwayMenu.group['table_management'],
            //        pages: [{
            //            label: 'edit_table_mode',
            //            type: 'function', mode: {
            //                mode: 'floorPlan',
            //                modeFloorPlan: 'editTable',
            //                modeOrderMenu: 'normal',
            //                modeNavMenu: 'editTable'
            //            },
            //            requiredLogin: true,
            //            config: uc.takeAwayMenu['edit_table'],
            //            col: 4,
            //            permission: 'tableManagement',
            //            func: function () {
            //                $mdSidenav('left').close();
            //                self.switchMode3(this.mode, this.requiredLogin, this.label, this.label, this.permission);
            //                Snap('#grid-helper').removeClass('ng-hide');
            //            }
            //        }
            //        ]
            //    });
            //    mainMenuList.push({
            //        label: 'user_management',
            //        type: 'toggle',
            //        config: uc.takeAwayMenu.group['user_management'],
            //        pages: [{
            //            label: 'user_management',
            //            type: 'function',
            //            mode: {
            //                mode: 'userManagement',
            //                modeNavMenu: 'normal'
            //            },
            //            requiredLogin: true,
            //            config: uc.takeAwayMenu['user_management'],
            //            col: 5,
            //            permission: 'userManagement'
            //        },
            //        {
            //            label: 'user_group',
            //            type: 'function',
            //            mode: {
            //                mode: 'userGroupManagement',
            //                modeNavMenu: 'normal'
            //            },
            //            requiredLogin: true,
            //            config: uc.takeAwayMenu['user_group_management'],
            //            col: 5,
            //            permission: 'userManagement'
            //        },
            //        {
            //            label: 'user_clock_management', menuGroup: '',
            //            type: 'function',
            //            mode: { mode: 'userClockManagement', modeNavMenu: 'normal' },
            //            requiredLogin: false,
            //            config: uc.takeAwayMenu["user_clock_management"],
            //            col: 1,
            //            permission: 'all'
            //        }
            //        ]
            //    });
            //}
            // mainMenuList.push({
            //   label: 'group template',
            //   type: 'toggle',
            //   pages: [
            // ]});
            // mainMenuList.push({
            //   label: 'group template',
            //   type: 'toggle',
            //   pages: [
            // ]});
            // mainMenuList.push();

            // dynamic adding function if action function is not explicitly added in obove list
            for (var i = 0; i < mainMenuList.length; i++) {
                if (mainMenuList[i].type != 'toggle') {
                    if (angular.isUndefined(mainMenuList[i].func)) {
                        mainMenuList[i].func = function () {
                            $mdSidenav('left').close();
                            self.switchMode3(this.mode, this.requiredLogin, this.label, this.label, this.permission);
                        }
                    } else {
                        // console.log('yeah defined');
                    }
                } else {
                    for (var p = 0; p < mainMenuList[i].pages.length; p++) {
                        // console.log(mainMenuList[i].pages[p].func);
                        if (typeof mainMenuList[i].pages[p].func === 'undefined') {
                            mainMenuList[i].pages[p].func = function () {
                                $mdSidenav('left').close();

                                // console.log(self.switchMode3);

                                self.switchMode3(this.mode, this.requiredLogin, this.label, this.label, this.permission);
                            }
                        }

                    }
                }
            }
            return mainMenuList;
        }

        return self = {
            // sections: sections,
            mainMenuList: mainMenuList,
            buildMainMenuList: buildMainMenuList,

            toggleSelectSection: function (section) {
                self.openedSection = (self.openedSection === section ? null : section);
            },
            isSectionSelected: function (section) {
                return self.openedSection === section;
            },

            selectPage: function (section, page) {
                console.log(page);
                page && page.url && $location.path(page.url);
                self.currentSection = section;
                self.currentPage = page;
            }
        };

        function sortByHumanName(a, b) {
            return (a.humanName < b.humanName) ? -1 :
              (a.humanName > b.humanName) ? 1 : 0;
        }
    });

})();