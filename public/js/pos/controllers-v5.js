﻿(function () {
    var app = angular.module('rms.controllers', ['common.controllers']);

    /*
        dummy controllers to create prototype layout
    */
    var dummyCategories = [
        { name: 'Sushi-好味道' }
    ];
    for (var i = 0; i < 7; i++) {
        dummyCategories.push({ name: dummyCategories[0].name + 'x ' + i });
    }
    // console.log(dummyCategories);

    var dummyCategoryItems = [
        { name: 'Sushi かつお' }
    ];
    for (var i = 0; i < 17; i++) {
        dummyCategoryItems.push({ name: dummyCategoryItems[0].name + 'x ' + i });
    }

    var dummyShortcuts = [
        //{name: '加'}
        //{ name: '' }
    ];
    for (var i = 1; i <= 8; i++) {
        dummyShortcuts.push({ name: i });
    }

    var dummyOptions = [
        { name: '加' }
    ];
    for (var i = 0; i < 7; i++) {
        dummyOptions.push({ name: dummyOptions[0].name + 'x ' + i });
    }

    var dummyDeleteOptions = [];
    for (var i = 0; i < 7; i++) {
        // String(i)
        dummyDeleteOptions.push({ name: '1234' });
    }

    var dummyFoodOptions = [];
    for (var i = 0; i < (5 * 5 - 3) ; i++) {
        // String(i)
        dummyFoodOptions.push({ name: '1234 Set' });
    }

    var modifierOption = [];
    modifierOption.push.apply(modifierOption, [
        { name: 'Show value', element: 'input', type: 'text' }
    ]); // option 1 is showing number
    for (var n = 1; n < 10; n++) {
        modifierOption.push({ name: n, element: 'text', type: '' });
    }

    var cancelReason = [];
    for (var n = 0; n < 18; n++) {
        cancelReason.push({ name: 'Reason' + n });
    }

    var foodTransfer = [];
    for (var n = 0; n < 6; n++) {
        foodTransfer.push({ name: 'Food' + n });
    }

    var tableTransfer = [];
    for (var n = 0; n < 6; n++) {
        tableTransfer.push({ name: 'Table' + n });
    }


    // for(var n=0; n<28; n++) {
    //     reportOption.push({name: 'Report'+n});
    // }

    app.controller('divideBillCtrl', function ($scope, MainService, $controller, $mdDialog, $element, $attrs, $timeout) {

        $controller('messageBoxController', { $scope: $scope }); // passing current scope to commmon controller
        $controller('keypadController', { $scope: $scope, $mdDialog: $mdDialog, $element: $element, $attrs: $attrs, $timeout: $timeout, MainService: MainService });

        $scope.divideTablePerPage = 3;
        $scope.currentDividePage = 4;


        $scope.swapIndex = -1;
        $scope.clickMode = "";

        $scope.next = function (event) {
            $mdDialog.hide();
            //MainService.Cart.noOfPeople = $scope.keypadInfo.value;
            //alert($scope.swapIndex);
            if ($scope.clickMode == "changeQty") {
                MainService.splitListClick($scope.swapIndex, $scope.keypadInfo.value);
            } else if ($scope.clickMode == "addSomeGroup") {
                for (var i = 0; i < $scope.keypadInfo.value; i++) {
                    if (MainService.Cart.allSplitList.length > 25) break;
                    MainService.addSplitList();
                }
            }
            angular.element($element).blur();

        };

        //$scope.isShowItem = function (item) {

        //    if (item.voidItemList == undefined) {
        //        console.debug(item.qty + item.voidQty);
        //        return (item.qty + item.voidQty) != 0
        //    }
        //    else {
        //        return (item.qty - item.voidQty - item.voidItemNotTransferQty) != 0
        //    }
        //}




        $scope.addSomeGroup = function ($event) {
            $scope.clickMode = "addSomeGroup";
            $scope.callKeypad($event, $scope.dialogTest1);
        }


        $scope.changeQty = function ($event) {
            //callKeypad($event, dialogTest1)
            $scope.clickMode = "changeQty";
            //console.log('divideBillCtrl / changeQty');
            //console.log('MainService.Cart.splitListRIndex ' + MainService.Cart.splitListRIndex);
            //console.log('MainService.Cart.splitListLIndex ' + MainService.Cart.splitListLIndex);

            var swapIndex = -1;
            $.each(MainService.Cart.allSplitList[MainService.Cart.splitListRIndex].item, function (idx, item) {
                if (item.isEdit) {
                    swapIndex = MainService.Cart.splitListLIndex;
                }
            });
            if (swapIndex === -1) {
                $.each(MainService.Cart.allSplitList[MainService.Cart.splitListLIndex].item, function (idx, item) {
                    if (item.isEdit) {
                        swapIndex = MainService.Cart.splitListRIndex;
                    }
                });
            }
            if (swapIndex === -1) {
                $scope.showAlert("警告", "請先選擇食物");
            } else {
                $scope.swapIndex = swapIndex;
                $scope.callKeypad($event, $scope.dialogTest1)
                //MainService.splitListClick(swapIndex);
            }
        }
    });

    app.controller('dummyCtrl', function ($scope, $timeout, uiLanguage, INFO, $http, SocketFactory, MainService, $mdDialog) {
        ctrl = this;
        this.categories = dummyCategories;
        this.categoryItems = dummyCategoryItems;
        this.shortcuts = dummyShortcuts;
        this.deleteOptions = dummyDeleteOptions;
        this.foodOptions = dummyFoodOptions;
        // this.cancelOption = cancelOption;
        this.modifierOption = modifierOption;
        this.cancelReason = cancelReason;
        this.foodTransfer = foodTransfer;
        this.tableTransfer = tableTransfer;
        $scope.MainService = MainService;

        $scope.ui = uiLanguage[INFO.lang];
        $scope.lang = INFO.lang;
        // console.log(this.ui);

        $scope.triggerKeypad = function (element) {
            $timeout(function () {
                angular.element(element).trigger('click');
            }, 1000);
        }

        // for generating an array to repeat dummy
        $scope.numberToArray = function (num) {
            return new Array(num);
        }
    });

    app.controller('modifierCtrl', function ($scope, uiLanguage, INFO, MainService, $rootScope) {
        this.modifierOption = modifierOption;

        // console.log($scope);
        $scope.cancel = function () {
            MainService.modeOrder = MainService.schema.modeOrder.normal;
            $scope.currentPage = 1;
            $scope.modifierGroupCode == "";
        };

        // console.log('calling modifierCtrl');

        $scope.optionPerPage = 3 * 5 - 2; // row * col - pagination
        $scope.currentPage = 1;

        $rootScope.$on('modifierChanges', function (o) {
            //$element.find('.orderPanelCtrl').removeClass('ng-hide');
            //alert(123);
            console.log('modifierChanges controller');
            console.log('modifierChanges');
        });



        $scope.modifierGroupCode = "";
        $scope.isModifierGroupMode = function () {
            return $scope.modifierGroupCode == "";
        }


        //$rootScope.$emit('returnToOrder', []);
        $scope.init = function () {
            $scope.modifierGroupClick({ modifierGroupCode: $scope.modifierGroupCode });
        }

        $scope.modifierGroupClick = function (item) {
            $scope.currentPage = 1;
            MainService.food.modifiers = angular.copy(MainService.food.allModifiers);
            MainService.food.modifiers = MainService.food.modifiers.filter(function (m) {
                return m.modifierGroupCode == item.modifierGroupCode;
            });

            MainService.noOfModifierPage = Math.ceil(MainService.food.modifiers.length / MainService.cancelOptionPerPage);
            $scope.modifierGroupCode = item.modifierGroupCode;
        }

        //$scope.backModifierGroup = function () {
        //    $scope.currentPage = 1;
        //    MainService.food.modifiers = angular.copy(MainService.food.allModifiers);
        //    MainService.noOfModifierPage = Math.ceil(MainService.food.modifiers.length / MainService.cancelOptionPerPage);
        //    $scope.modifierGroupCode = "";

        //}

        // if( typeof MainService.food != 'undefined' ) {
        //     // MainService.food.modifiers = MainService.food.modifiers ? MainService.food.modifiers : [];
        //     $scope.noOfPage = Math.ceil( MainService.food.modifiers.length / $scope.optionPerPage );
        //     $scope.reminderPage = MainService.getPageNumber( $scope.optionPerPage - MainService.food.kitchenmsg.length % $scope.optionPerPage );
        // }
        //
        // console.log(typeof MainService.food);
        // console.log(typeof MainService.food.modifiers);
        // $scope.noOfPage = Math.ceil( MainService.food.modifiers.length / $scope.optionPerPage );
        // $scope.reminderPage = MainService.getPageNumber( $scope.optionPerPage - MainService.food.kitchenmsg.length % $scope.optionPerPage );
    });

    app.controller('modifierGroupCtrl', function ($scope, uiLanguage, INFO, MainService, $rootScope, SocketFactory, $timeout) {
        $scope.init = function () {
            SocketFactory.emit('GetAllModifierGroup', null, function (r) {
                r = JSON.parse(r);
                $rootScope.$on('modifierClose', function () {
                    $scope.$parent.modifierGroupCode = r[0].modifierGroupCode;
                    $scope.$parent.init();
                });

                if (!MainService.food) {
                    $timeout(function () {
                        $scope.$parent.modifierGroupCode = r[0].modifierGroupCode;
                        $scope.$parent.init();
                    }, 2000)
                } else {
                    $scope.$parent.modifierGroupCode = r[0].modifierGroupCode;
                    $scope.$parent.init();
                }


                $scope.modifierGroup = r;
                $scope.mg_optionPerPage = 8;
                $scope.mg_currentPage = 1;
                $scope.noOfModifierGroupPage = Math.ceil($scope.modifierGroup.length / $scope.mg_optionPerPage);
            });
        }






    });

    app.controller('cancelController', function ($scope, uiLanguage, INFO, MainService) {
        $scope.ui = uiLanguage[INFO.lang];

        var cancelOption = [];
        cancelOption.push.apply(cancelOption, [
            { name: 'Show value', element: 'input', type: 'text' },
            { name: uiLanguage[INFO.lang]['all'], element: 'text', type: '' }
        ]); // option 1 is showing number
        for (var n = 1; n < 10; n++) {
            cancelOption.push({ name: n, element: 'text', type: '' });
        }
        // cancelOption.push({name: 'keyboard', element: 'icon', type: ''});

        this.cancelOption = cancelOption;

        $scope.cancelDeleteItem = function () {
            MainService.modeOrder = MainService.schema.modeOrder.normal;
        }
    });

    /*app.service('reportService', function() {
      var obj = {};

      obj = {
        remark : '',
        setRemark : function( message ){
            this.remark = message
        }
      }

      return obj;
    });

    app.service('headerService', function() {
      var obj = {};

      obj = {
        remark : '',
        setRemark : function( message ){
            this.remark = message
        }
      }

      return obj;
    });*/


    // console.log(userConfig);

    // please add also in settings.js, let user / support config wether they want to use or not
    // add the type as "key" and true or false as "property"
    var reportOption = [
        { name: 'daily_report', type: 'dailyReport' },
        { name: 'daily_Clearance_Report', type: 'dailyClearanceReport' },
        { name: 'daily_report_eat', type: 'dailyReportEat' },
        { name: 'daily_report_take_away', type: 'dailyReportTakeAway' },
        { name: 'daily_Report_Delivery', type: 'dailyReportDelivery' },
        { name: 'daily_attendance', type: 'dailyAttendance' },
        { name: 'daily_food_timeslot_report', type: 'dailyFoodTimeslotReport' },
        { name: 'daily_report_void_bill', type: 'dailyReportVoidBill' },
        { name: 'daily_cancel_item', type: 'dailyCancelItem' },
        { name: 'daily_net_sales', type: 'dailyNetSales' },
        { name: 'daily_discount', type: 'dailyDiscount' },
        { name: 'daily_most_sales_food', type: 'dailyMostSalesFood' },
        { name: 'daily_most_sales_qty_food', type: 'dailyMostSalesQtyFood' },
        { name: 'daily_payment_detail', type: 'dailyPaymentDetail' },
        { name: 'daily_category_sales', type: 'dailyCategorySales' },
        { name: 'daily_per_hour_sales', type: 'dailyPerHourSales' },
        { name: 'daily_item_report', type: 'dailyItemReport' },
        { name: 'daily_report_cloud', type: 'dailyReportCloud' },
        { name: 'daily_watch_service_report', type: 'dailyWatchServiceReport' },
        { name: 'daily_void_ECR_report', type: 'dailyVoidECRReport' }

        // { name: 'daily_cash', type: 'dailyCash' }
        //,{name: 'sales_report'}
    ];

    app.controller('reportController', function ($scope, $controller, $timeout, $element, $rootElement, $mdDialog, uiLanguage, INFO, userConfig, $http, SocketFactory, MainService) {
        // $controller('headerCtrl',{$scope: $scope, $element: $element}); // passing current scope to commmon controller
        // $controller('calendarCtrl',{$scope: $scope, $timeout: $timeout, $element: $element}); // not neces...
        ctrl = this;
        console.log('ctrl', ctrl);
        this.reportOption = [];
        // console.log($scope.uc);
        for (var i = 0; i < reportOption.length; i++) {
            // console.log($scope.uc.report[reportOption[i].type]);
            if ($scope.uc.report[reportOption[i].type].isShow) {
                reportOption[i].isPrint = $scope.uc.report[reportOption[i].type].isPrint;
                this.reportOption.push(reportOption[i]);
            }
        }
        // this.reportOption = reportOption;

        /*$http.get('data/dummy_report.json').success(function (data) {
            $scope.dailyReport = data.daily;
            console.log($scope.dailyReport);
            // console.log(data);
        });*/

        // console.log($scope.uc);
        $scope.currentReport = reportOption[0].name; // default, first report
        $scope.init = function () {
            console.log('init report');
            SocketFactory.emit('loadReport', { membercode: $scope.loginValue, isPrint: false }, function (r) {
                //console.log(389, r);
                $scope.reportData = JSON.parse(r);
            })
        }

        MainService.sceneInitStatus['report'] = true;
        MainService.initReport = $scope.init;
        $scope.init();


        $scope.reportOptionPerPage = 5 * 5 - 2;
        $scope.noOfReportOptionPage = Math.ceil(ctrl.reportOption.length / $scope.reportOptionPerPage);
        // console.log($scope.reportOptionPerPage);
        // console.log(ctrl.reportOption.length);
        // console.log($scope.noOfReportOptionPage);
        $scope.cleanObj = {
            currentCleanTime: null,
            changeCleanTime: function () {
                if ($scope.dateFrom) {
                    $scope.setReport();
                } else {
                    $scope.changeReport('daily_Clearance_Report');
                }

            }
        }
        $scope.btnPrintShow = true;
        $scope.isShowPrint = function () {
            return $scope.btnPrintShow;
        }

        $scope.changeReport = function (code_name) {

            //$('.btn-printReport').hide();
            console.log('current report ' + code_name);
            var reportType = '';
            reportOption.forEach(function (reportOpt) {
                if (reportType === '' && reportOpt.name === code_name) {
                    reportType = reportOpt.type;
                    $scope.btnPrintShow = reportOpt.isPrint;
                }
            });
            var postData = { "type": reportType, membercode: $scope.loginValue, isPrint: false, selectCleanTime: $scope.cleanObj.currentCleanTime };

            if (reportType == 'dailyVoidECRReport' || reportType == 'dailyClearanceReport') {
                postData.till = userConfig.page.till;
            }

            SocketFactory.emit("loadReport", postData, function (r) {

                r = JSON.parse(r);
                //console.debug(440, r);
                if (reportType == 'daily_Clearance_Report' || reportType == 'dailyClearanceReport') {
                    $scope.cleanObj.cleanTimeList = r.cleanTimeList;
                    $scope.cleanObj.currentCleanTime = r.cleanTime;
                }
                console.log("type: " + reportType)
                $scope.reportData = r;
                $scope.currentReport = code_name;
            });

            // SocketFactory.emit('loadReport', { membercode: $scope.loginValue,isPrint:false }, function (r) {
            //     console.log(r);
            //     $scope.reportData = JSON.parse(r);
            //     $scope.currentReport = code_name;
            // })
            // replace report info here
        }

        $scope.printReport = function () {

            // needle
            // alert(userConfig.page.printer)
            // return

            var reportType = '';
            reportOption.forEach(function (reportOpt) {
                if (reportType === '' && reportOpt.name === $scope.currentReport) {
                    reportType = reportOpt.type;
                }
            });


            var postObj = { "type": reportType, membercode: $scope.loginValue, isPrint: true, printer: userConfig.page.printer, selectCleanTime: $scope.cleanObj.currentCleanTime };

            if ($scope.selectedDateRange.length > 0) {
                var dateFromSplit = $scope.dateFrom.split('-');
                postObj.year = dateFromSplit[0];
                postObj.month = dateFromSplit[1];
                postObj.day = dateFromSplit[2];


                if ($scope.selectedDateRange.length > 1) {
                    var dateToSplit = $scope.dateTo.split('-');
                    postObj.toYear = dateToSplit[0];
                    postObj.toMonth = dateToSplit[1];
                    postObj.toDay = dateToSplit[2];
                }
            }

            if ($scope.currentReport == 'daily_void_ECR_report' || $scope.currentReport == 'daily_Clearance_Report') {
                postObj.till = userConfig.page.till;
            }

            console.log('loadReport postObj', postObj)
            SocketFactory.emit('loadReport', postObj, function (r) {
                r = JSON.parse(r);
                //console.log(496, r);
                if ($scope.currentReport == 'daily_Clearance_Report' || $scope.currentReport == 'dailyClearanceReport') {
                    $scope.cleanObj.cleanTimeList = r.cleanTimeList;
                    $scope.cleanObj.currentCleanTime = r.cleanTime;
                }

                $scope.reportData = r;
                $scope.currentReport = $scope.currentReport;
            })

            /*
            if ($scope.year != "") {
                SocketFactory.emit('loadReport', { "type": reportType, membercode: $scope.loginValue, isPrint: true, printer: userConfig.page.printer, year: $scope.year, month: $scope.month, day: $scope.day }, function (r) {
                    //console.log(r);
                    $scope.reportData = JSON.parse(r);
                    $scope.currentReport = $scope.currentReport;
                })
            } else {
                SocketFactory.emit('loadReport', { "type": reportType, membercode: $scope.loginValue, isPrint: true, printer: userConfig.page.printer }, function (r) {
                    //console.log(r);
                    $scope.reportData = JSON.parse(r);
                    // $scope.currentReport = code_name;
                })

            }*/
        }

        // $scope.dailyClearance = function () {
        //     SocketFactory.emit('dailyClearance', { usercode: $scope.MainService.UserManager.staffCode, isPrint: true }, function (r) {
        //         console.log(r);
        //         r = JSON.parse(r);
        //         if (r.result == 'OK') {
        //             $scope.reportData = r.resultObj;
        //         } else {
        //             var msg = "請完成所有訂單";
        //             if (r.msg == "please_complete_orders") {
        //                 msg = "請完成所有訂單";
        //             } else if (r.msg == "no_order") {
        //                 msg = "沒有訂單";
        //             }
        //             $mdDialog.show(
        //              $mdDialog.alert()
        //                .parent(angular.element(document.body))
        //                .title(uiLanguage[INFO.lang]['alert'])
        //                //.content(uiLanguage[INFO.lang]['no_order'])
        //                 .content(msg)
        //                // .content('There is no order')
        //                .ok(uiLanguage[INFO.lang]['ok'])
        //            );
        //         }
        //         //$scope.currentReport = code_name;
        //     })
        // }

        $scope.setActive = function (element, callback) {
            console.log('set active report date');
            // console.log(element);
            if (typeof element === 'object') {
                // treat as event object
                var target = element.currentTarget || element.srcElement;
            } else {
                var target = element;
            }
            // console.log(element);
            // console.log(target);
            if ($scope.selectedDateRange.length <= 2) {
                angular.element(target).addClass("selected");
            }

            if ($scope.selectedDateRange.length > 2) {
                // console.log('reset active');
                angular.element(target).parents('.day-content').find('.selected').removeClass("selected");
                angular.element(target).addClass("selected");
            }

            (callback || angular.noop)();
        }

        /*
            version1: params: (year, month, day)
            version2: support range selection

            1. first click, determine first range,
            2. 2nd click, determine range boundary (starts over again if click again afterward)
            3. click again, go to first click again

            {from: [2015,10,1], to: [2015,10,16]}
        */
        $scope.selectedDateRange = [];
        $scope.dateFrom = $scope.dateTo = '';
        $scope.setReportDate = function (year, month, day, element) {
            console.log('oh my god');
            console.log($scope);
            console.log(day);

            $scope.selectedDateRange.push({ year: year, month: month, day: day });
            if ($scope.selectedDateRange.length == 1 || $scope.selectedDateRange.length > 2) {
                // assume from date
                $scope.dateFrom = year + '-' + month + '-' + day;
                $scope.dateTo = '';
            }
            $scope.setActive(element);

            if ($scope.selectedDateRange.length == 2) {
                // sort and compare before display
                var d1 = $scope.selectedDateRange[0],
                    d2 = $scope.selectedDateRange[1]
                // console.log(new Date(d1.year, d1.month - 1, d1.day).getTime() > new Date(d2.year, d2.month - 1, d2.day).getTime() );
                if (new Date(d1.year, d1.month - 1, d1.day).getTime() > new Date(d2.year, d2.month - 1, d2.day).getTime()) {
                    // display d2 in the second place
                    $scope.dateTo = d1.year + '-' + d1.month + '-' + d1.day;
                    $scope.dateFrom = d2.year + '-' + d2.month + '-' + d2.day;
                } else {
                    // display d2 in first and d1 in second
                    $scope.dateFrom = d1.year + '-' + d1.month + '-' + d1.day;
                    $scope.dateTo = d2.year + '-' + d2.month + '-' + d2.day;
                }
            }

            if ($scope.selectedDateRange.length > 2) {
                // reset from step 1
                $scope.selectedDateRange = [{ year: year, month: month, day: day }];
            };
            // console.log($scope.dateFrom);
            // console.log($scope.dateTo);
            // $mdDialog.hide(); // use confirm button instead

            console.log($scope.dateFrom);
            console.log($scope.dateTo);
            console.log($scope.selectedDateRange);
            // $scope.setHeaderRemark('選擇範圍：2015-10-23 至 2015-10-31');



            // $scope.fromYear = option.from[0];
            // $scope.fromMonth = month;
            // $scope.fromDay = day;

            // $scope.toYear = year;
            // $scope.toMonth = month;
            // $scope.toDay = day;


            // SocketFactory.emit('loadReport', { membercode: $scope.loginValue, isPrint: false, year: $scope.year, month: $scope.month, day: $scope.day }, function (r) {
            //     console.log(r);
            //     $scope.reportData = JSON.parse(r);
            //     // $scope.currentReport = code_name;
            // })
        }

        $scope.setReport = function () {
            // submit date and close dialog
            console.log($scope.dateFrom);
            console.log($scope.dateTo);
            $mdDialog.hide();
            var postObj = { type: $scope.currentReport, membercode: $scope.loginValue, isPrint: false, selectCleanTime: $scope.cleanObj.currentCleanTime };
            if ($scope.selectedDateRange.length > 0) {
                var dateFromSplit = $scope.dateFrom.split('-');
                postObj.year = dateFromSplit[0];
                postObj.month = dateFromSplit[1];
                postObj.day = dateFromSplit[2];
                if ($scope.selectedDateRange.length > 1) {
                    var dateToSplit = $scope.dateTo.split('-');
                    postObj.toYear = dateToSplit[0];
                    postObj.toMonth = dateToSplit[1];
                    postObj.toDay = dateToSplit[2];
                }
            }

            if ($scope.currentReport == 'daily_void_ECR_report' || $scope.currentReport == 'daily_Clearance_Report') {
                postObj.till = userConfig.page.till;
            }

            $scope.cleanObj.cleanTimeList = [];
            SocketFactory.emit('loadReport', postObj, function (r) {
                r = JSON.parse(r);
                //console.log(672, r);
                if ($scope.currentReport == 'daily_Clearance_Report' || $scope.currentReport == 'dailyClearanceReport') {
                    $scope.cleanObj.cleanTimeList = r.cleanTimeList;
                    $scope.cleanObj.currentCleanTime = r.cleanTime;
                    console.debug(672, $scope.cleanObj);
                }

                $scope.reportData = r;




                // $scope.currentReport = code_name;
            })

        }

        // $scope.year = '';
        // $scope.month = '';
        // $scope.day = '';

        $scope.calendarInfo = {
            reportType: ''
        };

        $scope.closeDialog = function ($mdDialog) {
            // $scope.calendarInfo.reportType = $scope.calendarInfo.lastReportType;
            $mdDialog.cancel();
        };

        $scope.callCalendar = function (dialogCtrl) {
            console.log('call calendar');
            //console.log(event);
            $scope.calendarInfo.lastReportType = $scope.calendarInfo.reportType;
            //console.log(angular.element('md-dialog[aria-label="keypad"]').scope());
            $scope.calendarInfo.value = '';
            if (angular.element('md-dialog[aria-label="calendar"]').scope() == undefined) {
                $mdDialog.show({
                    controller: dialogCtrl,
                    templateUrl: 'tmpl/calendar.html',
                    // targetEvent: event,
                    escapeToClose: false,
                    focusOnOpen: true,
                    bindToController: true,
                    scope: $scope,
                    preserveScope: true,
                    // for generic purpose, each instance only affect local property keypad value
                    locals: {
                        calendarInfo: $scope.calendarInfo,
                    }
                })

                $rootElement.on('keyup', function (e) {
                    // console.log("its's me");
                    if (e.keyCode === 27) {
                        $timeout($mdDialog.hide);
                        $scope.calendarInfo.reportType = $scope.calendarInfo.lastReportType;
                    }
                });
            } else {
                $mdDialog.hide();
            }
        };

        $scope.todayReport = function () {
            console.log('called');
            console.log($scope.currentReport)
            $scope.selectedDateRange.length = 0;
            $scope.dateFrom = '';
            $scope.dateTo = '';

            var day = new Date;
            //this.setReport( day.getFullYear(), day.getMonth() + 1, day.getDate());
            this.setReport();
        }

        $scope.dialogCtrl = function ($scope, $mdDialog, calendarInfo) {
            $scope.calendarInfo = calendarInfo;
        };
    });

    app.controller('clearanceController', function ($scope, uiLanguage, INFO, userConfig, SocketFactory, MainService, $mdDialog) {
        // console.log($scope);
        this.reportOption = reportOption;

        // load default report (incompleted report)

        $scope.currentReport = reportOption[0].name; // default, first report

        var args = { membercode: $scope.loginValue, isPrint: false, till: userConfig.page.till };
        //console.log(args);

        if (userConfig.ecrPayment) args.ecrClearance = true;
        args.isClearanceCalled = true;
        SocketFactory.emit('loadReport', args, function (r) {
            //console.log(767, r);
            $scope.reportData = JSON.parse(r);
        })

        $scope.dailyClearance = function () {

            // needle
            // alert(userConfig.page.printer)
            // return

            var content = $scope.ui['generic_action_confirm'].replace('%1', $scope.ui['daily_clearance']),
                title = $scope.ui['generic_action_confirm_title'].replace('%1', $scope.ui['daily_clearance']);
            MainService.showConfirm(content, title, function () {
                console.log('confirmed daily clearance item');

                SocketFactory.emit('dailyClearance', { usercode: $scope.MainService.UserManager.staffCode, isPrint: true, printer: userConfig.page.printer, till: userConfig.page.till, isUseECR: userConfig.ecrPayment, isClearanceCalled: true }, function (r) {
                    console.log(r);
                    r = JSON.parse(r);
                    if (r.result == 'OK') {
                        $scope.reportData = r.resultObj;
                    }
                    else {
                        var msg = "請完成所有訂單";
                        if (r.msg == "please_complete_orders") {
                            msg = "請完成所有訂單";
                        } else if (r.msg == "no_order") {
                            msg = "沒有訂單";
                        } else if (r.msg == "not_complete_payment") {
                            msg = "<span style='color:red'>以下帳單付款金額不正確</span><br/>"
                            r.notCompleteBill.forEach(function (b) {
                                msg += b.refNo + "<br/>";
                            })
                        }
                        // $mdDialog.show(
                        //  $mdDialog.alert()
                        //    .parent(angular.element(document.body))
                        //    .title(uiLanguage[INFO.lang]['alert'])
                        //    //.content(uiLanguage[INFO.lang]['no_order'])
                        //     .content(msg)
                        //    // .content('There is no order')
                        //    .ok(uiLanguage[INFO.lang]['ok'])
                        //);

                        var alert = $mdDialog.show({
                            template:
                              '<md-dialog aria-label="List dialog">' +
                              '  <md-dialog-content>' +
                              '<h2 class="md-title">' + title + '</h2>' +
                              '<p>' + msg + '</p>' +
                              '<button class="md-primary md-button md-default-theme" style="float:right;font-size:18px;margin:0" ng-click="btnClick();">' + uiLanguage[INFO.lang]['ok'] + '</button>' +
                              '  </md-dialog-content>' +
                              '</md-dialog>',
                            controller: function ($scope) {
                                $scope.btnClick = function () {
                                    $mdDialog.cancel();
                                }
                            }
                        });
                    }
                    //$scope.currentReport = code_name;
                })
            }, function () {

            });
        }

        $scope.resetPage = function () {

            SocketFactory.emit('loadReport', { membercode: $scope.loginValue, isPrint: false }, function (r) {
                //console.log(836, r);
                console.log('called');
                $scope.reportData = JSON.parse(r);
                //without called in Clearance
            })
        }
        MainService.sceneInitStatus['dailyClearance'] = true;
        MainService.resetPage['dailyClearance'] = $scope.resetPage;
        console.log(MainService.resetPage);
    });

    // temp storage for easy development
    app.controller('cashManageController', function ($scope, $element, $filter, $timeout, uiLanguage, INFO, $http, SocketFactory, MainService) {

        // $scope.goto = function( mode ) {
        //     MainService.modeCash = MainService.schema.modeCash[mode];

        // }
        // $scope.cashAmount = 0;
        $scope.startAmount = function () {
            // MainService.switchMode2({modeCash:'open'});
        }

        $scope.pettyCash = function () {
            // MainService.switchMode2({modeCash:'petty'});
        }

        $scope.selectPetty = function () {

        }

        $scope.init = function () {
            console.log('init cashbox');
            var _o = { 'cash': 0, 'type': 'open', 'remark': '', 'staffId': MainService.UserManager.staffCode };
            SocketFactory.emit('cashControl', _o, function (r) {
                // console.log(r);
                $scope.reportData = JSON.parse(r);
                // $scope.cashReport = JSON.parse( r ).CASH_RECORDS;

                $timeout(function () {
                    MainService.scrollToBottom(angular.element('.report-content'));
                }, 10);
            });
        }

        MainService.switchMode2({ modeCash: 'open' });
        MainService.sceneInitStatus['cashControl'] = true;
        MainService.initCashControl = $scope.init;
        $scope.init();

        $scope.setAmount = function () {
            // amount is this.cashAmount
            var t = ''
            if (MainService.modeCash == MainService.schema.modeCash.open) {
                t = 'open';
            } else if (MainService.modeCash == MainService.schema.modeCash.petty) {
                t = 'petty';
            }

            var _o = { 'cash': parseFloat(String(this.cashAmount).trim().replace(/,/g, '')), 'type': t, 'remark': '', 'staffId': MainService.UserManager.staffCode };
            SocketFactory.emit('cashControl', _o, function (r) {
                console.log(r);
                $scope.reportData = JSON.parse(r);
                // $scope.cashReport = JSON.parse( r ).CASH_RECORDS;

                $timeout(function () {
                    MainService.scrollToBottom(angular.element('.report-content'));
                }, 10);
            });
        }

        $scope.inputFree = function (val) {
            // console.log($scope.cashAmount);

            this.appendNumFree(val);
        };

        $scope.appendNumFree = function (val) {

            // do nothing if floating point is already input
            /*var inputValue = String( this.cashAmount )
            if( inputValue === 'undefined')
                inputValue = '';
            console.log(parseFloat( inputValue.concat(val) ));
            this.cashAmount = parseFloat( inputValue.concat(val) );*/
            // default

            // if floating point is activated, not allow to add one character
            // this.inputPrice += '';
            this.cashAmount = this.cashAmount ? this.cashAmount : 0;
            this.cashAmount = String(this.cashAmount).trim().replace(/,/g, ''); // type aware method and meaning is obvious without comment

            // since Angular filter seems malfuncationing or weird formatting after certain big amounts, allow only up to billion
            if (this.cashAmount.replace('.', '').length > 11) {
                // console.log('7');
                this.cashAmount = $filter('number')(parseFloat(this.cashAmount)); // original
                return
            }

            // first time "dot"
            if (this.cashAmount.indexOf('.') === -1 && val === '.') {
                console.log('1');
                // this.cashAmount = this.cashAmount.trim().concat( val );
                this.cashAmount = String($filter('number')(parseFloat(this.cashAmount))) + '.';
                return;
            }

            // already have dot, not allowed
            if (this.cashAmount.indexOf('.') != -1 && val === '.') {
                console.log('2');
                this.cashAmount = $filter('number')(parseFloat(this.cashAmount)); // original
                return
            }

            // already have dot and already have 1 decimal (limited to 1 decimal)
            if (this.cashAmount.indexOf('.') != -1 && !isNaN(parseInt(this.cashAmount[this.cashAmount.length - 1]))) {
                console.log("3");
                this.cashAmount = $filter('number')(parseFloat(this.cashAmount)); // original
                return;
            }

            if (this.cashAmount.trim() == "0") { // 排除預設等值有space的障礙
                console.log("4");
                this.cashAmount = val;
            } else {
                console.log("5");
                console.log(this.cashAmount);
                console.log(this.cashAmount.concat(val));
                this.cashAmount = $filter('number')(parseFloat(this.cashAmount.concat(val)));
            }
        }

        $scope.close = function () {
            MainService.switchMode2({ mode: 'main' }, false);
        }

        $scope.cancel = function () {
            if (this.cashAmount != '' || this.cashAmount != 0)
                this.cashAmount = '';
            else
                MainService.switchMode2({ mode: 'main' });
        }

        $scope.backspace = function () {
            // console.log('here!');
            // $scope.loginValue = "12347";
            var str = String(this.cashAmount).trim().replace(/,/g, '');

            // already have dot, does not allow filtering since . is at the end of string
            if (this.cashAmount.indexOf('.') != -1) {
                console.log("6");

                this.cashAmount = str.substring(0, str.length - 1);
            } else {
                this.cashAmount = $filter('number')(parseFloat(str.substring(0, str.length - 1))); // original
            }

        };

        /*$scope.contentFlexSize = function(){
         // modeChecking([{modeCash: [\'petty\']}])
         // console.log('contentFlexSize');
         // console.log(MainService.modeChecking({mode: ['cashManagement'], modeCash: ['open']}));
         // console.log(MainService.modeChecking({mode: ['cashManagement'], modeCash: ['petty']}));
             if( MainService.modeCash == MainService.modeCash.open ) {
                 console.log('100 -0------------------------------------');
                 return 100;
             }
 
             if( MainService.modeCash == MainService.modeCash.petty ) {
                 console.log('40 -0------------------------------------');
 
                 return 40;
             }
 
             console.log('nothing by default');
        }*/

        $scope.resetPage = function () {
            $element.find('.btn.important.active').removeClass("active");
            MainService.modeCash = MainService.schema.modeCash.normal;
        }
        MainService.resetPage['cashManagement'] = $scope.resetPage;

        // event
        $element.find('.btn.important').on('click', function (e) {
            // console.log(this);
            // console.log(e);
            $scope.reportData = null; // init
            console.log($scope.Data);
            angular.element(this).parents('.row').find('.active.important').removeClass("active");
            angular.element(this).addClass("active");
        });

        $element.find('.petty-list').on('click', '.btn', function (e) {
            angular.element(this).parents('.row').find('.active').removeClass("active");
            angular.element(this).addClass("active");
        });

        $scope.$watch('cashAmount', function (newValue, oldValue) {
            oldValue = String(oldValue).replace(/,/g, '');
            newValue = String(newValue).replace(/,/g, '');

            // access new and old value here
            //console.log("Your former user.name was " + oldValue + ", you're current user name is " + newValue + ".");
            if (isNaN(newValue) || newValue.indexOf('-') != -1) {
                //console.log('debug' + attrs.ngModel);
                //console.log('debug' + scope.login.value);
                $scope.cashAmount = oldValue ? oldValue : '';
            }

            if (newValue.indexOf('.') != -1 && (newValue.match(/\./g) || []).length == 1) {
                console.log('test');
                console.log(newValue);
                //console.log('debug' + attrs.ngModel);
                //console.log('debug' + scope.login.value);
                $scope.cashAmount = newValue;
            } else {
                // keep
                $scope.cashAmount = $filter('number')(String($scope.cashAmount).trim().replace(/,/g, ''));
            }
        }, true);

        // $element.find('input').on('keyup keypress', function(e){
        //     console.log(e);
        //     // console.log(isNaN( parseInt(String.fromCharCode(e.charCode)) ));
        //     //  e.preventDefault();
        //     console.log(e.keyCode);
        //     console.log( String.fromCharCode(e.keyCode) );
        //     $scope.cashAmount = 1
        //     if( isNaN( parseInt(String.fromCharCode(e.keyCode)) ) ) {
        //      console.log('test');
        //      var str = String( $scope.cashAmount );
        //      $scope.cashAmount = $filter('number')(parseFloat( str.substring(0, str.length - 1) ) );
        //     }
        //     // return 'asdasd'
        // });
    });

    app.controller('billingController', function ($scope, $timeout, $element, uiLanguage, INFO, $http, SocketFactory, MainService) {
        // $scope.optionPerPage = 12 - 2;
        // $scope.currentTenderOptionPage = $scope.currentDiscountOptionPage = 1;

        // console.log();
        // put in separate controller later: printBillController
        $scope.cancel = function () {
            MainService.mode = MainService.schema.mode.floorPlan;
            // MainService.modeOrder = MainService.schema.modeOrder.normal;
            $scope.$emit('cancelOrder', []);
        }

        // console.log($scope.config);
        $scope.printBill = function (tableNo) {
            console.log('bill table no to print ?');
            console.log(MainService.Cart.tableNo);
            MainService.printOrder(MainService.Cart.tableNo, 'bill');
            MainService.mode = MainService.schema.mode.floorPlan;
            MainService.modeOrder = MainService.schema.modeOrder.normal;
        }

        // console.log($element.find('.md-grid-button'));
        // $timeout(function () {
        //     $element.find('.md-grid-button')
        //         .fitText(0.55, { minFontSize: '21px', maxFontSize: '24px'}); 
        // }, 10);


        // jQuery(".md-grid-button").fitText(0.55, { minFontSize: '21px', maxFontSize: '48px'}); 
    });

    app.controller('searchBillControllerDelivery', function ($scope, MainService, uiLanguage, INFO, $http, SocketFactory, $mdDialog) {
        $scope.inputFree = function (val) {
            console.log($scope.billNo);
            this.appendNumFree(val);
        };


        $scope.lastBill = function () {
            $scope.loadBill(null);
        }

        $scope.searchBill = function () {
            $scope.loadBill({ "searchNo": this.billNo });
        }

        $scope.appendNumFree = function (val) {
            // do nothing if floating point is already input
            var inputValue = String(this.billNo)
            if (inputValue === 'undefined')
                inputValue = '';

            this.billNo = inputValue.concat(val);
            // default
        }

        $scope.cancel = function () {
            this.billNo = '';
        }

        $scope.browseBill = function (direction) {
            // prev
            $scope.loadBill({ "refNo": MainService.Cart.refNo, "direction": direction });
            // next
            //alert(MainService.Cart.refNo)
        }

        $scope.loadBill = function (args) {


            SocketFactory.emit('LoadBillByDelivery', args, function (r) {
                r = JSON.parse(r);
                console.log(r);

                if (typeof r.order[0] == 'undefined')
                    return;


                if (typeof r.order[0] != 'undefined' && r.order[0].header.orderId == '') {
                    $mdDialog.show(
                     $mdDialog.alert()
                       .parent(angular.element(document.body))
                       .title(uiLanguage[INFO.lang]['alert'])
                       .content(uiLanguage[INFO.lang]['no_order'])
                       // .content('There is no order')
                       .ok(uiLanguage[INFO.lang]['ok'])
                   );
                } else {
                    var order = r.order[0];
                    MainService.modeMultiSelectItem = false;
                    //MainService.mode = MainService.schema.mode.order;
                    //MainService.modeOrder = MainService.schema.modeOrder.normal;
                    MainService.Cart.tableNo = order.header.tableNum;
                    MainService.Cart.noOfPeople = '';
                    MainService.Cart.refNo = order.header.refNo;
                    MainService.Cart.transTime = order.header.transTime;
                    MainService.Cart.member = {};
                    //console.log(angular.element('input-keypad').scope());
                    MainService.Cart.cartList = [];
                    MainService.Cart.orderList = [];
                    MainService.appliedPaymentMethod = [];
                    MainService.Cart.couponObj = [];
                    MainService.Cart.noOfPeople = order.header.peopleNum;
                    if (order.header.member != undefined) {
                        MainService.Cart.member = $.extend(true, order.header.member, MainService.schema.member);
                    }

                    MainService.assign2OrderList(order, MainService.Cart.orderList);
                    MainService.calcPrice([MainService.Cart.orderList], MainService.Cart.couponObj);

                    MainService.remainder();
                    MainService.checkPaymentTips();
                }
            })
        }


    });

    app.controller('searchBillController', function ($scope, MainService, uiLanguage, INFO, $http, SocketFactory, $mdDialog) {
        console.log('searchBillController');
        // for searching bill

        MainService.modeFloorPlan = 0;


        $scope.voidBill = function (refNo) {
            refNo = MainService.Cart.refNo;

            MainService.checkPermission('voidBill', function () {
                var isCard = false;
                var cardType = null;
                MainService.appliedPaymentMethod.forEach(function (m) {
                    var templ = MainService.tender.filter(function (t) {
                        return t.code == m.method;
                    });
                    if (templ[0].tenderType == 'card') {
                        isCard = true;
                        cardType = m.method;
                        return false;
                    }
                });

                var confirm = $mdDialog.confirm()
                           .title(uiLanguage[INFO.lang]['alert'])
                           .content(uiLanguage[INFO.lang]['voidBill_alert_content'])
                           .ok(uiLanguage[INFO.lang]['yes'])
                           .cancel(uiLanguage[INFO.lang]['cancel']);
                $mdDialog.show(confirm).then(function () {

                    var tempArgs = {};
                    tempArgs.refNo = refNo;
                    tempArgs.staffCode = MainService.UserManager.staffCode;
                    tempArgs.printer = userConfig.page.printer;
                    tempArgs.isCard = isCard;
                    tempArgs.cardType = cardType;
                    tempArgs.till = $scope.uc.page.till;
                    tempArgs.isUseECR = userConfig.ecrPayment;
                    var showAlert = true;
                    if (isCard && userConfig.ecrPayment) {
                        var content = "請等待";
                        var title = uiLanguage[INFO.lang]['alert'];

                        var alert = $mdDialog.show({
                            template:
                              '<md-dialog aria-label="List dialog">' +
                              '  <md-dialog-content>' +
                              '<h2 class="md-title">' + title + '</h2>' +
                              '<p>' + content + '</p>' +
                              '<button class="md-primary md-button md-default-theme" style="float:right;font-size:18px;margin:0" ng-click="btnClick();">手動</button>' +
                              '  </md-dialog-content>' +
                              '</md-dialog>',
                            controller: function ($scope) {
                                $scope.btnClick = function () {
                                    tempArgs.isUseECR = false;
                                    showAlert = false;
                                    postSocket(tempArgs);
                                }
                            }
                        });

                    }


                    function postSocket(tempArgs) {
                        SocketFactory.emit('voidTakeOutBill', tempArgs, function (r) {
                            $mdDialog.cancel();
                            if (r == "OK") {
                                $scope.lastBill(null, true);
                            } else if (r == "fail" && showAlert) {
                                MainService.showAlert('取消帳單失敗');
                            }


                        })
                    }

                    postSocket(tempArgs, true);

                });

            })
        }
        $scope.printBill = function () {

            //alert(MainService.Cart.refNo);
            SocketFactory.emit('printBill', { "refNo": MainService.Cart.refNo, printer: userConfig.page.printer }, function (r) {

            })
        }

        $scope.loadBill = function (args, isVoid) {
            SocketFactory.emit('loadBill', args, function (r) {
                r = JSON.parse(r);
                console.log(r);

                if (r.order.length == 0 && isVoid) {
                    MainService.switchMode2({ mode: 'main' });
                    return
                }

                if (typeof r.order[0] == 'undefined') return;

                if (typeof r.order[0] != 'undefined' && r.order[0].header.orderId == '') {
                    $mdDialog.show(
                     $mdDialog.alert()
                       .parent(angular.element(document.body))
                       .title(uiLanguage[INFO.lang]['alert'])
                       .content(uiLanguage[INFO.lang]['no_order'])
                       // .content('There is no order')
                       .ok(uiLanguage[INFO.lang]['ok'])
                   );
                } else {

                    var order = r.order[0];
                    MainService.modeMultiSelectItem = false;
                    //MainService.mode = MainService.schema.mode.order;
                    //MainService.modeOrder = MainService.schema.modeOrder.normal;
                    console.log('order', order);
                    MainService.Cart.tableNo = order.header.tableNum;
                    MainService.Cart.noOfPeople = '';
                    MainService.Cart.refNo = order.header.refNo;
                    MainService.Cart.isVoid = order.header.void;
                    MainService.Cart.transTime = order.header.transTime;
                    MainService.Cart.member = {};
                    //console.log(angular.element('input-keypad').scope());
                    MainService.Cart.cartList = [];
                    MainService.Cart.orderList = [];
                    MainService.appliedPaymentMethod = [];
                    MainService.Cart.couponObj = [];
                    MainService.Cart.noOfPeople = order.header.peopleNum;
                    MainService.deposit.appliedDeposit = [];
                    if (order.header.member != undefined) {
                        MainService.Cart.member = $.extend(true, order.header.member, MainService.schema.member);
                    }
                    // var lastInsertType = "",
                    //     lastInsertIdx = 0,
                    //     lastInsertOptIdx = -1;
                    // angular.forEach(order.item, function (v, k) {
                    //     var item = $.extend(true, {}, MainService.schema.baseItem, v);
                    //     // var item = $.extend(true, {}, MainService.findItemFromFood(v.code), MainService.schema.baseItem, v);
                    //     console.log("index " + v.index, v.type, v);
                    //     if (item.type === "I" || item.type === "T") {
                    //         item.init();
                    //         lastInsertOptIdx = -1;
                    //         if (v.voidIndex != undefined) {
                    //             if (v.voidIndex != -1) {
                    //                 //MainService.Cart.orderList[v.voidIndex].voidQty = v.qty;
                    //                 $.each(MainService.Cart.orderList, function (idx, vitem) {
                    //                     if (vitem.index == v.voidIndex) {
                    //                         vitem.voidQty += v.qty;
                    //                     }
                    //                 });
                    //             }
                    //             item.voidRemark = v.voidRemark;
                    //         }
                    //         //console.log(item)
                    //         MainService.Cart.orderList.push(item);
                    //     } else if (item.type === "O") {
                    //         if (lastInsertIdx != item.index)
                    //             lastInsertOptIdx++;
                    //
                    //         if (MainService.Cart.orderList[MainService.Cart.orderList.length - 1].option == undefined)
                    //             MainService.Cart.orderList[MainService.Cart.orderList.length - 1].option = [];
                    //         if (MainService.Cart.orderList[MainService.Cart.orderList.length - 1].option[lastInsertOptIdx] == undefined)
                    //             MainService.Cart.orderList[MainService.Cart.orderList.length - 1].option[lastInsertOptIdx] = { items: [] };
                    //         MainService.Cart.orderList[MainService.Cart.orderList.length - 1].option[lastInsertOptIdx].items.push(item);
                    //
                    //         //if (MainService.Cart.orderList[MainService.Cart.orderList.length - 1] != undefined) {
                    //         //    angular.forEach(MainService.Cart.orderList[MainService.Cart.orderList.length - 1].option[lastInsertOptIdx].items, function (optItem) {
                    //         //        if (optItem.code === item.code) {
                    //         //            $.extend(optItem, item);
                    //         //        }
                    //         //    });
                    //         //}
                    //     } else if (item.type === "S") {
                    //         angular.forEach(MainService.Cart.orderList[MainService.Cart.orderList.length - 1].subitem, function (subitem) {
                    //             if (subitem.code === item.code) {
                    //                 $.extend(subitem, item);
                    //             }
                    //         });
                    //     } else if (item.type === "M") {
                    //         var m = MainService.food.modifiers.filter(function (matchItem) {
                    //             return matchItem.code == item.code;
                    //         });
                    //         if (MainService.Cart.orderList[MainService.Cart.orderList.length - 1] != undefined && item.index > 1 && lastInsertIdx == item.index && lastInsertOptIdx > -1) {
                    //             //console.log("insert option", MainService.Cart.orderList[MainService.Cart.orderList.length - 1].option);
                    //             //console.log(MainService.Cart.orderList[MainService.Cart.orderList.length - 1].option[lastInsertOptIdx].items);
                    //             angular.forEach(MainService.Cart.orderList[MainService.Cart.orderList.length - 1].option[lastInsertOptIdx].items, function (optItem) {
                    //                 if (optItem.index == item.index) {
                    //                     optItem.modifier.push(m[0]);
                    //                 }
                    //             });
                    //         }
                    //         else {
                    //             MainService.Cart.orderList[MainService.Cart.orderList.length - 1].modifier.push(m[0]);
                    //         }
                    //     } else if (item.type === "K") {
                    //         var k = MainService.food.kitchenmsg.filter(function (matchItem) {
                    //             return matchItem.code == item.code;
                    //         });
                    //         if (MainService.Cart.orderList[MainService.Cart.orderList.length - 1] != undefined && item.index > 1 && lastInsertIdx == item.index && lastInsertOptIdx > -1) {
                    //             //console.log("insert option", MainService.Cart.orderList[MainService.Cart.orderList.length - 1].option);
                    //             //console.log(MainService.Cart.orderList[MainService.Cart.orderList.length - 1].option[lastInsertOptIdx].items);
                    //             angular.forEach(MainService.Cart.orderList[MainService.Cart.orderList.length - 1].option[lastInsertOptIdx].items, function (optItem) {
                    //                 if (optItem.index == item.index) {
                    //                     optItem.kitchenMsgOld.push(k[0]);
                    //                 }
                    //             });
                    //         }
                    //         else {
                    //             MainService.Cart.orderList[MainService.Cart.orderList.length - 1].kitchenMsgOld.push(k[0]);
                    //         }
                    //     }
                    //     lastInsertIdx = item.index;
                    // });
                    // angular.forEach(MainService.Cart.orderList, function (order) {
                    //     if (order.option != undefined && order.option) {
                    //         angular.forEach(order.option, function (optionList) {
                    //             var selectQty = 0;
                    //             angular.forEach(optionList.items, function (optItem) {
                    //                 if (optItem.qty > 0) {
                    //                     selectQty += optItem.qty
                    //                 }
                    //             })
                    //             if (selectQty == 0) {
                    //                 var fillOption = $.extend(true, {}, MainService.schema.unselectOption, MainService.schema.baseItem);
                    //                 // var fillOption = $.extend(true, {}, MainService.schema.unselectOption, {qty: 1, _qty: 1});
                    //                 optionList.items.push(fillOption);
                    //             }
                    //         })
                    //     }
                    // });
                    //
                    // for (var i = 0; i < order.discount.length; i++) {
                    //
                    //     //coupon_alias: "off_20",
                    //     //amount: 0.8,
                    //     //type: 'percentage'
                    //     MainService.appliedDiscount.push({ 'coupon_alias': order.discount[i].discountCode, 'amount': order.discount[i].discountAmount, 'type': order.discount[i].discountType, 'price': order.discount[i].discountValue });
                    //     MainService.Cart.couponObj = [{ 'coupon_alias': order.discount[i].discountCode, 'amount': order.discount[i].discountAmount, 'type': order.discount[i].discountType }];
                    // }
                    //
                    // for (var i = 0; i < order.payment.length; i++) {
                    //     MainService.appliedPaymentMethod.push({ 'method': order.payment[i].paymentType, 'amount': order.payment[i].paymentValue })
                    // }
                    MainService.assign2OrderList(order, MainService.Cart.orderList);
                    MainService.calcPrice([MainService.Cart.orderList], MainService.Cart.couponObj);

                    MainService.remainder();
                    MainService.checkPaymentTips();
                }
            })
        }

        $scope.loadBill(null);


        $scope.lastBill = function () {
            $scope.loadBill(null);
        }

        $scope.successClick = function () {
            MainService.mode = MainService.schema.mode.main;
        }

        $scope.browseBill = function (direction) {
            // prev
            $scope.loadBill({ "refNo": MainService.Cart.refNo, "direction": direction });
            // next
            //alert(MainService.Cart.refNo)
        }


        $scope.inputFree = function (val) {
            console.log($scope.billNo);
            this.appendNumFree(val);
        };

        $scope.appendNumFree = function (val) {
            // do nothing if floating point is already input
            var inputValue = String(this.billNo)
            if (inputValue === 'undefined')
                inputValue = '';

            this.billNo = inputValue.concat(val);
            // default
        }

        $scope.searchBill = function () {
            $scope.loadBill({ "searchNo": this.billNo });
        }

        $scope.cancel = function () {
            this.billNo = '';
        }
        // for searching bill

        $scope.changeBillInfo = function () {
            // MainService.mode = MainService.schema.mode.order;
            MainService.checkPermission('changeTender', function () {
                MainService.cloneAppliedPaymentMethod = [];
                angular.copy(MainService.appliedPaymentMethod, MainService.cloneAppliedPaymentMethod);
                MainService.modeOrder = MainService.schema.modeOrder.amendBill;
            })
        }

        // $scope.changeCreditCard = function() {

        // }
    });

    app.controller('amendBillController', function ($scope, $timeout, $element, uiLanguage, INFO, $http, SocketFactory) {
        // init
        MainService.originalPriceInput = MainService.totalPriceInput;
        MainService.originalRemainerAmount = MainService.remainerAmount;
        // $scope.itemsPerPage = 3*8 - 2;
        // $scope.currentPage = 1;
        // $scope.totalItems = 1;
        $scope.paymentPerPage = 7 * 3 - 2;

        // for amending bill
        $scope.inputExact = function () {
            // MainService.checkPermission('changeBillAmount', function () {
            //     return;
            // })
           
            MainService.inputExact.apply(MainService, arguments);
        }

        function checkOriginal(func) {
            MainService.checkPermission('changeBillAmount', function () {
                func();
                // return;
            }, function () {
                // if no permission, only upper amount
                if (MainService.appliedPaymentMethod.length === 1) {
                    return; // if only one method means no changed
                } else {
                    // if input total amount > original, reset to the limit
                    // MainService.addAmount.apply( MainService, arguments );
                    //console.log('checkOriginal');
                    func();

                    if (MainService.totalPriceInput > MainService.originalPriceInput) {
                        var msg = $scope.ui['no_more_than_original_amount'].replace('%1', MainService.originalPriceInput);
                        for (var i = 0; i < MainService.appliedPaymentMethod.length; i++) {
                            console.log(MainService.appliedPaymentMethod[i]);
                            MainService.appliedPaymentMethod[i].amount = 0;
                        }
                        MainService.appliedPaymentMethod[MainService.appliedPaymentMethod.length - 1].amount = MainService.originalPriceInput;
                        MainService.remainerAmount = MainService.originalRemainerAmount;

                        // MainService.appliedPaymentMethod.amount = MainService.originalPriceInput; // reset
                        MainService.showAlert(msg);
                        return;
                    }
                }
            })


        }

        $scope.addAmount = function () {
            // MainService.checkPermission('changeBillAmount', function () {
            //     return;
            // })
            // if( MainService.appliedPaymentMethod.length === 1 ) {
            //     return; // if only one method means no changed
            // } else {
            //     // if input total amount > original, reset to the limit
            //     MainService.addAmount.apply( MainService, arguments );

            //     if( MainService.totalPriceInput > MainService.originalPriceInput ) {
            //         var msg = $scope.ui['no_more_than_original_amount'].replace('%1', MainService.originalPriceInput);
            //         for (var i = 0; i < MainService.appliedPaymentMethod.length; i++) {
            //             console.log(MainService.appliedPaymentMethod[i]);
            //             MainService.appliedPaymentMethod[i].amount = 0;
            //         }
            //         MainService.appliedPaymentMethod[MainService.appliedPaymentMethod.length-1].amount = MainService.originalPriceInput;
            //         MainService.remainerAmount = MainService.originalRemainerAmount;

            //         // MainService.appliedPaymentMethod.amount = MainService.originalPriceInput; // reset
            //         MainService.showAlert( msg );
            //         return;
            //     }
            // }
            var args = arguments;
            checkOriginal(function () { MainService.addAmount.apply(MainService, args); })


            // alert(MainService.totalPriceInput)


        }

        $scope.appendAmount = function () {
            var args = arguments;
            checkOriginal(function () { MainService.appendAmount.apply(MainService, args); })
            // MainService.appendAmount.apply( MainService, arguments );
        }

        $scope.truncateAmount = function () {
            MainService.truncateAmount.apply(MainService, arguments);
        }

        // $timeout(function () {
        //     $element.find('.md-grid-button')
        //         .fitText(0.55, { minFontSize: '21px', maxFontSize: '24px'}); 
        // }, 10);
    });

    // move by daniel
    app.controller('transferItemController', function ($scope, uiLanguage, INFO, $http, SocketFactory, MainService, $controller, $rootScope, $mdDialog, $element, $attrs, $timeout) {
        // console.log($scope.returnStatus);
        //console.debug('init transferItem');
   
        var option = [];
        ctrl = this;
        $scope.currentPage = 1;
        $scope.transferOptionPerPage = 5 * 5 - 2;

        $attrs.title = "Number of Qty";
        $controller('keypadController', { $scope: $scope, $mdDialog: $mdDialog, $element: $element, $attrs: $attrs, $timeout: $timeout, MainService: MainService });
  


        // $scope.noOfTransferItemPage = 13;
        SocketFactory.emit('loadTableForTransfer', null, function (r) {
            console.log(r);
            r = JSON.parse(r);
            // console.log(r.length);
            for (var i = 0; i < r.length; i++) {
                if (r[i].name != MainService.Cart.tableNo) {
                    option.push({ name: r[i].name, status: r[i].status });
                }
            }
            // console.log(option.length);
            $scope.option = option;
            // console.log($scope.option)
            //MainService.modeMultiSelectItem = true;
            MainService.modeMultiSelectItem = false;
            $scope.noOfTransferItemPage = Math.ceil(option.length / $scope.transferOptionPerPage);

        });



        $scope.submitTransfer = function (destinationTableNo, sourceTableNo, status) {
            // billed order is not allowed for transfering
            if (status == 4) {
                return;
            }

            var content = $scope.ui['transfer_item_confirm'].replace('%1', destinationTableNo),
                title = $scope.ui['transfer_item_confirm_title'].replace('%1', destinationTableNo);

            var transferList = []
            $scope.currentItemQty = 0;
            $.each(MainService.Cart.orderList, function (oi, oo) {
                if (oo.isEdit) {
                    $.each(MainService.Cart.cartList, function (ci, co) {
                        if (co.voidIndex == oo.index) {
                            MainService.showAlert('please_confirm_delete_food');
                            return;
                        }
                    });

                    transferList.push(oo);
                    $scope.currentItemQty += oo.qty;

                    $.each(MainService.Cart.orderList, function (vi, vo) {
                        if (vo.voidIndex == oo.index) {
                            //transferList.push(vo);
                            $scope.currentItemQty += oo.qty;
                        }
                    });
                }
            });

            var order = { item: [] };
     
            angular.forEach(transferList, function (item) {
                
                order.item.push({
                    index: [item.index]
                });
                
                if (item.option != undefined && item.option.length != 0) {
                    $.each(item.option, function (optionIndex, option) {
                        $.each(option.items, function (optionItemIndex, optionItem) {
                            if (optionItem.qty > 0) {
                                order.item.push({
                                    index: [optionItem.index]
                                });
                                $.each(optionItem.modifier, function (modifierIndex, modifier) {
                                    order.item.push({
                                        index: [modifier.index]
                                    });
                                })
                                $.each(optionItem.kitchenMsg, function (kitchenMsgIndex, kitchenMsg) {
                                    order.item.push({
                                        index: [kitchenMsg.index]
                                    });
                                })
                            }
                        })
                    });
                }
                if (item.subitem != undefined && item.subitem.length != 0) {
                    $.each(item.subitem, function (subitemIndex, subitem) {
                        order.item.push({
                            index: [subitem.index]
                        });
                    });
                }
                if (item.modifier.length != 0) {
                    $.each(item.modifier, function (modifierIndex, modifier) {
                        order.item.push({
                            index: [modifier.index]
                        });
                    })
                }
                if (item.kitchenMsg.length != 0) {
                    $.each(item.kitchenMsg, function (kitchenMsgIndex, kitchenMsg) {
                        order.item.push({
                            index: [kitchenMsg.index]
                        });
                    })
                }
            });


            if (transferList.length == 0) {
                MainService.showAlert('please_select_food');
                return;
            } else {
                //$scope.keypadInfo.value = "1";
                $scope.next = function (event) {
                    $mdDialog.hide();
                    //MainService.Cart.noOfPeople = $scope.keypadInfo.value;
                    //angular.element($element).blur();
                    console.debug($scope.currentItemQty);
                    MainService.showConfirm(content, title, function () {
                        var _o = {};
                        _o.transferList = order;
                        _o.staffCode = MainService.UserManager.staffCode;
                        _o.sourceTableNo = MainService.Cart.tableNo;
                        _o.destinationTableNo = destinationTableNo;
                        //console.log(destinationTableNo);
                       
                        if ($scope.keypadInfo.value > $scope.currentItemQty) $scope.keypadInfo.value = $scope.currentItemQty;
                        _o.transferList.item[0].qty = $scope.keypadInfo.value;

                        console.debug(_o)
                        return;
                        SocketFactory.emit('transferItem', _o, function (r) {
                            content = $scope.ui['transferred'].replace('%2', destinationTableNo).replace('%1', sourceTableNo);
                            title = $scope.ui['alert'];
                            MainService.showAlert(content, title);
                            r = JSON.parse(r);
                            var order = r.order[0];
                            MainService.Cart.orderList.length = 0;
                            MainService.assign2OrderList(order, MainService.Cart.orderList);
                        })
                    }, function () {

                    });
                };

                $scope.callKeypad(null, $scope.dialogTest1);
                
            }


        }


        $scope.return = function () {
            MainService.resetTitle.apply(MainService, arguments);
            MainService.modeOrder = MainService.schema.modeOrder.normal;
            MainService.modeMultiSelectItem = false;
            MainService.Cart.resetEditItem();
        }
    });

    app.controller('mainMenuController', function ($scope, uiLanguage, INFO, SocketFactory, MainService) {
        // just an alias to call, for lesser code in template purpose


        // even object is work from template ng-click
        $scope.test = function (arg) {
            //console.log('hahahahaahah');
            console.log(arg);
        }
    });

    app.controller('designInit', function ($scope) {
        $scope.keypad = keypad;
        // $scope.keypad = [];
        // for(var n=1; n<10; n++) {
        //     $scope.keypad.push({text: n});
        // }
        // $scope.keypad.push({text: 'reset'},{text: 0},{text: ''});
        // console.log($scope.keypad);


        // $scope.isReset = function(text){
        //     // console.log(text);
        //     if( typeof text != 'undefined' && text.toLowerCase() === 'reset') {
        //         return true;
        //     }
        //     return false;
        // }
    });
    //--------------------------------------------------------

    // prototype test
    // app.controller('selectableController', function($scope){
    //     console.log('selectable controller');
    //     $scope.setActive = function( $event, callback ) {
    //         console.log($event);
    //         var element = $event.currentTarget || $event.srcElement;
    //         // console.log('setactive????');
    //         // console.log( element );

    //         // can pass any params to callback if needed using arguments[2], [3]....
    //         if(angular.element( element ).hasClass("active")) {
    //             angular.element( element ).removeClass("active");
    //             (callback || angular.noop)()
    //             // $scope.removeFromBasket( callback || angular.noop );
    //         } else {
    //             angular.element( element ).addClass("active");
    //             (callback || angular.noop)()
    //             // $scope.addToBasket( callback || angular.noop );
    //         }
    //     };

    //     // $scope.addToBasket = function( callback ){
    //     //     callback();
    //     // };

    //     // $scope.removeFromBasket = function( callback ) {
    //     //     callback();
    //     // };
    // });

    // controllers created by Daniel for merge and test here
    app.controller('headerCtrl', function ($scope, $log, $mdDialog, $mdSidenav, $rootScope, $element, MainService, $mdToast, $controller, SocketFactory, uiLanguage, INFO) {
        $controller('tableController', { $scope: $scope }); // passing current scope to commmon controller
        // $controller('reportController',{$scope: $scope, $element: $element}); // passing current scope to commmon controller
        // console.log($scope);

        // $scope.headerRemark = reportService.remark;
        $scope.ui = uiLanguage[INFO.lang];
        $scope.user = '';
        $scope.MainService = MainService;

        // just an alias to call, for lesser code in template purpose
        $scope.switchMode = function (modeType, modeName, requiredLogin) {
            MainService.switchMode(modeType, modeName, requiredLogin);
        }

        // legacy method, change mode name and call svg methods, combine later with latest functions
        $scope.changeMode = function (mode, modeNo) {
            // MainService.switchModeFloorPlan( modeNo );
            MainService.RMSTable.changeMode(mode);
            this.cancelTransfer();
        }


        // console.log($scope.MainService);
        //SocketFactory.on('test', function (data) {
        //    console.log('socket connected 1' + data);
        //});

        $scope.nextZone = function () {
            // console.log("please chagne to next zone");
            $scope.$emit('nextZone', '');
        }

        $scope.prevZone = function () {
            // console.log("please change tp prev zone");
            $scope.$emit('prevZone', '');
        }

        $scope.login = function (ev) {
            if (angular.element('md-dialog[aria-label="Login"]').scope() == undefined) {
                $mdDialog.show({
                    templateUrl: 'tmpl/login.html',
                    targetEvent: ev,
                })
            } else {
                $mdDialog.hide();
            }
        }
        $scope.next = function () { console.log('headerctrl') }

        $scope.submitOrder = function (ev) {
            console.log('emit submit order');
            $scope.$emit('submitOrder', {});
        }

        $scope.modeFoodControl = function (ev) {
            //angular.element('order-ctrl').scope().mode = angular.element('order-ctrl').scope().mode === 'detail' ? '' : 'detail';
            //angular.element('order-ctrl').scope().$element.removeClass('ng-hide');
            angular.element('order-scene').scope().toggleFoodControl();

        }

        $scope.modeToggleOrder = function (ev) {
            console.log('toggle order?');
            angular.element('order-ctrl').scope().mode = angular.element('order-ctrl').scope().mode === angular.element('order-ctrl').scope().modeOrderDetail ? angular.element('order-ctrl').scope().modeOrder : angular.element('order-ctrl').scope().modeOrderDetail;
        }

        $scope.panelMember = function (ev) {
            if (angular.element('md-dialog[aria-label="Member"]').scope() == undefined) {
                $mdDialog.show({
                    controller: DialogControllerMember,
                    templateUrl: 'tmpl/member.html',
                    targetEvent: ev,
                })
            } else {
                $mdDialog.hide();
            }
        }

        $scope.toggleRight = buildToggler('left');

        function buildToggler(navID) {
            return function () {
                // default action for toggler
                // angular.element('.side-menu > li:eq(3) .md-button-toggle').click()
                $mdSidenav(navID)
                  .toggle()
                // .then(function () {
                //   $log.debug("toggle " + navID + " is done");
                // });
            }
        }

        // event
        // put it on respective controller to call MainService without doing it over and over again in rms-templates
        $element.find('.btn:not(".no-active, .toggle")').on('mousedown keyup touch', function (e) {
            MainService.setActive(e);
        });



        //$rootScope.$on('modeTableOrder', function (o) {
        //    $element.find('.orderPanelCtrl').removeClass('ng-hide');
        //});
    });

    // controllers for headCtrl->panelMember

    // function DialogControllerLogin($scope, $mdDialog, SocketFactory, MainService, uiLanguage, INFO) {
    //     // console.log("called once");
    //     console.log($scope);
    //     $scope.mode = 'username';
    //     $scope.modeName = 'Login';
    //     $scope.username = '';
    //     $scope.password = '';
    //     $scope.tableNum = '';
    //     $scope.keypad = keypad;
    //     $scope.ui = uiLanguage[INFO.lang];

    //     $scope.input = function (val) {
    //         angular.element(".login").addClass("md-input-focused");
    //         $scope.loginValue += '' + val;
    //     }

    //     $scope.reset = function (val) {
    //         angular.element(".login").addClass("md-input-focused");
    //         if ($scope.loginValue == '' && $scope.mode == 'username') {
    //             $mdDialog.cancel();
    //             // angular.element(".login").addClass("md-input-focused");
    //         } else if ($scope.loginValue == '' && $scope.mode == 'password') {
    //             $scope.modeName = 'Login';
    //             $scope.mode = 'username';
    //             $scope.loginValue = '';
    //         } else {
    //             $scope.loginValue = '';
    //         }
    //     }

    //     $scope.backspace = function () {
    //         // console.log('here!');
    //         // $scope.loginValue = "12347";
    //         $scope.loginValue = $scope.loginValue.substring(0, $scope.loginValue.length - 1);
    //     };

    //     $scope.next = function (val) {
    //         //console.log('********');
    //         //console.log($scope.tableNum);
    //         //console.log($scope.mode);
    //         //console.log($scope.loginValue);
    //         switch ($scope.mode) {
    //             case 'username':
    //                 $scope.username = $scope.loginValue;
    //                 MainService.UserManager.login($scope.username, '').then(function (r) {
    //                     //console.log(r);
    //                     //console.log(r.success);
    //                     if (r.success) {
    //                         $scope.loginError = false;
    //                         $mdDialog.cancel();
    //                         MainService.UserManager.loginCallback();
    //                     } else {
    //                         $scope.mode = 'password';
    //                         $scope.modeName = 'Password';
    //                         $scope.resetLoginInputValue();
    //                     }
    //                 }, function (reason) {
    //                     console.log('Failed: ' + reason);
    //                 }, function (update) {
    //                     console.log('Got notification: ' + update);
    //                 });
    //                 break;
    //             case 'password':
    //                 $scope.password = $scope.loginValue;
    //                 MainService.UserManager.login($scope.username, $scope.password).then(function (r) {
    //                     console.log(r);
    //                     if (r.success) {
    //                         $scope.loginError = false;
    //                         $mdDialog.cancel();

    //                         // check permission on login callback

    //                         MainService.UserManager.loginCallback();
    //                     } else {
    //                         $scope.mode = 'password';
    //                         $scope.resetLoginInputValue();
    //                         $scope.loginError = true;
    //                     }
    //                 }, function (reason) {
    //                     console.log('Failed: ' + reason);
    //                 }, function (update) {
    //                     console.log('Got notification: ' + update);
    //                 });
    //                 break;
    //         }
    //     }
    // }

    // app.controller('DialogControllerLogin', DialogControllerLogin);

    // original writing style
    /*app.controller('DialogControllerLogin', function ($scope, $mdDialog, SocketFactory, MainService, uiLanguage, INFO) {
        // console.log("called once");
        console.log($scope);
        $scope.mode = 'username';
        $scope.modeName = 'Login';
        $scope.username = '';
        $scope.password = '';
        $scope.tableNum = '';
        $scope.keypad = keypad;
        $scope.ui = uiLanguage[INFO.lang];

        $scope.input = function (val) {
            angular.element(".login").addClass("md-input-focused");
            $scope.loginValue += '' + val;
        }

        $scope.reset = function (val) {
            angular.element(".login").addClass("md-input-focused");
            if ($scope.loginValue == '' && $scope.mode == 'username') {
                $mdDialog.cancel();
                // angular.element(".login").addClass("md-input-focused");
            } else if ($scope.loginValue == '' && $scope.mode == 'password') {
                $scope.modeName = 'Login';
                $scope.mode = 'username';
                $scope.loginValue = '';
            } else {
                $scope.loginValue = '';
            }
        }

        $scope.backspace = function () {
            // console.log('here!');
            // $scope.loginValue = "12347";
            $scope.loginValue = $scope.loginValue.substring(0, $scope.loginValue.length - 1);
        };

        $scope.next = function (val) {
            //console.log('********');
            //console.log($scope.tableNum);
            //console.log($scope.mode);
            //console.log($scope.loginValue);
            switch ($scope.mode) {
                case 'username':
                    $scope.username = $scope.loginValue;
                    MainService.UserManager.login($scope.username, '').then(function (r) {
                        //console.log(r);
                        //console.log(r.success);
                        if (r.success) {
                            $scope.loginError = false;
                            $mdDialog.cancel();
                            MainService.UserManager.loginCallback();
                        } else {
                            $scope.mode = 'password';
                            $scope.modeName = 'Password';
                            $scope.resetLoginInputValue();
                        }
                    }, function (reason) {
                        console.log('Failed: ' + reason);
                    }, function (update) {
                        console.log('Got notification: ' + update);
                    });
                    break;
                case 'password':
                    $scope.password = $scope.loginValue;
                    MainService.UserManager.login($scope.username, $scope.password).then(function (r) {
                        console.log(r);
                        if (r.success) {
                            $scope.loginError = false;
                            $mdDialog.cancel();

                            // check permission on login callback

                            MainService.UserManager.loginCallback();
                        } else {
                            $scope.mode = 'password';
                            $scope.resetLoginInputValue();
                            $scope.loginError = true;
                        }
                    }, function (reason) {
                        console.log('Failed: ' + reason);
                    }, function (update) {
                        console.log('Got notification: ' + update);
                    });
                    break;
            }
        }
    });*/

    app.controller('DialogControllerVoidItemApproval', function ($scope, $mdDialog, SocketFactory, MainService, uiLanguage, INFO, $timeout) {
        // console.log("called once");
        $scope.mode = 'username';
        $scope.modeName = 'Login';
        $scope.username = '';
        $scope.password = '';
        $scope.tableNum = '';
        $scope.keypad = keypad;
        $scope.ui = uiLanguage[INFO.lang];
        $scope.MainService = MainService;
        $scope.input = function (val) {
            angular.element(".login").addClass("md-input-focused");
            $scope.loginValue += '' + val;
        }

        $scope.approve = function () {
            //MainService.approve(arguments);
        }

        $scope.reset = function (val) {
            angular.element(".login").addClass("md-input-focused");
            if ($scope.loginValue == '' && $scope.mode == 'username') {
                $mdDialog.cancel();
                // angular.element(".login").addClass("md-input-focused");
            } else if ($scope.loginValue == '' && $scope.mode == 'password') {
                $scope.modeName = 'Login';
                $scope.mode = 'username';
                $scope.loginValue = '';
            } else {
                $scope.loginValue = '';
            }
        }

        $scope.backspace = function () {
            $scope.loginValue = $scope.loginValue.substring(0, $scope.loginValue.length - 1);
        };

        $scope.approveVoidItem = function (r) {
            $scope.loginError = false;
            if (r.staffDetails[MainService.schema.permission['voidItem']]) {
                for (var i = 0; i < MainService.pendingVoidItem.length; i++) {
                    //MainService.Cart.addItem(MainService.pendingVoidItem[i]);
                    var oItem = {};
                    angular.copy(MainService.pendingVoidItem[i], oItem);
                    var rItem = MainService.Cart.addItem(MainService.pendingVoidItem[i]);
                    rItem.qty = oItem.qty;
                    rItem.voidIndex = oItem.voidIndex;
                    rItem.voidRemark = oItem.voidRemark;
                    rItem.voidApproveStaff = r.staffDetails.username;
                }
                MainService.Cart.resetEditItem();
                MainService.modeItem = MainService.schema.modeItem.normal;
                MainService.modeMultiSelectItem = false;
                $timeout(function () {
                    MainService.scrollToBottom();
                }, 10);
                $mdDialog.cancel();
            } else {
                MainService.showAlert('no_permission')
            }
        }

        $scope.next = function (val) {
            switch ($scope.mode) {
                case 'username':
                    $scope.username = $scope.loginValue;
                    MainService.UserManager.requestApproval($scope.username, '').then(function (r) {
                        if (r.success) {
                            $scope.approveVoidItem(r);
                        } else {
                            $scope.mode = 'password';
                            $scope.modeName = 'Password';
                            $scope.resetLoginInputValue();
                        }
                    }, function (reason) {
                        console.log('Failed: ' + reason);
                    }, function (update) {
                        console.log('Got notification: ' + update);
                    });
                    break;
                case 'password':
                    $scope.password = $scope.loginValue;
                    console.log(1);
                    MainService.UserManager.requestApproval($scope.username, $scope.password).then(function (r) {
                        console.log(r);
                        if (r.success) {
                            $scope.approveVoidItem(r);
                        } else {
                            $scope.mode = 'password';
                            $scope.resetLoginInputValue();
                            $scope.loginError = true;
                        }
                    }, function (reason) {
                        console.log('Failed: ' + reason);
                    }, function (update) {
                        console.log('Got notification: ' + update);
                    });
                    break;
            }
        }
    });

    // 有generic企圖，未整好
    app.controller('DialogControllerGenericApproval', function ($scope, $mdDialog, SocketFactory, MainService, uiLanguage, INFO, $timeout) {
        // console.log("called once");
        $scope.mode = 'username';
        $scope.modeName = 'Login';
        $scope.username = '';
        $scope.password = '';
        $scope.tableNum = '';
        $scope.keypad = keypad;
        $scope.ui = uiLanguage[INFO.lang];
        $scope.MainService = MainService;
        $scope.input = function (val) {
            angular.element(".login").addClass("md-input-focused");
            $scope.loginValue += '' + val;
        }

        $scope.reset = function (val) {
            angular.element(".login").addClass("md-input-focused");
            if ($scope.loginValue == '' && $scope.mode == 'username') {
                $mdDialog.cancel();
                // angular.element(".login").addClass("md-input-focused");
            } else if ($scope.loginValue == '' && $scope.mode == 'password') {
                $scope.modeName = 'Login';
                $scope.mode = 'username';
                $scope.loginValue = '';
            } else {
                $scope.loginValue = '';
            }
        }

        $scope.backspace = function () {
            $scope.loginValue = $scope.loginValue.substring(0, $scope.loginValue.length - 1);
        };

        $scope.approveVoidItem = function (r) {
            $scope.loginError = false;
            if (r.staffDetails[MainService.schema.permission['voidItem']]) {
                for (var i = 0; i < MainService.pendingVoidItem.length; i++) {
                    //MainService.Cart.addItem(MainService.pendingVoidItem[i]);
                    var oItem = {};
                    angular.copy(MainService.pendingVoidItem[i], oItem);
                    var rItem = MainService.Cart.addItem(MainService.pendingVoidItem[i]);
                    rItem.qty = oItem.qty;
                    rItem.voidIndex = oItem.voidIndex;
                    rItem.voidRemark = oItem.voidRemark;
                    rItem.voidApproveStaff = r.staffDetails.username;
                }
                MainService.Cart.resetEditItem();
                MainService.modeItem = MainService.schema.modeItem.normal;
                MainService.modeMultiSelectItem = false;
                $timeout(function () {
                    MainService.scrollToBottom();
                }, 10);
                $mdDialog.cancel();
            } else {
                MainService.showAlert('no_permission')
            }
        }

        $scope.next = function (val) {
            switch ($scope.mode) {
                case 'username':
                    $scope.username = $scope.loginValue;
                    MainService.UserManager.requestApproval($scope.username, '').then(function (r) {
                        if (r.success) {
                            $scope.approveVoidItem(r);
                        } else {
                            $scope.mode = 'password';
                            $scope.modeName = 'Password';
                            $scope.resetLoginInputValue();
                        }
                    }, function (reason) {
                        console.log('Failed: ' + reason);
                    }, function (update) {
                        console.log('Got notification: ' + update);
                    });
                    break;
                case 'password':
                    $scope.password = $scope.loginValue;
                    console.log(1);
                    MainService.UserManager.requestApproval($scope.username, $scope.password).then(function (r) {
                        console.log(r);
                        if (r.success) {
                            $scope.approveVoidItem(r);
                        } else {
                            $scope.mode = 'password';
                            $scope.resetLoginInputValue();
                            $scope.loginError = true;
                        }
                    }, function (reason) {
                        console.log('Failed: ' + reason);
                    }, function (update) {
                        console.log('Got notification: ' + update);
                    });
                    break;
            }
        }
    });

    // controllers for headCtrl->login
    function DialogControllerMember($scope, $mdDialog, SocketFactory, MainService, uiLanguage, INFO, keyMap) {
        // $scope.calcNumberGroup = [[1, 2, 3], [4, 5, 6], [7, 8, 9]];
        $scope.MainService = MainService;
        $scope.keypad = keypad;
        $scope.MemberNotFound = false;
        $scope.input = function (val) {
            $scope.loginValue += '' + val;
        }
        $scope.reset = function (val) {
            if ($scope.loginValue == '') {
                $mdDialog.cancel();
            } else {
                $scope.loginValue = '';
            }
        }
        $scope.cancel = function () {
            $mdDialog.cancel();
        }

        $scope.next = function (val) {
            console.log($scope.loginValue);
            console.log("next");
            //console.log(angular.element('order-ctrl').scope().cart.tableNo);
            //SocketFactory.emit('linkMemberToTable', { membercode: $scope.loginValue, tableno: MainService.Cart.tableNo }, function (r) {
            SocketFactory.emit('searchMember', { membercode: $scope.loginValue }, function (r) {
                console.log(r);
                if (r) {
                    $scope.Member = $.extend(true, r, MainService.schema.member);
                    $scope.MemberNotFound = false;
                } else {
                    $scope.MemberNotFound = true;
                }
                //if (r.success) {

                //}
            });
        }

        $scope.backspace = function () {
            // console.log('here!');
            // $scope.loginValue = "12347";
            $scope.loginValue = $scope.loginValue.substring(0, $scope.loginValue.length - 1);
        };

        $scope.ui = uiLanguage[INFO.lang];
    };

    app.controller('messageBoxController', function ($scope, $mdDialog, uiLanguage, INFO) {
        $scope.alert = '';

        $scope.showAlert = function (title, content) {
            // Appending dialog to document.body to cover sidenav in docs app
            // Modal dialogs should fully cover application
            // to prevent interaction outside of dialog

            var title = title || 'Please provide title',
            content = content || 'Please provide content';

            $mdDialog.show(
              $mdDialog.alert()
                .parent(angular.element(document.body))
                .title(title)
                .content(content)
                .ok(uiLanguage[INFO.lang]["affirmative"])
            );
        };
    });

    app.controller('kitchenController', function ($scope, $element, uiLanguage, INFO, $http, SocketFactory, MainService) {
        console.log('kitchenController');
        $scope.ui = uiLanguage[INFO.lang];
        $scope.lang = INFO.lang;

        //console.debug('init');
        MainService.modeMultiSelectItem = true;
        // dynamic?
        var ctrl = this;
        // var option = [];
        // var option = [
        //     { name: 'message_fire' },
        //     { name: 'message_wait_for_call' },
        //     { name: 'message_serve_now' },
        //     { name: 'message_take_away_with_box' },
        //     { name: 'message_client_recall' },
        //     { name: 'message_missed_order' },
        //     { name: 'message_for_cashier' },
        //     { name: 'message_exempt' },
        //     { name: 'message_no_salad_dressing' },
        //     { name: 'message_first_priority' },
        //     { name: 'message_compensate' },
        //     { name: 'message_seat_a' },
        //     { name: 'message_seat_b' }
        //     //,{name: 'sales_report'}
        // ];

        // this.option = option;
        // console.log('this');
        // console.log(this);

        // can be related to MainService, centralize all page option later for user customization
        $scope.optionPerPage = 23;
        $scope.currentPage = 1;
        $scope.noOfOptionPage = Math.ceil(MainService.food.kitchenmsg.length / $scope.optionPerPage);
        $scope.reminderPage = MainService.getPageNumber($scope.optionPerPage - MainService.food.kitchenmsg.length % $scope.optionPerPage);

        // case
        // page 1: per page - length % per page = no of dummy to repeat, eg 12 % 13, remain 12, 13 - 12 = 1 dummy
        // page 1: length % per page = 0, no of dummy to repeat = 0, eg 13 % per page = 0, remain 13 - 0 = 13 (all are not dummy), per page must > length to make this true
        // page 2/page n(last page) same as page 1

        // console.log($scope.noOfOptionPage);
        // console.log($scope.reminderPage);
        // console.log($scope.optionPerPage);
        // console.log(ctrl.option.length % $scope.optionPerPage);

        $scope.cancel = function () {
            MainService.Cart.cancelOrder();
            MainService.modeMultiSelectItem = false;
        }

        $scope.submit = function () {
            MainService.submitCart();

            // $scope.$emit('saveOrder', []);
        }

        $scope.selectAll = function () {
            // console.log('selectall?');
            MainService.schema.baseItem.selectAllItem();
        }

        $scope.toggleAll = function () {
            // console.log('selectall?');
            $scope.selectAll();
            //MainService.schema.baseItem.toggleAllItem();
        }

        // event
        // $element.find('.kitchen-message').on('click', '.btn', function(e){
        //     console.log('click me');
        //     // angular.element(this).parents('.row').find('.active').removeClass("active");
        //     // angular.element(this).addClass("active");
        //     if( angular.element(this).hasClass("active") )
        //         angular.element(this).removeClass("active")
        //     else
        //         angular.element(this).addClass("active")
        // });
    });

    app.controller('compositeItemController', function ($scope, $element, uiLanguage, INFO, $http, SocketFactory, MainService, $rootScope) {
        $scope.ui = uiLanguage[INFO.lang];
        $scope.lang = INFO.lang;

        var ctrl = this;

        $scope.optionPerPage = 23;
        $scope.currentPage = 1;
        $scope.noOfOptionPage = Math.ceil(MainService.combo.selectMode.itemList.length / $scope.optionPerPage);
        $scope.reminderPage = MainService.getPageNumber($scope.optionPerPage - MainService.combo.selectMode.itemList.length % $scope.optionPerPage);

        $rootScope.$on('resetCompositeItem', function () {
            $scope.currentPage = 1;
            $scope.noOfOptionPage = Math.ceil(MainService.combo.selectMode.itemList.length / $scope.optionPerPage);
            $scope.reminderPage = MainService.getPageNumber($scope.optionPerPage - MainService.combo.selectMode.itemList.length % $scope.optionPerPage);
        });

        $scope.cancel = function () {
            MainService.switchMode2({ modeItem: 'normal' });
        }

        $scope.changeItem = function (obj, stock) {
            if (stock != undefined && stock != "") {
                if (stock <= 0) return;
            }
            var idx = -1;
            MainService.Cart.cartList.forEach(function (cartItem, _idx) {
                if (cartItem.isEdit) { idx = _idx; return false; }
            })

            if (idx == -1) return;
            var newItem = $.extend(true, {}, obj, MainService.schema.baseCartItem, { "voidIndex": obj.voidIndex });
            newItem.init();
            newItem.itemKey = MainService.Cart.cartList[idx].itemKey;
            newItem.comboItemKey = MainService.Cart.cartList[idx].comboItemKey;
            newItem.qty = MainService.Cart.cartList[idx].qty;
            newItem.itemUnitPrice = newItem.unitprice;
            newItem.unitprice = 0;
            newItem.kitchenMsg = MainService.Cart.cartList[idx].kitchenMsg;
            newItem.modifier = MainService.Cart.cartList[idx].modifier;
            MainService.Cart.cartList[idx] = newItem;
            var key = MainService.combo.compositeOptionGroupKey[MainService.combo.selectMode.optionGroupIndex];
            var optionGroup = MainService.combo.compositeOptionGroup[MainService.combo.selectMode.optionGroupIndex];
            var itemIdx = MainService.combo.selectMode.itemIndex;
            MainService.combo.compositeItemList[MainService.combo.selectMode.optionGroupIndex].items[itemIdx] = MainService.Cart.cartList[idx];
            optionGroup.items[key.optionItemIndex[itemIdx]].qty -= newItem.qty;
            delete optionGroup.items[key.optionItemIndex[itemIdx]].composite;

            var newOptionIdx = -1;
            optionGroup.items.forEach(function (option, optionIdx) {
                if (option.plu == newItem.itemId) {
                    newOptionIdx = optionIdx;
                    option.qty += newItem.qty;
                    option.composite = true;
                    return false
                }
            })
            if (newOptionIdx == -1) return;
            key.optionItemIndex[itemIdx] = newOptionIdx;
            MainService.combo.mapOption();
            MainService.switchMode2({ modeItem: 'normal' });


        }

        $scope.delete = function ($event) {
            var idx = -1;
            MainService.Cart.cartList.forEach(function (cartItem, _idx) {
                if (cartItem.isEdit) { idx = _idx; return false; }
            })
            MainService.Cart.cartList[idx].deleteItem($event, idx);
            MainService.switchMode2({ modeItem: 'normal' });
        }
    });

    app.controller("keypadControllerOpenItem", function ($scope, $mdDialog, $element, $attrs, $timeout, MainService, $rootScope) {
        $scope.keypad = keypad;
        console.log('keypad', $scope.keypad);
        $scope.keypadInfo = {
            value: '0.0',
            name: ''
        };
        // $scope.ui = uiLanguage[INFO.lang];
        $scope.title = $attrs.title ? $attrs.title : 'Keypad';
        $scope.input = function (val) {
            var maxlength = $attrs.maxlength ? $attrs.maxlength : 3;
            if ($scope.keypadInfo.value.length < maxlength)
                $scope.keypadInfo.value += '' + val;
        };

        $scope.currentFieldName = "name";
        $scope.focusField = function (fieldName) {
            this.currentFieldName = fieldName;
            return true;
        }

        // for non-numerical character
        $scope.inputFree = function (val) {
            console.log($scope.keypadInfo);
            // simpler:
            // number type field
            if (this.currentFieldName === 'value') {
                switch (val) {
                    case '.':
                        console.log('. ar');
                        this.appendNumFree(val);
                        break;
                    case 'tab':
                    case 'space':
                        break;
                    default:
                        /*$scope.keypadInfo[this.currentFieldName] = parseFloat( String( $scope.keypadInfo[this.currentFieldName] ) + val );
                        break;*/
                };
            } else {
                // text field (exclude date, time and so on... bare text only)
                switch (val) {
                    case 'space':
                        val = " ";
                        break;
                    case 'tab':
                        val = '';
                        for (var i = 0; i < 3; i++) {
                            val += " ";
                        }
                        break;
                    default:
                        break;
                };

                if (this.currentFieldName == 'batchNumber' || this.currentFieldName == 'traceNumber')
                    $scope.paymentKeypadObj[this.currentFieldName] += '' + val;
                else
                    $scope.keypadInfo[this.currentFieldName] += '' + val;
            }

            // switch( this.currentFieldName ) {
            //     case "value":
            //         $scope.keypadInfo.value += '' + val;
            //         break;
            //     default:
            //     case "name":
            //         $scope.keypadInfo.name += '' + val;
            //         break;
            // }
        };

        // for numerical character, support append, decimal points
        $scope.appendNumFree = function (amount) {

            console.log(amount);
            // do nothing if floating point is already input
            var inputValue = $scope.keypadInfo[this.currentFieldName];
            if (amount === '.' && $scope.keypadInfo[this.currentFieldName][inputValue.length - 1] === '.') {
                console.log('0');
                return;
            }

            // if floating point is activated, not allow to add one character
            // this.inputPrice += '';
            $scope.keypadInfo[this.currentFieldName] = String(inputValue); // type aware method and meaning is obvious without comment
            if (inputValue.indexOf('.') === -1 && amount === '.') {
                console.log('1');
                this.floatingInput = true;
                // not necessary because " ", it will make it .# instead of 0.# when it is 0
                // if (this.inputPrice.trim() == "0") { // input 7, result: .7 instead of 0.7
                //     console.log('1-1');
                //     console.log(this.inputPrice.trim());
                //     this.inputPrice = amount;
                // }else{
                //     console.log('1-2');
                //     this.inputPrice = this.inputPrice.concat(amount);
                // }
                $scope.keypadInfo[this.currentFieldName] = inputValue.trim().concat(amount);
                return;
            }

            if (inputValue.indexOf('.') != -1 && amount === '.') {
                console.log('2');
                return
            }

            // console.log(this.inputPrice.indexOf('.'));
            // console.log( !isNaN(parseInt(this.inputPrice[this.inputPrice.length - 1])));
            if (inputValue.indexOf('.') != -1 && !isNaN(parseInt($scope.keypadInfo[this.currentFieldName][inputValue.length - 1]))) {
                console.log("3");
                return;
            }
            // console.log(typeof this.inputPrice);
            // console.log(typeof this.inputPrice.trim());
            // console.log('when is this.inputPrice.trim() == 0 ' + this.inputPrice.trim());

            if (inputValue.trim() == "0") { // 排除預設等值有space的障礙
                console.log("4");
                $scope.keypadInfo[this.currentFieldName] = amount;
            } else {
                console.log("5");
                // append 0 if prev is "."
                if (inputValue[0] === '.') {
                    console.log("0" + inputValue);
                    $scope.keypadInfo[this.currentFieldName] = "0" + inputValue.concat(amount);
                } else {
                    $scope.keypadInfo[this.currentFieldName] = inputValue.concat(amount);
                }

            }
            // default
        }

        $scope.backspaceFree = function () {
            console.log($scope.keypadInfo);
            // convert it to string or it will get error when it is 0 and use backspace

            if (this.currentFieldName === 'value') {
                var inputValue = String($scope.keypadInfo[this.currentFieldName]);
                /*var tmpStr = String( $scope.keypadInfo[this.currentFieldName] );
                if( tmpStr.length > 1) {
                    $scope.keypadInfo[this.currentFieldName] = parseFloat( tmpStr.substring(0, tmpStr.length - 1) );
                }

                if( tmpStr.length == 1) {
                    $scope.keypadInfo[this.currentFieldName] = 0;
                }*/

                if (inputValue.length === 0)
                    return;

                console.log(inputValue);
                if (inputValue[inputValue.length - 1] === '.') {
                    this.floatingInput = false;
                }

                $scope.keypadInfo[this.currentFieldName] = inputValue.substring(0, inputValue.length - 1);

                if (isNaN(inputValue) || inputValue === '') {
                    console.log('nan');
                    $scope.keypadInfo[this.currentFieldName] = '';
                }
            } else {
                $scope.keypadInfo[this.currentFieldName] = $scope.keypadInfo[this.currentFieldName].substring(0, $scope.keypadInfo[this.currentFieldName].length - 1);
            }
        }

        $scope.backspace = function () {
            $scope.keypadInfo.value = $scope.keypadInfo.value.substring(0, $scope.keypadInfo.value.length - 1);
        };

        $scope.reset = function (val) {
            console.log($scope.keypadInfo.lastValue);
            console.log($scope.keypadInfo.lastName);
            if ($scope.keypadInfo.name == '') {
                $scope.keypadInfo.name = $scope.keypadInfo.lastName;
                $mdDialog.cancel();
            } else {
                $scope.keypadInfo.name = '';
            }

            if ($scope.keypadInfo.value == '') {
                $scope.keypadInfo.value = $scope.keypadInfo.lastValue;
                $mdDialog.cancel();
            } else {
                $scope.keypadInfo.value = '';
            }
        };

        $scope.cancel = function () {
            // var focusedElement = document.activeElement;
            // console.log(focusedElement);
            // console.log($scope.lastActiveField);
            console.log('cancel');
            console.log($scope.lastActiveField == "item-name" && $scope.keypadInfo.name != "");
            console.log($scope.lastActiveField == "item-price" && $scope.keypadInfo.value != "");
            console.log($scope.lastActiveField);
            console.log($scope.keypadInfo.value);
            if ($scope.lastActiveField == "item-name" && $scope.keypadInfo.name != "") {
                $scope.keypadInfo.name = "";
                return;
            }

            if ($scope.lastActiveField == "item-price" && $scope.keypadInfo.value != "") {
                $scope.keypadInfo.value = "";
                return;
            }

            $mdDialog.cancel();
        }

        $scope.keypressed = function (event) {
            console.log(event);
        };

        $scope.next = function (event) {
            $mdDialog.hide();
            //MainService.Cart.noOfPeople = $scope.keypadInfo.value;
            //alert($scope.keypadInfo.value);
            if (MainService.modeCheckOrder('foodControl')) {
                // food control operation
                MainService.stock.updateStockItem($scope.keypadInfo.value, $scope.currentItem);
            } else {
                angular.element($element).blur();
                // alert($scope.keypadInfo.value);
            }
        };

        $scope.checkDecimal = function (amount) {
            if (amount[amount.length - 1] === '.' || amount[0] === '.')
                return false; // input is incorrect
            else
                return true;
        }

        $scope.nextFree = function (event) {
            if ($scope.paymentKeypadObj.isUse) {
                console.log('paymentKeypad is using');
                if (!$scope.paymentKeypadObj.batchNumber || !$scope.paymentKeypadObj.traceNumber) return;
                var passData = { batchNumber: $scope.paymentKeypadObj.batchNumber, traceNumber: $scope.paymentKeypadObj.traceNumber, amount: $scope.paymentKeypadObj.tender.amount };
                MainService.getOtherWriteOrderServiceFullDetail().paymentKeypadSubmit(passData);
                $mdDialog.hide();
                return;
            }

            if (!this.checkDecimal($scope.keypadInfo.value) || $scope.keypadInfo.value === '') {
                return; // input is not completed
            }

            $mdDialog.hide();
            //MainService.Cart.noOfPeople = $scope.keypadInfo.value;
            //MainService.stock.updateStock($scope.keypadInfo.value);
            angular.element($element).blur();
            //alert($scope.keypadInfo.value);
            //alert($scope.keypadInfo.name);
            console.log($scope.currentItem);
            //$scope.currentItem.name = $scope.keypadInfo.name;

            var customItem = {};
            $.extend(true, customItem, $scope.currentItem);
            customItem.desc1 = $scope.keypadInfo.name;
            customItem.name['001'] = $scope.keypadInfo.name;
            customItem.customName = $scope.keypadInfo.name;
            customItem.unitprice = parseFloat($scope.keypadInfo.value) * 100;
            console.log(customItem)
            MainService.Cart.addItem(customItem);
        };

        $scope.closeDialog = function ($mdDialog) {
            $scope.keypadInfo.value = $scope.keypadInfo.lastValue;
            $scope.keypadInfo.name = $scope.keypadInfo.lastName;
            $scope.paymentKeypadObj.isUse = false;
            $mdDialog.cancel();
        };

        $scope.callKeypad = function (event, dialogCtrl, item) {
            console.log('call callKeypad right keypadControllerFoodControl');
            // console.log($scope.keypadInfo.value);
            // console.log(angular.element('md-dialog[aria-label="keypad"]').scope());
            $scope.keypadInfo.lastValue = $scope.keypadInfo.value;
            $scope.keypadInfo.lastName = $scope.keypadInfo.name;
            $scope.keypadInfo.value = ''; // demo mode
            $scope.currentItem = item;
            if (MainService.modeOrder == MainService.schema.modeOrder.normal) {
                if (item.stock == -1) {
                    MainService.stock.updateStock(-2);
                } else {
                    if (angular.element('md-dialog[aria-label="keypad"]').scope() == undefined) {
                        $mdDialog.show({
                            controller: dialogCtrl,
                            // templateUrl: 'tmpl/keypad.html',
                            templateUrl: 'tmpl/keypad.html',
                            targetEvent: event,
                            focusOnOpen: true,
                            bindToController: true,
                            scope: $scope,
                            preserveScope: true,

                            // for generic purpose, each instance only affect local property keypad value
                            locals: {
                                keypadInfo: $scope.keypadInfo,
                            }
                        })
                    } else {
                        $mdDialog.hide();
                    }

                }
            }
        };

        $scope.currentItem = {};

        /**********************************/
        /** for manually payment **/
        $scope.paymentKeypadObj = {
            batchNumber: "",
            traceNumber: "",
            tender: {},
            isUse: false,
            inputValue: function (value) {
                $scope.paymentKeypadObj[$scope.currentFieldName] += '' + value;
            },
            backspace: function () {
                $scope.paymentKeypadObj[$scope.currentFieldName] = $scope.paymentKeypadObj[$scope.currentFieldName].substring(0, $scope.paymentKeypadObj[$scope.currentFieldName].length - 1);
            },
            reset: function () {
                this.batchNumber = "";
                this.traceNumber = "";
            }

        };

        $scope.initPaymentKeypad = function () {
            if (!$scope.openKeypadByPayment) {
                console.debug('$scope.openKeypadByPayment', !$scope.openKeypadByPayment);
                $scope.openKeypadByPayment = $rootScope.$on('openKeypadByPayment', function (event, obj) {
                    console.log('in keypad', obj);
                    $scope.paymentKeypadObj.batchNumber = "";
                    $scope.paymentKeypadObj.traceNumber = "";
                    $scope.currentFieldName = "batchNumber";
                    $scope.paymentKeypadObj.isUse = true;
                    $scope.paymentKeypadObj.tender = angular.copy(obj);
                    $scope.callKeypadFree(null, $scope.dialogTest1, null);
                });
            }



        }

        /**********************************/

        $scope.callKeypadFree = function (event, dialogCtrl, item) {
            console.log('callKeypadFree / keypadControllerOpenItem');

            /** for manually payment **/
            if (!item) {
                $scope.cardList = MainService.tender.filter(function (t) {
                    return t.tenderType == 'card';
                });
                $mdDialog.show({
                    controller: function () {
                        $scope.close = function () {
                            $scope.paymentKeypadObj.isUse = false;
                            $mdDialog.cancel();
                        }
                    },
                    // templateUrl: 'tmpl/keypad.html',
                    templateUrl: 'tmpl/keypad-payment-free.html',
                    targetEvent: event,
                    focusOnOpen: true,
                    bindToController: true,
                    scope: $scope,
                    preserveScope: true,
                    onComplete: function () {
                        console.log('onComplete');
                        // determine which is last active field for cancel button to discriminate
                        /*$scope.lastActiveField = angular.element('#free-item-keypad input[name="item-name"]').attr('name');
                        angular.element('#free-item-keypad input').on('focus', function () {
                            // console.log(angular.element(this).attr('name'));
                            $scope.lastActiveField = angular.element(this).attr('name');
                        })*/
                    },

                    // for generic purpose, each instance only affect local property keypad value
                    locals: {
                        keypadInfo: $scope.paymentKeypadObj
                    }
                })

                return;
            }

            // usage cases
            // case 1: not food control, add item to order
            // case 2: in foodcontrol, popup the dialog and ask for control item with control number
            console.log(item.type);
            // console.log($element);
            if ($scope.modeCheckOrder('detailNormal') && MainService.modeOrder != MainService.schema.modeOrder.foodControl) {
                console.log('for food ordering');
                $scope.loadSubitem(item, dialogCtrl);
            } else {
                console.log('for food control');
                console.log(item);
                $scope.keypadInfo.lastValue = $scope.keypadInfo.value;
                $scope.keypadInfo.lastName = $scope.keypadInfo.name;
                $scope.keypadInfo.name = '';
                $scope.keypadInfo.value = '';
                $scope.currentItem = item;
                $scope.title = $scope.ui['food_control'] + ' / ' + item.name[INFO.lang];

                if (item.option != null && item.option.length > 0) {
                    // provide option for selection (either option or itself)
                    if (angular.element('md-dialog[aria-label="Food Control Option"]').scope() == undefined) {
                        console.log('food control option');
                        $mdDialog.show({
                            controller: dialogCtrl,
                            bindToController: true,
                            scope: $scope,
                            preserveScope: true,
                            controller: function ($scope) {
                                $scope.closeDialog = function () {
                                    $mdDialog.hide();
                                }
                            },
                            templateUrl: 'tmpl/foodControlOption.html'
                        })
                    } else {
                        $mdDialog.hide();
                    }
                } else {
                    // direct control current item
                    $scope.foodControlDirect(dialogCtrl);
                }
            }
        };

        $scope.loadSubitem = function (item, dialogCtrl) {
            if (item.type === "o") {
                console.log('call callKeypad right keypadControllerFoodControl');
                console.log(item);
                // console.log(angular.element('md-dialog[aria-label="keypad"]').scope());
                $scope.keypadInfo.lastValue = $scope.keypadInfo.value;
                $scope.keypadInfo.lastName = $scope.keypadInfo.name;
                //$scope.keypadInfo.name = item.name1;
                $scope.keypadInfo.name = '';
                $scope.keypadInfo.value = '';
                $scope.currentItem = item;
                if (item.stock == -1) {
                    console.log('-----------------------------1');
                    MainService.stock.updateStock(-2);
                } else {
                    console.log('-----------------------------2');
                    if (angular.element('md-dialog[aria-label="keypad"]').scope() == undefined) {
                        $mdDialog.show({
                            controller: dialogCtrl,
                            // templateUrl: 'tmpl/keypad.html',
                            templateUrl: 'tmpl/keypad-free.html',
                            targetEvent: event,
                            focusOnOpen: true,
                            bindToController: true,
                            scope: $scope,
                            preserveScope: true,
                            onComplete: function () {
                                // determine which is last active field for cancel button to discriminate
                                $scope.lastActiveField = angular.element('#free-item-keypad input[name="item-name"]').attr('name');
                                angular.element('#free-item-keypad input').on('focus', function () {
                                    // console.log(angular.element(this).attr('name'));
                                    $scope.lastActiveField = angular.element(this).attr('name');
                                })
                            },

                            // for generic purpose, each instance only affect local property keypad value
                            locals: {
                                keypadInfo: $scope.keypadInfo
                            }
                        })
                    } else {
                        $mdDialog.hide();
                    }

                }
                // if (MainService.modeOrder == MainService.schema.modeOrder.normal) {
                // }
            } else {
                // console.log(item);
                MainService.Cart.addItem(item);
            }
        }

        $scope.foodControlDirect = function (dialogCtrl) {
            if (angular.element('md-dialog[aria-label="keypad"]').scope() == undefined) {
                $mdDialog.show({
                    controller: dialogCtrl,
                    // templateUrl: 'tmpl/keypad.html',
                    templateUrl: 'tmpl/keypad.html',
                    targetEvent: event,
                    focusOnOpen: true,
                    bindToController: true,
                    scope: $scope,
                    preserveScope: true,
                    onComplete: function () {
                        // determine which is last active field for cancel button to discriminate
                        $scope.lastActiveField = angular.element('#free-item-keypad input[name="item-name"]').attr('name');
                        angular.element('#free-item-keypad input').on('focus', function () {
                            // console.log(angular.element(this).attr('name'));
                            $scope.lastActiveField = angular.element(this).attr('name');
                        })
                    },

                    // for generic purpose, each instance only affect local property keypad value
                    locals: {
                        keypadInfo: $scope.keypadInfo
                    }
                })
            } else {
                $mdDialog.hide();
            }
        }

        $scope.control = function (option, dialogCtrl) {
            dialogCtrl = dialogCtrl ? dialogCtrl : angular.noop;
            switch (option) {
                default:
                case 'this':
                    $scope.foodControlDirect(dialogCtrl);
                    break;
                case 'child':
                    // load option
                    console.log('load subitem');
                    // $scope.loadSubitem( $scope.currentItem );
                    MainService.Cart.loadOption($scope.currentItem);
                    $mdDialog.hide();
                    break;
            }
        }

        $scope.dialogTest1 = function ($scope, $mdDialog, keypadInfo) {
            console.log('dialogTest1');
            // console.log($scope.title);
            // console.log($scope.keypad);
            // console.log($scope.keypadInfo.value);
            console.log(keypadInfo);
            $scope.keypadInfo = keypadInfo;
        };

        $scope.triggerKeypad = function (element) {
            $timeout(function () {
                angular.element(element).trigger("click");
            }, 1);
        };
    });

    app.controller("foodMenuController", function ($scope, $controller, $timeout, $rootScope) {

        $rootScope.$on('resetCategoryCurrentPage', function () {
            console.debug('resetCategoryCurrentPage')
            $timeout(function () {
                $scope.currentCatsPage = 1;
            }, 500);
        })


        // $controller('gridMenuOptionController', {$scope: $scope});

        // console.log($scope);
        // $scope.foodMenuCancel = function() {
        //     console.log('test');
        //     // alert('haha');
        // }

        // $scope.testtesttest = function(){}
    });

    app.controller("categoryItemController", function ($scope, $controller, $timeout, $rootScope, $interval) {
        $scope.currentPage = 1;
    });

    app.controller("userClockController", function ($scope, $controller, $mdDialog, $element, $attrs, $timeout, uiLanguage, INFO, MainService, SocketFactory) {
        $controller('keypadController', { $scope: $scope, $mdDialog: $mdDialog, $element: $element, $attrs: $attrs, $timeout: $timeout, MainService: MainService });

        // console.log($scope);
        $scope.title = uiLanguage[INFO.lang]['please_place_the_card'];
        $attrs.maxlength = 20;
        ctrl = this;

        var global = $scope;
        $scope.isTopBarCalled = false;
        $scope.takeAttendanceOnTopBar = function () {
            $mdDialog.show({
                controller: function ($scope, userConfig, uiLanguage, INFO) {
                    $scope.closeDialog = function () {
                        $mdDialog.hide();
                    }

                    $scope.checkIn = function ($event) {
                        console.log('test');
                        global.isTopBarCalled = true;
                        global.clock($event, 'in');
                    }

                    $scope.checkOut = function ($event) {
                        global.isTopBarCalled = true;
                        global.clock($event, 'out');
                    }


                },
                template:
                  '<md-dialog aria-label="List dialog">' +
                  '<div class="md-toolbar-tools" style="background-color:rgb(121,85,72); color:white;">   ' +
                  '<h2>' + uiLanguage[INFO.lang]["user_clock_management"] + '</h2>' +
                        '<span flex=""></span>  ' +
                           ' <button class="md-icon-button md-button md-default-theme" ng-click="closeDialog();" > ' +
                           '<i class="md-icon dp45 ng-scope">clear</i> ' +
                           '</button> ' +
                    '</div>' +
                  '  <md-dialog-content>' +
                  '<button class="md-raised md-button-text md-button" ng-click="checkIn($event);" style="background-color:#0D752D; color:white;" >' +
                   '<span class="ng-binding ng-scope"> ' + uiLanguage[INFO.lang]["clock_in"] + ' </span></button>' +
                   '<button class="md-raised md-button-text md-button" ng-click="checkOut($event);"style="background-color:#2B2B8C; color:white;" >' +
                   '<span class="ng-binding ng-scope"> ' + uiLanguage[INFO.lang]["clock_out"] + ' </span></button>' +
                  '<p></p>' +
                  '  </md-dialog-content>' +
                  '</md-dialog>'
            })
        }

        // console.log($scope.next);
        $scope.next = function () {
            // console.log('user name');
            // return user id with socket then going back to main screen
            console.log($scope.keypadInfo.value);

            $mdDialog.hide();

            if ($scope.isTopBarCalled) {
                $scope.isTopBarCalled = false;
            } else {
                MainService.switchMode2({ mode: 'main' }, false);
            }


            // console.log(ctrl.action);
            switch (ctrl.action) {
                case 'in':
                    console.log('clock in');
                    SocketFactory.emit('clockIn', { "username": $scope.keypadInfo.value }, function (data) {
                        console.log(data)
                        data = JSON.parse(data);
                        var _validUser = true;
                        try {
                            if (data.result === "ERROR") {
                                _validUser = false;
                            }
                        } catch (e) { }
                        if (_validUser) {

                            var t = new Date(data.time),
                                dateTime = t.getFullYear() + '-' + (t.getMonth() + 1) + '-' + t.getDate() + ' ' + t.getHours() + ':' + t.getMinutes();
                            MainService.showAlert(data.staffName + uiLanguage[INFO.lang]['txt_attendance_clock_in'] + dateTime);
                        } else {
                            MainService.showAlert(uiLanguage[INFO.lang]['msg_staff_not_found']);
                        }
                    })
                    break;
                case 'out':
                    console.log('clock out');
                    SocketFactory.emit('clockOut', { "username": $scope.keypadInfo.value }, function (data) {
                        console.log(data)
                        data = JSON.parse(data);
                        var _validUser = true;
                        try {
                            if (data.result === "ERROR") {
                                _validUser = false;
                            }
                        } catch (e) { }
                        if (_validUser) {

                            var t = new Date(data.time),
                                dateTime = t.getFullYear() + '-' + (t.getMonth() + 1) + '-' + t.getDate() + ' ' + t.getHours() + ':' + t.getMinutes();
                            MainService.showAlert(data.staffName + uiLanguage[INFO.lang]['txt_attendance_clock_out'] + dateTime);
                        } else {
                            MainService.showAlert(uiLanguage[INFO.lang]['msg_staff_not_found']);
                        }
                    })
                    // SocketFactory.emit('clockOut', {"username": $scope.keypadInfo.value }, function(data){ console.log(data)})
                    break;
                default:
                    break;
            }
        }
        $scope.clockDialog = function ($scope, $mdDialog, keypadInfo) {
            console.log('clockDialog');
            $scope.keypadInfo = keypadInfo;
        };

        $scope.clock = function (ev, action) {
            // pop up keypad
            ctrl.action = action;
            $scope.callKeypad(ev, $scope.clockDialog);
        }

        $scope.cancel = function () {
            MainService.switchMode2({ mode: 'main' }, false);
        }
    });


    app.controller("changeBillController", function ($scope, $controller, $mdDialog, $element, $attrs, $timeout, uiLanguage, INFO, MainService, SocketFactory, $rootScope) {
        MainService.ecrChangePaymentMode = 0;
        $scope.postData = {};
        $scope.postData.refNo = MainService.Cart.refNo;
        SocketFactory.emit("getAllPayment", $scope.postData, function (result) {
            $scope.currentPayment = result[0];
            $scope.voidPayment = result[1];
            $scope.ecrPayment = result[2];
            $scope.currentPayment.forEach(function (cp) {
                cp = angular.copy($scope.getTenderAndTraceNumber(cp));
            });

            $scope.voidPayment.forEach(function (vp) {
                vp = angular.copy($scope.getTenderAndTraceNumber(vp));
            });

            console.debug('currentPayment', $scope.currentPayment);
            console.debug('voidPayment', $scope.voidPayment);
        });

        $rootScope.$on('refreshPaymentList', function (event, result) {
            console.log(result);
            $scope.currentPayment = [];
            $scope.currentPayment = result;
            $scope.currentPayment.forEach(function (cp) {
                cp = angular.copy($scope.getTenderAndTraceNumber(cp));
            });
        })

        $scope.getTenderAndTraceNumber = function (obj) {
            var tender = [];
            var tender = MainService.tender.filter(function (t) {
                return t.code == obj.paymentType;
            });
            delete tender[0].$$hashKey;
            $.extend(true, obj, tender[0]);
            if (tender[0].tenderType == 'card') {
                if (obj.remark)
                    obj.traceNumber = obj.remark.split('##|##')[0];
            }
            return obj;
        }

        $scope.insertPayment = function () {
            if (typeof MainService.Cart.refNo != 'string') return;
            MainService.cloneAppliedPaymentMethod = [];
            angular.copy(MainService.appliedPaymentMethod, MainService.cloneAppliedPaymentMethod);
            MainService.ecrChangePaymentMode = 1;
        }

        $scope.deletePayment = function (seq, idx) {
            $scope.postData.isCard = false;
            var showAlert = true;
            function postSocket(postData) {
                console.debug(postData);
                SocketFactory.emit('deletePayment', postData, function (result) {
                    $mdDialog.cancel();
                    if (result == "failInCard" && showAlert) {
                        MainService.showAlert('失敗');
                        return;
                    }
                    console.debug(result);
                    MainService.deletePaymentMethod(idx);
                    $scope.currentPayment.splice(idx, 1);
                    $scope.voidPayment = [];
                    $scope.voidPayment = result[0];
                    $scope.voidPayment.forEach(function (vp) {
                        vp = angular.copy($scope.getTenderAndTraceNumber(vp));
                    });
                });
            }

            var confirm = $mdDialog.confirm()
                  .parent(angular.element(document.body))
                  .title(uiLanguage[INFO.lang]['alert'])
                  .content('決定要取消這項付款？')
                  .ok(uiLanguage[INFO.lang]['yes'])
                  .cancel(uiLanguage[INFO.lang]['cancel']);
            $mdDialog.show(confirm).then(function () {
                $scope.postData.seq = seq;
                $scope.postData.voidStaffId = MainService.UserManager.staffCode;
                console.debug($scope.postData);
                //console.log('test', $scope.currentPayment[idx]);
                if ($scope.currentPayment[idx].tenderType == 'card') {
                    $scope.postData.isCard = true;
                    $scope.postData.isUseECR = true;
                    $scope.postData.tender = angular.copy($scope.currentPayment[idx]);



                    var postData = angular.copy($scope.postData);

                    var content = "等待取消付款";
                    var title = uiLanguage[INFO.lang]['alert'];

                    var alert = $mdDialog.show({
                        template:
                          '<md-dialog aria-label="List dialog">' +
                          '  <md-dialog-content>' +
                          '<h2 class="md-title">' + title + '</h2>' +
                          '<p>' + content + '</p>' +
                          '<button class="md-primary md-button md-default-theme" style="float:right;font-size:18px;margin:0" ng-click="btnClick();">手動</button>' +
                          '  </md-dialog-content>' +
                          '</md-dialog>',
                        controller: function ($scope) {

                            console.debug('manually cancel', $scope.postData);

                            $scope.btnClick = function () {
                                //$mdDialog.cancel();
                                postData.isUseECR = false;
                                showAlert = false;
                                postSocket(postData);
                            }
                        }
                    });
                }

                postSocket($scope.postData);





            }, function () {
                console.log('cancel');
            });

        }

    });

    // app.controller("orderListLeftController", function ($scope, MainService, $element) {
    //     // console.log($scope);
    //     // console.log($element.find('.left-sidebar'));
    //     $element.find('#order-list-left').on('click', function(){
    //         MainService.leftClicked = true;
    //     });
    // });

    // app.controller("testController", function ($scope) {
    //     // console.log($scope);
    //     // $scope.testtesttest = function(){}
    // });

    app.controller("watchManagemenetController", function ($scope, $controller, $mdDialog, $element, $attrs, $timeout, uiLanguage, INFO, MainService, SocketFactory, $rootScope, $rootElement) {

        $scope.ui = uiLanguage[INFO.lang];
        $scope.keypad = keypad;
        $scope.keypadInfo = {
            value: ''
        };


        if (!$rootScope.watchManagement)
            $scope.title = $scope.ui["please_place_the_card"];
        else {
            $scope.title = $scope.ui["watch_management_place_watch"];
        }

        // $scope.$watch(function (scope) { return MainService.Cart.splitListLNoofpeople },
        //    function () {
        //        $scope.keypadInfo.value = MainService.Cart.splitListLNoofpeople;
        //    }
        //);

        $scope.input = function (val) {
            //var maxlength = $attrs.maxlength ? $attrs.maxlength : 3;
            //if ($scope.keypadInfo.value.length < maxlength)
            $scope.keypadInfo.value += '' + val;
        };

        $scope.backspace = function () {
            $scope.keypadInfo.value = $scope.keypadInfo.value.substring(0, $scope.keypadInfo.value.length - 1)
        };

        $scope.reset = function (val) {
            console.log($scope.keypadInfo.last);
            if ($scope.keypadInfo.value == '') {
                $scope.keypadInfo.value = $scope.keypadInfo.last;
                $mdDialog.cancel();
            } else {
                $scope.keypadInfo.value = '';
            }
        };

        $scope.keypressed = function (event) {
            console.log(event);
        };

        $scope.next = function (event) {

            if (!$rootScope.watchManagement) {
                SocketFactory.emit('checkUserExist', { "username": $scope.keypadInfo.value }, function (data) {
                    if (data[0].isExist) {
                        $rootScope.watchManagement = {};
                        $rootScope.watchManagement.userName = $scope.keypadInfo.value;
                        $scope.callKeypad(event);

                    } else {
                        MainService.showAlert(uiLanguage[INFO.lang]['msg_staff_not_found']);
                    }
                })
            } else {

                SocketFactory.emit('registerStaffWatch', { "username": $rootScope.watchManagement.userName, "watchTagId": $scope.keypadInfo.value }, function (data) {
                    delete $rootScope.watchManagement;
                    if (data == "OK") {
                        MainService.showAlert(uiLanguage[INFO.lang]["watch_management_success"]);
                    } else if (data.response == "fail1") {
                        MainService.showAlert(uiLanguage[INFO.lang]["watch_management_fail1"]);
                    }
                })

                console.debug('userName', $rootScope.watchManagement.userName);
                console.debug('watch tag:', $scope.keypadInfo.value);
            }



            $mdDialog.hide();
            //angular.element($element).blur();
        };

        $scope.close = function () {
            $scope.keypadInfo.value = $scope.keypadInfo.last;
            delete $rootScope.watchManagement;
            $mdDialog.hide();
        };

        $scope.callKeypad = function (event) {
            $scope.keypadInfo.last = $scope.keypadInfo.value;
            $scope.keypadInfo.value = '';

            $mdDialog.show({
                templateUrl: 'tmpl/watch-register-keypad.html',
                targetEvent: event,
                focusOnOpen: true,
                bindToController: true,
                preserveScope: true,
                locals: {
                    keypadInfo: $scope.keypadInfo,
                }
            })

            $rootElement.on('keyup', function (e) {
                // console.log("its's me");
                console.log($scope.keypadInfo);
                if (e.keyCode === 27) {
                    $timeout($mdDialog.hide);
                    $scope.keypadInfo.value = $scope.keypadInfo.last;
                }
            });
        };

    });


    app.controller("paymentPermissionController", function ($scope, $controller, $mdDialog, $element, $attrs, $timeout, uiLanguage, INFO, MainService, SocketFactory, $rootScope, $rootElement) {

        $scope.ui = uiLanguage[INFO.lang];
        $scope.keypad = keypad;
        $scope.keypadInfo = {
            value: ''
        };

        $scope.title = $scope.ui["please_place_the_card"];


        $scope.input = function (val) {
            $scope.keypadInfo.value += '' + val;
        };

        $scope.backspace = function () {
            $scope.keypadInfo.value = $scope.keypadInfo.value.substring(0, $scope.keypadInfo.value.length - 1)
        };

        $scope.reset = function (val) {
            console.log($scope.keypadInfo.last);
            if ($scope.keypadInfo.value == '') {
                $scope.keypadInfo.value = $scope.keypadInfo.last;
                $mdDialog.cancel();
            } else {
                $scope.keypadInfo.value = '';
            }
        };

        $scope.keypressed = function (event) {
            console.log(event);
        };

        $scope.next = function (event) {
            console.debug($scope.keypadInfo.value);

            SocketFactory.emit('checkPaymentPermission', { "userName": $scope.keypadInfo.value }, function (data) {
                $mdDialog.hide();
                if (data[0].count > 0) {
                    MainService.checkingPaymentPermission = true;
                    MainService.submitCart();

                } else {
                    MainService.showAlert('no_permission');
                }
            })

            //angular.element($element).blur();
        };

        $scope.close = function () {
            $scope.keypadInfo.value = $scope.keypadInfo.last;
            delete $rootScope.watchManagement;
            $mdDialog.hide();
        };

        $scope.callKeypad = function (event) {
            $scope.keypadInfo.last = $scope.keypadInfo.value;
            $scope.keypadInfo.value = '';

            $mdDialog.show({
                templateUrl: 'tmpl/paymentPermission.html',
                targetEvent: event,
                focusOnOpen: true,
                bindToController: true,
                preserveScope: true,
                locals: {
                    keypadInfo: $scope.keypadInfo,
                }
            })

            $rootElement.on('keyup', function (e) {
                // console.log("its's me");
                console.log($scope.keypadInfo);
                if (e.keyCode === 27) {
                    $timeout($mdDialog.hide);
                    $scope.keypadInfo.value = $scope.keypadInfo.last;
                }
            });
        };

    });

    app.controller("dineInFoodOtherFooterSummaryController", function ($scope, MainService) {

        $scope.checkingNoMultiPaymentChangeOrTips = function () {


            if (!MainService.appliedPaymentMethod) return;

            if (MainService.deposit.appliedDeposit.length != 0 && MainService.appliedPaymentMethod.length == 0) return false;

            if (!MainService.modeCheckBill('searchAmendBill')) return false

            if (MainService.multiPayment.isEnable()) {
                if (MainService.deposit.appliedDeposit.length == 0) {
                    if (MainService.appliedPaymentMethod.length == 1) return true;
                    else return false;
                } else {

                    if (MainService.deposit.appliedDeposit[0].refNo != null && MainService.appliedPaymentMethod.length == 1) return false;

                    if (MainService.appliedPaymentMethod.length >= 2) {
                        if (MainService.deposit.appliedDeposit[0].refNo != null && MainService.appliedPaymentMethod.length == 2) return true;
                        else return false
                    }

                    else return true;
                }

            } else {
                if (MainService.deposit.appliedDeposit.length == 0) {
                    if (MainService.appliedPaymentMethod.length == 1) return true;
                    else return false;
                } else {
                    if (MainService.appliedPaymentMethod.length <= 2) return true;
                    else return false;
                }
            }
        }

        $scope.checkingBtnDeleteOfTender = function () {

            //console.debug($scope);

            //if (currentTender.totalAmount != 0 && false) {
            //    currentTender.totalAmount += remainingPayAmount;
            //}
            //else {
            //    currentTender.totalAmount = max;
            if (!MainService.appliedPaymentMethod) return;
            if (!MainService.modeCheckBill('printAmendBill')) return false
            //}
            if (MainService.multiPayment.isEnable()) {
                if (MainService.deposit.appliedDeposit.length == 0) {
                    if (MainService.appliedPaymentMethod.length == 1) return true;
                    else return false

                }
                else {
                    if (MainService.deposit.appliedDeposit[0].refNo != null) {
                        if (MainService.appliedPaymentMethod.length > 2) return false;
                        else return true
                    }
                    else if (MainService.appliedPaymentMethod.length >= 2) return false;
                    else return true;
                }


            }
            else {
                return true;
            }


        }

        $scope.checkingMultiPaymentChangeOrTips = function () {

            if (!MainService.appliedPaymentMethod) return;
            if (!MainService.modeCheckBill('Bill')) return false;
            // convert it to string or it will get error when it is 0 and use backspace
            if (!$scope.checkingNoMultiPaymentChangeOrTips()) {
                if (MainService.deposit.appliedDeposit.length != 0 && MainService.deposit.appliedDeposit[0].refNo != null && MainService.appliedPaymentMethod.length == 1) {
                    return false;
                }

                else if (MainService.appliedPaymentMethod.length == 0) return false;
                else return true;
            } else {
                return false;
            }

        }

    })


})();
