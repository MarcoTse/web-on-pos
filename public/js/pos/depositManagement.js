﻿(function () {

    /*function clone(obj) {
        if (null == obj || "object" != typeof obj) return obj;
        var copy = obj.constructor();
        for (var attr in obj) {
            if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
        }
        return copy;
    }*/

    var app = angular.module("depositModule", ['ngMaterial', 'ui.bootstrap']);

    app.service("deposit", function (INFO, $injector) {
        //MainService = $injector.get('MainService');
        //var MainService = $injector.get('MainService');
        //console.log(MainService)
        function deposit() {
            if (angular.isUndefined(MainService)) MainService = $injector.get('MainService');
            this.depositNo = null;
            this.phone = null;
            this.name = null;
            this.remark = null;
            this.refNo = null;
            this.paymentMethod = null;
            this.paymentPrice = null;
            this.serveDate = new Date();
            this.lastUpdateById = null;
            this.lastUpdateDatetime = null;
            this.lastUpdateByName = null;
            this.createById = null;
            this.createDatetime = null;
            this.createByName = null;
            this.voidByRefNo = [];

            deposit.prototype.init = function () {
                var self = this;

                MainService.getTenderList().forEach(function (t) {
                    if (t.code == 'cash') {
                        self.paymentMethod = t;
                        return false;
                    }
                })

            }

            deposit.prototype.getPaymentName = function () {
                switch (INFO.lang) {
                    case "001":
                        return this.paymentMethod.name1;
                    case "003":
                        return this.paymentMethod.name3;
                }
            }

            deposit.prototype.setValue = function (d) {
                this.depositNo = d.depositNo;
                this.lastUpdateById = d.lastUpdateById;
                this.lastUpdateDatetime = new Date(d.lastUpdateDatetime);
                this.lastUpdateByName = d.lastUpdateByName;
                this.createById = d.createById;
                this.createDatetime = new Date(d.createDatetime);
                this.createByName = d.createByName;
                
                this.name = d.name;
                this.phone = d.phone;
                this.paymentPrice = d.price;
                this.refNo = d.refNo;
                this.remark = d.remark;
                
                //this.serveDate = moment(d.serveDate, 'YYYY-MM-DD').toDate();
                this.serveDate = new Date(d.serveDate);
                this.shopCode = d.shopCode;
                this.paymentMethod = MainService.getTenderList().filter(function (t) {
                    return t.code == d.tenderCode
                })[0];


                this.void = d.void;

                try {
                    this.voidByRefNo = JSON.parse(d.voidByRefNo);
                } catch (ex) { }

                if (this.voidByRefNo == null || typeof this.voidByRefNo != "object") this.voidByRefNo = [];


            }

            this.init();

        }
        return deposit;
    })

    app.service("depositManager", function (deposit) {

        return {
            appliedDeposit: [],
            deleteAppliedDeposit: function () {
                this.appliedDeposit.splice(0, 1);
            },
            calcPrice: function () {
                if (this.appliedDeposit.length != 0) {
                    if(this.appliedDeposit[0].refNo == null)
                        MainService.remainerAmount += this.appliedDeposit[0].paymentPrice
                }
            },
            depositModel: deposit
        }
    })

    app.directive('depositScene', function ($templateCache, $compile, $rootScope) {
        return {
            restrict: "E",
            templateUrl: 'tmpl/depositManagement.html',
            replace: true,
            link: function (scope, element) {
                setTimeout(function () {
                    Materialize.updateTextFields();
                }, 10)
            },
            controller: function ($scope, deposit, MainService, SocketFactory, $rootScope, userConfig) {

                MainService.inputPrice = 0;

                $scope.paymentPerPage = 10;
                $scope.currentPaymentPage = 1;
                $scope.noOfPaymentPage = Math.ceil(MainService.getTenderList().length / $scope.paymentPerPage);
                $scope.reminderPage = MainService.getPageNumber($scope.paymentPerPage - (MainService.getTenderList().length) % $scope.paymentPerPage);

                $scope.changeInputMode = function () {
                    MainService.setActive(angular.element('.btn-input-deposit'));
                    $scope.init();
                    MainService.switchMode2({ modeDeposit: 'input' }, false);
                }

                $scope.changeReadMode = function () {
                    MainService.setActive(angular.element('.btn-read-deposit'));
                    MainService.switchMode2({ modeDeposit: 'read' }, false);
                }

                $scope.changeSummaryMode = function () {
                    MainService.switchMode2({ modeDeposit: 'summary' }, false);
                }

                $scope.isInputMode = function () {
                    return MainService.modeDeposit == MainService.schema.modeDeposit.input;
                }

                $scope.isReadMode = function () {
                    return MainService.modeDeposit == MainService.schema.modeDeposit.read;
                }

                $scope.isSummaryMode = function () {
                    return MainService.modeDeposit == MainService.schema.modeDeposit.summary;
                }

                $scope.setPaymentMethod = function (tender) {
                    if (MainService.inputPrice == 0) {
                        $scope.deposit.paymentMethod = tender;
                    }
                }

                $scope.checkPhoneTypeInEvent = function ($event) {
                    var fnKeyCode = [8, 9, 46, 37, 39];
                    var numberKeyCode = [48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 96, 97, 98, 99, 100, 101, 102, 103, 104];

                    console.debug($event.keyCode);
                    if ($event.keyCode == 229) {
                        $event.preventDefault();
                        return false;
                    }
                    if (fnKeyCode.indexOf($event.keyCode) != -1) {

                    }
                    else if ($event.target.value.length >= 8) {
                        console.debug(64, '>=8');
                        $event.preventDefault();
                        return false;
                    }
                    else if (numberKeyCode.indexOf($event.keyCode) == -1) {
                        $event.preventDefault()
                        return false;
                    }
                }

                $scope.btnSubmitClick = function () {

                    if (!$scope.deposit.phone || $scope.deposit.phone.length != 8) return;

                    if (MainService.inputPrice == 0) {
                        MainService.showAlert('please_set_amount');
                        return;
                    }

                    if (!$scope.deposit.serveDate) {
                        MainService.showAlert('時間不正確!');
                        return;
                    }

                    $scope.deposit.lastUpdateBy = MainService.UserManager.staffDetails.staffId;
                    $scope.deposit.paymentPrice = MainService.inputPrice;

                    $scope.deposit.till = userConfig.page.till;
                    $scope.deposit.staffName = MainService.UserManager.staffName;
                    SocketFactory.emit('addDeposit', $scope.deposit, function (result) {
                        if (result.result == 'OK') {
                            $scope.deposit.depositNo = result.depositNo;
                            $scope.changeSummaryMode();
                        }
                    })

                }

                //MainService.switchMode2({ modeDeposit: 'input' }, false);

                $('.datepicker').pickadate({
                    formatSubmit: 'yyyy-mm-dd',
                    format: 'yyyy-mm-dd',
                    min: new Date(),
                })

                MainService.setActive(angular.element('.btn-input-deposit'));

                $scope.init = function () {
                    MainService.inputPrice = 0;
                    $scope.deposit = new deposit();
                }

                $scope.init();

                $rootScope.$on('changeToInputMode', function () {
                    $scope.changeInputMode();
                })

                $scope.mode = "management";
            }
        }
    })

    app.controller('depositModeController', function ($scope, MainService, $rootScope) {

        $scope.changeInputMode = function () {
            $rootScope.$emit('changeToInputMode');
        }

        $scope.changeReadMode = function () {
            $rootScope.$emit('depositGetInitData');
            MainService.switchMode2({ modeDeposit: 'read' }, false);
        }

        $scope.changeSummaryMode = function () {
            MainService.switchMode2({ modeDeposit: 'summary' }, false);
        }

    })

    app.controller('depositViewerController', function ($scope, MainService, SocketFactory, deposit, $rootScope, userConfig) {
    
        $scope.depositList = [];
        $scope.depositCollection = [];
        var args = {}

        if (MainService.mode == MainService.schema.mode.order) args.isUnused = true;

        $scope.init = function () {
            $scope.depositList = [];
            $scope.depositCollection = [];
            SocketFactory.emit('getLocalDeposit', args, function (result) {
                console.debug(result);
                result.forEach(function (d) {
                    var tmpl = new deposit();
                    tmpl.setValue(d);
                    $scope.depositList.push(tmpl);
                })

                angular.extend($scope.depositCollection, $scope.depositList);
                console.debug($scope.depositList);
            })
        }

        $rootScope.$on('depositGetInitData', function () {
            $scope.init();
        })

        $scope.init();
        console.debug($rootScope);

        $scope.btnSelectDepositClick = function (d) {
            MainService.deposit.appliedDeposit = [];
            MainService.deposit.appliedDeposit[0] = d;
            if (MainService.appliedPaymentMethod.length == 1) {
                if (MainService.appliedPaymentMethod[0].amount == 0) MainService.appliedPaymentMethod.splice(0, 1);
            }
            MainService.calcPrice([MainService.Cart.cartList, MainService.Cart.orderList], MainService.Cart.couponObj)
            MainService.switchMode2({ modeOrder: "billing" });
        }

        $scope.btnReprintClick = function (d) {
            d.till = userConfig.page.till;
            SocketFactory.emit('depositReprint', d, function (result) {
                
            })

        }

        $scope.searchDeposit = function ($event) {
            var searchValue = $event.target.value;
            console.debug();
            searchValue = $.trim(searchValue);

            var tmpl = $scope.depositList.filter(function (d) {
                return d.depositNo.indexOf(searchValue) != -1 || d.phone.indexOf(searchValue) != -1 || moment(d.serveDate).format('YYYYMMDD').indexOf(searchValue) != -1 || ($scope.isManagement && d.refNo != null && d.refNo.indexOf(searchValue) != -1);
            })

            $scope.depositCollection = [];
            angular.extend($scope.depositCollection, tmpl);
        }

        $scope.isManagement = $scope.$parent.mode === 'management' ? true : false;
        
        $scope.searchPlaceHolder = $scope.isManagement == true ?　"Deposit No/Phone/Serve date/Ref No.":"Deposit No/Phone/Serve date"
        
    })

    app.controller("depositPanelController", function ($scope, $rootScope, MainService) {

        $scope.backPayment = function () {
            MainService.switchMode2({ modeOrder: "billing" });
        }

    })
})()
