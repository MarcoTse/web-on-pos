/********

==============================LockTable===============================

1--order
**********************************************************************
  --active
    obj.assignTableNo
    tableMode == undefined
**********************************************************************
  --release
  1)cancel click
    MainService.Cart.cancelOrder2()
    obj.mode == obj.schema.mode.order

  2)submit click (MainService.submitCart())
    MainService.submitCart()

  3)special message screen submit click (MainService.submitCart())
    MainService.submitCart()

**********************************************************************
  --alert(locked)
    (main.js) this.AdminLoadTableOrder 
    (rmsApp-v5.js) obj.assignTableNo
    if (r.result === "Locked")
======================================================================
======================================================================

2--kitchenMessage
**********************************************************************
  --active
  obj.assignTableNo
  tableMode == 'kitchenMessage'

**********************************************************************
  --release
  1)cancel click
    obj.cancelOrder2
    if (obj.modeCheckOrder('kitchenMessage'))

  2)submit click (MainService.submitCart())
    MainService.submitCart()

**********************************************************************
  --alert(locked)
  (rmsApp-v5.js) obj.assignTableNo
  if (r.result === "Locked")
======================================================================
======================================================================

3--split Table
**********************************************************************
  --active
  obj.splitTableNo 

**********************************************************************
  --release
  1)cancel click
    MainService.Cart.cancelOrder2()
    obj.modeFloorPlan == obj.schema.modeFloorPlan.split

  2)submit click
    obj.submitSplitTable

**********************************************************************
  --alert(locked)
    order.length == 1
      obj.splitTableNo
      if (r.result === "Locked")
    order.length > 1
      (main.js) this.AdminLoadTableOrder
        var callbackResult = "OK";
        var lockedTableNo = "";
        if (args.tableMode == "splitTable")
      

======================================================================
======================================================================


4--print Bill
**********************************************************************
  --active
    obj.assignTableNo
    if (tableMode === 'printBill')
**********************************************************************
  --release
  1) cancel click
    (rmsApp-v5.js)$rootScope.$on('cancelOrder', function (event, data)

  2) submit click
    obj.printOrder
    if (type == "bill")

**********************************************************************
  --alert(locked)
    (main.js) this.AdminLoadTableOrder 
    (rmsApp-v5.js) obj.assignTableNo
    if (r.result === "Locked")
======================================================================
======================================================================

5--billing
**********************************************************************
  --active
    obj.assignTableNo
    tableMode === 'billing'

**********************************************************************
  --release
  1) cancel click
    obj.cancelOrder2()
    if (obj.modeFloorPlan == obj.schema.modeFloorPlan.billing 
    && obj.mode == obj.schema.mode.order)

  2) submit click
     obj.submitCart()

**********************************************************************
  --alert(locked)
    (main.js) this.AdminLoadTableOrder 
    (rmsApp-v5.js) obj.assignTableNo
    if (r.result === "Locked")
======================================================================
======================================================================

6--open table
**********************************************************************
  1) original selected people num
    (directive-v5.js) keypadControllerNoofpeople
    $scope.next
    var oriNum = MainService.Cart.noOfPeople;
    if (MainService.Cart.noOfPeople == 0)
           MainService.switchMode2({ mode: 'floorPlan' });
    else if (oriNum == 0 && MainService.Cart.noOfPeople != 0) {
           MainService.lockTable.tableNum = MainService.Cart.tableNo;
    }

  2) select table
  --active
  obj.assignTableNo
  else if (order.header.status == undefined)

  --release
  1) input 0 people num
     (directive-v5.js) keypadControllerNoofpeople
     $scope.next  
     if (MainService.Cart.noOfPeople == 0)
     
  2) Esc key
     (directive-v5.js) keypadControllerNoofpeople
     $rootElement.on('keyup', function (e)

  --alert(locked)
    (rmsApp-v5.js) obj.assignTableNo
    if (r.result === "Locked")
======================================================================
======================================================================


7--back to main page
**********************************************************************
  commonController.js
    $scope.switchMode3

======================================================================
======================================================================

8--refresh the page
**********************************************************************
    -- active & release
    (directives-dummy.js) scope.RMSTable.renderTable => final call 
    MainService.lockTable.getAllLockTable();

======================================================================
======================================================================

9--change table
**********************************************************************
    --alert(locked)
    (main.js) this.ChangeTable
    dTableObj & sTableObj if is exist at lockTableList
    return to (rmsApp-v5.js) obj.assignTableNo => if (obj.isModeFloor('changeTable'))
    data.result == "Locked" => alert(data.tableOrderCode)
     
    
======================================================================
======================================================================

10--change people num in order
**********************************************************************
click ESC
    --release
    (directive-v5.js) keypadControllerNoofpeople
     $rootElement.on('keyup', function (e)
     || MainService.mode == MainService.schema.mode.order

======================================================================
======================================================================

*******/

