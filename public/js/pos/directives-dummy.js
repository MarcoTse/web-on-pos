(function () {
    var app = angular.module('rms.directives.dummy', []);



    app.directive('tableScene', function ($compile, $http, $animate, $interval, SocketFactory, MainService, $controller, $timeout, $rootElement, uiLanguage, INFO, $templateCache) {
        return {
            restrict: 'E',
            // not in use: stretchy-wrapper
            // template: '<svg id="svg-canvas" class="" style="visibility: visible; width: 100%; height: 100%;"></svg>',
            template: '<div class="" ng-init="init();" style="height:100%"><div class="" ng-show="MainService.modeTable == MainService.schema.modeTable.svg"><svg id="svg-canvas" style="visibility: visible;"></svg></div>\
                <div class="tableView" style="height:100%" ng-show="MainService.modeTable == MainService.schema.modeTable.tableView">\
                </div>\
            </div>',
            controller: 'tableController',
            link: function (scope, element, attrs) {
                $controller('ToastCtrl',{$scope: scope}); // passing current scope to commmon controller
                $controller('tableViewController', { $scope: scope });
                element.find('.tableView').append($compile($templateCache.get('tmp/tableView.html'))(scope))

                //console.debug(scope);
                // console.log(scope);

                // 743 16:9
                // 988 4:3
                // 
                var newLayout = '<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">\n<svg version=\"1.1\" id=\"layout_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\"\n    width=\"1920\" height=\"1440\" viewBox=\"0 0 1920 1440\" enable-background=\"new 0 0 1920 1440\" xml:space=\"preserve\">\n<g id=\"partition\">\n</g>\n<g id=\"indicator\">\n</g>\n</svg>\n';
                // SocketFactory.emit('saveTableSchema', {"layout":newLayout}, function (r) {
                //     console.log(r);
                // })

                // change dom information, add/delete/remove dom object
                SocketFactory.on('refreshTable', function (data) {
                     console.debug('refreshTable');

                     // Snap("#svg-canvas").selectAll('.transform text').forEach(function(element, index, array) {
                     // element.attr("opacity", 0)
                     // })
                     initTableScene( data );
                     // showMessage("#status-message", scope.RMSTable.messages.table_refresh, "", "warn", 30000);
                });

                // scope.$on('nextZone', function(event, args) {
                //     // console.log('respond to nextZone call');
                //     scope.RMSTable.next();
                //     // console.log(scope.RMSTable.getValue());
                //     // console.log(this);
                //     // console.log(event);
                //     // console.log(args);
                // });

                // scope.$on('prevZone', function(event, args) {
                //     // console.log('respond to prevZone call');
                //     scope.RMSTable.prev();
                // });

                // SocketFactory.on('removeTableFlag', function ( obj ) {
                //     // obj -> table info
                //     // console.log('removeTableFlag'+obj);
                //     var tableInfo = JSON.parse( obj );
                //     try{
                //         findTableEle( tableInfo.tableNo ).select( "." + tableInfo.type ).remove();
                //     }catch(e){
                //         // console.log(e);
                //     }
                    
                // });

                // SocketFactory.on('setTableFlag', function ( obj ) {
                //     // console.log('setTableFlag'+obj);
                //     var tableInfo = JSON.parse( obj );
                //     scope.RMSTable.currentActiveElementSnap = findTableEle( tableInfo.tableNo );
                //     addFlagIcon( "", tableInfo.type );
                // });

                SocketFactory.on('setChairFlag', function ( obj ) {
                   console.log('setTableFlag ', obj);
                   // scope.RMSTable.currentActiveElementSnap = findTableEle( tableInfo.tableNo );
                   var element = findTableEle( obj.tableNo );
                   addTextToElement( element, obj.orderNo );
               });


                var 
                globalScale = 3.5,
                defaultGraphicHeight = 32,
                defaultGraphicWidth = 32,
                
                fontWHRatio = 1.8, // approximately by observation 1.75-1.88, no definite data found
                offsetCanvasH = 25, // from left padding
                offsetCanvasV = -64, // from top bar
                sceneWidth = 1920,
                sceneHeight = 1080,

                // basic known settings, can be dyanmic later once the formula established without problem
                defaultDesignScreenW = 1024,
                defaultDesignScreenH = 768,
                defaultDesignContentH = 653,
                defaultViewboxW = 1920,
                defaultViewboxH,
                // defaultViewboxH = based on caculation

                // table color
                legendColor = {
                    available: "#2B2B8C",
                    inputOrder: "#EC155A",
                    using: "#0D752D",
                    printedBill: "#F16523",
                    billed: "#8D2289",
                    served: "#702D10"
                },
                statusCode = userConfig.statusCode,
                // statusCode = [];
                // statusCode[0] = "available";
                // statusCode[1] = "inputOrder";
                // statusCode[2] = "using"; // => 
                // statusCode[3] = "printedBill"; // changed to printed
                // statusCode[4] = "billed"; // => changed to billed

                buffetStatusCode = userConfig.buffetStatusCode;
                // buffetStatusCode = [];
                // buffetStatusCode[0] = "available";
                // buffetStatusCode[1] = "nearlyDoneReminder";
                // buffetStatusCode[2] = "nearlyDone";
                // buffetStatusCode[3] = "timesUp";

                function showEditMode() {
                    showMessage("#edit-mode", scope.RMSTable.editMode + " mode", "", "warn", 30000);
                }

                function isTableFree( element ) {
                    // console.log(element.parent().data());
                    if (element.parent().data("status") != 0) {
                        showMessage("#status-message", scope.RMSTable.messages.table_in_use, "", "warn", 30000);
                        return false;
                    }
                    return true;
                }

                function addFlagIcon( element, flag ) {
                    if( scope.RMSTable.currentActiveElementSnap === "" | scope.RMSTable.currentActiveElementSnap === undefined ) {
                        showMessage("#status-message", scope.RMSTable.messages.selectElementFirst, "", "warn", 30000);
                        return;
                    }

                    if( scope.RMSTable.currentActiveElementSnap.select( "."+flag ) != null ) {
                        return;
                    }

                    switch( flag ) {
                        case "vip":
                            $http.get('assets/svg/star118.svg').success(function (f) {
                            // Snap.load( "assets/svg/star118.svg", function ( f ) {
                                // console.log(arguments);
                                f = Snap.parse(f);
                                var cssClass = [];
                                cssClass.push( "flag" );
                                cssClass.push( flag );

                                try{
                                    var icon = f.select( "svg" );
                                    icon.attr( { width: 8, height: 8, class: cssClass.join(" "), fill: "gold", "filter" : "url(#dropShadowLight)" });

                                    // reposition it if there is another flag
                                    if( scope.RMSTable.currentActiveElementSnap.select(".flag") != null ) {
                                        var offsetX = scope.RMSTable.currentActiveElementSnap.selectAll(".flag").length * 8;
                                        icon.attr( { x: offsetX });
                                    }
                                    var element = scope.RMSTable.currentActiveElementSnap;
                                    element.select(".transform").append( icon );
                                }catch(e){
                                    console.log(e);
                                }
                            });
                            break;
                        case "water":
                            $http.get('assets/svg/water42.svg').success(function (f) {
                            // Snap.load( "assets/svg/water42.svg", function ( f ) {
                                f = Snap.parse(f);
                                var cssClass = [];
                                cssClass.push( "flag" );
                                cssClass.push( flag );
                                // console.log(arguments);
                                try{
                                    var icon = f.select( "svg" );
                                    icon.attr( { width: 8, height: 8, class: cssClass.join(" "), fill: "blue", "filter" : "url(#dropShadowLight)" });

                                    // reposition it if there is another flag
                                    if( scope.RMSTable.currentActiveElementSnap.select(".flag") != null ) {
                                        var offsetX = scope.RMSTable.currentActiveElementSnap.selectAll(".flag").length * 8;
                                        icon.attr( { x: offsetX });
                                    }
                                    var element = scope.RMSTable.currentActiveElementSnap;
                                    element.select(".transform").append( icon );
                                }catch(e){
                                    console.log(e);
                                }
                            })
                            break;
                        case "service":
                            $http.get('assets/svg/black302.svg').success(function (f) {
                            // Snap.load( "assets/svg/black302.svg", function ( f ) {
                                f = Snap.parse(f);
                                var cssClass = [];
                                cssClass.push( "flag" );
                                cssClass.push( flag );
                                // console.log(arguments);
                                try{
                                    var icon = f.select( "svg" );
                                    icon.attr( { width: 8, height: 8, 
                                        class: cssClass.join(" "), 
                                        fill: "greenyellow", 
                                        "filter" : 
                                        "url(#dropShadowLight)" 
                                    });

                                    // reposition it if there is another flag
                                    if( scope.RMSTable.currentActiveElementSnap.select(".flag") != null ) {
                                        var offsetX = scope.RMSTable.currentActiveElementSnap.selectAll(".flag").length * 8;
                                        icon.attr( { x: offsetX });
                                    }
                                    var element = scope.RMSTable.currentActiveElementSnap;
                                    element.select(".transform").append( icon );
                                }catch(e){
                                    console.log(e);
                                }
                            })
                            break;
                        case "bill":
                            $http.get('assets/svg/money2.svg').success(function (f) {
                            // Snap.load( "assets/svg/money2.svg", function ( f ) {
                                f = Snap.parse(f);
                                var cssClass = [];
                                cssClass.push( "flag" );
                                cssClass.push( flag );
                                // console.log(arguments);
                                try{
                                    var icon = f.select( "svg" );
                                    icon.attr( { width: 8, height: 8, class: cssClass.join(" "), fill: "green", "filter" : "url(#dropShadowLight)" });

                                    // reposition it if there is another flag
                                    if( scope.RMSTable.currentActiveElementSnap.select(".flag") != null ) {
                                        var offsetX = scope.RMSTable.currentActiveElementSnap.selectAll(".flag").length * 8;
                                        icon.attr( { x: offsetX });
                                    }
                                    var element = scope.RMSTable.currentActiveElementSnap;
                                    element.select(".transform").append( icon );
                                }catch(e){
                                    console.log(e);
                                }
                            })
                            break;
                        default:
                            break;
                    }
                }

                function addTextToElement( element, text ) {
                    console.log(element);
                    var paper = element.paper,
                        eleID = element.parent().attr('id'),
                        bbox = element.getBBox(1);

                    text = text ? text : '999';
                    console.log(element.parent().attr('id'));
                    console.log(bbox);

                    // calculate how many PO already there?

                    var poText = paper.text(0, 0, text).attr({
                        y: 3,
                            style: 'font-size: 10px',
                            fill: 'white'
                        }),
                        textBBox = poText.getBBox(1),
                        textBg = paper.rect(0,  0, bbox.w, textBBox.h / 2).attr({
                            y: textBBox.y + textBBox.h / 2 - 3,
                            fill: 'red'
                        }); // match element's width
                        // console.log(poText);

                        console.log(textBBox);
                        element.group(textBg, poText).attr({
                            class: [eleID + '-po', eleID + '-po-'+text].join(' '),
                            style: 'font-size: 11px'
                        });
                }


                function removeFlagIcon( flag ) {
                    if( scope.RMSTable.currentActiveElementSnap != "") {
                        showMessage("#status-message", scope.RMSTable.messages.selectElementFirst, "", "warn", 30000);
                        return;
                    }
                    var element = scope.RMSTable.currentActiveElementSnap;
                    try{
                        element.select( "." + flag ).remove();
                        // repositionFlagIcon( element );
                    }catch(e){
                        // console.log(e);
                    }
                }

                function findTableEle(tableNo) {
                    var svgScene = Snap("#svg-canvas"),
                        tmp;
                    svgScene.selectAll(".tableGroup .transform").forEach(function (element) {
                        if (element.select("text").attr("text") === tableNo) {
                            // console.log("found");
                            tmp = element;
                        }
                    });
                    return tmp;
                }

                scope.RMSTable = {
                    defaultFontColor : "white",
                    defaultFontSize : userConfig.table.display.defaultFontSize,
                    fontFamily : "\"Microsoft JhengHei\", STXihei, Helvetica, Arial, sans-serif",
                    currentSceneNo : null,
                    editMode : "transfer", // default: edit, order
                    messages : scope.ui,
                    groupEdit : false,
                    svgScene : Snap("#svg-canvas")
                }

                scope.RMSTable.renderPattern = function ( baseShape ) {
                    var layoutPattern = baseShape.clone().attr({
                        "id": "layoutPattern_" + this.currentSceneNo,
                        "stroke": "#000000",
                        "stroke-dasharray": "20 10"
                    });

                    layoutPattern.attr({
                        fill: Snap("#cross-line-pattern")
                    });
                };

                // scene operation
                scope.RMSTable.changeZone = function (sceneNo) {
                    MainService.RMSTable.currentSceneNo = sceneNo;
                    MainService.RMSTable.zoneID = scope.RMSTable.Scene[sceneNo].id;
                    MainService.RMSTable.sceneName = scope.RMSTable.Scene[sceneNo].name;

                    // alert(MainService.RMSTable.zoneID)

                    console.log('change scene');
                    // scene layout = zone in old version
                    // whole screen is sceen -> layout scene (zone)
                    // next or prev scene or change to scene no.
                    // console.log("reset scene");
                    removeAllHandles();
                    scope.cancelTransfer('reset');
                    // showMessage("#status-message", "", "", "warn", 30000);

                    // this.currentZoneLayoutObj = this.sceneObjArr[ sceneNo ];
                    var currentZoneLayoutObj = this.Scene[sceneNo];
                    // console.log(sceneNo);
                    // init title position, only for first time running
                    // this.svgScene.select("#title text").attr("text", currentZoneLayoutObj.name);

                    // hide all scene(layout) if there is any
                    var allLayout = $(".layoutScene");
                    if (allLayout.length != 0) {
                        // if zone rendered already, just hide and show
                        // show specified sceneNo if the scene is already exist/rendered
                        allLayout.hide();
                    }

                    var $loadScene = $("#layoutScene_" + sceneNo);
                    if ($loadScene.length != 0) {
                        // if zone rendered already, just hide and show
                        // show specified sceneNo if the scene is already exist/rendered
                        $loadScene.show();
                    } else {
                        // if zone does not exist, render it
                        // render layout scene if it is not yet exist
                        console.log('sceneNo ' + sceneNo);
                        console.log('render new Layout');
                        this.renderLayout( scope.RMSTable.Scene[sceneNo].layout, sceneNo);
                    }

                    // check if table group exist or not, let the incoming refresh function if not exist
                    if (this.svgScene.select('#layoutScene_'+sceneNo+' #table-group') === null) {
                        this.renderTable(sceneNo);
                    }
                };

                scope.changeZone = function( direction ) {
                    switch( direction ) {
                        case 'prev':
                            scope.RMSTable.prev();
                            break;
                        case 'next':
                            scope.RMSTable.next();
                            break;
                    }
                }

                scope.RMSTable.next = function () {
                    var totalScene = this.Scene.length;
                    if(this.currentSceneNo == totalScene - 1) {
                        this.currentSceneNo = 0;
                    } else {
                        this.currentSceneNo = this.currentSceneNo + 1;
                    }
                    this.changeZone(this.currentSceneNo);
                }

                scope.RMSTable.prev = function () {
                    var totalScene = this.Scene.length;
                    if(this.currentSceneNo == 0) {
                        this.currentSceneNo = totalScene - 1;
                    } else {
                        this.currentSceneNo = this.currentSceneNo - 1;
                    }
                    this.changeZone(this.currentSceneNo);
                }

                scope.RMSTable._init_edit_panel = function() {
                    var boxWidth = $(".edit-panel-boxes").outerWidth();
                    var alignBoxBBox = this.svgScene.select( "#textArea" ).getBBox();
                    var currentSceneScaleW = $( "#svg-canvas" ).outerWidth();
                    var currentSceneScaleH = $( "#svg-canvas" ).outerHeight();
                    var newPos_X = currentSceneScaleW * alignBoxBBox.x / sceneWidth;
                    var newPos_Y = currentSceneScaleH * alignBoxBBox.y / sceneHeight;

                    // to align to right hand side instead, plus the width difference can do, ie
                    var alignBoxRelativeW = alignBoxBBox.w / sceneWidth * currentSceneScaleW;
                    var widthDiff = alignBoxRelativeW - boxWidth;
                    newPos_X = newPos_X + widthDiff;

                    $(".edit-panel-boxes").css( { "left": newPos_X, "top": newPos_Y } );
                };

                function findModified() {
                    MainService.RMSTable.svgScene.selectAll('.table').forEach(function(ele){
                        // console.log(ele.data('modified'))
                        if(ele.data('modified')) {
                            return true;
                        }
                    })
                }

                // ele: transformation group of the element
                // eg. node with ID => node with ".shape" class
                scope.RMSTable.makeElementActive = function( ele ){
                    // Snap('#svg-canvas').select('#table_1418785047372').data()
                    if( findModified() ) {
                        scope.saveTable();
                    }

                    // too much parent and child dependency, make it confuse and hard to be generic

                    // console.log(ele.parent().data());
                    // console.log(ele.data("id"));
                    // console.log(ele.selectAll("path"));
                    // console.log(ele);
                    
                    // remove all previous added selection indication box
                    try {
                        this.svgScene.select(".selected-box").remove();
                    } catch(e){
                    }

                    // update current selected item to global variable
                    try {
                        // console.log("try 2-1");
                        this.currentActiveElement = ele.parent().data("id");
                        this.currentActiveElementSnap = ele.parent();
                        this.currentTableNo = ele.select("text").attr("text");
                    } catch(e){
                        // console.log("e 2-1");
                        try{
                            // console.log("try 2-2");
                            this.currentActiveElement = ele.attr("id");
                            this.currentActiveElementSnap = ele;
                        }catch(e){
                            // console.log("e 2-2");
                            this.currentActiveElement = ele.attr("id");
                            this.currentActiveElementSnap = ele;
                        }
                    }

                    // add active indication box to current selected element and update active element ID to variable RMS.currentActiveElement
                    var eleBBox = ele.getBBox();
                        // boxSVGStr = "<rect class='selected-box' x='0' y='0' width='"+defaultGraphicWidth+"' height='"+defaultGraphicHeight+"' fill='none' style='stroke: #f00; stroke-width: 1.3px; stroke-dasharray: 5 2'></rect>";
                    // console.log(eleBBox);
                    // ele.append( Snap.parse( boxSVGStr ) );

                    // update label-box to reflect current selected box to update
                    try{
                        var currentLabel = ele.select("text").attr("text");
                        $("#label-box").val(currentLabel);
                        // console.log(currentLabel);

                        // console.log( ele.data( "shape" ) );
                        // update size list to reflect current shape and size
                        // shape-name
                        var shape = ele.parent().data( "shape" );
                        var size = ele.parent().data( "size" );
                        var shapeArr = getAllShape(), optionList = "";
                        for(var i = 0; i < shapeArr.length; i++ ) {
                            optionList += "<option value=" + shapeArr[i].toLowerCase() + ">" + shapeArr[i] + "</option>";
                        }
                        $(".shape-name").html(optionList).val( shape );
                        // console.log(optionList);
                        // console.log(getShapeAllSize( shape ));

                        // shape-size
                        var sizeArr = getShapeAllSize( shape ), optionList = "";
                        for(var i = 0; i < sizeArr.length; i++ ) {
                            optionList += "<option value=" + sizeArr[i] + ">" + sizeArr[i] + " people</option>";
                        }
                        $(".shape-size").html(optionList).val( size );
                        // console.log(optionList);
                        // console.log(size);
                    }catch(e){
                        // console.log(e);
                        $("#label-box").val("");
                    }

                    // reinit the boxes position
                    // this._init_edit_panel();

                    // if previous = current, remove selection box only
                };

                scope.RMSTable.centerLabel = function (attachEle, textEle) {
                    // console.log(attachEle.selectAll("rect"));
                    // console.log(textEle.parent().parent().attr('id'));
                    
                    // textEle.parent().parent().attr("opacity", 0); // parent box
                    textEle.attr("opacity", 0); // text

                    var textBBox = textEle.getBBox();
                    var attachBBox = attachEle.getBBox();
                    
                    // $interval(function(){
                    //     console.log(textBBox);
                    // }, 5000);


                    // console.log(textBBox);
                    // console.log(attachBBox);

                    // var attachW = Snap("#svg-canvas").select("#table_1418785047317 rect").attr("width");
                    // var attachH = Snap("#svg-canvas").select("#table_1418785047317 rect").attr("height");
                    // console.log(Snap("#svg-canvas").select("#table_1418785047317 rect").attr("width"));
                    // console.log(textEle.getBBox());

                    // if (attachEle.parent().parent().data("scale_X") != undefined) {
                    //     var scale_X = attachEle.parent().parent().data("scale_X");
                    //     var scale_Y = attachEle.parent().parent().data("scale_Y");
                    // } else {
                    //     var scale_X = attachEle.parent().data("scale_X");
                    //     var scale_Y = attachEle.parent().data("scale_Y");
                    // }

                    // console.log(attachEle.parent().parent());
                    // console.log("attachBBox.w " + attachBBox.w);
                    // console.log("label_X " + label_X);
                    // not accurate // plus half of the height manually
                    // var label_X = ( (attachBBox.w - textBBox.w)/2 );
                    // var label_Y = ( (attachBBox.h + textBBox.h)/2 );
                    // console.log('textBBox.w ' + textBBox.w);
                    // console.log('textBBox.h ' + textBBox.h);
                    // var label_X = ((attachW - textBBox.w) / 2);

                    setTimeout(function(){
                        var label_X = ((attachBBox.w - textBBox.w) / 2);
                        var label_Y = ((textBBox.h * 2 - attachBBox.h/2)); // not sure why, just follow the numerical value to make formula

                        // find the space between top of text and actual bounding box (text height from its zero Y is offset ) (O)
                        // textBBox.h/2 is the half of text height with space above (A)
                        // actual center without space from attachBBox is (attachBBox.h - textBBox.h)/2 (B)
                        // so the space A - B, and the actual offset is O + A - B
                        // textBBox.h + textBBox.h/2 - (attachBBox.h - textBBox.h)/2  
                        // simplified formula is => textBBox.h + textBBox.h/2 - attachBBox.h/2 + textBBox.h/2  

                        if( textEle.parent().parent().attr('id') === "table_1418785047429" ) {
                            // console.log(textBBox);
                            // console.log(attachBBox);
                            // console.log(textBBox.h);
                            // console.log((attachBBox.h - textBBox.h)/2);
                            // console.log(textBBox.h + textBBox.h/2 - (attachBBox.h - textBBox.h)/2);
                            // 25 + 18/2 - (25 - 18)/2 = 34 - 7/2 = 31.5
                            // console.log(textBBox.h * 2 - attachBBox.h/2 );
                            // console.log(attachBBox.h - textBBox.h/2);

                            // 18 + 18/2 = 9 - (30 - 18)/2 = 6 18 + 3 = 21
                        }

                        // label_Y still is not correct in calculation @ 2015/11/03
                        textEle.attr({ "x": label_X, "y": 14 });
                        // console.log('label_X', label_X);
                        // console.log('label_Y', label_Y);
                        // textEle.parent().parent().attr("opacity", 1);
                        textEle.attr("opacity", 1);
                    }, 1)

                    // hard code for demo
                    // textEle.attr({ "x": 10, "y": 21 }); // 12 / 21 for 1 character, 8 / 21 for 2 characters
                }; // constraint proportion

                scope.RMSTable.addHoverRect2 = function (element, index, array) {
                    // console.log('addHoverRect2');
                    var referencePoint = (this.ref != undefined && this.ref != "") ? this.ref : "absolute";
                    var BBox = element.getBBox();
                    // console.log(BBox);
                    
                    // $timeout(function(){
                    //     console.log(BBox);
                    // }, 1000);


                    // var tmp = 0;
                    // var renderTrial = $interval(function(){
                    //     // console.log(tmp);
                    //     // console.log(BBox);
                    //     if( BBox.width != 0 ) {
                    //         $interval.cancel(renderTrial);
                    //     } else {
                    //         console.log(element);
                    //         $interval.cancel(renderTrial);
                    //     }
                    //     tmp = tmp + 10;
                    // }, tmp);
                    
                    // console.log('element BBox');
                    // console.log(element.getBBox());

                    // console.log("----------------------------------------");
                    // console.log( element.attr("id") );
                    // console.log(this);
                    // console.log(this.ref);
                    // console.log("----------------------------------------");

                    function renderHoverRect() {
                        switch (referencePoint) {
                            default:
                            case "absolute":
                                // for non-text element
                                var pos_X = BBox.x;
                                var pos_Y = BBox.y;
                                break;
                            case "relative":
                                // for text element (since its X and Y is relative to , like Google Slide, they are also using transform group for transformation and leave x and y untouched for relative calculation)
                                var pos_X = 0;
                                var pos_Y = 0;
                                break;
                        }

                        
                        if( element.parent().attr('id') === "table_1418785047317" ) {
                            // console.log('element bbox');
                            // console.log(BBox);
                        }

                        // don't count any scale or things, it will make things complex, scale should be performed after object creation
                        element.prepend(element.rect(pos_X, pos_Y, BBox.w, BBox.h).attr({
                            "opacity": "0",
                            "class": "hoverArea"
                        }));
                        delete this.ref;
                    }
                    renderHoverRect();

                    // switch (referencePoint) {
                    //     default:
                    //     case "absolute":
                    //         // for non-text element
                    //         var pos_X = BBox.x;
                    //         var pos_Y = BBox.y;
                    //         break;
                    //     case "relative":
                    //         // for text element (since its X and Y is relative to , like Google Slide, they are also using transform group for transformation and leave x and y untouched for relative calculation)
                    //         var pos_X = 0;
                    //         var pos_Y = 0;
                    //         break;
                    // }

                    // if( element.attr('class') === "table_1418785047317" ) {
                    //     // console.log('element bbox');
                    //     // console.log(BBox);
                    // }

                    // // don't count any scale or things, it will make things complex, scale should be performed after object creation
                    // element.prepend(element.rect(pos_X, pos_Y, BBox.w, BBox.h).attr({
                    //     "opacity": "0",
                    //     "class": "hoverArea"
                    // }));
                    // delete this.ref;
                };

                /*
                    stick to 4:3
                    var fourToThreeRatio = 1920/1024;
                    var fourToThreeH = 768 * fourToThreeRatio;
                    var fourToThreeW = 1024 * fourToThreeRatio;
                    scope.RMSTable._viewBox = [0, 0, 1920, 1080];
                    scope.RMSTable._viewBox = [0, 0, fourToThreeW, fourToThreeH];
                */

                /*
                    for a screen with 1024x768, the visible area for md-content(table-scene) is 1024 / 653
                    the SVG design is based on 1920 so, the SVG height is
                    1024 / 653 = 1920 / h
                    h = 1920 * 653 / 1024 = 1920 * table-scene height / table-scene width

                    on the other hand, when screen available area is bigger or smaller, the SVG viewport will render differently relative to the screen available width/height
                    to maintain same aspect ratio for the viewbox while changing it to fit the screen width/height

                    all design is based on 1920 x 1080 16:9 and 4:3 is based on 1920 x 1440
                    so base ratio is 1920*
                */
                var screenRatioW = $(window).width() / 1024; // 1920 is layout, normally it design for 1024, it is 1024, ratio = 1
                // screenRatioW = 1

                // elastic viewbox

                // based on default design screen w/h and content H, the viewboxH is 1920 x 1224
                if( angular.isUndefined(scope.RMSTable.defaultViewboxH) ) {
                    defaultViewboxH = scope.RMSTable.defaultViewboxH = sceneWidth * defaultDesignContentH / defaultDesignScreenW;
                }

                // for background image (need to further fine tune)
                var elasticRatio = defaultViewboxW / $('table-scene').width();
                var elasticH =  Math.round( $('table-scene').height() ) * elasticRatio;
                var elasticW =  Math.round( $('table-scene').width() ) * elasticRatio;
                var tableSceneRatio = Math.max($('table-scene').height(), $('table-scene').width()) / Math.min($('table-scene').height(), $('table-scene').width()); // always get the bigger side / smaller side

                // scene area
                var areaRatio = $('table-scene').width() / $('table-scene').height();
                // var areaRatio = $(window).width() / $(window).height();
                if( screen.width < screen.height ) {
                    scope.RMSTable._viewBox = [0, 0, defaultViewboxW , defaultViewboxW / areaRatio ];
                } else {
                    scope.RMSTable._viewBox = [0, 0, defaultViewboxH * areaRatio , defaultViewboxH ];
                }

                scope.tableMouseDown = function() {
                    // console.log(scope);
                    var args = Array.prototype.slice.call(arguments).concat([scope]);
                    tableMouseDown2.apply( scope, args );
                };

                scope.RMSTable.renderSVGLayout = function (layout, sceneNo) {
                    console.log('scope.RMSTable.renderSVGLayout');
                    // for pure svg layout
                    var layoutGroup = this.svgScene.group(Snap.parse(layout).selectAll("g")).attr({
                        // "id": "layout",
                        "class": "layout"
                    });

                    // change default layer ID to prevent ID collision in DOM
                    // layoutGroup.select("#partition").attr({
                    //     "id": "partition",
                    //     "class": "partition"
                    // });

                    // layoutGroup.select("#indicator").attr({
                    //     "id": "indicator",
                    //     "class": "indicator"
                    // });

                    // layoutGroup.select("#basic").attr({
                    //     "id": "basic_" + sceneNo,
                    //     "class": "basic"
                    // });

                    // var basicShape = layoutGroup.select("#basic_" + sceneNo + " polygon").attr({
                    // var basicShape = layoutGroup.select("#basic_" + sceneNo + " rect").attr({
                    //     "id": "layoutShape_" + sceneNo,
                    //     "class": "layoutShape"
                    // });

                    // this.renderPattern(basicShape); // use basucShape as a reference layout to create pattern

                    // <g id=\"basic\">\n<image overflow=\"visible\" width=\"1920\" height=\"1440\" xlink:href=\"data/background.png\">\n <rect x=\"0\" fill=\"#eeeeee\" width=\"100%\" height=\"100%\"/>\n </image></g>

                    

                    this.svgScene.select("#floorGroup").group(layoutGroup).attr({
                        "id": "layoutScene_" + sceneNo,
                        "class": "layoutScene"
                    });


                    // this.svgScene.attr({
                    //     "width": 1920,
                    //     "height": this.sceneHeight,
                    //     "viewBox": "0 0 1920 " + this.sceneHeight // 16:9
                    // });

                    // this.workArea = {
                    //     w: this.svgScene.select("#work-canvas").getBBox().w,
                    //     h: this.svgScene.select("#work-canvas").getBBox().h
                    // }

                    // init title position, only for first time running
                    // var sceneTitle = currentZoneLayoutObj.name;
                    // svgScene.select("#title text").attr("text", sceneTitle);

                    // console.log(Snap.path.toRelative(layout));
                    // var test = Snap.path.toRelative(layout);

                    // console.log(layout);

                    // this.layoutArea = {
                    //     w: layoutGroup.getBBox().width,
                    //     h: layoutGroup.getBBox().height
                    // }

                    // offset_X = (this.workArea.w - this.layoutArea.w) / 2 + offsetCanvasH;
                    // offset_Y = (this.workArea.h - this.layoutArea.h) / 2 + offsetCanvasV;
                    // not autmatically offset anymore
                    // offset_X = 0;
                    // offset_Y = 0;

                    // console.log(offsetCanvasV);
                    // console.log((this.workArea.h - this.layoutArea.h) / 2);
                    // console.log(offset_Y);
                    // console.log(Snap("#svg-canvas").select("#layout_1").getBBox());
                    // console.log(layoutGroup.getBBox());
                    // console.log(this.workArea);
                    // console.log(this.layoutArea);
                    // console.log(offset_X);
                    // console.log(offset_Y);

                    // auto resize the layout to fit the screen if there is not saved transformation for this layout

                    // layout is not in used now
                    // eleMatrix = new Snap.Matrix();
                    // eleMatrix.translate(offset_X, offset_Y);
                    // eleMatrix.scale(this._viewBox[2] / this.layoutArea.w, this._viewBox[2] / this.layoutArea.w);
                    // layoutGroup.transform(eleMatrix);

                    // console.log(this.layoutArea);
                    // console.log(this._viewBox );
                    // console.log(this._viewBox[2] );

                    
                }

                scope.RMSTable.renderBasic = function() {
                    this.sceneHeight = 950.4;
                    var paper = this.svgScene.paper,
                        screenRatio = 16/9,
                        _height = 100;

                    this.svgScene.attr({
                        // "width": "screen.width",
                        // "height": screen.width / screenRatio,
                        "width": "100%",
                        "height": _height + "%",
                        "viewBox": this._viewBox.join(" "),
                        "preserveAspectRatio": "xMinYMin meet"
                    });

                    var paperSize = {
                            w: paper.node.clientWidth,
                            h: paper.node.clientHeight
                        };

                    // background for all scenes
                    var bg = this.svgScene.image('data/background.png', 0, 0, 1920, 1440).attr({
                        "preserveAspectRatio": "xMidYMid meet"
                        }),
                        basic = this.svgScene.group(bg).attr({
                            "id": "basic"
                        });

                    // resize background image
                    if( typeof this.bgNativeRatio === 'undefined' ) {
                        this.bgNativeRatio = bg.attr('width') / bg.attr('height');
                        this.bgNativeWidth = bg.attr('width');
                        this.bgNativeHeight = bg.attr('height');
                    };
                    // console.log(this.svgScene.select('image').attr('height'));
                    bg.attr({
                        width: this.bgNativeWidth * tableSceneRatio,
                        height: this.bgNativeHeight * tableSceneRatio
                    });
                    // bg.attr({
                    //     width: elasticW * screenRatioW,
                    //     height: elasticW * screenRatioW / this.bgNativeRatio
                    // });

                    // add working helper for current screen available area
                    // since viewport is based on 1920 x 1440, so a 1024 x 768 monitor is mapped to 1920...
                    // thus W * 1.875 and H * 1.875

                    var helperRect = this.svgScene.rect(0,0,1920,653*1.875).attr({
                        fill:'none', stroke: 'red', 'stroke-dasharray': '12px, 12px', 'stroke-width': '8px', opacity: 0.2
                    }),

                    helperText = this.svgScene.text(1050, 1180, '1024 x 768 Monitor Area').attr({
                        fill: 'red', 
                        style: "font-size: 70px; font-weight: bold; font-family: 'Microsoft JhengHei', STXihei, Helvetica, Arial, sans-serif; opacity: 0.7; stroke: black; stroke-width: 2px;"
                    });

                    var vwHelper = this.svgScene.group(helperRect, helperText).attr({
                        class: 'viewport-helper ng-hide'
                    });

                    // add grid helper
                    var p = this.svgScene.path("M 50 0 L 0 0 0 50").attr({
                            fill: "none",
                            stroke: "#333",
                            strokeWidth: 0.5
                        }).pattern(0, 0, 50, 50);
                    
                    var gridHelper = this.svgScene.rect(0, 0, '100%', '100%').attr({
                        id: 'grid-helper',
                        fill: p
                        // class: 'ng-hide'
                    });

                    var helper = this.svgScene.group(gridHelper, vwHelper).attr({
                        id: 'helper-container'
                    });

                    basic.insertBefore( this.svgScene.select("#floorGroup") );
                    helper.insertAfter( basic );
                    $('.btn-grid').addClass('active');

                    // console.log(Snap('#svg-canvas').select('image').getBBox());
                    var imgBBox = this.svgScene.select('image').getBBox();
                    this.bgNativeRatio = imgBBox.w / imgBBox.h;

                    $(window).on({'resize': function(){
                        // console.log("I am doing my job");

                        /*
                            var currentLayout = scope.RMSTable.svgScene.select("#layout_" + scope.RMSTable.currentSceneNo);

                            // need to calculate with the 1/1 scale instead of scaled width
                            var layoutMatrix = currentLayout.attr("transform").totalMatrix.split();

                            var layoutArea = {
                                w: currentLayout.getBBox().w / layoutMatrix.scalex,
                                h: currentLayout.getBBox().h / layoutMatrix.scaley
                            }
                            // console.log(layoutGroup);
                            // console.log(layoutMatrix);
                            // console.log(layoutArea);

                            eleMatrix = new Snap.Matrix();
                            eleMatrix.scale(scope.RMSTable._viewBox[2] / layoutArea.w, scope.RMSTable._viewBox[2] / layoutArea.w);
                            currentLayout.transform(eleMatrix);
                        */

                            // maintain same aspect ratio for the viewbox while changing it to fit the screen width/height
                            var screenRatioW = $(window).width() / 1024; // 1920 is layout, normally it design for 1024, it is 1024, ratio = 1

                            // elastic viewbox
                            var elasticRatio = defaultViewboxW / $('table-scene').width();
                            var elasticH =  Math.round( $('table-scene').height() ) * elasticRatio;
                            var elasticW =  Math.round( $('table-scene').width() ) * elasticRatio;
                            var tableSceneRatio = Math.max($('table-scene').height(), $('table-scene').width()) / Math.min($('table-scene').height(), $('table-scene').width()); // always get the bigger side / smaller side

                            // this method keep the same aspect ratio
                            // var _viewBox = [0, 0, elasticW * screenRatioW, elasticH * screenRatioW ];

                            // scene area
                            var areaRatio = $('table-scene').width() / $('table-scene').height();
                            // var areaRatio = $(window).width() / $(window).height();
                            if( screen.width < screen.height ) {
                                _viewBox = [0, 0, defaultViewboxW , defaultViewboxW / areaRatio ];
                            } else {
                                _viewBox = [0, 0, defaultViewboxH * areaRatio , defaultViewboxH ];
                            }

                            // resize background image
                            if( typeof scope.RMSTable.bgNativeRatio === 'undefined' ) {
                                scope.RMSTable.bgNativeRatio = scope.RMSTable.svgScene.select('image').attr('width') / scope.RMSTable.svgScene.select('image').attr('height');
                            }
                            // console.log(scope.RMSTable.svgScene.select('image').attr('height'));
                            scope.RMSTable.svgScene.select('image').attr({
                                width: scope.RMSTable.bgNativeWidth * tableSceneRatio,
                                height: scope.RMSTable.bgNativeHeight * tableSceneRatio
                            })
                            // scope.RMSTable.svgScene.select('image').attr({
                            //     width: elasticW * screenRatioW,
                            //     height: elasticW * screenRatioW / scope.RMSTable.bgNativeRatio
                            // })

                            // console.log('screenRatio ' + screenRatio);
                            // console.log('elasticRatio ' + elasticRatio);

                            MainService.RMSTable._viewBox = scope.RMSTable._viewBox = _viewBox;
                            scope.RMSTable.svgScene.attr({
                                "viewBox": _viewBox.join(" ")
                            });
                        }
                    });
                }

                scope.RMSTable.renderLayout = function (layout, sceneNo) {
                    console.log('scope.RMSTable.renderLayout');
                    // var currentZoneLayoutObj = this.Scene[sceneNo];
                    // console.log(this == $scope.RMSTable); // true
                    // console.log(this.Scene);
                    // console.log(sceneNo);

                    // this.svgScene.prepend(this.svgScene.rect(0, 0, 1920, this.sceneHeight).attr({ id: "work-canvas", fill: "transparent" }));

                    this.renderSVGLayout(layout, sceneNo);

                    console.log('current scene no ' + sceneNo);
                    this.renderTable(sceneNo);
                    try {
                    } catch (e) {
                        console.log(e);
                        console.log("Please re-confirm if provided layout is a SVG string.");
                    }
                };

                scope.RMSTable.renderTable = function (sceneNo) {
                    console.log('scope.RMSTable.renderTable');
                    // console.log("render table");
                    var currentZoneLayoutObj = this.Scene[this.currentSceneNo];
                    //console.log(currentZoneLayoutObj);
                    // create table group to group all table object inside
                    // if( masterTableEleGroup = this.scene.select( "tableGroup_" + sceneNo ) === null ) {

                    // console.log(this.svgScene.getBBox());

                    var masterTableEleGroup = this.svgScene.select("#layoutScene_" + sceneNo).group().attr({
                        "id": "table-group",
                        "class": "tableGroup"
                    });

                    var tableF, tableNode, eleMatrix;

                    // $floorPlanCanvas = $("#floorGroup");
                    // console.log('all table object in current scene');
                    // console.log(currentZoneLayoutObj.memberObject);
                    for (var i = 0; i < currentZoneLayoutObj.memberObject.length; i++) {
                        // if in case, want the shape completely free from global scale
                        // if( currentZoneLayoutObj.memberObject[i].globalScale != false)
                        //     scale_X = scale_Y = globalScale;
                        var loadedData = currentZoneLayoutObj.memberObject[i];
                        // console.log(loadedData);
                        // console.log(loadedData);

                        var currentTableSVGStr = getTableSVG.apply( scope, [loadedData.shape, loadedData.size]);
                        var className = loadedData.shape + "-" + loadedData.size;
                        var noOfChar = loadedData.label.length;
                        var ID = "table" + "_" + loadedData.uid;

                        // place the text in the center of the shape, use BBox to obtain the accuracy
                        // var label_x = (defaultGraphicWidth/2 - 2) - Math.ceil(defaultFontSize/fontWHRatio/2)*(noOfChar-1);
                        // var label_y = defaultGraphicHeight/2 + Math.floor(defaultFontSize/2);

                        // table element for adding:
                        // render object node to scene and store data to it for update later
                        tableF = Snap.parse(currentTableSVGStr);
                        tableNode = tableF.select("g");
                        
                        // console.log(tableNode.getBBox());
                        // tableShapeNode = tableF.select("g");

                        // restore color base on status code
                        try {
                            var code;
                            if( loadedData.status == 2 && loadedData.buffetStatus > 0 ) {
                                colorCode = buffetStatusCode[loadedData.buffetStatus];
                            } else {
                                colorCode = statusCode[loadedData.status];
                            }
                            // console.log('loadedData.status ' + loadedData.status);

                            // console.log(loadedData.status);
                            // if( loadedData.status != 2 ) {
                            //     // default color
                                
                            // }

                            // tableNode.parent().parent().attr({
                            //     "class": colorCode + ' shape'
                            // });

                            // use class instead @ 20151106
                            // tableNode.selectAll("path").attr({
                            //     fill: legendColor[colorCode]
                            // });
                        } catch (e) {
                        }

                        // add to scene
                        var gTransform = masterTableEleGroup.group(tableNode).attr("class", "transform");

                        // console.log(masterTableEleGroup.getBBox());
                        // console.log(tableNode.getBBox());
                        // console.log(gTransform.getBBox());
                        var tableContainerGroup = masterTableEleGroup.group(gTransform).attr({
                            "id": ID, // current table ID
                            "class": className + ' table ' + colorCode,
                            'ng-click': 'tableMouseDown("'+ID+'");'
                        });
                        // var gTransform = masterTableEleGroup.group(tableNode).attr("class", "transform");
                        tableContainerGroup.data('loadedData', loadedData);

                        // console.log(tableF.select("path").getBBox()); // no width/height
                        // console.log(tableF.select("g").getBBox()); // no width/height
                        // console.log(tableNode.getBBox()); // no width/height before group to gTransform
                        // console.log(gTransform.getBBox()); // no width/height

                        // add data
                        // the following will be stored in browser instead of attribute,
                        // to make it more compatible with other tools, use attribute instead or
                        // get the data with SVG tool when needed
                        for (var key in loadedData) {
                            tableContainerGroup.data(key, loadedData[key]);
                        }
                        tableContainerGroup.data('id', ID);
                        tableContainerGroup.data('zone', loadedData.zone);
                        tableContainerGroup.data('state', false); // for single/multiple table saving
                        tableContainerGroup.data('status', loadedData.status);

                        // before append, all BBox consist of 0 value
                        this.ref = "relative";
                        this.addHoverRect2(gTransform);
                        // console.log(gTransform.select( ".hoverArea" ));

                        // add label
                        var tableLabel = this.svgScene.text(0, 0, "").attr({
                            "fill": this.defaultFontColor,
                            "font-size": this.defaultFontSize,
                            "font-family": this.fontFamily
                        });
                        gTransform.append(tableLabel);
                        // tableLabel.getBBox(); // run once before text assign to calibrate, not sure if it is the bug of Snap SVG, after run one, figure seems correct
                        tableLabel.attr("text", loadedData.label);
                        // console.log(tableLabel.getBBox());

                        // console.log(ID + ' original 1');
                        // console.log('w ' +tableLabel.getBBox().w);
                        // console.log('h ' + tableLabel.getBBox().h);
                        // console.log('--------------------------------------------------------------------');

                        // add transformation, always come last is better for data accuracy and easy maintenance

                        // console.log(tableNode.getBBox());
                        // console.log(gTransform.getBBox());

                        var eleBBox = gTransform.select('rect').getBBox();
                        if( ID == "table_1418785047402") {
                            // console.log(loadedData.pos_X);
                            // console.log(loadedData.pos_Y);
                        // console.log(eleMatrix);
                        // console.log(eleMatrix.toTransformString());
                        // console.log('gTransform bbox ');
                        // console.log(eleBBox);
                        }
                        /*
                        eleMatrix = new Snap.Matrix();
                        // eleMatrix.translate(loadedData.pos_X, loadedData.pos_Y);
                        // eleMatrix.scale(loadedData.scale_X, loadedData.scale_Y);
                        // eleMatrix.rotate( 0 ); 
                        // eleMatrix.rotate(45, 0, 0);

                        eleMatrix.rotate(loadedData.angle_A, eleBBox.cx, eleBBox.cy);
                        // console.log(eleMatrix.toTransformString());
                        // eleMatrix.toTransformString();
                        gTransform.transform(eleMatrix); // after transform, width, height, x, y of BBox are different
                        */

                        // auto offset with the cX and cY
                        // console.log(eleBBox);
                        gTransform.transform([
                            'R' + loadedData.angle_A, eleBBox.cx, eleBBox.cy,
                            'S' + loadedData.scale_X, loadedData.scale_Y, eleBBox.cx, eleBBox.cy,
                            'T' + loadedData.pos_X, loadedData.pos_Y

                            // with first time offset
                            // 'T' + (loadedData.pos_X + eleBBox.cx * (userConfig.table.display.globalScale - 1)), (loadedData.pos_Y + eleBBox.cy * (userConfig.table.display.globalScale - 1))
                        ].join(','));

                        // for easy tracking
                        gTransform.parent().attr({
                            'ref-posX': loadedData.pos_X,
                            'ref-posY': loadedData.pos_Y
                        });

                        // console.log(eleMatrix.toTransformString());

                        // gTransform.mousedown(function () {
                        // gTransform.mouseup(function () {
                            // tableMouseDown(this);
                        // })

                        // some computer chrome even same version reports incorrect bbox info (before transformation), put it here, it is ensure the updated transformation information
                        this.centerLabel(gTransform.select(".hoverArea"), tableLabel);
                        // console.log(gTransform.select(".hoverArea"));
                        // console.log(gTransform.select(".hoverArea").getBBox());
                        // console.log(ID + ' original 2');
                        // console.log('w ' +tableLabel.getBBox().w);
                        // console.log('h ' + tableLabel.getBBox().h);
                        // console.log('--------------------------------------------------------------------');

                        /*// add event handler
                        var move = function(dx, dy, x, y, obj) {
                            console.log("dragging moving");
                        }

                        var start = function() {
                                // this.data('origTransform', this.transform().local );
                        }

                        var stop = function() {
                                console.log('finished dragging');
                        }

                        gTransform.drag( move, start, stop );*/
                        // test drag ratio to viewbox
                        // svgScene.select("#floorGroup").drag( move, start, stop );

                        // use ng-click instead of snap svg library
                        // yet, compile may make this slower

                        // console.log($('#'+ID));
                        // console.log(ID);
                        $compile($('#' + ID))(scope);

                        $timeout(function () {
                            MainService.lockTable.getAllLockTable();
                        }, 1200)
                    } // for
                }

                scope.RMSTable.makeOrder = function (ele) {
                    var tableNo = ele.select("text").attr("text");
                    var uid = ele.parent().data("uid");
                    console.log('tableNo ' + tableNo);
                    //element.addClass('ng-hide');
                    // console.log(scope.$emit);
                    // console.log(MainService);

                    // MainService.assignTableNo(tableNo);
                    // var status = Snap("#svg-canvas").select('#table_1418785047317').data().loadedData.status;
                    // console.log(ele.parent().data().loadedData);
                    var status = ele.parent().data().loadedData.status;
                    // console.log(status);
                    
                    MainService.clickTable(tableNo, ele);
                    scope.message = "Table " + tableNo + " is Selected / " + MainService.RMSTable.editMode;
                    // scope.showCustomToast( 5000 );

                    //scope.$emit('modeTableOrder', { 'tableNo': tableNo }, function (r) {
                    //    console.log(r);
                    //});
                    // console.log(tableNo);
                    /*if( typeof tableNo != "undefined" ) {
                        this.changeElementColor(ele, legendColor.inputOrder );
                        SocketFactory.emit('setTableStatus', { tableNo: tableNo , status : 1});
                        window.location = "order.html?tableNo=" + tableNo;
                        //  + "&uid=" + uid
                    }*/

                    // place new action here
                }

                scope.RMSTable.returnDataObject = function( eleID ) {
                    var currentZoneLayoutObj = this.Scene[this.currentSceneNo];
                    // for multiple Scene support, use sceneID later, use sceneID to check and then loop through member object
                    for( var i = 0; i < currentZoneLayoutObj.memberObject.length; i++ ) {
                        if( currentZoneLayoutObj.memberObject[i].uid === eleID ) {
                            return currentZoneLayoutObj.memberObject[i];
                        }
                    }
                    return null;
                }; // returnDataObject

                scope.RMSTable.updateDataObject = function( data ) {
                    var currentZoneLayoutObj = this.Scene[this.currentSceneNo];
                    // for multiple Scene support, use sceneID later, use sceneID to check and then loop through member object
                    eleID = data.uid;
                    var savedObj = this.returnDataObject( eleID );
                    // console.log("returned Data Object");
                    // console.log(this.returnDataObject( eleID ));
                    // console.log("update id: " + eleID);
                    // console.log(savedObj);
                    // console.log(data);

                    // create an object from data same structure as loaded object for accurately compare and add
                    var tmpObj = {};
                    for( var key in data ) {
                        if( key != "id" ) {
                            tmpObj[ key ] = data[ key ];
                        }
                    }

                    // add to object if it does not exist in existing object
                    if( savedObj === null ) {
                        currentZoneLayoutObj.memberObject.push( tmpObj );
                        return "new data is added";
                    }

                    // update to object if exist
                    for( var key in savedObj) {
                        if( data[key] != undefined ) {
                            savedObj[ key ] = data[ key ];
                        }
                    }
                    // savedObj.angle_A = 0
                    // console.log(savedObj.angle_A);
                    // console.log(savedObj.pos_X);
                    // console.log(this.currentZoneLayoutObj.memberObject[7].pos_X);
                    // console.log(sceneJSON.scene[1].memberObject[7].pos_X);
                    return "data is updated";
                } // updateDataObject

                function loadTableScene( data ){
                   console.log('loadTableScene');
                   // console.log(data);
                   if( typeof data === 'string' ) {
                        scope.RMSTable.Scene = JSON.parse(data).scene; // default load first one
                   } else {
                        scope.RMSTable.Scene = data.scene;
                   }

                    scope.tableView.initTableView(scope.RMSTable.Scene[0].memberObject, scope.RMSTable.Scene[0].tableViewModel, scope.RMSTable.Scene[0].serverTime);
                   scope.RMSTable.noOfScene = scope.RMSTable.Scene.length;

                   window.sceneDataNeedle = scope.RMSTable.Scene;
                    
                   // prevObj = JSON.parse(r).scene;
                   // console.log(scope.RMSTable.Scene);

                   if( scope.RMSTable.currentSceneNo === null || scope.RMSTable.svgScene.select("#floorGroup") === null) {
                        console.log('first time load scene RMSTable.currentSceneNo');
                       scope.RMSTable.currentSceneNo = 0; // suppose to be a var called in function
                       // scope.RMSTable.Scene.length - 1
                       scope.RMSTable.sceneName = scope.RMSTable.Scene[scope.RMSTable.currentSceneNo].name; // suppose to be a var called in function
                       scope.RMSTable.zoneID = scope.RMSTable.Scene[scope.RMSTable.currentSceneNo].id; // suppose to be a var called in function

                       // console.log(scope.RMSTable);
                       // console.log(MainService.RMSTable);

                       console.log('then2 render scene');
                       // console.log(scope.RMSTable.svgScene.select("#floorGroup"));
                       
                       // Snap.load("rms.svg", function (f) {
                        $http.get('rms2.svg').success(function (f) { // timing is better than Snap's for angular
                            // console.log(Snap.parse(data));
                           var f = Snap.parse(f);
                           // svgScene.append(f.selectAll("filter"))    ; // to debug IE11 crash
                           // tmp = f.selectAll("#new-shape-list");
                           // console.log("load rms.svg");
                           scope.RMSTable.svgScene.append(f.selectAll("g#floorGroup")); // grid pattern
                           scope.RMSTable.svgScene.select("defs").append(f.selectAll("pattern"));
                           scope.RMSTable.renderBasic();
                           scope.RMSTable.renderLayout( scope.RMSTable.Scene[scope.RMSTable.currentSceneNo].layout, scope.RMSTable.currentSceneNo );

                           // demo load
                           // angular.element(scope.RMSTable.makeOrder(scope.RMSTable.svgScene.select("#table_1418785047326")));
                           scope.message = uiLanguage[INFO.lang]['sceneLoaded'];
                           // scope.showCustomToast();

                           // for convenience debugging and testing
                           MainService.tableList = [];
                           Snap("#svg-canvas").selectAll("#tableGroup_0 .transform text").forEach(function(element, index, array){
                               // console.log(element.attr('text'))
                               MainService.tableList.push(
                                   {
                                       tableNo: element.attr('text')
                                   });
                           });
                           window.tableListNeedle = MainService.tableList;
                       }); // snap.load

                   } else {
                       // scope.message = "Table Scene is already initiated once. Table is re-rendered.";
                       // // scope.showCustomToast();

                       console.log("render table only");
                       // render table only
                       scope.RMSTable.svgScene.selectAll(".transform").forEach(function(element, index, array) {
                           if (element.freeTransform) {
                               element.freeTransform.unplug();
                           }
                       })

                       // old method
                       console.log('removeAllHandles');
                       removeAllHandles();
                       scope.cancelTransfer('reset');

                       // remove all and re-render
                       scope.RMSTable.svgScene.selectAll( ".tableGroup" ).remove();
                       scope.RMSTable.renderTable( scope.RMSTable.currentSceneNo );
                       scope.RMSTable.changeZone( scope.RMSTable.currentSceneNo ); // reset to current scene
                   }

                   if( angular.isUndefined(MainService.RMSTable )) {
                       // console.log('new');
                       MainService.RMSTable = scope.RMSTable;
                   } else {
                       // console.log('already exist');
                       console.debug(scope.RMSTable);
                       MainService.RMSTable = {}
                       //MainService.RMSTable = $.extend(true, MainService.RMSTable, scope.RMSTable );
                       $.extend(true, MainService.RMSTable, scope.RMSTable);

                   }

                   MainService.RMSTable.removeAllHandles = removeAllHandles;
                   MainService.RMSTable.isTableFree = isTableFree;
                   MainService.RMSTable.editMode = "order";
                   MainService.RMSTable.changeMode = function( mode ) {
                       this.editMode = lowerFirstLetter( mode );
                       console.log(lowerFirstLetter( mode ));
                       console.log(this.editMode);
                       showMessage('#status-message', uiLanguage[INFO.lang][lowerFirstLetter( mode )] + ' ' + uiLanguage[INFO.lang]['mode'], '', 'warn', 'stick');
                   }
                   console.log(MainService.RMSTable);
                }

                function initTableScene( data ) {
                    console.log('initTableScene');
                    // if( typeof scope.tableType === 'object') {
                        console.log("loading table");

                        // if data is not supplied load data, or load table scene directly
                        if( typeof data === 'undefined' ) {
                            SocketFactory.emit("loadTableSchema", '{}', function (r) {
                                console.log('loadTableSchema by default / initTableScene');
                                 // console.log(r);
                                loadTableScene( r );

                                // var finish = new Date();
                                // var difference = new Date();
                                // difference.setTime(finish.getTime() - start.getTime());
                                // console.log(start.getMilliseconds());
                                // console.log(finish.getMilliseconds());
                                // alert( difference.getMilliseconds() );
                            }); // socketFactory
                        } else {
                            console.log('refresh using data supplied by refreshTable / initTableScene');
                            loadTableScene( data );
                        }

                        // not working
                        // SocketFactory.on('connect_error', function() {
                        //     // console.log(r);
                        //     // console.log('connection error');
                        //     scope.message = uiLanguage[INFO.lang]['sceneLoadError'];
                        //     // $http.get('data/scene_objects.json').success(function (data) {
                        //     //     // console.log(data);
                        //     //    loadTableScene( data );
                        //     //    $("body > svg").remove(); // avoid accumulated garbage if server cannot be connected
                        //     //     // scope.showCustomToast( "123" );
                        //     // });
                        //     showMessage("#status-message", scope.message, "", "warn", 30000);
                        // });
                    // } else {
                    //     scope.message = uiLanguage[INFO.lang]['jsonLoadError'];
                    // }
                } // initTableScene

                // load table
                if( angular.isUndefined( scope.RMSTable.tableType ) ) {
                    $http.get('data/table_type.json').success(function (data) {
                        MainService.RMSTable.tableType = scope.tableType = data.tableType;
                        // console.log(scope);
                        // scope.RMSTable.renderTableType();

                        // initTableScene();

                        // debug initTableScene slowness
                        SocketFactory.emit("loadTableSchema", '{}', function (r) {
                            console.log('loadTableSchema');
                            // console.log(r);
                            loadTableScene( r );
                        }); // socketFactory
                        
                        // scope.closeToast();
                        // scope.showCustomToast(3000);
                    });
                } else {
                    scope.tableType = scope.RMSTable.tableType;
                    SocketFactory.emit("loadTableSchema", '{}', function (r) {
                        // console.log(r);
                        loadTableScene( r );
                    }); // socketFactory
                }

                function limitRotate( explicitRotate ) {
                    console.log('limitRotate');
                    var rotate = rotate ? rotate : explicitRotate;
                    console.log(rotate);
                    if ( userConfig.table.edit.constraint.rotate ) {
                        console.log('limit range rotate / bounding box');
                        var deg = ( 360 + rotate ) % 360;

                        if ( deg > 180 ) { deg -= 360; }

                        if ( deg < userConfig.table.edit.constraint.rotate[0] ) { rotate += userConfig.table.edit.constraint.rotate[0] - deg; }
                        if ( deg > userConfig.table.edit.constraint.rotate[1] ) { rotate += userConfig.table.edit.constraint.rotate[1] - deg; }
                    }

                    return rotate;
                }

                function limitScale( explicitScale, explicitBB ) {
                    var scale = scale ? scale : explicitScale;
                    var bbtElementBBox = bbtElementBBox ? bbtElementBBox : explicitBB;
                    console.log(scale);
                    if ( userConfig.table.edit.constraint.scale ) {
                        // console.log('limit scale / bounding box');
                        if ( scale.x * bbtElementBBox.width < userConfig.table.edit.constraint.scale[0] ) {
                            // console.log('1');
                            scale.x = userConfig.table.edit.constraint.scale[0] / bbtElementBBox.width;
                        }

                        if ( scale.y * bbtElementBBox.height < userConfig.table.edit.constraint.scale[0] ) {
                            // console.log('2');
                            scale.y = userConfig.table.edit.constraint.scale[0] / bbtElementBBox.height;
                        }

                        if ( scale.x * bbtElementBBox.width > userConfig.table.edit.constraint.scale[1] ) {
                            // console.log('3');
                            // console.log(scale.x);
                            // console.log(bbtElementBBox.width);
                            // console.log(userConfig.table.edit.constraint.scale[1]);
                            scale.x = userConfig.table.edit.constraint.scale[1] / bbtElementBBox.width;
                        }

                        if ( scale.y * bbtElementBBox.height > userConfig.table.edit.constraint.scale[1] ) {
                            // console.log('4');
                            scale.y = userConfig.table.edit.constraint.scale[1] / bbtElementBBox.height;
                        }
                    }
                    return scale;
                }

                function limitMove( explicitTranslate, bbtElementBBox, element ) {
                    console.log('limitMove');
                    // console.log(explicitTranslate);
                    // explicitTranslate = {x: ??, y: ??}
                    // if ( ft.opts.boundary ) {
                        console.log('apply boundary');
                        // var b = ft.opts.boundary;
                        var paper = element.paper;
                        var vpBoundaryBBox = Snap('.viewport-helper').getBBox();

                        b = { x: paper._left || 0, y: paper._top || 0, width: vpBoundaryBBox.width, height: vpBoundaryBBox.height }; // replace ft.opts.boundary
                        // bbox = element.getBBox(true);
                        bbox = bbtElementBBox;

                        /**
                         * Get dimension of the paper
                         */
                        function getPaperSize() {
                            return {
                                x: parseInt(paper.node.clientWidth) || parseInt(paper.node.getBBox().width),
                                y: parseInt(paper.node.clientHeight) || parseInt(paper.node.getBBox().height),
                                w: parseInt(paper.node.clientWidth) || parseInt(paper.node.getBBox().width),
                                h: parseInt(paper.node.clientHeight) || parseInt(paper.node.getBBox().height)
                            }
                        }

                        ///////////////////////////////////////////////
                        // _viewBox, boundary width, height, viewBoxRatio, getPaperSize 等未substitute

                        paper._viewBox = paper.attr("viewBox").vb.split(" ");
                        var viewBoxRatio = {
                            x: paper._viewBox[2] / getPaperSize().w,
                            y: paper._viewBox[3] / getPaperSize().h
                        };

                        // console.log(paper._viewBox);
                        // console.log(viewBoxRatio);
                        // console.log(getPaperSize);
                        // console.log(b);

                        // console.log(b);
                        // console.log(!!paper._viewBox);
                        // console.log(b.width);
                        // console.log(b.height);

                        if( paper._viewBox && ( b.width === null && b.height === null )) {
                            console.log('with viewbox ratio');
                            // console.log(ft.attrs.viewBoxRatio.x);
                            b.width = getPaperSize().x * viewBoxRatio.x;
                            b.height = getPaperSize().y * viewBoxRatio.y;
                        } 
                        else {
                            console.log('without viewbox ratio');
                            b.width  = ( b.width  || getPaperSize().x );
                            b.height = ( b.height || getPaperSize().y );
                        }
                        // console.log(b);
                        ///////////////////////////////////////////////

                        // left edge
                        if ( bbox.cx + explicitTranslate.x < b.x            ) { 
                            explicitTranslate.x += b.x - ( bbox.cx + explicitTranslate.x ); 
                        }

                        // top edge
                        if ( bbox.cy + explicitTranslate.y < b.y            ) { 
                            explicitTranslate.y += b.y -            ( bbox.cy + explicitTranslate.y ); }

                        // right edge
                        // console.log('explicitTranslate.x ', explicitTranslate.x);
                        // console.log('bbox.cx + explicitTranslate.x ', bbox.cx + explicitTranslate.x);
                        // console.log('b.x + b.width ', b.x + b.width);
                        // console.log('b.x ', b.x);
                        // console.log('b.width ', b.width);
                        if ( bbox.cx + explicitTranslate.x > b.x + b.width  ) { 
                            console.log("b.width " + b.width);
                            explicitTranslate.x += b.x + b.width  - ( explicitTranslate.x + bbox.cx ); 
                            // explicitTranslate.x += b.x + b.width  - ( bbox.cx + explicitTranslate.x ); 
                        }

                        // bottom edge
                        if ( bbox.cy + explicitTranslate.y > b.y + b.height ) { 
                            explicitTranslate.y += b.y + b.height - ( bbox.cy + explicitTranslate.y ); 
                        }
                    // }

                    // console.log(explicitTranslate);
                    return explicitTranslate;
                }

                scope.move = function( direction ) {
                    var ele = scope.RMSTable.currentActiveElementSnap.select('.transform');
                    // var loadedMatrixtest = ele.attr("transform").totalMatrix.split();
                    var loadedMatrix = Snap.parseTransformString(ele.transform());

                    var dx = dy = dR = dS = 0;
                    // console.log(direction);
                    switch( direction ) {
                        case 'up':
                            var dy = -userConfig.table.edit.snap.translate;
                            break;
                        case 'right':
                            var dx = userConfig.table.edit.snap.translate;
                            break;
                        case 'down':
                            var dy = userConfig.table.edit.snap.translate;
                            break;
                        case 'left':
                            var dx = -userConfig.table.edit.snap.translate;
                            break;
                        default:
                            return;
                            break;
                    }

                    if( MainService.RMSTable.groupEdit ) {
                        hideTableHandles();

                        var selectedElements = MainService.RMSTable.svgScene.selectAll('.selected');

                        // group edit manipulations
                        var orgX, orgY, orgScaleX, orgScaleY;
                        selectedElements.forEach(function(element){
                            element.parent().data('state', 'modified');

                            eleBB = element.getBBox();

                            orgRotate = element.freeTransform.updateHandles().attrs.rotate;
                            orgX = element.freeTransform.updateHandles().attrs.translate.x;
                            orgY = element.freeTransform.updateHandles().attrs.translate.y;
                            orgScaleX = element.freeTransform.updateHandles().attrs.scale.x;
                            orgScaleY = element.freeTransform.updateHandles().attrs.scale.y;

                            // console.log(element.freeTransform.updateHandles().attrs.rotate);

                            element.freeTransform.updateHandles().attrs.rotate += dR;

                            element.freeTransform.updateHandles().attrs.scale.x += dS;
                            element.freeTransform.updateHandles().attrs.scale.y += dS;

                            element.freeTransform.updateHandles().attrs.translate.x += dx;
                            element.freeTransform.updateHandles().attrs.translate.y += dy;
                            element.freeTransform.updateHandles().apply('limit');

                            // transform the auxiliary bounding box
                            var transformed = [],
                                bbtMatrix,
                                // verify = [],
                                eleID = element.parent().attr('id'),
                                // allBBT = MainService.RMSTable.svgScene.selectAll('.bbt-handle-box');
                                bbtElement = MainService.RMSTable.svgScene.select('.bbt-handle-box.'+eleID),
                                bbElement = MainService.RMSTable.svgScene.select('.bb-handle-box.'+eleID),
                                bbtTransformStr = bbtElement.transform().toString(),
                                bbtElementBBox = bbtElement.getBBox(1); // without tranform

                                if( bbtTransformStr.indexOf('r') === -1 && bbtTransformStr.indexOf('R') === -1 ) {
                                    // no rotation string...
                                    bbtTransformStr = bbtTransformStr + 'r0,0,0';
                                }

                                bbtMatrix = Snap.parseTransformString(bbtTransformStr);

                                // console.log(eleID);
                                console.log(bbtElementBBox);
                                console.log(eleBB);
                                // console.log(bbtMatrix);
                                // console.log(allBBT);

                                bbtMatrix.forEach(function(transformP) {
                                    if(transformP[0].toUpperCase() == 'R') {
                                        var rotate = transformP[1] + dR;

                                        transformed.push( 'R' + (rotate), bbtElementBBox.cx, bbtElementBBox.cy);
                                    }
                                });

                                bbtMatrix.forEach(function(transformP) {
                                    var scale = {x: transformP[1] + dS, y: transformP[2] + dS};

                                    if(transformP[0].toUpperCase() == 'S') {
                                        // Limit scale
                                        // scale = limitScale( scale, bbtElementBBox );
                                        transformed.push( 'S' + (scale.x), (scale.y), bbtElementBBox.cx, bbtElementBBox.cy);
                                    }
                                });

                                bbtMatrix.forEach(function(transformP) {
                                    if(transformP[0].toUpperCase() == 'T') {
                                        var translate = {
                                            x: transformP[1] + dx,
                                            y: transformP[2] + dy
                                        }
                                        translate = limitMove(translate, bbtElementBBox, element);
                                        transformed.push( 'T' + (translate.x ), ( translate.y ) );
                                    }
                                });

                                console.log(transformed.join(','));
                                bbtElement.transform(transformed.join(','));

                                // reset and create
                                // remove
                                element.data("bb", "");
                                Snap('.bb-handle-box.'+eleID).remove();
                                element.data("bb", element.paper.rect( rectObjFromBB( element.getBBox() ) )
                                .attr({ 
                                    fill: "none", 
                                    stroke: userConfig.table.svg.handleFill, 
                                    strokeDasharray: userConfig.table.svg.handleStrokeDash,
                                    class: 'bb-handle-box '+eleID
                                }) );

                            // for easy tracking
                            var loadedMatrix = Snap.parseTransformString(element.transform());

                            loadedMatrix.forEach(function(transformP) {
                                if(transformP[0] == 'T') {
                                    x = transformP[1];
                                    y = transformP[2];
                                }

                                if(transformP[0] == 'R') {
                                    rotate = transformP[1];
                                    centerx = transformP[2];
                                    centery = transformP[3];
                                }

                                if(transformP[0] == 'S') {
                                    scalex = transformP[1];
                                    scaley = transformP[2];
                                }
                            });
                            element.parent().attr({
                                'ref-posX': x,
                                'ref-posY': y
                            });
                        });

                        return;
                    } else {
                        // single edit
                        ele.parent().data('state', 'modified')
                        ele.freeTransform.updateHandles().attrs.translate.x += dx;
                        ele.freeTransform.updateHandles().attrs.translate.y += dy;
                        ele.freeTransform.updateHandles().apply('limit');

                        // for easy tracking
                        var loadedMatrix = Snap.parseTransformString(ele.transform());

                        loadedMatrix.forEach(function(transformP) {
                            if(transformP[0] == 'T') {
                                x = transformP[1];
                                y = transformP[2];
                            }

                            if(transformP[0] == 'R') {
                                rotate = transformP[1];
                                centerx = transformP[2];
                                centery = transformP[3];
                            }

                            if(transformP[0] == 'S') {
                                scalex = transformP[1];
                                scaley = transformP[2];
                            }
                        });
                        ele.parent().attr({
                            'ref-posX': x,
                            'ref-posY': y
                        });
                    }


                    // eleMatrix = new Snap.Matrix();
                    // var transformed = [];

                    // loadedMatrix.forEach(function(transformP) {
                    //     if(transformP[0] == 'T') {
                    //         // eleMatrix.translate((transformP[1] ),(transformP[2] ));
                    //         transformed.push( 'T' + (transformP[1] + dx), (transformP[2] + dy) );
                    //     }

                    //     if(transformP[0] == 'R') {
                    //         // eleMatrix.rotate(transformP[1], transformP[2], transformP[3]);
                    //         transformed.push( 'R' + transformP[1], transformP[2], transformP[3] );
                    //     }

                    //     if(transformP[0] == 'S') {
                    //         // eleMatrix.scale(transformP[1], transformP[2], transformP[3], transformP[4]);
                    //         transformed.push( 'S' + transformP[1], transformP[2], transformP[3], transformP[4] );
                    //     }
                    // })
                    // // ele.transform(transformed.join(','));
                    // console.log(transformed);
                    
                    // ele.transform(eleMatrix);
                    // console.log(eleMatrix);
                }

                scope.scale = function( size ) {
                    var element = scope.RMSTable.currentActiveElementSnap.select('.transform');
                    var loadedMatrix = Snap.parseTransformString(element.transform());

                    var dx = dy = dS = x = y = dR = 0;
                    switch( size ) {
                        case 'up':
                            var dS = userConfig.table.edit.snap.scale;
                            break;
                        case 'down':
                            var dS = -userConfig.table.edit.snap.scale;
                            break;
                        default:
                            break;
                    }

                    if( MainService.RMSTable.groupEdit ) {
                        hideTableHandles();

                        var selectedElements = MainService.RMSTable.svgScene.selectAll('.selected');

                        // group edit manipulations
                        var orgX, orgY, orgScaleX, orgScaleY;
                        selectedElements.forEach(function(element){
                            element.parent().data('state', 'modified');

                            eleBB = element.getBBox();

                            orgRotate = element.freeTransform.updateHandles().attrs.rotate;
                            orgX = element.freeTransform.updateHandles().attrs.translate.x;
                            orgY = element.freeTransform.updateHandles().attrs.translate.y;
                            orgScaleX = element.freeTransform.updateHandles().attrs.scale.x;
                            orgScaleY = element.freeTransform.updateHandles().attrs.scale.y;

                            // console.log(element.freeTransform.updateHandles().attrs.rotate);

                            element.freeTransform.updateHandles().attrs.rotate += dR;

                            element.freeTransform.updateHandles().attrs.scale.x += dS;
                            element.freeTransform.updateHandles().attrs.scale.y += dS;

                            element.freeTransform.updateHandles().attrs.translate.x += dx;
                            element.freeTransform.updateHandles().attrs.translate.y += dy;
                            element.freeTransform.updateHandles().apply('limit');

                            // transform the auxiliary bounding box
                            var transformed = [],
                                bbtMatrix,
                                // verify = [],
                                eleID = element.parent().attr('id'),
                                // allBBT = MainService.RMSTable.svgScene.selectAll('.bbt-handle-box');
                                bbtElement = MainService.RMSTable.svgScene.select('.bbt-handle-box.'+eleID),
                                bbElement = MainService.RMSTable.svgScene.select('.bb-handle-box.'+eleID),
                                bbtTransformStr = bbtElement.transform().toString(),
                                bbtElementBBox = bbtElement.getBBox(1); // without tranform

                                if( bbtTransformStr.indexOf('r') === -1 && bbtTransformStr.indexOf('R') === -1 ) {
                                    // no rotation string...
                                    bbtTransformStr = bbtTransformStr + 'r0,0,0';
                                }

                                bbtMatrix = Snap.parseTransformString(bbtTransformStr);

                                // console.log(eleID);
                                console.log(bbtElementBBox);
                                console.log(eleBB);
                                // console.log(bbtMatrix);
                                // console.log(allBBT);

                                bbtMatrix.forEach(function(transformP) {
                                    if(transformP[0].toUpperCase() == 'R') {
                                        var rotate = transformP[1] + dR;

                                        transformed.push( 'R' + (rotate), bbtElementBBox.cx, bbtElementBBox.cy);
                                    }
                                });

                                bbtMatrix.forEach(function(transformP) {
                                    var scale = {x: transformP[1] + dS, y: transformP[2] + dS};

                                    if(transformP[0].toUpperCase() == 'S') {
                                        // Limit scale
                                        scale = limitScale( scale, bbtElementBBox );
                                        transformed.push( 'S' + (scale.x), (scale.y), bbtElementBBox.cx, bbtElementBBox.cy);
                                    }
                                });

                                bbtMatrix.forEach(function(transformP) {
                                    if(transformP[0].toUpperCase() == 'T') {
                                        transformed.push( 'T' + (transformP[1] + dx ), (transformP[2] + dy ) );
                                    }
                                });

                                console.log(transformed.join(','));
                                bbtElement.transform(transformed.join(','));

                                // reset and create
                                // remove
                                element.data("bb", "");
                                Snap('.bb-handle-box.'+eleID).remove();
                                element.data("bb", element.paper.rect( rectObjFromBB( element.getBBox() ) )
                                .attr({ 
                                    fill: "none", 
                                    stroke: userConfig.table.svg.handleFill, 
                                    strokeDasharray: userConfig.table.svg.handleStrokeDash,
                                    class: 'bb-handle-box '+eleID
                                }) );

                            // for easy tracking
                            var loadedMatrix = Snap.parseTransformString(element.transform());

                            loadedMatrix.forEach(function(transformP) {
                                if(transformP[0] == 'T') {
                                    x = transformP[1];
                                    y = transformP[2];
                                }

                                if(transformP[0] == 'R') {
                                    rotate = transformP[1];
                                    centerx = transformP[2];
                                    centery = transformP[3];
                                }

                                if(transformP[0] == 'S') {
                                    scalex = transformP[1];
                                    scaley = transformP[2];
                                }
                            });
                            element.parent().attr({
                                'ref-posX': x,
                                'ref-posY': y
                            });
                        });

                        return;
                    } else {
                        // single edit
                        element.parent().data('state', 'modified')
                        var scale = {x: element.freeTransform.updateHandles().attrs.scale.x + dS, y: element.freeTransform.updateHandles().attrs.scale.y + dS};
                        var eleBB = element.getBBox(1);
                        element.freeTransform.updateHandles().attrs.scale.x = scale.x;
                        element.freeTransform.updateHandles().attrs.scale.y = scale.y;
                        element.freeTransform.updateHandles().apply('limit');
                    }
                }

                scope.rotate = function( direction ) {
                    var element = scope.RMSTable.currentActiveElementSnap.select('.transform');
                    var loadedMatrix = Snap.parseTransformString(element.transform());

                    var dx = dy = dS = x = y = dR = 0;
                    switch( direction ) {
                        case 'right':
                            var dR = userConfig.table.edit.snap.rotate;
                            break;
                        case 'left':
                            var dR = -userConfig.table.edit.snap.rotate;
                            break;
                        default:
                            break;
                    }

                    if( MainService.RMSTable.groupEdit ) {
                        hideTableHandles();

                        var selectedElements = MainService.RMSTable.svgScene.selectAll('.selected');

                        // group edit manipulations
                        

                        var orgX, orgY, orgScaleX, orgScaleY;
                        selectedElements.forEach(function(element){
                            element.parent().data('state', 'modified');

                            eleBB = element.getBBox();

                            orgRotate = element.freeTransform.updateHandles().attrs.rotate;
                            orgX = element.freeTransform.updateHandles().attrs.translate.x;
                            orgY = element.freeTransform.updateHandles().attrs.translate.y;
                            orgScaleX = element.freeTransform.updateHandles().attrs.scale.x;
                            orgScaleY = element.freeTransform.updateHandles().attrs.scale.y;

                            // console.log(element.freeTransform.updateHandles().attrs.rotate);

                            element.freeTransform.updateHandles().attrs.rotate += dR;

                            element.freeTransform.updateHandles().attrs.scale.x += dS;
                            element.freeTransform.updateHandles().attrs.scale.y += dS;

                            element.freeTransform.updateHandles().attrs.translate.x += dx;
                            element.freeTransform.updateHandles().attrs.translate.y += dy;
                            element.freeTransform.updateHandles().apply('limit');

                            // transform the auxiliary bounding box
                            var transformed = [],
                                bbtMatrix,
                                // verify = [],
                                eleID = element.parent().attr('id'),
                                // allBBT = MainService.RMSTable.svgScene.selectAll('.bbt-handle-box');
                                bbtElement = MainService.RMSTable.svgScene.select('.bbt-handle-box.'+eleID),
                                bbElement = MainService.RMSTable.svgScene.select('.bb-handle-box.'+eleID),
                                bbtTransformStr = bbtElement.transform().toString(),
                                bbtElementBBox = bbtElement.getBBox(1); // without tranform

                                if( bbtTransformStr.indexOf('r') === -1 && bbtTransformStr.indexOf('R') === -1 ) {
                                    // no rotation string...
                                    bbtTransformStr = bbtTransformStr + 'r0,0,0';
                                }

                                bbtMatrix = Snap.parseTransformString(bbtTransformStr);

                                // console.log(eleID);
                                console.log(bbtElementBBox);
                                console.log(eleBB);
                                // console.log(bbtMatrix);
                                // console.log(allBBT);

                                bbtMatrix.forEach(function(transformP) {
                                    if(transformP[0].toUpperCase() == 'R') {
                                        var rotate = transformP[1] + dR;

                                        // limit rotate
                                        rotate = limitRotate(rotate);
                                        console.log(rotate);

                                        transformed.push( 'R' + (rotate), bbtElementBBox.cx, bbtElementBBox.cy);
                                    }
                                });

                                bbtMatrix.forEach(function(transformP) {
                                    var scale = {x: transformP[1] + dS, y: transformP[2] + dS};

                                    if(transformP[0].toUpperCase() == 'S') {
                                        // Limit scale
                                        limitScale( scale, bbtElementBBox );
                                        transformed.push( 'S' + (scale.x), (scale.y), bbtElementBBox.cx, bbtElementBBox.cy);
                                    }
                                });

                                bbtMatrix.forEach(function(transformP) {
                                    if(transformP[0].toUpperCase() == 'T') {
                                        var translate = {
                                            x: dx,
                                            y: dy
                                        }

                                        transformed.push( 'T' + (transformP[1] + translate.x ), (transformP[2] + translate.y ) );
                                    }
                                });

                                console.log(transformed.join(','));
                                bbtElement.transform(transformed.join(','));

                                // reset and create
                                // remove
                                element.data("bb", "");
                                Snap('.bb-handle-box.'+eleID).remove();
                                element.data("bb", element.paper.rect( rectObjFromBB( element.getBBox() ) )
                                .attr({ 
                                    fill: "none", 
                                    stroke: userConfig.table.svg.handleFill, 
                                    strokeDasharray: userConfig.table.svg.handleStrokeDash,
                                    class: 'bb-handle-box '+eleID
                                }) );

                            // for easy tracking
                            var loadedMatrix = Snap.parseTransformString(element.transform());

                            loadedMatrix.forEach(function(transformP) {
                                if(transformP[0] == 'T') {
                                    x = transformP[1];
                                    y = transformP[2];
                                }

                                if(transformP[0] == 'R') {
                                    rotate = transformP[1];
                                    centerx = transformP[2];
                                    centery = transformP[3];
                                }

                                if(transformP[0] == 'S') {
                                    scalex = transformP[1];
                                    scaley = transformP[2];
                                }
                            });
                            element.parent().attr({
                                'ref-posX': x,
                                'ref-posY': y
                            });
                        });

                        return;
                    } else {
                        // single edit
                        element.parent().data('state', 'modified');
                        element.freeTransform.updateHandles().attrs.rotate += dR;
                        element.freeTransform.updateHandles().apply('limit');
                    }
                }

                scope.saveTable = function() {
                    var allTable = [];
                    var x = y = scalex = scaley = rotate = centerx = centery = 0,
                        transformP = [];
                    var loadedMatrix = '';
                    // var ele = scope.RMSTable.currentActiveElementSnap;
                    // var allModified = Snap.parseTransformString(ele.select('.modified').transform());
                    var allModified = Snap('#svg-canvas').selectAll('.modified');

                    if( allModified.length > 0 ) {
                        allModified.forEach(function(ele){
                            // console.log(ele);
                            ele = ele.parent();
                            x = y = scalex = scaley = rotate = centerx = centery = 0,
                                transformP = [];

                            loadedMatrix = Snap.parseTransformString(ele.select('.transform').transform());

                            loadedMatrix.forEach(function(transformP) {
                                if(transformP[0] == 'T') {
                                    x = transformP[1];
                                    y = transformP[2];
                                }

                                if(transformP[0] == 'R') {
                                    rotate = transformP[1];
                                    centerx = transformP[2];
                                    centery = transformP[3];
                                }

                                if(transformP[0] == 'S') {
                                    scalex = transformP[1];
                                    scaley = transformP[2];
                                }
                            });

                            // console.log(ele.data('state'));
                            var tableData = {
                                code: ele.data('uid'), 
                                type: ele.data('type'),
                                x: x, 
                                y: y, 
                                scalex: scalex, 
                                scaley: scaley, 
                                // zone: ele.data('zone'), 
                                zone: ele.data('zone'), 
                                label: ele.select('text').attr('text'), 
                                shape: ele.data('shape'), 
                                size: ele.data('size'),
                                angle: rotate,
                                state: ele.data('state') || 'modified',
                                status: ele.data('status'),
                                new: ele.data('new') ? ele.data('new') : false
                            };

                            // console.log(tableData);
                            allTable.push(tableData);
                            // ele.data('modified', false);
                            ele.data('state', false)
                        })
                    }
                    
                    console.log('save all table');
                    console.log('----------------------------');
                    console.log(allTable);
                    
                    SocketFactory.emit('saveLayoutTable', allTable, function(r){
                        r = JSON.parse(r);
                        console.log(r);
                        if( r.result.toLowerCase() === 'ok' ) {
                            // refresh table
                            
                        }
                    });
                }

                scope.delete = function( elements ) {
                    if( MainService.RMSTable.groupEdit && elements != null) {
                        elements.forEach(function(element){
                            if( element.hasClass('new') ) {
                                element.select('.transform').freeTransform.unplug();
                                element.remove();
                            }

                            element.data('state', 'delete');
                            element.select('.shape path, .shape circle').attr('style', 'fill: #9600FF');
                        });
                        return;
                    }
                    // console.log(MainService.RMSTable.currentActiveElementSnap.hasClass('new'));
                    if( MainService.RMSTable.currentActiveElementSnap.hasClass('new') ) {
                        MainService.RMSTable.currentActiveElementSnap.select('.transform').freeTransform.unplug();
                        MainService.RMSTable.currentActiveElementSnap.remove();
                        return;
                    }

                    MainService.RMSTable.currentActiveElementSnap.data('state', 'delete');
                    MainService.RMSTable.currentActiveElementSnap.select('.shape path, .shape circle').attr('style', 'fill: #9600FF');
                }

                // scope.readAsText(file|blob) = function(elements) {
                //     if( MainService.RMSTable.groupEdit && elements != null) {
                //         elements.forEach(function(element){
                //             element.data('state', false);
                //             element.select('.shape path, .shape circle').attr('style', '');
                //         });
                //         return;
                //     } else {
                //         MainService.RMSTable.currentActiveElementSnap.data('state', false);
                //         MainService.RMSTable.currentActiveElementSnap.select('.shape path, .shape circle').attr('style', '');
                //     }
                // }

                scope.restore = function(elements) {
                    if( MainService.RMSTable.groupEdit && elements != null) {
                        elements.forEach(function(element){
                            element.data('state', false);
                            element.select('.shape path, .shape circle').attr('style', '');
                        });
                        return;
                    } else {
                        MainService.RMSTable.currentActiveElementSnap.data('state', false);
                        MainService.RMSTable.currentActiveElementSnap.select('.shape path, .shape circle').attr('style', '');
                    }
                }

                $rootElement.on('keypress keydown', function(e){
                    // console.log(e.which);
                    // prevent key from responsing to non-edit mode
                    if( MainService.isNotModeNavMenu('editTable') ) {
                        return;
                    }

                    if( !MainService.RMSTable.currentActiveElementSnap || MainService.RMSTable.currentActiveElementSnap.select('.modified').length === 0 ) 
                        return;

                    if( MainService.RMSTable.groupEdit ) {
                        hideTableHandles();

                        var selectedElements = MainService.RMSTable.svgScene.selectAll('.selected');
                        // console.log(selectedElements);

                        // function markModified() {
                        //     selectedElements.forEach(function(element){
                        //         element.parent().data('state', 'modified');
                        //     });
                        // }

                        console.log(e.which);

                        // group edit manipulations
                        var dx = dy = dS = x = y = dR = 0;
                        switch( e.which ) {
                            case keyMap.del:
                                console.log('mark delete / group edit');
                                scope.delete(selectedElements);
                                return;
                                break;
                            case keyMap.ESC:
                                console.log('remove delete mark / group edit');
                                scope.restore(selectedElements);

                                selectedElements.forEach(function(element){
                                    removeSelectionBB(element);
                                });
                                return;
                                break;
                            case keyMap.plus:
                                var dS = userConfig.table.edit.snap.scale;
                                break;
                            case keyMap.minus:
                                var dS = -userConfig.table.edit.snap.scale;
                                break;
                            case keyMap.up:
                                var dy = -userConfig.table.edit.snap.translate;
                            break;
                            case keyMap.down:
                                var dy = userConfig.table.edit.snap.translate;
                            break;
                            case keyMap.right:
                                var dx = userConfig.table.edit.snap.translate;
                            break;
                            case keyMap.left:
                                var dx = -userConfig.table.edit.snap.translate;
                            break;
                            case keyMap.close_square_bracket:
                                var dR = userConfig.table.edit.snap.rotate;
                            break;
                            case keyMap.open_square_bracket:
                                var dR = -userConfig.table.edit.snap.rotate;
                            break;
                            default:
                                return;
                            break;
                        }
                        console.log(dR);
                        // markModified();

                        var orgX, orgY, orgScaleX, orgScaleY;
                        selectedElements.forEach(function(element){
                            element.parent().data('state', 'modified');

                            eleBB = element.getBBox();

                            orgRotate = element.freeTransform.updateHandles().attrs.rotate;
                            orgX = element.freeTransform.updateHandles().attrs.translate.x;
                            orgY = element.freeTransform.updateHandles().attrs.translate.y;
                            orgScaleX = element.freeTransform.updateHandles().attrs.scale.x;
                            orgScaleY = element.freeTransform.updateHandles().attrs.scale.y;

                            // console.log(element.freeTransform.updateHandles().attrs.rotate);

                            element.freeTransform.updateHandles().attrs.rotate += dR;

                            element.freeTransform.updateHandles().attrs.scale.x += dS;
                            element.freeTransform.updateHandles().attrs.scale.y += dS;

                            element.freeTransform.updateHandles().attrs.translate.x += dx;
                            element.freeTransform.updateHandles().attrs.translate.y += dy;
                            element.freeTransform.updateHandles().apply('limit');

                            // bb-handle-box update
                            // $('.bb-handle-box')

                            // function compare( arr ) {
                            //     if( typeof arr.length === 'undefined' )
                            //         return;

                            //     var result = true;
                            //     for(var i = 0; i<arr.length; i++){
                            //         result = result && arr[i];
                            //     }
                            //     return result;
                            // }

                            var transformed = [],
                                bbtMatrix,
                                // verify = [],
                                eleID = element.parent().attr('id'),
                                // allBBT = MainService.RMSTable.svgScene.selectAll('.bbt-handle-box');
                                bbtElement = MainService.RMSTable.svgScene.select('.bbt-handle-box.'+eleID),
                                bbElement = MainService.RMSTable.svgScene.select('.bb-handle-box.'+eleID),
                                bbtTransformStr = bbtElement.transform().toString(),
                                bbtElementBBox = bbtElement.getBBox(1); // without tranform

                                if( bbtTransformStr.indexOf('r') === -1 && bbtTransformStr.indexOf('R') === -1 ) {
                                    // no rotation string...
                                    bbtTransformStr = bbtTransformStr + 'r0,0,0';
                                }

                                bbtMatrix = Snap.parseTransformString(bbtTransformStr);

                                // console.log(eleID);
                                console.log(bbtElementBBox);
                                console.log(eleBB);
                                // console.log(bbtMatrix);
                                // console.log(allBBT);

                                bbtMatrix.forEach(function(transformP) {
                                    if(transformP[0].toUpperCase() == 'R') {
                                        var rotate = transformP[1] + dR;

                                        // limit rotate
                                        // if ( userConfig.table.edit.constraint.rotate ) {
                                        //     console.log('limit range rotate / bounding box');
                                        //     var deg = ( 360 + rotate ) % 360;

                                        //     if ( deg > 180 ) { deg -= 360; }

                                        //     if ( deg < userConfig.table.edit.constraint.rotate[0] ) { rotate += userConfig.table.edit.constraint.rotate[0] - deg; }
                                        //     if ( deg > userConfig.table.edit.constraint.rotate[1] ) { rotate += userConfig.table.edit.constraint.rotate[1] - deg; }
                                        // }
                                        // rotate = limitRotate(rotate);
                                        console.log(rotate);

                                        transformed.push( 'R' + (rotate), bbtElementBBox.cx, bbtElementBBox.cy);
                                    }
                                });

                                bbtMatrix.forEach(function(transformP) {
                                    var scale = {x: transformP[1] + dS, y: transformP[2] + dS};

                                    if(transformP[0].toUpperCase() == 'S') {
                                        // Limit scale
                                        // if ( userConfig.table.edit.constraint.scale ) {
                                        //     // console.log('limit scale / bounding box');
                                        //     if ( scale.x * bbtElementBBox.width < userConfig.table.edit.constraint.scale[0] ) {
                                        //         // console.log('1');
                                        //         scale.x = userConfig.table.edit.constraint.scale[0] / bbtElementBBox.width;
                                        //     }

                                        //     if ( scale.y * bbtElementBBox.height < userConfig.table.edit.constraint.scale[0] ) {
                                        //         // console.log('2');
                                        //         scale.y = userConfig.table.edit.constraint.scale[0] / bbtElementBBox.height;
                                        //     }

                                        //     if ( scale.x * bbtElementBBox.width > userConfig.table.edit.constraint.scale[1] ) {
                                        //         // console.log('3');
                                        //         // console.log(scale.x);
                                        //         // console.log(bbtElementBBox.width);
                                        //         // console.log(userConfig.table.edit.constraint.scale[1]);
                                        //         scale.x = userConfig.table.edit.constraint.scale[1] / bbtElementBBox.width;
                                        //     }

                                        //     if ( scale.y * bbtElementBBox.height > userConfig.table.edit.constraint.scale[1] ) {
                                        //         // console.log('4');
                                        //         scale.y = userConfig.table.edit.constraint.scale[1] / bbtElementBBox.height;
                                        //     }
                                        // }
                                        limitScale( scale, bbtElementBBox );
                                        transformed.push( 'S' + (scale.x), (scale.y), bbtElementBBox.cx, bbtElementBBox.cy);
                                    }
                                });

                                bbtMatrix.forEach(function(transformP) {
                                    if(transformP[0].toUpperCase() == 'T') {
                                        var translate = {
                                            x: transformP[1] + dx,
                                            y: transformP[2] + dy
                                        }
                                        translate = limitMove(translate, bbtElementBBox, element);
                                        transformed.push( 'T' + (translate.x ), (translate.y ) );
                                    }
                                });

                                console.log(transformed.join(','));
                                bbtElement.transform(transformed.join(','));

                                // reset and create
                                // remove
                                element.data("bb", "");
                                Snap('.bb-handle-box.'+eleID).remove();
                                element.data("bb", element.paper.rect( rectObjFromBB( element.getBBox() ) )
                                .attr({ 
                                    fill: "none", 
                                    stroke: userConfig.table.svg.handleFill, 
                                    strokeDasharray: userConfig.table.svg.handleStrokeDash,
                                    class: 'bb-handle-box '+eleID
                                }) );

                                // bbElement.attr({
                                //     x: eleBB.x + dx,
                                //     y: eleBB.y + dy
                                // })

                                // bbtElement.transform([
                                //     'R' + loadedData.angle_A, 0, 0,
                                //     'S' + loadedData.scale_X, loadedData.scale_Y, 0, 0,
                                //     'T' + eleBB.x, eleBB.y

                                //     // with first time offset
                                //     // 'T' + (loadedData.pos_X + eleBBox.cx * (userConfig.table.display.globalScale - 1)), (loadedData.pos_Y + eleBBox.cy * (userConfig.table.display.globalScale - 1))
                                // ].join(','));
                                
                                // allBBT.forEach(function(bbtElement){
                                //     // match current element's bbtElement

                                //     bbtMatrix = Snap.parseTransformString(bbtElement.transform().toString());
                                //     console.log(bbtElement);
                                //     console.log(bbtMatrix);

                                //     bbtMatrix.forEach(function(transformP) {
                                //         console.log(transformP);
                                //         if(transformP[0].toUpperCase() == 'T') {
                                //             if(transformP.indexOf(eleBB.x) > -1 && transformP.indexOf(eleBB.y) > -1 ) {
                                //                 verify.push(true);
                                //             } else {
                                //                 verify.push(false);
                                //             }
                                //         }
                                //     });

                                //     if( compare( verify ) ) {
                                //         console.log('matched / found');
                                        
                                //         bbtMatrix.forEach(function(transformP) {
                                //             if(transformP[0].toUpperCase() == 'R') {
                                //                 transformed.push( 'R' + transformP[1], transformP[2], transformP[3] );
                                //             }
                                //         });

                                //         bbtMatrix.forEach(function(transformP) {
                                //             if(transformP[0].toUpperCase() == 'S') {
                                //                 transformed.push( 'S' + (transformP[1] + scale), (transformP[2]), transformP[3], (transformP[4] + scale));
                                //             }
                                //         });

                                //         bbtMatrix.forEach(function(transformP) {
                                //             if(transformP[0].toUpperCase() == 'T') {
                                //                 transformed.push( 'T' + (transformP[1] + dx), (transformP[2] + dy) );
                                //             }
                                //         });
                                //         console.log(transformed.join(','));
                                //         bbtElement.transform(transformed.join(','));
                                //     }
                                // });

                            

                            // for easy tracking
                            var loadedMatrix = Snap.parseTransformString(element.transform());

                            loadedMatrix.forEach(function(transformP) {
                                if(transformP[0] == 'T') {
                                    x = transformP[1];
                                    y = transformP[2];
                                }

                                if(transformP[0] == 'R') {
                                    rotate = transformP[1];
                                    centerx = transformP[2];
                                    centery = transformP[3];
                                }

                                if(transformP[0] == 'S') {
                                    scalex = transformP[1];
                                    scaley = transformP[2];
                                }
                            });
                            element.parent().attr({
                                'ref-posX': x,
                                'ref-posY': y
                            });
                        });


                        return;
                    } else {
                        // single edit
                        var ele = MainService.RMSTable.currentActiveElementSnap.select('.transform'),
                            scale = dx = dy = x = y = rotate = 0;

                        // if( angular.isUndefined(ele))
                        //     return;

                        // console.log(keyMap);
                        // console.log(e.which);

                        switch( e.which ) {
                            case keyMap.del:
                                console.log('mark delete');
                                scope.delete();
                                return;
                                break;
                            case keyMap.ESC:
                                console.log('remove delete mark');
                                scope.restore();
                                return;
                                break;
                            case keyMap.plus:
                                var scale = userConfig.table.edit.snap.scale;
                                break;
                            case keyMap.minus:
                                var scale = -userConfig.table.edit.snap.scale;
                                break;
                            case keyMap.up:
                                var dy = -userConfig.table.edit.snap.translate;
                            break;
                            case keyMap.right:
                                var dx = userConfig.table.edit.snap.translate;
                            break;
                            case keyMap.down:
                                var dy = userConfig.table.edit.snap.translate;
                            break;
                            case keyMap.left:
                                var dx = -userConfig.table.edit.snap.translate;
                            break;
                            case keyMap.close_square_bracket:
                                var rotate = userConfig.table.edit.snap.rotate;
                                break;
                            case keyMap.open_square_bracket:
                                var rotate = -userConfig.table.edit.snap.rotate;
                                break;
                            default:
                                return;
                            break;
                        }

                        // console.log('dy ' + dy);
                        // console.log(ele.data());
                        ele.parent().data('state', 'modified');
                        ele.freeTransform.updateHandles().attrs.rotate += rotate;

                        ele.freeTransform.updateHandles().attrs.scale.x += scale;
                        ele.freeTransform.updateHandles().attrs.scale.y += scale;

                        ele.freeTransform.updateHandles().attrs.translate.x += dx;
                        ele.freeTransform.updateHandles().attrs.translate.y += dy;
                        ele.freeTransform.updateHandles().apply('limit');

                        // for easy tracking
                        var loadedMatrix = Snap.parseTransformString(ele.transform());

                        loadedMatrix.forEach(function(transformP) {
                            if(transformP[0] == 'T') {
                                x = transformP[1];
                                y = transformP[2];
                            }

                            if(transformP[0] == 'R') {
                                rotate = transformP[1];
                                centerx = transformP[2];
                                centery = transformP[3];
                            }

                            if(transformP[0] == 'S') {
                                scalex = transformP[1];
                                scaley = transformP[2];
                            }
                        });
                        ele.parent().attr({
                            'ref-posX': x,
                            'ref-posY': y
                        });

                        // eleMatrix = new Snap.Matrix();
                    }
                });

                // function checkLabel( txt, count ) {
                //     var duplicate = false,
                //         count = 0;
                //     if( count == 0 ) {

                //     }
                //     MainService.RMSTable.svgScene.selectAll('.table text').forEach(function(element){
                //         if( element.attr('text') === txt ) {
                //             duplicate = true;
                //             checkLabel(txt, count)
                //         }
                //     });

                //     return label;
                // }

                $rootElement.on('keyup change', '#current-table-label', function(e){
                    if( MainService.RMSTable.groupEdit ) {
                        hideTableHandles();
                        return;
                    }

                    // console.log($rootElement.find('#current-table-label'));
                    // console.log(MainService.RMSTable.currentActiveElementSnap);
                    if( !MainService.RMSTable.currentActiveElementSnap || MainService.RMSTable.currentActiveElementSnap.select('.modified').length === 0 ) 
                        return;

                    // console.log(MainService.RMSTable.currentActiveElementSnap);
                    // console.log($(this).val());
                    var ele = MainService.RMSTable.currentActiveElementSnap,
                        tableLabel = ele.select('text').attr('text', $(this).val());

                    ele.data('state', 'modified');
                    MainService.RMSTable.centerLabel(ele.select(".hoverArea"), tableLabel);
                });
            }
        }
    });

    app.controller('tableController', function ($scope, MainService, uiLanguage, INFO){
        // the scope inside this controller
        // default table icon settings
        
        // console.log($scope.RMSTable);
        // console.log($scope.svgScene);

        // console.log($element);
        // $http.get('data/table_type.json').success(function (data) {
        //     $scope.tableTypeJSON = data;
        //     // console.log(data);
        // });
        // share with other controllers or change state of the directive such as active/inactive
        $scope.ui = uiLanguage[INFO.lang];

        $scope.RMSTable = {};

        $scope.cancelTransfer = function cancelTransfer( option ) {
            // status message
            if( MainService.modeCheckFloor('changeTable') && $scope.RMSTable.transfer_from != 9999 && option != 'reset' )
                showMessage("#status-message", $scope.ui.transfer_cancel, "", "warn", 30000);

            // console.log(this);
            // console.log($scope);
            this.removeAllTransferBB();
            var svgScene = Snap("#svg-canvas");
            svgScene.selectAll(".transfer").forEach(function (element, index, array) {
                element.removeClass("transfer");
            });
            $('body').removeClass('transfer transfer-from');
            $scope.RMSTable.transfer_to = 9999;
            $scope.RMSTable.transfer_from = 9999;
            $('button.table-model').css('border-width', '0px');
        }

        MainService.cancelTransfer = function () {
        
            $scope.cancelTransfer.apply( $scope, arguments );
        }

        $scope.removeAllTransferBB = function removeAllTransferBB() {
            var svgScene = Snap("#svg-canvas");
            if ((activeTransferItem = svgScene.selectAll(".transfer")).length != 0) {
                // console.log(activeTransferItem);
                activeTransferItem.forEach(this.removeBB);
            }
        }

        $scope.removeBB = function removeBB(element, index, array) {
            // console.log(element);
            element.ftRemoveBB();
        }
        $scope.init = function () {
            MainService.modeTable = userConfig.defaultViewMode;
        }

        $scope.toggleTableView = function () {
            console.debug('table view toggle');
            //switchMode2({mode: \'tableList\'}, true, \'table_list\');
        }
    });

    app.directive('newTableShape', function ( $compile, $http, SocketFactory, $timeout ) {
            return {
                restrict: 'E',
                // <svg id="table-shape" style="visibility: visible; height: 72px; background: red;"></svg>
                template: '<div id="new-table-shape-container" class="" style="height: 250px; width: 205px;"><svg id="table-shape" style="visibility: visible; height: 100%; width:100%; background: white;"></svg></div>',
                link: function( scope, element, attrs ) {

                },
                controller: 'newTableShapeController'
            }
        });

    app.controller('newTableShapeController', function( $scope, $http, $timeout, $interval, $compile, $rootElement, MainService ) {
        var RMSTable = {};
        RMSTable.renderTableType = function() {
            // console.log('RMSTable.renderTableType ');
            this.svgShape = Snap('#table-shape');
            // console.log(RMSTable);

            var masterTableEleGroup = this.svgShape.group().attr({
                'id': 'new-table-group',
                'class': 'table-group'
            });
            // console.log(masterTableEleGroup);

            // var test = masterTableEleGroup.group(Snap.parse("<path d='M0,0h30.625v25.875H0V0z'/>").select('path')).attr('id', 'test1234');

            // var renderTimeout = [];
            var fillArr = [];
            fillArr[0] = 'red';
            fillArr[1] = 'red';
            fillArr[2] = 'red';
            for( i=0; i<MainService.RMSTable.tableType.length; i++ ) {
                var tmp = 0;

                /*
                    reference: https://developer.mozilla.org/en/docs/Web/API/SVGElement
                    'outer most element has no ownerSVGElement property'
                    before adding to any paper, 'g' is outer most element, thus, no ownerSVGElement property
                    so, in Snap.svg, it use "owner" to check if it need to create paper since 
                    it doesnt know if its parent is SVG or not, if ever needed paper, just add back manually is fine in known cases
                */
                var f = Snap.parse('<g class="shape">' + MainService.RMSTable.tableType[i].sizeList[0].svgCode + '</g>').select('g'),
                    // tableNode = f,
                    shape = MainService.RMSTable.tableType[i].type.toLowerCase(),
                    ID = 'shape-' + shape,
                    type = 'table',
                    size = 0;

                f.data({
                    type: type,
                    shape: shape, 
                    size: 0,
                });

                // console.log(f.paper); // itself is group because selected already but no paper defined
                // console.log(f.select('path').paper); // paper is not yet defined
                var gTransform = masterTableEleGroup.group(f).attr({
                    'class': 'transform',
                    'id': 'transform'+i
                });
                // console.log(f.select('path').paper); // paper is defined after group, follow #table-shape
                // console.log(gTransform); // paper is defined, follow #table-shape

                var shapeContainer = masterTableEleGroup.group(gTransform).attr({
                    'id': ID,
                    'ng-click': 'addTable($event)'
                });
                // console.log(shapeContainer) // paper is defined, follow #table-shape

                shapeContainer.data('type', MainService.RMSTable.tableType[i].type.toLowerCase()+'-0');
                // console.log(MainService.RMSTable.tableType[i].type.toLowerCase()+'-0');

                // shapeContainer.select('path').attr({
                //     fill: fillArr[i],
                // });

                shapeContainer.selectAll('path, circle').forEach(function(element){
                    element.attr({
                        fill: fillArr[i],
                    });
                })

                // console.log(shapeContainer);

                // var eleBBox = gTransform.select('rect').getBBox();
                $compile( $('#'+ID) )( $scope );

                // console.log(gTransform.select('.shape').data());
            } // for

            var renderTimeout = $interval(function(){
                eleBBox = masterTableEleGroup.getBBox();

                // console.log(eleBBox);
                // console.log(tmp); // timer
                // if( tmp === 10000 ) {
                //     console.log('timeout');
                //     $interval.cancel( renderTimeout );
                // }

                if( eleBBox.width != 0 || tmp === 100000  ) {
                    console.log('SVG load completed');
                    $interval.cancel( renderTimeout );

                    var offsetX = 10, offsetY = 10;
                    for( i=0; i<MainService.RMSTable.tableType.length; i++ ) {
                        var shape = MainService.RMSTable.tableType[i].type.toLowerCase(),
                            ID = '#shape-' + shape,
                            ele = masterTableEleGroup.select(ID);

                        // var type = ele.data('type');
                        // console.log(type);

                        eleBBox = ele.getBBox();
                        // console.log(ID);
                        // console.log(ele);
                        // console.log(offsetX);
                        // console.log(eleBBox);
                        var scale = 1.8;
                        var tableInit = [
                            'R' + 0, 0, 0,
                            'S' + scale, scale, 0, 0,
                            'T' + offsetX, offsetY
                        ].join(',');
                        // console.log('after');
                        // console.log(eleBBox);
                        // console.log(tableInit);
                        // offsetX += eleBBox.width * scale + 10;
                        // offsetX += 10;
                        offsetY += eleBBox.height * scale + 10;
                        ele.select('.transform').transform(tableInit);
                    }

                    $('new-table-shape').appendTo($('#add-table-container'));
                    $('#new-table-shape-container').addClass('ng-hide');
                    $('new-table-shape').css({opacity: 0.9});
                } else {
                    tmp += 10;
                }
            }, tmp)
            

            // console.log(Snap('#table-shape').getBBox());
        }

        if( angular.isUndefined( MainService.RMSTable ) ) {
            // console.log('new');
            // console.log('new');
            MainService.RMSTable = RMSTable;
        } else {
            // console.log('copy/extend');
            MainService.RMSTable = $.extend( true, RMSTable, MainService.RMSTable );
        }

        if( angular.isUndefined( $scope.RMSTable.tableType ) ) {
            // console.log('$scope.RMSTable.tableType ');
            $http.get('data/table_type.json').success(function (data) {
                MainService.RMSTable.tableType = $scope.tableType = data.tableType;
                // console.log($scope);
                RMSTable.renderTableType();
                // console.log(MainService.RMSTable.tableType );
            });
        }

        // RMSTable.renderTableType();
        
        $scope.addTable = function( ev ) {
            console.log(ev.currentTarget.id);
            var ele = Snap('#' + ev.currentTarget.id),
                gTransform = ele.select('.transform').clone(), // after clone, shape have no data
                currentZoneNo = MainService.RMSTable.currentSceneNo || 0,
                uid = createTimeStamp(),
                newID = 'table_' + uid,
                className = [ele.select('.shape').data("shape") + '-'+ele.select('.shape').data("size"), 'table', 'new'];
            // alert(currentZoneNo);

            console.log(ele);
            console.log(ele.getBBox());

            // console.log(ele.select('.shape').data());
            // console.log(currentZoneNo);
            // console.log(newID);
            // console.log(MainService.RMSTable);
            
            // create new table
            var newTable = Snap('#layoutScene_'+currentZoneNo+' #table-group').group(gTransform.attr({
            })).attr({
                'id': newID,
                'class': className.join(' '),
                'ng-click': 'tableMouseDown( "'+newID+'" )'
            });
            MainService.RMSTable.currentActiveElementSnap = newTable;
            // console.log($('#'+newID));

            console.log(Snap('#'+newID));
            console.log(Snap('#'+newID).getBBox());

            // change Paper
            newTable.selectAll('g, path, rect').forEach(function(element){
                if( element.paper ) {
                    element.paper = MainService.RMSTable.svgScene;
                }
                // console.log(element.paper)
            });

            // "angularize"
            $compile( $('#'+newID) )( $scope );

            // normalize the transformation, change to zero and rescale to fit table
            gTransform.transform([
                'R' + 0, 0, 0,
                'S' + 1, 1, 0, 0,
                'T' + 0, 0
            ].join(','));
            MainService.RMSTable.addHoverRect2(gTransform); // add hover area after reset

            // tableLabel.getBBox(); // run once before text assign to calibrate, not sure if it is the bug of Snap SVG, after run one, figure seems correct

            /*
                reset to 0, 0
                for initial state, offset it to zero with CX
                when save, just put the resultant Tx, Ty is fine, next time when restore, just restore this value
            */ 
            var eleBBox = gTransform.getBBox(); // eleBBox is affected by scale, define after reset to scale 1:1
            console.log(eleBBox);
            gTransform.transform([
                'R' + 0, eleBBox.cx, eleBBox.cy,
                'S' + userConfig.table.display.globalScale, userConfig.table.display.globalScale, eleBBox.cx, eleBBox.cy,
                'T' + (eleBBox.cx * (userConfig.table.display.globalScale - 1)), (eleBBox.cy * (userConfig.table.display.globalScale - 1))
                // offset is based on:
                // scale: 1, offset 0
                // scale: 2, offset -12.563(CX)
                // scale: 3, offset (CX * 2)
                // scale: 4, offset (CX * 3) ... (n-1)
            ].join(','));

            console.log(gTransform.getBBox());

            // add label
            var newLabel = 'new';
            var tableLabel = gTransform.text(0, 0, '').attr({
                'fill': MainService.RMSTable.defaultFontColor,
                'font-size': MainService.RMSTable.defaultFontSize,
                'font-family': MainService.RMSTable.fontFamily,
                // 'text-rendering': 'geometricPrecision',
                'text': newLabel
            });

            $('#current-table-label').val( newLabel );
            MainService.RMSTable.centerLabel(gTransform.select(".hoverArea"), tableLabel);

            // prepare static data
            newTable.data({
                uid: uid,
                type: ele.select('.shape').data("type"),
                shape: ele.select('.shape').data("shape"), 
                size: ele.select('.shape').data("size"),
                zone: MainService.RMSTable.zoneID, 
                label: newTable.select('text').attr('text'),
                state: 'new',
                status: -1
            });

            // console.log(newTable.data());

            // trigger freeTransform
            // $scope.tableMouseDown( newID ); // at this point, the boundbox is not right yet
            // console.log(gTransform.paper);
            var addValidation = $interval(function(){
                if( gTransform.paper.node.id === 'svg-canvas' ) {
                    // console.log(gTransform.paper);
                    $interval.cancel( addValidation );
                    // $scope.tableMouseDown( newID );
                }
            }, 100 );
        }

        $scope.tableMouseDown = function() {
            // console.log(scope);
            var args = Array.prototype.slice.call(arguments).concat([$scope]);
            // console.log(args);
            tableMouseDown2.apply( $scope, args );

            // $rootElement.find('#current-table-label').on('keyup change', function(e){
            //     // console.log(MainService.RMSTable.currentActiveElementSnap);
            //     // console.log($(this).val());
            //     var ele = MainService.RMSTable.currentActiveElementSnap,
            //         tableLabel = ele.select('text').attr('text', $(this).val());
            //     MainService.RMSTable.centerLabel(ele.select(".hoverArea"), tableLabel);
            // });
        };
    });

    // app.directive('testScene', function (MainService, $compile, $timeout, uiLanguage, INFO, userConfig) {
    //     return {
    //         restrict: "E",
    //         template: '<div id="test-scene"></div>',
    //         link: function (scope, element, attrs) {
    //             scope.ui = uiLanguage[INFO.lang];
    //             scope.lang = INFO.lang;
    //             scope.renderTable = function ( data ) {
    //                 $('#test-scene').html('');

    //                 // table food list
    //                 HandlebarsIntl.registerWith(Handlebars);
    //                 var intlData = {
    //                     "locales": "en-US"
    //                 };
    //                 var contextCategoryList = {};
    //                 contextCategoryList.tableHead = [
    //                   { "name": '123123', "fieldType": "textfield", "widthClass": "display", obj:{ha: 1234} },
    //                   { "name": scope.ui['group_name'], "fieldType": "textfield", "widthClass": "sort-order" }];
    //                 content = Handlebars.compile($("#tpl-test").html())(contextCategoryList);

    //                 content = $compile( content )( scope );
    //                 $('#test-scene').append(content);
    //             }

    //             scope.renderTable();

    //             // console.log(scope);
    //           // var elmnt;
    //           // attrs.$observe( 'template', function ( myTemplate ) {
    //           //   if ( angular.isDefined( myTemplate ) ) {
    //           //     // compile the provided template against the current scope
    //           //     elmnt = $compile( myTemplate )( scope );

    //           //       element.html(""); // dummy "clear"

    //           //     element.append( elmnt );
    //           //   }
    //           // });
    //         },
    //         controller: function( $scope ) {
    //             // console.log($scope);
    //             // $scope.renderTable();
    //         }
    //         // controller: function ($scope, MainService, $timeout, uiLanguage, INFO) {
    //         //     // console.log(MainService.schema.modeOrder.billing);
    //         //     var ui = $scope.ui = uiLanguage[INFO.lang];
    //         //     $scope.staffTypeList = [
    //         //         {code:1, name:ui['manager']},
    //         //         {code:2, name:ui['cashier']},
    //         //         {code:3, name:ui['staff']}
    //         //     ];
    //         //     $timeout(function(){

    //         //         $('select').material_select();
    //         //     }, 100)
    //         // }
    //     }
    // });

    // app.directive('orderListHandlerbar', function( MainService, $compile, $timeout, uiLanguage, INFO, userConfig, $filter ) {
    //     return {
    //         restrict: "E",
    //         template: '<div class="order-list-template-wrapper col order-list"></div>',
    //         link: function (scope, element, attrs) {
    //             // alert()
    //             scope.ui = uiLanguage[INFO.lang];
    //             scope.lang = INFO.lang;
    //             scope.test = function( data ) {
    //                 console.log((data));
    //             }
    //             scope.renderTable = function ( data ) {
    //                 $('.order-list-template-wrapper').html('');

    //                 // table food list
    //                 HandlebarsIntl.registerWith(Handlebars);
    //                 var intlData = {
    //                     "locales": "en-US"
    //                 };

    //                 Handlebars.registerHelper('execFuncThis', function(context, funcName, funcParam1, funcParam2, options) {
    //                   // console.log($filter);

    //                   if( funcParam1 === 'currency' ) {
    //                     return $filter(funcParam1)( context[funcName](), INFO.currency, funcParam2 );
    //                   }
    //                   return context[funcName]();
    //                 });

    //                 /*Handlebars.registerHelper('stringify', function(context, options) {
    //                     function functionReplacer(key, value) {
    //                       if (typeof value === "function") {
    //                     // console.log('function')
    //                     return value.toString()
    //                       }
    //                       return value;
    //                     }

    //                     // console.log('-----------------------------------------');
    //                     // // console.log(context);
    //                     // console.log('-----------------------------------------');
    //                     console.log(JSON.stringify( context, functionReplacer ));
    //                     // console.log('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx');
    //                   return JSON.stringify( context, functionReplacer );
    //                 });*/

    //                 console.log(MainService.Cart.orderList);
    //                 var orderData = MainService.Cart.orderList;
    //                 content = Handlebars.compile($("#tpl-order-list").html())( orderData );

    //                 content = $compile( content )( scope );
    //                 $('.order-list-template-wrapper').append(content);

    //                 console.log(MainService.Cart.orderList);
    //             }

    //             // scope.renderTable();
    //             MainService.renderOrderList = scope.renderTable;
    //         }
    //         ,
    //         controller: function( $scope ) {
    //             $scope.getQtyWithVoid = function( idx ) {
    //                 // console.log(MainService.Cart.orderList[idx]);
    //                 return MainService.Cart.orderList[idx].getQtyWithVoid();
    //             }

    //             $scope.editItem = function( $event, idx ) {
    //                 console.log(MainService.Cart.orderList[idx]);
    //                 // console.log($event);
    //                 return MainService.Cart.orderList[idx].editItem($event);
    //             }

    //             $scope.deleteItem = function( $event, idx ) {
    //                 console.log(MainService.Cart.orderList[idx]);
    //                 return MainService.Cart.orderList[idx].deleteItem($event, idx);
    //             }
    //         }
    //     }
    // });

    
    // 'ngMaterial'
    
    // app.run(function ($templateCache){
    //   // override is working with material directive tag
    //   // $templateCache.put('template/pagination/pager.html', '<md-button class="md-raised md-button md-mini md-button" style="color: black">haha</md-button>');
    //   $templateCache.put('template/pagination/pager.html', '<md-grid-list md-cols-sm="1" md-cols-md="2" md-cols-gt-md="6" md-row-height-gt-md="1:1" md-row-height="2:2" md-gutter="12px" md-gutter-gt-sm="8px" ><md-grid-tile><md-grid-tile-header><h3>This is a header</h3></md-grid-tile-header><md-grid-tile class="green"><md-grid-tile-footer><h3>#2: (1r x 1c)</h3></md-grid-tile-footer></md-grid-tile></md-grid-tile></md-grid-list>');
    // });

    // test
    /*app.directive('mdButtonTest', function () {
        return {
          restrict: 'EA',
          scope: {
            totalItems: '=',
            previousText: '@',
            nextText: '@'
          },
          require: ['pager', '?ngModel'],
          controller: 'PaginationController',
          templateUrl: 'template/pagination/pager.html',
          replace: true,
          link: function(scope, element, attrs, ctrls) {
            var paginationCtrl = ctrls[0], ngModelCtrl = ctrls[1];

            if (!ngModelCtrl) {
               return; // do nothing if no ng-model
            }

            scope.align = angular.isDefined(attrs.align) ? scope.$parent.$eval(attrs.align) : pagerConfig.align;
            paginationCtrl.init(ngModelCtrl, pagerConfig);
          }
        };

        // return {
        //     restrict: 'E',
        //     templateUrl: 'template/pagination/pager.html',
        //     link: function (scope, element, attrs) {
                
        //     },
        //     controller: function( $templateCache ) {
        //         console.log($templateCache.get('template/pagination/pager.html'));
        //     }
        // };
    });*/

    //------------------------------------------- update end

    /*
        page/scene directives
    */
    // app.directive('kitchenInfoScene', function (MainService) {
    //     return {
    //         restrict: "E",
    //         templateUrl: 'tmpl/kitchenInfo.html',
    //         controller: function($scope, MainService) {
    //             $scope.MainService = MainService;
    //         }
    //     }
    // })

    // app.directive('cancelOptionScene', function (MainService) {
    //     return {
    //         restrict: "E",
    //         templateUrl: 'tmpl/cancelOptions.html',
    //         controller: function($scope, MainService,INFO) {
    //             $scope.MainService = MainService;
    //             $scope.INFO = INFO;
    //             $scope.itemsPerPage = 13;
    //             $scope.totalItems = INFO.cancelRemark.length;
    //             $scope.currentPage = 1;
    //         }
    //     }
    // });

    // app.directive('divideBill', function (MainService) {
    //     return {
    //         restrict: "E",
    //         templateUrl: 'tmpl/divideBill.html',
    //         controller: function($scope, MainService) {
    //             $scope.MainService = MainService;
    //         }
    //     }
    // });

    // app.directive('reportScene', function (MainService) {
    //     return {
    //         restrict: "E",
    //         templateUrl: 'tmpl/reportScene.html',
    //         controller: function($scope, MainService) {
    //             $scope.MainService = MainService;
    //         }
    //     }
    // });

    // directives created by Daniel for merge and test here
    // app.controller('tableController', function ($scope, INFO, SocketFactory, $element, $q, $http){
    //     console.log($q.all);
    //  // console.log(MainService.table);

    //     // console.log("here?22222");
    //     // console.log(MainService.food);
    //     // console.log(MainService.totalItems);
    //     $scope.Hide = function () {
    //         $element.addClass('ng-hide');
    //     }
    //     $scope.Show = function () {
    //         $element.removeClass('ng-hide');
    //     }
    //     // $scope.MainService = MainService;
    // })

})();