(function () {
    var app = angular.module('rms.directives', ['shared.directives', 'rms.directives.dummy', 'common.directives', 'rms.templates']);
    // 'ngMaterial'
    app.config(function ($mdThemingProvider, $provide, $httpProvider) {
        $mdThemingProvider.theme('default')
          .primaryPalette("brown")
          .accentPalette('yellow')
          .warnPalette('red');
        // .backgroundPalette('lime')
        $httpProvider.useApplyAsync(true)
        // console.log($httpProvider.useApplyAsync( true ));
    });

    //------------------------------------------- update@2015/6/18
    // added angular-momentum-scroll in RMSAPP
    // added angular-momentum-scroll settings in order3 for left scrollbar

    app.directive('inputKeypadNoofpeople', function () {
        return {
            restrict: 'E',
            controller: 'keypadControllerNoofpeople',
            transclude: true,
            scope: {
                keypadInfo: '@'
            },
            template: '',
            link: function (scope, element, attrs, ctrl, transclude) {
                transclude(scope, function (clone) {
                    for (var k = 0; k < element[0].attributes.length; k++) {
                        clone.attr(element[0].attributes[k].name, element[0].attributes[k].value);
                    }
                    element.append(clone);
                });
            }
        };
    })

    // consolidated to 'reportController'
    /*app.controller("reportCalendarController", function ($scope, $controller, $mdDialog, $timeout, $element, $rootElement, MainService) {
        $controller('calendarCtrl',{$scope: $scope, $timeout: $timeout, $element: $element});

        $scope.setReport = function() {
            console.log('oh my god');
        }

        $scope.keypad = keypad;
        $scope.calendarInfo = {
            reportType: ''
        };

        $scope.closeDialog = function ($mdDialog) {
            // $scope.calendarInfo.reportType = $scope.calendarInfo.lastReportType;
            $mdDialog.cancel();
        };

        $scope.callCalendar = function (dialogCtrl) {
            console.log('call calendar');
            //console.log(event);
            // console.log($scope.keypadInfo.value);
            $scope.calendarInfo.lastReportType = $scope.calendarInfo.reportType;
            //console.log(angular.element('md-dialog[aria-label="keypad"]').scope());
            $scope.calendarInfo.value = '';
            if (angular.element('md-dialog[aria-label="calendar"]').scope() == undefined) {
                $mdDialog.show({
                    controller: dialogCtrl,
                    templateUrl: 'tmpl/calendar.html',
                    // targetEvent: event,
                    escapeToClose: false,
                    focusOnOpen: true,
                    bindToController: true,
                    scope: $scope,
                    preserveScope: true,
                    // for generic purpose, each instance only affect local property keypad value
                    locals: {
                        calendarInfo: $scope.calendarInfo,
                    }
                })

                $rootElement.on('keyup', function(e){
                    // console.log("its's me");
                    if (e.keyCode === 27) {
                      $timeout($mdDialog.hide);
                      $scope.keypadInfo.reportType = $scope.keypadInfo.lastReportType;
                    }
                });
            } else {
                $mdDialog.hide();
            }
        };

        $scope.dialogCtrl = function ($scope, $mdDialog, calendarInfo) {
            $scope.calendarInfo = calendarInfo;
        };
    });*/

    app.controller("keypadControllerNoofpeople", function ($scope, $mdDialog, $element, $attrs, $timeout, MainService, $rootElement, $rootScope, uiLanguage, INFO) {
        $scope.ui = uiLanguage[INFO.lang];
        $scope.keypad = keypad;
        $scope.keypadInfo = {
            value: ''
        };
        $scope.title = $attrs.title ? $attrs.title : 'Keypad';
        $scope.$watch(function (scope) { return MainService.Cart.noOfPeople },
            function () {
                $scope.keypadInfo.value = MainService.Cart.noOfPeople;
            }
        );

        $scope.input = function (val) {
            var maxlength = $attrs.maxlength ? $attrs.maxlength : 3;
            console.log(String($scope.keypadInfo.value).length);

            // reset value if 0, when zero operate as string, it will become 01, 02...
            if (String($scope.keypadInfo.value).length == 1 && $scope.keypadInfo.value == 0)
                $scope.keypadInfo.value = '';

            if (String($scope.keypadInfo.value).length < maxlength)
                $scope.keypadInfo.value += val;
        };

        // $scope.backspace = function () {
        //     console.log('me?');
        //     $scope.keypadInfo.value = $scope.keypadInfo.value.substring(0, $scope.keypadInfo.value.length - 1)
        // };

        $scope.backspace = function () {
            var inputValue = String($scope.keypadInfo.value);
            $scope.keypadInfo.value = inputValue.substring(0, inputValue.length - 1)
        };

        $scope.reset = function (val) {
            console.log($scope.keypadInfo.last);
            if ($scope.keypadInfo.value == '' || $scope.keypadInfo.value == 0) {
                $scope.keypadInfo.value = $scope.keypadInfo.last;
                $mdDialog.cancel();
                MainService.Cart.cancelOrder2();
            } else {
                $scope.keypadInfo.value = '';
            }

            // cancel order if not input any people
            // alert($scope.keypadInfo.value )
            // if( $scope.keypadInfo.value == 0) {

            // }

        };

        $scope.keypressed = function (event) {
            console.log(event);
        };

        $scope.next = function (event) {
            $mdDialog.hide();
            //var oriNum = MainService.Cart.noOfPeople;
            MainService.Cart.noOfPeople = parseInt($scope.keypadInfo.value);
            angular.element($element).blur();

            console.log('MainService.Cart.noOfPeople');
            console.log(MainService.Cart.noOfPeople);

            if (MainService.Cart.noOfPeople == 0) {
                MainService.switchMode2({ mode: 'floorPlan' });
                MainService.lockTable.toReleaseLockTable(MainService.lockTable.tableNum);
                return;
            }

            MainService.mode = MainService.schema.mode.order;
            MainService.modeOrder = MainService.schema.modeOrder["normal"];
            MainService.assign2OrderList([], MainService.Cart.orderList);

            //MainService.mifarePayment.isEnable


            //else if (oriNum == 0 && MainService.Cart.noOfPeople != 0) {
            //    MainService.lockTable.tableNum = MainService.Cart.tableNo;
            //}

        };

        $scope.closeDialog = function ($mdDialog) {
            $scope.keypadInfo.value = $scope.keypadInfo.last;
            $mdDialog.cancel();
        };

        $scope.callKeypad = function (dialogCtrl) {
            console.log('call callKeypad');
            //console.log(event);
            // console.log($scope.keypadInfo.value);
            // .Cart.tableNo
            // console.log('MainService.Cart.noOfPeople');
            // console.log(MainService.Cart.noOfPeople);
            console.log($scope.keypadInfo.value);

            // console.log(MainService.mode);
            // console.log(MainService.modeOrder);
            // console.log(MainService.modeItem);

            if (MainService.modeOrder == MainService.schema.modeOrder.searchBill || MainService.modeOrder == MainService.schema.modeOrder.amendBill) {
                return;
            }

            $scope.keypadInfo.last = MainService.Cart.noOfPeople;
            $scope.keypadInfo.value = '';
            //console.log(angular.element('md-dialog[aria-label="keypad"]').scope());
            // console.log('$scope.keypadInfo.value');
            // console.log($scope.keypadInfo.value);
            if (angular.element('md-dialog[aria-label="keypad"]').scope() == undefined) {
                $mdDialog.show({
                    controller: dialogCtrl,
                    templateUrl: 'tmpl/keypad.html',
                    // targetEvent: event,
                    escapeToClose: false,
                    focusOnOpen: true,
                    bindToController: true,
                    scope: $scope,
                    preserveScope: true,
                    // for generic purpose, each instance only affect local property keypad value
                    locals: {
                        keypadInfo: $scope.keypadInfo,
                    }
                })

                $rootElement.on('keyup', function (e) {
                    // console.log("its's me");
                    console.log($scope.keypadInfo);
                    if (e.keyCode === 27) {
                        $timeout($mdDialog.hide);
                        $scope.keypadInfo.value = $scope.keypadInfo.last;

                        if (MainService.Cart.noOfPeople == 0 || MainService.mode == MainService.schema.mode.order) {
                            MainService.switchMode2({ mode: 'floorPlan' });
                            MainService.lockTable.toReleaseLockTable(MainService.lockTable.tableNum);
                            return;
                        }
                        MainService.switchMode2({ mode: 'floorPlan' });
                    }
                });
            } else {
                $mdDialog.hide();
            }
        };

        $scope.dialogTest1 = function ($scope, $mdDialog, keypadInfo) {
            //console.log('dialogTest1');
            // console.log($scope.title);
            // console.log($scope.keypad);
            // console.log($scope.keypadInfo.value);
            // console.log(keypadInfo);
            $scope.keypadInfo = keypadInfo;
        };

        // MainService.setNoOfPeople = $scope.callKeypad;
        // MainService.keypadDialog = $scope.dialogTest1;
        // MainService.input = $scope.input;


        MainService.setNoOfPeople = function () {
            $scope.callKeypad($scope.dialogTest1);
        }

        // callKeypad($event, dialogTest1)
        // $scope.callKeypad($scope.dialogTest1);
        // $rootScope.$on('assignTable', function(event, args) {
        //     console.log(event);
        //     console.log('table is assigned');
        //     $scope.callKeypad($scope.dialogTest1);
        // });
    });

    app.directive('inputKeypadLeftNoofpeople', function () {
        return {
            restrict: 'E',
            controller: 'keypadControllerLeftNoofpeople',
            transclude: true,
            scope: {
                keypadInfo: '@'
            },
            template: '',
            link: function (scope, element, attrs, ctrl, transclude) {
                transclude(scope, function (clone) {
                    for (var k = 0; k < element[0].attributes.length; k++) {
                        clone.attr(element[0].attributes[k].name, element[0].attributes[k].value);
                    }
                    element.append(clone);
                });
            }
        };
    })

    app.controller("keypadControllerLeftNoofpeople", function ($scope, $mdDialog, $element, $attrs, $timeout, MainService, $rootElement, uiLanguage, INFO) {
        $scope.ui = uiLanguage[INFO.lang];
        $scope.keypad = keypad;
        $scope.keypadInfo = {
            value: ''
        };
        $scope.title = $attrs.title ? $attrs.title : 'Keypad';

        $scope.$watch(function (scope) { return MainService.Cart.splitListLNoofpeople },
           function () {
               $scope.keypadInfo.value = MainService.Cart.splitListLNoofpeople;
           }
       );

        $scope.input = function (val) {
            var maxlength = $attrs.maxlength ? $attrs.maxlength : 3;
            if ($scope.keypadInfo.value.length < maxlength)
                $scope.keypadInfo.value += '' + val;
        };
        $scope.backspace = function () {
            $scope.keypadInfo.value = $scope.keypadInfo.value.substring(0, $scope.keypadInfo.value.length - 1)
        };
        $scope.reset = function (val) {
            console.log($scope.keypadInfo.last);
            if ($scope.keypadInfo.value == '') {
                $scope.keypadInfo.value = $scope.keypadInfo.last;
                $mdDialog.cancel();
            } else {
                $scope.keypadInfo.value = '';
            }
        };

        $scope.keypressed = function (event) {
            console.log(event);
        };

        $scope.next = function (event) {
            // not allow 0 people
            if ($scope.keypadInfo.value == 0) {
                $scope.keypadInfo.value = 1;
            }
            $mdDialog.hide();
            MainService.Cart.allSplitList[MainService.Cart.splitListLIndex].nop = $scope.keypadInfo.value;
            MainService.Cart.splitListLNoofpeople = $scope.keypadInfo.value;
            angular.element($element).blur();
        };

        $scope.closeDialog = function ($mdDialog) {
            $scope.keypadInfo.value = $scope.keypadInfo.last;
            $mdDialog.cancel();
        };

        $scope.callKeypad = function (event, dialogCtrl) {
            console.log('call callKeypad');
            //console.log(event);
            // console.log($scope.keypadInfo.value);
            $scope.keypadInfo.last = $scope.keypadInfo.value;
            //console.log(angular.element('md-dialog[aria-label="keypad"]').scope());
            //console.log(123);
            $scope.keypadInfo.value = '';
            if (angular.element('md-dialog[aria-label="keypad"]').scope() == undefined) {
                $mdDialog.show({
                    controller: dialogCtrl,
                    templateUrl: 'tmpl/keypad.html',
                    targetEvent: event,
                    escapeToClose: false,
                    focusOnOpen: true,
                    bindToController: true,
                    scope: $scope,
                    preserveScope: true,
                    // for generic purpose, each instance only affect local property keypad value
                    locals: {
                        keypadInfo: $scope.keypadInfo,
                    }
                })

                $rootElement.on('keyup', function (e) {
                    // console.log("its's me");
                    console.log($scope.keypadInfo);
                    if (e.keyCode === 27) {
                        $timeout($mdDialog.hide);
                        $scope.keypadInfo.value = $scope.keypadInfo.last;
                    }
                });
            } else {
                $mdDialog.hide();
            }
        };

        $scope.dialogTest1 = function ($scope, $mdDialog, keypadInfo) {
            //console.log('dialogTest1');
            // console.log($scope.title);
            // console.log($scope.keypad);
            // console.log($scope.keypadInfo.value);
            // console.log(keypadInfo);
            $scope.keypadInfo = keypadInfo;
        };
    });

    app.directive('inputKeypadRightNoofpeople', function () {
        return {
            restrict: 'E',
            controller: 'keypadControllerRightNoofpeople',
            transclude: true,
            scope: {
                keypadInfo: '@'
            },
            template: '',
            link: function (scope, element, attrs, ctrl, transclude) {
                transclude(scope, function (clone) {
                    for (var k = 0; k < element[0].attributes.length; k++) {
                        clone.attr(element[0].attributes[k].name, element[0].attributes[k].value);
                    }
                    element.append(clone);
                });
            }
        };
    })

    app.controller("keypadControllerRightNoofpeople", function ($scope, $mdDialog, $element, $attrs, $timeout, MainService, $rootElement, uiLanguage, INFO) {
        $scope.ui = uiLanguage[INFO.lang];
        $scope.keypad = keypad;
        $scope.keypadInfo = {
            value: ''
        };
        $scope.title = $attrs.title ? $attrs.title : 'Keypad';

        $scope.$watch(function (scope) { return MainService.Cart.splitListRNoofpeople },
           function () {
               $scope.keypadInfo.value = MainService.Cart.splitListRNoofpeople;
           }
       );

        $scope.input = function (val) {
            var maxlength = $attrs.maxlength ? $attrs.maxlength : 3;
            if ($scope.keypadInfo.value.length < maxlength)
                $scope.keypadInfo.value += '' + val;
        };
        $scope.backspace = function () {
            $scope.keypadInfo.value = $scope.keypadInfo.value.substring(0, $scope.keypadInfo.value.length - 1)
        };
        $scope.reset = function (val) {
            console.log($scope.keypadInfo.last);
            if ($scope.keypadInfo.value == '') {
                $scope.keypadInfo.value = $scope.keypadInfo.last;
                $mdDialog.cancel();
            } else {
                $scope.keypadInfo.value = '';
            }
        };

        $scope.keypressed = function (event) {
            console.log(event);
        };

        $scope.next = function (event) {
            // not allow 0 people
            if ($scope.keypadInfo.value == 0) {
                $scope.keypadInfo.value = 1;
            }
            $mdDialog.hide();
            MainService.Cart.allSplitList[MainService.Cart.splitListRIndex].nop = $scope.keypadInfo.value;
            MainService.Cart.splitListRNoofpeople = $scope.keypadInfo.value;
            angular.element($element).blur();
        };

        $scope.closeDialog = function ($mdDialog) {
            $scope.keypadInfo.value = $scope.keypadInfo.last;
            $mdDialog.cancel();
        };

        $scope.callKeypad = function (event, dialogCtrl) {
            console.log('call callKeypad');
            //console.log(event);
            // console.log($scope.keypadInfo.value);
            $scope.keypadInfo.last = $scope.keypadInfo.value;
            //console.log(angular.element('md-dialog[aria-label="keypad"]').scope());
            //console.log(123);
            $scope.keypadInfo.value = '';
            if (angular.element('md-dialog[aria-label="keypad"]').scope() == undefined) {
                $mdDialog.show({
                    controller: dialogCtrl,
                    templateUrl: 'tmpl/keypad.html',
                    targetEvent: event,
                    escapeToClose: false,
                    focusOnOpen: true,
                    bindToController: true,
                    scope: $scope,
                    preserveScope: true,
                    // for generic purpose, each instance only affect local property keypad value
                    locals: {
                        keypadInfo: $scope.keypadInfo,
                    }
                })

                $rootElement.on('keyup', function (e) {
                    console.log('test', $scope.keypadInfo);
                    if (e.keyCode === 27) {
                        $timeout($mdDialog.hide);
                        $scope.keypadInfo.value = $scope.keypadInfo.last;
                    }
                });
            } else {
                $mdDialog.hide();
            }
        };

        $scope.dialogTest1 = function ($scope, $mdDialog, keypadInfo) {
            //console.log('dialogTest1');
            // console.log($scope.title);
            // console.log($scope.keypad);
            // console.log($scope.keypadInfo.value);
            // console.log(keypadInfo);
            $scope.keypadInfo = keypadInfo;
        };
    });

    app.directive('inputKeypadCancelItem', function () {
        return {
            restrict: 'E',
            controller: 'keypadControllerCancelItem',
            transclude: true,
            scope: {
                // customerInfo: '=info'
                keypadInfo: '@'
            },
            template: '',
            // <input class="{{class}}" ngMinlength="1" ngMaxlength="3" ng-model="keypadInfo.value" ng-click="callKeypad($event, dialogTest1)"/>
            // template: '<div class="{{class}}" ngMinlength="1" ngMaxlength="3" ng-click="callKeypad($event, dialogTest1)">{{keypadInfo.value}}</div>',
            link: function (scope, element, attrs, ctrl, transclude) {

                // console.log(scope.ui);
                // console.log(attrs);
                // console.log(scope.keypadInfo);
                // console.log(scope.title);
                transclude(scope, function (clone) {
                    // copy attributes to transcluded item
                    for (var k = 0; k < element[0].attributes.length; k++) {
                        clone.attr(element[0].attributes[k].name, element[0].attributes[k].value);
                    }
                    element.append(clone);

                    // clone.filter('div').addClass('reddos');
                    // console.log(clone.attr("maxlength", scope.maxlength));
                    // for(var key in attrs) {
                    //     if(key.indexOf("$") === -1)
                    //         // console.log(key);
                    //         console.log(element[0].attributes);
                    //         // clone.attr(key, element[0].)
                    // }
                    // clone.attr("maxlength", scope.maxlength)

                    // console.log(element[0].attributes);
                    // console.log(element[0].attributes[0]);
                    // console.log(element[0].attributes[1]);
                });
                // copy attribute
                // for(attr in scope.list){
                //     elem.attr(scope.list[attr].attr, scope.list[attr].value);   
                // }
            }
            // ,
            // controller: function($scope, uiLanguage, INFO){
            //     $scope.ui = uiLanguage[INFO.lang];
            // }
            // variable template
            // templateUrl: function (elem, attr) {
            //     // console.log(attr)
            //     return attr.template;
            // },
            // replace:true, work but require at least one valid DOM native container like input, div etc
        };
    })

    app.controller("keypadControllerCancelItem", function ($scope, $mdDialog, $element, $attrs, $timeout, MainService, uiLanguage, INFO) {
        // console.log($scope.title);
        // console.log($scope.keypadInfo.value);
        // console.log($attrs);, uiLanguage, INFO){
        $scope.ui = uiLanguage[INFO.lang];

        $scope.keypad = keypad;
        $scope.keypadInfo = {
            value: ''
        };
        $scope.title = $attrs.title ? $scope.ui[$attrs.title] : 'Keypad';

        $scope.$watch(function (scope) { return MainService.Cart.voidItemCount },
            function () {
                $scope.keypadInfo.value = MainService.Cart.voidItemCount;
            }
        );

        $scope.input = function (val) {
            var maxlength = $attrs.maxlength ? $attrs.maxlength : 3;
            if ($scope.keypadInfo.value.length < maxlength)
                $scope.keypadInfo.value += '' + val;
        };

        $scope.backspace = function () {
            $scope.keypadInfo.value = $scope.keypadInfo.value.substring(0, $scope.keypadInfo.value.length - 1)
        };

        $scope.reset = function (val) {
            console.log($scope.keypadInfo.last);
            if ($scope.keypadInfo.value == '') {
                $scope.keypadInfo.value = $scope.keypadInfo.last;
                $mdDialog.cancel();
            } else {
                $scope.keypadInfo.value = '';
            }
        };

        $scope.keypressed = function (event) {
            console.log(event);
        };

        $scope.next = function (event) {
            $mdDialog.hide();
            MainService.Cart.voidItemCount = $scope.keypadInfo.value;
            angular.element($element).blur();
        };

        $scope.closeDialog = function ($mdDialog) {
            $scope.keypadInfo.value = $scope.keypadInfo.last;
            $mdDialog.cancel();
        };

        $scope.callKeypad = function (event, dialogCtrl) {
            // console.log('call callKeypad');
            // console.log($scope.keypadInfo.value);
            $scope.keypadInfo.last = $scope.keypadInfo.value;
            // console.log(angular.element('md-dialog[aria-label="keypad"]').scope());
            $scope.keypadInfo.value = '';
            if (angular.element('md-dialog[aria-label="keypad"]').scope() == undefined) {
                $mdDialog.show({
                    controller: dialogCtrl,
                    templateUrl: 'tmpl/keypad.html',
                    targetEvent: event,
                    focusOnOpen: true,
                    bindToController: true,
                    scope: $scope,
                    preserveScope: true,

                    // for generic purpose, each instance only affect local property keypad value
                    locals: {
                        keypadInfo: $scope.keypadInfo,
                    }
                })
            } else {
                $mdDialog.hide();
            }
        };

        $scope.dialogTest1 = function ($scope, $mdDialog, keypadInfo) {
            console.log('dialogTest1');
            // console.log($scope.title);
            // console.log($scope.keypad);
            // console.log($scope.keypadInfo.value);
            console.log(keypadInfo);
            $scope.keypadInfo = keypadInfo;
        };

        $scope.triggerKeypad = function (element) {
            $timeout(function () {
                angular.element(element).trigger("click");
            }, 1);
        };
    });

    app.directive('inputKeypadModifier', function () {
        return {
            restrict: 'E',
            controller: 'keypadControllerModifier',
            transclude: true,
            scope: {
                keypadInfo: '@'
            },
            template: '',
            link: function (scope, element, attrs, ctrl, transclude) {
                transclude(scope, function (clone) {
                    for (var k = 0; k < element[0].attributes.length; k++) {
                        clone.attr(element[0].attributes[k].name, element[0].attributes[k].value);
                    }
                    element.append(clone);
                });
            }
        };
    })

    app.controller("keypadControllerModifier", function ($scope, $mdDialog, $element, $attrs, $timeout, MainService, uiLanguage, INFO) {
        $scope.ui = uiLanguage[INFO.lang];

        $scope.keypad = keypad;
        $scope.keypadInfo = {
            value: ''
        };
        $scope.title = $attrs.title ? $attrs.title : 'Keypad';
        //$scope.$watch(function (scope) { return MainService.Cart.itemCount },
        //    function () {
        //        $scope.keypadInfo.value = MainService.Cart.itemCount;
        //    }
        //);
        $scope.input = function (val) {
            var maxlength = $attrs.maxlength ? $attrs.maxlength : 3;
            if ($scope.keypadInfo.value.length < maxlength)
                $scope.keypadInfo.value += '' + val;
        };

        $scope.backspace = function () {
            $scope.keypadInfo.value = $scope.keypadInfo.value.substring(0, $scope.keypadInfo.value.length - 1)
        };

        $scope.reset = function (val) {
            console.log($scope.keypadInfo.last);
            if ($scope.keypadInfo.value == '') {
                $scope.keypadInfo.value = $scope.keypadInfo.last;
                $mdDialog.cancel();
            } else {
                $scope.keypadInfo.value = '';
            }
        };

        $scope.keypressed = function (event) {
            console.log(event);
        };

        $scope.next = function (event) {
            $mdDialog.hide();
            //MainService.Cart.voidItemCount = $scope.keypadInfo.value;
            console.log(MainService.Cart.changeItemQty);
            MainService.Cart.changeItemQty($scope.keypadInfo.value);
            angular.element($element).blur();
        };

        $scope.closeDialog = function ($mdDialog) {
            $scope.keypadInfo.value = $scope.keypadInfo.last;
            $mdDialog.cancel();
        };

        $scope.callKeypad = function (event, dialogCtrl) {
            $scope.keypadInfo.last = $scope.keypadInfo.value;
            // console.log(angular.element('md-dialog[aria-label="keypad"]').scope());
            $scope.keypadInfo.value = '';
            if (angular.element('md-dialog[aria-label="keypad"]').scope() == undefined) {
                $mdDialog.show({
                    controller: dialogCtrl,
                    templateUrl: 'tmpl/keypad.html',
                    targetEvent: event,
                    focusOnOpen: true,
                    bindToController: true,
                    scope: $scope,
                    preserveScope: true,

                    // for generic purpose, each instance only affect local property keypad value
                    locals: {
                        keypadInfo: $scope.keypadInfo,
                    }
                })
            } else {
                $mdDialog.hide();
            }
        };

        $scope.dialogTest1 = function ($scope, $mdDialog, keypadInfo) {
            //console.log('dialogTest1');
            // console.log($scope.title);
            // console.log($scope.keypad);
            // console.log($scope.keypadInfo.value);
            console.log(keypadInfo);
            $scope.keypadInfo = keypadInfo;
        };

        $scope.triggerKeypad = function (element) {
            $timeout(function () {
                angular.element(element).trigger("click");
            }, 1);
        };
    });

    app.directive('inputKeypad', function () {
        return {
            restrict: 'E',
            controller: 'keypadController',
            transclude: true,
            scope: {
                // customerInfo: '=info'
                keypadInfo: '@'
            },
            template: '',
            // <input class="{{class}}" ngMinlength="1" ngMaxlength="3" ng-model="keypadInfo.value" ng-click="callKeypad($event, dialogTest1)"/>
            // template: '<div class="{{class}}" ngMinlength="1" ngMaxlength="3" ng-click="callKeypad($event, dialogTest1)">{{keypadInfo.value}}</div>',
            link: function (scope, element, attrs, ctrl, transclude) {
                // console.log(attrs);
                // console.log(scope.keypadInfo);
                // console.log(scope.title);
                transclude(scope, function (clone) {
                    // copy attributes to transcluded item
                    for (var k = 0; k < element[0].attributes.length; k++) {
                        clone.attr(element[0].attributes[k].name, element[0].attributes[k].value);
                    }
                    element.append(clone);

                    // clone.filter('div').addClass('reddos');
                    // console.log(clone.attr("maxlength", scope.maxlength));
                    // for(var key in attrs) {
                    //     if(key.indexOf("$") === -1)
                    //         // console.log(key);
                    //         console.log(element[0].attributes);
                    //         // clone.attr(key, element[0].)
                    // }
                    // clone.attr("maxlength", scope.maxlength)

                    // console.log(element[0].attributes);
                    // console.log(element[0].attributes[0]);
                    // console.log(element[0].attributes[1]);
                });
                // copy attribute
                // for(attr in scope.list){
                //     elem.attr(scope.list[attr].attr, scope.list[attr].value);   
                // }
            }
            // variable template
            // templateUrl: function (elem, attr) {
            //     // console.log(attr)
            //     return attr.template;
            // },
            // replace:true, work but require at least one valid DOM native container like input, div etc
        };
    });

    app.controller('keypadController', function ($scope, $mdDialog, $element, $attrs, $timeout, MainService) {
        // console.log($scope.title);
        // console.log($scope.keypadInfo.value);
        // console.log($attrs);

        $scope.keypad = keypad;
        $scope.keypadInfo = {
            value: ''
        };
        $scope.title = $attrs.title ? $attrs.title : 'Keypad';

        //$scope.$watch(function (scope) { return MainService.Cart.noOfPeople },
        //    function () {
        //        $scope.keypadInfo.value = MainService.Cart.noOfPeople;
        //    }
        //);

        $scope.input = function (val) {
            maxlength = $attrs.maxlength ? $attrs.maxlength : 3;
            if ($scope.keypadInfo.value.length < maxlength)
                $scope.keypadInfo.value += '' + val;
        };

        $scope.backspace = function () {
            $scope.keypadInfo.value = $scope.keypadInfo.value.substring(0, $scope.keypadInfo.value.length - 1)
        };

        $scope.reset = function (val) {
            console.log($scope.keypadInfo.last);
            if ($scope.keypadInfo.value == '') {
                $scope.keypadInfo.value = $scope.keypadInfo.last;
                $mdDialog.cancel();
            } else {
                $scope.keypadInfo.value = '';
            }
        };

        $scope.keypressed = function (event) {
            console.log(event);
        };

        $scope.next = function (event) {
            $mdDialog.hide();
            //MainService.Cart.noOfPeople = $scope.keypadInfo.value;
            angular.element($element).blur();
        };

        $scope.closeDialog = function ($mdDialog) {
            $scope.keypadInfo.value = $scope.keypadInfo.last;
            $mdDialog.cancel();
        };

        $scope.callKeypad = function (event, dialogCtrl) {
            // console.log('call callKeypad');
            // console.log($scope.keypadInfo.value);
            $scope.keypadInfo.last = $scope.keypadInfo.value;
            // console.log(angular.element('md-dialog[aria-label="keypad"]').scope());
            $scope.keypadInfo.value = '';
            if (angular.element('md-dialog[aria-label="keypad"]').scope() == undefined) {
                $mdDialog.show({
                    controller: dialogCtrl,
                    templateUrl: 'tmpl/keypad.html',
                    targetEvent: event,
                    focusOnOpen: true,
                    bindToController: true,
                    scope: $scope,
                    preserveScope: true,

                    // for generic purpose, each instance only affect local property keypad value
                    locals: {
                        keypadInfo: $scope.keypadInfo,
                    }
                })
            } else {
                $mdDialog.hide();
            }
        };

        $scope.dialogTest1 = function ($scope, $mdDialog, keypadInfo) {
            console.log('dialogTest1');
            // console.log($scope.title);
            // console.log($scope.keypad);
            // console.log($scope.keypadInfo.value);
            console.log(keypadInfo);
            $scope.keypadInfo = keypadInfo;
        };

        $scope.triggerKeypad = function (element) {
            $timeout(function () {
                angular.element(element).trigger("click");
            }, 1);
        };
    });

    app.controller("keypadControllerFoodControl", function ($scope, $mdDialog, $element, $attrs, $timeout, MainService) {
        // console.log($scope.title);
        // console.log($scope.keypadInfo.value);
        //console.log($attrs);
        $scope.keypad = keypad;
        $scope.keypadInfo = {
            value: ''
        };
        $scope.title = $attrs.title ? $attrs.title : 'Keypad';

        //$scope.$watch(function (scope) { return MainService.Cart.noOfPeople },
        //    function () {
        //        $scope.keypadInfo.value = MainService.Cart.noOfPeople;
        //    }
        //);

        $scope.input = function (val) {
            var maxlength = $attrs.maxlength ? $attrs.maxlength : 3;
            if ($scope.keypadInfo.value.length < maxlength)
                $scope.keypadInfo.value += '' + val;
        };

        $scope.backspace = function () {
            $scope.keypadInfo.value = $scope.keypadInfo.value.substring(0, $scope.keypadInfo.value.length - 1)
        };

        $scope.reset = function (val) {
            console.log($scope.keypadInfo.last);
            if ($scope.keypadInfo.value == '') {
                $scope.keypadInfo.value = $scope.keypadInfo.last;
                $mdDialog.cancel();
            } else {
                $scope.keypadInfo.value = '';
            }
        };

        $scope.keypressed = function (event) {
            console.log(event);
        };

        $scope.next = function (event) {
            $mdDialog.hide();
            //MainService.Cart.noOfPeople = $scope.keypadInfo.value;
            MainService.stock.updateStock($scope.keypadInfo.value);
            angular.element($element).blur();
        };

        $scope.closeDialog = function ($mdDialog) {
            $scope.keypadInfo.value = $scope.keypadInfo.last;
            $mdDialog.cancel();
        };

        $scope.callKeypad = function (event, dialogCtrl, item) {
            // console.log('call callKeypad');
            // console.log($scope.keypadInfo.value);
            // console.log(angular.element('md-dialog[aria-label="keypad"]').scope());
            $scope.keypadInfo.last = $scope.keypadInfo.value;
            $scope.keypadInfo.value = '';
            if (MainService.modeOrder == MainService.schema.modeOrder.foodControl) {
                console.log('item.stock ' + item.stock);
                if (item.stock == -1) {
                    console.log('update stock number');
                    MainService.stock.updateStock(-2);
                } else {
                    if (angular.element('md-dialog[aria-label="keypad"]').scope() == undefined) {
                        $mdDialog.show({
                            controller: dialogCtrl,
                            templateUrl: 'tmpl/keypad.html',
                            targetEvent: event,
                            focusOnOpen: true,
                            bindToController: true,
                            scope: $scope,
                            preserveScope: true,

                            // for generic purpose, each instance only affect local property keypad value
                            locals: {
                                keypadInfo: $scope.keypadInfo,
                            }
                        })
                    } else {
                        $mdDialog.hide();
                    }

                }
            }
        };

        $scope.dialogTest1 = function ($scope, $mdDialog, keypadInfo) {
            console.log('dialogTest1');
            // console.log($scope.title);
            // console.log($scope.keypad);
            // console.log($scope.keypadInfo.value);
            console.log(keypadInfo);
            $scope.keypadInfo = keypadInfo;
        };

        $scope.triggerKeypad = function (element) {
            $timeout(function () {
                angular.element(element).trigger("click");
            }, 1);
        };
    });

    // app.controller("keypadControllerOpenItem", function ($scope, $mdDialog, $element, $attrs, $timeout, MainService) {
    //     $scope.keypad = keypad;
    //     $scope.keypadInfo = {
    //         value: '0.0',
    //         name: ''
    //     };
    //     $scope.title = $attrs.title ? $attrs.title : 'Keypad';
    //     $scope.input = function (val) {
    //         var maxlength = $attrs.maxlength ? $attrs.maxlength : 3;
    //         if ($scope.keypadInfo.value.length < maxlength)
    //             $scope.keypadInfo.value += '' + val;
    //     };

    //     $scope.currentFieldName = "name";
    //     $scope.focusField = function (fieldName) {
    //         this.currentFieldName = fieldName;
    //         return true;
    //     }

    //     // for non-numerical character
    //     $scope.inputFree = function (val) {
    //         console.log($scope.keypadInfo);
    //         // simpler:
    //         // number type field
    //         if (this.currentFieldName === 'value') {
    //             switch (val) {
    //                 case '.':
    //                     console.log('. ar');
    //                     this.appendNumFree(val);
    //                     break;
    //                 case 'tab':
    //                 case 'space':
    //                     break;
    //                 default:
    //                     /*$scope.keypadInfo[this.currentFieldName] = parseFloat( String( $scope.keypadInfo[this.currentFieldName] ) + val );
    //                     break;*/
    //             };
    //         } else {
    //             // text field (exclude date, time and so on... bare text only)
    //             switch (val) {
    //                 case 'space':
    //                     val = " ";
    //                     break;
    //                 case 'tab':
    //                     val = '';
    //                     for (var i = 0; i < 3; i++) {
    //                         val += " ";
    //                     }
    //                     break;
    //                 default:
    //                     break;
    //             };
    //             $scope.keypadInfo[this.currentFieldName] += '' + val;
    //         }

    //         // switch( this.currentFieldName ) {
    //         //     case "value":
    //         //         $scope.keypadInfo.value += '' + val;
    //         //         break;
    //         //     default:
    //         //     case "name":
    //         //         $scope.keypadInfo.name += '' + val;
    //         //         break;
    //         // }
    //     };

    //     // for numerical character, support append, decimal points
    //     $scope.appendNumFree = function (amount) {

    //         console.log(amount);
    //         // do nothing if floating point is already input
    //         var inputValue = $scope.keypadInfo[this.currentFieldName];
    //         if (amount === '.' && $scope.keypadInfo[this.currentFieldName][inputValue.length - 1] === '.') {
    //             console.log('0');
    //             return;
    //         }

    //         // if floating point is activated, not allow to add one character
    //         // this.inputPrice += '';
    //         $scope.keypadInfo[this.currentFieldName] = String(inputValue); // type aware method and meaning is obvious without comment
    //         if (inputValue.indexOf('.') === -1 && amount === '.') {
    //             console.log('1');
    //             this.floatingInput = true;
    //             // not necessary because " ", it will make it .# instead of 0.# when it is 0
    //             // if (this.inputPrice.trim() == "0") { // input 7, result: .7 instead of 0.7
    //             //     console.log('1-1');
    //             //     console.log(this.inputPrice.trim());
    //             //     this.inputPrice = amount;
    //             // }else{
    //             //     console.log('1-2');
    //             //     this.inputPrice = this.inputPrice.concat(amount);
    //             // }
    //             $scope.keypadInfo[this.currentFieldName] = inputValue.trim().concat(amount);
    //             return;
    //         }

    //         if (inputValue.indexOf('.') != -1 && amount === '.') {
    //             console.log('2');
    //             return
    //         }

    //         // console.log(this.inputPrice.indexOf('.'));
    //         // console.log( !isNaN(parseInt(this.inputPrice[this.inputPrice.length - 1])));
    //         if (inputValue.indexOf('.') != -1 && !isNaN(parseInt($scope.keypadInfo[this.currentFieldName][inputValue.length - 1]))) {
    //             console.log("3");
    //             return;
    //         }
    //         // console.log(typeof this.inputPrice);
    //         // console.log(typeof this.inputPrice.trim());
    //         // console.log('when is this.inputPrice.trim() == 0 ' + this.inputPrice.trim());

    //         if (inputValue.trim() == "0") { // 排除預設等值有space的障礙
    //             console.log("4");
    //             $scope.keypadInfo[this.currentFieldName] = amount;
    //         } else {
    //             console.log("5");
    //             // append 0 if prev is "."
    //             if (inputValue[0] === '.') {
    //                 console.log("0" + inputValue);
    //                 $scope.keypadInfo[this.currentFieldName] = "0" + inputValue.concat(amount);
    //             } else {
    //                 $scope.keypadInfo[this.currentFieldName] = inputValue.concat(amount);
    //             }

    //         }
    //         // default
    //     }

    //     $scope.backspaceFree = function () {
    //         console.log($scope.keypadInfo);
    //         // convert it to string or it will get error when it is 0 and use backspace

    //         if (this.currentFieldName === 'value') {
    //             var inputValue = String($scope.keypadInfo[this.currentFieldName]);
    //             /*var tmpStr = String( $scope.keypadInfo[this.currentFieldName] );
    //             if( tmpStr.length > 1) {
    //                 $scope.keypadInfo[this.currentFieldName] = parseFloat( tmpStr.substring(0, tmpStr.length - 1) );
    //             }

    //             if( tmpStr.length == 1) {
    //                 $scope.keypadInfo[this.currentFieldName] = 0;
    //             }*/

    //             if (inputValue.length === 0)
    //                 return;

    //             console.log(inputValue);
    //             if (inputValue[inputValue.length - 1] === '.') {
    //                 this.floatingInput = false;
    //             }

    //             $scope.keypadInfo[this.currentFieldName] = inputValue.substring(0, inputValue.length - 1);

    //             if (isNaN(inputValue) || inputValue === '') {
    //                 console.log('nan');
    //                 $scope.keypadInfo[this.currentFieldName] = '';
    //             }
    //         } else {
    //             $scope.keypadInfo[this.currentFieldName] = $scope.keypadInfo[this.currentFieldName].substring(0, $scope.keypadInfo[this.currentFieldName].length - 1);
    //         }
    //     }

    //     $scope.backspace = function () {
    //         $scope.keypadInfo.value = $scope.keypadInfo.value.substring(0, $scope.keypadInfo.value.length - 1);
    //     };

    //     $scope.reset = function (val) {
    //         console.log($scope.keypadInfo.lastValue);
    //         console.log($scope.keypadInfo.lastName);
    //         if ($scope.keypadInfo.name == '') {
    //             $scope.keypadInfo.name = $scope.keypadInfo.lastName;
    //             $mdDialog.cancel();
    //         } else {
    //             $scope.keypadInfo.name = '';
    //         }

    //         if ($scope.keypadInfo.value == '') {
    //             $scope.keypadInfo.value = $scope.keypadInfo.lastValue;
    //             $mdDialog.cancel();
    //         } else {
    //             $scope.keypadInfo.value = '';
    //         }
    //     };

    //     $scope.cancel = function () {
    //         // var focusedElement = document.activeElement;
    //         // console.log(focusedElement);
    //         // console.log($scope.lastActiveField);
    //         console.log('cancel');
    //         console.log($scope.lastActiveField == "item-name" && $scope.keypadInfo.name != "");
    //         console.log($scope.lastActiveField == "item-price" && $scope.keypadInfo.value != "");
    //         console.log($scope.lastActiveField);
    //         console.log($scope.keypadInfo.value);
    //         if ($scope.lastActiveField == "item-name" && $scope.keypadInfo.name != "") {
    //             $scope.keypadInfo.name = "";
    //             return;
    //         }

    //         if ($scope.lastActiveField == "item-price" && $scope.keypadInfo.value != "") {
    //             $scope.keypadInfo.value = "";
    //             return;
    //         }

    //         $mdDialog.cancel();
    //     }

    //     $scope.keypressed = function (event) {
    //         console.log(event);
    //     };

    //     $scope.next = function (event) {
    //         $mdDialog.hide();
    //         //MainService.Cart.noOfPeople = $scope.keypadInfo.value;
    //         MainService.stock.updateStock($scope.keypadInfo.value);
    //         angular.element($element).blur();
    //         alert($scope.keypadInfo.value);
    //     };

    //     $scope.checkDecimal = function (amount) {
    //         if (amount[amount.length - 1] === '.' || amount[0] === '.')
    //             return false; // input is incorrect
    //         else
    //             return true;
    //     }

    //     $scope.nextFree = function (event) {
    //         if (!this.checkDecimal($scope.keypadInfo.value) || $scope.keypadInfo.value === '') {
    //             return; // input is not completed
    //         }

    //         $mdDialog.hide();
    //         //MainService.Cart.noOfPeople = $scope.keypadInfo.value;
    //         //MainService.stock.updateStock($scope.keypadInfo.value);
    //         angular.element($element).blur();
    //         //alert($scope.keypadInfo.value);
    //         //alert($scope.keypadInfo.name);
    //         console.log($scope.currentItem);
    //         //$scope.currentItem.name = $scope.keypadInfo.name;

    //         var customItem = {};
    //         $.extend(true, customItem, $scope.currentItem);
    //         customItem.desc1 = $scope.keypadInfo.name;
    //         customItem.name['001'] = $scope.keypadInfo.name;
    //         customItem.customName = $scope.keypadInfo.name;
    //         customItem.unitprice = parseFloat($scope.keypadInfo.value) * 100;
    //         console.log(customItem)
    //         MainService.Cart.addItem(customItem);
    //     };

    //     $scope.closeDialog = function ($mdDialog) {
    //         $scope.keypadInfo.value = $scope.keypadInfo.lastValue;
    //         $scope.keypadInfo.name = $scope.keypadInfo.lastName;
    //         $mdDialog.cancel();
    //     };

    //     $scope.callKeypad = function (event, dialogCtrl, item) {
    //         console.log('call callKeypad right keypadControllerFoodControl');
    //         // console.log($scope.keypadInfo.value);
    //         // console.log(angular.element('md-dialog[aria-label="keypad"]').scope());
    //         $scope.keypadInfo.lastValue = $scope.keypadInfo.value;
    //         $scope.keypadInfo.lastName = $scope.keypadInfo.name;
    //         $scope.keypadInfo.value = '8888';
    //         if (MainService.modeOrder == MainService.schema.modeOrder.normal) {
    //             if (item.stock == -1) {
    //                 MainService.stock.updateStock(-2);
    //             } else {
    //                 if (angular.element('md-dialog[aria-label="keypad"]').scope() == undefined) {
    //                     $mdDialog.show({
    //                         controller: dialogCtrl,
    //                         // templateUrl: 'tmpl/keypad.html',
    //                         templateUrl: 'tmpl/keypad.html',
    //                         targetEvent: event,
    //                         focusOnOpen: true,
    //                         bindToController: true,
    //                         scope: $scope,
    //                         preserveScope: true,

    //                         // for generic purpose, each instance only affect local property keypad value
    //                         locals: {
    //                             keypadInfo: $scope.keypadInfo,
    //                         }
    //                     })
    //                 } else {
    //                     $mdDialog.hide();
    //                 }

    //             }
    //         }
    //     };

    //     $scope.currentItem = {};

    //     $scope.callKeypadFree = function (event, dialogCtrl, item) {
    //         $timeout(function () {
    //             // $element[0].focus();
    //         });
    //         console.log(item.type);
    //         if (item.type === "o" && MainService.modeOrder != MainService.schema.modeOrder.foodControl) {
    //             console.log($element);
    //             console.log('call callKeypad right keypadControllerFoodControl');
    //             console.log(item);
    //             // console.log(angular.element('md-dialog[aria-label="keypad"]').scope());
    //             $scope.keypadInfo.lastValue = $scope.keypadInfo.value;
    //             $scope.keypadInfo.lastName = $scope.keypadInfo.name;
    //             //$scope.keypadInfo.name = item.name1;
    //             $scope.keypadInfo.name = '';
    //             $scope.keypadInfo.value = '';
    //             $scope.currentItem = item;
    //             if (MainService.modeOrder == MainService.schema.modeOrder.normal) {
    //                 if (item.stock == -1) {
    //                     MainService.stock.updateStock(-2);
    //                 } else {
    //                     if (angular.element('md-dialog[aria-label="keypad"]').scope() == undefined) {
    //                         $mdDialog.show({
    //                             controller: dialogCtrl,
    //                             // templateUrl: 'tmpl/keypad.html',
    //                             templateUrl: 'tmpl/keypad-free.html',
    //                             targetEvent: event,
    //                             focusOnOpen: true,
    //                             bindToController: true,
    //                             scope: $scope,
    //                             preserveScope: true,
    //                             onComplete: function () {
    //                                 // determine which is last active field for cancel button to discriminate
    //                                 $scope.lastActiveField = angular.element('#free-item-keypad input[name="item-name"]').attr('name');
    //                                 angular.element('#free-item-keypad input').on('focus', function () {
    //                                     // console.log(angular.element(this).attr('name'));
    //                                     $scope.lastActiveField = angular.element(this).attr('name');
    //                                 })
    //                             },

    //                             // for generic purpose, each instance only affect local property keypad value
    //                             locals: {
    //                                 keypadInfo: $scope.keypadInfo
    //                             }
    //                         })
    //                     } else {
    //                         $mdDialog.hide();
    //                     }

    //                 }
    //             }
    //         } else {

    //             MainService.Cart.addItem(item);
    //         }
    //     };

    //     $scope.dialogTest1 = function ($scope, $mdDialog, keypadInfo) {
    //         console.log('dialogTest1');
    //         // console.log($scope.title);
    //         // console.log($scope.keypad);
    //         // console.log($scope.keypadInfo.value);
    //         console.log(keypadInfo);
    //         $scope.keypadInfo = keypadInfo;
    //     };

    //     $scope.triggerKeypad = function (element) {
    //         $timeout(function () {
    //             angular.element(element).trigger("click");
    //         }, 1);
    //     };
    // });

    app.directive('rmsTop', function (INFO, uiLanguage, userConfig) {
        return {
            restrict: "E",
            templateUrl: "tmpl/top-bar.html",
            controller: function ($scope, INFO, uiLanguage, userConfig) {
                if (INFO.lang == "003") {
                    if (angular.element('#toolbar-top').length != 0) {
                        angular.element('#toolbar-top').addClass('en');
                    }

                    //angular.element('.content-area').addClass('en');
                }
                // console.log('called me?'); // answer: once
                $scope.config = userConfig;
                $scope.ui = uiLanguage[INFO.lang];
            }
        }
    })

    app.directive('rmsStatus', function (userConfig) {
        return {
            restrict: "E",
            link: function ($scope) {
                $("#clock").html((new Date()).toLocaleTimeString('en-US', { hour12: false }));
                window.clock = setInterval(function () {
                    var current = new Date().getTime() - ($scope.uc.serverTimeInterval || 0);
                    $("#clock").html(new Date(current).toLocaleTimeString('en-US', { hour12: false }));
                }, 1000);
            },
            templateUrl: 'tmpl/status-bar.html',
            controller: function ($scope) {
            }
        }
    })




    // RMS helpers
    // addon helpers
    /* Create a shape node with the given settings. */
    // function returnTableSVG(t, s) {
    //     var tableType = this.tableType;
    //     return tableType[t].sizeList[s].svgCode;
    // }

    // // similar to getTableSVG, without adding group, maybe too redundant with too many functions
    // function returnTableSVGCode(shape, size) {
    //     // console.log('shape ' + shape);
    //     // console.log('size ' + typeof size);
    //     var tableType = this.tableType;
    //     var svgCode = "";
    //     for (var t = 0; t < tableType.length; t++) {
    //         if (tableType[t].type.toLowerCase() === shape.toLowerCase()) {
    //             // console.log(tableType[t].type.toLowerCase() === shape.toLowerCase());
    //             for (var s = 0; s < tableType[t].sizeList.length; s++) {
    //                 if (tableType[t].sizeList[s].noOfPeople === size) {
    //                     // console.log(tableType[t].sizeList[s].noOfPeople === size);
    //                     return returnTableSVG(t, s);
    //                 }
    //             }
    //         }
    //     } // for
    //     return svgCode;
    // }

    // /*
    //     return available size for a shape in table shape object
    //     return array
    // */
    // function getShapeAllSize(shape) {
    //     var tableType = scope.tableType;
    //     var size = [];
    //     for (var t = 0; t < tableType.length; t++) {
    //         if (tableType[t].type.toLowerCase() === shape.toLowerCase()) {
    //             for (var s = 0; s < tableType[t].sizeList.length; s++) {
    //                 size.push(tableType[t].sizeList[s].noOfPeople);
    //             }
    //         }
    //     }
    //     return size;
    // }

    // // want to make it generic but not yet success
    // function getTableSVG(shape, size, option, callback) {
    //     var tableType = this.tableType;
    //     // console.log('tableType inside getTableSVG ' + tableType);
    //     option = "single";
    //     switch (option) {
    //         default:
    //         case "single":
    //             for (var t = 0; t < tableType.length; t++) {
    //                 if (tableType[t].type.toLowerCase() === shape.toLowerCase()) {
    //                     for (var s = 0; s < tableType[t].sizeList.length; s++) {
    //                         if (tableType[t].sizeList[s].noOfPeople === size) {
    //                             // var className = shape + "-" + size;
    //                             // return "<g class='shape " + className + "'>" + returnTableSVG(t, s) + "</g>";
    //                             return "<g class='shape'>" + returnTableSVG.apply(this, [t, s]) + "</g>";
    //                         }
    //                     }
    //                 }
    //             }

    //             return "SVG Object Not Found";
    //             break;
    //     }
    // }

    // /*
    //     return available size for a shape in table shape object
    //     return array
    // */
    // function getShapeAllSize(shape) {
    //     var tableType = scope.RMS.tableTypeJSON.tableType;
    //     var size = [];
    //     for (var t = 0; t < tableType.length; t++) {
    //         if (tableType[t].type.toLowerCase() === shape.toLowerCase()) {
    //             for (var s = 0; s < tableType[t].sizeList.length; s++) {
    //                 size.push(tableType[t].sizeList[s].noOfPeople);
    //             }
    //         }
    //     }
    //     return size;
    // }

    // /*
    //     return available shape in table shape object
    //     return array
    // */
    // function getAllShape() {
    //     var shape = [];
    //     var tableType = scope.tableType;
    //     for (var t = 0; t < tableType.length; t++) {
    //         shape.push(tableType[t].type);
    //     }
    //     return shape;
    // }

    // function hideTableHandles() {
    //     Snap("#svg-canvas").selectAll(".transform").forEach(function(element, index, array) {
    //         if (element.freeTransform) {
    //             element.freeTransform.hideHandles();
    //         }
    //     });
    // }

    // function removeSelectionBB( ele ) {
    //   console.log('removeSelectionBB');
    //     var svgScene = Snap("#svg-canvas");
    //     ele.ftRemoveBB();
    // }

    // function toggleSelectionBB( ele, scope ) {
    //     console.log('toggleSelectionBB');

    //     var eleID = ele.parent().attr('id');
    //     // console.log(ele.parent().attr('id'));

    //     if( ele.hasClass('selected') ) {
    //         removeSelectionBB( ele );
    //     } else {
    //         ele.ftHighlightBB({ handleFill: "orange", handleClass: ['-handle-box', eleID ] });
    //     }

    //     ele.toggleClass('selected');

    //     // console.log(scope);
    //     console.log(ele.data());
    // }

    // // ng-click version
    // function tableMouseDown2(ID, scope) {
    //     // console.log('tableMouseDown2');
    //     ele = Snap('#'+ID).select('.transform');
    //     MainService.RMSTable.currentActiveElementSnap = Snap('#'+ID);
    //     // console.log(ID);
    //     // console.log(ele.parent().attr('id'));

    //     if( typeof ele.parent().data('state') === 'undefined' )
    //         ele.parent().data('state', 'preModify');

    //     // console.log(ele.parent().data());

    //     $('#current-table-label').val(ele.select('text').attr('text'));

    //     var className = ele.attr('class') || '';
    //     // console.log('add class');
    //     // console.log(className.indexOf('modified'));
    //     if( className.indexOf('modified') === -1 ) {
    //         if( className ) {
    //             ele.attr('class', className + ' modified');
    //         } else {
    //             ele.attr('class', 'modified');
    //         }
    //     }


    //     if( scope.isMode('floorPlan') ) {
    //         switch( MainService.modeNavMenu ) {
    //             default :
    //             case MainService.schema.modeNavMenu.normal:
    //                 removeAllHandles();
    //                 scope.RMSTable.makeOrder(ele);
    //                 break;
    //             case MainService.schema.modeNavMenu.editTable:
    //                 // update current selected item to global variable
    //                 try {
    //                     // console.log("try 2-1");
    //                     scope.RMSTable.currentActiveElement = ele.parent().data("id");
    //                     scope.RMSTable.currentActiveElementSnap = ele.parent();
    //                     scope.RMSTable.currentTableNo = ele.select("text").attr("text");
    //                 } catch(e){
    //                     // console.log("e 2-1");
    //                     try{
    //                         // console.log("try 2-2");
    //                         scope.RMSTable.currentActiveElement = ele.attr("id");
    //                         scope.RMSTable.currentActiveElementSnap = ele;
    //                     }catch(e){
    //                         // console.log("e 2-2");
    //                         scope.RMSTable.currentActiveElement = ele.attr("id");
    //                         scope.RMSTable.currentActiveElementSnap = ele;
    //                     }
    //                 }
    //                 // console.log('scope.RMSTable.currentTableNo ' + scope.RMSTable.currentTableNo);
    //                 // console.log(scope.RMSTable.currentActiveElementSnap);

    //                 // hide all handles
    //                 hideTableHandles();

    //                 // in multiple selection mode is on, will not turn on handle, only input field is valid
    //                 console.log('MainService.RMSTable.groupEdit ' + MainService.RMSTable.groupEdit);
    //                 if( MainService.RMSTable.groupEdit ) {
    //                     if( ele.freeTransform ) {
    //                         ele.freeTransform.hideHandles();
    //                     } else {
    //                         createHandles();
    //                         ele.freeTransform.hideHandles();
    //                     }
    //                     toggleSelectionBB( ele, scope );
    //                     return;
    //                 }

    //                 function createHandles() {
    //                     // var loadedMatrix = ele.attr("transform").totalMatrix.split();
    //                     var loadedMatrix = Snap.parseTransformString(ele.transform());
    //                     console.log(loadedMatrix);

    //                     /*defaultAttrs: {
    //                         rotate: loadedMatrix.rotate,
    //                         scale: { x: loadedMatrix.scalex, y: loadedMatrix.scaley },
    //                         translate: { x: loadedMatrix.dx, y: loadedMatrix.dy },
    //                         ratio: 1
    //                     }*/

    //                     var opts = {};
    //                     var vpBoundaryBBox = MainService.RMSTable.svgScene.select('.viewport-helper').getBBox();
    //                     opts = {
    //                        size: { x: ele.getBBox().width / 2 , y: ele.getBBox().height / 2 },
    //                        attrs: { fill: '#fff', stroke: '#000', class: 'center' },
    //                        axesAttrs: { fill: '#fff', stroke: '#000', 'stroke-dasharray': '4, 4', 'stroke-width': '8px', opacity: 0.5, class: 'axes'},
    //                        discAttrs: { fill: '#fff', stroke: '#000', class: 'disc' },
    //                        boundary: { x: 0|| 0, y: 0|| 0, width: vpBoundaryBBox.width, height: vpBoundaryBBox.height },
    //                        keepRatio: true,
    //                        snap: { 
    //                            rotate: userConfig.table.edit.snap.rotate, 
    //                            scale: 1, 
    //                            drag: userConfig.table.edit.snap.translate
    //                        },
    //                        rotate: userConfig.table.edit.allowRotate,
    //                        scale: userConfig.table.edit.allowScale,
    //                        snapDist: { rotate: 0, scale: 0, drag: 1},
    //                        size: userConfig.table.display.handle.size,
    //                        defaultAttrs: {
    //                            // rotate: loadedMatrix.rotate,
    //                            // // center: { x: 16, y: 0 },
    //                            // scale: { x: loadedMatrix.scalex, y: loadedMatrix.scaley },
    //                            // translate: { x: loadedMatrix.dx, y: loadedMatrix.dy },
    //                            rotate: loadedMatrix[0][1],
    //                            center: { x: loadedMatrix[0][2], y: loadedMatrix[0][3] },
    //                            scale: { x: loadedMatrix[1][1], y: loadedMatrix[1][2] },
    //                            translate: { x: loadedMatrix[2][1], y: loadedMatrix[2][2] },
    //                            // 40 offset x
    //                            ratio: 1,
    //                        }
    //                     }

    //                     // console.log(opts.defaultAttrs);
    //                     // if( ele.parent().data("id") == "table_1418785047402") {
    //                     // }

    //                     var updateEle = function( ft, events, ele ) {
    //                         // console.log(arguments);
    //                         // console.log(events);
    //                         if( events[0] === 'drag end' ) {
    //                             console.log("callback after apply");
    //                             ele.parent().data('modified', true);
    //                             // console.log(ele.parent());
    //                             // console.log(ele.parent().data);

    //                             var updatedMatrix = ele.attr("transform").totalMatrix.split();

    //                             ele.parent().data('state', 'modified');
    //                         }
    //                     }

    //                     // console.log(opts);
    //                     // console.log(ele);
    //                     // console.log(updateEle);
    //                     Snap.freeTransform( ele, opts, updateEle );
    //                 }

    //                 // show handles for selected element
    //                 if( ele.freeTransform ) {
    //                     ele.freeTransform.showHandles();
    //                 } else {
    //                     createHandles();
    //                 }
    //                 break;
    //             case MainService.schema.modeNavMenu.addTable:
    //                 removeAllHandles();
    //                 break;
    //         }
    //     }
    // }

    // function removeAllHandles() {
    //     var svgScene = Snap("#svg-canvas");
    //     svgScene.selectAll(".tableGroup .transform").forEach(function (element, index, array) {
    //         element.ftRemoveHandles();
    //         element.ftRemoveBB();
    //         MainService.RMSTable.groupEdit = false;
    //         $('.btn-multi-edit').removeClass('active');
    //         $('#current-table-label').attr('disabled', false);

    //         if (element.freeTransform) {
    //             console.log('remove free transform');
    //             element.freeTransform.unplug();
    //         }
    //     });
    // }

    // app.directive('newTableShape', function ( $compile, $http, SocketFactory, $timeout ) {
    //     return {
    //         restrict: 'E',
    //         // <svg id="table-shape" style="visibility: visible; height: 72px; background: red;"></svg>
    //         template: '<div id="new-table-shape-container" class="" style="height: 250px; width: 205px;"><svg id="table-shape" style="visibility: visible; height: 100%; width:100%; background: white;"></svg></div>',
    //         link: function( scope, element, attrs ) {

    //         },
    //         controller: 'newTableShapeController'
    //     }
    // });

    // app.controller('newTableShapeController', function( $scope, $http, $timeout, $interval, $compile, $rootElement, MainService ) {
    //     var RMSTable = {};
    //     RMSTable.renderTableType = function() {
    //         // console.log('RMSTable.renderTableType ');
    //         this.svgShape = Snap('#table-shape');
    //         // console.log(RMSTable);

    //         var masterTableEleGroup = this.svgShape.group().attr({
    //             'id': 'new-table-group',
    //             'class': 'table-group'
    //         });
    //         // console.log(masterTableEleGroup);

    //         // var test = masterTableEleGroup.group(Snap.parse("<path d='M0,0h30.625v25.875H0V0z'/>").select('path')).attr('id', 'test1234');

    //         // var renderTimeout = [];
    //         var fillArr = [];
    //         fillArr[0] = 'red';
    //         fillArr[1] = 'red';
    //         fillArr[2] = 'red';
    //         for( i=0; i<MainService.RMSTable.tableType.length; i++ ) {
    //             var tmp = 0;

    //             /*
    //                 reference: https://developer.mozilla.org/en/docs/Web/API/SVGElement
    //                 'outer most element has no ownerSVGElement property'
    //                 before adding to any paper, 'g' is outer most element, thus, no ownerSVGElement property
    //                 so, in Snap.svg, it use "owner" to check if it need to create paper since 
    //                 it doesnt know if its parent is SVG or not, if ever needed paper, just add back manually is fine in known cases
    //             */
    //             var f = Snap.parse('<g class="shape">' + MainService.RMSTable.tableType[i].sizeList[0].svgCode + '</g>').select('g'),
    //                 // tableNode = f,
    //                 shape = MainService.RMSTable.tableType[i].type.toLowerCase(),
    //                 ID = 'shape-' + shape,
    //                 type = 'table',
    //                 size = 0;

    //             f.data({
    //                 type: type,
    //                 shape: shape, 
    //                 size: 0,
    //             });

    //             // console.log(f.paper); // itself is group because selected already but no paper defined
    //             // console.log(f.select('path').paper); // paper is not yet defined
    //             var gTransform = masterTableEleGroup.group(f).attr({
    //                 'class': 'transform',
    //                 'id': 'transform'+i
    //             });
    //             // console.log(f.select('path').paper); // paper is defined after group, follow #table-shape
    //             // console.log(gTransform); // paper is defined, follow #table-shape

    //             var shapeContainer = masterTableEleGroup.group(gTransform).attr({
    //                 'id': ID,
    //                 'ng-click': 'addTable($event)'
    //             });
    //             // console.log(shapeContainer) // paper is defined, follow #table-shape

    //             shapeContainer.data('type', MainService.RMSTable.tableType[i].type.toLowerCase()+'-0');
    //             // console.log(MainService.RMSTable.tableType[i].type.toLowerCase()+'-0');

    //             // shapeContainer.select('path').attr({
    //             //     fill: fillArr[i],
    //             // });

    //             shapeContainer.selectAll('path, circle').forEach(function(element){
    //                 element.attr({
    //                     fill: fillArr[i],
    //                 });
    //             })

    //             // console.log(shapeContainer);

    //             // var eleBBox = gTransform.select('rect').getBBox();
    //             $compile( $('#'+ID) )( $scope );

    //             // console.log(gTransform.select('.shape').data());
    //         } // for

    //         var renderTimeout = $interval(function(){
    //             eleBBox = masterTableEleGroup.getBBox();

    //             // console.log(eleBBox);
    //             // console.log(tmp); // timer
    //             // if( tmp === 10000 ) {
    //             //     console.log('timeout');
    //             //     $interval.cancel( renderTimeout );
    //             // }

    //             if( eleBBox.width != 0 || tmp === 100000  ) {
    //                 console.log('SVG load completed');
    //                 $interval.cancel( renderTimeout );

    //                 var offsetX = 10, offsetY = 10;
    //                 for( i=0; i<MainService.RMSTable.tableType.length; i++ ) {
    //                     var shape = MainService.RMSTable.tableType[i].type.toLowerCase(),
    //                         ID = '#shape-' + shape,
    //                         ele = masterTableEleGroup.select(ID);

    //                     // var type = ele.data('type');
    //                     // console.log(type);

    //                     eleBBox = ele.getBBox();
    //                     // console.log(ID);
    //                     // console.log(ele);
    //                     // console.log(offsetX);
    //                     // console.log(eleBBox);
    //                     var scale = 1.8;
    //                     var tableInit = [
    //                         'R' + 0, 0, 0,
    //                         'S' + scale, scale, 0, 0,
    //                         'T' + offsetX, offsetY
    //                     ].join(',');
    //                     // console.log('after');
    //                     // console.log(eleBBox);
    //                     // console.log(tableInit);
    //                     // offsetX += eleBBox.width * scale + 10;
    //                     // offsetX += 10;
    //                     offsetY += eleBBox.height * scale + 10;
    //                     ele.select('.transform').transform(tableInit);
    //                 }

    //                 $('new-table-shape').appendTo($('#add-table-container'));
    //                 $('#new-table-shape-container').addClass('ng-hide');
    //             } else {
    //                 tmp += 10;
    //             }
    //         }, tmp)


    //         // console.log(Snap('#table-shape').getBBox());
    //     }

    //     if( angular.isUndefined( MainService.RMSTable ) ) {
    //         // console.log('new');
    //         // console.log('new');
    //         MainService.RMSTable = RMSTable;
    //     } else {
    //         // console.log('copy/extend');
    //         MainService.RMSTable = $.extend( true, RMSTable, MainService.RMSTable );
    //     }

    //     if( angular.isUndefined( $scope.RMSTable.tableType ) ) {
    //         // console.log('$scope.RMSTable.tableType ');
    //         $http.get('data/table_type.json').success(function (data) {
    //             MainService.RMSTable.tableType = $scope.tableType = data.tableType;
    //             // console.log($scope);
    //             RMSTable.renderTableType();
    //             // console.log(MainService.RMSTable.tableType );
    //         });
    //     }

    //     // RMSTable.renderTableType();

    //     $scope.addTable = function( ev ) {
    //         console.log(ev.currentTarget.id);
    //         var ele = Snap('#' + ev.currentTarget.id),
    //             gTransform = ele.select('.transform').clone(), // after clone, shape have no data
    //             currentZoneNo = MainService.RMSTable.currentSceneNo || MainService.RMSTable.Scene.length - 1,
    //             uid = createTimeStamp(),
    //             newID = 'table_' + uid,
    //             className = [ele.select('.shape').data("shape") + '-'+ele.select('.shape').data("size"), 'table', 'new'];

    //         // console.log(ele.select('.shape').data());
    //         // console.log(currentZoneNo);
    //         // console.log(newID);
    //         // console.log(MainService.RMSTable);

    //         // create new table
    //         var newTable = Snap('#layoutScene_'+currentZoneNo+' #table-group').group(gTransform.attr({
    //         })).attr({
    //             'id': newID,
    //             'class': className.join(' '),
    //             'ng-click': 'tableMouseDown( "'+newID+'" )'
    //         });
    //         MainService.RMSTable.currentActiveElementSnap = newTable;
    //         // console.log($('#'+newID));

    //         // change Paper
    //         newTable.selectAll('g, path, rect').forEach(function(element){
    //             if( element.paper ) {
    //                 element.paper = MainService.RMSTable.svgScene;
    //             }
    //             // console.log(element.paper)
    //         });


    //         // "angularize"
    //         $compile( $('#'+newID) )( $scope );

    //         // normalize the transformation, change to zero and rescale to fit table
    //         gTransform.transform([
    //             'R' + 0, 0, 0,
    //             'S' + 1, 1, 0, 0,
    //             'T' + 0, 0
    //         ].join(','));
    //         MainService.RMSTable.addHoverRect2(gTransform); // add hover area after reset

    //         // tableLabel.getBBox(); // run once before text assign to calibrate, not sure if it is the bug of Snap SVG, after run one, figure seems correct

    //         /*
    //             reset to 0, 0
    //             for initial state, offset it to zero with CX
    //             when save, just put the resultant Tx, Ty is fine, next time when restore, just restore this value
    //         */ 
    //         var eleBBox = gTransform.getBBox(); // eleBBox is affected by scale, define after reset to scale 1:1
    //         console.log(eleBBox);
    //         gTransform.transform([
    //             'R' + 0, eleBBox.cx, eleBBox.cy,
    //             'S' + userConfig.table.display.globalScale, userConfig.table.display.globalScale, eleBBox.cx, eleBBox.cy,
    //             'T' + (eleBBox.cx * (userConfig.table.display.globalScale - 1)), (eleBBox.cy * (userConfig.table.display.globalScale - 1))
    //             // offset is based on:
    //             // scale: 1, offset 0
    //             // scale: 2, offset -12.563(CX)
    //             // scale: 3, offset (CX * 2)
    //             // scale: 4, offset (CX * 3) ... (n-1)
    //         ].join(','));

    //         // add label
    //         var newLabel = 'new';
    //         var tableLabel = gTransform.text(0, 0, '').attr({
    //             'fill': MainService.RMSTable.defaultFontColor,
    //             'font-size': MainService.RMSTable.defaultFontSize,
    //             'font-family': MainService.RMSTable.fontFamily,
    //             // 'text-rendering': 'geometricPrecision',
    //             'text': newLabel
    //         });
    //         $('#current-table-label').val( newLabel );
    //         MainService.RMSTable.centerLabel(gTransform.select(".hoverArea"), tableLabel);

    //         // prepare static data
    //         newTable.data({
    //             uid: uid,
    //             type: ele.select('.shape').data("type"),
    //             shape: ele.select('.shape').data("shape"), 
    //             size: ele.select('.shape').data("size"),
    //             zone: MainService.RMSTable.currentSceneNo, 
    //             label: newTable.select('text').attr('text'),
    //             state: 'new',
    //             status: -1
    //         });

    //         // console.log(newTable.data());

    //         // trigger freeTransform
    //         $scope.tableMouseDown( newID );
    //     }

    //     $scope.tableMouseDown = function() {
    //         // console.log(scope);
    //         var args = Array.prototype.slice.call(arguments).concat([$scope]);
    //         // console.log(args);
    //         tableMouseDown2.apply( $scope, args );

    //         // $rootElement.find('#current-table-label').on('keyup change', function(e){
    //         //     // console.log(MainService.RMSTable.currentActiveElementSnap);
    //         //     // console.log($(this).val());
    //         //     var ele = MainService.RMSTable.currentActiveElementSnap,
    //         //         tableLabel = ele.select('text').attr('text', $(this).val());
    //         //     MainService.RMSTable.centerLabel(ele.select(".hoverArea"), tableLabel);
    //         // });
    //     };
    // });

    // app.directive('tableScene', function ( $compile, $http, $animate, $interval, SocketFactory, MainService, $controller, $timeout, $rootElement, uiLanguage, INFO ) {
    //         return {
    //             restrict: 'E',
    //             // not in use: stretchy-wrapper
    //             // template: '<svg id="svg-canvas" class="" style="visibility: visible; width: 100%; height: 100%;"></svg>',
    //             template: '<div class=""><div class=""><svg id="svg-canvas" style="visibility: visible;"></svg></div></div>',
    //             controller: 'tableController',
    //             link: function (scope, element, attrs) {
    //                 $controller('ToastCtrl',{$scope: scope}); // passing current scope to commmon controller
    //                 // console.log(scope);

    //                 // 743 16:9
    //                 // 988 4:3
    //                 // 
    //                 var newLayout = '<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<!-- Generator: Adobe Illustrator 16.0.5, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->\n<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">\n<svg version=\"1.1\" id=\"layout_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\"\n    width=\"1920\" height=\"1440\" viewBox=\"0 0 1920 1440\" enable-background=\"new 0 0 1920 1440\" xml:space=\"preserve\">\n<g id=\"partition\">\n</g>\n<g id=\"indicator\">\n</g>\n</svg>\n';
    //                 // SocketFactory.emit('saveTableSchema', {"layout":newLayout}, function (r) {
    //                 //     console.log(r);
    //                 // })

    //                 // change dom information, add/delete/remove dom object
    //                 SocketFactory.on('refreshTable', function (data) {
    //                      console.log('refreshTable done');
    //                      // console.log(data);

    //                      // Snap("#svg-canvas").selectAll('.transform text').forEach(function(element, index, array) {
    //                      // element.attr("opacity", 0)
    //                      // })
    //                      initTableScene( data );
    //                      // showMessage("#status-message", scope.RMSTable.messages.table_refresh, "", "warn", 30000);
    //                 });

    //                 // scope.$on('nextZone', function(event, args) {
    //                 //     // console.log('respond to nextZone call');
    //                 //     scope.RMSTable.next();
    //                 //     // console.log(scope.RMSTable.getValue());
    //                 //     // console.log(this);
    //                 //     // console.log(event);
    //                 //     // console.log(args);
    //                 // });

    //                 // scope.$on('prevZone', function(event, args) {
    //                 //     // console.log('respond to prevZone call');
    //                 //     scope.RMSTable.prev();
    //                 // });

    //                 // SocketFactory.on('removeTableFlag', function ( obj ) {
    //                 //     // obj -> table info
    //                 //     // console.log('removeTableFlag'+obj);
    //                 //     var tableInfo = JSON.parse( obj );
    //                 //     try{
    //                 //         findTableEle( tableInfo.tableNo ).select( "." + tableInfo.type ).remove();
    //                 //     }catch(e){
    //                 //         // console.log(e);
    //                 //     }

    //                 // });

    //                 // SocketFactory.on('setTableFlag', function ( obj ) {
    //                 //     // console.log('setTableFlag'+obj);
    //                 //     var tableInfo = JSON.parse( obj );
    //                 //     scope.RMSTable.currentActiveElementSnap = findTableEle( tableInfo.tableNo );
    //                 //     addFlagIcon( "", tableInfo.type );
    //                 // });

    //                 var 
    //                 globalScale = 3.5,
    //                 defaultGraphicHeight = 32,
    //                 defaultGraphicWidth = 32,

    //                 fontWHRatio = 1.8, // approximately by observation 1.75-1.88, no definite data found
    //                 offsetCanvasH = 25, // from left padding
    //                 offsetCanvasV = -64, // from top bar
    //                 sceneWidth = 1920,
    //                 sceneHeight = 1080,

    //                 // basic known settings, can be dyanmic later once the formula established without problem
    //                 defaultDesignScreenW = 1024,
    //                 defaultDesignScreenH = 768,
    //                 defaultDesignContentH = 653,
    //                 defaultViewboxW = 1920,
    //                 defaultViewboxH,
    //                 // defaultViewboxH = based on caculation

    //                 // table color
    //                 legendColor = {
    //                     available: "#2B2B8C",
    //                     inputOrder: "#EC155A",
    //                     using: "#0D752D",
    //                     printedBill: "#F16523",
    //                     billed: "#8D2289"
    //                 },
    //                 statusCode = userConfig.statusCode,
    //                 // statusCode = [];
    //                 // statusCode[0] = "available";
    //                 // statusCode[1] = "inputOrder";
    //                 // statusCode[2] = "using"; // => 
    //                 // statusCode[3] = "printedBill"; // changed to printed
    //                 // statusCode[4] = "billed"; // => changed to billed

    //                 buffetStatusCode = userConfig.buffetStatusCode;
    //                 // buffetStatusCode = [];
    //                 // buffetStatusCode[0] = "available";
    //                 // buffetStatusCode[1] = "nearlyDoneReminder";
    //                 // buffetStatusCode[2] = "nearlyDone";
    //                 // buffetStatusCode[3] = "timesUp";

    //                 function showEditMode() {
    //                     showMessage("#edit-mode", scope.RMSTable.editMode + " mode", "", "warn", 30000);
    //                 }

    //                 function isTableFree( element ) {
    //                     // console.log(element.parent().data());
    //                     if (element.parent().data("status") != 0) {
    //                         showMessage("#status-message", scope.RMSTable.messages.table_in_use, "", "warn", 30000);
    //                         return false;
    //                     }
    //                     return true;
    //                 }

    //                 function addFlagIcon( element, flag ) {
    //                     if( scope.RMSTable.currentActiveElementSnap === "" | scope.RMSTable.currentActiveElementSnap === undefined ) {
    //                         showMessage("#status-message", scope.RMSTable.messages.selectElementFirst, "", "warn", 30000);
    //                         return;
    //                     }

    //                     if( scope.RMSTable.currentActiveElementSnap.select( "."+flag ) != null ) {
    //                         return;
    //                     }

    //                     switch( flag ) {
    //                         case "vip":
    //                             $http.get('assets/svg/star118.svg').success(function (f) {
    //                             // Snap.load( "assets/svg/star118.svg", function ( f ) {
    //                                 // console.log(arguments);
    //                                 f = Snap.parse(f);
    //                                 var cssClass = [];
    //                                 cssClass.push( "flag" );
    //                                 cssClass.push( flag );

    //                                 try{
    //                                     var icon = f.select( "svg" );
    //                                     icon.attr( { width: 8, height: 8, class: cssClass.join(" "), fill: "gold", "filter" : "url(#dropShadowLight)" });

    //                                     // reposition it if there is another flag
    //                                     if( scope.RMSTable.currentActiveElementSnap.select(".flag") != null ) {
    //                                         var offsetX = scope.RMSTable.currentActiveElementSnap.selectAll(".flag").length * 8;
    //                                         icon.attr( { x: offsetX });
    //                                     }
    //                                     var element = scope.RMSTable.currentActiveElementSnap;
    //                                     element.select(".transform").append( icon );
    //                                 }catch(e){
    //                                     console.log(e);
    //                                 }
    //                             });
    //                             break;
    //                         case "water":
    //                             $http.get('assets/svg/water42.svg').success(function (f) {
    //                             // Snap.load( "assets/svg/water42.svg", function ( f ) {
    //                                 f = Snap.parse(f);
    //                                 var cssClass = [];
    //                                 cssClass.push( "flag" );
    //                                 cssClass.push( flag );
    //                                 // console.log(arguments);
    //                                 try{
    //                                     var icon = f.select( "svg" );
    //                                     icon.attr( { width: 8, height: 8, class: cssClass.join(" "), fill: "blue", "filter" : "url(#dropShadowLight)" });

    //                                     // reposition it if there is another flag
    //                                     if( scope.RMSTable.currentActiveElementSnap.select(".flag") != null ) {
    //                                         var offsetX = scope.RMSTable.currentActiveElementSnap.selectAll(".flag").length * 8;
    //                                         icon.attr( { x: offsetX });
    //                                     }
    //                                     var element = scope.RMSTable.currentActiveElementSnap;
    //                                     element.select(".transform").append( icon );
    //                                 }catch(e){
    //                                     console.log(e);
    //                                 }
    //                             })
    //                             break;
    //                         case "service":
    //                             $http.get('assets/svg/black302.svg').success(function (f) {
    //                             // Snap.load( "assets/svg/black302.svg", function ( f ) {
    //                                 f = Snap.parse(f);
    //                                 var cssClass = [];
    //                                 cssClass.push( "flag" );
    //                                 cssClass.push( flag );
    //                                 // console.log(arguments);
    //                                 try{
    //                                     var icon = f.select( "svg" );
    //                                     icon.attr( { width: 8, height: 8, 
    //                                         class: cssClass.join(" "), 
    //                                         fill: "greenyellow", 
    //                                         "filter" : 
    //                                         "url(#dropShadowLight)" 
    //                                     });

    //                                     // reposition it if there is another flag
    //                                     if( scope.RMSTable.currentActiveElementSnap.select(".flag") != null ) {
    //                                         var offsetX = scope.RMSTable.currentActiveElementSnap.selectAll(".flag").length * 8;
    //                                         icon.attr( { x: offsetX });
    //                                     }
    //                                     var element = scope.RMSTable.currentActiveElementSnap;
    //                                     element.select(".transform").append( icon );
    //                                 }catch(e){
    //                                     console.log(e);
    //                                 }
    //                             })
    //                             break;
    //                         case "bill":
    //                             $http.get('assets/svg/money2.svg').success(function (f) {
    //                             // Snap.load( "assets/svg/money2.svg", function ( f ) {
    //                                 f = Snap.parse(f);
    //                                 var cssClass = [];
    //                                 cssClass.push( "flag" );
    //                                 cssClass.push( flag );
    //                                 // console.log(arguments);
    //                                 try{
    //                                     var icon = f.select( "svg" );
    //                                     icon.attr( { width: 8, height: 8, class: cssClass.join(" "), fill: "green", "filter" : "url(#dropShadowLight)" });

    //                                     // reposition it if there is another flag
    //                                     if( scope.RMSTable.currentActiveElementSnap.select(".flag") != null ) {
    //                                         var offsetX = scope.RMSTable.currentActiveElementSnap.selectAll(".flag").length * 8;
    //                                         icon.attr( { x: offsetX });
    //                                     }
    //                                     var element = scope.RMSTable.currentActiveElementSnap;
    //                                     element.select(".transform").append( icon );
    //                                 }catch(e){
    //                                     console.log(e);
    //                                 }
    //                             })
    //                             break;
    //                         default:
    //                             break;
    //                     }
    //                 }

    //                 function removeFlagIcon( flag ) {
    //                     if( scope.RMSTable.currentActiveElementSnap != "") {
    //                         showMessage("#status-message", scope.RMSTable.messages.selectElementFirst, "", "warn", 30000);
    //                         return;
    //                     }
    //                     var element = scope.RMSTable.currentActiveElementSnap;
    //                     try{
    //                         element.select( "." + flag ).remove();
    //                         // repositionFlagIcon( element );
    //                     }catch(e){
    //                         // console.log(e);
    //                     }
    //                 }

    //                 function findTableEle(tableNo) {
    //                     var tmp;
    //                     var svgScene = Snap("#svg-canvas");
    //                     svgScene.selectAll(".tableGroup .transform").forEach(function (element, index, array) {
    //                         // console.log(tableNo);
    //                         // console.log(element.select( "text" ).attr("text"));
    //                         if (element.select("text").attr("text") === tableNo) {
    //                             // console.log("found");
    //                             tmp = element.parent();
    //                         }
    //                     });
    //                     // console.log(tmp);
    //                     return tmp;
    //                 }

    //                 scope.RMSTable = {
    //                     defaultFontColor : "white",
    //                     defaultFontSize : userConfig.table.display.defaultFontSize,
    //                     fontFamily : "\"Microsoft JhengHei\", STXihei, Helvetica, Arial, sans-serif",
    //                     currentSceneNo : null,
    //                     editMode : "transfer", // default: edit, order
    //                     messages : scope.ui,
    //                     groupEdit : false,
    //                     svgScene : Snap("#svg-canvas")
    //                 }

    //                 scope.RMSTable.renderPattern = function ( baseShape ) {
    //                     var layoutPattern = baseShape.clone().attr({
    //                         "id": "layoutPattern_" + this.currentSceneNo,
    //                         "stroke": "#000000",
    //                         "stroke-dasharray": "20 10"
    //                     });

    //                     layoutPattern.attr({
    //                         fill: Snap("#cross-line-pattern")
    //                     });
    //                 };

    //                 // scene operation
    //                 scope.RMSTable.changeZone = function (sceneNo) {
    //                     console.log('change scene');
    //                     // scene layout = zone in old version
    //                     // whole screen is sceen -> layout scene (zone)
    //                     // next or prev scene or change to scene no.
    //                     // console.log("reset scene");
    //                     removeAllHandles();
    //                     scope.cancelTransfer('reset');
    //                     // showMessage("#status-message", "", "", "warn", 30000);

    //                     // this.currentZoneLayoutObj = this.sceneObjArr[ sceneNo ];
    //                     var currentZoneLayoutObj = this.Scene[sceneNo];
    //                     // console.log(sceneNo);
    //                     // init title position, only for first time running
    //                     // this.svgScene.select("#title text").attr("text", currentZoneLayoutObj.name);

    //                     // hide all scene(layout) if there is any
    //                     var allLayout = $(".layoutScene");
    //                     if (allLayout.length != 0) {
    //                         // if zone rendered already, just hide and show
    //                         // show specified sceneNo if the scene is already exist/rendered
    //                         allLayout.hide();
    //                     }

    //                     var $loadScene = $("#layoutScene_" + sceneNo);
    //                     if ($loadScene.length != 0) {
    //                         // if zone rendered already, just hide and show
    //                         // show specified sceneNo if the scene is already exist/rendered
    //                         $loadScene.show();
    //                     } else {
    //                         // if zone does not exist, render it
    //                         // render layout scene if it is not yet exist
    //                         console.log('sceneNo ' + sceneNo);
    //                         console.log('render new Layout');
    //                         this.renderLayout( scope.RMSTable.Scene[sceneNo].layout, sceneNo);
    //                     }

    //                     // check if table group exist or not, let the incoming refresh function if not exist
    //                     if (this.svgScene.select('#layoutScene_'+sceneNo+' #table-group') === null) {
    //                         this.renderTable(sceneNo);
    //                     }
    //                 };

    //                 scope.RMSTable.next = function () {
    //                     var totalScene = this.Scene.length;
    //                     if(this.currentSceneNo == totalScene - 1) {
    //                         this.currentSceneNo = 0;
    //                     } else {
    //                         this.currentSceneNo = this.currentSceneNo + 1;
    //                     }
    //                     this.changeZone(this.currentSceneNo);
    //                 }

    //                 scope.RMSTable.prev = function () {
    //                     var totalScene = this.Scene.length;
    //                     if(this.currentSceneNo == 0) {
    //                         this.currentSceneNo = totalScene - 1;
    //                     } else {
    //                         this.currentSceneNo = this.currentSceneNo - 1;
    //                     }
    //                     this.changeZone(this.currentSceneNo);
    //                 }

    //                 scope.RMSTable._init_edit_panel = function() {
    //                     var boxWidth = $(".edit-panel-boxes").outerWidth();
    //                     var alignBoxBBox = this.svgScene.select( "#textArea" ).getBBox();
    //                     var currentSceneScaleW = $( "#svg-canvas" ).outerWidth();
    //                     var currentSceneScaleH = $( "#svg-canvas" ).outerHeight();
    //                     var newPos_X = currentSceneScaleW * alignBoxBBox.x / sceneWidth;
    //                     var newPos_Y = currentSceneScaleH * alignBoxBBox.y / sceneHeight;

    //                     // to align to right hand side instead, plus the width difference can do, ie
    //                     var alignBoxRelativeW = alignBoxBBox.w / sceneWidth * currentSceneScaleW;
    //                     var widthDiff = alignBoxRelativeW - boxWidth;
    //                     newPos_X = newPos_X + widthDiff;

    //                     $(".edit-panel-boxes").css( { "left": newPos_X, "top": newPos_Y } );
    //                 };

    //                 function findModified() {
    //                     MainService.RMSTable.svgScene.selectAll('.table').forEach(function(ele){
    //                         // console.log(ele.data('modified'))
    //                         if(ele.data('modified')) {
    //                             return true;
    //                         }
    //                     })
    //                 }

    //                 // ele: transformation group of the element
    //                 // eg. node with ID => node with ".shape" class
    //                 scope.RMSTable.makeElementActive = function( ele ){
    //                     // Snap('#svg-canvas').select('#table_1418785047372').data()
    //                     if( findModified() ) {
    //                         scope.saveTable();
    //                     }

    //                     // too much parent and child dependency, make it confuse and hard to be generic

    //                     // console.log(ele.parent().data());
    //                     // console.log(ele.data("id"));
    //                     // console.log(ele.selectAll("path"));
    //                     // console.log(ele);

    //                     // remove all previous added selection indication box
    //                     try {
    //                         this.svgScene.select(".selected-box").remove();
    //                     } catch(e){
    //                     }

    //                     // update current selected item to global variable
    //                     try {
    //                         // console.log("try 2-1");
    //                         this.currentActiveElement = ele.parent().data("id");
    //                         this.currentActiveElementSnap = ele.parent();
    //                         this.currentTableNo = ele.select("text").attr("text");
    //                     } catch(e){
    //                         // console.log("e 2-1");
    //                         try{
    //                             // console.log("try 2-2");
    //                             this.currentActiveElement = ele.attr("id");
    //                             this.currentActiveElementSnap = ele;
    //                         }catch(e){
    //                             // console.log("e 2-2");
    //                             this.currentActiveElement = ele.attr("id");
    //                             this.currentActiveElementSnap = ele;
    //                         }
    //                     }

    //                     // add active indication box to current selected element and update active element ID to variable RMS.currentActiveElement
    //                     var eleBBox = ele.getBBox();
    //                         // boxSVGStr = "<rect class='selected-box' x='0' y='0' width='"+defaultGraphicWidth+"' height='"+defaultGraphicHeight+"' fill='none' style='stroke: #f00; stroke-width: 1.3px; stroke-dasharray: 5 2'></rect>";
    //                     // console.log(eleBBox);
    //                     // ele.append( Snap.parse( boxSVGStr ) );

    //                     // update label-box to reflect current selected box to update
    //                     try{
    //                         var currentLabel = ele.select("text").attr("text");
    //                         $("#label-box").val(currentLabel);
    //                         // console.log(currentLabel);

    //                         // console.log( ele.data( "shape" ) );
    //                         // update size list to reflect current shape and size
    //                         // shape-name
    //                         var shape = ele.parent().data( "shape" );
    //                         var size = ele.parent().data( "size" );
    //                         var shapeArr = getAllShape(), optionList = "";
    //                         for(var i = 0; i < shapeArr.length; i++ ) {
    //                             optionList += "<option value=" + shapeArr[i].toLowerCase() + ">" + shapeArr[i] + "</option>";
    //                         }
    //                         $(".shape-name").html(optionList).val( shape );
    //                         // console.log(optionList);
    //                         // console.log(getShapeAllSize( shape ));

    //                         // shape-size
    //                         var sizeArr = getShapeAllSize( shape ), optionList = "";
    //                         for(var i = 0; i < sizeArr.length; i++ ) {
    //                             optionList += "<option value=" + sizeArr[i] + ">" + sizeArr[i] + " people</option>";
    //                         }
    //                         $(".shape-size").html(optionList).val( size );
    //                         // console.log(optionList);
    //                         // console.log(size);
    //                     }catch(e){
    //                         // console.log(e);
    //                         $("#label-box").val("");
    //                     }

    //                     // reinit the boxes position
    //                     // this._init_edit_panel();

    //                     // if previous = current, remove selection box only
    //                 };

    //                 scope.RMSTable.centerLabel = function (attachEle, textEle) {
    //                     // console.log(attachEle.selectAll("rect"));
    //                     // console.log(textEle.parent().parent().attr('id'));

    //                     // textEle.parent().parent().attr("opacity", 0); // parent box
    //                     textEle.attr("opacity", 0); // text

    //                     var textBBox = textEle.getBBox();
    //                     var attachBBox = attachEle.getBBox();

    //                     // $interval(function(){
    //                     //     console.log(textBBox);
    //                     // }, 5000);


    //                     // console.log(textBBox);
    //                     // console.log(attachBBox);

    //                     // var attachW = Snap("#svg-canvas").select("#table_1418785047317 rect").attr("width");
    //                     // var attachH = Snap("#svg-canvas").select("#table_1418785047317 rect").attr("height");
    //                     // console.log(Snap("#svg-canvas").select("#table_1418785047317 rect").attr("width"));
    //                     // console.log(textEle.getBBox());

    //                     // if (attachEle.parent().parent().data("scale_X") != undefined) {
    //                     //     var scale_X = attachEle.parent().parent().data("scale_X");
    //                     //     var scale_Y = attachEle.parent().parent().data("scale_Y");
    //                     // } else {
    //                     //     var scale_X = attachEle.parent().data("scale_X");
    //                     //     var scale_Y = attachEle.parent().data("scale_Y");
    //                     // }

    //                     // console.log(attachEle.parent().parent());
    //                     // console.log("attachBBox.w " + attachBBox.w);
    //                     // console.log("label_X " + label_X);
    //                     // not accurate // plus half of the height manually
    //                     // var label_X = ( (attachBBox.w - textBBox.w)/2 );
    //                     // var label_Y = ( (attachBBox.h + textBBox.h)/2 );
    //                     // console.log('textBBox.w ' + textBBox.w);
    //                     // console.log('textBBox.h ' + textBBox.h);
    //                     // var label_X = ((attachW - textBBox.w) / 2);

    //                     setTimeout(function(){
    //                         var label_X = ((attachBBox.w - textBBox.w) / 2);
    //                         var label_Y = ((textBBox.h * 2 - attachBBox.h/2)); // not sure why, just follow the numerical value to make formula

    //                         // find the space between top of text and actual bounding box (text height from its zero Y is offset ) (O)
    //                         // textBBox.h/2 is the half of text height with space above (A)
    //                         // actual center without space from attachBBox is (attachBBox.h - textBBox.h)/2 (B)
    //                         // so the space A - B, and the actual offset is O + A - B
    //                         // textBBox.h + textBBox.h/2 - (attachBBox.h - textBBox.h)/2  
    //                         // simplified formula is => textBBox.h + textBBox.h/2 - attachBBox.h/2 + textBBox.h/2  

    //                         if( textEle.parent().parent().attr('id') === "table_1418785047429" ) {
    //                             // console.log(textBBox);
    //                             // console.log(attachBBox);
    //                             // console.log(textBBox.h);
    //                             // console.log((attachBBox.h - textBBox.h)/2);
    //                             // console.log(textBBox.h + textBBox.h/2 - (attachBBox.h - textBBox.h)/2);
    //                             // 25 + 18/2 - (25 - 18)/2 = 34 - 7/2 = 31.5
    //                             // console.log(textBBox.h * 2 - attachBBox.h/2 );
    //                             // console.log(attachBBox.h - textBBox.h/2);

    //                             // 18 + 18/2 = 9 - (30 - 18)/2 = 6 18 + 3 = 21
    //                         }

    //                         // label_Y still is not correct in calculation @ 2015/11/03
    //                         textEle.attr({ "x": label_X, "y": 14 });
    //                         // console.log('label_X', label_X);
    //                         // console.log('label_Y', label_Y);
    //                         // textEle.parent().parent().attr("opacity", 1);
    //                         textEle.attr("opacity", 1);
    //                     }, 1)

    //                     // hard code for demo
    //                     // textEle.attr({ "x": 10, "y": 21 }); // 12 / 21 for 1 character, 8 / 21 for 2 characters
    //                 }; // constraint proportion

    //                 scope.RMSTable.addHoverRect2 = function (element, index, array) {
    //                     // console.log('addHoverRect2');
    //                     var referencePoint = (this.ref != undefined && this.ref != "") ? this.ref : "absolute";
    //                     var BBox = element.getBBox();
    //                     // console.log(BBox);

    //                     // $timeout(function(){
    //                     //     console.log(BBox);
    //                     // }, 1000);


    //                     // var tmp = 0;
    //                     // var renderTrial = $interval(function(){
    //                     //     // console.log(tmp);
    //                     //     // console.log(BBox);
    //                     //     if( BBox.width != 0 ) {
    //                     //         $interval.cancel(renderTrial);
    //                     //     } else {
    //                     //         console.log(element);
    //                     //         $interval.cancel(renderTrial);
    //                     //     }
    //                     //     tmp = tmp + 10;
    //                     // }, tmp);

    //                     // console.log('element BBox');
    //                     // console.log(element.getBBox());

    //                     // console.log("----------------------------------------");
    //                     // console.log( element.attr("id") );
    //                     // console.log(this);
    //                     // console.log(this.ref);
    //                     // console.log("----------------------------------------");

    //                     function renderHoverRect() {
    //                         switch (referencePoint) {
    //                             default:
    //                             case "absolute":
    //                                 // for non-text element
    //                                 var pos_X = BBox.x;
    //                                 var pos_Y = BBox.y;
    //                                 break;
    //                             case "relative":
    //                                 // for text element (since its X and Y is relative to , like Google Slide, they are also using transform group for transformation and leave x and y untouched for relative calculation)
    //                                 var pos_X = 0;
    //                                 var pos_Y = 0;
    //                                 break;
    //                         }


    //                         if( element.parent().attr('id') === "table_1418785047317" ) {
    //                             // console.log('element bbox');
    //                             // console.log(BBox);
    //                         }

    //                         // don't count any scale or things, it will make things complex, scale should be performed after object creation
    //                         element.prepend(element.rect(pos_X, pos_Y, BBox.w, BBox.h).attr({
    //                             "opacity": "0",
    //                             "class": "hoverArea"
    //                         }));
    //                         delete this.ref;
    //                     }
    //                     renderHoverRect();

    //                     // switch (referencePoint) {
    //                     //     default:
    //                     //     case "absolute":
    //                     //         // for non-text element
    //                     //         var pos_X = BBox.x;
    //                     //         var pos_Y = BBox.y;
    //                     //         break;
    //                     //     case "relative":
    //                     //         // for text element (since its X and Y is relative to , like Google Slide, they are also using transform group for transformation and leave x and y untouched for relative calculation)
    //                     //         var pos_X = 0;
    //                     //         var pos_Y = 0;
    //                     //         break;
    //                     // }

    //                     // if( element.attr('class') === "table_1418785047317" ) {
    //                     //     // console.log('element bbox');
    //                     //     // console.log(BBox);
    //                     // }

    //                     // // don't count any scale or things, it will make things complex, scale should be performed after object creation
    //                     // element.prepend(element.rect(pos_X, pos_Y, BBox.w, BBox.h).attr({
    //                     //     "opacity": "0",
    //                     //     "class": "hoverArea"
    //                     // }));
    //                     // delete this.ref;
    //                 };

    //                 /*
    //                     stick to 4:3
    //                     var fourToThreeRatio = 1920/1024;
    //                     var fourToThreeH = 768 * fourToThreeRatio;
    //                     var fourToThreeW = 1024 * fourToThreeRatio;
    //                     scope.RMSTable._viewBox = [0, 0, 1920, 1080];
    //                     scope.RMSTable._viewBox = [0, 0, fourToThreeW, fourToThreeH];
    //                 */

    //                 /*
    //                     for a screen with 1024x768, the visible area for md-content(table-scene) is 1024 / 653
    //                     the SVG design is based on 1920 so, the SVG height is
    //                     1024 / 653 = 1920 / h
    //                     h = 1920 * 653 / 1024 = 1920 * table-scene height / table-scene width

    //                     on the other hand, when screen available area is bigger or smaller, the SVG viewport will render differently relative to the screen available width/height
    //                     to maintain same aspect ratio for the viewbox while changing it to fit the screen width/height

    //                     all design is based on 1920 x 1080 16:9 and 4:3 is based on 1920 x 1440
    //                     so base ratio is 1920*
    //                 */
    //                 var screenRatioW = $(window).width() / 1024; // 1920 is layout, normally it design for 1024, it is 1024, ratio = 1
    //                 // screenRatioW = 1

    //                 // elastic viewbox

    //                 // based on default design screen w/h and content H, the viewboxH is 1920 x 1224
    //                 if( angular.isUndefined(scope.RMSTable.defaultViewboxH) ) {
    //                     defaultViewboxH = scope.RMSTable.defaultViewboxH = sceneWidth * defaultDesignContentH / defaultDesignScreenW;
    //                 }

    //                 // for background image (need to further fine tune)
    //                 var elasticRatio = defaultViewboxW / $('table-scene').width();
    //                 var elasticH =  Math.round( $('table-scene').height() ) * elasticRatio;
    //                 var elasticW =  Math.round( $('table-scene').width() ) * elasticRatio;

    //                 // scene area
    //                 var areaRatio = $('table-scene').width() / $('table-scene').height();
    //                 // var areaRatio = $(window).width() / $(window).height();
    //                 if( screen.width < screen.height ) {
    //                     scope.RMSTable._viewBox = [0, 0, defaultViewboxW , defaultViewboxW / areaRatio ];
    //                 } else {
    //                     scope.RMSTable._viewBox = [0, 0, defaultViewboxH * areaRatio , defaultViewboxH ];
    //                 }

    //                 scope.tableMouseDown = function() {
    //                     // console.log(scope);
    //                     var args = Array.prototype.slice.call(arguments).concat([scope]);
    //                     tableMouseDown2.apply( scope, args );
    //                 };

    //                 scope.RMSTable.renderSVGLayout = function (layout, sceneNo) {
    //                     console.log('scope.RMSTable.renderSVGLayout');
    //                     // for pure svg layout
    //                     var layoutGroup = this.svgScene.group(Snap.parse(layout).selectAll("g")).attr({
    //                         // "id": "layout",
    //                         "class": "layout"
    //                     });

    //                     // change default layer ID to prevent ID collision in DOM
    //                     // layoutGroup.select("#partition").attr({
    //                     //     "id": "partition",
    //                     //     "class": "partition"
    //                     // });

    //                     // layoutGroup.select("#indicator").attr({
    //                     //     "id": "indicator",
    //                     //     "class": "indicator"
    //                     // });

    //                     // layoutGroup.select("#basic").attr({
    //                     //     "id": "basic_" + sceneNo,
    //                     //     "class": "basic"
    //                     // });

    //                     // var basicShape = layoutGroup.select("#basic_" + sceneNo + " polygon").attr({
    //                     // var basicShape = layoutGroup.select("#basic_" + sceneNo + " rect").attr({
    //                     //     "id": "layoutShape_" + sceneNo,
    //                     //     "class": "layoutShape"
    //                     // });

    //                     // this.renderPattern(basicShape); // use basucShape as a reference layout to create pattern

    //                     // <g id=\"basic\">\n<image overflow=\"visible\" width=\"1920\" height=\"1440\" xlink:href=\"data/background.png\">\n <rect x=\"0\" fill=\"#eeeeee\" width=\"100%\" height=\"100%\"/>\n </image></g>

    //                     // background for all scenes
    //                     var bg = this.svgScene.image('data/background.png', 0, 0, 1920, 1440).attr({
    //                         "preserveAspectRatio": "xMidYMid meet"
    //                         }),
    //                         basic = this.svgScene.group(bg).attr({
    //                             "id": "basic"
    //                         });

    //                     // resize background image
    //                     if( typeof this.bgNativeRatio === 'undefined' ) {
    //                         this.bgNativeRatio = bg.attr('width') / bg.attr('height');
    //                     };
    //                     // console.log(this.svgScene.select('image').attr('height'));
    //                     bg.attr({
    //                         width: elasticW * screenRatioW,
    //                         height: elasticW * screenRatioW / this.bgNativeRatio
    //                     });

    //                     // add working helper for current screen available area
    //                     // since viewport is based on 1920 x 1440, so a 1024 x 768 monitor is mapped to 1920...
    //                     // thus W * 1.875 and H * 1.875

    //                     var helperRect = this.svgScene.rect(0,0,1920,653*1.875).attr({
    //                         fill:'none', stroke: 'red', 'stroke-dasharray': '12px, 12px', 'stroke-width': '8px', opacity: 0.2
    //                     }),

    //                     helperText = this.svgScene.text(1050, 1180, '1024 x 768 Monitor Area').attr({
    //                         fill: 'red', 
    //                         style: "font-size: 70px; font-weight: bold; font-family: 'Microsoft JhengHei', STXihei, Helvetica, Arial, sans-serif; opacity: 0.7; stroke: black; stroke-width: 2px;"
    //                     });

    //                     var vwHelper = this.svgScene.group(helperRect, helperText).attr({
    //                         class: 'viewport-helper ng-hide'
    //                     });

    //                     // add grid helper
    //                     var p = this.svgScene.path("M 50 0 L 0 0 0 50").attr({
    //                             fill: "none",
    //                             stroke: "#333",
    //                             strokeWidth: 0.5
    //                         }).pattern(0, 0, 50, 50);

    //                     var gridHelper = this.svgScene.rect(0, 0, '100%', '100%').attr({
    //                         id: 'grid-helper',
    //                         fill: p,
    //                         class: 'ng-hide'
    //                     });

    //                     var helper = this.svgScene.group(gridHelper, vwHelper).attr({
    //                         id: 'helper-container'
    //                     });

    //                     basic.insertBefore( this.svgScene.select("#floorGroup") );
    //                     helper.insertAfter( basic );

    //                     this.svgScene.select("#floorGroup").group(layoutGroup).attr({
    //                         "id": "layoutScene_" + sceneNo,
    //                         "class": "layoutScene"
    //                     });


    //                     // this.svgScene.attr({
    //                     //     "width": 1920,
    //                     //     "height": this.sceneHeight,
    //                     //     "viewBox": "0 0 1920 " + this.sceneHeight // 16:9
    //                     // });

    //                     // this.workArea = {
    //                     //     w: this.svgScene.select("#work-canvas").getBBox().w,
    //                     //     h: this.svgScene.select("#work-canvas").getBBox().h
    //                     // }

    //                     // init title position, only for first time running
    //                     // var sceneTitle = currentZoneLayoutObj.name;
    //                     // svgScene.select("#title text").attr("text", sceneTitle);

    //                     // console.log(Snap.path.toRelative(layout));
    //                     // var test = Snap.path.toRelative(layout);

    //                     // console.log(layout);

    //                     this.layoutArea = {
    //                         w: layoutGroup.getBBox().width,
    //                         h: layoutGroup.getBBox().height
    //                     }

    //                     // offset_X = (this.workArea.w - this.layoutArea.w) / 2 + offsetCanvasH;
    //                     // offset_Y = (this.workArea.h - this.layoutArea.h) / 2 + offsetCanvasV;
    //                     // not autmatically offset anymore
    //                     // offset_X = 0;
    //                     // offset_Y = 0;

    //                     // console.log(offsetCanvasV);
    //                     // console.log((this.workArea.h - this.layoutArea.h) / 2);
    //                     // console.log(offset_Y);
    //                     // console.log(Snap("#svg-canvas").select("#layout_1").getBBox());
    //                     // console.log(layoutGroup.getBBox());
    //                     // console.log(this.workArea);
    //                     // console.log(this.layoutArea);
    //                     // console.log(offset_X);
    //                     // console.log(offset_Y);

    //                     // auto resize the layout to fit the screen if there is not saved transformation for this layout

    //                     // layout is not in used now
    //                     // eleMatrix = new Snap.Matrix();
    //                     // eleMatrix.translate(offset_X, offset_Y);
    //                     // eleMatrix.scale(this._viewBox[2] / this.layoutArea.w, this._viewBox[2] / this.layoutArea.w);
    //                     // layoutGroup.transform(eleMatrix);

    //                     // console.log(this.layoutArea);
    //                     // console.log(this._viewBox );
    //                     // console.log(this._viewBox[2] );

    //                     $(window).on({'resize': function(){
    //                         // console.log("I am doing my job");

    //                         /*
    //                             var currentLayout = scope.RMSTable.svgScene.select("#layout_" + scope.RMSTable.currentSceneNo);

    //                             // need to calculate with the 1/1 scale instead of scaled width
    //                             var layoutMatrix = currentLayout.attr("transform").totalMatrix.split();

    //                             var layoutArea = {
    //                                 w: currentLayout.getBBox().w / layoutMatrix.scalex,
    //                                 h: currentLayout.getBBox().h / layoutMatrix.scaley
    //                             }
    //                             // console.log(layoutGroup);
    //                             // console.log(layoutMatrix);
    //                             // console.log(layoutArea);

    //                             eleMatrix = new Snap.Matrix();
    //                             eleMatrix.scale(scope.RMSTable._viewBox[2] / layoutArea.w, scope.RMSTable._viewBox[2] / layoutArea.w);
    //                             currentLayout.transform(eleMatrix);
    //                         */

    //                             // maintain same aspect ratio for the viewbox while changing it to fit the screen width/height
    //                             var screenRatioW = $(window).width() / 1024; // 1920 is layout, normally it design for 1024, it is 1024, ratio = 1

    //                             // elastic viewbox
    //                             var elasticRatio = defaultViewboxW / $('table-scene').width();
    //                             var elasticH =  Math.round( $('table-scene').height() ) * elasticRatio;
    //                             var elasticW =  Math.round( $('table-scene').width() ) * elasticRatio;

    //                             // this method keep the same aspect ratio
    //                             // var _viewBox = [0, 0, elasticW * screenRatioW, elasticH * screenRatioW ];

    //                             // scene area
    //                             var areaRatio = $('table-scene').width() / $('table-scene').height();
    //                             // var areaRatio = $(window).width() / $(window).height();
    //                             if( screen.width < screen.height ) {
    //                                 _viewBox = [0, 0, defaultViewboxW , defaultViewboxW / areaRatio ];
    //                             } else {
    //                                 _viewBox = [0, 0, defaultViewboxH * areaRatio , defaultViewboxH ];
    //                             }

    //                             // resize background image
    //                             if( typeof scope.RMSTable.bgNativeRatio === 'undefined' ) {
    //                                 scope.RMSTable.bgNativeRatio = scope.RMSTable.svgScene.select('image').attr('width') / scope.RMSTable.svgScene.select('image').attr('height');
    //                             }
    //                             // console.log(scope.RMSTable.svgScene.select('image').attr('height'));
    //                             scope.RMSTable.svgScene.select('image').attr({
    //                                 width: elasticW * screenRatioW,
    //                                 height: elasticW * screenRatioW / scope.RMSTable.bgNativeRatio
    //                             })

    //                             // console.log('screenRatio ' + screenRatio);
    //                             // console.log('elasticRatio ' + elasticRatio);

    //                             MainService.RMSTable._viewBox = scope.RMSTable._viewBox = _viewBox;
    //                             scope.RMSTable.svgScene.attr({
    //                                 "viewBox": _viewBox.join(" ")
    //                             });
    //                         }
    //                     });
    //                 }

    //                 scope.RMSTable.renderLayout = function (layout, sceneNo) {
    //                     console.log('scope.RMSTable.renderLayout');
    //                     // var currentZoneLayoutObj = this.Scene[sceneNo];
    //                     // console.log(this == $scope.RMSTable); // true
    //                     // console.log(this.Scene);
    //                     // console.log(sceneNo);

    //                     this.sceneHeight = 950.4;
    //                     var paper = this.svgScene.paper,
    //                         screenRatio = 16/9,
    //                         _height = 100;

    //                     this.svgScene.attr({
    //                         // "width": "screen.width",
    //                         // "height": screen.width / screenRatio,
    //                         "width": "100%",
    //                         "height": _height + "%",
    //                         "viewBox": this._viewBox.join(" "),
    //                         "preserveAspectRatio": "xMinYMin meet"
    //                     });

    //                     var paperSize = {
    //                             w: paper.node.clientWidth,
    //                             h: paper.node.clientHeight
    //                         };

    //                     // this.svgScene.prepend(this.svgScene.rect(0, 0, 1920, this.sceneHeight).attr({ id: "work-canvas", fill: "transparent" }));

    //                     this.renderSVGLayout(layout, sceneNo);

    //                     // console.log(Snap('#svg-canvas').select('image').getBBox());
    //                     var imgBBox = this.svgScene.select('image').getBBox();
    //                     this.bgNativeRatio = imgBBox.w / imgBBox.h;

    //                     console.log('current scene no ' + sceneNo);
    //                     this.renderTable(sceneNo);
    //                     try {
    //                     } catch (e) {
    //                         console.log(e);
    //                         console.log("Please re-confirm if provided layout is a SVG string.");
    //                     }
    //                 };

    //                 scope.RMSTable.renderTable = function (sceneNo) {
    //                     console.log('scope.RMSTable.renderTable');
    //                     // console.log("render table");
    //                     var currentZoneLayoutObj = this.Scene[this.currentSceneNo];
    //                     //console.log(currentZoneLayoutObj);
    //                     // create table group to group all table object inside
    //                     // if( masterTableEleGroup = this.scene.select( "tableGroup_" + sceneNo ) === null ) {

    //                     // console.log(this.svgScene.getBBox());

    //                     var masterTableEleGroup = this.svgScene.select("#layoutScene_" + sceneNo).group().attr({
    //                         "id": "table-group",
    //                         "class": "tableGroup"
    //                     });

    //                     var tableF, tableNode, eleMatrix;

    //                     // $floorPlanCanvas = $("#floorGroup");
    //                     // console.log('all table object in current scene');
    //                     // console.log(currentZoneLayoutObj.memberObject);
    //                     for (var i = 0; i < currentZoneLayoutObj.memberObject.length; i++) {
    //                         // if in case, want the shape completely free from global scale
    //                         // if( currentZoneLayoutObj.memberObject[i].globalScale != false)
    //                         //     scale_X = scale_Y = globalScale;
    //                         var loadedData = currentZoneLayoutObj.memberObject[i];
    //                         // console.log(loadedData);
    //                         // console.log(loadedData);

    //                         var currentTableSVGStr = getTableSVG.apply( scope, [loadedData.shape, loadedData.size]);
    //                         var className = loadedData.shape + "-" + loadedData.size;
    //                         var noOfChar = loadedData.label.length;
    //                         var ID = "table" + "_" + loadedData.uid;

    //                         // place the text in the center of the shape, use BBox to obtain the accuracy
    //                         // var label_x = (defaultGraphicWidth/2 - 2) - Math.ceil(defaultFontSize/fontWHRatio/2)*(noOfChar-1);
    //                         // var label_y = defaultGraphicHeight/2 + Math.floor(defaultFontSize/2);

    //                         // table element for adding:
    //                         // render object node to scene and store data to it for update later
    //                         tableF = Snap.parse(currentTableSVGStr);
    //                         tableNode = tableF.select("g");

    //                         // console.log(tableNode.getBBox());
    //                         // tableShapeNode = tableF.select("g");

    //                         // restore color base on status code
    //                         try {
    //                             var code;
    //                             if( loadedData.status == 2 && loadedData.buffetStatus > 0 ) {
    //                                 colorCode = buffetStatusCode[loadedData.buffetStatus];
    //                             } else {
    //                                 colorCode = statusCode[loadedData.status];
    //                             }
    //                             // console.log('loadedData.status ' + loadedData.status);

    //                             // console.log(loadedData.status);
    //                             // if( loadedData.status != 2 ) {
    //                             //     // default color

    //                             // }

    //                             // tableNode.parent().parent().attr({
    //                             //     "class": colorCode + ' shape'
    //                             // });

    //                             // use class instead @ 20151106
    //                             // tableNode.selectAll("path").attr({
    //                             //     fill: legendColor[colorCode]
    //                             // });
    //                         } catch (e) {
    //                         }

    //                         // add to scene
    //                         var gTransform = masterTableEleGroup.group(tableNode).attr("class", "transform");

    //                         // console.log(masterTableEleGroup.getBBox());
    //                         // console.log(tableNode.getBBox());
    //                         // console.log(gTransform.getBBox());
    //                         var tableContainerGroup = masterTableEleGroup.group(gTransform).attr({
    //                             "id": ID, // current table ID
    //                             "class": className + ' table ' + colorCode,
    //                             'ng-click': 'tableMouseDown("'+ID+'");'
    //                         });
    //                         // var gTransform = masterTableEleGroup.group(tableNode).attr("class", "transform");
    //                         tableContainerGroup.data('loadedData', loadedData);

    //                         // console.log(tableF.select("path").getBBox()); // no width/height
    //                         // console.log(tableF.select("g").getBBox()); // no width/height
    //                         // console.log(tableNode.getBBox()); // no width/height before group to gTransform
    //                         // console.log(gTransform.getBBox()); // no width/height

    //                         // add data
    //                         // the following will be stored in browser instead of attribute,
    //                         // to make it more compatible with other tools, use attribute instead or
    //                         // get the data with SVG tool when needed
    //                         for (var key in loadedData) {
    //                             tableContainerGroup.data(key, loadedData[key]);
    //                         }
    //                         tableContainerGroup.data('id', ID);
    //                         tableContainerGroup.data('zone', loadedData.zone);
    //                         tableContainerGroup.data('state', false); // for single/multiple table saving
    //                         tableContainerGroup.data('status', loadedData.status);

    //                         // before append, all BBox consist of 0 value
    //                         this.ref = "relative";
    //                         this.addHoverRect2(gTransform);
    //                         // console.log(gTransform.select( ".hoverArea" ));

    //                         // add label
    //                         var tableLabel = this.svgScene.text(0, 0, "").attr({
    //                             "fill": this.defaultFontColor,
    //                             "font-size": this.defaultFontSize,
    //                             "font-family": this.fontFamily
    //                         });
    //                         gTransform.append(tableLabel);
    //                         // tableLabel.getBBox(); // run once before text assign to calibrate, not sure if it is the bug of Snap SVG, after run one, figure seems correct
    //                         tableLabel.attr("text", loadedData.label);
    //                         // console.log(tableLabel.getBBox());

    //                         // console.log(ID + ' original 1');
    //                         // console.log('w ' +tableLabel.getBBox().w);
    //                         // console.log('h ' + tableLabel.getBBox().h);
    //                         // console.log('--------------------------------------------------------------------');

    //                         // add transformation, always come last is better for data accuracy and easy maintenance

    //                         // console.log(tableNode.getBBox());
    //                         // console.log(gTransform.getBBox());

    //                         var eleBBox = gTransform.select('rect').getBBox();
    //                         if( ID == "table_1418785047402") {
    //                             // console.log(loadedData.pos_X);
    //                             // console.log(loadedData.pos_Y);
    //                         // console.log(eleMatrix);
    //                         // console.log(eleMatrix.toTransformString());
    //                         // console.log('gTransform bbox ');
    //                         // console.log(eleBBox);
    //                         }
    //                         /*
    //                         eleMatrix = new Snap.Matrix();
    //                         // eleMatrix.translate(loadedData.pos_X, loadedData.pos_Y);
    //                         // eleMatrix.scale(loadedData.scale_X, loadedData.scale_Y);
    //                         // eleMatrix.rotate( 0 ); 
    //                         // eleMatrix.rotate(45, 0, 0);

    //                         eleMatrix.rotate(loadedData.angle_A, eleBBox.cx, eleBBox.cy);
    //                         // console.log(eleMatrix.toTransformString());
    //                         // eleMatrix.toTransformString();
    //                         gTransform.transform(eleMatrix); // after transform, width, height, x, y of BBox are different
    //                         */

    //                         // auto offset with the cX and cY
    //                         // console.log(eleBBox);
    //                         gTransform.transform([
    //                             'R' + loadedData.angle_A, eleBBox.cx, eleBBox.cy,
    //                             'S' + loadedData.scale_X, loadedData.scale_Y, eleBBox.cx, eleBBox.cy,
    //                             'T' + loadedData.pos_X, loadedData.pos_Y

    //                             // with first time offset
    //                             // 'T' + (loadedData.pos_X + eleBBox.cx * (userConfig.table.display.globalScale - 1)), (loadedData.pos_Y + eleBBox.cy * (userConfig.table.display.globalScale - 1))
    //                         ].join(','));

    //                         // for easy tracking
    //                         gTransform.parent().attr({
    //                             'ref-posX': loadedData.pos_X,
    //                             'ref-posY': loadedData.pos_Y
    //                         });

    //                         // console.log(eleMatrix.toTransformString());

    //                         // gTransform.mousedown(function () {
    //                         // gTransform.mouseup(function () {
    //                             // tableMouseDown(this);
    //                         // })

    //                         // some computer chrome even same version reports incorrect bbox info (before transformation), put it here, it is ensure the updated transformation information
    //                         this.centerLabel(gTransform.select(".hoverArea"), tableLabel);
    //                         // console.log(gTransform.select(".hoverArea"));
    //                         // console.log(gTransform.select(".hoverArea").getBBox());
    //                         // console.log(ID + ' original 2');
    //                         // console.log('w ' +tableLabel.getBBox().w);
    //                         // console.log('h ' + tableLabel.getBBox().h);
    //                         // console.log('--------------------------------------------------------------------');

    //                         /*// add event handler
    //                         var move = function(dx, dy, x, y, obj) {
    //                             console.log("dragging moving");
    //                         }

    //                         var start = function() {
    //                                 // this.data('origTransform', this.transform().local );
    //                         }

    //                         var stop = function() {
    //                                 console.log('finished dragging');
    //                         }

    //                         gTransform.drag( move, start, stop );*/
    //                         // test drag ratio to viewbox
    //                         // svgScene.select("#floorGroup").drag( move, start, stop );

    //                         // use ng-click instead of snap svg library
    //                         // yet, compile may make this slower

    //                         // console.log($('#'+ID));
    //                         // console.log(ID);
    //                         $compile( $('#'+ID) )( scope );
    //                     } // for
    //                 }

    //                 scope.RMSTable.makeOrder = function (ele) {
    //                     var tableNo = ele.select("text").attr("text");
    //                     var uid = ele.parent().data("uid");
    //                     console.log('tableNo ' + tableNo);
    //                     //element.addClass('ng-hide');
    //                     // console.log(scope.$emit);
    //                     // console.log(MainService);

    //                     // MainService.assignTableNo(tableNo);
    //                     // var status = Snap("#svg-canvas").select('#table_1418785047317').data().loadedData.status;
    //                     // console.log(ele.parent().data().loadedData);
    //                     var status = ele.parent().data().loadedData.status;
    //                     // console.log(status);

    //                     MainService.clickTable(tableNo, ele);
    //                     scope.message = "Table " + tableNo + " is Selected / " + MainService.RMSTable.editMode;
    //                     // scope.showCustomToast( 5000 );

    //                     //scope.$emit('modeTableOrder', { 'tableNo': tableNo }, function (r) {
    //                     //    console.log(r);
    //                     //});
    //                     // console.log(tableNo);
    //                     /*if( typeof tableNo != "undefined" ) {
    //                         this.changeElementColor(ele, legendColor.inputOrder );
    //                         SocketFactory.emit('setTableStatus', { tableNo: tableNo , status : 1});
    //                         window.location = "order.html?tableNo=" + tableNo;
    //                         //  + "&uid=" + uid
    //                     }*/

    //                     // place new action here
    //                 }

    //                 scope.RMSTable.returnDataObject = function( eleID ) {
    //                     var currentZoneLayoutObj = this.Scene[this.currentSceneNo];
    //                     // for multiple Scene support, use sceneID later, use sceneID to check and then loop through member object
    //                     for( var i = 0; i < currentZoneLayoutObj.memberObject.length; i++ ) {
    //                         if( currentZoneLayoutObj.memberObject[i].uid === eleID ) {
    //                             return currentZoneLayoutObj.memberObject[i];
    //                         }
    //                     }
    //                     return null;
    //                 }; // returnDataObject

    //                 scope.RMSTable.updateDataObject = function( data ) {
    //                     var currentZoneLayoutObj = this.Scene[this.currentSceneNo];
    //                     // for multiple Scene support, use sceneID later, use sceneID to check and then loop through member object
    //                     eleID = data.uid;
    //                     var savedObj = this.returnDataObject( eleID );
    //                     // console.log("returned Data Object");
    //                     // console.log(this.returnDataObject( eleID ));
    //                     // console.log("update id: " + eleID);
    //                     // console.log(savedObj);
    //                     // console.log(data);

    //                     // create an object from data same structure as loaded object for accurately compare and add
    //                     var tmpObj = {};
    //                     for( var key in data ) {
    //                         if( key != "id" ) {
    //                             tmpObj[ key ] = data[ key ];
    //                         }
    //                     }

    //                     // add to object if it does not exist in existing object
    //                     if( savedObj === null ) {
    //                         currentZoneLayoutObj.memberObject.push( tmpObj );
    //                         return "new data is added";
    //                     }

    //                     // update to object if exist
    //                     for( var key in savedObj) {
    //                         if( data[key] != undefined ) {
    //                             savedObj[ key ] = data[ key ];
    //                         }
    //                     }
    //                     // savedObj.angle_A = 0
    //                     // console.log(savedObj.angle_A);
    //                     // console.log(savedObj.pos_X);
    //                     // console.log(this.currentZoneLayoutObj.memberObject[7].pos_X);
    //                     // console.log(sceneJSON.scene[1].memberObject[7].pos_X);
    //                     return "data is updated";
    //                 } // updateDataObject

    //                 function loadTableScene( data ){
    //                     console.log('loadTableScene');
    //                    // console.log(data);
    //                    if( typeof data === 'string' ) {
    //                         scope.RMSTable.Scene = JSON.parse(data).scene; // default load first one
    //                    } else {
    //                         scope.RMSTable.Scene = data.scene;
    //                    }

    //                    window.sceneDataNeedle = scope.RMSTable.Scene;
    //                    // prevObj = JSON.parse(r).scene;
    //                    // console.log(scope.RMSTable.Scene);

    //                    if( scope.RMSTable.currentSceneNo === null || scope.RMSTable.svgScene.select("#floorGroup") === null) {
    //                         console.log('first time load scene RMSTable.currentSceneNo');
    //                        scope.RMSTable.currentSceneNo = scope.RMSTable.Scene.length - 1; // suppose to be a var called in function

    //                        // console.log(scope.RMSTable);
    //                        // console.log(MainService.RMSTable);

    //                        console.log('then2 render scene');
    //                        // console.log(scope.RMSTable.svgScene.select("#floorGroup"));

    //                        // Snap.load("rms.svg", function (f) {
    //                         $http.get('rms2.svg').success(function (f) { // timing is better than Snap's for angular
    //                             // console.log(Snap.parse(data));
    //                            var f = Snap.parse(f);
    //                            // svgScene.append(f.selectAll("filter"))    ; // to debug IE11 crash
    //                            // tmp = f.selectAll("#new-shape-list");
    //                            // console.log("load rms.svg");
    //                            scope.RMSTable.svgScene.append(f.selectAll("g#floorGroup")); // grid pattern
    //                            scope.RMSTable.svgScene.select("defs").append(f.selectAll("pattern"));
    //                            scope.RMSTable.renderLayout( scope.RMSTable.Scene[scope.RMSTable.currentSceneNo].layout, scope.RMSTable.currentSceneNo );

    //                            // demo load
    //                            // angular.element(scope.RMSTable.makeOrder(scope.RMSTable.svgScene.select("#table_1418785047326")));
    //                            scope.message = uiLanguage[INFO.lang]['sceneLoaded'];
    //                            // scope.showCustomToast();

    //                            // for convenience debugging and testing
    //                            MainService.tableList = [];
    //                            Snap("#svg-canvas").selectAll("#tableGroup_0 .transform text").forEach(function(element, index, array){
    //                                // console.log(element.attr('text'))
    //                                MainService.tableList.push(
    //                                    {
    //                                        tableNo: element.attr('text')
    //                                    });
    //                            });
    //                            window.tableListNeedle = MainService.tableList;
    //                        }); // snap.load

    //                    } else {
    //                        // scope.message = "Table Scene is already initiated once. Table is re-rendered.";
    //                        // // scope.showCustomToast();

    //                        console.log("render table only");
    //                        // render table only
    //                        scope.RMSTable.svgScene.selectAll(".transform").forEach(function(element, index, array) {
    //                            if (element.freeTransform) {
    //                                element.freeTransform.unplug();
    //                            }
    //                        })

    //                        // old method
    //                        console.log('removeAllHandles');
    //                        removeAllHandles();
    //                        scope.cancelTransfer('reset');

    //                        // remove all and re-render
    //                        scope.RMSTable.svgScene.selectAll( ".tableGroup" ).remove();
    //                        scope.RMSTable.renderTable( scope.RMSTable.currentSceneNo );
    //                        scope.RMSTable.changeZone( scope.RMSTable.currentSceneNo ); // reset to current scene
    //                    }

    //                    if( angular.isUndefined(MainService.RMSTable )) {
    //                        // console.log('new');
    //                        MainService.RMSTable = scope.RMSTable;
    //                    } else {
    //                        // console.log('already exist');
    //                        MainService.RMSTable = $.extend( true, MainService.RMSTable, scope.RMSTable );
    //                    }

    //                    MainService.RMSTable.removeAllHandles = removeAllHandles;
    //                    MainService.RMSTable.isTableFree = isTableFree;
    //                    MainService.RMSTable.editMode = "order";
    //                    MainService.RMSTable.changeMode = function( mode ) {
    //                        this.editMode = lowerFirstLetter( mode );
    //                        console.log(lowerFirstLetter( mode ));
    //                        console.log(this.editMode);
    //                        showMessage('#status-message', uiLanguage[INFO.lang][lowerFirstLetter( mode )] + ' ' + uiLanguage[INFO.lang]['mode'], '', 'warn', 'stick');
    //                    }
    //                    console.log(MainService.RMSTable);
    //                 }

    //                 function initTableScene( data ) {
    //                     console.log('initTableScene');
    //                     // if( typeof scope.tableType === 'object') {
    //                         console.log("loading table");

    //                         // if data is not supplied load data, or load table scene directly
    //                         if( typeof data === 'undefined' ) {
    //                             SocketFactory.emit("loadTableSchema", '{}', function (r) {
    //                                 console.log('loadTableSchema by default / initTableScene');
    //                                  // console.log(r);
    //                                 loadTableScene( r );

    //                                 // var finish = new Date();
    //                                 // var difference = new Date();
    //                                 // difference.setTime(finish.getTime() - start.getTime());
    //                                 // console.log(start.getMilliseconds());
    //                                 // console.log(finish.getMilliseconds());
    //                                 // alert( difference.getMilliseconds() );
    //                             }); // socketFactory
    //                         } else {
    //                             console.log('refresh using data supplied by refreshTable / initTableScene');
    //                             loadTableScene( data );
    //                         }

    //                         // not working
    //                         // SocketFactory.on('connect_error', function() {
    //                         //     // console.log(r);
    //                         //     // console.log('connection error');
    //                         //     scope.message = uiLanguage[INFO.lang]['sceneLoadError'];
    //                         //     // $http.get('data/scene_objects.json').success(function (data) {
    //                         //     //     // console.log(data);
    //                         //     //    loadTableScene( data );
    //                         //     //    $("body > svg").remove(); // avoid accumulated garbage if server cannot be connected
    //                         //     //     // scope.showCustomToast( "123" );
    //                         //     // });
    //                         //     showMessage("#status-message", scope.message, "", "warn", 30000);
    //                         // });
    //                     // } else {
    //                     //     scope.message = uiLanguage[INFO.lang]['jsonLoadError'];
    //                     // }
    //                 } // initTableScene

    //                 // load table
    //                 if( angular.isUndefined( scope.RMSTable.tableType ) ) {
    //                     $http.get('data/table_type.json').success(function (data) {
    //                         MainService.RMSTable.tableType = scope.tableType = data.tableType;
    //                         // console.log(scope);
    //                         // scope.RMSTable.renderTableType();

    //                         // initTableScene();

    //                         // debug initTableScene slowness
    //                         SocketFactory.emit("loadTableSchema", '{}', function (r) {
    //                             console.log('loadTableSchema');
    //                             // console.log(r);
    //                             loadTableScene( r );
    //                         }); // socketFactory

    //                         // scope.closeToast();
    //                         // scope.showCustomToast(3000);
    //                     });
    //                 } else {
    //                     scope.tableType = scope.RMSTable.tableType;
    //                     SocketFactory.emit("loadTableSchema", '{}', function (r) {
    //                         // console.log(r);
    //                         loadTableScene( r );
    //                     }); // socketFactory
    //                 }

    //                 // MainService.RMSTable.removeAllHandles = removeAllHandles;
    //                 // MainService.RMSTable.isTableFree = isTableFree;
    //                 // MainService.RMSTable.editMode = "order";
    //                 // MainService.RMSTable.changeMode = function( mode ) {
    //                 //     this.editMode = lowerFirstLetter( mode );
    //                 //     console.log(lowerFirstLetter( mode ));
    //                 //     console.log(this.editMode);
    //                 //     showMessage('#status-message', uiLanguage[INFO.lang][lowerFirstLetter( mode )] + ' ' + uiLanguage[INFO.lang]['mode'], '', 'warn', 'stick');
    //                 // }
    //                 // console.log(MainService.RMSTable);

    //                 scope.move = function( direction ) {
    //                     var ele = scope.RMSTable.currentActiveElementSnap.select('.transform');
    //                     // var loadedMatrixtest = ele.attr("transform").totalMatrix.split();
    //                     var loadedMatrix = Snap.parseTransformString(ele.transform());
    //                     // window.tmp = ele;

    //                     // console.log(ele.freeTransform);

    //                     var dx = dy = 0;
    //                     // console.log(direction);
    //                     switch( direction ) {
    //                         case 'up':
    //                             var dy = -userConfig.table.edit.snap.translate;
    //                             // ele.data('modified', true);
    //                             ele.parent().data('state', 'modified')
    //                             break;
    //                         case 'right':
    //                             var dx = userConfig.table.edit.snap.translate;
    //                             // ele.data('modified', true);
    //                             ele.parent().data('state', 'modified')
    //                             break;
    //                         case 'down':
    //                             var dy = userConfig.table.edit.snap.translate;
    //                             // ele.data('modified', true);
    //                             ele.parent().data('state', 'modified')
    //                             break;
    //                         case 'left':
    //                             var dx = -userConfig.table.edit.snap.translate;
    //                             // ele.data('modified', true);
    //                             ele.parent().data('state', 'modified')
    //                             break;
    //                         default:
    //                             break;
    //                     }

    //                     // console.log('dy ' + dy);
    //                     // console.log(ele.data());

    //                     ele.freeTransform.updateHandles().attrs.translate.x += dx;
    //                     ele.freeTransform.updateHandles().attrs.translate.y += dy;
    //                     ele.freeTransform.updateHandles().apply();

    //                     // for easy tracking
    //                     var loadedMatrix = Snap.parseTransformString(ele.transform());

    //                     loadedMatrix.forEach(function(transformP) {
    //                         if(transformP[0] == 'T') {
    //                             x = transformP[1];
    //                             y = transformP[2];
    //                         }

    //                         if(transformP[0] == 'R') {
    //                             rotate = transformP[1];
    //                             centerx = transformP[2];
    //                             centery = transformP[3];
    //                         }

    //                         if(transformP[0] == 'S') {
    //                             scalex = transformP[1];
    //                             scaley = transformP[2];
    //                         }
    //                     });
    //                     ele.parent().attr({
    //                         'ref-posX': x,
    //                         'ref-posY': y
    //                     });


    //                     // eleMatrix = new Snap.Matrix();
    //                     // var transformed = [];

    //                     // loadedMatrix.forEach(function(transformP) {
    //                     //     if(transformP[0] == 'T') {
    //                     //         // eleMatrix.translate((transformP[1] ),(transformP[2] ));
    //                     //         transformed.push( 'T' + (transformP[1] + dx), (transformP[2] + dy) );
    //                     //     }

    //                     //     if(transformP[0] == 'R') {
    //                     //         // eleMatrix.rotate(transformP[1], transformP[2], transformP[3]);
    //                     //         transformed.push( 'R' + transformP[1], transformP[2], transformP[3] );
    //                     //     }

    //                     //     if(transformP[0] == 'S') {
    //                     //         // eleMatrix.scale(transformP[1], transformP[2], transformP[3], transformP[4]);
    //                     //         transformed.push( 'S' + transformP[1], transformP[2], transformP[3], transformP[4] );
    //                     //     }
    //                     // })
    //                     // // ele.transform(transformed.join(','));
    //                     // console.log(transformed);

    //                     // ele.transform(eleMatrix);
    //                     // console.log(eleMatrix);
    //                 }

    //                 scope.scale = function( size ) {
    //                     var ele = scope.RMSTable.currentActiveElementSnap.select('.transform');
    //                     // var loadedMatrixtest = ele.attr("transform").totalMatrix.split();
    //                     var loadedMatrix = Snap.parseTransformString(ele.transform());

    //                     var scale = 0;
    //                     // console.log(direction);
    //                     switch( size ) {
    //                         case 'up':
    //                             var scale = userConfig.table.edit.snap.scale;
    //                             // ele.data('modified', true);
    //                             ele.parent().data('state', 'modified')
    //                             break;
    //                         case 'down':
    //                             var scale = -userConfig.table.edit.snap.scale;
    //                             // ele.data('modified', true);
    //                             ele.parent().data('state', 'modified')
    //                             break;
    //                         default:
    //                             break;
    //                     }

    //                     ele.freeTransform.updateHandles().attrs.scale.x += scale;
    //                     ele.freeTransform.updateHandles().attrs.scale.y += scale;
    //                     ele.freeTransform.updateHandles().apply();
    //                 }

    //                 scope.rotate = function( direction ) {
    //                     var ele = scope.RMSTable.currentActiveElementSnap.select('.transform');
    //                     var loadedMatrix = Snap.parseTransformString(ele.transform());

    //                     var rotate = 0;
    //                     switch( direction ) {
    //                         case 'right':
    //                             var rotate = userConfig.table.edit.snap.rotate;
    //                             break;
    //                         case 'left':
    //                             var rotate = -userConfig.table.edit.snap.rotate;
    //                             break;
    //                         default:
    //                             break;
    //                     }

    //                     ele.parent().data('state', 'modified')
    //                     ele.freeTransform.updateHandles().attrs.rotate += rotate;
    //                     ele.freeTransform.updateHandles().apply();
    //                 }

    //                 scope.saveTable = function() {
    //                     var allTable = [];
    //                     var x = y = scalex = scaley = rotate = centerx = centery = 0,
    //                         transformP = [];
    //                     var loadedMatrix = '';
    //                     // var ele = scope.RMSTable.currentActiveElementSnap;
    //                     // var allModified = Snap.parseTransformString(ele.select('.modified').transform());
    //                     var allModified = Snap('#svg-canvas').selectAll('.modified');

    //                     if( allModified.length > 0 ) {
    //                         allModified.forEach(function(ele){
    //                             // console.log(ele);
    //                             ele = ele.parent();
    //                             x = y = scalex = scaley = rotate = centerx = centery = 0,
    //                                 transformP = [];

    //                             loadedMatrix = Snap.parseTransformString(ele.select('.transform').transform());

    //                             loadedMatrix.forEach(function(transformP) {
    //                                 if(transformP[0] == 'T') {
    //                                     x = transformP[1];
    //                                     y = transformP[2];
    //                                 }

    //                                 if(transformP[0] == 'R') {
    //                                     rotate = transformP[1];
    //                                     centerx = transformP[2];
    //                                     centery = transformP[3];
    //                                 }

    //                                 if(transformP[0] == 'S') {
    //                                     scalex = transformP[1];
    //                                     scaley = transformP[2];
    //                                 }
    //                             });

    //                             // console.log(ele.data('state'));
    //                             var tableData = {
    //                                 code: ele.data('uid'), 
    //                                 type: ele.data('type'),
    //                                 x: x, 
    //                                 y: y, 
    //                                 scalex: scalex, 
    //                                 scaley: scaley, 
    //                                 // zone: ele.data('zone'), 
    //                                 zone: MainService.RMSTable.currentSceneNo + 1, 
    //                                 label: ele.select('text').attr('text'), 
    //                                 shape: ele.data('shape'), 
    //                                 size: ele.data('size'),
    //                                 angle: rotate,
    //                                 state: ele.data('state') || 'modified',
    //                                 status: ele.data('status'),
    //                                 new: ele.data('new') ? ele.data('new') : false
    //                             };

    //                             // console.log(tableData);
    //                             allTable.push(tableData);
    //                             // ele.data('modified', false);
    //                             ele.data('state', false)
    //                         })
    //                     }

    //                     console.log(allTable);

    //                     SocketFactory.emit('saveLayoutTable', allTable, function(r){
    //                         r = JSON.parse(r);
    //                         console.log(r);
    //                         if( r.result.toLowerCase() === 'ok' ) {
    //                             // refresh table

    //                         }
    //                     });
    //                 }

    //                 scope.delete = function( elements ) {
    //                     if( MainService.RMSTable.groupEdit && elements != null) {
    //                         elements.forEach(function(element){
    //                             if( element.hasClass('new') ) {
    //                                 element.select('.transform').freeTransform.unplug();
    //                                 element.remove();
    //                             }

    //                             element.data('state', 'delete');
    //                             element.select('.shape path, .shape circle').attr('style', 'fill: #9600FF');
    //                         });
    //                         return;
    //                     }
    //                     // console.log(MainService.RMSTable.currentActiveElementSnap.hasClass('new'));
    //                     if( MainService.RMSTable.currentActiveElementSnap.hasClass('new') ) {
    //                         MainService.RMSTable.currentActiveElementSnap.select('.transform').freeTransform.unplug();
    //                         MainService.RMSTable.currentActiveElementSnap.remove();
    //                         return;
    //                     }

    //                     MainService.RMSTable.currentActiveElementSnap.data('state', 'delete');
    //                     MainService.RMSTable.currentActiveElementSnap.select('.shape path, .shape circle').attr('style', 'fill: #9600FF');
    //                 }

    //                 scope.restore = function(elements) {
    //                     if( MainService.RMSTable.groupEdit && elements != null) {
    //                         elements.forEach(function(element){
    //                             element.data('state', false);
    //                             element.select('.shape path, .shape circle').attr('style', '');
    //                         });
    //                         return;
    //                     } else {
    //                         MainService.RMSTable.currentActiveElementSnap.data('state', false);
    //                         MainService.RMSTable.currentActiveElementSnap.select('.shape path, .shape circle').attr('style', '');
    //                     }
    //                 }

    //                 $rootElement.on('keypress keydown', function(e){
    //                     // console.log(e.which);
    //                     if( !MainService.RMSTable.currentActiveElementSnap || MainService.RMSTable.currentActiveElementSnap.select('.modified').length === 0 ) 
    //                         return;

    //                     if( MainService.RMSTable.groupEdit ) {
    //                         hideTableHandles();

    //                         var selectedElements = MainService.RMSTable.svgScene.selectAll('.selected');
    //                         // console.log(selectedElements);

    //                         // function markModified() {
    //                         //     selectedElements.forEach(function(element){
    //                         //         element.parent().data('state', 'modified');
    //                         //     });
    //                         // }

    //                         // group edit manipulations
    //                         var dx = dy = dS = x = y = dR = 0;
    //                         switch( e.which ) {
    //                             case keyMap.del:
    //                                 console.log('mark delete / group edit');
    //                                 scope.delete(selectedElements);
    //                                 return;
    //                                 break;
    //                             case keyMap.ESC:
    //                                 console.log('remove delete mark / group edit');
    //                                 scope.restore(selectedElements);
    //                                 return;
    //                                 break;
    //                             case keyMap.plus:
    //                                 var dS = userConfig.table.edit.snap.scale;
    //                                 break;
    //                             case keyMap.minus:
    //                                 var dS = -userConfig.table.edit.snap.scale;
    //                                 break;
    //                             case keyMap.up:
    //                                 var dy = -userConfig.table.edit.snap.translate;
    //                             break;
    //                             case keyMap.down:
    //                                 var dy = userConfig.table.edit.snap.translate;
    //                             break;
    //                             case keyMap.right:
    //                                 var dx = userConfig.table.edit.snap.translate;
    //                             break;
    //                             case keyMap.left:
    //                                 var dx = -userConfig.table.edit.snap.translate;
    //                             break;
    //                             case keyMap.close_square_bracket:
    //                                 var dR = userConfig.table.edit.snap.rotate;
    //                             break;
    //                             case keyMap.open_square_bracket:
    //                                 var dR = -userConfig.table.edit.snap.rotate;
    //                             break;
    //                             default:
    //                                 return;
    //                             break;
    //                         }
    //                         console.log(dR);
    //                         // markModified();

    //                         var orgX, orgY, orgScaleX, orgScaleY;
    //                         selectedElements.forEach(function(element){
    //                             element.parent().data('state', 'modified');

    //                             eleBB = element.getBBox();

    //                             orgRotate = element.freeTransform.updateHandles().attrs.rotate;
    //                             orgX = element.freeTransform.updateHandles().attrs.translate.x;
    //                             orgY = element.freeTransform.updateHandles().attrs.translate.y;
    //                             orgScaleX = element.freeTransform.updateHandles().attrs.scale.x;
    //                             orgScaleY = element.freeTransform.updateHandles().attrs.scale.y;

    //                             console.log(element.freeTransform.updateHandles().attrs.rotate);

    //                             element.freeTransform.updateHandles().attrs.rotate += dR;

    //                             element.freeTransform.updateHandles().attrs.scale.x += dS;
    //                             element.freeTransform.updateHandles().attrs.scale.y += dS;

    //                             element.freeTransform.updateHandles().attrs.translate.x += dx;
    //                             element.freeTransform.updateHandles().attrs.translate.y += dy;
    //                             element.freeTransform.updateHandles().apply();

    //                             // bb-handle-box update
    //                             // $('.bb-handle-box')

    //                             // function compare( arr ) {
    //                             //     if( typeof arr.length === 'undefined' )
    //                             //         return;

    //                             //     var result = true;
    //                             //     for(var i = 0; i<arr.length; i++){
    //                             //         result = result && arr[i];
    //                             //     }
    //                             //     return result;
    //                             // }

    //                             var transformed = [],
    //                                 bbtMatrix,
    //                                 verify = [],
    //                                 eleID = element.parent().attr('id'),
    //                                 // allBBT = MainService.RMSTable.svgScene.selectAll('.bbt-handle-box');
    //                                 bbtElement = MainService.RMSTable.svgScene.select('.bbt-handle-box.'+eleID),
    //                                 bbElement = MainService.RMSTable.svgScene.select('.bb-handle-box.'+eleID),
    //                                 bbtTransformStr = bbtElement.transform().toString(),
    //                                 bbtElementBBox = bbtElement.getBBox();

    //                                 if( bbtTransformStr.indexOf('r') === -1 && bbtTransformStr.indexOf('R') === -1 ) {
    //                                     // no rotation string...
    //                                     bbtTransformStr = bbtTransformStr + 'r0,0,0';
    //                                 }

    //                                 bbtMatrix = Snap.parseTransformString(bbtTransformStr);

    //                                 console.log(eleID);
    //                                 console.log(bbtElementBBox);
    //                                 // console.log(eleBB);
    //                                 console.log(bbtMatrix);
    //                                 // console.log(allBBT);

    //                                 bbtMatrix.forEach(function(transformP) {
    //                                     if(transformP[0].toUpperCase() == 'R') {
    //                                         transformed.push( 'R' + (transformP[1] + dR), transformP[2], transformP[3]);
    //                                     }
    //                                 });

    //                                 bbtMatrix.forEach(function(transformP) {
    //                                     if(transformP[0].toUpperCase() == 'S') {
    //                                         transformed.push( 'S' + (transformP[1] + dS), (transformP[2] + dS), transformP[3], (transformP[4]));
    //                                     }
    //                                 });

    //                                 bbtMatrix.forEach(function(transformP) {
    //                                     if(transformP[0].toUpperCase() == 'T') {
    //                                         transformed.push( 'T' + (transformP[1] + dx ), (transformP[2] + dy ) );
    //                                     }
    //                                 });

    //                                 console.log(transformed.join(','));
    //                                 bbtElement.transform(transformed.join(','));
    //                                 bbElement.attr({
    //                                     x: eleBB.x + dx,
    //                                     y: eleBB.y + dy
    //                                 })

    //                                 // bbtElement.transform([
    //                                 //     'R' + loadedData.angle_A, 0, 0,
    //                                 //     'S' + loadedData.scale_X, loadedData.scale_Y, 0, 0,
    //                                 //     'T' + eleBB.x, eleBB.y

    //                                 //     // with first time offset
    //                                 //     // 'T' + (loadedData.pos_X + eleBBox.cx * (userConfig.table.display.globalScale - 1)), (loadedData.pos_Y + eleBBox.cy * (userConfig.table.display.globalScale - 1))
    //                                 // ].join(','));

    //                                 // allBBT.forEach(function(bbtElement){
    //                                 //     // match current element's bbtElement

    //                                 //     bbtMatrix = Snap.parseTransformString(bbtElement.transform().toString());
    //                                 //     console.log(bbtElement);
    //                                 //     console.log(bbtMatrix);

    //                                 //     bbtMatrix.forEach(function(transformP) {
    //                                 //         console.log(transformP);
    //                                 //         if(transformP[0].toUpperCase() == 'T') {
    //                                 //             if(transformP.indexOf(eleBB.x) > -1 && transformP.indexOf(eleBB.y) > -1 ) {
    //                                 //                 verify.push(true);
    //                                 //             } else {
    //                                 //                 verify.push(false);
    //                                 //             }
    //                                 //         }
    //                                 //     });

    //                                 //     if( compare( verify ) ) {
    //                                 //         console.log('matched / found');

    //                                 //         bbtMatrix.forEach(function(transformP) {
    //                                 //             if(transformP[0].toUpperCase() == 'R') {
    //                                 //                 transformed.push( 'R' + transformP[1], transformP[2], transformP[3] );
    //                                 //             }
    //                                 //         });

    //                                 //         bbtMatrix.forEach(function(transformP) {
    //                                 //             if(transformP[0].toUpperCase() == 'S') {
    //                                 //                 transformed.push( 'S' + (transformP[1] + scale), (transformP[2]), transformP[3], (transformP[4] + scale));
    //                                 //             }
    //                                 //         });

    //                                 //         bbtMatrix.forEach(function(transformP) {
    //                                 //             if(transformP[0].toUpperCase() == 'T') {
    //                                 //                 transformed.push( 'T' + (transformP[1] + dx), (transformP[2] + dy) );
    //                                 //             }
    //                                 //         });
    //                                 //         console.log(transformed.join(','));
    //                                 //         bbtElement.transform(transformed.join(','));
    //                                 //     }
    //                                 // });



    //                             // for easy tracking
    //                             var loadedMatrix = Snap.parseTransformString(element.transform());

    //                             loadedMatrix.forEach(function(transformP) {
    //                                 if(transformP[0] == 'T') {
    //                                     x = transformP[1];
    //                                     y = transformP[2];
    //                                 }

    //                                 if(transformP[0] == 'R') {
    //                                     rotate = transformP[1];
    //                                     centerx = transformP[2];
    //                                     centery = transformP[3];
    //                                 }

    //                                 if(transformP[0] == 'S') {
    //                                     scalex = transformP[1];
    //                                     scaley = transformP[2];
    //                                 }
    //                             });
    //                             element.parent().attr({
    //                                 'ref-posX': x,
    //                                 'ref-posY': y
    //                             });
    //                         });


    //                         return;
    //                     } else {
    //                         // single edit
    //                         var ele = MainService.RMSTable.currentActiveElementSnap.select('.transform'),
    //                             scale = dx = dy = x = y = rotate = 0;

    //                         // if( angular.isUndefined(ele))
    //                         //     return;

    //                         // console.log(keyMap);
    //                         console.log(e.which);

    //                         switch( e.which ) {
    //                             case keyMap.del:
    //                                 console.log('mark delete');
    //                                 scope.delete();
    //                                 return;
    //                                 break;
    //                             case keyMap.ESC:
    //                                 console.log('remove delete mark');
    //                                 scope.restore();
    //                                 return;
    //                                 break;
    //                             case keyMap.plus:
    //                                 var scale = userConfig.table.edit.snap.scale;
    //                                 break;
    //                             case keyMap.minus:
    //                                 var scale = -userConfig.table.edit.snap.scale;
    //                                 break;
    //                             case keyMap.up:
    //                                 var dy = -userConfig.table.edit.snap.translate;
    //                             break;
    //                             case keyMap.right:
    //                                 var dx = userConfig.table.edit.snap.translate;
    //                             break;
    //                             case keyMap.down:
    //                                 var dy = userConfig.table.edit.snap.translate;
    //                             break;
    //                             case keyMap.left:
    //                                 var dx = -userConfig.table.edit.snap.translate;
    //                             break;
    //                             case keyMap.close_square_bracket:
    //                                 var rotate = userConfig.table.edit.snap.rotate;
    //                                 break;
    //                             case keyMap.open_square_bracket:
    //                                 var rotate = -userConfig.table.edit.snap.rotate;
    //                                 break;
    //                             default:
    //                                 return;
    //                             break;
    //                         }

    //                         // console.log('dy ' + dy);
    //                         // console.log(ele.data());
    //                         ele.parent().data('state', 'modified')
    //                         ele.freeTransform.updateHandles().attrs.rotate += rotate;

    //                         ele.freeTransform.updateHandles().attrs.scale.x += scale;
    //                         ele.freeTransform.updateHandles().attrs.scale.y += scale;

    //                         ele.freeTransform.updateHandles().attrs.translate.x += dx;
    //                         ele.freeTransform.updateHandles().attrs.translate.y += dy;
    //                         ele.freeTransform.updateHandles().apply();

    //                         // for easy tracking
    //                         var loadedMatrix = Snap.parseTransformString(ele.transform());

    //                         loadedMatrix.forEach(function(transformP) {
    //                             if(transformP[0] == 'T') {
    //                                 x = transformP[1];
    //                                 y = transformP[2];
    //                             }

    //                             if(transformP[0] == 'R') {
    //                                 rotate = transformP[1];
    //                                 centerx = transformP[2];
    //                                 centery = transformP[3];
    //                             }

    //                             if(transformP[0] == 'S') {
    //                                 scalex = transformP[1];
    //                                 scaley = transformP[2];
    //                             }
    //                         });
    //                         ele.parent().attr({
    //                             'ref-posX': x,
    //                             'ref-posY': y
    //                         });

    //                         // eleMatrix = new Snap.Matrix();
    //                     }
    //                 })

    //                 $rootElement.on('keyup change', '#current-table-label', function(e){
    //                     if( MainService.RMSTable.groupEdit ) {
    //                         hideTableHandles();
    //                         return;
    //                     }

    //                     // console.log($rootElement.find('#current-table-label'));
    //                     // console.log(MainService.RMSTable.currentActiveElementSnap);
    //                     if( !MainService.RMSTable.currentActiveElementSnap || MainService.RMSTable.currentActiveElementSnap.select('.modified').length === 0 ) 
    //                         return;

    //                     // console.log(MainService.RMSTable.currentActiveElementSnap);
    //                     // console.log($(this).val());
    //                     var ele = MainService.RMSTable.currentActiveElementSnap,
    //                         tableLabel = ele.select('text').attr('text', $(this).val());

    //                     ele.data('state', 'modified');
    //                     MainService.RMSTable.centerLabel(ele.select(".hoverArea"), tableLabel);
    //                 });
    //             }
    //         }
    //     });

    // app.controller('tableController', function ($scope, MainService, uiLanguage, INFO){
    //     // the scope inside this controller
    //     // default table icon settings

    //     // console.log($scope.RMSTable);
    //     // console.log($scope.svgScene);

    //     // console.log($element);
    //     // $http.get('data/table_type.json').success(function (data) {
    //     //     $scope.tableTypeJSON = data;
    //     //     // console.log(data);
    //     // });
    //     // share with other controllers or change state of the directive such as active/inactive
    //     $scope.ui = uiLanguage[INFO.lang];

    //     $scope.RMSTable = {};

    //     $scope.cancelTransfer = function cancelTransfer( option ) {
    //         // status message
    //         if( MainService.modeCheckFloor('changeTable') && $scope.RMSTable.transfer_from != 9999 && option != 'reset' )
    //             showMessage("#status-message", $scope.ui.transfer_cancel, "", "warn", 30000);

    //         // console.log(this);
    //         // console.log($scope);
    //         this.removeAllTransferBB();
    //         var svgScene = Snap("#svg-canvas");
    //         svgScene.selectAll(".transfer").forEach(function (element, index, array) {
    //             element.removeClass("transfer");
    //         });
    //         $('body').removeClass('transfer transfer-from');
    //         $scope.RMSTable.transfer_to = 9999;
    //         $scope.RMSTable.transfer_from = 9999;
    //     }

    //     MainService.cancelTransfer = function() {
    //         $scope.cancelTransfer.apply( $scope, arguments );
    //     }

    //     $scope.removeAllTransferBB = function removeAllTransferBB() {
    //         var svgScene = Snap("#svg-canvas");
    //         if ((activeTransferItem = svgScene.selectAll(".transfer")).length != 0) {
    //             // console.log(activeTransferItem);
    //             activeTransferItem.forEach(this.removeBB);
    //         }
    //     }

    //     $scope.removeBB = function removeBB(element, index, array) {
    //         // console.log(element);
    //         element.ftRemoveBB();
    //     }
    // });

    app.directive('kitchenInfoScene', function (MainService) {
        return {
            restrict: "E",
            templateUrl: 'tmpl/kitchenInfoScene.html',
            controller: function ($scope, MainService) {
                $scope.MainService = MainService;
            }
        }
    })

    app.directive('modifierScene', function (MainService) {
        return {
            restrict: "E",
            templateUrl: 'tmpl/modifier.html',
            controller: function ($scope, MainService, INFO) {
                $scope.MainService = MainService;
                $scope.INFO = INFO;
                $scope.itemsPerPage = 13;
                $scope.totalItems = INFO.cancelRemark.length;
                $scope.currentPage = 1;
            }
        }
    });

    app.directive('divideBill', function (MainService) {
        return {
            restrict: "E",
            templateUrl: 'tmpl/divideBill.html',
            controller: function ($scope, MainService) {
                $scope.MainService = MainService;
            }
        }
    });

    app.directive('reportScene', function (MainService) {
        return {
            restrict: "E",
            templateUrl: 'tmpl/reportScene.html',
            controller: function (INFO) {
            }
        }
    });

    app.directive('clearanceScene', function (MainService) {
        return {
            restrict: "E",
            templateUrl: 'tmpl/clearanceScene.html'
        }
    });

    app.directive('cashManagementScene', function (MainService) {
        return {
            restrict: "E",
            templateUrl: 'tmpl/cashManagementScene.html',
            link: function (scope, element) {
            },
            controller: 'cashManageController'
        }
    });

    app.controller('cashManageController', function ($scope, $element, $filter, $timeout, uiLanguage, INFO, $http, SocketFactory, MainService) {

        var remark = '';



        // $scope.goto = function( mode ) {
        //     MainService.modeCash = MainService.schema.modeCash[mode];

        // }
        // $scope.cashAmount = 0;
        $scope.startAmount = function () {
            // MainService.switchMode2({modeCash:'open'});
        }

        $scope.pettyCashRemark = '';

        $scope.pettyCash = function () {
            // MainService.switchMode2({modeCash:'petty'});
        }

        $scope.selectPetty = function () {

        }

        $scope.init = function () {
            console.log('init cashbox 789');
            $scope.pettyCashRemark = '';
            var _o = { 'staffId': MainService.UserManager.staffCode };
            SocketFactory.emit('prepareDailyCashReport', _o, function (r) {
                // console.log(r);
                $scope.reportData = JSON.parse(r);
                // $scope.cashReport = JSON.parse( r ).CASH_RECORDS;

                $timeout(function () {
                    MainService.scrollToBottom(angular.element('.report-content'));
                }, 10);
            });
        }

        $scope.resetRemark = function () {
            $scope.pettyCashRemark = '';
        }

        MainService.switchMode2({ modeCash: 'open' });
        MainService.sceneInitStatus['cashControl'] = true;
        MainService.initCashControl = $scope.init;
        $scope.init();

        $scope.setAmount = function () {
            // amount is this.cashAmount
            var t = ''
            if (MainService.modeCash == MainService.schema.modeCash.open) {
                t = 'open';
                remark = '';
            } else if (MainService.modeCash == MainService.schema.modeCash.petty) {
                t = 'petty';
                remark = $scope.pettyCashRemark;
            }
            if ($scope.cashAmount == 0) return;
            var _o = { 'cash': parseFloat(String(this.cashAmount).trim().replace(/,/g, '')), 'type': t, 'remark': remark, 'staffId': MainService.UserManager.staffCode };
            SocketFactory.emit('cashControl', _o, function (r) {
                console.log(r);
                $scope.reportData = JSON.parse(r);
                // $scope.cashReport = JSON.parse( r ).CASH_RECORDS;

                $timeout(function () {
                    MainService.scrollToBottom(angular.element('.report-content'));
                    $scope.cashAmount = 0;
                    $scope.pettyCashRemark = '';
                }, 10);
            });
        }

        $scope.inputFree = function (val) {
            // console.log($scope.cashAmount);

            this.appendNumFree(val);
        };

        $scope.appendNumFree = function (val) {

            // do nothing if floating point is already input
            /*var inputValue = String( this.cashAmount )
            if( inputValue === 'undefined')
                inputValue = '';
            console.log(parseFloat( inputValue.concat(val) ));
            this.cashAmount = parseFloat( inputValue.concat(val) );*/
            // default

            // if floating point is activated, not allow to add one character
            // this.inputPrice += '';
            this.cashAmount = this.cashAmount ? this.cashAmount : 0;
            this.cashAmount = String(this.cashAmount).trim().replace(/,/g, ''); // type aware method and meaning is obvious without comment

            // since Angular filter seems malfuncationing or weird formatting after certain big amounts, allow only up to billion
            if (this.cashAmount.replace('.', '').length > 11) {
                // console.log('7');
                this.cashAmount = $filter('number')(parseFloat(this.cashAmount)); // original
                return
            }

            // first time "dot"
            if (this.cashAmount.indexOf('.') === -1 && val === '.') {
                console.log('1');
                // this.cashAmount = this.cashAmount.trim().concat( val );
                this.cashAmount = String($filter('number')(parseFloat(this.cashAmount))) + '.';
                return;
            }

            // already have dot, not allowed
            if (this.cashAmount.indexOf('.') != -1 && val === '.') {
                console.log('2');
                this.cashAmount = $filter('number')(parseFloat(this.cashAmount)); // original
                return
            }

            // already have dot and already have 1 decimal (limited to 1 decimal)
            if (this.cashAmount.indexOf('.') != -1 && !isNaN(parseInt(this.cashAmount[this.cashAmount.length - 1]))) {
                console.log("3");
                this.cashAmount = $filter('number')(parseFloat(this.cashAmount)); // original
                return;
            }

            if (this.cashAmount.trim() == "0") { // 排除預設等值有space的障礙
                console.log("4");
                this.cashAmount = val;
            } else {
                console.log("5");
                console.log(this.cashAmount);
                console.log(this.cashAmount.concat(val));
                this.cashAmount = $filter('number')(parseFloat(this.cashAmount.concat(val)));
            }
        }

        $scope.close = function () {
            MainService.switchMode2({ mode: 'main' }, false);
        }

        $scope.cancel = function () {
            if (this.cashAmount != '' || this.cashAmount != 0)
                this.cashAmount = '';
            else
                MainService.switchMode2({ mode: 'main' });
        }

        $scope.backspace = function () {
            // console.log('here!');
            // $scope.loginValue = "12347";
            var str = String(this.cashAmount).trim().replace(/,/g, '');

            // already have dot, does not allow filtering since . is at the end of string
            if (this.cashAmount.indexOf('.') != -1) {
                console.log("6");

                this.cashAmount = str.substring(0, str.length - 1);
            } else {
                this.cashAmount = $filter('number')(parseFloat(str.substring(0, str.length - 1))); // original
            }

        };

        /*$scope.contentFlexSize = function(){
         // modeChecking([{modeCash: [\'petty\']}])
         // console.log('contentFlexSize');
         // console.log(MainService.modeChecking({mode: ['cashManagement'], modeCash: ['open']}));
         // console.log(MainService.modeChecking({mode: ['cashManagement'], modeCash: ['petty']}));
             if( MainService.modeCash == MainService.modeCash.open ) {
                 console.log('100 -0------------------------------------');
                 return 100;
             }
 
             if( MainService.modeCash == MainService.modeCash.petty ) {
                 console.log('40 -0------------------------------------');
 
                 return 40;
             }
 
             console.log('nothing by default');
        }*/

        $scope.resetPage = function () {
            $element.find('.btn.important.active').removeClass("active");
            MainService.modeCash = MainService.schema.modeCash.normal;
        }
        MainService.resetPage['cashManagement'] = $scope.resetPage;

        // event
        $element.find('.btn.important').on('click', function (e) {
            // console.log(this);
            // console.log(e);
            $scope.reportData = null; // init
            console.log($scope.Data);
            angular.element(this).parents('.row').find('.active.important').removeClass("active");
            angular.element(this).addClass("active");
        });

        $element.find('.petty-list').on('click', '.btn', function (e) {
            angular.element(this).parents('.row').find('.active').removeClass("active");
            angular.element(this).addClass("active");
        });

        $scope.$watch('cashAmount', function (newValue, oldValue) {
            oldValue = String(oldValue).replace(/,/g, '');
            newValue = String(newValue).replace(/,/g, '');

            // access new and old value here
            //console.log("Your former user.name was " + oldValue + ", you're current user name is " + newValue + ".");
            if (isNaN(newValue) || newValue.indexOf('-') != -1) {
                //console.log('debug' + attrs.ngModel);
                //console.log('debug' + scope.login.value);
                $scope.cashAmount = oldValue ? oldValue : '';
            }

            if (newValue.indexOf('.') != -1 && (newValue.match(/\./g) || []).length == 1) {
                console.log('test');
                console.log(newValue);
                //console.log('debug' + attrs.ngModel);
                //console.log('debug' + scope.login.value);
                $scope.cashAmount = newValue;
            } else {
                // keep
                $scope.cashAmount = $filter('number')(String($scope.cashAmount).trim().replace(/,/g, ''));
            }
        }, true);

        // $element.find('input').on('keyup keypress', function(e){
        //     console.log(e);
        //     // console.log(isNaN( parseInt(String.fromCharCode(e.charCode)) ));
        //     //  e.preventDefault();
        //     console.log(e.keyCode);
        //     console.log( String.fromCharCode(e.keyCode) );
        //     $scope.cashAmount = 1
        //     if( isNaN( parseInt(String.fromCharCode(e.keyCode)) ) ) {
        //      console.log('test');
        //      var str = String( $scope.cashAmount );
        //      $scope.cashAmount = $filter('number')(parseFloat( str.substring(0, str.length - 1) ) );
        //     }
        //     // return 'asdasd'
        // });
    });


    app.directive('searchBillScene', function (MainService) {
        return {
            restrict: "E",
            templateUrl: 'tmpl/searchBillScene.html'
        }
    });

    app.directive('mainScene', function ($http, INFO, MainService, uiLanguage) {
        // console.log(MainService.selectPaymentMethod);
        return {
            restrict: "E",
            templateUrl: 'tmpl/main.html',
            link: function (scope, element) {
                if (INFO.lang == "003") {
                    if (angular.element('.menu-container').length != 0) {
                        angular.element('.menu-container').addClass('en');
                        angular.element('.menu-container').closest('md-content.content-area').addClass('en');
                    }

                    //angular.element('.content-area').addClass('en');
                }
            },
            controller: 'mainController'
        }
    });

    app.controller('mainController', function ($scope, INFO, SocketFactory, $element, MainService, $http, $rootScope, uiLanguage, $mdDialog) {

        $scope.MainService = MainService;
    });

    app.directive('orderScene', function ($http, INFO, MainService, uiLanguage) {
        // console.log(MainService.selectPaymentMethod);
        return {
            restrict: "E",
            templateUrl: 'tmpl/order.html',
            link: function (scope, element) {
            },
            controller: 'orderController'
        }
    });

    app.controller('orderController', function ($scope, INFO, SocketFactory, $element, MainService, $http, $rootScope, uiLanguage, $mdDialog) {
        // console.log('orderController');
        // $mdDialog.show();
        $scope.ui = uiLanguage[INFO.lang];
        var watchFood = $scope.$watch(function (scope) { return MainService.food },
              function () {
                  if (MainService.food) {
                      // console.log(MainService.food);
                      watchFood();
                  }
              }
             );
        $scope.MainService = MainService;
        $scope.$on('submitOrder', function (o) {
            $scope.cart.submit(SocketFactory);
        });



        $scope.$watch('MainService.Cart', function (newValue, oldValue) {
            console.log('watcher / MainService.Cart');
            if (newValue === oldValue) return;
            //var totalPrice = 0;
            //var price = 0;
            //$.each(newValue.cartList, function (i, v) {
            //    //if (v.getQty() > 0 && !v.isDelete)
            //        price += v.getTotalPrice();
            //});
            //$.each(newValue.orderList, function (i, v) {
            //    //if (v.getQty() > 0)
            //        price += v.getTotalPrice();
            //});
            //newValue.price = price;
            // console.log(price)
            var couponObj;
            //console.log(scObj);
            //console.log(newValue.couponObj);
            if (MainService.appliedDiscount) {
                if (MainService.appliedDiscount.length != 0) {
                    couponObj = MainService.appliedDiscount;
                }
            }
            //if (newValue.couponObj) {
            //    if (newValue.couponObj.length != 0) {
            //        couponObj = newValue.couponObj;
            //    }
            //}

            var priceObj = MainService.calcPrice([newValue.cartList, newValue.orderList], MainService.Cart.couponObj);

            newValue.price = priceObj.price;

            newValue.serviceCharge = priceObj.serviceCharge;
            newValue.remainings = priceObj.remainings;
            newValue.totalPrice = priceObj.totalPrice;
            localStorage.setItem("customer-display-cart", JSON.stringify(newValue));
        }, true);

        var watchCollections = ['appliedPaymentMethod', 'appliedDiscount', 'remainerAmount', 'isCash', 'mode', 'modeOrder', 'modeDeliveryOrder', 'modeTakeAway', 'modeFastFood', 'combo'];
        watchCollections.forEach(function (watch) {
            $scope.$watch('MainService.' + watch, function (newValue, oldValue) {
                console.log('watcher / MainService.' + watch, newValue);
                if (newValue === oldValue) return;
                localStorage.setItem("customer-display-" + watch, JSON.stringify(newValue));
            }, true);
        });

        $scope.INFO = INFO;
        $scope.currentPage = 1;
        $scope.currentCatsPage = 1;
        $scope.currentSubCatsPage = 1;

        $scope.itemsPerPage = 20;
        $scope.modeOrder = 0;
        $scope.modeOrderDetail = 1;
        $scope.modeFoodControl = 2;



        $scope.cart = {
            tableNo: '',
            cartList: [],
            orderList: [],
            outofstockList: [],
            setOutofstock: function (obj) {
                outofstockList = obj;
                if ($scope.food) {

                }
            },
            assignTableNo: function (tableNo) {
                this.tableNo = tableNo;
                SocketFactory.emit('loadTableOrder', { 'tableNum': { '$value': tableNo } }, function (r) {
                    r = JSON.parse(r);
                    console.log(r.result)
                    if (r.result === "OK") {
                        //console.log(r)
                        $scope.cart.orderList = [];
                        angular.forEach(r.order.item, function (v, k) {
                            //this.push(key + ': ' + value);
                            var item = $.extend(true, {}, $scope.baseItem, v);
                            $scope.cart.orderList.push(item);
                        });
                    }
                })
            },
            submit: function (SocketFactory) {
                console.log('submit');
                console.log('cartList.length', this.cartList.length);
                var order = { item: [] };
                angular.forEach(this.cartList, function (item) {
                    order.item.push({
                        code: [item.code], qty: [item.qty], unitPrice: [item.unitprice], voidIndex: [item.voidIndex], voidRemark: [item.voidRemark], type: ['I'],
                        desc1: [item.getName()], customName: [item.customName]
                    });
                });
                if (order.item.length > 0) {
                    var postData = {
                        "action": "MDSaveTableOrder",
                        "tableNum": {
                            "$value": this.tableNo
                        },
                        "itemData": {
                            "json": {
                                "order": order
                            }
                        }
                    };
                    SocketFactory.emit('saveTableOrder', postData, function (r) {
                        r = JSON.parse(r);
                        //console.log(r.result);
                        SocketFactory.emit('loadTableOrder', { 'tableNum': { '$value': $scope.cart.tableNo } }, function (r) {
                            r = JSON.parse(r);
                            console.log(r.result)
                            if (r.result === "OK") {
                                //console.log(r)
                                $scope.cart.orderList = [];
                                $scope.cart.cartList = [];
                                angular.forEach(r.order.item, function (v, k) {
                                    //this.push(key + ': ' + value);
                                    var item = $.extend(true, {}, $scope.baseItem, v);
                                    $scope.cart.orderList.push(item);
                                });
                            }
                            //console.log(111111);
                            //console.log(r);
                            //$scope.cart
                        })
                    });
                }
            }
        };

        SocketFactory.emit('loadItemQty', '', function (r) {
            // console.log("debug000");
            // console.log('loadItemQty');
            // console.log(r);
            //angular.forEach($scope.cart)
        });

        $scope.selectCategory = function (category) {
            //console.log(category);
            // $scope.previousCategory = $scope.currentCategory;
            $scope.currentCategory = category;
            $scope.currentPage = 1;
            $scope.totalItems = $scope.currentCategory.items.length;
        }

        $scope.changeQty = function (number) {

        };

        $scope.selectItem = function (item) {
            //console.log(item);
        };

        $scope.editItem = function (item) {

        };

        $scope.baseItem = {
            getName: function () {
                if (this.name != undefined) {
                    return this.name[$scope.INFO.lang];
                }
                else {
                    return this.desc1;
                }
            },
            qty: 1,
            voidIndex: -1,
            voidRemark: ''
        }

        $scope.toggleFoodControl = function () {
            if ($element.hasClass('ng-hide')) {
                angular.element('table-scene').scope().Hide();
                $element.removeClass('ng-hide');
            }

            if ($scope.mode != angular.element('order-scene').scope().modeFoodControl) {
                $scope.mode = $scope.modeFoodControl;
            } else {
                $scope.mode = $scope.modeOrder;
                if ($scope.cart.tableNo == '') {
                    angular.element('rms-svg').scope().Show();
                    $element.addClass('ng-hide');
                }
            }
            //angular.element('order-ctrl').scope().mode = angular.element('order-ctrl').scope().modeFoodControl;
        }
    });

    app.directive('loginInput', function ($timeout, $parse, userConfig) {
        return {
            require: '?ngModel',
            link: function (scope, elem, attrs, ngModel) {
                elem.bind("keydown keypress", function (event) {
                    if (event.which === 13) {
                        scope.$apply(attrs.loginInputEnter);
                        event.preventDefault();
                    }
                });
            },
            controller: function ($scope) {
                $scope.loginValue = userConfig.dev ? userConfig.devParam.password : ''; // demo mode

                $scope.change = function (v) {
                    //$scope.counter++;
                    console.log($scope.loginValue);
                };

                $scope.$watch('loginValue', function (newValue, oldValue) {
                    // access new and old value here
                    //console.log("Your former user.name was " + oldValue + ", you're current user name is " + newValue + ".");
                    if (isNaN(newValue) || newValue.indexOf('.') != -1 || newValue.indexOf('-') != -1) {
                        //console.log('debug' + attrs.ngModel);
                        //console.log('debug' + scope.login.value);
                        $scope.loginValue = oldValue ? oldValue : '';
                    }
                    $scope.loginValue = $scope.loginValue.trim();
                }, true);

                $scope.resetLoginInputValue = function () {
                    $scope.loginValue = userConfig.dev ? userConfig.devParam.password : ''; // demo mode
                    //ngModel.$setViewValue('');
                    //ngModel.$render();
                }
            }
        };
    });

    app.directive('userScene', function (MainService, $timeout, uiLanguage, INFO) {
        return {
            restrict: "E",
            templateUrl: 'tmpl/userScene.html',
            link: function (scope, element) {
            },
            controller: 'userController'
            // controller: function ($scope, MainService, $timeout, uiLanguage, INFO) {
            //     // console.log(MainService.schema.modeOrder.billing);
            //     var ui = $scope.ui = uiLanguage[INFO.lang];
            //     $scope.staffTypeList = [
            //         {code:1, name:ui['manager']},
            //         {code:2, name:ui['cashier']},
            //         {code:3, name:ui['staff']}
            //     ];
            //     $timeout(function(){

            //         $('select').material_select();
            //     }, 100)
            // }
        }
    });

    app.controller('userController', function ($scope, $timeout, uiLanguage, INFO, $http, SocketFactory) {
        $scope.ui = uiLanguage[INFO.lang];
        $scope.lang = INFO.lang;
        $scope.currentMode = 'list';

        SocketFactory.emit('loadStaffGroup', null, function (r) {
            console.log(JSON.parse(r));
            $scope.groupData = JSON.parse(r);
            // console.log($scope.groupData[0].staffGroupCode);
        })

        var ui = $scope.ui = uiLanguage[INFO.lang];
        // $scope.staffTypeList = [
        //     {code:1, name:ui['manager']},
        //     {code:2, name:ui['cashier']},
        //     {code:3, name:ui['staff']}
        // ];
        $timeout(function () {
            $('select').material_select();
        }, 100)

        $scope.deleteUser = function (username) {
            SocketFactory.emit('deleteStaff', { username: username }, function (r) {
                r = JSON.parse(r);
                if (r.result == "OK") {
                    SocketFactory.emit('loadStaff', null, function (r) {
                        $scope.Staffs = JSON.parse(r);
                        $scope.currentMode = 'list';
                    })
                } else {

                }
            })
        }

        $scope.editUser = function (username) {
            // console.log($scope.Staffs);
            $scope.currentMode = 'user';
            var foundStaff = $scope.Staffs.filter(function (s) {
                return s.username == username;
            })
            if (foundStaff.length == 0) {
                $scope.currentUser = { 'oldusername': 'new' };
            } else {
                $scope.currentUser = foundStaff[0];
                $scope.currentUser.oldusername = $scope.currentUser.username;
            }
        }

        $scope.submitUser = function () {
            console.log('currentUser', $scope.currentUser)
            var checking = $scope.currentUser.username && $scope.currentUser.name && $scope.currentUser.password && $scope.currentUser.staffGroupCode;

            if (!checking) {
                MainService.showAlert($scope.ui.txt_add_user_fail);
                return;
            }

            SocketFactory.emit('editStaff', $scope.currentUser, function (r) {
                r = JSON.parse(r);
                if (r.result == "OK") {
                    SocketFactory.emit('loadStaff', null, function (r) {
                        $scope.Staffs = JSON.parse(r);
                        $scope.currentMode = 'list';
                    })
                } else {

                }
            })
        }

        SocketFactory.emit('loadStaff', null, function (r) {
            //alert(r);
            $scope.Staffs = JSON.parse(r);
        })
    });

    app.directive('userGroupScene', function (MainService, $compile, $timeout, uiLanguage, INFO, userConfig) {
        return {
            restrict: "E",
            templateUrl: 'tmpl/userGroupScene.html',
            link: function (scope, element, attrs) {
                scope.ui = uiLanguage[INFO.lang];
                scope.lang = INFO.lang;
                scope.permissionList = userConfig.permissionList;
                // scope.permissionListName = userConfig.permissionList;

                // for( var key in scope.permissionListName ) {
                //     scope.permissionListName[key] = scope.ui[scope.permissionListName[key]];
                // }

                scope.renderTable = function (data) {
                    $('#user-group .list-container').html('');

                    // table food list
                    HandlebarsIntl.registerWith(Handlebars);
                    var intlData = {
                        "locales": "en-US"
                    };

                    var contextCategoryList = {};
                    contextCategoryList.caption = scope.ui['add_group'];
                    contextCategoryList.permissionListName = userConfig.permissionList;
                    contextCategoryList.lang = INFO.lang;
                    contextCategoryList.tableHead = [
                      { "name": scope.ui['group_code'], "fieldType": "textfield", "widthClass": "display" },
                      { "name": scope.ui['group_name'], "fieldType": "textfield", "widthClass": "sort-order" }];
                    // { "name": scope.ui['permission'], "fieldType": "textfield", "widthClass": "name1" }];
                    contextCategoryList.tableContent = data;
                    // console.log(contextCategoryList);
                    content = Handlebars.compile($("#tpl-table-user-group").html())(contextCategoryList);

                    content = $compile(content)(scope);
                    $('#user-group .list-container').append(content);
                }

                // console.log(scope);
                // var elmnt;
                // attrs.$observe( 'template', function ( myTemplate ) {
                //   if ( angular.isDefined( myTemplate ) ) {
                //     // compile the provided template against the current scope
                //     elmnt = $compile( myTemplate )( scope );

                //       element.html(""); // dummy "clear"

                //     element.append( elmnt );
                //   }
                // });
            },
            controller: 'userGroupController'
            // controller: function ($scope, MainService, $timeout, uiLanguage, INFO) {
            //     // console.log(MainService.schema.modeOrder.billing);
            //     var ui = $scope.ui = uiLanguage[INFO.lang];
            //     $scope.staffTypeList = [
            //         {code:1, name:ui['manager']},
            //         {code:2, name:ui['cashier']},
            //         {code:3, name:ui['staff']}
            //     ];
            //     $timeout(function(){

            //         $('select').material_select();
            //     }, 100)
            // }
        }
    });

    app.controller('userGroupController', function ($scope, $timeout, $http, SocketFactory, userConfig) {
        // $scope.ui = uiLanguage[INFO.lang];
        // $scope.lang = INFO.lang;
        $scope.currentMode = 'list'; // list, detail, edit

        SocketFactory.emit('loadStaffGroup', null, function (r) {
            console.log(JSON.parse(r));
            $scope.groupData = JSON.parse(r);
            $scope.renderTable($scope.groupData);
        })

        $scope.highlightMe = function (key, $event) {

            if (angular.element('.permission.' + key).hasClass('active')) {
                angular.element('.permission').removeClass('active');
            } else {
                angular.element('.permission').removeClass('active');
                angular.element('.permission.' + key).addClass('active');
            }

            if ($event) {
                // console.log('baseOption');
                $event.stopPropagation();
            }
        }

        // follow p1 to p10, for rendering
        // $scope.permissionList = userConfig.permissionList;
        // console.log($scope.permissionList);
        // {
        //     p1 : 'open_table_and_order',
        //     p2 : 'print_bill',
        //     p3 : 'billing',
        //     p4 : 'void_item',
        //     p5 : 'change_tender',
        //     p6 : 'report',
        //     p7 : 'user_management',
        //     p8 : 'search_bill',
        //     p9 : 'daily_clearance',
        //     p10 : 'cash_management'
        //     // all : 'all_permissions'
        // }

        // $scope.checkedTest = false;
        // $scope.clickTest = function(){
        //     $scope.checkedTest = !$scope.checkedTest;
        //     return $scope.checkedTest;
        // }

        $scope.currentPermissionList = [];
        $scope.checkbox = function (permission) {
            console.log($scope.currentGroup);

        }

        $scope.checkAll = function (permission) {
            // console.log($scope.checkAllStatus);

            if ($scope.checkAllStatus) {
                for (var key in $scope.currentPermissionList) {
                    $scope.currentGroup[key] = true;
                }
            } else {
                for (var key in $scope.currentPermissionList) {
                    $scope.currentGroup[key] = false;
                }
            }
        }

        $scope.uncheckAll = function (permission) {
            for (var key in $scope.currentPermissionList) {
                $scope.currentGroup[key] = false;
            }
        }



        // moved to link, so that all dynamic template can be compiled and using angular merits such as ng-click
        // $scope.renderTable = function ( data ) {
        // ...
        // }

        $scope.delete = function (staffGroupCode) {
            SocketFactory.emit('deleteStaffGroup', { staffGroupCode: staffGroupCode }, function (r) {
                r = JSON.parse(r);
                // if (r.result == "OK") {
                //     SocketFactory.emit('loadStaff', null, function (r) {
                //         $scope.Staffs = JSON.parse(r);
                //         $scope.currentMode = 'list';
                //     })
                // } else {

                // }
            })
        }

        $scope.edit = function (staffGroupCode) {
            // $scope.currentMode = 'user';
            var found = $scope.groupData.filter(function (s) {
                return s.staffGroupCode == staffGroupCode;
            })
            if (found.length == 0) {
                // $scope.currentGroup = {oldGroupCode:-1};
                console.log($scope.currentGroup);
                $scope.currentGroup = {
                    oldGroupCode: -1,
                    p1: false,
                    p2: false,
                    p3: false,
                    p4: false,
                    p5: false,
                    p6: false,
                    p7: false,
                    p8: false,
                    p9: false,
                    p10: false,
                    p11: false,
                    p12: false,
                    p13: false,
                    p14: false,
                    p15: false,
                    p16: false,
                    p17: false,
                    p18: false,
                    p19: false,
                    p20: false,
                    staffGroupCode: "-1",
                    staffGroupName: "NEW"
                }
            } else {
                $scope.currentGroup = found[0];
                // $scope.currentGroup.oldGroupCode = $scope.currentGroup.staffGroupCode;
                console.log($scope.currentGroup);
            }

            for (var key in $scope.currentPermissionList) {
                $scope.currentPermissionList[key] = $scope.currentGroup[key];
            }

            // console.log($scope.currentPermissionList);
        }

        $scope.cancel = function () {
            // MainService.switchMode2({modeUserGroup: 'list'});
            MainService.switchMode2({ mode: 'main' });
        }

        $scope.submit = function () {
            console.log('editStaffGroup', $scope.currentGroup)
            SocketFactory.emit('editStaffGroup', $scope.currentGroup, function (r) {
                r = JSON.parse(r);

                console.log('editStaffGroup result', r);
                // if (r.result == "OK") {
                //     SocketFactory.emit('loadStaff', null, function (r) {
                //         $scope.Staffs = JSON.parse(r);
                //         $scope.currentMode = 'list';
                //     })
                // } else {
                MainService.switchMode2({ mode: 'main' });
                // $scope.renderTable( r );
                // }
            })
        }

        SocketFactory.emit('loadStaff', null, function (r) {
            //alert(r);
            $scope.Staffs = JSON.parse(r);
        })
    });

    app.directive('userClockScene', function (MainService, $timeout) {
        return {
            restrict: "E",
            templateUrl: 'tmpl/userClockScene.html',
            link: function (scope, element) {
            },
            controller: function ($scope, MainService) {
                // console.log(MainService.schema.modeOrder.billing);


            }
        }
    });

    app.directive('tableListScene', function (MainService, $timeout) {
        return {
            restrict: "E",
            templateUrl: 'tmpl/tableListScene.html',
            link: function (scope, element) {
                for (var i = 0; i < 13; i++) {
                    $('.table-list .col:eq(0)').clone().insertBefore($('table-list-scene .md-custom-grid-container'))
                }

            },
            controller: function ($scope, MainService, $timeout, uiLanguage, INFO) {
                // console.log(MainService.schema.modeOrder.billing);


            }
        }
    });

    app.directive('transferItemMenu', function (MainService, $timeout) {
        return {
            restrict: "E",
            templateUrl: 'tmpl/transfer-item.html',
            link: function (scope, element) {


            },
            controller: 'transferItemController'
        }
    });

    app.directive('settingsScene', function (MainService, $compile, $timeout, uiLanguage, INFO, userConfig) {
        return {
            restrict: "E",
            templateUrl: 'tmpl/settings.html',
            link: function (scope, element, attrs) {
                scope.ui = uiLanguage[INFO.lang];
                scope.lang = INFO.lang;
                scope.permissionList = userConfig.permissionList;
            },
            controller: 'settingsController'
        }
    });

    app.controller("settingsController", function ($scope, SocketFactory, MainService) {
        $scope.settings = {};
        $scope.init = function () {
            console.log('load settings');
            SocketFactory.emit('loadSettings', {}, function (r) {
                // console.log(r);
                var config = JSON.parse(r);
                // console.log(config);
                // console.log(config.billPrintTwice);
                $scope.settings.billPrintTwice = config.billPrintTwice || false;
                $scope.settings.paymentPrintTwice = config.paymentPrintTwice || false;
                $scope.settings.useKds = config.useKds || false;
                $scope.settings.takeAwayMode = config.takeAwayMode || false;
                $scope.settings.takeAwayPrintTwice = config.takeAwayPrintTwice || false;
            });
        }
        $scope.init();
        console.log($scope.settings);

        $scope.cancel = function () {
            MainService.switchMode2({ mode: 'main' });
        }

        $scope.submit = function () {
            console.log($scope.settings);
            SocketFactory.emit('changeSettings', $scope.settings, function (r) {
                console.log(r);

                MainService.switchMode2({ mode: 'main' });
                location.reload();

            });
        }
    });

    app.directive('takeAwayScene', function ($compile, $http, $animate, $interval, SocketFactory, MainService, $controller, $timeout, $rootElement, uiLanguage, INFO) {
        return {
            restrict: "E",
            templateUrl: 'tmpl/take-away.html',
            link: function (scope, element, attrs) {
                scope.ui = uiLanguage[INFO.lang];
                scope.lang = INFO.lang;
                scope.permissionList = userConfig.permissionList;

            },
            controller: 'takeAwayController'
            //controller: 'takeAwayController'
        }
    });

    app.directive('fastFoodScene', function ($compile, $http, $animate, $interval, SocketFactory, MainService, $controller, $timeout, $rootElement, uiLanguage, INFO) {
        return {
            restrict: "E",
            templateUrl: 'tmpl/take-away.html',
            link: function (scope, element, attrs) {
                scope.ui = uiLanguage[INFO.lang];
                scope.lang = INFO.lang;
                scope.permissionList = userConfig.permissionList;

            },
            controller: 'takeAwayController'
            //controller: 'takeAwayController'
        }
    });

    app.directive('buildBillFromVoidScene', function ($compile, $http, $animate, $interval, SocketFactory, MainService, $controller, $timeout, $rootElement, uiLanguage, INFO) {
        return {
            restrict: "E",
            templateUrl: 'tmpl/take-away.html',
            link: function (scope, element, attrs) {
                scope.ui = uiLanguage[INFO.lang];
                scope.lang = INFO.lang;
                scope.permissionList = userConfig.permissionList;

            },
            controller: 'takeAwayController'
            //controller: 'takeAwayController'
        }
    });


    app.controller("takeAwayController", function ($scope, MainService, $timeout, uiLanguage, INFO, SocketFactory, $mdDialog, userConfig, $rootElement, $rootScope) {
        var ui = $scope.ui = uiLanguage[INFO.lang];
        $scope.uc = $scope.config = userConfig;
        //console.debug($scope.uc);

        if (!MainService.getOtherWriteOrderService()) {
            switch (MainService.mode) {
                case MainService.schema.mode.takeAway:
                    MainService.initOtherWriteOrderService(MainService.schema.modeOtherWriteOrderService.takeAway);
                    break;
                case MainService.schema.mode.fastFood:
                    MainService.initOtherWriteOrderService(MainService.schema.modeOtherWriteOrderService.fastFood);
                    break;
                case MainService.schema.mode.buildBillFromVoid:
                    MainService.initOtherWriteOrderService(MainService.schema.modeOtherWriteOrderService.buildBillFromVoid);
                    break;
                case MainService.schema.mode.deliveryOrder:
                    MainService.initOtherWriteOrderService(MainService.schema.modeOtherWriteOrderService.deliveryOrder);
                    break;
            }

        }


        if (MainService.getOtherWriteOrderService().isFirst) {
            MainService.getOtherWriteOrderService().resetCartList();
            var modeFieldName = MainService.getOtherWriteOrderService().modeSetting.modeInService.value;
            MainService[modeFieldName] = MainService.schema[modeFieldName].writeOrder;
            MainService.getOtherWriteOrderService().backTakeAwayOrderMode();
            MainService.getOtherWriteOrderService().isFirst = 0;
            return;
        }

        MainService.Cart.init();

        $scope.stop = function () {
            $timeout.cancel($scope.promise);
        };

        $scope.timeout = angular.isDefined(userConfig.takeawayTimeout) ? userConfig.takeawayTimeout : "60000";
        $scope.start = function () {
            $scope.stop();
            $scope.count = 1;
            $scope.promise = $timeout(function () {
                var modeFieldName = MainService.getOtherWriteOrderService().getModeInServiceValue();
                var checkMode = {}
                checkMode[modeFieldName] = 'writeOrder'
                MainService.switchMode2(checkMode, false);
                MainService.getOtherWriteOrderService().backTakeAwayOrderMode();
            }, $scope.timeout);
        };
        $scope.start();


        $rootElement.on('mousedown keyup touch', function (e) {
            console.log('action event');
            $scope.start();
        });

        $scope.$on('$destroy', function () {
            console.log("scope distroyed");
            $scope.stop();
            $rootElement.off('mousedown keyup touch');
        });

        //var modeFieldName = MainService.getOtherWriteOrderService().getModeInServiceValue();
        MainService[MainService.getOtherWriteOrderService().getModeInServiceValue()] = MainService.schema[MainService.getOtherWriteOrderService().getModeInServiceValue()].normal;

        MainService.modeOrderMenu = MainService.schema.modeOrderMenu.normal;
        MainService.setActive(angular.element('.btn-view'));
        $scope.TakeOutOrderList = [];

        var args = {};
        args.serviceType = MainService.getOtherWriteOrderService().modeSetting.mode.value;
        args.till = $scope.uc.page.till;

        $scope.init = function () {
            var tmp = JSON.parse(JSON.stringify(args));
            $scope.setOrderList(tmp);
        }

        $rootScope.$on('refreshBillList', function () {
            $scope.setOrderList(args);
        })
       

        $scope.setOrderList = function (args) {
            console.debug(5201, args);
            SocketFactory.emit('GetAllTakeOutOrder', args, function (result) {
                $scope.TakeOutOrderList = JSON.parse(result);

                $scope.TakeOutOrderList.forEach(function (t) {
                    if (t.customerData != null) {

                        try {
                            t.customerData = JSON.parse(t.customerData);
                        } catch (ex) {
                            t.customerData = {};
                        }

                    }
                })

                console.debug(5217, $scope.TakeOutOrderList);

                if ($scope.TakeOutOrderList.length > 0)
                    $scope.GetOrderTansaction($scope.TakeOutOrderList[0].refNo);
            });
        }
        
        $rootScope.$on('getOrderByPhone', function (event, data) {
            console.debug(5222, data);
            MainService.Cart.init();
            $scope.getOrderByPhone(data.phone);
        })

        $rootScope.$on('initAllOrderInTakeOut', function (event, data) {
            $scope.init();
        })

        $scope.getOrderByPhone = function (phone) {
            var tmp = JSON.parse(JSON.stringify(args));
            tmp.phone = phone;
            $scope.setOrderList(tmp);
        }

        $scope.changeBill = {
            isOpen: false,
            backSearchMode: function () {
                this.isOpen = false;
                return MainService.getOtherWriteOrderService().searchBillMode();
            }
        }

        $scope.setPaymentForLaterPay = function (refNo) {
            $scope.GetOrderTansaction(refNo, function () {
                //$scope.changeBillInfo();
                angular.extend(MainService.Cart.cartList, MainService.Cart.orderList);
                MainService.switchMode2({ modeTakeAway: 'payLater' }, false, 'take_away');
                //MainService.getOtherWriteOrderService().modeSetting.mode == MainService.schema.modeTakeAway.payLater;
                MainService.getOtherWriteOrderService().submitCartInTakeOut();
                MainService.getOtherWriteOrderService().cloneCart = [];
                
            });
        }

        $scope.showCustomerInfo = function (customerData) {
            MainService.getOtherWriteOrderService().takeOutAndDelivery.showInputInfoDialog(true, customerData);
            console.debug(5253, customerData);
        }

        $scope.BillRowClick = function (refNo) {
            $scope.GetOrderTansaction(refNo);

            switch (MainService[MainService.getOtherWriteOrderService().getModeInServiceValue()]) {
                case MainService.schema[MainService.getOtherWriteOrderService().getModeInServiceValue()].normal:
                    break;
                case MainService.schema[MainService.getOtherWriteOrderService().getModeInServiceValue()].printBill:
                    $scope.printBill(refNo);
                    break;
                case MainService.schema[MainService.getOtherWriteOrderService().getModeInServiceValue()].voidBill:
                    $scope.voidBill(refNo);
                    break;
            }

        }

        $scope.isBasicMode = function () {

            return MainService[MainService.getOtherWriteOrderService().getModeInServiceValue()] != MainService.schema[MainService.getOtherWriteOrderService().getModeInServiceValue()].searchBill && MainService[MainService.getOtherWriteOrderService().getModeInServiceValue()] != MainService.schema[MainService.getOtherWriteOrderService().getModeInServiceValue()].amendBill;
        }

        $scope.isSearchMode = function () {

            return MainService[MainService.getOtherWriteOrderService().getModeInServiceValue()] === MainService.schema[MainService.getOtherWriteOrderService().getModeInServiceValue()].searchBill;
        }

        $scope.isAmendBillMode = function () {
            return MainService[MainService.getOtherWriteOrderService().getModeInServiceValue()] === MainService.schema[MainService.getOtherWriteOrderService().getModeInServiceValue()].amendBill;
        }

        $scope.printBill = function (refNo) {
            if (MainService[MainService.getOtherWriteOrderService().getModeInServiceValue()] == MainService.schema[MainService.getOtherWriteOrderService().getModeInServiceValue()].searchBill) {
                refNo = MainService.Cart.refNo;
            }
            SocketFactory.emit('printBill', { "refNo": refNo, printer: userConfig.page.printer }, function (r) {

            })

        }

        $scope.voidBill = function (refNo) {
            if (MainService[MainService.getOtherWriteOrderService().getModeInServiceValue()] == MainService.schema[MainService.getOtherWriteOrderService().getModeInServiceValue()].searchBill) {
                refNo = MainService.Cart.refNo;
            }



            MainService.checkPermission('voidBill', function () {

                var isCard = false;
                var cardType = null;
                MainService.appliedPaymentMethod.forEach(function (m) {
                    var templ = MainService.tender.filter(function (t) {
                        return t.code == m.method;
                    });
                    if (templ[0].tenderType == 'card') {
                        isCard = true;
                        cardType = m.method;
                        return false;
                    }
                });

                var confirm = $mdDialog.confirm()
                           .title(uiLanguage[INFO.lang]['alert'])
                           .content(uiLanguage[INFO.lang]['voidBill_alert_content'])
                           .ok(uiLanguage[INFO.lang]['yes'])
                           .cancel(uiLanguage[INFO.lang]['cancel']);
                $mdDialog.show(confirm).then(function () {

                    var tempArgs = {};
                    tempArgs.refNo = refNo;

                    //tempArgs.refNo = 'ThePantry001-160622-000004';

                    tempArgs.staffCode = MainService.UserManager.staffCode;
                    tempArgs.printer = userConfig.page.printer;
                    tempArgs.isCard = isCard;
                    tempArgs.cardType = cardType;
                    tempArgs.till = $scope.uc.page.till;
                    tempArgs.isUseECR = userConfig.ecrPayment;
                    var showAlert = true;
                    if (isCard && userConfig.ecrPayment) {
                        var content = "請等待";
                        var title = uiLanguage[INFO.lang]['alert'];

                        var alert = $mdDialog.show({
                            template:
                              '<md-dialog aria-label="List dialog">' +
                              '  <md-dialog-content>' +
                              '<h2 class="md-title">' + title + '</h2>' +
                              '<p>' + content + '</p>' +
                              '<button class="md-primary md-button md-default-theme" style="float:right;font-size:18px;margin:0" ng-click="btnClick();">手動</button>' +
                              '  </md-dialog-content>' +
                              '</md-dialog>',
                            controller: function ($scope) {
                                $scope.btnClick = function () {
                                    tempArgs.isUseECR = false;
                                    showAlert = false;
                                    postSocket(tempArgs);
                                }
                            }
                        });

                    }


                    function postSocket(tempArgs) {

                        SocketFactory.emit('voidTakeOutBill', tempArgs, function (r) {
                            console.log('voidTakeOutBill', r);
                            $mdDialog.cancel();
                            if (r == "OK") {

                                $scope.TakeOutOrderList = $.grep($scope.TakeOutOrderList, function (n) {
                                    return n.refNo != refNo;
                                });
                                MainService[MainService.getOtherWriteOrderService().getModeInServiceValue()] = MainService.schema[MainService.getOtherWriteOrderService().getModeInServiceValue()].normal;
                                MainService.Cart.init();
                                MainService.setActive(angular.element('.btn-view'));

                            } else if (r == "fail" && showAlert) {
                                MainService.showAlert('取消帳單失敗');
                            }


                        })
                    }

                    postSocket(tempArgs, true);
                    //console.debug(tempArgs)

                    //return;


                    //SocketFactory.emit('voidTakeOutBill', tempArgs, function (r) {
                    //    console.log('voidTakeOutBill', r);
                    //    $mdDialog.cancel();
                    //    if (r == "OK") {

                    //        $scope.TakeOutOrderList = $.grep($scope.TakeOutOrderList, function (n) {
                    //            return n.refNo != refNo;
                    //        });
                    //        MainService[MainService.getOtherWriteOrderService().getModeInServiceValue()] = MainService.schema[MainService.getOtherWriteOrderService().getModeInServiceValue()].normal;
                    //        MainService.Cart.init();
                    //        MainService.setActive(angular.element('.btn-view'));

                    //    } else if (r == "fail") {
                    //        MainService.showAlert('取消帳單失敗');
                    //    }


                    //})
                });

            })
        }

        $scope.changeBillInfo = function () {
            //console.debug(5381, MainService.Cart.refNo);
            if (MainService.Cart.paymentLater) {
                MainService.showAlert(MainService.label["txt_takeOutAndDelivery_amend_bill_error"]);
                return;
            }
            // MainService.mode = MainService.schema.mode.order;
            MainService.checkPermission('changeTender', function () {
                $scope.changeBill.isOpen = true;
                MainService.getOtherWriteOrderService().amendBillMode();
                MainService.cloneAppliedPaymentMethod = [];
                angular.copy(MainService.appliedPaymentMethod, MainService.cloneAppliedPaymentMethod);
                var cardList = MainService.tender.filter(function (t) {
                    return t.tenderType == 'card';
                });
                MainService.cloneAppliedPaymentMethod.forEach(function (p) {
                    p.void = 0;
                    var templ = cardList.filter(function (c) {
                        return c.code == p.method;
                    });

                    if (templ.length != 0) p.tenderType = 'card';
                })

            })
        }

        $scope.backViewModeInOrder = function () {
            $scope.init();
            MainService.setActive(angular.element('.btn-view'));
            MainService[MainService.getOtherWriteOrderService().getModeInServiceValue()] = MainService.schema[MainService.getOtherWriteOrderService().getModeInServiceValue()].normal
        }

        $scope.getTakeoutOrDinningStatus = function (refNo) {
            var splitList = refNo.split('-');
            if (false)
                MainService.getOtherWriteOrderService().modeSetting.isTakeout = splitList[1] == 'FT' ? 1 : 0;
        }

        $scope.GetOrderTansaction = function (refNo, callback) {
            console.debug(5456, 'getOrderTansaction');
            var args = { refNo: refNo, direction: "equal", searchMode: MainService.getOtherWriteOrderService().modeSetting.mode.value };
            args.till = userConfig.page.till;
            SocketFactory.emit('LoadBillByTakeAway', args, function (r) {
                r = JSON.parse(r);


                //console.debug(5463, r);
                if (typeof r.order[0] == 'undefined')
                    return;


                if (typeof r.order[0] != 'undefined' && r.order[0].header.orderId == '') {
                    $mdDialog.show(
                     $mdDialog.alert()
                       .parent(angular.element(document.body))
                       .title(uiLanguage[INFO.lang]['alert'])
                       .content(uiLanguage[INFO.lang]['no_order'])
                       // .content('There is no order')
                       .ok(uiLanguage[INFO.lang]['ok'])
                   );
                }
                else {
                    var order = r.order[0];
                    bindOrder(order, callback);

                }



                $('.ticket-info').removeClass('active');
                $('.ticket-info[refno=' + refNo + ']').addClass('active');

            })
        }

        $scope.inputFree = function (val) {
            this.appendNumFree(val);
        };

        $scope.lastBill = function () {
            $scope.loadBill(null);
        }

        $scope.searchBill = function () {

            $scope.loadBill({ "searchNo": this.billNo });
        }

        $scope.appendNumFree = function (val) {
            // do nothing if floating point is already input
            var inputValue = String(this.billNo)
            if (inputValue === 'undefined')
                inputValue = '';

            this.billNo = inputValue.concat(val);
            // default
        }

        $scope.cancel = function () {
            this.billNo = '';
        }

        $scope.browseBill = function (direction) {
            // prev
            $scope.loadBill({ "refNo": MainService.Cart.refNo, "direction": direction });
            // next
            //alert(MainService.Cart.refNo)
        }

        $scope.loadBill = function (args) {
            console.debug(5561, 'loadBill');
            if (!args) args = {};
            args.till = userConfig.page.till;
            args.searchMode = MainService.getOtherWriteOrderService().modeSetting.mode.value;
            SocketFactory.emit('LoadBillByTakeAway', args, function (r) {
                r = JSON.parse(r);

                if (typeof r.order[0] == 'undefined')
                    return;


                if (typeof r.order[0] != 'undefined' && r.order[0].header.orderId == '') {
                    $mdDialog.show(
                     $mdDialog.alert()
                       .parent(angular.element(document.body))
                       .title(uiLanguage[INFO.lang]['alert'])
                       .content(uiLanguage[INFO.lang]['no_order'])
                       // .content('There is no order')
                       .ok(uiLanguage[INFO.lang]['ok'])
                   );
                } else {

                    var order = r.order[0];
                    console.log(order);
                    bindOrder(order);
                }
            })
        }

        function bindOrder(order, callback) {
                    MainService.modeMultiSelectItem = false;
                    //MainService.mode = MainService.schema.mode.order;
                    //MainService.modeOrder = MainService.schema.modeOrder.normal;
                    MainService.Cart.tableNo = order.header.tableNum;
                    MainService.Cart.noOfPeople = '';
                    MainService.Cart.refNo = order.header.refNo;
                    MainService.Cart.paymentLater = order.paymentLater;
            if (MainService.getOtherWriteOrderService().modeSetting.mode.value == 'fastFood') {
                $scope.getTakeoutOrDinningStatus(MainService.Cart.refNo);
            }
                    MainService.Cart.transTime = order.header.transTime;
                    MainService.Cart.member = {};
                    //console.log(angular.element('input-keypad').scope());
                    MainService.Cart.cartList = [];
                    MainService.Cart.orderList = [];
                    MainService.appliedPaymentMethod = [];
                    MainService.Cart.couponObj = [];
                    MainService.Cart.noOfPeople = order.header.peopleNum;
            MainService.deposit.appliedDeposit = [];
                    if (order.header.member != undefined) {
                        MainService.Cart.member = $.extend(true, order.header.member, MainService.schema.member);
                    }

                    MainService.assign2OrderList(order, MainService.Cart.orderList);
                    MainService.calcPrice([MainService.Cart.orderList], MainService.Cart.couponObj);
            MainService.Cart.orderList.forEach(function (item) {
                item.isEdit = false;
            })

                    MainService.remainder();
                    MainService.checkPaymentTips();
            if (callback) callback();
        }


        $scope.takeAwayCollected = {
            isCollected: function (remark) {
                var result
                try {
                    var tmp = JSON.parse(remark);
                    result = tmp.isCollected;
                } catch (e) {
                    result = false
                }

                //console.debug(result);
                return [result];
            },
            changeStatus: function (isCollected, refNo) {
                //console.debug(5567, isCollected, refNo);
                //console.debug(5568, idx);

                SocketFactory.emit('changeCollectStatus', { isCollected: isCollected, refNo: refNo }, function (result) {
                    console.debug(result);
                    if (result) {
                        if (result.error) {

                            switch (result.error) {
                                case "error1":
                                    MainService.showAlert('txt_takeAwayCollected_error1');
                                    break;
                            }


                        } else {
                            var tmp = $scope.TakeOutOrderList.filter(function (o) {
                                return o.refNo == refNo;
                            })

                            if (tmp.length != 0) {
                                tmp[0].remark = result.remark;
                            }
                        }
                        
                    }
                });


            }
        };

        $scope.checkingBtnDeleteOfTender = function () {
            if (MainService.multiPayment.isEnable()) {
                if (MainService.deposit.appliedDeposit.length == 0) {
                    if (MainService.appliedPaymentMethod.length == 1) return true;
                    else return false

                }
                else {
                    if (MainService.deposit.appliedDeposit[0].refNo != null) {
                        if (MainService.appliedPaymentMethod.length > 2) return false;
                        else return true
                    }
                    else if (MainService.appliedPaymentMethod.length >= 2) return false;
                    else return true;
                }


            }
            else {
                return true;
            }
        }

    });

    app.directive('deliveryOrder', function ($compile, $http, $animate, $interval, SocketFactory, MainService, $controller, $timeout, $rootElement, uiLanguage, INFO) {
        /*return {
            restrict: "E",
            templateUrl: 'tmpl/delivery-order.html', //tmpl/delivery-order.html
            link: function (scope, element, attrs) {
                scope.ui = uiLanguage[INFO.lang];
                scope.lang = INFO.lang;
                scope.permissionList = userConfig.permissionList;

            }, controller: function ($scope, MainService, $timeout, uiLanguage, INFO, SocketFactory, $mdDialog, userConfig) {
                var ui = $scope.ui = uiLanguage[INFO.lang];
                $scope.uc = $scope.config = userConfig;

                MainService.Cart.init();

                if (MainService.deliveryOrderService.isFirst) {
                    MainService.deliveryOrderService.resetCartList();
                    MainService.modeDeliveryOrder = MainService.schema.modeDeliveryOrder.writeOrder;
                    MainService.deliveryOrderService.takeOrderMode();
                    MainService.deliveryOrderService.isFirst = 0;
                    return;
                }

                MainService.modeDeliveryOrder = MainService.schema.modeDeliveryOrder.normal;
                MainService.modeOrderMenu = MainService.schema.modeOrderMenu.normal;
                MainService.toggleActive(angular.element('.btn-view'));
                $scope.deliveryOrderList = [];

                $scope.isBasicMode = function () {

                    return !MainService.isModeDeliveryOrder('searchBill') && !MainService.isModeDeliveryOrder('amendBill');
                }

                $scope.isSearchMode = function () {

                    return MainService.isModeDeliveryOrder('searchBill');
                }

                $scope.isAmendBillMode = function () {
                    return MainService.isModeDeliveryOrder('amendBill');
                }



                SocketFactory.emit('GetAllDeliveryOrder', null, function (result) {
                    $scope.deliveryOrderList = JSON.parse(result);
                    if ($scope.deliveryOrderList.length > 0)
                        $scope.GetOrderTansaction($scope.deliveryOrderList[0].refNo);
                });



                $scope.GetOrderTansaction = function (refNo) {

                    var args = { refNo: refNo, direction: "equal", searchMode: 'take-away' };
                    SocketFactory.emit('LoadBillByDelivery', args, function (r) {
                        r = JSON.parse(r);

                        if (typeof r.order[0] == 'undefined')
                            return;


                        if (typeof r.order[0] != 'undefined' && r.order[0].header.orderId == '') {
                            $mdDialog.show(
                             $mdDialog.alert()
                               .parent(angular.element(document.body))
                               .title(uiLanguage[INFO.lang]['alert'])
                               .content(uiLanguage[INFO.lang]['no_order'])
                               // .content('There is no order')
                               .ok(uiLanguage[INFO.lang]['ok'])
                           );
                        } else {
                            var order = r.order[0];
                            MainService.modeMultiSelectItem = false;
                            //MainService.mode = MainService.schema.mode.order;
                            //MainService.modeOrder = MainService.schema.modeOrder.normal;

                            MainService.Cart.tableNo = order.header.tableNum;
                            MainService.Cart.noOfPeople = '';
                            MainService.Cart.refNo = order.header.refNo;
                            MainService.Cart.transTime = order.header.transTime;
                            MainService.Cart.member = {};
                            //console.log(angular.element('input-keypad').scope());
                            MainService.Cart.cartList = [];
                            MainService.Cart.orderList = [];
                            MainService.appliedPaymentMethod = [];
                            MainService.Cart.couponObj = [];
                            MainService.Cart.noOfPeople = order.header.peopleNum;
                            if (order.header.member != undefined) {
                                MainService.Cart.member = $.extend(true, order.header.member, MainService.schema.member);
                            }
                            MainService.assign2OrderList(order, MainService.Cart.orderList);
                            MainService.calcPrice([MainService.Cart.orderList], MainService.Cart.couponObj);
                            MainService.Cart.orderList.forEach(function (item) {
                                item.isEdit = false;
                            })
                            MainService.remainder();
                            MainService.checkPaymentTips();
                        }



                        $('.ticket-info').removeClass('active');
                        $('.ticket-info[refno=' + refNo + ']').addClass('active');

                    })
                }


                $scope.BillRowClick = function (refNo) {
                    $scope.GetOrderTansaction(refNo);
                    switch (MainService.modeDeliveryOrder) {
                        case MainService.schema.modeDeliveryOrder.normal:
                            break;
                        case MainService.schema.modeDeliveryOrder.printBill:
                            $scope.printBill(refNo);
                            break;
                        case MainService.schema.modeDeliveryOrder.voidBill:
                            $scope.voidBill(refNo);
                            break;
                    }

                }

                $scope.printBill = function (refNo) {
                    if (MainService.isModeDeliveryOrder('printBill')) {
                        refNo = MainService.Cart.refNo;
                    }
                    SocketFactory.emit('printBill', { "refNo": refNo, printer: userConfig.page.printer }, function (r) {

                    })

                }

                $scope.changeBillInfo = function () {
                    // MainService.mode = MainService.schema.mode.order;
                    MainService.checkPermission('changeTender', function () {
                        MainService.deliveryOrderService.amendBillMode();
                    })
                }


                $scope.voidBill = function (refNo) {
                    if (MainService.isModeDeliveryOrder('searchBill')) {
                        refNo = MainService.Cart.refNo;
                    }


                    MainService.checkPermission('voidBill', function () {

                        var confirm = $mdDialog.confirm()
                                   .title(uiLanguage[INFO.lang]['alert'])
                                   .content(uiLanguage[INFO.lang]['voidBill_alert_content'])
                                   .ok(uiLanguage[INFO.lang]['yes'])
                                   .cancel(uiLanguage[INFO.lang]['cancel']);
                        $mdDialog.show(confirm).then(function () {

                            var tempArgs = {};
                            tempArgs.refNo = refNo;
                            tempArgs.staffCode = MainService.UserManager.staffCode;
                            tempArgs.printer = userConfig.page.printer;
                            SocketFactory.emit('voidTakeOutBill', tempArgs, function (r) {
                                console.log('voidTakeOutBill', r);

                                if (r == "OK") {

                                    $scope.deliveryOrderList = $.grep($scope.deliveryOrderList, function (n) {
                                        return n.refNo != refNo;
                                    });

                                    MainService.modeDeliveryOrder = MainService.schema.modeDeliveryOrder.normal;
                                    MainService.Cart.init();
                                    MainService.toggleActive(angular.element('.btn-view'));

                                }


                            })
                        });

                    })
                }

            }
        }*/
        return {
            restrict: "E",
            templateUrl: 'tmpl/take-away.html',
            link: function (scope, element, attrs) {
                scope.ui = uiLanguage[INFO.lang];
                scope.lang = INFO.lang;
                scope.permissionList = userConfig.permissionList;

            },
            controller: 'takeAwayController'
            //controller: 'takeAwayController'
        }
    })


    app.directive('scanQrcodeScene', function ($compile, $http, $animate, $interval, SocketFactory, MainService, $controller, $timeout, $rootElement, uiLanguage, INFO) {
        return {
            restrict: "E",
            templateUrl: 'tmpl/scan-qrcode-scene.html',
            link: function (scope, element, attrs) {
                scope.ui = uiLanguage[INFO.lang];
                scope.lang = INFO.lang;
                scope.permissionList = userConfig.permissionList;
                $timeout(function () {
                    element.find('#code').focus();
                }, 100);
            },
            controller: function ($scope, MainService, $timeout, uiLanguage, INFO, SocketFactory, $mdDialog) {
                var ui = $scope.ui = uiLanguage[INFO.lang];
                $scope.QR_Code = "";


                $scope.payBill = function () {
                    if ($scope.QR_Code != "" || $scope.QR_Code != null) {
                        $scope.QR_Code = $scope.QR_Code.split('_')[0];
                        SocketFactory.emit('GetTableCodeByTableSubCode', $scope.QR_Code, function (result) {
                            result = JSON.parse(result);
                            var tableCode = result[0].tableCode + "_" + $scope.QR_Code;
                            console.log(tableCode);
                            //MainService.billTable(tableCode, 'QRbilling');
                            MainService.assignTableNo(tableCode, null, 'QRbilling', true, null);
                        });
                    }
                }

                $scope.printBill = function () {
                    if ($scope.QR_Code != "" || $scope.QR_Code != null) {
                        $scope.QR_Code = $scope.QR_Code.split('_')[0];
                        SocketFactory.emit('GetTableCodeByTableSubCode', $scope.QR_Code, function (result) {
                            result = JSON.parse(result);
                            var tableCode = result[0].tableCode + "_" + $scope.QR_Code;
                            MainService.printOrder(tableCode, 'bill');
                        });
                    }


                }
            }
            //controller: 'takeAwayController'
        }
    });

    app.directive('sideNavLeft', function (MainService, uiLanguage, INFO, userConfig, $log) {
        return {
            restrict: "E",
            templateUrl: 'tmpl/sidenav.html',
            link: function (scope, element, attrs) {
                // console.log($log);
                // console.log(element.controller());
                scope.ui = uiLanguage[INFO.lang];
                scope.lang = INFO.lang;
                scope.permissionList = userConfig.permissionList;
            }
            // controller: 'sideMenuCtrl as vm'
        }
    });
    // .controller('navLeftCtrl', function ($scope, $mdSidenav, $log, menu) {
    //     $scope.close = function () {
    //       $mdSidenav('left').close()
    //         .then(function () {
    //           $log.debug("close LEFT is done");
    //         });
    //     };

    //    var vm = this;

    //    //functions for menu-link and menu-toggle
    //    vm.isOpen = isOpen;
    //    vm.toggleOpen = toggleOpen;
    //    vm.autoFocusContent = false;
    //    vm.menu = menu;

    //    vm.status = {
    //      isFirstOpen: true,
    //      isFirstDisabled: false
    //    };

    //    function isOpen(section) {
    //      return menu.isSectionSelected(section);
    //    }

    //    function toggleOpen(section) {
    //      menu.toggleSelectSection(section);
    //    }
    // });

    app.controller('sideMenuCtrl', function ($scope, $rootScope, $log, $timeout, $location, menu) {
        $scope.close = function () {
            $mdSidenav('left').close()
              .then(function () {
                  $log.debug("close LEFT is done");
              });
        };

        // console.log('sideMenuCtrl');
        var vm = this;
        // var aboutMeArr = ['Family', 'Location', 'Lifestyle'];
        // var budgetArr = ['Housing', 'LivingExpenses', 'Healthcare', 'Travel'];
        // var incomeArr = ['SocialSecurity', 'Savings', 'Pension', 'PartTimeJob'];
        // var advancedArr = ['Assumptions', 'BudgetGraph', 'AccountBalanceGraph', 'IncomeBalanceGraph'];

        //functions for menu-link and menu-toggle
        vm.isOpen = isOpen;
        vm.toggleOpen = toggleOpen;
        vm.autoFocusContent = false;
        vm.menu = menu;

        vm.status = {
            isFirstOpen: true,
            isFirstDisabled: false
        };

        function isOpen(section) {
            return menu.isSectionSelected(section);
        }

        function toggleOpen(section) {
            menu.toggleSelectSection(section);
        }

        // console.log(vm);
    });

    app.directive('homeScene', function (MainService, uiLanguage, INFO, userConfig, $log) {
        return {
            restrict: "E",
            templateUrl: 'tmpl/home-scene.html',
            link: function (scope, element, attrs) {
                // console.log($log);
                // console.log(element.controller());
                scope.ui = uiLanguage[INFO.lang];
                scope.lang = INFO.lang;
                scope.permissionList = userConfig.permissionList;
            },
            controller: "homeSceneCtrl"
        }
    });

    app.controller('homeSceneCtrl', function ($scope, uiLanguage, $rootScope, $log, $timeout, $location) {
        var ui = $scope.ui = uiLanguage[INFO.lang];
        //console.log(MainService.UserManager.checkLogin());DialogControllerLogin
        // $scope.keypad = keypad; MainService.switchMode2({mode: 'main'}, false);

    });


    app.directive('categorySetting', function ($compile, $http, $animate, $interval, SocketFactory, MainService, $controller, $timeout, $rootElement, uiLanguage, INFO) {
        return {
            restrict: "E",
            templateUrl: 'tmpl/category-setting.html', //tmpl/delivery-order.html
            link: function (scope, element, attrs) {
                scope.ui = uiLanguage[INFO.lang];
                scope.lang = INFO.lang;
                scope.permissionList = userConfig.permissionList;


                $('ul.tabs.basic-tabs').each(function () {
                    var $active, $content, $links = $(this).find('a');
                    $(this).children('li:eq(0)').css('border-bottom', '1px solid #ee6e73');
                    $active = $($links.filter('[href="' + location.hash + '"]')[0] || $links[0]);
                    $active.addClass('active');

                    $content = $($active[0].hash);
                    $links.not($active).each(function () {
                        $(this.hash).hide();
                    });

                    $(this).on('click', 'a', function (e) {
                        var mainThis = $(this);
                        var mainContent = $(this.hash);
                        e.preventDefault();

                        $(this).closest('ul').children('li').find('a').removeClass('active');
                        $(this).closest('ul').children('li').find('a').each(function () {
                            $(this).closest('li').css('border-bottom-width', '0px');
                            $(this.hash).hide();
                        })
                        $(this).closest('li').css('border-bottom', '1px solid #ee6e73');
                        $(this).addClass('active');
                        $(this.hash).show();



                    });


                });


            },
            controller: function ($scope, MainService, $timeout, uiLanguage, INFO, SocketFactory, $mdDialog, userConfig) {
                $scope.ui = uiLanguage[INFO.lang];
                $scope.getData = function (mode) {
                    $http.get('data/' + mode.filename + '.json').then(function (result) {
                        console.log(result);
                        SocketFactory.emit('GetCategory', null, function (r) {
                            r = JSON.parse(r);
                            console.log('GetCategory', r);
                            mode.notInCategoryList = [];
                            mode.notInCategoryList = r;
                            mode.categoryList = [];
                            result.data.category.sort((a, b) => {
                                if (a.code != b.code) { return a.code - b.code }
                            });

                            result.data.category.forEach(function (item) {
                                r.forEach(function (cate) {
                                    if (cate.code == item.code) {
                                        mode.categoryList.push(cate);
                                        return false;
                                    }

                                })
                            });



                            mode.categoryList.forEach(function (cate) {
                                mode.notInCategoryList = mode.notInCategoryList.filter(function (item) {
                                    return item.code != cate.code;
                                })
                            })

                        })
                    });
                }

                $scope.addCategory = function (mode, cate, $index) {
                    var tempAddList = [];
                    //angular.extend(tempAddList, mode.categoryList);


                    mode.categoryList.forEach(function (item) {
                        tempAddList.push({ code: item.code, name1: item.name1, name2: item.name2 });
                    });

                    tempAddList.push({ code: cate.code, name1: cate.name1, name2: cate.name2 });
                    console.log(tempAddList);
                    var args = {};
                    args.category = [];
                    args.category = tempAddList;
                    args.filename = mode.filename;
                    SocketFactory.emit('changeCategorySetting', args, function (r) {
                        if (r == "OK") {
                            mode.categoryList.push({ code: cate.code, name1: cate.name1, name2: cate.name2 });
                            mode.notInCategoryList.splice($index, 1);
                        }
                    });
                }

                $scope.removeCategory = function (mode, cate, $index) {
                    var tempAddList = [];
                    //angular.extend(tempAddList, mode.categoryList);

                    mode.categoryList.forEach(function (item) {
                        tempAddList.push({ code: item.code, name1: item.name1 });
                    });

                    tempAddList.splice($index, 1);
                    console.log(tempAddList);

                    var args = {};
                    args.category = [];
                    args.category = tempAddList;
                    args.filename = mode.filename;

                    SocketFactory.emit('changeCategorySetting', args, function (r) {
                        if (r == "OK") {
                            mode.categoryList.splice($index, 1);
                            mode.notInCategoryList.push(cate);
                        }
                    });
                }

                $scope.resetCategory = function (mode) {
                    SocketFactory.emit('ResetCategorySetting', { 'filename': mode.filename }, function (r) {
                        if (r == "OK") {
                            mode.getData();
                        }
                    });
                }

                $scope.takeAway = {
                    filename: 'takeaway-category',
                    categoryList: [],
                    notInCategoryList: [],
                    getData: function () {
                        $scope.getData(this);
                    },
                    addCategory: function (cate, $index) {
                        $scope.addCategory(this, cate, $index);
                    },
                    removeCategory: function (cate, $index) {
                        $scope.removeCategory(this, cate, $index);
                    },
                    resetCategory: function () {
                        $scope.resetCategory(this);
                    }
                };

                $scope.eat = {
                    filename: 'eat-category',
                    categoryList: [],
                    notInCategoryList: [],
                    getData: function () {
                        $scope.getData(this);
                    },
                    addCategory: function (cate, $index) {
                        $scope.addCategory(this, cate, $index);
                    },
                    removeCategory: function (cate, $index) {
                        $scope.removeCategory(this, cate, $index);
                    },
                    resetCategory: function () {
                        $scope.resetCategory(this);
                    }
                };

                $scope.delivery = {
                    filename: 'delivery-category',
                    categoryList: [],
                    notInCategoryList: [],
                    getData: function () {
                        $scope.getData(this);
                    },
                    addCategory: function (cate, $index) {
                        $scope.addCategory(this, cate, $index);
                    },
                    removeCategory: function (cate, $index) {
                        $scope.removeCategory(this, cate, $index);
                    },
                    resetCategory: function () {
                        $scope.resetCategory(this);
                    }
                };

                $scope.takeAway.getData();
                $scope.eat.getData();
                $scope.delivery.getData();





                //
            }
        }

    });
})();