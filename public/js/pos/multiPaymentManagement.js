﻿(function () {
    var app = angular.module('multiPaymentModule', ['ngMaterial', 'ui.bootstrap', 'tmpl/bill-numeric-keypad-multiPay.html']).run(function (MainService, multiPayment) {
        MainService.multiPayment = multiPayment;
    })

    angular.module('tmpl/bill-numeric-keypad-multiPay.html', []).run(["$templateCache", "$templateRequest", function ($templateCache, $templateRequest) {
        $templateRequest('tmpl/multi-payment-keypad.html').then(function (response) {
            $templateCache.put('tmpl/bill-numeric-keypad-multiPay.html', response);
        });
    }]);

    app.service("multiPayment", function ($mdDialog, MainService) {
        var multiPayment = {
            isEnable: function () {
                return userConfig.page.multiPaymentPanel && userConfig.page.individualPayment;
            },
            tender: function () {
                var result = [];
                result = MainService.tender.filter(function (t) {
                    return t.code != 'multipay' && t.code != 'payLater' && t.code != 'deposit';
                })

                return result;
            },
            scope: null,
            show: function () {
                var alert = $mdDialog.show({
                    templateUrl: '/tmpl/multipayment.html',
                    //escapeToClose: false,
                    controller: 'multiPaymentController'

                });
            },
            getAmount: function (mode) {
                var value = 0;
                MainService.appliedPaymentMethod.forEach(function (m, i) {
                    /*if (i == 0 && MainService.deposit.appliedDeposit.length != 0 && MainService.deposit.appliedDeposit[0].refNo != null) return true;
                    else value += m[mode]*/
                    value += m[mode]
                })

                if (MainService.deposit.appliedDeposit.length != 0) {
                    if(MainService.deposit.appliedDeposit[0].refNo == null)
                        value += MainService.deposit.appliedDeposit[0].paymentPrice;
                }

                /*
                var submitChecking = false;
                for (var i = 0; i < arguments.length; i++) {
                    if (typeof arguments[i] == 'object') {
                        if (arguments[i].submitChecking == true) {
                            submitChecking = true;
                        }
                    }
                }

                if (MainService.deposit.appliedDeposit.length != 0 && mode == 'totalAmount' && submitChecking) {
                    value += MainService.deposit.appliedDeposit[0].paymentPrice;
                }*/

                return value;
            },
            getRemainder: function (mode) {
                var value = 0;

                MainService.appliedPaymentMethod.forEach(function (m) {
                    m.remainder = parseFloat(m.amount) - parseFloat(m.totalAmount);
                    if (m.method != 'cash' && mode == 'tips') value += m.remainder;
                    else if (mode != 'tips' && m.method == 'cash') value += m.remainder;
                })

                return value;
            },
            getRemainingPayAmount: function () { //Get 
                var value = 0;
                MainService.appliedPaymentMethod.forEach(function (m) {
                    value += m.totalAmount;
                })

                value = value - MainService.Cart.totalPrice;

                if (MainService.deposit.appliedDeposit.length != 0 && MainService.deposit.appliedDeposit[0].refNo == null) {
                    value += MainService.deposit.appliedDeposit[0].paymentPrice;
                }

                if (value > 0) value = 0;
                return value;
            },
        }
        return multiPayment;
    })

    app.controller("multiPaymentController", function ($scope, MainService, uiLanguage, INFO, $mdDialog) {

        $scope.MainService = MainService;

        $scope.ui = uiLanguage[INFO.lang];

        var langNameField = "";

        switch (INFO.lang) {
            case "003":
                langNameField = "name2";
                break;
            default:
                langNameField = "name1";
        }

        $scope.paymentPerPage = 10;
        $scope.currentPaymentPage = 1;
        $scope.noOfPaymentPage = Math.ceil(MainService.multiPayment.tender().length / $scope.paymentPerPage);

        $scope.reminderPage = MainService.getPageNumber($scope.paymentPerPage - (MainService.multiPayment.tender().length) % $scope.paymentPerPage);

        $scope.mode = 'keypad';

        $scope.closeDialog = function () {
            $mdDialog.cancel();
        }

        $scope.changeMode = function (mode) {
            $scope.mode = mode;
        }

        $scope.changeCurrentTender = function (tender, type) {

            if (tender == undefined) return;

            MainService.appliedPaymentMethod.forEach(function (t) {
                t.isActive = false;
                t.activeField = "";
            })


            tender.isActive = true;
            tender.activeField = type;
            tender.firstClick = true;
            $scope.currentTender = tender;
            $scope.mode = 'keypad'
        }

        $scope.init = function () {

            $scope.tender = []

            MainService.multiPayment.scope = $scope;


            if (MainService.appliedPaymentMethod.length == 1) {
                //MainService.appliedPaymentMethod[0].totalAmount = 0;
                //delete MainService.appliedPaymentMethod[0].totalAmount;
                if (Math.abs(MainService.multiPayment.getRemainingPayAmount()) == MainService.Cart.totalPrice) {
                    MainService.appliedPaymentMethod[0].totalAmount = Math.abs(MainService.multiPayment.getRemainingPayAmount());
                }
                /*else if (MainService.deposit.appliedDeposit.length != 0) {
                    MainService.appliedPaymentMethod[0].totalAmount = MainService.Cart.totalPrice - MainService.deposit.appliedDeposit[0].paymentPrice
                }*/

            }

            MainService.appliedPaymentMethod.forEach(function (m, idx) {
                if (!m.totalAmount) m.totalAmount = 0;
                //if (m.totalAmount == 0 && m.amount != 0) m.totalAmount = m.amount;
                m.remainder = parseFloat(m.amount) - parseFloat(m.totalAmount);
                m.isActive = false;
                m.activeField = "";
                if (!m.name) {
                    m.name = MainService.tender.filter(function (t) {
                        return m.method == t.code;
                    })[0][langNameField];
                }
            })

            if(MainService.deposit.appliedDeposit.length == 0)
                $scope.changeCurrentTender(MainService.appliedPaymentMethod[0], 'totalAmount');
            else 
                $scope.changeCurrentTender(MainService.appliedPaymentMethod[1], 'totalAmount');

        }

        $scope.deleteMethod = function (i) {
            MainService.appliedPaymentMethod.splice(i, 1);
            MainService.remainder();
            if (MainService.appliedPaymentMethod.length != 0)
            if (MainService.appliedPaymentMethod[0].totalAmount == undefined)
                MainService.appliedPaymentMethod[0].totalAmount = 0;
        }

        $scope.addMethod = function (method) {

            if (MainService.multiPayment.getRemainingPayAmount() == 0) return;

            var last = MainService.appliedPaymentMethod[MainService.appliedPaymentMethod.length - 1];

            if (last != undefined && last.totalAmount == 0 && last.amount < last.totalAmount) {
                $scope.changeCurrentTender(last, 'totalAmount');
                return;
            }

            if (last != undefined && last.totalAmount == 0 && last.amount == 0) {
                MainService.appliedPaymentMethod.splice(MainService.appliedPaymentMethod.length - 1, 1);
                //MainService.remainder();
            }

            MainService.appliedPaymentMethod.push({
                method: method.code,
                name: method[langNameField],
                tenderType: method.tenderType,
                amount: 0,
                totalAmount: 0,
                remainder: 0,
            });

            last = MainService.appliedPaymentMethod[MainService.appliedPaymentMethod.length - 1];
            last.totalAmount = Math.abs(MainService.multiPayment.getRemainingPayAmount());
            last.amount = last.totalAmount;
            MainService.remainder();
            $scope.changeCurrentTender(last, 'totalAmount');

        }

        $scope.submit = function () {
            $scope.closeDialog();

            var temp = '<md-dialog aria-label="List dialog">' +
            '  <md-dialog-content>' +
            '<h2 class="md-title">' + $scope.ui.alert + '</h2>' +
            '<p>' + '##content##' + '</p>' +
            '<button class="md-primary md-button md-default-theme" style="float:right;font-size:18px;margin:0" ng-click="btnClick();">' + $scope.ui.yes + '</button>' +
            '  </md-dialog-content>' +
            '</md-dialog>';

            var func = function () {
                MainService.multiPayment.show();
                return;
            }

            function dialogShow(content) {
                var alert = $mdDialog.show({
                    template: content,
                    controller: function ($scope) {
                        $scope.btnClick = func;
                    }
                });
            }

            if (MainService.appliedPaymentMethod[0].totalAmount == 0) return;

            if (MainService.deposit.appliedDeposit.length != 0 && MainService.deposit.appliedDeposit[0].paymentPrice > MainService.Cart.totalPrice) {
                if (MainService.deposit.appliedDeposit[0].refNo != null && MainService.appliedPaymentMethod.length == 1) {
                    return;
                } else if (MainService.deposit.appliedDeposit[0].refNo == null && MainService.appliedPaymentMethod.length == 0) return;
            }

            var stop = false

            if ((MainService.multiPayment.getAmount('totalAmount', {submitChecking:true}) < MainService.Cart.totalPrice)) {
                console.debug('txt_multipay_error1')
                var content = temp.replace('##content##', $scope.ui.txt_multipay_error1);
                dialogShow(content);
                return;
            }

            if ((MainService.multiPayment.getAmount('totalAmount') > MainService.Cart.totalPrice)) {
                console.debug('txt_multipay_error2')
                var content = temp.replace('##content##', $scope.ui.txt_multipay_error2);
                dialogShow(content);
                return;
            }

            MainService.appliedPaymentMethod.forEach(function (m) {
                if (m.amount < m.totalAmount) {
                    console.debug('txt_multipay_error3')
                    var content = temp.replace('##content##', $scope.ui.txt_multipay_error3);
                    dialogShow(content);
                    stop = true;
                    return false;
                }
                else if ((m.totalAmount == 0 && m.amount != 0)) {
                    console.debug('txt_multipay_error4')
                    var content = temp.replace('##content##', $scope.ui.txt_multipay_error4);
                    dialogShow(content);
                    stop = true;
                    return false;
                }
            })

            if (stop) return;
            //$scope.tender.forEach(function (t) {
            //    if (t.payAmount != 0 && t.totalAmount != 0) {
            //        MainService.appliedPaymentMethod.push({
            //            amount: parseInt(t.payAmount),
            //            method: t.code,
            //            name: t.name1,
            //            tenderType: t.tenderType
            //        })
            //    }
            //});

            //if (MainService.appliedPaymentMethod[0].amount == 0) MainService.appliedPaymentMethod.splice(0, 1);

            //MainService.remainder();
        }

    })

    app.controller("multiPayKeypadController", function ($scope, $controller, $mdDialog, $element, $attrs, $timeout, uiLanguage, INFO, MainService, SocketFactory, $rootScope, $rootElement) {

        $scope.ui = uiLanguage[INFO.lang];
        $scope.appendAmount = function (amount) {

            var currentTender = MainService.multiPayment.scope.currentTender;
            console.debug(currentTender);
            if (!currentTender) return;

            if (currentTender.firstClick) {
                currentTender[currentTender.activeField] = 0;
                currentTender.firstClick = false;
            }

            console.debug(amount);

            var inputPrice = currentTender[currentTender.activeField];

            if (amount === '.' && inputPrice[inputPrice.length - 1] === '.') {
                return;
            }

            inputPrice = String(inputPrice);

            if (inputPrice.indexOf('.') != -1 && !isNaN(parseInt(inputPrice[inputPrice.length - 1]))) {
                return;
            }

            if (inputPrice.trim() == "0") {
                inputPrice = amount;
            }
            else if (inputPrice.indexOf('.') === -1 && amount === '.') {
                $scope.floatingInput = true;
                inputPrice = inputPrice.trim().concat(amount);
            }
            else {
                inputPrice = inputPrice.concat(amount);
            }

            console.debug(typeof inputPrice);
            if (typeof inputPrice == 'string')
                if (inputPrice.indexOf('.') != inputPrice.length - 1)
                    inputPrice = Number(inputPrice);

            //var remainingPayAmount = MainService.multiPayment.getRemainingPayAmount();
            //if (remainingPayAmount != 0) remainingPayAmount = parseFloat(Math.abs(remainingPayAmount));


            var max = MainService.Cart.totalPrice - MainService.multiPayment.getAmount('totalAmount') + currentTender.totalAmount;
            max = parseFloat(Math.abs(max));

            if (currentTender.activeField == 'totalAmount') {

                if (inputPrice > max) {
                    currentTender.totalAmount = max;
                    currentTender.amount = max;
                    //if (currentTender.totalAmount != 0 && false) {
                    //    currentTender.totalAmount += remainingPayAmount;
                    //}
                    //else {
                    //    currentTender.totalAmount = max;
                    //}
                } else {
                    currentTender[currentTender.activeField] = inputPrice;
                    currentTender.amount = inputPrice;
                }

            }
            else if (currentTender.method.toLowerCase().indexOf('octopus') != -1) {

                if (inputPrice > max) {
                    currentTender.totalAmount = max;
                    currentTender.amount = max;
                    //if (currentTender.totalAmount != 0 && false) {
                    //    currentTender.totalAmount += remainingPayAmount;
                    //}
                    //else {
                    //    currentTender.totalAmount = max;
                    //}
                } else {
                    currentTender.totalAmount = inputPrice;
                    currentTender.amount = inputPrice;
                }


            }
            else if (currentTender.activeField == 'amount' && currentTender.totalAmount != 0) {
                currentTender[currentTender.activeField] = inputPrice;
            }


            $scope.calcPrice();
        }

        $scope.truncateAmount = function () {

            var currentTender = MainService.multiPayment.scope.currentTender;
            if (currentTender == undefined) return;
            var inputPrice = currentTender[currentTender.activeField];

            if (inputPrice[inputPrice.length - 1] === '.') {
                $scope.floatingInput = false;
            }
            // convert it to string or it will get error when it is 0 and use backspace
            inputPrice = String(inputPrice);
            inputPrice = inputPrice.substring(0, inputPrice.length - 1);

            if (isNaN(inputPrice) || inputPrice === '') {
                inputPrice = 0;
            }

            inputPrice = parseFloat(inputPrice);

            currentTender[currentTender.activeField] = inputPrice;
            $scope.calcPrice();
        }

        $scope.calcPrice = function () {
            var currentTender = MainService.multiPayment.scope.currentTender;
            currentTender.remainder = parseFloat(currentTender.amount) - parseFloat(currentTender.totalAmount);
            MainService.remainder();
        }

    });


})()


