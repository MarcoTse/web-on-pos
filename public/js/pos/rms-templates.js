//  md-is-locked-open="$mdMedia(\'gt-sm\')"
(function () {
    var app = angular.module('rms.templates', ['shared.templates', 'tmpl/top-bar.html', 'tmpl/status-bar.html', 'tmpl/order.html', 'tmpl/divide-bill.html', 'tmpl/login.html', 'tmpl/login2.html', 'tmpl/voidItemApproval.html', 'tmpl/keypad.html', 'tmpl/keypad-free.html', 'tmpl/billOption.html', 'tmpl/foodControlOption.html', 'tmpl/gridMenuOption-order.html', 'tmpl/modifier.html', 'tmpl/options.html', 'tmpl/optionsFoodControl.html', 'tmpl/cancelOptions.html', 'tmpl/leftFoodMenu.html', 'tmpl/rightFoodMenu.html', 'tmpl/reportScene.html', 'tmpl/clearanceScene.html', 'tmpl/calendar.html', 'tmpl/billOption.html', 'tmpl/finish.html', 'tmpl/member.html', 'tmpl/pager-default-materialize.html', 'tmpl/pager-materialize-s4.html', 'tmpl/toast-template.html', 'tmpl/pager-default.html', 'tmpl/gridMenuOption-cancel.html', 'tmpl/billing-menu.html', 'tmpl/search-bill-menu.html', 'tmpl/amend-bill-menu.html', 'tmpl/billing-summary.html', 'tmpl/pager-default-grid-only.html', 'tmpl/main.html', 'tmpl/userScene.html', 'tmpl/userGroupScene.html', 'tmpl/cashManagementScene.html', 'tmpl/transfer-item.html', 'tmpl/kitchenMessage.html', 'tmpl/userClockScene.html', 'tmpl/tableListScene.html', 'tmpl/bill-payment-options.html', 'tmpl/bill-discount-options.html', 'tmpl/bill-numeric-keypad.html', 'tmpl/bill-amount-shortcuts.html', 'tmpl/settings.html', 'tmpl/sidenav.html', 'tmpl/search-bill-menu-takeAway.html', 'tmpl/split-order-list.html', 'tmpl/search-bill-menu-delivery.html', 'tmpl/keypad-payment-free.html', 'tmpl/watch-register-keypad.html', 'tmpl/paymentPermission.html', 'tmpl/compositeItem.html']);

    // 

    // disabled: , 'tmpl/kitchenInfo.html', 'tmpl/ordered-food-list-prototype.html'
    // , 'tmpl/order.html'
    // !!!!!!!!!!!!!!!!the order here is for quick development for other pages!!!!!!!!!!!!!!!!
    // swap
    // templates
    // switchMode3(menu.mode, menu.requiredLogin, menu.label, menu.label, menu.permission);
    angular.module('tmpl/sidenav.html', []).run(["$templateCache", function ($templateCache) {
        $templateCache.put('tmpl/sidenav.html',
          '<!--<md-sidenav class="md-sidenav-left md-whiteframe-z2" md-component-id="left">\
          <md-toolbar class="md-theme-light">\
            <h1 class="md-toolbar-tools">Sidenav Left</h1>\
          </md-toolbar>\
          <md-content layout-padding>\
            <form>\
              <md-input-container>\
                <label for="testInput">Test input</label>\
                <input type="text" id="testInput"\
                       ng-model="data" md-autofocus>\
              </md-input-container>\
            </form>\
            <md-button ng-click="close()" class="md-primary">\
              Close Sidenav Left\
            </md-button>\
          </md-content>\
        </md-sidenav> -->\
        \
          <md-sidenav class="md-sidenav-left md-whiteframe-z1" md-component-id="left">\
            <md-toolbar md-scroll-shrink>\
              <div class="md-toolbar-tools">\
                <h3>\
                  <span>RARe    v.1.9.0</span>\
                </h3>\
              </div>\
            </md-toolbar>\
            <md-content role="navigation">\
              <ul class="side-menu">\
                <li ng-repeat="section in vm.menu.mainMenuList" class="parent-list-item"\
                    ng-class="{\'parentActive\' : vm.isOpen(section)}" ng-if="section.config === true">\
                  <!-- <h2 class="menu-heading" ng-if="section.type === \'heading\'"\
                      id="heading_{{ section.label | nospace }}">\
                    {{ui[section.label]}}\
                  </h2> -->\
                  <menu-toggle section="section" ng-if="section.type === \'toggle\'"></menu-toggle>\
                  <menu-link section="section" ng-if="section.type === \'link\'"></menu-link>\
                  <menu-function section="section" ng-if="section.type === \'function\'"></menu-function>\
                </li>\
              </ul>\
            </md-content>\
          </md-sidenav>\
          <md-content flex>\
            <ui-view name="content"></ui-view>\
          </md-content>\
        ');
    }]);

    angular.module('tmpl/top-bar.html', []).run(["$templateCache", function ($templateCache) {
        $templateCache.put('tmpl/top-bar.html',
          ' \
      <md-toolbar id="toolbar-top" class="md-hue-2 md-whiteframe-glow-z2 md-rms-theme toolbar-top" > \
        <div class="md-toolbar-tools" ng-controller="headerCtrl"> \
          <div layout="row" flex class="fill-height" ng-hide="MainService.mode == MainService.schema.mode.home" style="margin-left:-10px;"> \
            <h1 class="logo md-toolbar-item" >\
              <md-button md-no-ink="true" aria-label="menu" class="md-icon-button" ng-click="toggleRight()" > \
                <md-icon class="dp45" md-svg-src="assets/icon/hexagon.svg" style="color: orange;" alt="RMS"></md-icon> \
              </md-button> \
            </h1> \
              \
            <!--{{MainService.RMSTable.currentSceneNo + 1}}-->\
            <!-- {{MainService.currentPageTitle}} -->\
            <div class="ng-cloak"> <span ng-if="isModeNavMenu(\'editTable\')"></span ng-if="isModeNavMenu(\'editTable\')"><span ng-show="MainService.RMSTable.noOfScene > 1">{{MainService.RMSTable.sceneName}}</span></div>\
            <div class="flex-center reference ng-cloak" ng-show="modeCheckBill(\'summary\')">\
            <div class="col">{{::ui[\'order_no\'] + ui[\'colon\']}}<em><span>{{ MainService.lastBillReference }}</span></em> </div>\
            <div class="col" ng-show="MainService.checkIsNotTakeAwayMode();">{{::ui[\'table_no\'] + ui[\'colon\']}} <em><span>{{ MainService.lastTableCode }}</span>\
        <span ng-if="MainService.lastTableSubCode">-{{ MainService.lastTableSubCode }}</span></em> </div></div> \
            <span flex=""></span> \
            <div class="md-toolbar-item" layout="row"> \
              \
              <div class="order-group dine-in-top-bar" layout="row" ng-show="isModeNavMenu()">\
                <!-- Table View -->\
                <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button" title="" ng-controller="tableViewTopBarController" ng-click="changeToTableView();" ng-show="config.topMenu.order_mode && isMode()  && MainService.modeTable == MainService.schema.modeTable.svg"><strong>{{::ui[\'txt_tableView\']}}</strong></button> \
                <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button" title="" ng-controller="tableViewTopBarController" ng-click="changeToSVGView();" ng-show="config.topMenu.order_mode && isMode() && MainService.modeTable == MainService.schema.modeTable.tableView"><strong>{{::ui[\'txt_floorPlan\']}}</strong></button> \
                <!-- 查詢八達通 -->\
                <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button" title="{{::ui[\'octopus_wording_enquiry_octopus\']}}" ng-click="MainService.enquiryOctopus()" ng-show="config.topMenu.enquiry_octopus && isModeOrder(\'all\') && isModeOrderMenu() && isModeFloor(\'order\')" style="width:94px"><strong>{{ui[\'octopus_wording_enquiry_octopus\']}}</strong></button> \
                \
                <!-- 檯號一覽 -->\
                <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button" title="{{::ui[\'table_list\']}}" ng-click="switchMode2({mode: \'tableList\'}, true, \'table_list\');" ng-show="config.topMenu.table_list && isMode()"><strong>{{ui[\'table_list\']}}</strong></button> \
                \
                <!-- 落單模式 -->\
                <button class="btn-order btn md-rounded-square md-accent md-hue-3 md-raised md-button active" title="{{::ui[\'order_mode\']}}" ng-click="switchMode2({mode: \'floorPlan\', modeFloorPlan: \'order\'}, true, \'order_mode\');" ng-show="config.topMenu.order_mode && isMode()"><strong>{{ui[\'order_mode\']}}</strong></button> \
                \
                <!-- 做檯紙 -->\
                <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button" title="{{::ui[\'print_order\']}}" ng-click="switchMode2({modeFloorPlan: \'printOrder\'}, true, \'PrintOrder\', \'print_order\');" ng-show="config.topMenu.print_order && isMode()"><strong>{{ui[\'print_order\']}}</strong></button> \
                \
                <!-- 廚房訊息(已下單的項目，在檯模式下使用) -->\
                <button class="btn-kitchen btn md-rounded-square md-accent md-hue-3 md-raised md-button" title="{{::ui[\'print_order\']}}" ng-click="switchMode2({modeFloorPlan: \'kitchenMessage\'}, true, \'additional_kitchen_message\', \'additional_kitchen_message\');"  ng-show="config.topMenu.kitchen_message && isMode()"><strong>{{ui[\'additional_message\']}}</strong></button> \
                \
                <!-- 特別訊息 -->\
                <button class="btn-special btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active" title="{{::ui[\'special_message\']}}" ng-click="switchMode2({modeOrder: \'specialMessage\', modeItem: \'normal\'}, true, \'special_message\', \'special_message\');" ng-show="config.topMenu.special_message && isModeOrder(\'all\') && isModeOrderMenu() && isModeFloor(\'order\')"><strong>{{ui[\'special_message\']}}</strong></button> \
                \
                <!-- 轉檯 -->\
                <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button" title="{{::ui[\'transfer_table\']}}" ng-click="switchMode2({modeFloorPlan: \'changeTable\'}, true, \'transfer_table\');" ng-show="config.topMenu.transfer_table &&  isMode()"><strong>{{ui[\'transfer_table\']}}</strong></button> \
                \
                <!-- 分單 -->\
                <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button" title="{{::ui[\'divide_bill\']}}" ng-click="switchMode2({modeFloorPlan: \'split\'}, true, \'divide_bill\', \'divide_bill\');" ng-show="config.topMenu.split_table && isMode()"><strong>{{::ui["txt_split_table"]}}</strong></button>\
                \
                <!-- 印單 -->\
                <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button" title="{{::ui[\'print_bill\']}}" ng-click="switchMode2({modeFloorPlan: \'printBill\', modeOrder: \'printBill\'}, true, \'print_bill\');" ng-show="config.topMenu.print_bill && isMode()"><strong>{{ui[\'print_bill\']}}</strong></button> \
                \
                <!-- 結賬 -->\
                <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button" title="{{::ui[\'billing\']}}" ng-click="switchMode2({modeFloorPlan: \'billing\'}, true, \'billing\');MainService.mifarePayment.createEvent();" ng-show="isMode() && (config.page.bill  || config.page.bill == \'yes\') "><strong>{{ui[\'billing\']}}</strong></button> \
                \
                <!-- 轉移項目 -->\
                <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active" title="{{::ui[\'transfer_item\']}}" ng-click="toggleItemOption(\'transfer_item\');" ng-show="config.topMenu.transfer_item && isModeOrder(\'all\') && isModeOrderMenu() && isModeFloor(\'order\')"><strong>{{ui[\'transfer_item\']}}</strong></button> \
                \
                <!-- 食品控制 -->\
                <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active" title="{{::ui[\'food_control\']}}" ng-click="toggleFoodControlMode()" ng-show="config.topMenu.food_control && isModeOrder(\'all\') && isModeOrderMenu() && isModeFloor(\'order\')"><strong>{{ui[\'food_control\']}}</strong></button> \
                \
                <!-- 附加選項 -->\
                <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active" title="{{::ui[\'modifier\']}}" ng-click="MainService.modeToggleModifier()" ng-show="config.topMenu.modifier && isModeOrder(\'normal\') && isModeOrderMenu()"><strong>{{ui[\'modifier\']}}</strong></button> \
                \
                <!-- 詳細資料 -->\
                <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active" title="{{::ui[\'details\']}}" ng-click="modeToggleItemDetail($event)" ng-show="config.topMenu.item_details && isModeOrder(\'normal\') && isModeOrderMenu()"><strong>{{ui[\'details\']}}</strong></button> \
                \
                <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button" title="{{::ui[\'member_info\']}}" ng-show="config.topMenu.member_info && isMode()" ng-click="panelMember()"><strong>{{ui[\'member_info\']}}</strong></button>\
              </div>\
                <!-- important function for food submit --> \
              <!-- table mode function --> \
              \
              <div class="take-away-group" layout="row" ng-show="isModeNavMenu(\'takeAway\')">\
                <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button" title="{{::ui[\'search_bill\']}}" ng-show="config.topMenu.print_bill" ng-click="switchMode2({modeTakeAway: \'searchBill\'}, false, \'take_away\');takeOut.searchBill();"><strong>{{::ui[\'search_bill\']}}</strong></button> \
                <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button" title="{{::ui[\'print_bill\']}}" ng-click="switchMode2({modeTakeAway: \'printBill\'}, false, \'take_away\');takeOut.viewOrder();" ng-show="config.topMenu.print_bill"><strong>{{ui[\'print_bill\']}}</strong></button> \
                <!--<button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button" title="{{::ui[\'order_mode\']}}" ng-click="switchMode2({mode: \'order\', modeTakeAway: \'order\'}, true, \'order_mode\');" ng-show="config.topMenu.order_mode"><strong>{{ui[\'order_mode\']}}</strong></button>--> \
                <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button" title="{{::ui[\'take_away_order_top_bar_order\']}}" ng-show="config.topMenu.print_bill" ng-click="switchMode2({modeTakeAway: \'writeOrder\'}, false, \'take_away\');takeOut.takeOrder();"><strong>{{::ui[\'take_away_order_top_bar_order\']}}</strong></button> \
                <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button btn-view" title="{{::ui[\'take_away_order_top_bar_view\']}}" ng-show="config.topMenu.print_bill" ng-click="switchMode2({modeTakeAway: \'normal\'}, false, \'take_away\');takeOut.viewOrder();"><strong>{{::ui[\'take_away_order_top_bar_view\']}}</strong></button> \
                <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button" title="" ng-show="config.topMenu.print_bill" ng-click="switchMode2({modeTakeAway: \'normal\'}, false, \'take_away\');takeOut.checkTakeOutAndDeliveryByPhone();"><strong>{{::ui[\'txt_takeOutAndDelivery_search_bill\']}}</strong></button> \
              </div>\
                \
                <div class="take-away-in-order-group" layout="row" ng-show="isModeNavMenu(\'takeAwayInOrder\')">\
                <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button" title="{{::ui[\'octopus_wording_enquiry_octopus\']}}" ng-click="MainService.enquiryOctopus()" ng-show="config.topMenu.enquiry_octopus && isModeOrder(\'all\') && isModeOrderMenu() && isModeFloor(\'order\')" style="width:94px"><strong>{{ui[\'octopus_wording_enquiry_octopus\']}}</strong></button> \
                <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active" title="{{::ui[\'modifier\']}}" ng-click="MainService.modeToggleModifier()" ng-show="config.topMenu.modifier && isModeOrder(\'normal\') && isModeOrderMenu()"><strong>{{ui[\'modifier\']}}</strong></button> \
                <button class="btn-special btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active" title="{{::ui[\'special_message\']}}" ng-click="switchMode2({modeOrder: \'specialMessage\', modeItem: \'normal\'}, true, \'special_message\', \'special_message\');" ng-show="config.topMenu.special_message && isModeOrder(\'all\') && isModeOrderMenu() && isModeFloor(\'order\') && MainService.getOtherWriteOrderService().mode == MainService.getOtherWriteOrderService().schema.orderPage"><strong>{{ui[\'special_message\']}}</strong></button> \
                <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active" title="{{::ui[\'details\']}}" ng-click="modeToggleItemDetail($event)" ng-show="config.topMenu.item_details && isModeOrder(\'normal\') && isModeOrderMenu()"><strong>{{ui[\'details\']}}</strong></button> \
                <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button active  btn-takeaway-order" title="{{::ui[\'take_away_order_top_bar_order\']}}" ng-show="config.topMenu.print_bill" ng-click=""><strong>{{::ui[\'take_away_order_top_bar_order\']}}</strong></button> \
                <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button btn-view" title="{{::ui[\'take_away_order_top_bar_view\']}}" ng-show="config.topMenu.print_bill" ng-click="switchMode2({modeTakeAway: \'normal\'}, false, \'take_away\');takeOut.viewOrder();"><strong>{{::ui[\'take_away_order_top_bar_view\']}}</strong></button> \
              </div>\
              <!--<button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button" title="取消帳單" ng-click="switchMode2({modeFastFood: \'voidBill\'}, false);takeOut.viewOrder();" ng-show="config.topMenu.print_bill"><strong>取消帳單</strong></button>\-->\
              <div class="fast-food-group" layout="row" ng-show="isModeNavMenu(\'fastFood\')">\
                <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button" title="{{::ui[\'search_bill\']}}" ng-show="config.topMenu.print_bill" ng-click="switchMode2({modeFastFood: \'searchBill\'}, false);takeOut.searchBill();"><strong>{{::ui[\'search_bill\']}}</strong></button> \
                <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button" title="{{::ui[\'print_bill\']}}" ng-click="switchMode2({modeFastFood: \'printBill\'}, false);takeOut.viewOrder();" ng-show="config.topMenu.print_bill"><strong>{{ui[\'print_bill\']}}</strong></button> \
                <!--<button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button" title="{{::ui[\'order_mode\']}}" ng-click="switchMode2({mode: \'order\', modeFastFood: \'order\'}, true);" ng-show="config.topMenu.order_mode"><strong>{{ui[\'order_mode\']}}</strong></button>--> \
                <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button btn-view" title="{{::ui[\'take_away_order_top_bar_view\']}}" ng-show="config.topMenu.print_bill" ng-click="switchMode2({modeFastFood: \'normal\'}, false);takeOut.viewOrder();"><strong>{{::ui[\'take_away_order_top_bar_view\']}}</strong></button>\
                <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button" title="{{::ui[\'take_away_order_top_bar_order\']}}" ng-show="config.topMenu.print_bill" ng-click="switchMode2({modeFastFood: \'writeOrder\'}, false);takeOut.takeOrder();"><strong>{{::ui[\'take_away_order_top_bar_order\']}}</strong></button> \
                <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active" style="background-color:white" ng-show="uc.ecrPayment && !MainService.getOtherWriteOrderServiceFullDetail().isCardManually"  ng-click="takeOut.changeSwipeOrManually()"><strong>{{::ui["txt_swipe_card"]}}</strong></button>\
                <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active" style="background-color:white" ng-show="uc.ecrPayment && MainService.getOtherWriteOrderServiceFullDetail().isCardManually" ng-controller="keypadControllerOpenItem" ng-click="takeOut.changeSwipeOrManually()"><strong>{{::ui["txt_keyboard"]}}</strong></button>\
              </div>\
                \
                <div class="fast-food-in-order-group" layout="row" ng-if="isModeNavMenu(\'fastFoodInOrder\')" ng-init="takeOut.topBarInit();">\
                <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button" title="{{::ui[\'octopus_wording_enquiry_octopus\']}}" ng-click="MainService.enquiryOctopus()" ng-show="config.topMenu.enquiry_octopus && isModeOrder(\'all\') && isModeOrderMenu() && isModeFloor(\'order\')" style="width:94px"><strong>{{ui[\'octopus_wording_enquiry_octopus\']}}</strong></button> \
                <button ng-controller="userClockController" class="btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active" title="{{ui[\'take_away_take_attendance\']}}" ng-click="takeAttendanceOnTopBar();" ng-show="MainService.getOtherWriteOrderService().mode == MainService.getOtherWriteOrderService().schema.orderPage && (config.page.duty == \'yes\' || config.page.duty)"><strong>{{ui[\'take_away_take_attendance\']}}</strong></button> \
                <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active color-show" ng-style="takeOut.getBtnBackgroundColor(isModeOrder(\'foodControl\'))" title="{{::ui[\'food_control\']}}" ng-click="toggleFoodControlMode()" ng-show="config.topMenu.food_control && isModeOrder(\'all\') && isModeOrderMenu() && isModeFloor(\'order\')    && !isModeOrder(\'billingSummary\')"><strong>{{ui[\'food_control\']}}</strong></button> \
                <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active color-show" ng-style="takeOut.getBtnBackgroundColor(modeCheckOrderItem(\'modifier\'))" title="{{::ui[\'modifier\']}}" ng-click="MainService.modeToggleModifier()" ng-show="config.topMenu.modifier && isModeOrder(\'all\') && isModeOrderMenu() && !isModeOrder(\'billingSummary\')"><strong>{{ui[\'modifier\']}}</strong></button> \
                <button class="btn-special btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active color-show" ng-style="takeOut.getBtnBackgroundColor(modeCheckOrder(\'specialMessage\'))" title="{{::ui[\'special_message\']}}" ng-click="MainService.modeToggleSpecialMessage();" ng-show="config.topMenu.special_message && isModeOrder(\'all\') && isModeOrderMenu() && isModeFloor(\'order\') && MainService.getOtherWriteOrderService().mode == MainService.getOtherWriteOrderService().schema.orderPage && !isModeOrder(\'billingSummary\')"><strong>{{ui[\'special_message\']}}</strong></button> \
                <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active" title="{{::ui[\'details\']}}" ng-click="modeToggleItemDetail($event)" ng-show="config.topMenu.item_details && isModeOrder(\'all\') && isModeOrderMenu() && !isModeOrder(\'billingSummary\')"><strong>{{ui[\'details\']}}</strong></button> \
                <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button active  btn-fastfood-order" title="{{::ui[\'take_away_order_top_bar_order\']}}" ng-show="config.topMenu.print_bill" ng-click=""><strong>{{::ui[\'take_away_order_top_bar_order\']}}</strong></button> \
                <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button btn-view" title="{{::ui[\'take_away_order_top_bar_view\']}}" ng-show="config.topMenu.print_bill && !isModeOrder(\'billingSummary\')" ng-click="switchMode2({modeFastFood: \'normal\'}, false);takeOut.viewOrder();"><strong>{{::ui[\'take_away_order_top_bar_view\']}}</strong></button> \
                <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active" style="background-color:white" ng-show="!MainService.getOtherWriteOrderService().modeSetting.isTakeout" ng-click="takeOut.changeTakeoutOrDinning()"><strong>{{::ui[\'txt_dinning\']}}</strong></button>\
                <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active" style="background-color:white" ng-show="MainService.getOtherWriteOrderService().modeSetting.isTakeout" ng-click="takeOut.changeTakeoutOrDinning()"><strong>{{::ui[\'txt_takeout\']}}</strong></button>\
                <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active" style="background-color:white" ng-show="uc.ecrPayment && !MainService.getOtherWriteOrderServiceFullDetail().isCardManually"  ng-click="takeOut.changeSwipeOrManually()"><strong>{{::ui[\'txt_swipe_card\']}}</strong></button>\
                <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active" style="background-color:white" ng-show="MainService.getOtherWriteOrderServiceFullDetail().isCardManually" ng-controller="keypadControllerOpenItem" ng-click="uc.ecrPayment && takeOut.changeSwipeOrManually()" ng-init="initPaymentKeypad();"><strong>{{::ui[\'txt_keyboard\']}}</strong></button>\
              </div>\
                \
              <div class="delivery-order-group" layout="row" ng-show="isModeNavMenu(\'deliveryOrder\')">\
                <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button" title="{{::ui[\'search_bill\']}}" ng-show="config.topMenu.print_bill" ng-click="switchMode2({modeDeliveryOrder: \'searchBill\'}, false, \'delivery_order\');takeOut.searchBill();"><strong>{{::ui[\'search_bill\']}}</strong></button> \
                <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button" title="{{::ui[\'print_bill\']}}" ng-click="switchMode2({modeDeliveryOrder: \'printBill\'}, false, \'delivery_order\');takeOut.viewOrder();" ng-show="config.topMenu.print_bill"><strong>{{ui[\'print_bill\']}}</strong></button> \
                <!--<button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button" title="{{::ui[\'order_mode\']}}" ng-click="switchMode2({mode: \'order\', modeDeliveryOrder: \'order\'}, true, \'order_mode\');" ng-show="config.topMenu.order_mode"><strong>{{ui[\'order_mode\']}}</strong></button>--> \
                <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button" title="{{::ui[\'take_away_order_top_bar_order\']}}" ng-show="config.topMenu.print_bill" ng-click="switchMode2({modeDeliveryOrder: \'writeOrder\'}, false, \'delivery_order\');takeOut.takeOrder();"><strong>{{::ui[\'take_away_order_top_bar_order\']}}</strong></button> \
                <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button btn-view" title="{{::ui[\'take_away_order_top_bar_view\']}}" ng-show="config.topMenu.print_bill" ng-click="switchMode2({modeDeliveryOrder: \'normal\'}, false, \'delivery_order\');takeOut.viewOrder();"><strong>{{::ui[\'take_away_order_top_bar_view\']}}</strong></button> \
                \
              </div>\
              <div class="delivery-order-in-order-group" layout="row" ng-show="isModeNavMenu(\'deliveryOrderInOrder\')">\
                <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button" title="{{::ui[\'octopus_wording_enquiry_octopus\']}}" ng-click="MainService.enquiryOctopus()" ng-show="config.topMenu.enquiry_octopus && isModeOrder(\'all\') && isModeOrderMenu() && isModeFloor(\'order\')" style="width:94px"><strong>{{ui[\'octopus_wording_enquiry_octopus\']}}</strong></button> \
                <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active" title="{{::ui[\'modifier\']}}" ng-click="MainService.modeToggleModifier()" ng-show="config.topMenu.modifier && isModeOrder(\'normal\') && isModeOrderMenu()"><strong>{{ui[\'modifier\']}}</strong></button> \
                <button class="btn-special btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active" title="{{::ui[\'special_message\']}}" ng-click="switchMode2({modeOrder: \'specialMessage\', modeItem: \'normal\'}, true, \'special_message\', \'special_message\');" ng-show="config.topMenu.special_message && isModeOrder(\'all\') && isModeOrderMenu() && isModeFloor(\'order\') && MainService.getOtherWriteOrderService().mode == MainService.getOtherWriteOrderService().schema.orderPage"><strong>{{ui[\'special_message\']}}</strong></button> \
                <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active" title="{{::ui[\'details\']}}" ng-click="modeToggleItemDetail($event)" ng-show="config.topMenu.item_details && isModeOrder(\'normal\') && isModeOrderMenu()"><strong>{{ui[\'details\']}}</strong></button> \
                <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button active  btn-takeaway-order" title="{{::ui[\'take_away_order_top_bar_order\']}}" ng-show="config.topMenu.print_bill" ng-click=""><strong>{{::ui[\'take_away_order_top_bar_order\']}}</strong></button> \
                <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button btn-view" title="{{::ui[\'take_away_order_top_bar_view\']}}" ng-show="config.topMenu.print_bill" ng-click="switchMode2({modeDeliveryOrder: \'normal\'}, false, \'delivery_order\');takeOut.viewOrder();"><strong>{{::ui[\'take_away_order_top_bar_view\']}}</strong></button> \
              </div>\
                \
              <!-- ng-show="isModeNavMenu(\'editTable\')" -->\
              <div class="shared-group" layout="row" ng-show="(isModeNavMenu(\'editTable\') || isMode()) && MainService.RMSTable.noOfScene > 1 ">\
                <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active md-40p" title="" ng-click="changeZone(\'prev\')"><strong><i class="md-icon dp70 solo">keyboard_arrow_left</i></strong></button> \
                <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active md-40p" title="" ng-click="changeZone(\'next\')"><strong><i class="md-icon dp70 solo">keyboard_arrow_right</i></strong></button> \
              </div>\
               \
               <!-- ng-show="isModeNavMenu(\'depositNavBar\')" -->\
               <div layout="row" ng-show="isModeNavMenu(\'depositNavBar\')" ng-controller="depositModeController">\
                <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button  md-40p active btn-input-deposit" title="" ng-click="changeInputMode();"><strong>{{::ui[\'txt_deposit_top_bar_input_info\']}}</strong></button> \
                <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button  md-40p btn-read-deposit" title="" ng-click="changeReadMode();"><strong>{{::ui[\'txt_deposit_top_bar_deposit_records\']}}</strong></button>\
            </div>\
                \
              <div class="edit-group" layout="row" ng-show="isModeNavMenu(\'editTable\')">\
              \
                <!-- scene control --> \
                <input id="current-table-label" type="text">\
                <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button save proceed no-active" title="{{::ui.txt_edit_table_save_title}}" ng-click="saveTable()"><strong>{{::ui["txt_edit_table_save"]}}</strong></button> \
                <div layout="column" class="column">\
                  <div layout="row" class="row"> \
                    <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active d33" title="上" ng-click="rotate(\'left\')"><strong><i class="md-icon dp24 solo">rotate_left</i></strong></button> \
                    <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active d33" title="上" ng-click="rotate(\'right\')"><strong><i class="md-icon dp24 solo">rotate_right</i></strong></button> \
                    <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active d33" title="上" ng-click="move(\'up\')"><strong><i class="md-icon dp24 solo">arrow_upward</i></strong></button> \
                    <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active d33" title="下" ng-click="move(\'down\')"><strong><i class="md-icon dp24 solo">arrow_downward</i></strong></button> \
                    <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active d33" title="左" ng-click="move(\'left\')"><strong><i class="md-icon dp24 solo">arrow_back</i></strong></button> \
                    <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active d33" title="右" ng-click="move(\'right\')"><strong><i class="md-icon dp24 solo">arrow_forward</i></strong></button> \
                    <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active d33" title="" ng-click="scale(\'up\')">\
                      <md-icon class="dp24 flex-center" md-svg-src="assets/icon/scale-up.svg" style="color: orange;" alt="RMS"></md-icon> \
                    </button> \
                  </div>\
                  <div layout="row" class="row">\
                    <button class="btn-multi-edit btn md-rounded-square md-accent md-hue-3 md-raised md-button toggle d33" title="{{::ui.txt_edit_table_multi_title}}" ng-click="toggleGroupEdit(); toggleActive($event);"><strong>{{::ui.txt_edit_table_multi}}</strong></button> \
                    <button class="btn-grid btn md-rounded-square md-accent md-hue-3 md-raised md-button toggle d33" title="輔助格線顯示開關" ng-click="toggleGrid(); toggleActive($event);"><md-icon class="dp24" md-svg-src="assets/icon/grid-two-up.svg"></md-icon></button> \
                    <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button toggle d33" title="顯示範圍參考" ng-click="toggleSVG(\'viewport-helper\'); toggleActive($event);"><strong><i class="md-icon dp24 solo">aspect_ratio</i></strong></button> \
                    <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active d33" title="" ng-click="restore()"><strong><i class="md-icon dp24 solo">undo</i></strong></button> \
                    <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active cancel d33" title="" ng-click="delete()"><strong><i class="md-icon dp24 solo">clear</i></strong></button> \
                    <div id="add-table-container" style="position: relative">\
                      <button class="btn-add-table btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active d33" title="{{::ui.txt_edit_table_add_title}}" ng-click="toggleEditTable()">\
                        <strong>{{::ui.txt_edit_table_add}}</strong>\
                      </button> \
                      <!--<new-table-shape style="" class=""></new-table-shape>-->\
                    </div>\
                    <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active d33" title="" ng-click="scale(\'down\')">\
                      <md-icon class="dp24 flex-center" md-svg-src="assets/icon/scale-down.svg" style="color: orange;" alt="RMS"></md-icon> \
                    </button> \
                  </div>\
                </div> \
                <!--<button class="btn-edit-table btn md-rounded-square md-accent md-hue-3 md-raised md-button" title="修改檯面" ng-click="toggleNavMode(\'editTable\')"><strong>修改檯面</strong></button>--> \
                <!--<button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button" title="上一個場景" ng-click="prevZone()"><strong>上個場景</strong></button> \
                <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button" title="下一個場景" ng-click="nextZone()"><strong>下個場景</strong></button>--> \
                <!--<md-button class="md-raised md-icon-button md-mini md-rounded-square" style="color: black"> \
                    S{{SCENE.name}} \
                </md-button> --> \
              </div>\
                \
              <!-- system notification --> \
                <!--<md-button md-no-ink="true" class="md-icon-button md-mini" aria-label="系統訊息" ng-click="showAlert($event)">--> \
                 <!-- <md-icon md-svg-src="assets/icon/error.svg"></md-icon> --> \
                 <!-- <i class="md-icon dp45">warning</i> --> \
                 <!--<md-tooltip> \
                   系統訊息 \
                 </md-tooltip> \
                 <i class="md-icon dp45">announcement</i> \
                </md-button>--> \
                <!-- primary function/each page --> \
                \
                \
                <!-- login logout -->\
                <div class="primary-group" layout="row">\
                  <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active" title="{{::ui[\'reload\']}}" ng-click="refreshScreen();" ng-show="config.topMenu.reload"><strong>{{ui[\'reload\']}}</strong></button> \
                  \
                  <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active" title="{{::ui[\'login\']}}" ng-click="login()" ng-hide="isLogin()"><strong>{{ui[\'login\']}}</strong></button> \
                  \
                  <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active" title="{{::ui[\'logout\']}}" ng-click="logout($event)" ng-show="isLogin()"><strong>{{ui[\'logout\']}}</strong></button> \
                </div>\
            </div> \
          </div> \
    </md-toolbar>');
    }]);

    //angular.module('tmpl/top-bar-en.html', []).run(["$templateCache", function ($templateCache) {
    //    $templateCache.put('tmpl/top-bar-en.html',
    //      ' \
    //  <md-toolbar id="toolbar-top" class="md-hue-2 md-whiteframe-glow-z2 md-rms-theme toolbar-top en" > \
    //    <div class="md-toolbar-tools" ng-controller="headerCtrl"> \
    //      <div layout="row" flex class="fill-height" ng-hide="MainService.mode == MainService.schema.mode.home" style="margin-left:-10px;"> \
    //        <h1 class="logo md-toolbar-item" >\
    //          <md-button md-no-ink="true" aria-label="menu" class="md-icon-button" ng-click="toggleRight()" > \
    //            <md-icon class="dp45" md-svg-src="assets/icon/hexagon.svg" style="color: orange;" alt="RMS"></md-icon> \
    //          </md-button> \
    //        </h1> \
    //          \
    //        <!--{{MainService.RMSTable.currentSceneNo + 1}}-->\
    //        <!-- {{MainService.currentPageTitle}} -->\
    //        <div class="ng-cloak"> <span ng-if="isModeNavMenu(\'editTable\')"></span ng-if="isModeNavMenu(\'editTable\')"><span ng-show="MainService.RMSTable.noOfScene > 1">{{MainService.RMSTable.sceneName}}</span></div>\
    //        <div class="flex-center reference ng-cloak" ng-show="modeCheckBill(\'summary\')">\
    //        <div class="col">{{::ui[\'order_no\'] + ui[\'colon\']}}<em><span>{{ MainService.lastBillReference }}</span></em> </div>\
    //        <div class="col" ng-show="MainService.checkIsNotTakeAwayMode();">{{::ui[\'table_no\'] + ui[\'colon\']}} <em><span>{{ MainService.lastTableCode }}</span>\
    //    <span ng-if="MainService.lastTableSubCode">-{{ MainService.lastTableSubCode }}</span></em> </div></div> \
    //        <span flex=""></span> \
    //        <div class="md-toolbar-item" layout="row"> \
    //          \
    //          <div class="order-group" layout="row" ng-show="isModeNavMenu()">\
    //            <!-- 檯號一覽 -->\
    //            <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button" title="{{::ui[\'table_list\']}}" ng-click="switchMode2({mode: \'tableList\'}, true, \'table_list\');" ng-show="config.topMenu.table_list && isMode()"><strong>{{ui[\'table_list\']}}</strong></button> \
    //            \
    //            <!-- 落單模式 -->\
    //            <button class="btn-order btn md-rounded-square md-accent md-hue-3 md-raised md-button active" title="{{::ui[\'order_mode\']}}" ng-click="switchMode2({mode: \'floorPlan\', modeFloorPlan: \'order\'}, true, \'order_mode\');" ng-show="config.topMenu.order_mode && isMode()"><strong>{{ui[\'order_mode\']}}</strong></button> \
    //            \
    //            <!-- 做檯紙 -->\
    //            <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button" title="{{::ui[\'print_order\']}}" ng-click="switchMode2({modeFloorPlan: \'printOrder\'}, true, \'PrintOrder\', \'print_order\');" ng-show="config.topMenu.print_order && isMode()"><strong>{{ui[\'print_order\']}}</strong></button> \
    //            \
    //            <!-- 廚房訊息(已下單的項目，在檯模式下使用) -->\
    //            <button class="btn-kitchen btn md-rounded-square md-accent md-hue-3 md-raised md-button" title="{{::ui[\'print_order\']}}" ng-click="switchMode2({modeFloorPlan: \'kitchenMessage\'}, true, \'additional_kitchen_message\', \'additional_kitchen_message\');"  ng-show="config.topMenu.kitchen_message && isMode()"><strong>{{ui[\'additional_message\']}}</strong></button> \
    //            \
    //            <!-- 特別訊息 -->\
    //            <button class="btn-special btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active" title="{{::ui[\'special_message\']}}" ng-click="switchMode2({modeOrder: \'specialMessage\', modeItem: \'normal\'}, true, \'special_message\', \'special_message\');" ng-show="config.topMenu.special_message && isModeOrder(\'all\') && isModeOrderMenu() && isModeFloor(\'order\')"><strong>{{ui[\'special_message\']}}</strong></button> \
    //            \
    //            <!-- 轉檯 -->\
    //            <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button" title="{{::ui[\'transfer_table\']}}" ng-click="switchMode2({modeFloorPlan: \'changeTable\'}, true, \'transfer_table\');" ng-show="config.topMenu.transfer_table &&  isMode()"><strong>{{ui[\'transfer_table\']}}</strong></button> \
    //            \
    //            <!-- 分單 -->\
    //            <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button" title="{{::ui[\'divide_bill\']}}" ng-click="switchMode2({modeFloorPlan: \'split\'}, true, \'divide_bill\', \'divide_bill\');" ng-show="config.topMenu.split_table && isMode()"><strong>分單</strong></button>\
    //            \
    //            <!-- 印單 -->\
    //            <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button" title="{{::ui[\'print_bill\']}}" ng-click="switchMode2({modeFloorPlan: \'printBill\', modeOrder: \'printBill\'}, true, \'print_bill\');" ng-show="config.topMenu.print_bill && isMode()"><strong>{{ui[\'print_bill\']}}</strong></button> \
    //            \
    //            <!-- 結賬 -->\
    //            <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button" title="{{::ui[\'billing\']}}" ng-click="switchMode2({modeFloorPlan: \'billing\'}, true, \'billing\');" ng-show="isMode() && (config.page.bill  || config.page.bill == \'yes\') "><strong>{{ui[\'billing\']}}</strong></button> \
    //            \
    //            <!-- 轉移項目 -->\
    //            <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active" title="{{::ui[\'transfer_item\']}}" ng-click="toggleItemOption(\'transfer_item\');" ng-show="config.topMenu.transfer_item && isModeOrder(\'all\') && isModeOrderMenu() && isModeFloor(\'order\')"><strong>{{ui[\'transfer_item\']}}</strong></button> \
    //            \
    //            <!-- 食品控制 -->\
    //            <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active" title="{{::ui[\'food_control\']}}" ng-click="toggleFoodControlMode()" ng-show="config.topMenu.food_control && isModeOrder(\'all\') && isModeOrderMenu() && isModeFloor(\'order\')"><strong>{{ui[\'food_control\']}}</strong></button> \
    //            \
    //            <!-- 附加選項 -->\
    //            <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active" title="{{::ui[\'modifier\']}}" ng-click="MainService.modeToggleModifier()" ng-show="config.topMenu.modifier && isModeOrder(\'normal\') && isModeOrderMenu()"><strong>{{ui[\'modifier\']}}</strong></button> \
    //            \
    //            <!-- 詳細資料 -->\
    //            <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active" title="{{::ui[\'details\']}}" ng-click="modeToggleItemDetail($event)" ng-show="config.topMenu.item_details && isModeOrder(\'normal\') && isModeOrderMenu()"><strong>{{ui[\'details\']}}</strong></button> \
    //            \
    //            <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button" title="{{::ui[\'member_info\']}}" ng-show="config.topMenu.member_info && isMode()" ng-click="panelMember()"><strong>{{ui[\'member_info\']}}</strong></button>\
    //          </div>\
    //            <!-- important function for food submit --> \
    //          <!-- table mode function --> \
    //          \
    //          <div class="take-away-group" layout="row" ng-show="isModeNavMenu(\'takeAway\')">\
    //            <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button" title="{{::ui[\'search_bill\']}}" ng-show="config.topMenu.print_bill" ng-click="switchMode2({modeTakeAway: \'searchBill\'}, false, \'take_away\');takeOut.searchBill();"><strong>{{::ui[\'search_bill\']}}</strong></button> \
    //            <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button" title="{{::ui[\'print_bill\']}}" ng-click="switchMode2({modeTakeAway: \'printBill\'}, false, \'take_away\');takeOut.viewOrder();" ng-show="config.topMenu.print_bill"><strong>{{ui[\'print_bill\']}}</strong></button> \
    //            <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button" title="取消帳單" ng-click="switchMode2({modeTakeAway: \'voidBill\'}, false, \'take_away\');takeOut.viewOrder();" ng-show="config.topMenu.print_bill"><strong>取消帳單</strong></button>\
    //            <!--<button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button" title="{{::ui[\'order_mode\']}}" ng-click="switchMode2({mode: \'order\', modeTakeAway: \'order\'}, true, \'order_mode\');" ng-show="config.topMenu.order_mode"><strong>{{ui[\'order_mode\']}}</strong></button>--> \
    //            <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button" title="{{::ui[\'take_away_order_top_bar_order\']}}" ng-show="config.topMenu.print_bill" ng-click="switchMode2({modeTakeAway: \'writeOrder\'}, false, \'take_away\');takeOut.takeOrder();"><strong>{{::ui[\'take_away_order_top_bar_order\']}}</strong></button> \
    //            <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button btn-view" title="{{::ui[\'take_away_order_top_bar_view\']}}" ng-show="config.topMenu.print_bill" ng-click="switchMode2({modeTakeAway: \'normal\'}, false, \'take_away\');takeOut.viewOrder();"><strong>{{::ui[\'take_away_order_top_bar_view\']}}</strong></button> \
    //          </div>\
    //            \
    //            <div class="take-away-in-order-group" layout="row" ng-show="isModeNavMenu(\'takeAwayInOrder\')">\
    //            <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active" title="{{::ui[\'modifier\']}}" ng-click="MainService.modeToggleModifier()" ng-show="config.topMenu.modifier && isModeOrder(\'normal\') && isModeOrderMenu()"><strong>{{ui[\'modifier\']}}</strong></button> \
    //            <button class="btn-special btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active" title="{{::ui[\'special_message\']}}" ng-click="switchMode2({modeOrder: \'specialMessage\', modeItem: \'normal\'}, true, \'special_message\', \'special_message\');" ng-show="config.topMenu.special_message && isModeOrder(\'all\') && isModeOrderMenu() && isModeFloor(\'order\') && MainService.getOtherWriteOrderService().mode == MainService.getOtherWriteOrderService().schema.orderPage"><strong>{{ui[\'special_message\']}}</strong></button> \
    //            <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active" title="{{::ui[\'details\']}}" ng-click="modeToggleItemDetail($event)" ng-show="config.topMenu.item_details && isModeOrder(\'normal\') && isModeOrderMenu()"><strong>{{ui[\'details\']}}</strong></button> \
    //            <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button active  btn-takeaway-order" title="{{::ui[\'take_away_order_top_bar_order\']}}" ng-show="config.topMenu.print_bill" ng-click=""><strong>{{::ui[\'take_away_order_top_bar_order\']}}</strong></button> \
    //            <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button btn-view" title="{{::ui[\'take_away_order_top_bar_view\']}}" ng-show="config.topMenu.print_bill" ng-click="switchMode2({modeTakeAway: \'normal\'}, false, \'take_away\');takeOut.viewOrder();"><strong>{{::ui[\'take_away_order_top_bar_view\']}}</strong></button> \
    //          </div>\
    //          <!--<button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button" title="取消帳單" ng-click="switchMode2({modeFastFood: \'voidBill\'}, false);takeOut.viewOrder();" ng-show="config.topMenu.print_bill"><strong>取消帳單</strong></button>\-->\
    //          <div class="fast-food-group" layout="row" ng-show="isModeNavMenu(\'fastFood\')">\
    //            <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button" title="{{::ui[\'search_bill\']}}" ng-show="config.topMenu.print_bill" ng-click="switchMode2({modeFastFood: \'searchBill\'}, false);takeOut.searchBill();"><strong>{{::ui[\'search_bill\']}}</strong></button> \
    //            <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button" title="{{::ui[\'print_bill\']}}" ng-click="switchMode2({modeFastFood: \'printBill\'}, false);takeOut.viewOrder();" ng-show="config.topMenu.print_bill"><strong>{{ui[\'print_bill\']}}</strong></button> \
    //            <!--<button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button" title="{{::ui[\'order_mode\']}}" ng-click="switchMode2({mode: \'order\', modeFastFood: \'order\'}, true);" ng-show="config.topMenu.order_mode"><strong>{{ui[\'order_mode\']}}</strong></button>--> \
    //            <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button" title="{{::ui[\'take_away_order_top_bar_order\']}}" ng-show="config.topMenu.print_bill" ng-click="switchMode2({modeFastFood: \'writeOrder\'}, false);takeOut.takeOrder();"><strong>{{::ui[\'take_away_order_top_bar_order\']}}</strong></button> \
    //            <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button btn-view" title="{{::ui[\'take_away_order_top_bar_view\']}}" ng-show="config.topMenu.print_bill" ng-click="switchMode2({modeFastFood: \'normal\'}, false);takeOut.viewOrder();"><strong>{{::ui[\'take_away_order_top_bar_view\']}}</strong></button>\
    //            <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active" style="background-color:white" ng-show="uc.ecrPayment && !MainService.getOtherWriteOrderServiceFullDetail().isCardManually"  ng-click="takeOut.changeSwipeOrManually()"><strong>刷卡</strong></button>\
    //            <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active" style="background-color:white" ng-show="uc.ecrPayment && MainService.getOtherWriteOrderServiceFullDetail().isCardManually" ng-controller="keypadControllerOpenItem" ng-click="takeOut.changeSwipeOrManually()"><strong>鍵盤輸入</strong></button>\
    //          </div>\
    //            \
    //            <div class="fast-food-in-order-group" layout="row" ng-if="isModeNavMenu(\'fastFoodInOrder\')" ng-init="takeOut.topBarInit();">\
    //            <button ng-controller="userClockController" class="btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active" title="{{ui[\'take_away_take_attendance\']}}" ng-click="takeAttendanceOnTopBar();" ng-show="MainService.getOtherWriteOrderService().mode == MainService.getOtherWriteOrderService().schema.orderPage && (config.page.duty == \'yes\' || config.page.duty)"><strong>{{ui[\'take_away_take_attendance\']}}</strong></button> \
    //            <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active color-show" ng-style="takeOut.getBtnBackgroundColor(isModeOrder(\'foodControl\'))" title="{{::ui[\'food_control\']}}" ng-click="toggleFoodControlMode()" ng-show="config.topMenu.food_control && isModeOrder(\'all\') && isModeOrderMenu() && isModeFloor(\'order\')    && !isModeOrder(\'billingSummary\')"><strong>{{ui[\'food_control\']}}</strong></button> \
    //            <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active color-show" ng-style="takeOut.getBtnBackgroundColor(modeCheckOrderItem(\'modifier\'))" title="{{::ui[\'modifier\']}}" ng-click="MainService.modeToggleModifier()" ng-show="config.topMenu.modifier && isModeOrder(\'all\') && isModeOrderMenu() && !isModeOrder(\'billingSummary\')"><strong>{{ui[\'modifier\']}}</strong></button> \
    //            <button class="btn-special btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active color-show" ng-style="takeOut.getBtnBackgroundColor(modeCheckOrder(\'specialMessage\'))" title="{{::ui[\'special_message\']}}" ng-click="MainService.modeToggleSpecialMessage();" ng-show="config.topMenu.special_message && isModeOrder(\'all\') && isModeOrderMenu() && isModeFloor(\'order\') && MainService.getOtherWriteOrderService().mode == MainService.getOtherWriteOrderService().schema.orderPage && !isModeOrder(\'billingSummary\')"><strong>{{ui[\'special_message\']}}</strong></button> \
    //            <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active" title="{{::ui[\'details\']}}" ng-click="modeToggleItemDetail($event)" ng-show="config.topMenu.item_details && isModeOrder(\'all\') && isModeOrderMenu() && !isModeOrder(\'billingSummary\')"><strong>{{ui[\'details\']}}</strong></button> \
    //            <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button active  btn-fastfood-order" title="{{::ui[\'take_away_order_top_bar_order\']}}" ng-show="config.topMenu.print_bill" ng-click=""><strong>{{::ui[\'take_away_order_top_bar_order\']}}</strong></button> \
    //            <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button btn-view" title="{{::ui[\'take_away_order_top_bar_view\']}}" ng-show="config.topMenu.print_bill && !isModeOrder(\'billingSummary\')" ng-click="switchMode2({modeFastFood: \'normal\'}, false);takeOut.viewOrder();"><strong>{{::ui[\'take_away_order_top_bar_view\']}}</strong></button> \
    //            <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active" style="background-color:white" ng-show="!MainService.getOtherWriteOrderService().modeSetting.isTakeout" ng-click="takeOut.changeTakeoutOrDinning()"><strong>堂食</strong></button>\
    //            <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active" style="background-color:white" ng-show="MainService.getOtherWriteOrderService().modeSetting.isTakeout" ng-click="takeOut.changeTakeoutOrDinning()"><strong>外賣</strong></button>\
    //            <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active" style="background-color:white" ng-show="uc.ecrPayment && !MainService.getOtherWriteOrderServiceFullDetail().isCardManually"  ng-click="takeOut.changeSwipeOrManually()"><strong>刷卡</strong></button>\
    //            <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active" style="background-color:white" ng-show="MainService.getOtherWriteOrderServiceFullDetail().isCardManually" ng-controller="keypadControllerOpenItem" ng-click="uc.ecrPayment && takeOut.changeSwipeOrManually()" ng-init="initPaymentKeypad();"><strong>鍵盤輸入</strong></button>\
    //          </div>\
    //            \
    //          <div class="delivery-order-group" layout="row" ng-show="isModeNavMenu(\'deliveryOrder\')">\
    //            <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button" title="{{::ui[\'search_bill\']}}" ng-show="config.topMenu.print_bill" ng-click="switchMode2({modeDeliveryOrder: \'searchBill\'}, false, \'delivery_order\');deliveryOrder.searchBill();"><strong>{{::ui[\'search_bill\']}}</strong></button> \
    //            <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button" title="{{::ui[\'print_bill\']}}" ng-click="switchMode2({modeDeliveryOrder: \'printBill\'}, false, \'delivery_order\');deliveryOrder.viewOrder();" ng-show="config.topMenu.print_bill"><strong>{{ui[\'print_bill\']}}</strong></button> \
    //            <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button" title="取消帳單" ng-click="switchMode2({modeDeliveryOrder: \'voidBill\'}, false, \'delivery_order\');deliveryOrder.viewOrder();" ng-show="config.topMenu.print_bill"><strong>取消帳單</strong></button>\
    //            <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button" title="{{::ui[\'take_away_order_top_bar_order\']}}" ng-show="config.topMenu.print_bill" ng-click="switchMode2({modeDeliveryOrder: \'writeOrder\'}, false, \'delivery_order\');deliveryOrder.takeOrder();"><strong>{{::ui[\'take_away_order_top_bar_order\']}}</strong></button> \
    //            <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button btn-view" title="{{::ui[\'take_away_order_top_bar_view\']}}" ng-show="config.topMenu.print_bill" ng-click="switchMode2({modeDeliveryOrder: \'normal\'}, false, \'delivery_order\');deliveryOrder.viewOrder();"><strong>{{::ui[\'take_away_order_top_bar_view\']}}</strong></button> \
    //            \
    //          </div>\
    //          <div class="delivery-order-in-order-group" layout="row" ng-show="isModeNavMenu(\'deliveryOrderInOrder\')">\
    //          <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active" title="{{::ui[\'modifier\']}}" ng-click="MainService.modeToggleModifier()" ng-show="config.topMenu.modifier && isModeOrder(\'normal\') && isModeOrderMenu()"><strong>{{ui[\'modifier\']}}</strong></button> \
    //            <button class="btn-special btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active" title="{{::ui[\'special_message\']}}" ng-click="switchMode2({modeOrder: \'specialMessage\', modeItem: \'normal\'}, true, \'special_message\', \'special_message\');" ng-show="config.topMenu.special_message && isModeOrder(\'all\') && isModeOrderMenu() && isModeFloor(\'order\') && MainService.deliveryOrderService.isOrderPage();"><strong>{{ui[\'special_message\']}}</strong></button> \
    //            <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active" title="{{::ui[\'details\']}}" ng-click="modeToggleItemDetail($event)" ng-show="config.topMenu.item_details && isModeOrder(\'normal\') && isModeOrderMenu()"><strong>{{ui[\'details\']}}</strong></button> \
    //            <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button active  btn-takeaway-order" title="{{::ui[\'take_away_order_top_bar_order\']}}" ng-show="config.topMenu.print_bill" ng-click=""><strong>{{::ui[\'take_away_order_top_bar_order\']}}</strong></button> \
    //            <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button btn-view" title="{{::ui[\'take_away_order_top_bar_view\']}}" ng-show="config.topMenu.print_bill" ng-click="switchMode2({modeDeliveryOrder: \'normal\'}, false, \'delivery_order\');deliveryOrder.viewOrder();"><strong>{{::ui[\'take_away_order_top_bar_view\']}}</strong></button> \
    //          </div>\
    //            \
    //          <!-- ng-show="isModeNavMenu(\'editTable\')" -->\
    //          <div class="shared-group" layout="row" ng-show="(isModeNavMenu(\'editTable\') || isMode()) && MainService.RMSTable.noOfScene > 1 ">\
    //            <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active md-40p" title="" ng-click="changeZone(\'prev\')"><strong><i class="md-icon dp70 solo">keyboard_arrow_left</i></strong></button> \
    //            <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active md-40p" title="" ng-click="changeZone(\'next\')"><strong><i class="md-icon dp70 solo">keyboard_arrow_right</i></strong></button> \
    //          </div>\
    //          <div class="edit-group" layout="row" ng-show="isModeNavMenu(\'editTable\')">\
    //          \
    //            <!-- scene control --> \
    //            <input id="current-table-label" type="text">\
    //            <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button save proceed no-active" title="儲存設定" ng-click="saveTable()"><strong>儲存設定</strong></button> \
    //            <div layout="column" class="column">\
    //              <div layout="row" class="row"> \
    //                <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active d33" title="上" ng-click="rotate(\'left\')"><strong><i class="md-icon dp24 solo">rotate_left</i></strong></button> \
    //                <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active d33" title="上" ng-click="rotate(\'right\')"><strong><i class="md-icon dp24 solo">rotate_right</i></strong></button> \
    //                <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active d33" title="上" ng-click="move(\'up\')"><strong><i class="md-icon dp24 solo">arrow_upward</i></strong></button> \
    //                <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active d33" title="下" ng-click="move(\'down\')"><strong><i class="md-icon dp24 solo">arrow_downward</i></strong></button> \
    //                <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active d33" title="左" ng-click="move(\'left\')"><strong><i class="md-icon dp24 solo">arrow_back</i></strong></button> \
    //                <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active d33" title="右" ng-click="move(\'right\')"><strong><i class="md-icon dp24 solo">arrow_forward</i></strong></button> \
    //                <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active d33" title="" ng-click="scale(\'up\')">\
    //                  <md-icon class="dp24 flex-center" md-svg-src="assets/icon/scale-up.svg" style="color: orange;" alt="RMS"></md-icon> \
    //                </button> \
    //              </div>\
    //              <div layout="row" class="row">\
    //                <button class="btn-multi-edit btn md-rounded-square md-accent md-hue-3 md-raised md-button toggle d33" title="同時管理多張枱" ng-click="toggleGroupEdit(); toggleActive($event);"><strong>多</strong></button> \
    //                <button class="btn-grid btn md-rounded-square md-accent md-hue-3 md-raised md-button toggle d33" title="輔助格線顯示開關" ng-click="toggleGrid(); toggleActive($event);"><md-icon class="dp24" md-svg-src="assets/icon/grid-two-up.svg"></md-icon></button> \
    //                <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button toggle d33" title="顯示範圍參考" ng-click="toggleSVG(\'viewport-helper\'); toggleActive($event);"><strong><i class="md-icon dp24 solo">aspect_ratio</i></strong></button> \
    //                <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active d33" title="" ng-click="restore()"><strong><i class="md-icon dp24 solo">undo</i></strong></button> \
    //                <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active cancel d33" title="" ng-click="delete()"><strong><i class="md-icon dp24 solo">clear</i></strong></button> \
    //                <div id="add-table-container" style="position: relative">\
    //                  <button class="btn-add-table btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active d33" title="加檯" ng-click="toggleEditTable()">\
    //                    <strong>加檯</strong>\
    //                  </button> \
    //                  <!--<new-table-shape style="" class=""></new-table-shape>-->\
    //                </div>\
    //                <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active d33" title="" ng-click="scale(\'down\')">\
    //                  <md-icon class="dp24 flex-center" md-svg-src="assets/icon/scale-down.svg" style="color: orange;" alt="RMS"></md-icon> \
    //                </button> \
    //              </div>\
    //            </div> \
    //            <!--<button class="btn-edit-table btn md-rounded-square md-accent md-hue-3 md-raised md-button" title="修改檯面" ng-click="toggleNavMode(\'editTable\')"><strong>修改檯面</strong></button>--> \
    //            <!--<button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button" title="上一個場景" ng-click="prevZone()"><strong>上個場景</strong></button> \
    //            <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button" title="下一個場景" ng-click="nextZone()"><strong>下個場景</strong></button>--> \
    //            <!--<md-button class="md-raised md-icon-button md-mini md-rounded-square" style="color: black"> \
    //                S{{SCENE.name}} \
    //            </md-button> --> \
    //          </div>\
    //            \
    //          <!-- system notification --> \
    //            <!--<md-button md-no-ink="true" class="md-icon-button md-mini" aria-label="系統訊息" ng-click="showAlert($event)">--> \
    //             <!-- <md-icon md-svg-src="assets/icon/error.svg"></md-icon> --> \
    //             <!-- <i class="md-icon dp45">warning</i> --> \
    //             <!--<md-tooltip> \
    //               系統訊息 \
    //             </md-tooltip> \
    //             <i class="md-icon dp45">announcement</i> \
    //            </md-button>--> \
    //            <!-- primary function/each page --> \
    //            \
    //            \
    //            <!-- login logout -->\
    //            <div class="primary-group" layout="row">\
    //              <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active" title="{{::ui[\'reload\']}}" ng-click="refreshScreen();" ng-show="config.topMenu.reload"><strong>{{ui[\'reload\']}}</strong></button> \
    //              \
    //              <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active" title="{{::ui[\'login\']}}" ng-click="login()" ng-hide="isLogin()"><strong>{{ui[\'login\']}}</strong></button> \
    //              \
    //              <button class="btn md-rounded-square md-accent md-hue-3 md-raised md-button no-active" title="{{::ui[\'logout\']}}" ng-click="logout($event)" ng-show="isLogin()"><strong>{{ui[\'logout\']}}</strong></button> \
    //            </div>\
    //        </div> \
    //      </div> \
    //</md-toolbar>');
    //}]);

    angular.module('tmpl/status-bar.html', []).run(["$templateCache", function ($templateCache) {
        $templateCache.put('tmpl/status-bar.html',
        '<md-toolbar id="status-bar" class="md-hue-2 md-whiteframe-glow-z2 navbar-fixed-bottom md-rms-theme toolbar-bottom flex-center" layout="row"> \
        <!-- 100% - 95% --> \
        <!-- <div flex="80" class="messages"><i class="md-icon warn">expand_more</i><i class="md-icon">info_outline</i>收據已經列印到 *S4 打印機</div> 模式狀態--> \
        <div flex class="fill-height" style="" ><div id="status-message" ng-show="MainService.checkIsNotTakeAwayMode();"><div class="content message"></div></div></div> \
        <!-- <div flex class="fill-height" style="height: 100%;"><div id="warning-message"><div class="content message"></div></div></div> --> \
        \
        <div flex="60" class="legend-container" ng-show="isMode();"> \
          <ul class="legend"> \
            <li ng-repeat="(key, value) in ::config.statusBar.legend track by $index" class="{{strConvertTo(\'camel\', key )}}" ng-if="value"><i class="md-icon">stop</i>{{ui[key]}}</li> \
          </ul> \
        </div> \
        <div flex="20" class="employee" flex> \
        {{::ui["txt_staff"]}}：{{MainService.UserManager.staffName || ui["txt_logged_out"]}}\
          <!-- height 100% is for FF height: 100%; --> \
          <!-- <div class="" style="">服務員：<div class="name">{{MainService.UserManager.staffName || \'未登入/已登出\'}}</div></div>--> \
        </div> \
        <div flex="10" id="clock"></div> \
        </md-toolbar>');
    }]);

    //<!-- fixed for controller DialogControllerLogin -->
    angular.module('tmpl/login.html', []).run(["$templateCache", function ($templateCache) {
        $templateCache.put('tmpl/login.html',
          '<div id="login-box" class="keypad-container" ng-controller="DialogControllerLogin"> \
        <form name="loginForm"> \
            <md-dialog aria-label="Login" class="keypad" ng-app> \
                <md-dialog-content class="sticky-container login-html"> \
                    <div class="message-container" style="padding: 0; max-width: 240px"> \
                        <div ng-show="loginError" ng-class="{warn: loginError}">{{ ui["password_fail"] || "密碼 或 用戶名稱錯誤"}}</div> \
                    </div> \
                    <md-input-container class="login"> \
                        <label>{{ui[modeName] || "用戶名稱 / 密碼"}}</label> \
                        <input name="loginField" class="login-input dialog-close" login-input login-input-enter="next()" ng-model="loginValue" ng-trim="false" ng-change="change()" type="password" custom-always-focus="true" autocomplete="off"> \
                    </md-input-container> \
                    \
                    <div class="materialize-keypad col-5x3 keypad-container" style=""> \
                      <div class="row"> \
                        <div class="col s4"  ng-repeat="element in ::keypad">\
                          <button class="btn md-button md-grid-button" ng-click="input(element.text)" ng-if="!element.useIcon">{{::element.text}}</button>\
                          <button class="btn md-button md-grid-button" ng-click="reset()" ng-if="element.useIcon && element.text==\'reset\'">\
                            <i ng-show="loginValue && mode == \'username\'|| mode == \'password\'" class="md-icon">replay</i> \
                            <i ng-hide="loginValue && mode == \'username\'|| mode != \'username\'" class="md-icon">clear</i> \
                          </button>\
                          <button class="btn md-button md-grid-button" ng-click="backspace()" ng-if="element.useIcon && element.text==\'backspace\'">\
                            <i class="md-icon">backspace</i> \
                          </button>\
                        </div> \
                        <div class="col s12">\
                          <button class="btn md-button md-grid-button submit" ng-click="next()">{{::ui[\'done\']}}</button>\
                        </div> \
                      </div> \
                    </div> \
                    \
                    <!--<md-grid-list md-cols="6" md-row-height="1:1" md-gutter="1px" style="" class="keypad-grid"> \
                        <md-grid-tile ng-repeat="element in keypad" md-colspan="2" md-rowspan="2"> \
                            <md-button md-no-ink="true" class="md-raised" ng-click="input(element.text)" ng-if="!element.useIcon"> \
                                {{::element.text}} \
                            </md-button> \
                            <md-button md-no-ink="true" class="md-raised" ng-click="reset()" ng-if="element.useIcon && element.text==\'reset\'"> \
                                <i ng-show="loginValue && mode == \'username\'|| mode == \'password\'" class="md-icon dp40">replay</i> \
                                <i ng-hide="loginValue && mode == \'username\'|| mode != \'username\'" class="md-icon dp40">clear</i> \
                            </md-button> \
                            <md-button md-no-ink="true" class="md-raised" ng-click="backspace()" ng-if="element.useIcon && element.text==\'backspace\'"> \
                                <i class="md-icon dp40">backspace</i> \
                            </md-button> \
                        </md-grid-tile> \
                        <md-grid-tile md-rowspan="2" md-colspan="6" ng-show="loginValue"> \
                            <md-button md-no-ink="true" class="md-raised submit" ng-click="next()">{{::ui["done"] || "完成"}}</md-button> \
                        </md-grid-tile> \
                    </md-grid-list>--> \
                    <!-- </div> --> \
                </md-dialog-content> \
            </md-dialog> \
        </form> \
    </div>');
    }]);

    //<!-- flexible for specified or apply any controller, since $mdDialog.show can take a param for any controller -->
    angular.module('tmpl/login2.html', []).run(["$templateCache", function ($templateCache) {
        $templateCache.put('tmpl/login2.html',
          '<div id="login-box" class="keypad-container"> \
        <form name="loginForm"> \
            <md-dialog aria-label="Login" class="keypad" ng-app> \
                <md-dialog-content class="sticky-container login2-html"> \
                    <div class="message-container" style="padding: 0; max-width: 240px"> \
                        <div ng-show="loginError" ng-class="{warn: loginError}">{{ ui["password_fail"] || "密碼 或 用戶名稱錯誤"}}</div> \
                    </div> \
                    <md-input-container class="login"> \
                        <label>{{ui[modeName] || "用戶名稱 / 密碼"}}</label> \
                        <input name="loginField" class="login-input dialog-close" login-input login-input-enter="next()" ng-model="loginValue" ng-trim="false" ng-change="change()" type="password" custom-always-focus="true" autocomplete="off"> \
                    </md-input-container> \
                    \
                    <div class="materialize-keypad col-5x3 keypad-container" style=""> \
                      <div class="row"> \
                        <div class="col s4" ng-repeat="element in ::keypad">\
                          <button class="btn md-button md-grid-button" ng-click="input(element.text)" ng-if="!element.useIcon">{{::element.text}}</button>\
                          <button class="btn md-button md-grid-button" ng-click="reset()" ng-if="element.useIcon && element.text==\'reset\'">\
                            <i ng-show="loginValue && mode == \'username\'|| mode == \'password\'" class="md-icon">replay</i> \
                            <i ng-hide="loginValue && mode == \'username\'|| mode != \'username\'" class="md-icon">clear</i> \
                          </button>\
                          <button class="btn md-button md-grid-button" ng-click="backspace()" ng-if="element.useIcon && element.text==\'backspace\'">\
                            <i class="md-icon">backspace</i> \
                          </button>\
                        </div> \
                        <div class="col s12">\
                          <button class="btn md-button md-grid-button submit" ng-click="next()">{{::ui["done"] || "完成"}}</button>\
                        </div> \
                      </div> \
                    </div> \
                    \
                    <!--<md-grid-list md-cols="6" md-row-height="1:1" md-gutter="1px" style="" class="keypad-grid"> \
                        <md-grid-tile ng-repeat="element in keypad" md-colspan="2" md-rowspan="2"> \
                            <md-button md-no-ink="true" class="md-raised" ng-click="input(element.text)" ng-if="!element.useIcon"> \
                                {{::element.text}} \
                            </md-button> \
                            <md-button md-no-ink="true" class="md-raised" ng-click="reset()" ng-if="element.useIcon && element.text==\'reset\'"> \
                                <i ng-show="loginValue && mode == \'username\'|| mode == \'password\'" class="md-icon dp40">replay</i> \
                                <i ng-hide="loginValue && mode == \'username\'|| mode != \'username\'" class="md-icon dp40">clear</i> \
                            </md-button> \
                            <md-button md-no-ink="true" class="md-raised" ng-click="backspace()" ng-if="element.useIcon && element.text==\'backspace\'"> \
                                <i class="md-icon dp40">backspace</i> \
                            </md-button> \
                        </md-grid-tile> \
                        <md-grid-tile md-rowspan="2" md-colspan="6" ng-show="loginValue"> \
                            <md-button md-no-ink="true" class="md-raised submit" ng-click="next()">{{::ui["done"] || "完成"}}</md-button> \
                        </md-grid-tile> \
                    </md-grid-list>--> \
                    <!-- </div> --> \
                </md-dialog-content> \
            </md-dialog> \
        </form> \
    </div>');
    }]);

    angular.module('tmpl/voidItemApproval.html', []).run(["$templateCache", function ($templateCache) {
        $templateCache.put('tmpl/voidItemApproval.html',
        '<div id="void-box" class="keypad-container" ng-controller="DialogControllerVoidItemApproval"> \
        <form name="voidForm"> \
            <md-dialog aria-label="Void" class="keypad" ng-app> \
                <md-dialog-content class="sticky-container voiditemapproval-html" layout="row"> \
                    <div flex="50" class="input-container">\
                      <div class="message-container" style="padding: 0; max-width: 240px"> \
                          <div ng-show="loginError" ng-class="{warn: loginError}">{{ ui["password_fail"] || "密碼 或 用戶名稱錯誤"}}</div> \
                      </div> \
                      <md-input-container class="login"> \
                          <label>{{::ui[\'manager_confirmation\']}}/{{ui[modeName] || "用戶名稱 / 密碼"}}</label> \
                          <input name="loginField" class="login-input dialog-close" login-input login-input-enter="next()" ng-model="loginValue" ng-trim="false" ng-change="change()" type="text" custom-always-focus="true" autocomplete="off"> \
                      </md-input-container> \
                      <md-grid-list md-cols="6" md-row-height="1:1" md-gutter="1px" style="" class="keypad-grid"> \
                          <md-grid-tile ng-repeat="element in ::keypad" md-colspan="2" md-rowspan="2"> \
                              <md-button md-no-ink="true" class="md-raised" ng-click="input(element.text)" ng-if="!element.useIcon"> \
                                  {{::element.text}} \
                              </md-button> \
                              <md-button md-no-ink="true" class="md-raised" ng-click="reset()" ng-if="element.useIcon && element.text==\'reset\'"> \
                                  <i ng-show="loginValue && mode == \'username\'|| mode == \'password\'" class="md-icon dp40">replay</i> \
                                  <i ng-hide="loginValue && mode == \'username\'|| mode != \'username\'" class="md-icon dp40">clear</i> \
                              </md-button> \
                              <md-button md-no-ink="true" class="md-raised" ng-click="backspace()" ng-if="element.useIcon && element.text==\'backspace\'"> \
                                  <i class="md-icon dp40">backspace</i> \
                              </md-button> \
                          </md-grid-tile> \
                          <!-- finish button --> \
                          <!-- ng-show="loginValue"  --> \
                          <md-grid-tile md-rowspan="2" md-colspan="6" ng-show="loginValue"> \
                              <md-button md-no-ink="true" class="md-raised submit" ng-click="next()">{{::ui["done"] || "完成"}}</md-button> \
                          </md-grid-tile> \
                      </md-grid-list> \
                      <!-- </div> --> \
                    </div>\
                    <div flex class="void-list-container">\
                    <h2>{{::ui[\'food_to_be_cancelled\']}}</h2>\
                      <ul>\
                        <li ng-repeat="item in MainService.pendingVoidItem">{{item.getName()}} {{item.qty * -1}} {{item.voidRemark}}</li> \
                      </ul>\
                    </div>\
                </md-dialog-content> \
            </md-dialog> \
        </form> \
    </div>');
    }]);

    angular.module('tmpl/keypad.html', []).run(["$templateCache", function ($templateCache) {
        $templateCache.put('tmpl/keypad.html',
          '<div id="generic-keypad" class="keypad-container"> \
            <form>\
          <md-dialog aria-label="keypad" class="keypad" ng-app > \
            \
            <md-toolbar> \
              <div class="md-toolbar-tools"> \
                <h2>{{title || "Keypad"}}</h2> \
                <span flex></span> \
                <!-- <md-button md-no-ink="true" class="md-icon-button" ng-click="closeDialog()"> \
                  <i class="md-icon dp45">clear</i> \
                </md-button> --> \
              </div> \
            </md-toolbar> \
              \
            <md-dialog-content class="sticky-container keypad-html"> \
                <md-input-container class="login"> \
                <input class="dialog-close" ng-model="keypadInfo.value" login-input login-input-enter="next()"  ng-trim="false" type="text" custom-always-focus="true" autocomplete="off"> \
                </md-input-container> \
                \
                <md-grid-list md-cols="6" md-row-height="1:1" md-gutter="1px"  class="keypad-grid" style=""> \
                    <md-grid-tile ng-repeat="element in ::keypad" md-colspan="2" md-rowspan="2"> \
                        <md-button md-no-ink="true" class="md-raised" ng-click="input(element.text)" ng-if="!element.useIcon"> \
                            {{::element.text}} \
                        </md-button> \
                        \
                        <md-button md-no-ink="false" class="md-raised" ng-click="reset()" ng-if="element.useIcon && element.text==\'reset\'"> \
                            <i ng-show="keypadInfo.value" class="md-icon dp40">replay</i> \
                            <i ng-hide="keypadInfo.value" class="md-icon dp40">clear</i> \
                        </md-button> \
                        \
                        <md-button md-no-ink="true" class="md-raised" ng-click="backspace()" ng-if="element.useIcon && element.text==\'backspace\'"> \
                            <i class="md-icon dp40">backspace</i> \
                        </md-button> \
                    </md-grid-tile> \
                    <md-grid-tile md-colspan="2" md-rowspan="2" ng-if="MainService.modeCheckOrder(\'foodControl\')"> \
                        <md-button md-no-ink="true" class="md-raised" ng-click="input(\'-1\');next($event);"><span class="dp24 solo">{{::ui[\'txt_suspend\']}}</span></md-button> \
                    </md-grid-tile> \
                    <md-grid-tile md-colspan="2" md-rowspan="2" ng-if="MainService.modeCheckOrder(\'foodControl\')"> \
                        <md-button md-no-ink="true" class="md-raised" ng-click="input(\'-2\');next($event);"><span class="dp24 solo">{{::ui[\'txt_restore\']}}</span></md-button> \
                    </md-grid-tile> \
                    <md-grid-tile md-colspan="2" md-rowspan="2" ng-if="MainService.modeCheckOrder(\'foodControl\')"> \
                        <md-button md-no-ink="true" class="md-raised" ng-click="input(\'-3\');next($event);"><span class="dp24 solo">{{::ui[\'txt_preparing\']}}</span></md-button> \
                    </md-grid-tile> \
                    <!-- finish button --> \
                    <!-- ng-show="keypadInfo.value"  --> \
                    <md-grid-tile md-rowspan="2" md-colspan="6" ng-show="keypadInfo.value"> \
                        <md-button md-no-ink="false" class="md-raised submit" ng-click="next($event)">{{::ui["done"] || "完成"}}</md-button> \
                    </md-grid-tile> \
                </md-grid-list> \
             <!-- </div> --> \
            </md-dialog-content> \
        </md-dialog> \
        </form>\
      </div>');
    }]);

    angular.module('tmpl/watch-register-keypad.html', []).run(["$templateCache", function ($templateCache) {
        $templateCache.put('tmpl/watch-register-keypad.html',
          '<div id="generic-keypad" class="keypad-container" ng-controller="watchManagemenetController"> \
            <form>\
          <md-dialog aria-label="keypad" class="keypad" ng-app > \
            \
            <md-toolbar> \
              <div class="md-toolbar-tools"> \
                <h2>{{title || "Keypad"}}</h2> \
                <span flex></span> \
                <!-- <md-button md-no-ink="true" class="md-icon-button" ng-click="closeDialog()"> \
                  <i class="md-icon dp45">clear</i> \
                </md-button> --> \
              </div> \
            </md-toolbar> \
              \
            <md-dialog-content class="sticky-container keypad2-html"> \
                     <md-input-container class="login"> \
                       <input class="dialog-close" ng-model="keypadInfo.value" login-input login-input-enter="next()"  ng-trim="false" type="text" custom-always-focus="true" autocomplete="off"> \
                     </md-input-container> \
                            \
                     <md-grid-list md-cols="6" md-row-height="1:1" md-gutter="1px"  class="keypad-grid" style=""> \
                         <md-grid-tile ng-repeat="element in ::keypad" md-colspan="2" md-rowspan="2"> \
                          <md-button md-no-ink="true" class="md-raised" ng-click="input(element.text)" ng-if="!element.useIcon"> \
                            {{::element.text}} \
                          </md-button> \
                            \
                          <md-button md-no-ink="false" class="md-raised" ng-click="reset()" ng-if="element.useIcon && element.text==\'reset\'"> \
                            <i ng-show="keypadInfo.value" class="md-icon dp40">replay</i> \
                            <i ng-hide="keypadInfo.value" class="md-icon dp40">clear</i> \
                          </md-button> \
                            \
                          <md-button md-no-ink="true" class="md-raised" ng-click="backspace()" ng-if="element.useIcon && element.text==\'backspace\'"> \
                            <i class="md-icon dp40">backspace</i> \
                          </md-button> \
                         </md-grid-tile> \
                            \
                         <!-- finish button --> \
                         <!-- ng-show="keypadInfo.value"  --> \
                         <md-grid-tile md-rowspan="2" md-colspan="6" ng-show="keypadInfo.value"> \
                           <md-button md-no-ink="false" class="md-raised submit" ng-click="next($event)">{{::ui["done"] || "完成"}}</md-button> \
                         </md-grid-tile> \
                     </md-grid-list> \
             <!-- </div> --> \
            </md-dialog-content> \
        </md-dialog> \
        </form>\
      </div>');
    }]);

    angular.module('tmpl/paymentPermission.html', []).run(["$templateCache", function ($templateCache) {
        $templateCache.put('tmpl/paymentPermission.html',
          '<div id="generic-keypad" class="keypad-container" ng-controller="paymentPermissionController"> \
            <form>\
          <md-dialog aria-label="keypad" class="keypad" ng-app > \
            \
            <md-toolbar> \
              <div class="md-toolbar-tools"> \
                <h2>{{title || "Keypad"}}</h2> \
                <span flex></span> \
                <!-- <md-button md-no-ink="true" class="md-icon-button" ng-click="closeDialog()"> \
                  <i class="md-icon dp45">clear</i> \
                </md-button> --> \
              </div> \
            </md-toolbar> \
              \
            <md-dialog-content class="sticky-container paymentPermission-html"> \
                     <md-input-container class="login"> \
                       <input class="dialog-close" ng-model="keypadInfo.value" login-input login-input-enter="next()"  ng-trim="false" type="text" custom-always-focus="true" autocomplete="off"> \
                     </md-input-container> \
                            \
                     <md-grid-list md-cols="6" md-row-height="1:1" md-gutter="1px"  class="keypad-grid" style=""> \
                         <md-grid-tile ng-repeat="element in ::keypad" md-colspan="2" md-rowspan="2"> \
                          <md-button md-no-ink="true" class="md-raised" ng-click="input(element.text)" ng-if="!element.useIcon"> \
                            {{::element.text}} \
                          </md-button> \
                            \
                          <md-button md-no-ink="false" class="md-raised" ng-click="reset()" ng-if="element.useIcon && element.text==\'reset\'"> \
                            <i ng-show="keypadInfo.value" class="md-icon dp40">replay</i> \
                            <i ng-hide="keypadInfo.value" class="md-icon dp40">clear</i> \
                          </md-button> \
                            \
                          <md-button md-no-ink="true" class="md-raised" ng-click="backspace()" ng-if="element.useIcon && element.text==\'backspace\'"> \
                            <i class="md-icon dp40">backspace</i> \
                          </md-button> \
                         </md-grid-tile> \
                            \
                         <!-- finish button --> \
                         <!-- ng-show="keypadInfo.value"  --> \
                         <md-grid-tile md-rowspan="2" md-colspan="6" ng-show="keypadInfo.value"> \
                           <md-button md-no-ink="false" class="md-raised submit" ng-click="next($event)">{{::ui["done"] || "完成"}}</md-button> \
                         </md-grid-tile> \
                     </md-grid-list> \
             <!-- </div> --> \
            </md-dialog-content> \
        </md-dialog> \
        </form>\
      </div>');
    }]);


    angular.module('tmpl/keypad-free.html', []).run(["$templateCache", function ($templateCache) {
        $templateCache.put('tmpl/keypad-free.html',
          '<div id="free-item-keypad" class="keypad-container full-keyboard-container"> \
          <md-dialog aria-label="keypad" class="keypad" ng-app ng-keypress="keypressed($event)"> \
            \
            <md-toolbar> \
              <div class="md-toolbar-tools"> \
                <h2>{{::ui[\'custom\']}}</h2> \
                <span flex></span> \
              </div> \
            </md-toolbar> \
              \
            <md-dialog-content class="sticky-container keypadfree-html"> \
               <div layout="row"> \
                 <md-input-container class="login" flex="30"> \
                   <label>{{ui[\'item_name\']}}</label> \
                   <input name="item-name" autofocus ng-model="keypadInfo.name" ng-class="{\'focused\' : currentFieldName == \'name\'}" ng-trim="false" type="text" ng-click="focusField(\'name\')"> \
                 </md-input-container> \
                 <md-input-container class="login" flex="30"> \
                   <label>{{ui[\'item_price\']}}</label> \
                   <input name="item-price" type="text" ng-model="keypadInfo.value" ng-class="{\'focused\' : currentFieldName == \'value\'}" ng-trim="false" ng-click="focusField(\'value\')"> \
                 </md-input-container> \
               </div> \
                      \
                <div class="md-grid-container keyboard-full" style="display: inline-block;"> \
                  <!-- keyboard --> \
                  <div class="materialize-keypad col-10x4" style=""> \
                    <div class="row"> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="inputFree(\'q\')">q</button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="inputFree(\'w\')">w</button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="inputFree(\'e\')">e</button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="inputFree(\'r\')">r</button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="inputFree(\'t\')">t</button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="inputFree(\'y\')">y</button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="inputFree(\'u\')">u</button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="inputFree(\'i\')">i</button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="inputFree(\'o\')">o</button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="inputFree(\'p\')">p</button></div> \
                    </div> \
                    <div class="row"> \
                      <div class="col" style="width:5%"></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="inputFree(\'a\')">a</button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="inputFree(\'s\')">s</button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="inputFree(\'d\')">d</button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="inputFree(\'f\')">f</button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="inputFree(\'g\')">g</button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="inputFree(\'h\')">h</button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="inputFree(\'j\')">j</button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="inputFree(\'k\')">k</button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="inputFree(\'l\')">l</button></div> \
                    </div> \
                    <div class="row"> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="inputFree(\'z\')">z</button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="inputFree(\'x\')">x</button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="inputFree(\'c\')">c</button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="inputFree(\'v\')">v</button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="inputFree(\'b\')">b</button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="inputFree(\'n\')">n</button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="inputFree(\'m\')">m</button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="inputFree(\',\')">,</button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="inputFree(\'.\')">.</button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="inputFree(\'#\')">#</button></div> \
                    </div> \
                    <div class="row"> \
                      <div class="col s10-2"><button class="btn md-button md-grid-button" ng-click="inputFree(\'tab\')">tab</button></div> \
                      <div class="col s10-5"><button class="btn md-button md-grid-button" ng-click="inputFree(\'space\')">space bar</button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button flex-center" ng-click="backspaceFree()"><i class="md-icon">backspace</i></button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="inputFree(\'-\')">-</button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="inputFree(\'*\')">*</button></div> \
                    </div> \
                  </div> \
                  <!-- keypad --> \
                  <div class="materialize-keypad col-3x4" style=""> \
                    <div class="row"> \
                      <div class="col s4"><button class="btn md-button md-grid-button" ng-click="appendNumFree(1)">1</button></div> \
                      <div class="col s4"><button class="btn md-button md-grid-button" ng-click="appendNumFree(2)">2</button></div> \
                      <div class="col s4"><button class="btn md-button md-grid-button" ng-click="appendNumFree(3)">3</button></div> \
                    </div> \
                    <div class="row"> \
                      <div class="col s4"><button class="btn md-button md-grid-button" ng-click="appendNumFree(4)">4</button></div> \
                      <div class="col s4"><button class="btn md-button md-grid-button" ng-click="appendNumFree(5)">5</button></div> \
                      <div class="col s4"><button class="btn md-button md-grid-button" ng-click="appendNumFree(6)">6</button></div> \
                    </div> \
                    <div class="row"> \
                      <div class="col s4"><button class="btn md-button md-grid-button" ng-click="appendNumFree(7)">7</button></div> \
                      <div class="col s4"><button class="btn md-button md-grid-button" ng-click="appendNumFree(8)">8</button></div> \
                      <div class="col s4"><button class="btn md-button md-grid-button" ng-click="appendNumFree(9)">9</button></div> \
                    </div> \
                    <div class="row"> \
                      <div class="col s4"><button class="cancel btn md-button md-grid-button" ng-click="cancel()"><i class="md-icon dp40" aria-hidden="false" ng-show="lastActiveField == \'item-name\' && keypadInfo.name == \'\' || lastActiveField == \'item-price\' && keypadInfo.value == \'\'">clear</i> \
                      <i ng-show="lastActiveField == \'item-name\' && keypadInfo.name != \'\' || lastActiveField == \'item-price\' && keypadInfo.value != \'\'" class="md-icon dp40" aria-hidden="false">replay</i></button></div> \
                      <div class="col s4"><button class="btn md-button md-grid-button" ng-click="appendNumFree(0)">0</button></div> \
                      <div class="col s4"><button class="proceed btn md-button md-grid-button" ng-click="nextFree($event)"><i class="md-icon dp45">done</i></button></div> \
                    </div> \
                  </div> \
                </div> \
            </md-dialog-content> \
        </md-dialog> \
      </div>');
    }]);

    angular.module('tmpl/keypad-payment-free.html', []).run(["$templateCache", function ($templateCache) {
        $templateCache.put('tmpl/keypad-payment-free.html',
          '<div id="free-payment-keypad" class="keypad-container full-keyboard-container"> \
          <md-dialog aria-label="keypad" class="keypad" ng-app ng-keypress="keypressed($event)"> \
            \
            <md-toolbar> \
              <div class="md-toolbar-tools" style="display:block"> \
                <h2 style="margin-top:20px;float:left">{{::ui["txt_keyboard_input"]}}</h2> \
                <button style="width:50px;float:right;background-color:rgb(121,85,72);" class="btn md-button md-grid-button" ng-click="close();">X</button>\
                <span flex></span> \
              </div> \
            </md-toolbar> \
              \
            <md-dialog-content class="sticky-container keypadpaymentfree-html"> \
               <div layout="row"> \
                <md-input-container flex="30">\
                  <label>{{::ui["total_amount"]}}</label>\
                  <input  ng-model="paymentKeypadObj.tender.amount"  ng-trim="false" type="text" disabled style="color:black"> \
                  </md-input-container>\
                </div>\
                <div layout="row"> \
                <md-input-container flex="30">\
                  <label>{{ui["payment_method"]}}</label>\
                  <input  ng-model="paymentKeypadObj.tender.name"  ng-trim="false" type="text" disabled style="color:black"> \
                  </md-input-container>\
                 <md-input-container class="login" flex="30"> \
                   <label>{{::ui["txt_batch_number"]}}</label> \
                   <input name="item-name" autofocus ng-model="paymentKeypadObj.batchNumber" ng-class="{\'focused\' : currentFieldName == \'batchNumber\'}" ng-trim="false" type="text" ng-click="focusField(\'batchNumber\')"> \
                 </md-input-container> \
                 <md-input-container class="login" flex="30"> \
                   <label>{{::ui["txt_trace_number"]}}</label> \
                   <input name="item-name"  ng-model="paymentKeypadObj.traceNumber" ng-class="{\'focused\' : currentFieldName == \'traceNumber\'}" ng-trim="false" type="text" ng-click="focusField(\'traceNumber\')"> \
                 </md-input-container> \
               </div> \
                      \
                <div class="md-grid-container keyboard-full" style="display: inline-block;"> \
                  <!-- keyboard --> \
                  <div class="materialize-keypad col-10x4" style=""> \
                    <div class="row" style="height:33%;"> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="paymentKeypadObj.inputValue(\'q\')">q</button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="paymentKeypadObj.inputValue(\'w\')">w</button></div> \
                      <div class="col s10-1" style="border-right:1px solid white"><button class="btn md-button md-grid-button" ng-click="paymentKeypadObj.inputValue(\'e\')">e</button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="paymentKeypadObj.inputValue(\'r\')">r</button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="paymentKeypadObj.inputValue(\'t\')">t</button></div> \
                      <div class="col s10-1" style="border-right:1px solid white"><button class="btn md-button md-grid-button" ng-click="paymentKeypadObj.inputValue(\'y\')">y</button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="paymentKeypadObj.inputValue(\'u\')">u</button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="paymentKeypadObj.inputValue(\'i\')">i</button></div> \
                      <div class="col s10-1" style="border-right:1px solid white"><button class="btn md-button md-grid-button" ng-click="paymentKeypadObj.inputValue(\'o\')">o</button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="paymentKeypadObj.inputValue(\'p\')">p</button></div> \
                    </div> \
                    <div class="row" style="height:33%;"> \
                      <div class="col" style="width:5%"></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="paymentKeypadObj.inputValue(\'a\')">a</button></div> \
                      <div class="col s10-1" style="border-right:1px solid white"><button class="btn md-button md-grid-button" ng-click="paymentKeypadObj.inputValue(\'s\')">s</button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="paymentKeypadObj.inputValue(\'d\')">d</button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="paymentKeypadObj.inputValue(\'f\')">f</button></div> \
                      <div class="col s10-1" style="border-right:1px solid white"><button class="btn md-button md-grid-button" ng-click="paymentKeypadObj.inputValue(\'g\')">g</button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="paymentKeypadObj.inputValue(\'h\')">h</button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="paymentKeypadObj.inputValue(\'j\')">j</button></div> \
                      <div class="col s10-1" style="border-right:1px solid white"><button class="btn md-button md-grid-button" ng-click="paymentKeypadObj.inputValue(\'k\')">k</button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="paymentKeypadObj.inputValue(\'l\')">l</button></div> \
                    </div> \
                    <div class="row" style="height:33%;"> \
                     <div class="col" style="width:5%"></div> \
                         <div class="col" style="width:5%"></div> \
                      <div class="col s10-1" style="border-right:1px solid white"><button class="btn md-button md-grid-button" ng-click="paymentKeypadObj.inputValue(\'z\')">z</button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="paymentKeypadObj.inputValue(\'x\')">x</button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="paymentKeypadObj.inputValue(\'c\')">c</button></div> \
                      <div class="col s10-1" style="border-right:1px solid white"><button class="btn md-button md-grid-button" ng-click="paymentKeypadObj.inputValue(\'v\')">v</button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="paymentKeypadObj.inputValue(\'b\')">b</button></div> \
                      <div class="col s10-1"><button class="btn md-button md-grid-button" ng-click="paymentKeypadObj.inputValue(\'n\')">n</button></div> \
                      <div class="col s10-1" style="border-right:1px solid white"><button class="btn md-button md-grid-button" ng-click="paymentKeypadObj.inputValue(\'m\')">m</button></div> \
                        <div class="col s10-1"><button class="btn md-button md-grid-button flex-center" ng-click="paymentKeypadObj.backspace()"><i class="md-icon">backspace</i></button></div> \
                    </div> \
                  </div> \
                  <!-- keypad --> \
                  <div class="materialize-keypad col-3x4" style=""> \
                    <div class="row"> \
                      <div class="col s4"><button class="btn md-button md-grid-button" ng-click="paymentKeypadObj.inputValue(1)">1</button></div> \
                      <div class="col s4"><button class="btn md-button md-grid-button" ng-click="paymentKeypadObj.inputValue(2)">2</button></div> \
                      <div class="col s4"><button class="btn md-button md-grid-button" ng-click="paymentKeypadObj.inputValue(3)">3</button></div> \
                    </div> \
                    <div class="row"> \
                      <div class="col s4"><button class="btn md-button md-grid-button" ng-click="paymentKeypadObj.inputValue(4)">4</button></div> \
                      <div class="col s4"><button class="btn md-button md-grid-button" ng-click="paymentKeypadObj.inputValue(5)">5</button></div> \
                      <div class="col s4"><button class="btn md-button md-grid-button" ng-click="paymentKeypadObj.inputValue(6)">6</button></div> \
                    </div> \
                    <div class="row"> \
                      <div class="col s4"><button class="btn md-button md-grid-button" ng-click="paymentKeypadObj.inputValue(7)">7</button></div> \
                      <div class="col s4"><button class="btn md-button md-grid-button" ng-click="paymentKeypadObj.inputValue(8)">8</button></div> \
                      <div class="col s4"><button class="btn md-button md-grid-button" ng-click="paymentKeypadObj.inputValue(9)">9</button></div> \
                    </div> \
                    <div class="row"> \
                      <div class="col s4"><button class="cancel btn md-button md-grid-button" ng-click="paymentKeypadObj.reset()">\
                      <i class="md-icon dp40" aria-hidden="false">replay</i></button></div> \
                      <div class="col s4"><button class="btn md-button md-grid-button" ng-click="paymentKeypadObj.inputValue(0)">0</button></div> \
                      <div class="col s4"><button class="proceed btn md-button md-grid-button" ng-click="nextFree($event);"><i class="md-icon dp45">done</i></button></div> \
                    </div> \
                  </div> \
                </div> \
            </md-dialog-content> \
        </md-dialog> \
      </div>');
    }]);

    // ng-show="modeCheckOrder(\'transfer\')"
    angular.module('tmpl/divide-bill.html', []).run(["$templateCache", function ($templateCache) {
        $templateCache.put('tmpl/divide-bill.html',
          '<div id="divide-menu" class="menu-area" layout="row" ng-controller="divideBillCtrl"> \
            <!-- sidebar column --> \
                <ng-include include-replace src="\'tmpl/leftFoodMenu.html\'"></ng-include> \
                <ng-include include-replace src="\'tmpl/rightFoodMenu.html\'"></ng-include> \
                <div id="divide-menu-option" flex="10"> \
                    <div class="md-grid-container divide-table-button-container" style="height:100%" layout="column"> \
                      <div flex="20" layout="column" class="md-custom-grid one-column table-list" style="height: 100%;">\
                        <div flex="33" class="row"> \
                           <div class="col s10-10"><button class="md-button set-food-grass normal" ng-click="MainService.addSplitList($event)">{{::ui["txt_split_table_new_group"]}}</button></div> \
                           \
                        </div> \
                        <div flex="33" class="row"> \
                           <div class="col s10-10"><button class="md-button set-food-grass normal" ng-controller="keypadController" id="btn-addSomeGroup" ng-click="addSomeGroup($event)">{{::ui["txt_split_table_numofgroup"]}}</button></div> \
                           \
                        </div> \
                        <div flex="33" class="row"> \
                           <div class="col s10-10"><button class="md-button set-food-blue normal" ng-controller="keypadController" ng-click="changeQty($event)">{{::ui["txt_split_table_food_qty"]}}</button></div> \
                           \
                        </div> \
                      </div>\
                      \
                        <!--<md-grid-list md-cols="1" md-row-height="fit" md-gutter="2" style="" flex="30"> \
                            <md-grid-tile on-finish-render ng-repeat="transfer in MainService.Cart.allSplitList | startFrom:(currentDivdePage - 1) * 3 | limitTo: 3" ng-click="MainService.changeSplitList($index)"> \
                                <md-button md-no-ink="true" class="md-raised set-food-blue">{{transfer.name}}</md-button> \
                            </md-grid-tile>\
                            \
                            <md-grid-tile> \
                                <md-button md-no-ink="true" ng-controller="PaginationController" class="md-raised" ng-class="{disabled: noPrevious(), previous: align}" ng-click="selectPage(currentDivdePage - 1, $event)" ng-disabled="noPrevious()"><i ng-hide="loginValue" class="md-icon {{::iconSize || \'dp56\'}}">keyboard_arrow_up</i></md-button> \
                            </md-grid-tile> \
                              \
                            <md-grid-tile> \
                                <md-button md-no-ink="true" ng-controller="PaginationController" class="md-raised" ng-class="{disabled: noNext(), next: align}" ng-click="selectPage(currentDivdePage + 1, $event)" ng-disabled="noNext()"><i ng-hide="loginValue" class="md-icon {{::iconSize || \'dp56\'}}">keyboard_arrow_down</i></md-button> \
                            </md-grid-tile> \
                              \
                        </md-grid-list>-->  \
                          \
                          <div class="md-custom-grid col-3x1 table-list" flex="30" style="height: 30%;"> \
                            <div class="row" ng-repeat="transfer in MainService.Cart.allSplitList | startFrom:(currentDividePage - 1) * divideTablePerPage | limitTo: divideTablePerPage">\
                              <!--ng-show="transfer.status != 4"-->\
                              <div class="col s10-10">\
                                   <!-- food control -->\
                                   <!-- $index is offset by (curretpage - 1) * (number of item per page + billed length) in order to change divided list page correctly  + (currentDividePage-1)*(divideTablePerPage + MainService.Cart.billedSplitList.length) -->\
                                 <button class="btn-table md-button md-grid-button {{uc.statusCode[transfer.status]}}" ng-click="MainService.changeSplitList($index + (currentDividePage-1) * divideTablePerPage , transfer.status)" ng-class="{\'set-food-blue\' : transfer.status < 3, \'normal\' : transfer.status != 4}">\
                                      {{transfer.name}} {{returnStatus(transfer.status)}}\
                                 </button>\
                               </div> \
                            </div>\
                          </div>\
                          <!--MainService.Cart.billedSplitList.length-->\
                        <pager-custom icon-size="\'dp56\'" template="tmpl/pager-default-grid-only.html" total-items="MainService.Cart.allSplitList.length" items-per-page="divideTablePerPage" ng-model="currentDividePage" style="height: 15%;" class="md-custom-grid vertical one-column"></pager-custom> \
                        <md-grid-list md-cols="1" md-row-height="fit" md-gutter="2" style="" flex="35"> \
                            <md-grid-tile> \
                                <md-button md-no-ink="true" class="md-raised cancel" ng-click="MainService.removeSplitList($event)">{{::ui["txt_split_table_delete_last_group"]}}</md-button> \
                            </md-grid-tile> \
                              \
                            <md-grid-tile> \
                                <md-button md-no-ink="true" class="md-raised cancel icon-with-text icon-with-text-half" ng-click="MainService.Cart.cancelOrder2()"><i class="md-icon dp45">clear</i>{{::ui["cancel_back"]}}</md-button> \
                            </md-grid-tile> \
                            <md-grid-tile> \
                                <md-button md-no-ink="true" class="md-raised proceed icon-with-text" ng-click="MainService.submitSplitTable()"><i class="md-icon dp45">done</i>{{::ui["done"]}}</md-button> \
                            </md-grid-tile> \
                        </md-grid-list> \
                    </div> \
                </div> \
              \
            <!-- Bottom options --> \
              \
        </div><!-- divide-menu -->');
    }]);

    angular.module('tmpl/leftFoodMenu.html', []).run(["$templateCache", function ($templateCache) {
        $templateCache.put('tmpl/leftFoodMenu.html',
          '<div flex layout="column" class="sidebar left-sidebar" ng-class="{\'rms-palette-sky\': MainService.Cart.currentSelectedSplitIndex == MainService.Cart.splitListLIndex}"> \
        <!-- table information --> \
          <div layout="row" flex="15" class="header fill-height table-info" ng-click="MainService.splitListClick(MainService.Cart.splitListLIndex)"> \
              <div class="table-summary" layout="column"> \
                  <span class="inline-block table-no">{{MainService.Cart.allSplitList[MainService.Cart.splitListLIndex].name}}</span> \
                  <div flex class="inline-block no-of-people-wrapper"> \
                      <input-keypad-left-noofpeople title="No of People" class="inline-block no-of-people display-mode" maxlength="2"><input type="text" ngminlength="1" ngmaxlength="3" ng-model="keypadInfo.value" ng-click="callKeypad($event, dialogTest1)" custom-always-blur="true" id="input-keypad-left-noofpeople" /></input-keypad-left-noofpeople> \
                      <span class="inline-block unit">{{::ui["txt_split_table_people"]}}</span> \
                  </div> \
                  <!-- this present button area  --> \
              </div> \
              <span flex></span> \
              <div style="position: absolute; top: 6px; right: 6px;" class="table-function" ng-controller="scrollController" layout="row"> \
                  <md-button md-no-ink="true" class="md-40p md-raised md-rounded-square md-densed" ng-click="scrollUp( \'#order-list-left\', 30 )"> \
                      <i class="md-icon dp40">keyboard_arrow_up</i> \
                  </md-button><md-button class="md-40p md-raised md-rounded-square md-densed" ng-click="scrollDown( \'#order-list-left\', 30 )"> \
                      <i class="md-icon dp40">keyboard_arrow_down</i> \
                  </md-button> \
                  <md-button md-no-ink="true" class="show-details md-40p md-raised md-rounded-square md-densed" ng-click="toggleItemDisplay1(\'detail\')" aria-label=""> \
                      <md-icon class="dp30" md-svg-src="assets/icon/identification24.svg"></md-icon> \
                  </md-button>\
                  <md-button md-no-ink="true" class="show-details md-40p md-raised md-rounded-square md-densed" ng-click="toggleItemDisplay2(\'zeroItem\')" aria-label=""><i class="md-icon dp48">exposure_zero</i></md-button> \
                  <!--<md-button md-no-ink="true" class="md-40p md-raised md-rounded-square md-densed" ng-click="bindSomething()"><md-icon class="dp30" md-svg-src="assets/icon/identification24.svg"></md-icon></md-button>--> \
              </div> \
          </div> \
          \
        <!-- scrollable area only food list --> \
        <!-- class="active-item-list" --> \
        <!--  scrollable parameters="{{ {vScroll : true, vScrollbar: true} }}" --> \
          <div flex id="order-list-left" class="order-list-wrapper" ng-click="MainService.splitListClick(MainService.Cart.splitListLIndex)"> \
              <div class="col order-list"> \
                  <ul class="list-group"> \
                      <!-- 套餐 --> \
                      \
                      <!-- disabled -->\
                      <!--  ng-hide="item.getQtyWithVoid() <= 0 || item.getTotalPriceWithVoid() == 0 && item.type != \'T\' && isNotModeItemDisplay2(\'zeroItem\')" -->\
                      <!-- disabled ng-if="item.qty > 0" -->   \
                      \
                      <li class="list-group-item" ng-repeat="item in MainService.Cart.allSplitList[MainService.Cart.splitListLIndex].item track by $index"  ng-click="( (item.qty >= 0) && item.editItem($event,MainService.Cart.splitListLIndex))" ng-class="{active : item.isEdit}"\
                      > \
                        <split-order-list></split-order-list>\
                      </li> \
                  </ul> \
              </div> \
          </div> \
          \
        <!-- total etc --> \
        <div flex="5" class="footer account-summary"> \
            <div layout="row"> \
                <div class="inline-block">{{::ui["subtotal"]}}</div><span flex></span><div class="inline-block">{{ MainService.Cart.splitListLPrice | currency : INFO.currency : 1 }}</div> \
            </div> \
            <div layout="row"> \
                <div class="inline-block">10%服務費</div><span flex></span><div class="inline-block">{{ MainService.Cart.splitListLServiceCharge | currency : INFO.currency : 1 }}</div> \
            </div> \
            \
            <!--<div layout="row"> \
                <div class="inline-block discount">會員折扣</div><span flex></span><div class="inline-block">-$100.00</div> \
            </div> -->\
            \
            <div layout="row"> \
                <div class="inline-block">帳金小數</div><span flex></span><div class="inline-block">{{ MainService.Cart.splitListLRemainings | currency : INFO.currency : 1 }}</div> \
            </div> \
            \
            <div layout="row"> \
                <div class="inline-block">總金額</div><span flex></span><div class="inline-block">{{MainService.Cart.splitListLTotalPrice | currency : INFO.currency : 1 }}</div> \
            </div> \
        </div> \
      </div><div style="margin:0 1px"></div>');
    }]);

    angular.module('tmpl/rightFoodMenu.html', []).run(["$templateCache", function ($templateCache) {
        $templateCache.put('tmpl/rightFoodMenu.html',
          '<div flex layout="column" class="sidebar" ng-class="{\'rms-palette-sky\': MainService.Cart.currentSelectedSplitIndex == MainService.Cart.splitListRIndex}"> \
          <!-- table information --> \
            <div layout="row" flex="15" class="header fill-height table-info" style="" ng-click="MainService.splitListClick(MainService.Cart.splitListRIndex)"> \
                <div layout="column" class="table-summary"> \
                    <span class="inline-block table-no">{{MainService.Cart.allSplitList[MainService.Cart.splitListRIndex].name}}</span> \
                    <div flex class="inline-block no-of-people-wrapper"><input-keypad-right-noofpeople title="No of People" class="inline-block no-of-people display-mode" maxlength="2"><input type="text" ngminlength="1" ngmaxlength="3" ng-model="keypadInfo.value" ng-click="callKeypad($event, dialogTest1)" custom-always-blur="true" /></input-keypad-right-noofpeople><span class="inline-block unit">{{::ui["txt_split_table_people"]}}</span></div> \
                    <!-- this present button area  --> \
                </div> \
                <span flex></span> \
                <div style="position: absolute; top: 6px; right: 6px;" class="table-function" ng-controller="scrollController" layout="row"> \
                    <md-button md-no-ink="true" class="md-40p md-raised md-rounded-square md-densed" ng-click="scrollUp( \'#order-list-right\', 30 )"> \
                        <i class="md-icon dp40">keyboard_arrow_up</i> \
                    </md-button><md-button class="md-40p md-raised md-rounded-square md-densed" ng-click="scrollDown( \'#order-list-right\', 30 )"> \
                        <i class="md-icon dp40">keyboard_arrow_down</i> \
                    </md-button> \
                    <md-button md-no-ink="true" class="show-details md-40p md-raised md-rounded-square md-densed" ng-click="toggleItemDisplay(\'detail\')" aria-label=""> \
                        <md-icon class="dp30" md-svg-src="assets/icon/identification24.svg"></md-icon> \
                    </md-button>\
                    <md-button md-no-ink="true" class="show-details md-40p md-raised md-rounded-square md-densed" ng-click="toggleItemDisplay2(\'zeroItem\')" aria-label=""> \
                        <i class="md-icon dp48">exposure_zero</i> \
                    </md-button> \
                    <!--<md-button md-no-ink="true" class="md-40p md-raised md-rounded-square md-densed" ng-click="MainService.modeToggleOrder($event)" ng-hide="modeCheckOrder(\'foodControl\')"> \
                    <md-icon class="dp30" md-svg-src="assets/icon/identification24.svg"></md-icon> \
                </md-button>--> \
                </div> \
            </div> \
          <!-- scrollable area only food list --> \
            <div flex id="order-list-right" class="order-list-wrapper" md-is-locked-open="$mdMedia(\'min-width: 0px\')" flex="60" md-scroll-y style="" ng-click="MainService.splitListClick(MainService.Cart.splitListRIndex)"> \
                <div class="col order-list" ng-hide="isModeOrder(\'foodControl\')"> \
                    <ul class="list-group"> \
                        <!-- 套餐 --> \
                        \
                        <!-- disabled -->\
                        <!-- ng-hide="item.getQtyWithVoid() <= 0 && isNotModeItemDisplay(\'detail\') && isNotModeItemDisplay1(\'detail\') || item.getTotalPriceWithVoid() == 0 && item.type != \'T\' && isNotModeItemDisplay2(\'zeroItem\')" --> \
                        <!-- disabled ng-if="item.qty > 0" -->\
                        \
                        <li class="list-group-item" ng-repeat="item in MainService.Cart.allSplitList[MainService.Cart.splitListRIndex].item track by $index" ng-click="( (item.qty >= 0) && item.editItem($event,MainService.Cart.splitListRIndex)) " ng-class="{active : item.isEdit}" > \
                         \
                          <split-order-list></split-order-list>\
                        </li> \
                    </ul> \
                </div> \
                <div class="col order-list" ng-show="isModeOrder(\'foodControl\')"> \
                    <ul class="list-group"> \
                        <!-- 套餐 --> \
                        <li class="list-group-item" ng-repeat="item in MainService.stock.list track by $index" ng-hide="item.getQty() <= 0 && isNotModeItemDisplay(\'detail\')" ng-click="item.resetStock($event);" ng-class="{active : item.isEdit}" > \
                        \
                            <p class="list-group-item-heading">{{item.getName()}}</p> \
                        </li> \
                    </ul> \
                </div> \
            </div> \
            \
          <!-- total etc --> \
            <div flex="5" class="footer account-summary" ng-hide="isModeOrder(\'foodControl\')"> \
                <div class="account"> \
                    <div class=""><!--<strong>會員：</strong><span>味王 <span class="card-no">#12345</span></span>--></div> \
                    <div class=""><!--<strong>總積分：</strong><span>375</span>--></div> \
                </div> \
                \
                \
                <div layout="row"> \
                    <div class="inline-block">{{::ui["subtotal"]}}</div><span flex></span><div class="inline-block">{{ MainService.Cart.splitListRPrice | currency : INFO.currency : 1 }}</div> \
                </div> \
                <div layout="row"> \
                    <div class="inline-block">10%服務費</div><span flex></span><div class="inline-block">{{ MainService.Cart.splitListRServiceCharge | currency : INFO.currency : 1 }}</div> \
                </div> \
                \
                <!--<div layout="row"> \
                    <div class="inline-block discount">會員折扣</div><span flex></span><div class="inline-block">-$100.00</div> \
                </div> -->\
                \
                            <div layout="row"> \
                    <div class="inline-block">帳金小數</div><span flex></span><div class="inline-block">{{ MainService.Cart.splitListRRemainings | currency : INFO.currency : 1 }}</div> \
                </div> \
                \
                <div layout="row"> \
                    <div class="inline-block">總金額</div><span flex></span><div class="inline-block">{{ MainService.Cart.splitListRTotalPrice | currency : INFO.currency : 1 }}</div> \
                </div> \
            </div> \
        </div>');
    }]);

    angular.module('tmpl/split-order-list.html', []).run(["$templateCache", function ($templateCache) {
        $templateCache.put('tmpl/split-order-list.html',
          '<!-- one order# per order --> \
          <div layout="row" class="order-list-item"> \
                                <div flex="35">\
                                  <span class="index" ng-show="isModeItemDisplay1(\'detail\')">#{{item.index}} </span>\
                                  <span ng-if="item.voidIndex != -1">^{{item.voidIndex}}</span> \
                                  <span >(+){{item.orgIndex}}</span> \
                                  <span flex class="code" ng-show="isModeItemDisplay1(\'detail\')">{{item.code}}</span> \
                                </div>\
                            </div> \
          \
                            <ul class="food-info-container no-padding"> \
                                <!-- ng-repeat should go here for more than 1 food --> \
                                <li layout="row" class="food-info"> \
                                    <!-- for 1024 space, flex set to 55, overflow hidden, add support to responsive later --> \
                                    <div flex="50" layout="column"> \
                                        <span class="name"><a href="#">{{item.getName()}}</a></span> \
                                        <ul class="modifier" ng-show="item.voidRemark != \'\'"> \
                                            <!-- ng-repeat should go here for more than 1 modifier --> \
                                            <li>{{item.voidRemark}}</li> \
                                        </ul> \
                      \
                      <ul class="modifier attention child-list" ng-show="item.modifier.length != 0">\
                          <!-- ng-repeat should go here for more than 1 modifier -->\
                          <li ng-repeat="m in item.modifier">{{m.label}}</li>\
                      </ul>\
                      \
                      <ul class="kitchenMsg warn child-list" ng-show="item.kitchenMsgOld.length != 0">\
                          <!-- ng-repeat should go here for more than 1 modifier -->\
                          <li ng-repeat="k in item.kitchenMsgOld track by $index">{{k.label}}</li>\
                      </ul>\
                      \
                                    </div> \
                  \
                                    <div flex layout="column"> \
                                        <span class="qty">{{ item.getQtyWithVoid() }}</span> \
                                    </div> \
                                    <div flex="35" layout="column"> \
                                        <span class="price">{{ item.getTotalPriceWithVoid() | currency : INFO.currency : 1 }}</span> \
                                    </div> \
                                </li> \
            <ul class="split__list__left__food_info-option-list">\
              <li ng-repeat="optionList in item.option track by $index"> \
                <ul class="food_info-option_list-option sub-child-list">\
                                    <li layout="row" ng-repeat="option in optionList.items track by $index" class="food-info" ng-if="option.qty > 0"> \
                                        <div flex="50" layout="column"> \
                                            <span class="name"><a href="#">{{ option.getName() }}</a></span> \
                                              \
                          <ul class="option-modifier modifier attention child-list" ng-show="option.modifier.length != 0">\
                              <!-- ng-repeat should go here for more than 1 modifier -->\
                              <li ng-repeat="m in option.modifier">{{m.label}}</li>\
                          </ul>\
                          \
                          <ul class="option-kitchenMsg warn child-list" ng-show="option.kitchenMsgOld.length != 0">\
                              <!-- ng-repeat should go here for more than 1 modifier -->\
                              <li ng-repeat="k in option.kitchenMsgOld">{{k.label}}</li>\
                          </ul>\
                          \
                          <ul class="option-kitchenMsg warn child-list" ng-show="option.kitchenMsg.length != 0">\
                              <!-- ng-repeat should go here for more than 1 modifier -->\
                              <li ng-repeat="k in option.kitchenMsg">{{k.label}}</li>\
                          </ul>\
                                        </div> \
                                        <div flex layout="column"> \
                                            <span class="qty">{{ option.qty }}</span> \
                                        </div> \
                                        <div flex="35" layout="column"> \
                                            <span class="price"></span> \
                                        </div> \
                                    </li> \
                            </ul> \
                        </li> \
                    </ul> \
                \
            <ul class="split__list__left__food_info-subitem-list child-list">\
              <li layout="row" ng-repeat="subitem in item.subitem track by $index" class="food-info" ng-if="subitem.qty > 0" ng-class="{active : subitem.isEdit}">\
                  <div flex="50" layout="column">\
                      <span class="name 23456"><a href="#" ng-click="MainService.Cart.cartListEditSubitem($event, subitem.getIndex(), item, item.subitem, subitem);">{{subitem.getName()}}</a></span>\
                \
                      <ul class="subitem-modifier modifier attention child-list" ng-show="subitem.modifier.length != 0">\
                          <!-- ng-repeat should go here for more than 1 modifier -->\
                          <li ng-repeat="m in subitem.modifier">{{m.label}}</li>\
                      </ul>\
                \
                      <ul class="subitem-kitchenMsg warn child-list" ng-show="subitem.kitchenMsgOld.length != 0">\
                          <li ng-repeat="sk in subitem.kitchenMsgOld track by $index">{{sk.label}}</li>\
                      </ul>\
                \
                      <ul class="subitem-kitchenMsg warn child-list" ng-show="subitem.kitchenMsg.length != 0">\
                          <li ng-repeat="sk in subitem.kitchenMsg track by $index">{{sk.label}}</li>\
                      </ul>\
                </div> \
                  <div flex layout="column">\
                      <span class="qty">{{subitem.qty}}</span>\
                </div> \
                  <div flex="35" layout="column">\
                      <span class="price"></span>\
            </div> \
              </li>\
            </ul>\
          </ul> \
          <ul class="details no-padding" layout="column" style="" ng-show="isModeItemDisplay1(\'detail\')"> \
              <!--<li class="points" layout="row"> \
             <span flex="50"></span>積分<span flex>4</span> \
              </li>--> \
              <li class="order-trace"> \
                  <span class="employee">{{item.staff}}</span>@<span class="time">{{item.time}}</span> \
              </li> \
          </ul>');
    }]);

    angular.module('tmpl/cashManagementScene.html', []).run(["$templateCache", function ($templateCache) {
        $templateCache.put('tmpl/cashManagementScene.html',
          '<div id="cash-manage-menu" class="menu-area" layout="row"> \
          <!-- sidebar column --> \
          \
          <div flex id="cash-manage-option" flex class="left-col md-grid-container"> \
              <h2 class="header left-edge-margin fill-height ng-binding" style="">{{::ui[\'manage_option\']}}</h2> \
              <div class="materialize-keypad col-1x5" style="height: 13%"> \
                <div class="row" style=""> \
                  <!-- <div class="col s3 dummy-grid"><span></span></div> --> \
                  <div class="col s10-4"><button class="important btn md-button md-grid-button" ng-click="switchMode2({modeCash:\'open\'});init();">{{::ui[\'starting_amount\']}}</button></div> \
                  <div class="col s10-4"><button class="important btn md-button md-grid-button" ng-click="switchMode2({modeCash:\'petty\'});init();">{{::ui[\'petty_cash\']}}</button></div> \
                  <div class="col s10-2"><button class="proceed btn md-button md-grid-button no-login" ng-click="close();">{{::ui[\'close\']}}</button></div> \
                </div><!-- row --> \
              </div> \
              <md-input-container class="petty-input" ng-hide="isNotModeCash(\'petty\')" style="padding-bottom:2px;height:14%" > \
                  <label>{{::ui[\'remark\']}}</label> \
                  <input name="pettyCashRemark" class="" ng-model="pettyCashRemark" ng-trim="false" type="text" custom-always-focus="true"> \
              </md-input-container> \
              <md-input-container ng-hide="isModeCash(\'normal\')" ng-class="{\'petty-input\': isModeCash(\'petty\'), \'open-input\': isModeCash(\'open\')}" style="height:14%"> \
                  <label>{{::ui[\'please_input\'] + ui[\'lang_space\'] + ui[\'total_cash\']}}</label> \
                  <input name="cashAmount" class="" ng-model="cashAmount" ng-trim="false" type="text" disable> \
              </md-input-container> \
              <div style="height: 53%;"> \
                <div class="materialize-keypad col-5x5" style="" ng-hide="modeChecking([{modeCash: [\'normal\']}])"> \
                  <div class="row"> \
                    <div class="col s4"><button class="btn md-button md-grid-button" ng-click="inputFree(\'1\')">1</button></div> \
                    <div class="col s4"><button class="btn md-button md-grid-button" ng-click="inputFree(2)">2</button></div> \
                    <div class="col s4"><button class="btn md-button md-grid-button" ng-click="inputFree(3)">3</button></div> \
                  </div> \
                  <div class="row"> \
                    <div class="col s4"><button class="btn md-button md-grid-button" ng-click="inputFree(4)">4</button></div> \
                    <div class="col s4"><button class="btn md-button md-grid-button" ng-click="inputFree(5)">5</button></div> \
                    <div class="col s4"><button class="btn md-button md-grid-button" ng-click="inputFree(6)">6</button></div> \
                  </div> \
                  <div class="row"> \
                    <div class="col s4"><button class="btn md-button md-grid-button" ng-click="inputFree(7)">7</button></div> \
                    <div class="col s4"><button class="btn md-button md-grid-button" ng-click="inputFree(8)">8</button></div> \
                    <div class="col s4"><button class="btn md-button md-grid-button" ng-click="inputFree(9)">9</button></div> \
                  </div> \
                  <div class="row"> \
                    <div class="col s4"><button class="btn md-button md-grid-button" ng-click="inputFree(\'.\')">.</button></div> \
                    <div class="col s4"><button class="btn md-button md-grid-button" ng-click="inputFree(0)">0</button></div> \
                    <div class="col s4"><button class="btn md-button md-grid-button" ng-click="inputFree(\'00\')">00</button></div> \
                  </div> \
                  <div class="row"> \
                    <div class="col s4"><button class="btn md-button md-grid-button" ng-click="backspace()"><i class="md-icon dp40">backspace</i></button></div> \
                    <div class="col s4"><button class="cancel btn md-button md-grid-button" ng-click="cancel()"><i class="md-icon dp40" aria-hidden="false" style="">clear</i></button></div> \
                    <div class="col s4"><button class="proceed2 btn md-button md-grid-button" ng-click="setAmount()"><i class="md-icon dp45">done</i></button></div> \
                  </div> \
                </div> \
              </div> \
          </div> \
          \
          <div flex="60" class="right-col md-grid-container" layout="column" style="height: 100%;"> \
            <h2 class="header fill-height ng-binding" style="">{{::ui[\'manage_option\']}}</h2> \
            \
            <div flex="" class="report-content" ng-if="isModeCash(\'normal\')"> \
              {{::ui[\'please_select_item_to_manage\']}}\
            </div> \
            \
            <div flex="" class="report-content" ng-show="isModeCash(\'open\')"> \
              <ng-include include-replace src="\'tmpl/cash_management_template_open.html\'" ng-if="reportData != null"></ng-include> \
            </div> \
            \
            <!--  flex="80" style="max-height: 74%;" -->\
            <div class="report-content" ng-show="isModeCash(\'petty\')" > \
              <ng-include include-replace src="\'tmpl/cash_management_template_open.html\'" ng-if="reportData != null"></ng-include> \
            </div> \
            \
            <!--<div flex style="height: 20%;" ng-show="isModeCash(\'petty\')"> \
              <div class="petty-list materialize-keypad col-2x5" style=""> \
                <div class="row"> \
                   <div class="col s10-2" ng-repeat="pettyReport in MainService.pettyReport | startFrom:(MainService.currentPettyPage - 1) * MainService.pettyPerPage | limitTo: MainService.pettyPerPage"><button class="btn md-button md-grid-button" ng-click="doSomething()">{{::pettyReport.name}}</button></div> \
                   \
                  <div class="col s10-2 dummy-grid" ng-if="MainService.noOfPettyPage - MainService.currentPettyPage == 0" ng-repeat="i in MainService.getPageNumber( MainService.pettyPerPage - MainService.pettyReport.length % MainService.pettyPerPage ) track by $index"><span></span></div> \
                  <pager-custom icon-size="\'dp56\'" template="tmpl/pager-default-materialize.html" total-items="MainService.pettyReport.length" items-per-page="MainService.pettyPerPage" ng-model="currentPettyPage"></pager-custom> \
                </div> \
              </div> \
            </div>--> \
          </div> \
          \
      </div><!-- cash-management-menu -->');
    }]);

    // including the following templates: options, gridMenuOption-order, cancelOptions, modifier
    angular.module('tmpl/order.html', []).run(["$templateCache", "$templateRequest", function ($templateCache, $templateRequest) {
        $templateRequest('tmpl/order.html').then(function (response) {
            $templateCache.put('tmpl/order.html', response);
        });
    }]);



    angular.module('tmpl/modifier.html', []).run(["$templateCache", function ($templateCache) {
        $templateCache.put('tmpl/modifier.html',
          '<div id="modifier-menu" class="menu-area" flex layout="column" ng-controller="modifierCtrl as modifier" style="" ng-show="modeCheckOrderItem(\'modifier\')"> \
          <!-- option set menu, full page --> \
          <!--<div id="option" class="food__menu-option content md-custom-grid col-5x5" style="height: 100%; " ng-controller="keypadControllerOpenItem"> \
            <div class="row" style="position: relative;"> \
               <div class="col s10-2" ng-repeat="item in MainService.Cart.currentOptionList.items | startFrom:(currentPage - 1) * itemsPerPage | limitTo: itemsPerPage">\
                  <button class="md-raised md-button md-grid-button {{item.color}}" ng-click="MainService.Cart.addOption(item, 1)" ng-class="{suspend: item.stock != undefined && (item.stock < 0 || item.stock === 0)}">\
                       <span class="flex-center badge suspend" ng-if="item.stock != undefined && (item.stock === -3)">{{::ui["txt_preparing"]}}</span> \
                       <span class="flex-center badge suspend" ng-if="item.stock != undefined && (item.stock === -1 || item.stock === 0)">{{::ui["txt_suspend"]}}</span> \
                       <div class="name">{{::MainService.fn.shorten(item.name[INFO.lang])}}</div> \
                     \
                       <span class="badge stock" ng-if="item.stock != undefined && item.stock > 0">Q: {{item.stock}}</span> \
                            <small class="secondary"> \
                                 <span class="code" ng-show="modeCheckOrderItemDetail()">{{::item.code}}</span> \
                                 <span class="code" ng-show="modeCheckOrderItemDetail(\'printer\')">{{::item.printer}}</span> \
                             </small> \
                       <div class="price">{{ item.getUnitPrice() }}</div> \
                  </button>\
                </div> \
                \
               <div class="col s10-2 dummy-grid" ng-if="noOfPage( MainService.Cart.currentOptionList.items.length, itemsPerPage ) == currentPage" ng-repeat="i in remainderPageArr( MainService.Cart.currentOptionList.items.length, itemsPerPage ) track by $index"><span></span></div> \
              <pager-custom icon-size="\'dp70\'" template="tmpl/pager-default-materialize.html" total-items="MainService.Cart.currentOptionList.items.length" items-per-page="itemsPerPage" ng-model="currentPage"></pager-custom> \
            </div> \
          </div>--> \
          <div class="md-custom-grid md-grid-container" style="height: calc(25% - 2px); display:none" layout="column" > \
              <md-grid-list md-cols="6" md-row-height="fit" md-gutter="1" flex="100"> \
                  <md-grid-tile on-finish-render ng-repeat="option in ::modifier.modifierOption"> \
                      <md-button md-no-ink="true" class="md-raised button-dp40" ng-if="option.element == \'input\'"><input-keypad-modifier title="Number of Item" class="full-box inline-block display-mode 2345" maxlength="3"><input type="text" ngminlength="1" ngmaxlength="3" ng-model="keypadInfo.value" ng-click="callKeypad($event, dialogTest1)" custom-always-blur="true" /></input-keypad-modifier><i class="md-icon dp80 md-watermark">keyboard</i></md-button> \
                      <md-button md-no-ink="true" class="md-raised button-dp40" ng-if="option.element == \'text\'" ng-click="MainService.Cart.changeItemQty(option.name)">{{::option.name}}</md-button> \
                  </md-grid-tile> \
                  <md-grid-tile class="dummy-grid" ng-repeat="i in ::numberToArray( 2 ) track by $index"><span><span></md-grid-tile> \
              </md-grid-list> \
          </div> \
           <!--this is modifierGroup--> \
            <div class="modifier-option md-custom-grid col-2x5" style="height: 25%;" ng-controller="modifierGroupCtrl" ng-init="init();">\
                 <div class="row">\
                    <div class="col s10-2" ng-repeat="item in modifierGroup | startFrom:(mg_currentPage - 1) * mg_optionPerPage | limitTo: mg_optionPerPage" >\
                        <button class="btn md-raised md-button md-grid-button " ng-click="modifierGroupClick(item)" style="background-color:#333;color:white;">\
                       <div class="name">{{::item.modifierGroupName}}</div> \
                     \
                            <small class="secondary"> \
                                 <span class="code" ng-show="modeCheckOrderItemDetail()">{{::item.modifierGroupCode}}</span> \
                                 <span class="code" ng-show="modeCheckOrderItemDetail(\'printer\')">{{::item.printer}}</span> \
                             </small> \
                       <div class="price" style="background-color:rgb(161,136,127)"></div> \
                  </button>\
                    </div>\
                    <div class="col s10-2 dummy-grid" ng-if="noOfModifierGroupPage == mg_currentPage && modifierGroup.length % mg_optionPerPage > 0" ng-repeat="i in MainService.getPageNumber( mg_optionPerPage - modifierGroup.length % mg_optionPerPage ) track by $index"><span></span></div> \
                <pager-custom icon-size="\'dp56\'" template="tmpl/pager-default-materialize.html" total-items="modifierGroup.length" items-per-page="mg_optionPerPage" ng-model="mg_currentPage"></pager-custom> \
                 </div>\
            </div>\
          <!--this is modifier -->\
          <div class="modifier-option md-custom-grid col-3x5" style="height: 65%;" > \
              <div class="row"> \
                <div class="col s10-2" ng-repeat="item in MainService.food.modifiers | startFrom:(currentPage - 1) * optionPerPage | limitTo: optionPerPage">\
                  <!--<button class="md-raised md-button md-grid-button {{::category.color}}" ng-click="MainService.Cart.addModifier(item)">{{::item.label}}</button></div>--> \
                   <button class="md-raised md-button md-grid-button {{item.color}}" ng-click="MainService.Cart.addModifier(item)" ng-class="{suspend: item.stock != undefined && (item.stock < 0 || item.stock === 0)}">\
                       <span class="flex-center badge suspend" ng-if="item.stock != undefined && (item.stock === -3)">{{::ui["txt_preparing"]}}</span> \
                       <span class="flex-center badge suspend" ng-if="item.stock != undefined && (item.stock === -1 || item.stock === 0)">{{::ui["txt_suspend"]}}</span> \
                       <div class="name">{{::MainService.fn.shorten(item.name[INFO.lang])}}</div> \
                     \
                       <span class="badge stock" ng-if="item.stock != undefined && item.stock > 0">Q: {{item.stock}}</span> \
                            <small class="secondary"> \
                                 <span class="code" ng-show="modeCheckOrderItemDetail()">{{::item.code}}</span> \
                                 <span class="code" ng-show="modeCheckOrderItemDetail(\'printer\')">{{::item.printer}}</span> \
                             </small> \
                       <div class="price">{{ item.getUnitPrice() }}</div> \
                  </button>\
                 </div>\
                <div class="col s10-2 dummy-grid" ng-if="MainService.noOfModifierPage == currentPage && MainService.food.modifiers.length % optionPerPage > 0" ng-repeat="i in MainService.getPageNumber( optionPerPage - MainService.food.modifiers.length % optionPerPage ) track by $index"><span></span></div> \
                <pager-custom icon-size="\'dp56\'" template="tmpl/pager-default-materialize.html" total-items="MainService.food.modifiers.length" items-per-page="optionPerPage" ng-model="currentPage"></pager-custom> \
              </div> \
          </div> \
         <!--end of modifier -->\
          \
          <div class="modifier-option md-custom-grid col-1x5" style="height: 10%"> \
              <div class="row no-padding-bottom"> \
                <div class="col s10-2 dummy-grid"><span></span></div> \
                <div class="col s10-2 dummy-grid"><span></span></div> \
                <div class="col s10-2 dummy-grid"><span></span></div> \
                <div class="col s10-2 dummy-grid"><span></span></div> \
                <!-- <div class="col s10-2"><button class="md-button md-grid-button" ng-click="MainService.cancelModifier()">{{::ui["food_modifier"]}}</button></div> \
                <div class="col s10-2"><button class="md-button md-grid-button" ng-click="MainService.cancelModifier()">{{::ui["drink_modifier"]}}</button></div> \
                <div class="col s10-2"><button class="md-button md-grid-button" ng-click="MainService.cancelModifier()">{{::ui["special_message"]}}</button></div> --> \
                <div class="col s10-2"><button class="cancel icon-with-text btn md-button md-button md-grid-button" ng-click="MainService.cancelModifier({modeOrder: true, modeItem: true, modeBase: false})"><i class="md-icon dp45">clear</i><span class="">{{::ui["cancel_back"] || "取消/返回"}}</span></button></div> \
              </div> \
          </div> \
          \
      </div>');
    }]);

    angular.module('tmpl/gridMenuOption-order.html', []).run(["$templateCache", "$templateRequest", function ($templateCache, $templateRequest) {
        $templateRequest('tmpl/gridMenuOption-order.html').then(function (response) {
            $templateCache.put('tmpl/gridMenuOption-order.html', response);
        });
    }]);

    angular.module('tmpl/options.html', []).run(["$templateCache", function ($templateCache) {
        $templateCache.put('tmpl/options.html',
          '<div id="option-menu" class="menu-area" flex layout="column"> \
          <!-- option set menu, full page --> \
          <div id="option" class="food__menu-option content md-custom-grid col-5x5" style="height: 100%; " ng-controller="keypadControllerOpenItem"> \
            <div class="row" style="position: relative;"> \
               <div class="col s10-2" ng-repeat="item in MainService.Cart.currentOptionList.items | startFrom:(currentPage - 1) * itemsPerPage | limitTo: itemsPerPage">\
                    <!-- food control -->\
                  <button class="md-raised md-button md-grid-button {{item.color}}" ng-click="MainService.Cart.addOption(item, 1)" ng-class="{suspend: item.stock != undefined && (item.stock < 0 || item.stock === 0)}">\
                       <span class="flex-center badge suspend" ng-if="item.stock != undefined && (item.stock === -3)">{{::ui["txt_preparing"]}}</span> \
                       <span class="flex-center badge suspend" ng-if="item.stock != undefined && (item.stock === -1 || item.stock === 0)">{{::ui["txt_suspend"]}}</span> \
                       <div class="name">{{::MainService.fn.shorten(item.name[INFO.lang])}}</div> \
                     \
                       <span class="badge stock" ng-if="item.stock != undefined && item.stock > 0">Q: {{item.stock}}</span> \
                            <small class="secondary"> \
                                 <span class="code" ng-show="modeCheckOrderItemDetail()">{{::item.code}}</span> \
                                 <span class="code" ng-show="modeCheckOrderItemDetail(\'printer\')">{{::item.printer}}</span> \
                                 <!--<span class="badge stock">15</span>--> \
                             </small> \
                       <div class="price">{{ item.getUnitPrice() | currency : INFO.currency : 1}}</div> \
                  </button>\
                </div> \
                \
               <div class="col s10-2 dummy-grid" ng-if="noOfPage( MainService.Cart.currentOptionList.items.length, itemsPerPage ) == currentPage" ng-repeat="i in remainderPageArr( MainService.Cart.currentOptionList.items.length, itemsPerPage ) track by $index"><span></span></div> \
              <pager-custom icon-size="\'dp70\'" template="tmpl/pager-default-materialize.html" total-items="MainService.Cart.currentOptionList.items.length" items-per-page="itemsPerPage" ng-model="currentPage"></pager-custom> \
            </div> \
          </div> \
      </div><!-- option-menu -->');
    }]);

    angular.module('tmpl/optionsFoodControl.html', []).run(["$templateCache", function ($templateCache) {
        $templateCache.put('tmpl/optionsFoodControl.html',
          '<div id="option-control-menu" class="menu-area" flex layout="column" style="" style="height: 100%"> \
          <!-- option set menu, full page --> \
          <div id="option-control" class="food__menu-option content md-custom-grid col-5x5" style="height: 85%; " ng-controller="keypadControllerOpenItem"> \
            <div class="row" style="position: relative;"> \
               <div class="col s10-2" ng-repeat="item in MainService.Cart.optionControlList | startFrom:(currentPage - 1) * itemsPerPage | limitTo: itemsPerPage">\
                    <!-- food control -->\
                  <button class="md-raised md-button md-grid-button" ng-click="callKeypadFree($event, dialogTest1,item)" ng-class="{suspend: item.stock != undefined && (item.stock < 0 || item.stock === 0)}">\
                       <span class="flex-center badge suspend" ng-if="item.stock != undefined && (item.stock === -3)" style="font-size:24px;">{{::ui["txt_preparing"]}}</span> \
                       <span class="flex-center badge suspend" ng-if="item.stock != undefined && (item.stock === -1 || item.stock === 0)" style="font-size:24px;">{{::ui["txt_suspend"]}}</span> \
                       <div class="name">{{::MainService.fn.shorten(item.name[INFO.lang])}}</div> \
                     \
                       <span class="badge stock" ng-if="item.stock != undefined && item.stock > 0">Q: {{item.stock}}</span> \
                       <div class="price">{{ item.getUnitPrice() }}</div> \
                  </button>\
                </div> \
                \
               <div class="col s10-2 dummy-grid" ng-if="noOfPage( MainService.Cart.optionControlList.length, itemsPerPage ) == currentPage" ng-repeat="i in remainderPageArr( MainService.Cart.optionControlList.length, itemsPerPage ) track by $index"><span></span></div> \
              <pager-custom icon-size="\'dp70\'" template="tmpl/pager-default-materialize.html" total-items="MainService.Cart.optionControlList.length" items-per-page="itemsPerPage" ng-model="currentPage"></pager-custom> \
            </div> \
              <!-- <div layout="column" style="position: relative; height: 85%">\
                <md-grid-list flex md-cols="5" md-row-height="fit" md-gutter="1" style="height: 100%"> \
                    <md-grid-tile ng-repeat="option in MainService.Cart.optionControlList | startFrom:(currentPage - 1) * itemsPerPage | limitTo: itemsPerPage" ng-click="callKeypadFree($event, dialogTest1,option)"> \
                        <md-button md-no-ink="true" class="md-raised">{{option.name[INFO.lang]}}<br />{{option.getUnitPrice()}}</md-button> \
                    </md-grid-tile> \
                </md-grid-list> \
                <div class="row option__control__menu-row" style="position:absolute; right: 0; bottom: 0; width: calc(40% - 1px); height: 20%; margin: 0;">\
                  <pager-custom icon-size="\'dp56\'" template="tmpl/pager-default-materialize.html" total-items="totalItems" items-per-page="itemsPerPage" ng-model="currentPage"></pager-custom> \
                </div>\
              </div> -->\
                   \
                   <!-- width: calc((20% - 0.8px)*2 + 1px);-->\
                \
          </div> \
          <div class="materialize-keypad col-1x5" style="height: 15%"> \
            <div class="row" style="margin-bottom: 0;"> \
              <div class="col s10-2 dummy-grid"><span></span></div> \
              <div class="col s10-2 dummy-grid"><span></span></div> \
              <div class="col s10-2 dummy-grid"><span></span></div> \
              <div class="col s10-2 dummy-grid"><span></span></div> \
              <div class="col s10-2"><button class="proceed btn md-button md-grid-button" ng-click="returnToOrder();">{{::ui[\'done\']}}</button></div> \
            </div><!-- row --> \
          </div> \
      </div><!-- option-control-menu -->');
    }]);

    angular.module('tmpl/cancelOptions.html', []).run(["$templateCache", function ($templateCache) {
        $templateCache.put('tmpl/cancelOptions.html',
          '<div id="cancel-option-menu" class="menu-area" flex layout="column" ng-controller="cancelController as cancel" style="" ng-show="modeCheckOrderItem(\'cancel\')"> \
          <!-- option set menu, full page --> \
          <div class="cancel-option md-grid-container" style="height: 25%" layout="column"> \
              <md-grid-list md-cols="6" md-row-height="fit" md-gutter="1" style="" flex="100"> \
                  <!-- ng-if="category.items.length > 0" --> \
                  <md-grid-tile on-finish-render ng-repeat="option in cancel.cancelOption"> \
                      <md-button md-no-ink="true" class="md-raised button-dp40" ng-if="option.element == \'input\'"><input-keypad-cancel-item title="no_of_food_cancel" class="full-box inline-block display-mode" maxlength="3"><input type="text" ngminlength="1" ngmaxlength="3" ng-model="keypadInfo.value" ng-click="callKeypad($event, dialogTest1)" custom-always-blur="true" /></input-keypad-cancel-item><i class="md-icon dp80 md-watermark">keyboard</i></md-button> \
                      <md-button md-no-ink="true" class="md-raised button-dp40" ng-if="option.element == \'text\'" ng-click="MainService.Cart.voidItemCount = option.name">{{::option.name}}</md-button> \
                  </md-grid-tile> \
                  <md-grid-tile class="dummy-grid"><span><span></md-grid-tile> \
              </md-grid-list> \
            \
          </div> \
            \
          <div class="cancel-reason md-custom-grid col-3x5" style="height: 59%"> \
            <div class="row"> \
              <div class="col s10-2" style="margin-bottom: 0;" ng-repeat="option in INFO.cancelRemark | startFrom:(currentPage - 1) * MainService.cancelOptionPerPage | limitTo: MainService.cancelOptionPerPage"><button class="md-raised md-button md-grid-button" ng-click="MainService.Cart.voidItem(\'{{::option}}\')">{{::option}}</button></div> \
               \
              <div class="col s10-2 dummy-grid" ng-if="MainService.noOfCancelOptionPage == currentPage && MainService.cancelOptionTotalItems % MainService.cancelOptionPerPage > 0" ng-repeat="i in MainService.getPageNumber( MainService.cancelOptionPerPage - MainService.cancelOptionTotalItems % MainService.cancelOptionPerPage ) track by $index"><span></span></div> \
              <pager-custom icon-size="\'dp56\'" template="tmpl/pager-default-materialize.html" total-items="MainService.cancelOptionTotalItems" items-per-page="MainService.cancelOptionPerPage" ng-model="currentPage"></pager-custom> \
            </div> \
          </div> \
            \
          <grid-menu-option class="cancel_reason-menu-option menu-option md-grid-container" grid-height="\'fit\'" grid-col="5" template="tmpl/gridMenuOption-cancel.html" flex="100" layout="column" style="height: 16%;"></grid-menu-option> \
      </div>');
    }]);

    angular.module('tmpl/billOption.html', []).run(["$templateCache", function ($templateCache) {
        $templateCache.put('tmpl/billOption.html',
          '<div id="table-list-box" class=""> \
          <md-dialog aria-label="Table List" class="" ng-app> \
            <md-toolbar> \
               <div class="md-toolbar-tools"> \
                 <h2>{{::ui["please_select_bill"] || "請選擇賬單"}}</h2> \
                 <span flex></span> \
                 <md-button md-no-ink="true" class="md-icon-button" ng-click="closeDialog();"> \
                   <i class="md-icon dp45">clear</i> \
                 </md-button> \
               </div> \
             </md-toolbar> \
              <md-dialog-content> \
                  <div class="table-list" layout="row" style="margin: auto;"> \
                    <div  style="max-width: 250px;">\
                        <md-button md-no-ink="true" class="btn-table md-mini md-raised md-button-text md-button-dp16 md-button md-default-theme" aria-label="{{o.header.tableNum}}賬單" title="" ng-click="assignTableNo(o.header.tableNum, o.header.status, $event)" ng-repeat="o in order" ng-class="{true:\'lock\', false:[uc.statusCode[o.header.status]]}[lockStatus(o)]"><span>{{o.header.tableNum}} {{returnStatus(o.header.status)}}</span></md-button> \
                    </div> \
                  </div> \
              </md-dialog-content> \
          </md-dialog> \
      </div>');
    }]);

    // for multiple type of food such as single item, or a set item
    angular.module('tmpl/foodControlOption.html', []).run(["$templateCache", function ($templateCache) {
        $templateCache.put('tmpl/foodControlOption.html',
          '<div id="bill-option-box" class=""> \
          <md-dialog aria-label="Bill Option" class="" ng-app> \
            <md-toolbar> \
               <div class="md-toolbar-tools"> \
                 <h2>{{::ui["please_select_item"] || "請選擇項目"}}</h2> \
                 <span flex></span> \
                 <md-button md-no-ink="true" class="md-icon-button" ng-click="closeDialog();"> \
                   <i class="md-icon dp45">clear</i> \
                 </md-button> \
               </div> \
             </md-toolbar> \
              <md-dialog-content> \
                  <div class="bill-option" layout="row" style="margin: auto;"> \
                    <div  style="max-width: 250px;"> \
                        <md-button md-no-ink="true" class="md-accent md-hue-3 md-mini md-raised md-button-text md-button-dp16 md-button md-default-theme" aria-label="" title="" ng-click="control(\'this\')"><span>{{::ui["control_this_item"]}}</span></md-button> \
                        <md-button md-no-ink="true" class="md-accent md-hue-3 md-mini md-raised md-button-text md-button-dp16 md-button md-default-theme" aria-label="" title="" ng-click="control(\'child\', dialogTest1, item)"><span>{{::ui["select_subitem"]}}</span></md-button> \
                    </div> \
                  </div> \
              </md-dialog-content> \
          </md-dialog> \
      </div>');
    }]);

    angular.module('tmpl/finish.html', []).run(["$templateCache", function ($templateCache) {
        $templateCache.put('tmpl/finish.html',
          '<div class="finish md-grid-container" style=""> \
        <md-grid-list md-cols="{{::gridCol}}" md-row-height="{{::gridHeight}}" md-gutter="1" style=""> \
        <!-- ng-if="category.items.length > 0" --> \
         \
            <md-grid-tile ng-if="back"> \
              <md-button md-no-ink="true" class="md-raised"><i class="md-icon dp45">keyboard_return</i></md-button> \
            </md-grid-tile> \
            <md-grid-tile ng-if="cancel"> \
                <md-button md-no-ink="true" class="md-raised cancel"><i class="md-icon dp45">clear</i></md-button> \
            </md-grid-tile> \
            <md-grid-tile ng-if="done"> \
                <md-button md-no-ink="true" class="md-raised proceed"><i class="md-icon dp45">done</i></md-button> \
            </md-grid-tile> \
        </md-grid-list> \
      </div>');
    }]);

    angular.module('tmpl/member.html', []).run(["$templateCache", function ($templateCache) {
        $templateCache.put('tmpl/member.html',
          '<div id="member-box" class="keypad-container"> \
          <md-dialog aria-label="Member" class="keypad" ng-app> \
            <md-toolbar> \
               <div class="md-toolbar-tools"> \
                 <h2>{{::ui["search_member"] || "查詢會員"}}</h2> \
                 <span flex></span> \
               </div> \
             </md-toolbar> \
              <md-dialog-content class="sticky-container member-html"> \
                  <div class="member-info" layout="row" style="margin: auto;"> \
                    <div  style="max-width: 250px;"> \
                      <md-input-container class="login"> \
                        <label>{{ui["member_no"]}} / {{ui["member_phone"]}}</label> \
                        <!-- ng-always-focus="true", will make it look buggy --> \
                          <input class="dialog-close" login-input login-input-enter="next()" ng-model="loginValue" ng-trim="false" ng-change="change()" ng-focus="true" type="text" custom-always-focus="true"> \
                      </md-input-container> \
                      \
                      <md-grid-list md-cols="6" md-row-height="1:1" md-gutter="1px" style="" class="keypad-grid"> \
                          <md-grid-tile ng-repeat="element in keypad" md-colspan="2" md-rowspan="2"> \
                           <md-button md-no-ink="true" class="md-raised" ng-click="input(element.text)" ng-if="!element.useIcon"> \
                             {{::element.text}} \
                           </md-button> \
                      \
                           <md-button md-no-ink="true" class="md-raised" ng-click="reset()" ng-if="element.useIcon && element.text==\'reset\'"> \
                             <i ng-show="loginValue" class="md-icon dp40">replay</i> \
                             <i ng-hide="loginValue" class="md-icon dp40">clear</i> \
                           </md-button> \
                      \
                           <md-button md-no-ink="true" class="md-raised" ng-click="backspace()" ng-if="element.useIcon && element.text==\'backspace\'"> \
                             <i class="md-icon dp40">backspace</i> \
                           </md-button> \
                          </md-grid-tile> \
                      \
                          <!-- finish button --> \
                          <!-- ng-show="loginValue"  --> \
                          <md-grid-tile md-rowspan="2" md-colspan="6" ng-show="loginValue"> \
                            <md-button md-no-ink="true" class="md-raised submit" ng-click="next()">{{::ui["done"] || "完成"}}</md-button> \
                          </md-grid-tile> \
                      </md-grid-list> \
                    </div> \
                    <div flex> \
                      <md-card> \
                       <!-- <img src="card-image.png" class="md-card-image" alt="image caption"> --> \
                          <md-card-content ng-show="MemberNotFound"> \
                              找不到會員 \
                          </md-card-content> \
                              <md-card-content ng-hide="MemberNotFound"> \
                                  <h2>卡號：{{Member.vipcode}}</h2> \
                                  <ul> \
                                      <li>會藉：{{Member.vipgroup.string[0]}}</li> \
                                      <!--<li>累積點數：10,000,000</li>--> \
                                      <li>名稱：{{Member.name()}}</li> \
                                      <li>生日：{{Member.dob()}}</li> \
                                      <!--<li>最後一次光臨日期：2013年3月16日</li>--> \
                                  </ul> \
                              </md-card-content> \
                              <div class="md-actions" layout="row" layout-align="end center"> \
                                  <md-button md-no-ink="true" class="no-margin md-raised md-warn" ng-click="cancel()">取消/關閉</md-button> \
                                  <md-button md-no-ink="true" class="no-margin md-raised proceed" ng-show="MainService.mode == MainService.schema.mode.order" ng-click="MainService.Cart.assignMemberToOrder(Member)">套用</md-button> \
                              </div> \
                              <!-- <md-card-footer> \
                  Card footer \
                </md-card-footer> --> \
        </md-card> \
                    </div> \
                  </div> \
          \
              </md-dialog-content> \
          </md-dialog> \
        </div>');
    }]);

    angular.module('tmpl/pager-default-materialize.html', []).run(["$templateCache", function ($templateCache) {
        $templateCache.put('tmpl/pager-default-materialize.html',
          '<div class="md-custom-grid-container md-custom-grid col-1x2 md-grid-pager"> \
          <div class="col s10-5"><button class="flex-center md-button-arrow md-raised md-button md-grid-button" ng-class="{disabled: noPrevious(), previous: align}" ng-click="selectPage(page - 1, $event)" ng-disabled="noPrevious()"><i ng-hide="loginValue" class="md-icon {{::iconSize || \'dp70\'}}">keyboard_arrow_left</i></button></div> \
          <div class="col s10-5"><button class="flex-center md-raised md-button md-grid-button" ng-class="{disabled: noNext(), next: align}" ng-click="selectPage(page + 1, $event)" ng-disabled="noNext()"><i ng-hide="loginValue" class="md-icon {{::iconSize || \'dp70\'}}">keyboard_arrow_right</i></button></div> \
      </div>');
    }]);

    angular.module('tmpl/pager-materialize-s4.html', []).run(["$templateCache", function ($templateCache) {
        $templateCache.put('tmpl/pager-materialize-s4.html',
          '<div class="md-custom-grid-container"> \
          <div class="col s4"><button class="flex-center md-raised md-button md-grid-button" ng-class="{disabled: noPrevious(), previous: align}" ng-click="selectPage(page - 1, $event)" ng-disabled="noPrevious()"><i ng-hide="loginValue" class="md-icon {{::iconSize || \'dp70\'}}">keyboard_arrow_left</i></button></div> \
          <div class="col s4"><button class="flex-center md-raised md-button md-grid-button" ng-class="{disabled: noNext(), next: align}" ng-click="selectPage(page + 1, $event)" ng-disabled="noNext()"><i ng-hide="loginValue" class="md-icon {{::iconSize || \'dp70\'}}">keyboard_arrow_right</i></button></div> \
      </div>');
    }]);

    // <div>{{noPrevious()}} {{iconSize}}</div>
    angular.module('tmpl/pager-default-grid-only.html', []).run(["$templateCache", function ($templateCache) {
        $templateCache.put('tmpl/pager-default-grid-only.html',
          '<div layout="column"> \
          <div flex="50" class="col s10-2"><button class="md-raised md-button md-grid-button" ng-class="{disabled: noPrevious(), previous: align}" ng-click="selectPage(page - 1, $event)" ng-disabled="noPrevious()"><i ng-hide="loginValue" class="md-icon {{::iconSize || \'dp70\'}}">keyboard_arrow_up</i></button></div> \
          <div flex="50" class="col s10-2"><button class="md-raised md-button md-grid-button" ng-class="{disabled: noNext(), next: align}" ng-click="selectPage(page + 1, $event)" ng-disabled="noNext()"><i ng-hide="loginValue" class="md-icon {{::iconSize || \'dp70\'}}">keyboard_arrow_down</i></button></div> \
      </div>');
    }]);

    angular.module('tmpl/toast-template.html', []).run(["$templateCache", function ($templateCache) {
        $templateCache.put('tmpl/toast-template.html',
          '<md-toast> \
        <!-- <span flex>Custom toast!</span> --> \
        <div>{{message}}</div> \
        <md-button md-no-ink="true" class="md-raised md-warn" ng-click="closeToast()"> \
          {{close}} \
        </md-button> \
      </md-toast>');
    }]);

    angular.module('tmpl/pager-default.html', []).run(["$templateCache", function ($templateCache) {
        $templateCache.put('tmpl/pager-default.html',
          '<div> \
        <md-grid-list class="now" md-cols="{{::colNo || \'2\'}}" md-row-height="{{::gridHeight || \'64px\'}}" md-gutter="1" style=""> \
          <md-grid-tile> \
            <md-button md-no-ink="true" class="flex-center md-raised" ng-class="{disabled: noPrevious(), previous: align}" ng-click="selectPage(page - 1, $event)" ng-disabled="noPrevious()"><i ng-hide="loginValue" class="md-icon {{::iconSize || \'dp70\'}}">keyboard_arrow_left</i></md-button> \
          </md-grid-tile> \
            <md-grid-tile> \
              <md-button md-no-ink="true" class="flex-center md-raised" ng-class="{disabled: noNext(), next: align}" ng-click="selectPage(page + 1, $event)" ng-disabled="noNext()"><i ng-hide="loginValue" class="md-icon {{::iconSize || \'dp70\'}}">keyboard_arrow_right</i></md-button> \
          </md-grid-tile> \
        </md-grid-list> \
      </div>');
    }]);

    angular.module('tmpl/gridMenuOption-cancel.html', []).run(["$templateCache", function ($templateCache) {
        $templateCache.put('tmpl/gridMenuOption-cancel.html',
          '<div> \
        <md-grid-list md-cols="{{::gridCol}}" md-row-height="{{::gridHeight}}" md-gutter="1" style=""> \
            <md-grid-tile> \
                <md-button md-no-ink="true" class="md-raised" ng-click="selectAllItem();">全選</md-button> \
            </md-grid-tile> \
            <md-grid-tile> \
                <md-button md-no-ink="true" class="md-raised" ng-click="unselectAllItem();">取消選擇</md-button> \
            </md-grid-tile> \
            <md-grid-tile class="dummy-grid" ng-repeat="i in $parent.$parent.numberToArray( 2 ) track by $index"><span><span></md-grid-tile> \
            <md-grid-tile> \
                <md-button md-no-ink="true" class="md-raised cancel" ng-click="MainService.cancelOption();"><i class="md-icon dp45">clear</i></md-button> \
            </md-grid-tile> \
        </md-grid-list> \
      </div>');
    }]);

    // billing-menu.html, shared print bill and billing
    angular.module('tmpl/billing-menu.html', []).run(["$templateCache", function ($templateCache) {
        $templateCache.put('tmpl/billing-menu.html',
          '<div flex="90" id="billing-menu" class="menu-area" flex layout="row" ng-controller="billingController as billing" style="" ng-class="{\'discount-only\' : MainService.modeOrder == MainService.schema.modeOrder.printBill}"> \
            <div class="md-grid-container" flex style="height: 100%;" ng-class="{\'left-col\':MainService.modeOrder != MainService.schema.modeOrder.printBill}"> \
              <h2 class="header left-edge-margin fill-height" style="" ng-hide="isNotModeOrder(\'billing\')">{{ ::ui[\'payment_method\'] }}</h2> \
              \
              <bill-payment-options ng-hide="isNotModeOrder(\'billing\')"></bill-payment-options>\
              <h2 class="header left-edge-margin fill-height" style="">{{ ::ui[\'discount_option\'] }}</h2> \
              \
              <bill-discount-options></bill-discount-options>\
              \
              <!-- print bill order --> \
              <div style="height: 13.6%" ng-show="MainService.modeOrder == MainService.schema.modeOrder.printBill"> \
                <div class="materialize-keypad col-1x4"> \
                  <div class="row"> \
                    <div class="col s10-6"><button class="icon-with-text cancel btn md-button md-grid-button" ng-click="cancel()"><i class="md-icon dp45">clear</i><span>{{::ui["cancel_back"] || "取消/返回"}}</span></button></div> \
                    <div class="col s10-4"><button class="icon-with-text proceed btn md-button md-grid-button" ng-click="printBill()"><i class="md-icon dp45">done</i><span>{{::ui["done"] || "完成"}}</span></button></div> \
                  </div> \
                </div> \
              </div> \
              <!-- <div class="header left-edge-margin fill-height" style="height: 6%">{{ ::ui[\'total_amount\'] }}</div> --> \
            </div> \
              \
            <div class="right-col md-grid-container" flex="50" style="height: 100%;" ng-hide="MainService.modeOrder == MainService.schema.modeOrder.printBill"> \
              <!-- keypad --> \
              <h2 class="header right-edge-margin fill-height" style="">{{ ::ui[\'input_amount\'] }}</h2> \
              <bill-numeric-keypad></bill-numeric-keypad>\
              <!--<div class="keypad-amount numeric-keypad materialize-keypad col-4x3"> \
                <div class="row"> \
                  <div class="col s4"><button class="btn md-button md-grid-button" ng-click="MainService.appendAmount(1)">1</button></div> \
                  <div class="col s4"><button class="btn md-button md-grid-button" ng-click="MainService.appendAmount(2)">2</button></div> \
                  <div class="col s4"><button class="btn md-button md-grid-button" ng-click="MainService.appendAmount(3)">3</button></div> \
                </div> \
                <div class="row"> \
                  <div class="col s4"><button class="btn md-button md-grid-button" ng-click="MainService.appendAmount(4)">4</button></div> \
                  <div class="col s4"><button class="btn md-button md-grid-button" ng-click="MainService.appendAmount(5)">5</button></div> \
                  <div class="col s4"><button class="btn md-button md-grid-button" ng-click="MainService.appendAmount(6)">6</button></div> \
                </div> \
                <div class="row"> \
                  <div class="col s4"><button class="btn md-button md-grid-button" ng-click="MainService.appendAmount(7)">7</button></div> \
                  <div class="col s4"><button class="btn md-button md-grid-button" ng-click="MainService.appendAmount(8)">8</button></div> \
                  <div class="col s4"><button class="btn md-button md-grid-button" ng-click="MainService.appendAmount(9)">9</button></div> \
                </div> \
                <div class="row"> \
                  <div class="col s4"><button class="btn md-button md-grid-button" ng-click="MainService.appendAmount(\'.\')">.</button></div> \
                  <div class="col s4"><button class="btn md-button md-grid-button" ng-click="MainService.appendAmount(0)">0</button></div> \
                  <div class="col s4"><button class="btn md-button md-grid-button" ng-click="MainService.truncateAmount()"><i class="md-icon">backspace</i></button></div> \
                </div> \
              </div>--> \
                \
              <!-- keypad for cash shortcut --> \
              <h2 class="header right-edge-margin fill-height" style="">{{ ::ui[\'amount_shortcut\'] }}</h2> \
              <bill-amount-shortcuts></bill-amount-shortcuts>\
              <!--<div class="keypad-amount-shortcut materialize-keypad col-3x3"> \
                <div class="row"> \
                  <div class="col s4"><button class="btn md-button md-grid-button" ng-click="MainService.addAmount(20)">+20</button></div> \
                  <div class="col s4"><button class="btn md-button md-grid-button" ng-click="MainService.addAmount(50)">+50</button></div> \
                  <div class="col s4"><button class="btn md-button md-grid-button" ng-click="MainService.addAmount(100)">+100</button></div> \
                </div> \
                <div class="row"> \
                  <div class="col s6"><button class="btn md-button md-grid-button" ng-click="MainService.addAmount(500)">+500</button></div> \
                  <div class="col s6"><button class="btn md-button md-grid-button" ng-click="MainService.addAmount(1000)">+1000</button></div> \
                </div> \
                <div class="row"> \
                  <div class="col s6"><button class="btn md-button md-grid-button" ng-click="MainService.inputExact()">{{ ::ui[\'same_as_total_amount\'] }}</button></div> \
                  <div class="col s6"><button class="btn md-button md-grid-button" ng-click="MainService.inputExact(\'matchTotal\')">{{ ::ui[\'sum_of_method_same_as_total_amount\'] }}</button></div> \
                </div> \
              </div>--> \
              <grid-menu-option class="print_bill-menu-option menu-option md-grid-container" grid-height="\'fit\'" grid-col="2" template="tmpl/gridMenuOption-order.html" flex="100" layout="column" style="height: 13%;" ng-if="MainService.modeOrder === MainService.schema.modeOrder.billing"></grid-menu-option> \
            </div> \
        </div> \
        <div flex="10" class="flex-center reminder">{{ ::ui[\'total_amount\'] + ui[\'colon\'] }}{{ MainService.Cart.totalPrice | currency : INFO.currency : 1 }}</div>');
    }]);

    angular.module('tmpl/search-bill-menu-takeAway.html', []).run(["$templateCache", function ($templateCache) {
        $templateCache.put('tmpl/search-bill-menu-takeAway.html',
          '<div flex="100" id="search-bill-menu" class="menu-area" flex layout="row" style=""> \
          <div class="md-grid-container" flex="100" style="height: 100%;"> \
            \
              <md-input-container class="login" style="height: 14%; width: 50%;"> \
                  <label>{{::ui[\'bill_no\']}}</label> \
                  <input name="billNo" class="dialog-close" ng-model="billNo" ng-trim="false" type="text" custom-always-focus="true"> \
              </md-input-container> \
            \
            <div style="height: 10.5%; width: 45%; min-width: 335px; position: absolute; top: 6px; right: 6px;"> \
              <div class="materialize-keypad col-1x4"> \
                <div class="row"> \
                  <div class="col s10-2"><button class="flex-center btn md-button md-grid-button" ng-click="browseBill(\'prev\')"><i class="flex-center md-icon dp40">keyboard_arrow_up</i></button></div> \
                  <div class="col s10-2"><button class="flex-center btn md-button md-grid-button" ng-click="browseBill(\'next\')"><i class="flex-center md-icon dp40">keyboard_arrow_down</i></button></div> \
                  <div class="col s10-6"><button class="icon-with-text btn md-button md-grid-button" ng-click="lastBill()"><span>{{::ui[\'last_bill\']}}</span></button></div> \
                </div> \
              </div> \
            </div> \
            \
              <!-- keypad --> \
              <h2 class="header right-edge-margin fill-height" style="">{{ ::ui[\'bill_search\'] }}</h2> \
              <div layout="row" style="height: 60%;"> \
                <div flex="50" style="height: 100%;width: 300px;"> \
                  <div class="materialize-keypad col-3x4" style=""> \
                    <div class="row"> \
                      <div class="col s4"><button class="btn md-button md-grid-button" ng-click="inputFree(\'1\')">1</button></div> \
                      <div class="col s4"><button class="btn md-button md-grid-button" ng-click="inputFree(2)">2</button></div> \
                      <div class="col s4"><button class="btn md-button md-grid-button" ng-click="inputFree(3)">3</button></div> \
                    </div> \
                    <div class="row"> \
                      <div class="col s4"><button class="btn md-button md-grid-button" ng-click="inputFree(4)">4</button></div> \
                      <div class="col s4"><button class="btn md-button md-grid-button" ng-click="inputFree(5)">5</button></div> \
                      <div class="col s4"><button class="btn md-button md-grid-button" ng-click="inputFree(6)">6</button></div> \
                    </div> \
                    <div class="row"> \
                      <div class="col s4"><button class="btn md-button md-grid-button" ng-click="inputFree(7)">7</button></div> \
                      <div class="col s4"><button class="btn md-button md-grid-button" ng-click="inputFree(8)">8</button></div> \
                      <div class="col s4"><button class="btn md-button md-grid-button" ng-click="inputFree(9)">9</button></div> \
                    </div> \
                    <div class="row"> \
                      <div class="col s4"><button class="cancel btn md-button md-grid-button" ng-click="cancel()"><i class="md-icon dp40" aria-hidden="false" style="">clear</i></button></div> \
                      <div class="col s4"><button class="btn md-button md-grid-button" ng-click="inputFree(0)">0</button></div> \
                      <div class="col s4"><button class="proceed2 btn md-button md-grid-button" ng-click="searchBill()"><i class="md-icon dp45">done</i></button></div> \
                    </div> \
                  </div> \
                </div> \
                <div flex="50" class="bill-info"> \
                  {{::ui[\'ref_no\'] + ui[\'colon\']}} {{MainService.Cart.refNo}} <br/>\
                  {{::ui[\'trans_date\'] + ui[\'colon\']}} {{MainService.Cart.transTime}} \
                </div> \
                <div flex="50" class="bill-info" ng-init="test = {}" ng-if="test.length > 0"> \
                  haha\
                </div> \
                <!-- ::ui[\'暫時未有賬單\'] -->\
              </div> \
                \
              <!-- keypad for cash shortcut --> \
                <div style="height: 20%"> \
                  <div class="materialize-keypad col-1x4" ng-if="MainService.Cart.isVoid"> \
                   <div class="row"> \
                      <div class="col s6"><button class="btn md-button md-grid-button" ng-click="printBill()">{{::ui["print_bill"]}}</button></div> \
                      <div class="col s6"><button class="icon-with-text proceed btn md-button md-grid-button" ng-click="backViewModeInOrder();"><i class="md-icon dp45">done</i><span>{{::ui["done"] || "完成"}}</span></button></div> \
                    </div> \
                  </div> \
                   <div class="materialize-keypad col-1x4" ng-if="!MainService.Cart.isVoid"> \
                    <div class="row" ng-if="!MainService.Cart.isVoid"> \
                      <div class="col s3"><button class="btn md-button md-grid-button" ng-click="changeBillInfo()">{{::ui["txt_change_payment_info"]}}</button></div> \
                      <div class="col s3"><button class="btn md-button md-grid-button" ng-click="printBill()">{{::ui["print_bill"]}}</button></div> \
                      <div class="col s3"><button class="btn md-button md-grid-button" ng-click="voidBill();">{{::ui["txt_void_bill"]}}</button></div>\
                      <div class="col s3"><button class="icon-with-text proceed btn md-button md-grid-button" ng-click="backViewModeInOrder();"><i class="md-icon dp45">done</i><span>{{::ui["done"] || "完成"}}</span></button></div> \
                    </div> \
                  </div> \
                </div> \
            </div> \
        </div>')
    }]);

    angular.module('tmpl/search-bill-menu-delivery.html', []).run(["$templateCache", function ($templateCache) {
        $templateCache.put('tmpl/search-bill-menu-delivery.html',
          '<div flex="100" id="search-bill-menu" class="menu-area" flex layout="row"  style="" ng-controller="searchBillControllerDelivery"> \
          <div class="md-grid-container" flex="100" style="height: 100%;"> \
            \
              <md-input-container class="login" style="height: 14%; width: 50%;"> \
                  <label>{{::ui[\'bill_no\']}}</label> \
                  <input name="billNo" class="dialog-close" ng-model="billNo" ng-trim="false" type="text" custom-always-focus="true"> \
              </md-input-container> \
            \
            <div style="height: 10.5%; width: 45%; min-width: 335px; position: absolute; top: 6px; right: 6px;"> \
              <div class="materialize-keypad col-1x4"> \
                <div class="row"> \
                  <div class="col s10-2"><button class="flex-center btn md-button md-grid-button" ng-click="browseBill(\'prev\')"><i class="flex-center md-icon dp40">keyboard_arrow_up</i></button></div> \
                  <div class="col s10-2"><button class="flex-center btn md-button md-grid-button" ng-click="browseBill(\'next\')"><i class="flex-center md-icon dp40">keyboard_arrow_down</i></button></div> \
                  <div class="col s10-6"><button class="icon-with-text btn md-button md-grid-button" ng-click="lastBill()"><span>{{::ui[\'last_bill\']}}</span></button></div> \
                </div> \
              </div> \
            </div> \
            \
              <!-- keypad --> \
              <h2 class="header right-edge-margin fill-height" style="">{{ ::ui[\'bill_search\'] }}</h2> \
              <div layout="row" style="height: 60%;"> \
                <div flex="50" style="height: 100%;width: 300px;"> \
                  <div class="materialize-keypad col-3x4" style=""> \
                    <div class="row"> \
                      <div class="col s4"><button class="btn md-button md-grid-button" ng-click="inputFree(\'1\')">1</button></div> \
                      <div class="col s4"><button class="btn md-button md-grid-button" ng-click="inputFree(2)">2</button></div> \
                      <div class="col s4"><button class="btn md-button md-grid-button" ng-click="inputFree(3)">3</button></div> \
                    </div> \
                    <div class="row"> \
                      <div class="col s4"><button class="btn md-button md-grid-button" ng-click="inputFree(4)">4</button></div> \
                      <div class="col s4"><button class="btn md-button md-grid-button" ng-click="inputFree(5)">5</button></div> \
                      <div class="col s4"><button class="btn md-button md-grid-button" ng-click="inputFree(6)">6</button></div> \
                    </div> \
                    <div class="row"> \
                      <div class="col s4"><button class="btn md-button md-grid-button" ng-click="inputFree(7)">7</button></div> \
                      <div class="col s4"><button class="btn md-button md-grid-button" ng-click="inputFree(8)">8</button></div> \
                      <div class="col s4"><button class="btn md-button md-grid-button" ng-click="inputFree(9)">9</button></div> \
                    </div> \
                    <div class="row"> \
                      <div class="col s4"><button class="cancel btn md-button md-grid-button" ng-click="cancel()"><i class="md-icon dp40" aria-hidden="false" style="">clear</i></button></div> \
                      <div class="col s4"><button class="btn md-button md-grid-button" ng-click="inputFree(0)">0</button></div> \
                      <div class="col s4"><button class="proceed2 btn md-button md-grid-button" ng-click="searchBill($event)"><i class="md-icon dp45">done</i></button></div> \
                    </div> \
                  </div> \
                </div> \
                <div flex="50" class="bill-info"> \
                  {{::ui[\'ref_no\'] + ui[\'colon\']}} {{MainService.Cart.refNo}} <br/>\
                  {{::ui[\'trans_date\'] + ui[\'colon\']}} {{MainService.Cart.transTime}} \
                </div> \
                <div flex="50" class="bill-info" ng-init="test = {}" ng-if="test.length > 0"> \
                  haha\
                </div> \
                <!-- ::ui[\'暫時未有賬單\'] -->\
              </div> \
                \
              <!-- keypad for cash shortcut --> \
                <div style="height: 20%"> \
                  <div class="materialize-keypad col-1x4"> \
                    <div class="row"> \
                        <div class="col s3"><button class="btn md-button md-grid-button" ng-click="changeBillInfo()">{{::ui["txt_change_payment_info"]}}</button></div> \
                      <div class="col s3"><button class="btn md-button md-grid-button" ng-click="printBill()">{{::ui["print_bill"]}}</button></div> \
                      <div class="col s3"><button class="btn md-button md-grid-button" ng-click="voidBill();">{{::ui["txt_void_bill"]}}</button></div> \
                        <div class="col s3"><button class="icon-with-text proceed btn md-button md-grid-button" ng-click="MainService.modeDeliveryOrder = MainService.schema.modeDeliveryOrder.normal;"><i class="md-icon dp45">done</i><span>{{::ui["done"] || "完成"}}</span></button></div> \
                    </div> \
                  </div> \
                </div> \
            </div> \
        </div>')
    }]);


    // based on billing-menu.html
    angular.module('tmpl/search-bill-menu.html', []).run(["$templateCache", function ($templateCache) {
        $templateCache.put('tmpl/search-bill-menu.html',
          '<div flex="100" id="search-bill-menu" class="menu-area" flex layout="row" ng-controller="searchBillController as searchBill" style=""> \
          <div class="md-grid-container" flex="100" style="height: 100%;"> \
            \
              <md-input-container class="login" style="height: 14%; width: 50%;"> \
                  <label>{{::ui[\'bill_no\']}}</label> \
                  <input name="billNo" class="dialog-close" ng-model="billNo" ng-trim="false" type="text" custom-always-focus="true"> \
              </md-input-container> \
            \
            <div style="height: 10.5%; width: 45%; min-width: 335px; position: absolute; top: 6px; right: 6px;"> \
              <div class="materialize-keypad col-1x4"> \
                <div class="row"> \
                  <div class="col s10-2"><button class="flex-center btn md-button md-grid-button" ng-click="browseBill(\'prev\')"><i class="flex-center md-icon dp40">keyboard_arrow_up</i></button></div> \
                  <div class="col s10-2"><button class="flex-center btn md-button md-grid-button" ng-click="browseBill(\'next\')"><i class="flex-center md-icon dp40">keyboard_arrow_down</i></button></div> \
                  <div class="col s10-6"><button class="icon-with-text btn md-button md-grid-button" ng-click="lastBill()"><span>{{::ui[\'last_bill\']}}</span></button></div> \
                </div> \
              </div> \
            </div> \
            \
              <!-- keypad --> \
              <h2 class="header right-edge-margin fill-height" style="">{{ ::ui[\'bill_search\'] }}</h2> \
              <div layout="row" style="height: 60%;"> \
                <div flex="50" style="height: 100%;width: 300px;"> \
                  <div class="materialize-keypad col-3x4" style=""> \
                    <div class="row"> \
                      <div class="col s4"><button class="btn md-button md-grid-button" ng-click="inputFree(\'1\')">1</button></div> \
                      <div class="col s4"><button class="btn md-button md-grid-button" ng-click="inputFree(2)">2</button></div> \
                      <div class="col s4"><button class="btn md-button md-grid-button" ng-click="inputFree(3)">3</button></div> \
                    </div> \
                    <div class="row"> \
                      <div class="col s4"><button class="btn md-button md-grid-button" ng-click="inputFree(4)">4</button></div> \
                      <div class="col s4"><button class="btn md-button md-grid-button" ng-click="inputFree(5)">5</button></div> \
                      <div class="col s4"><button class="btn md-button md-grid-button" ng-click="inputFree(6)">6</button></div> \
                    </div> \
                    <div class="row"> \
                      <div class="col s4"><button class="btn md-button md-grid-button" ng-click="inputFree(7)">7</button></div> \
                      <div class="col s4"><button class="btn md-button md-grid-button" ng-click="inputFree(8)">8</button></div> \
                      <div class="col s4"><button class="btn md-button md-grid-button" ng-click="inputFree(9)">9</button></div> \
                    </div> \
                    <div class="row"> \
                      <div class="col s4"><button class="cancel btn md-button md-grid-button" ng-click="cancel()"><i class="md-icon dp40" aria-hidden="false" style="">clear</i></button></div> \
                      <div class="col s4"><button class="btn md-button md-grid-button" ng-click="inputFree(0)">0</button></div> \
                      <div class="col s4"><button class="proceed2 btn md-button md-grid-button" ng-click="searchBill($event)"><i class="md-icon dp45">done</i></button></div> \
                    </div> \
                  </div> \
                </div> \
                <div flex="50" class="bill-info"> \
                  {{::ui[\'ref_no\'] + ui[\'colon\']}} {{MainService.Cart.refNo}} <br/>\
                  {{::ui[\'trans_date\'] + ui[\'colon\']}} {{MainService.Cart.transTime}} \
                </div> \
                <div flex="50" class="bill-info" ng-init="test = {}" ng-if="test.length > 0"> \
                  haha\
                </div> \
                <!-- ::ui[\'暫時未有賬單\'] -->\
              </div> \
                \
              <!-- keypad for cash shortcut --> \
                <div style="height: 20%"> \
                  <div class="materialize-keypad col-1x4" ng-if="MainService.Cart.isVoid"> \
                   <div class="row"> \
                      <div class="col s6"><button class="btn md-button md-grid-button" ng-click="printBill()">{{::ui["print_bill"]}}</button></div> \
                      <div class="col s6"><button class="icon-with-text proceed btn md-button md-grid-button" ng-click="successClick();"><i class="md-icon dp45">done</i><span>{{::ui["done"] || "完成"}}</span></button></div> \
                    </div> \
                  </div> \
                   <div class="materialize-keypad col-1x4" ng-if="!MainService.Cart.isVoid"> \
                    <div class="row" ng-if="!MainService.Cart.isVoid"> \
                      <div class="col s3"><button class="btn md-button md-grid-button" ng-click="changeBillInfo()">{{::ui["txt_change_payment_info"]}}</button></div> \
                      <div class="col s3"><button class="btn md-button md-grid-button" ng-click="printBill()">{{::ui["print_bill"]}}</button></div> \
                      <div class="col s3"><button class="btn md-button md-grid-button" ng-click="voidBill();">{{::ui["txt_void_bill"]}}</button></div>\
                      <div class="col s3"><button class="icon-with-text proceed btn md-button md-grid-button" ng-click="successClick();"><i class="md-icon dp45">done</i><span>{{::ui["done"] || "完成"}}</span></button></div> \
                    </div> \
                  </div> \
                </div> \
            </div> \
        </div>')
    }]);

    // based on billing-menu.html
    angular.module('tmpl/amend-bill-menu.html', []).run(["$templateCache", function ($templateCache) {
        $templateCache.put('tmpl/amend-bill-menu.html',
          '<div flex="90" id="billing-amend-menu" class="menu-area" layout="row" ng-controller="amendBillController as amendBill"> \
          <div class="left-col md-grid-container" flex="50" style="height: 100%;"> \
            <h2 class="header left-edge-margin fill-height" style="">{{ ::ui[\'payment_method\'] }}</h2> \
            <bill-payment-options></bill-payment-options>\
            <!-- <div class="header left-edge-margin fill-height" style="height: 6%">{{ ::ui[\'total_amount\'] }}</div> --> \
          </div> \
            \
          <div class="right-col md-grid-container" flex="50" style="height: 100%;"> \
            <!-- keypad --> \
            <h2 class="header right-edge-margin fill-height">{{ ::ui[\'input_amount\'] }}</h2> \
            <bill-numeric-keypad></bill-numeric-keypad>\
              \
            <!-- keypad for cash shortcut --> \
            <h2 class="header right-edge-margin fill-height">{{ ::ui[\'amount_shortcut\'] }}</h2> \
            <bill-amount-shortcuts></bill-amount-shortcuts>\
            <grid-menu-option class="menu-option md-grid-container" grid-height="\'fit\'" grid-col="2" template="tmpl/gridMenuOption-order.html" flex="100" layout="column" style="height: 13%;"></grid-menu-option> \
          </div> \
      </div> \
      <div flex="10" class="flex-center reminder">{{ ::ui[\'total_amount\'] + ui[\'colon\'] }}{{ MainService.Cart.totalPrice | currency : INFO.currency : 1 }}</div>');
    }]);

    angular.module('tmpl/bill-payment-options.html', []).run(["$templateCache", function ($templateCache) {
        $templateCache.put('tmpl/bill-payment-options.html', '\
      <div class="payment-option materialize-keypad long-text" ng-class="{\'col-4x3\': isModeBill(\'billPrint\'), \'col-7x3\': isModeBill(\'amendBill\') || MainService.getOtherWriteOrderService().modeSetting.modeInService.isMode(\'amendBill\') || MainService.isModeDeliveryOrder(\'amendBill\')}"> \
        <div class="row">\
            <div class="col s4" ng-repeat="tender in MainService.getTenderList() | startFrom:(currentPaymentPage - 1) * paymentPerPage | limitTo: paymentPerPage" ><button class="btn md-button md-grid-button" ng-click="MainService.setPaymentMethod(tender)">{{:: INFO.lang=="001"? tender.name1 : tender.name3}}</button></div> \
            \
            <div class="col s4 dummy-grid" ng-if="noOfPage(MainService.getTenderList().length, paymentPerPage) == currentPaymentPage && MainService.getTenderList().length % paymentPerPage > 0" ng-repeat="i in remainderPageArr( MainService.getTenderList().length, paymentPerPage ) track by $index"><span></span></div> \
            \
            <pager-custom template="tmpl/pager-materialize-s4.html" total-items="MainService.getTenderList().length" items-per-page="paymentPerPage" ng-model="currentPaymentPage"></pager-custom> \
        </div> \
      </div> \
    ');
    }]);

    angular.module('tmpl/bill-discount-options.html', []).run(["$templateCache", function ($templateCache) {
        $templateCache.put('tmpl/bill-discount-options.html', '\
      <div class="discount-option materialize-keypad md-custom-grid col-4x3 long-text"> \
        <div class="row"> \
          <div class="col s4" ng-repeat="discount in MainService.availableDiscount | startFrom:(currentDiscountPage - 1) * discountPerPage | limitTo: discountPerPage" ng-if="MainService.availableDiscount.length > 0"><button class="btn md-button md-grid-button" ng-click="MainService.setDiscount(discount.coupon_alias,$event)"><span ng-if="INFO.lang==\'001\'">{{::discount.name1}}</span><span ng-if="INFO.lang==\'003\'">{{::discount.name2}}</span></button></div> \
           \
          <div class="col s4 dummy-grid" ng-if="noOfPage(MainService.availableDiscount.length, discountPerPage) == currentDiscountPage && MainService.availableDiscount.length % discountPerPage > 0" ng-repeat="i in remainderPageArr( MainService.availableDiscount.length, discountPerPage ) track by $index"><span></span></div> \
          \
          <pager-custom template="tmpl/pager-materialize-s4.html" total-items="MainService.availableDiscount.length" items-per-page="discountPerPage" ng-model="currentDiscountPage"></pager-custom> \
        </div> \
      </div> \
    ');
    }]);

    //<!-- for billing, amend bill -->
    angular.module('tmpl/bill-numeric-keypad.html', []).run(["$templateCache", function ($templateCache) {
        $templateCache.put('tmpl/bill-numeric-keypad.html', '\
      <div class="keypad-amount numeric-keypad materialize-keypad col-4x3"> \
        <div class="row"> \
          <div class="col s4"><button class="btn md-button md-grid-button" ng-click="MainService.appendAmount(1)">1</button></div> \
          <div class="col s4"><button class="btn md-button md-grid-button" ng-click="MainService.appendAmount(2)">2</button></div> \
          <div class="col s4"><button class="btn md-button md-grid-button" ng-click="MainService.appendAmount(3)">3</button></div> \
        </div> \
        <div class="row"> \
          <div class="col s4"><button class="btn md-button md-grid-button" ng-click="MainService.appendAmount(4)">4</button></div> \
          <div class="col s4"><button class="btn md-button md-grid-button" ng-click="MainService.appendAmount(5)">5</button></div> \
          <div class="col s4"><button class="btn md-button md-grid-button" ng-click="MainService.appendAmount(6)">6</button></div> \
        </div> \
        <div class="row"> \
          <div class="col s4"><button class="btn md-button md-grid-button" ng-click="MainService.appendAmount(7)">7</button></div> \
          <div class="col s4"><button class="btn md-button md-grid-button" ng-click="MainService.appendAmount(8)">8</button></div> \
          <div class="col s4"><button class="btn md-button md-grid-button" ng-click="MainService.appendAmount(9)">9</button></div> \
        </div> \
        <div class="row"> \
          <div class="col s4"><button class="btn md-button md-grid-button" ng-click="MainService.appendAmount(\'.\')">.</button></div> \
          <div class="col s4"><button class="btn md-button md-grid-button" ng-click="MainService.appendAmount(0)">0</button></div> \
          <div class="col s4"><button class="btn md-button md-grid-button" ng-click="MainService.truncateAmount()"><i class="md-icon">backspace</i></button></div> \
        </div> \
      </div>\
    ');
    }]);


    angular.module('tmpl/bill-amount-shortcuts.html', []).run(["$templateCache", function ($templateCache) {
        $templateCache.put('tmpl/bill-amount-shortcuts.html', '\
      <div class="keypad-amount-shortcut materialize-keypad col-3x3" style="height:35%"> \
        <div class="row"> \
          <div class="col s4"><button class="btn md-button md-grid-button" ng-click="MainService.addAmount(20)">+20</button></div> \
          <div class="col s4"><button class="btn md-button md-grid-button" ng-click="MainService.addAmount(50)">+50</button></div> \
          <div class="col s4"><button class="btn md-button md-grid-button" ng-click="MainService.addAmount(100)">+100</button></div> \
        </div> \
        <div class="row"> \
          <div class="col s6"><button class="btn md-button md-grid-button" ng-click="MainService.addAmount(500)">+500</button></div> \
          <div class="col s6"><button class="btn md-button md-grid-button" ng-click="MainService.addAmount(1000)">+1000</button></div> \
        </div> \
        <div class="row"> \
          <div class="col s6"><button class="btn md-button md-grid-button" ng-click="MainService.inputExact()">{{ ::ui[\'same_as_total_amount\'] }}</button></div> \
          <div class="col s6"><button class="btn md-button md-grid-button" ng-click="MainService.inputExact(\'matchTotal\')">{{ ::ui[\'sum_of_method_same_as_total_amount\'] }}</button></div> \
        </div> \
      </div> \
      <!--<div class="materialize-keypad"> \
        <div class="row one-third-height"> \
          <div class="col s6"><button class="btn md-button md-grid-button" ng-click="addAmount(20)">+20</button></div> \
          <div class="col s6"><button class="btn md-button md-grid-button" ng-click="addAmount(50)">+50</button></div> \
        </div> \
        <div class="row one-third-height"> \
          <div class="col s6"><button class="btn md-button md-grid-button" ng-click="addAmount(100)">+100</button></div> \
          <div class="col s6"><button class="btn md-button md-grid-button" ng-click="addAmount(500)">+500</button></div> \
        </div> \
        <div class="row one-third-height"> \
          <div class="col s6"><button class="btn md-button md-grid-button" ng-click="addAmount(1000)">+1000</button></div> \
          <div class="col s6"><button class="btn md-button md-grid-button" ng-click="inputExact()">{{ ::ui[\'same_as_total_amount\'] }}</button></div> \
        </div> \
      </div>--> \
    ');
    }]);

    // based on billing-menu.html
    angular.module('tmpl/transfer-item.html', []).run(["$templateCache", function ($templateCache) {
        $templateCache.put('tmpl/transfer-item.html',
          '<div id="transfer-item-menu" class="menu-area" layout="column" style=""> \
            \
            <div id="transfter-item-option" flex class="right-area md-grid-container" style="position: relative; width: 100%; height: 100%;"> \
                <div class="table-list materialize-keypad col-5x5" style="height: 85%"> \
                  <div class="row" style="margin-bottom: 0;"> \
                    <div class="col s10-2" ng-repeat="option in option | startFrom:(currentPage - 1) * transferOptionPerPage | limitTo: transferOptionPerPage"><button class="btn btn-table md-button md-grid-button" ng-click="submitTransfer(\'{{option.name}}\', MainService.Cart.tableNo, option.status)" ng-class="[uc.statusCode[option.status]]">{{option.name}} <div>{{returnStatus(option.status)}}</div></button></div> \
                     \
                    <div class="col s10-2 dummy-grid" ng-if="noOfPage( option.length, transferOptionPerPage ) == currentPage" ng-repeat="i in remainderPageArr( option.length, transferOptionPerPage ) track by $index"><span></span></div> \
                    \
                    <!-- temp measure -->\
                    <!--<div class="col s10-2 dummy-grid" ng-if="noOfTransferItemPage - currentPage == 0"><span></span></div> \
                    <div class="col s10-2 dummy-grid" ng-if="noOfTransferItemPage - currentPage == 0"><span></span></div>--> \
                    <pager-custom icon-size="\'dp56\'" template="tmpl/pager-default-materialize.html" total-items="option.length" items-per-page="transferOptionPerPage" ng-model="currentPage"></pager-custom> \
                  </div><!-- row --> \
                </div> \
                <div class="materialize-keypad col-1x5" style="height: 15%"> \
                  <div class="row" style="margin-bottom: 0;"> \
                    <div class="col s10-2 dummy-grid"><span></span></div> \
                    <div class="col s10-2 dummy-grid"><span></span></div> \
                    <div class="col s10-2 dummy-grid"><span></span></div> \
                    <div class="col s10-2 dummy-grid"><span></span></div> \
                    <div class="col s10-2"><button class="proceed btn md-button md-grid-button" ng-click="returnToOrder();">{{::ui[\'done\']}}</button></div> \
                  </div><!-- row --> \
                </div> \
            </div> \
            \
        </div> \
      ');
    }]);

    // billing-summary.html
    angular.module('tmpl/billing-summary.html', []).run(["$templateCache", function ($templateCache) {
        $templateCache.put('tmpl/billing-summary.html',
          '<div flex id="billing-summary" class="menu-area" flex layout="column" ng-controller="dummyCtrl as dummy" style=""> \
          <div class="md-grid-container" style="height: 100%;"> \
            <div class="materialize-keypad full-height" style=""> \
              <!-- payment --> \
              <div class="row total-container" style="height: 20%;"> \
                <div class="col s5 head" ><span class="cell">{{ ::ui[\'total_amount\'] + ui[\'colon\'] }}</span></div> \
                <div class="col s7 content">\
                    <span class="cell" ng-if="!MainService.Cart.paymentLater">{{ MainService.Cart.totalPrice | currency : INFO.currency : 1 }}</span>\
                    <span class="cell" ng-if="MainService.Cart.paymentLater">{{ MainService.Cart.price | currency : INFO.currency : 1 }}</span>\
                </div> \
              </div> \
              <!-- multiple row for multiple payment method and amount --> \
                <div class="paid-container" style="height: 60%;" ng-if="MainService.Cart.paymentLater"></div>\
              <div class="paid-container" style="height: 60%;" ng-if="!MainService.Cart.paymentLater"> \
                <div class="row" ng-if="MainService.deposit.appliedDeposit.length != 0">\
                     <div class="col s5 head"><span class="cell">[{{::ui[\'txt_deposit\']}}]{{MainService.deposit.appliedDeposit[0].getPaymentName()}}:</span></div> \
                     <div class="col s7 content"><span class="cell">{{MainService.deposit.appliedDeposit[0].paymentPrice | currency : INFO.currency : 1  }}</span></div> \
                  </div>\
                <div class="row" ng-repeat="methodList in MainService.appliedPaymentMethod"> \
                  <div class="row" ng-if="!(methodList.method == \'octopus\' || methodList.method == \'octopus2\')"> \
                    <div class="col s5 head"><span class="cell">{{methodList.name + ui[\'colon\']}}</span></div> \
                    <div class="col s7 content"><span class="cell">{{methodList.amount | currency : INFO.currency : 1  }}</span></div> \
                  </div> \
                  <div class="row" ng-if="methodList.method == \'octopus\' || methodList.method == \'octopus2\'"> \
                    <div class="row"> \
                      <div class="col s5 head dp30"><span class="cell">{{ui[\'octopus_wording_deduct_amount\']}}</span></div> \
                      <div class="col s7 content"><span class="cell">{{methodList.amount | currency : INFO.currency : 1  }}</span></div> \
                    </div> \
                    <div class="row"> \
                      <div class="col s5 head dp30"><span class="cell">{{ui[\'octopus_wording_remaining_value\']}}</span></div> \
                      <div class="col s7 content"><span class="cell">{{MainService.Cart.octopusBalance | currency : INFO.currency : 1  }}</span></div> \
                    </div> \
                  </div> \
                </div> \
              </div> \
                \
             <div ng-if="MainService.multiPayment.isEnable() && MainService.appliedPaymentMethod.length > 1" && !MainService.Cart.paymentLater>\
              <div class="row change-container multi-payment-panel">\
                <div class="col s5 head"><span class="cell" style="font-size:55px;">{{::ui["tips"] || "小費"}}:</span></div>\
                <div class="col s7 content"><span class="cell" style="font-size:55px;">{{ MainService.multiPayment.getRemainder(\'tips\') | currency : INFO.currency : 1  }}</span></div>\
                <div class="col s5 head"><span class="cell" style="font-size:55px;">{{::ui["payment_change"] || "找贖"}}:</span></div>\
                <div class="col s7 content"><span class="cell" style="font-size:55px;">{{ MainService.multiPayment.getRemainder() | currency : INFO.currency : 1  }}</span></div>\
                <md-button md-no-ink="true" class="btn-proceed md-grid-button md-raised proceed icon-with-text" ng-click="switchMode2({mode: \'floorPlan\', modeFloorPlan: \'order\'}, true, \'Order\'); MainService.resetPaymentSettings()" ng-if="MainService.checkIsNotTakeAwayMode() && !MainService.isCustomerDisplay;"><i class="md-icon dp45">done</i>{{::ui["done"] || "完成"}}</md-button> \
                <md-button md-no-ink="true" class="btn-proceed md-grid-button md-raised proceed icon-with-text" ng-click="MainService.getOtherWriteOrderService().successPaymentAndBackToWriteOrder()" ng-if="!MainService.checkIsNotTakeAwayMode()" style="font-size:22px;"><i class="md-icon dp45">done</i>{{::ui["done"] || "完成"}}</md-button> \
                <md-button md-no-ink="true" class="btn-proceed md-grid-button md-raised proceed icon-with-text" ng-click="switchMode2({ modeDeliveryOrder: \'writeOrder\' }, false, \'delivery_order\'); MainService.resetPaymentSettings()" ng-if="MainService.isModeDeliveryOrder(\'writeOrder\')"><i class="md-icon dp45">done</i>{{::ui["done"] || "完成"}}</md-button> \
              </div>\
            </div>\
               \
              <div class="row change-container" style="height: 20%;" ng-if="MainService.Cart.paymentLater">\
                <div class="col s5 head"><span class="cell">{{ui[\'txt_takeOutAndDelivery_payment_later_text\']}}:</span></div> \
                <div class="col s7 content"><span class="cell">{{ MainService.Cart.price | currency : INFO.currency : 1 }}</span></div> \
                <md-button md-no-ink="true" class="btn-proceed md-grid-button md-raised proceed icon-with-text" ng-click="MainService.getOtherWriteOrderService().successPaymentAndBackToWriteOrder()" ng-if="!MainService.checkIsNotTakeAwayMode()" style="font-size:22px;"><i class="md-icon dp45">done</i>{{::ui["done"] || "完成"}}</md-button> \
              </div>\
              <div class="row change-container" style="height: 20%;" ng-if="!MainService.Cart.paymentLater"> \
                <div class="col s5 head">\
                 <span class="cell" ng-if="MainService.isCash && MainService.isCard == false">{{ ::ui[\'payment_change\'] + ui[\'colon\'] }}</span>\
                 <span class="cell" ng-if="MainService.isCard">{{ ::ui[\'tips\'] + ui[\'colon\'] }}</span>\
                 <span class="cell" ng-if="MainService.deposit.appliedDeposit.length != 0 && MainService.appliedPaymentMethod.length == 0">{{::ui[\'txt_deposit_remainder\']}}</span>\
                </div>\
                <div class="col s7 content"><span class="cell">{{ MainService.remainerAmount | currency : INFO.currency : 1  }}</span></div> \
                <md-button md-no-ink="true" class="btn-proceed md-grid-button md-raised proceed icon-with-text" ng-click="switchMode2({mode: \'floorPlan\', modeFloorPlan: \'order\'}, true, \'Order\'); MainService.resetPaymentSettings()" ng-if="MainService.checkIsNotTakeAwayMode() && !MainService.isCustomerDisplay;"><i class="md-icon dp45">done</i>{{::ui["done"] || "完成"}}</md-button> \
                <md-button md-no-ink="true" class="btn-proceed md-grid-button md-raised proceed icon-with-text" ng-click="MainService.getOtherWriteOrderService().successPaymentAndBackToWriteOrder()" ng-if="!MainService.checkIsNotTakeAwayMode()" style="font-size:22px;"><i class="md-icon dp45">done</i>{{::ui["done"] || "完成"}}</md-button> \
                <md-button md-no-ink="true" class="btn-proceed md-grid-button md-raised proceed icon-with-text" ng-click="switchMode2({ modeDeliveryOrder: \'writeOrder\' }, false, \'delivery_order\'); MainService.resetPaymentSettings()" ng-if="MainService.isModeDeliveryOrder(\'writeOrder\')"><i class="md-icon dp45">done</i>{{::ui["done"] || "完成"}}</md-button> \
                </div> \
              </div> \
            </div> \
      </div>');
    }]);

    // layout="column"
    // s10-{{menu.col}}
    // switchMode3(menu.mode, menu.requiredLogin, menu.label, menu.label, menu.permission, downloadData);
    angular.module('tmpl/main.html', []).run(["$templateCache", function ($templateCache) {
        $templateCache.put('tmpl/main.html',
          ' \
        <div class="menu-container md-custom-grid col-5x5 default" style="height: 100%;" ng-controller="mainMenuController as mainMenu"> \
            <div class="row"> \
              <!-- exclude Main option in main menu, for other main menu + cash box(cash box affected only by query) -->\
              \
              <div class="col s10-2" ng-repeat="menu in mainMenuList" ng-if="menu.label != \'main_page\' && menu.col != 1 && ((menu.config == true) && menu.config != \'no\') || menu.label == \'open_cashbox\' && menu.col != 1 && ((config.page.cash == \'yes\') && menu.config != \'no\')"> \
                <md-button md-no-ink="true" class="md-grid-button md-raised" ng-click="menu.func();">{{::ui[menu.label]}}</md-button> \
              </div> \
              \
              <!--<div class="col s10-10 no-padding" ng-repeat="menu in mainMenuList" ng-if="menu.label != \'main_page\' && menu.col == 1 && ((menu.config == true  config.page.cash == \'yes\') && menu.config != \'no\')">--> \
                <div class="col s10-10 no-padding" ng-repeat="menu in mainMenuList" ng-if="menu.label != \'main_page\' && menu.col == 1 && menu.config == true">\
                <md-button md-no-ink="true" class="md-grid-button md-raised" ng-click="menu.func();">{{::ui[menu.label]}}</md-button> \
              </div> \
              \
            </div> \
          </div> \
      ');
    }]);

    angular.module('tmpl/userScene.html', []).run(["$templateCache", function ($templateCache) {
        $templateCache.put('tmpl/userScene.html',
          '<div id="user-menu" class="menu-area"> \
          <!-- sidebar column --> \
                <table class="fix-head user-management-table data-table style1 tablesorter group-table userManagementTable" ng-show="currentMode == \'list\'"> \
                    <caption>{{::ui[\'user_list\']}} <a class="btn" ng-click="editUser(\'\')"><i class="md-icon" style="vertical-align: top;">note_add</i>{{::ui[\'add_user\']}}</a></caption><thead>\
                    <tr><th>{{::ui[\'user_no\']}}</th><th>{{::ui[\'user_name\']}}</th><th>{{::ui[\'user_group\']}}</th><th>&nbsp;</<th></tr></thead>\
                    <tbody><tr ng-repeat="staff in Staffs"><td>{{staff.username || \'&nbsp;\'}}</td><td>{{staff.name || \'&nbsp;\'}}</td><td>{{staff.staffGroupName || \'&nbsp;\'}}</td><td class="action-container"><a href="#" ng-click="editUser(\'{{staff.username}}\')"><i class="md-icon">mode_edit</i>{{::ui[\'edit\']}}</a><a href="#" ng-click="deleteUser(\'{{staff.username}}\')"><i class="md-icon">close</i>{{::ui[\'delete\']}}</a></td></tr></tbody>\
                </table>\
                <div class="user-edit-container" ng-show="currentMode == \'user\'" style="width:100%">\
                    <div>{{ui[\'user_management\']}}</div>\
                    <md-input-container class="" flex="30"> \
                        <label>{{ui[\'user_no\'] || "員工編號"}}</label> \
                        <input autofocus ng-model="currentUser.username" ng-class="{\'focused\' : currentFieldName == \'name\'}" ng-trim="false" type="text" ng-click="focusField(\'name\')"> \
                        </md-input-container> \
                        <md-input-container class="" flex="30"> \
                        <label>{{ui[\'user_name\'] || "員工名稱"}}</label> \
                        <input type="text" ng-model="currentUser.name" ng-class="{\'focused\' : currentFieldName == \'value\'}" ng-trim="false" ng-click="focusField(\'value\')"> \
                        </md-input-container> \
                        <md-input-container class="" flex="30"> \
                        <label>{{ui[\'user_password\'] || "員工密碼"}}</label> \
                        <input type="text" ng-model="currentUser.password" ng-class="{\'focused\' : currentFieldName == \'value\'}" ng-trim="false" ng-click="focusField(\'value\')"> \
                        </md-input-container> \
                        \
                        \
                        <!-- angular example -->\
                        <!-- <select ng-model="mySelectedValue" \
                          ng-options="myValue for myValue in someDynamicArrayOfValues" \
                          ng-change="myFunctionForDeterminingWhetherValueIsSelected()"> \
                          <option value="">--</option> \
                        </select> --> \
                        \
                        \
                        <md-input-container flex="30">\
                          <label>{{ui[\'user_group\'] || "員工組別"}}</label>\
                          <md-select ng-model="currentUser.staffGroupCode">\
                            <md-option ng-repeat="type in groupData" value="{{type.staffGroupCode}}">\
                              {{type.staffGroupName}}\
                            </md-option>\
                          </md-select>\
                        </md-input-container>\
                        <!-- <div class="material-select input-field col s12">\
                            <select ng-model="currentUser.staffGroupCode">\
                              <option value="" disabled selected>{{ui[\'please_select\']}}</option>\
                              <option value="1">Option 1</option>\
                              <option value="2">Option 1</option>\
                              <option value="3">Option 1</option>\
                            </select>\
                            <label>{{ui[\'user_group\'] || "員工組別"}}</label>\
                          </div> -->\
                        <!-- <md-input-container class="login" flex="30"> \
                        <label>{{ui[usercode] || "員工組別"}}</label> \
                        <input type="text" ng-model="currentUser.staffGroupName" ng-class="{\'focused\' : currentFieldName == \'value\'}" ng-trim="false" ng-click="focusField(\'value\')"> \
                        </md-input-container> --> \
                        <md-button md-no-ink="true" class="md-raised submit" ng-click="submitUser()">{{::ui["done"] || "完成"}}</md-button> \
          </div> \
      </div><!-- report-menu -->');
    }]);

    angular.module('tmpl/userGroupScene.html', []).run(["$templateCache", function ($templateCache) {
        $templateCache.put('tmpl/userGroupScene.html', '\
      <div id="user-group" class="menu-area auto-scroll"> \
        <div class="list-container">\
        </div>\
        <div class="edit-container" ng-show="currentGroup">\
            <!-- <h2 class="md-title">{{ui[\'user_group_management\']}}</h2>-->\
            <div layout="row" style="margin-top: 10px;">\
              <div flex="50">\
                <md-input-container class="" flex="30"> \
                  <label for="user-group-code">{{ui[\'user_group_code\']}}</label> \
                  <input autofocus ng-model="currentGroup.staffGroupCode" ng-class="{\'focused\' : currentFieldName == \'name\'}" ng-trim="false" type="text" ng-click="focusField(\'name\')"> \
                </md-input-container> \
                <md-input-container class="" flex="30"> \
                  <label>{{ui[\'user_group_name\']}}</label> \
                  <input type="text" ng-model="currentGroup.staffGroupName" ng-class="{\'focused\' : currentFieldName == \'value\'}" ng-trim="false" ng-click="focusField(\'value\')"> \
                </md-input-container> \
              </div>\
                \
              <div flex="50" class="option-container">\
                <div class="custom-subhead">{{ui[\'user_permission\']}}</div>\
                <div class="checkbox-container">\
                  <div class="mcss-checkbox all"> \
                      <input type="checkbox" id="all" class="filled-in" ng-click="checkAll()" ng-model="checkAllStatus"/> \
                      <label for="all">{{::ui[\'all_permissions\']}}</label> \
                  </div> \
                  <!--<div class="mcss-checkbox unselect-all"> \
                      <input type="checkbox" id="unselect-all" class="filled-in" ng-click="uncheckAll()"/> \
                      <label for="unselect-all">{{::ui[\'unselect_all\']}}</label> \
                  </div>--> \
                  <hr class="custom-hr" style="width: 100%"/>\
                  <div class="mcss-checkbox" ng-repeat="(key, value) in ::permissionList" ng-class="{\'all\': key == \'all\'}" ng-init="currentPermissionList[key] = false"> \
                      <input type="checkbox" id="{{key}}" class="filled-in" name="key" ng-model="currentGroup[key]" ng-click="checkbox(key)" ng-init="false"/> \
                      <label for="{{key}}">{{key}}</label><span class="permission {{key}}" ng-click="highlightMe(key, $event)">{{::ui[value]}}</span> \
                  </div> \
                </div>\
              </div>\
            </div>\
            \
            <md-button md-no-ink="true" class="cancel md-raised submit" ng-click="cancel()">{{::ui["cancel_back"] || "取消/返回"}}</md-button> \
            <md-button md-no-ink="true" class="proceed md-raised submit" ng-click="submit()">{{::ui["done"] || "完成"}}</md-button> \
        </div>\
      </div>\
      ');
    }]);

    angular.module('tmpl/userClockScene.html', []).run(["$templateCache", function ($templateCache) {
        $templateCache.put('tmpl/userClockScene.html', '\
      <div class="menu-container" layout="column" ng-controller="userClockController as userClock"> \
        <h1>{{::ui[\'please_select\']}}</h1>\
          <div class="row"> \
            <div class="col s10-5"> \
              <md-button md-no-ink="true" class="md-raised" ng-click="clock($event, \'in\')">{{::ui[\'clock_in\']}}</md-button> \
            </div> \
            <div class="col s10-5"> \
              <md-button md-no-ink="true" class="md-raised" ng-click="clock($event, \'out\')">{{::ui[\'clock_out\']}}</md-button> \
            </div> \
            <div class="col s10-10"> \
              <md-button md-no-ink="true" class="cancel md-raised submit" ng-click="cancel()">{{::ui["cancel_back"] || "取消/返回"}}</md-button> \ \
            </div> \
          </div> \
        </div> \
      ');
    }]);

    angular.module('tmpl/tableListScene.html', []).run(["$templateCache", function ($templateCache) {
        $templateCache.put('tmpl/tableListScene.html', '\
      <div class="menu-container materialize-keypad col-4x4" layout="column" style="height: 85%;"> \
          <div class="row table-list" style=""> \
            <div class="col s3"> \
              <div class="btn md-grid-button">\
                <h3 class="table-no">3</h3>\
                <span class="no-of-people">人數：4</span>\
                <div class="time-container">\
                  <div class="start">開始：16:44:00</div>\
                  <div class="count-down">剩餘：1h 33m</div>\
                </div>\
                <span class="staff">(小肥)</span>\
              </div>\
            </div> \
            <pager-custom icon-size="\'dp56\'" template="tmpl/pager-default-materialize.html" total-items="MainService.food.kitchenmsg.length" items-per-page="optionPerPage" ng-model="currentPage"></pager-custom> \
          </div> \
      </div> \
          <div class="materialize-keypad col-1x5" style="height: 15%"> \
            <div class="row" style="margin-bottom: 0;"> \
              <div class="col s10-2 dummy-grid"><span></span></div> \
              <div class="col s10-2 dummy-grid"><span></span></div> \
              <div class="col s10-2 dummy-grid"><span></span></div> \
              <div class="col s10-2 dummy-grid"><span></span></div> \
              <div class="col s10-2"><button class="proceed btn md-button md-grid-button" ng-click="returnToOrder();">{{::ui[\'done\']}}</button></div> \
            </div><!-- row --> \
          </div> \
      ');
    }]);

    angular.module('tmpl/reportScene.html', []).run(["$templateCache", function ($templateCache) {
        $templateCache.put('tmpl/reportScene.html',
          '<div id="report-menu" class="menu-area" layout="row" ng-controller="reportController as report"> \
          <!-- sidebar column --> \
          \
            <div flex="45" class="report-content left-area" ng-hide="currentReport==\'\'"> \
              <!-- <div ng-show="currentReport==\'\'">{{::ui[\'please_select_report\']}}</div> --> \
              <ng-include include-replace src="\'tmpl/report_template_daily.html\'" ng-if="currentReport==\'daily_report\'"></ng-include> \
                <ng-include include-replace src="\'tmpl/report_template_daily_watch_service.html\'" ng-if="currentReport==\'daily_watch_service_report\'"></ng-include> \
                <ng-include include-replace src="\'tmpl/report_template_daily_void_ECR.html\'" ng-if="currentReport==\'daily_void_ECR_report\'"></ng-include> \
                <ng-include include-replace src="\'tmpl/report_template_clearance.html\'" ng-if="currentReport==\'daily_Clearance_Report\'"></ng-include> \
                <ng-include include-replace src="\'tmpl/report_template_daily.html\'" ng-if="currentReport==\'daily_report_eat\'"></ng-include> \                <ng-include include-replace src="\'tmpl/report_template_daily.html\'" ng-if="currentReport==\'daily_report_take_away\'"></ng-include> \
                <ng-include include-replace src="\'tmpl/report_template_daily.html\'" ng-if="currentReport==\'daily_Report_Delivery\'"></ng-include> \
              <ng-include include-replace src="\'tmpl/report_template_sales.html\'" ng-if="currentReport==\'sales_report\'"></ng-include> \
              <ng-include include-replace src="\'tmpl/report_template_attendance.html\'" ng-if="currentReport==\'daily_attendance\'"></ng-include> \
               <ng-include include-replace src="\'tmpl/report_template_food_timeslot.html\'" ng-if="currentReport==\'daily_food_timeslot_report\'"></ng-include> \
                <ng-include include-replace src="\'tmpl/report_template_daily_void_bill.html\'" ng-if="currentReport==\'daily_report_void_bill\'"></ng-include> \
                <ng-include include-replace src="\'tmpl/report_template_daily_cancel_item.html\'" ng-if="currentReport==\'daily_cancel_item\'"></ng-include> \
                 <ng-include include-replace src="\'tmpl/report_template_daily_net_sales.html\'" ng-if="currentReport==\'daily_net_sales\'"></ng-include> \
                 <ng-include include-replace src="\'tmpl/report_template_daily_discount.html\'" ng-if="currentReport==\'daily_discount\'"></ng-include> \
                 <ng-include include-replace src="\'tmpl/report_template_daily_MostSales_food.html\'" ng-if="currentReport==\'daily_most_sales_food\'"></ng-include> \
                 <ng-include include-replace src="\'tmpl/report_template_daily_MostSales_Qty_food.html\'" ng-if="currentReport==\'daily_most_sales_qty_food\'"></ng-include> \
                 <ng-include include-replace src="\'tmpl/report_template_daily_payment_detail.html\'" ng-if="currentReport==\'daily_payment_detail\'"></ng-include> \
                 <ng-include include-replace src="\'tmpl/report_template_daily_category_sales.html\'" ng-if="currentReport==\'daily_category_sales\'"></ng-include> \
                 <ng-include include-replace src="\'tmpl/report_template_daily_per_hour_sales.html\'" ng-if="currentReport==\'daily_per_hour_sales\'"></ng-include> \
                 <ng-include include-replace src="\'tmpl/report_template_daily_item_report.html\'" ng-if="currentReport==\'daily_item_report\'"></ng-include> \
                 <ng-include include-replace src="\'tmpl/report_template_daily.html\'" ng-if="currentReport==\'daily_report_cloud\'"></ng-include> \
            </div> \
            \
            <div id="report-option" flex class="right-area md-grid-container" style="position: relative; width: 100%; height: 100%;"> \
                <h2 class="header left-edge-margin fill-height ng-binding" style="">{{::ui[\'report\']}}<div class="remark"><span ng-if="selectedDateRange.length > 1">{{::ui[\'selected_period\'] + ui[\'colon\']}}</span><span ng-if="selectedDateRange.length == 1">{{::ui[\'selected_day\'] + ui[\'colon\']}}</span>{{dateFrom}}<span ng-if="selectedDateRange.length > 1"> {{::ui[\'to\']}} </span>{{dateTo}}</div></h2> \
                <div class="materialize-keypad col-5x5" style="height: 82%"> \
                  <div class="row" style="margin-bottom: 0;"> \
                    <div class="col s10-2" ng-repeat="option in ::report.reportOption | startFrom:(currentPage - 1) * reportOptionPerPage | limitTo: reportOptionPerPage"><button class="btn md-button md-grid-button" ng-click="changeReport(\'{{::option.name}}\')">{{::ui[option.name]}}</button></div> \
                     \
                    <div class="col s10-2 dummy-grid" ng-if="noOfReportOptionPage - currentPage == 0" ng-repeat="i in MainService.getPageNumber( reportOptionPerPage - report.reportOption.length % reportOptionPerPage ) track by $index"><span></span></div> \
                    \
                    <!-- temp measure -->\
                    <div class="col s10-2 dummy-grid" ng-if="noOfReportOptionPage - currentPage == 0"><span></span></div> \
                    <div class="col s10-2 dummy-grid" ng-if="noOfReportOptionPage - currentPage == 0"><span></span></div> \
                    <!--<pager-custom icon-size="\'dp56\'" template="tmpl/pager-default-materialize.html" total-items="report.reportOption.length" items-per-page="reportOptionPerPage" ng-model="currentPage"></pager-custom>--> \
                  </div><!-- row --> \
                </div> \
                  <div class="materialize-keypad col-1x5" style="height: 12%"> \
                    <div class="row" style="margin-bottom: 0;"> \
                      <!-- <div class="col s3 dummy-grid"><span></span></div> --> \
                      <div class="col s10-2"><button class="btn md-button md-grid-button" ng-click="todayReport()">{{::ui[\'current_report_data\']}}</button></div> \
                      <div class="col s10-2"><button class="btn md-button md-grid-button" ng-click="callCalendar(dialogCtrl)">{{::ui[\'past_report\']}}</button></div> \
                      <div class="col s10-2 dummy-grid"><span ng-show="!isShowPrint();"></span><button class="btn md-button md-grid-button btn-printReport" ng-click="printReport()" ng-show="isShowPrint();">{{::ui[\'print_report\']}}</button></div> \
                      <!--<div class="col s10-2"><button class="proceed2 btn md-button md-grid-button" ng-click="dailyClearance()">{{::ui[\'daily_clearance\']}}</button></div>--> \
                      <div class="col s10-2 dummy-grid"><span></span></div> \
                      <div class="col s10-2"><button class="proceed btn md-button md-grid-button" ng-click="MainService.mode = MainService.schema.mode.main">{{::ui[\'done\']}}</button></div> \
                    </div><!-- row --> \
                  </div> \
            </div> \
      </div><!-- report-menu -->');
    }]);
    
    angular.module('tmpl/compositeItem.html', []).run(["$templateCache", function ($templateCache) {
        $templateCache.put('tmpl/compositeItem.html',
          '<div id="kitchen-message-menu" class="menu-area compositeItem" layout="row" ng-controller="compositeItemController" ng-show="modeCheckOrderItem(\'compositeItem\')"> \
          <div id="kitchen-message-option" flex class="right-area md-grid-container" style="position: relative; width: 100%; height: 100%;"> \
              <h2 class="header fill-height ng-binding" style="">{{::ui.txt_select_compositem_page_title}}</h2> \
              <div class="md-custom-grid col-5x5" style="height: 84%"> \
                <div class="row" style="margin-bottom: 0;"> \
                  <!--<div class="col s10-2" ng-repeat="option in MainService.food.kitchenmsg | startFrom:(currentPage - 1) * optionPerPage | limitTo: optionPerPage"><button class="btn md-button md-grid-button" ng-click="MainService.Cart.addKitchenMsg(option, $event)">{{::option.label}}</button></div>--> \
                    \
                  <div class="col s10-2" ng-repeat="item in MainService.combo.selectMode.itemList | startFrom:(currentPage - 1) * optionPerPage | limitTo: optionPerPage">\
                        <!-- food control -->\
                        <button class="md-raised md-button md-grid-button {{::item.color}}" ng-click="changeItem(item, item.stock);" ng-class="{suspend: item.stock != undefined && (item.stock < 0 || item.stock === 0)}">\
                        \<!--<span class="badge -->\
                             <span class="flex-center badge suspend" ng-if="item.stock != undefined && (item.stock === -3)">{{::ui["txt_preparing"]}}</span> \
                             <span class="flex-center badge suspend" ng-if="item.stock != undefined && (item.stock === -1 || item.stock === 0)">{{::ui["txt_suspend"]}}</span> \
                             <div class="name">{{::MainService.fn.shorten(item.name[INFO.lang])}}</div> \
                           \
                             <span class="badge stock" ng-if="item.stock != undefined && item.stock > 0">Q: {{item.stock}}</span> \
                             <small class="secondary"> \
                                 <span class="code" ng-show="modeCheckOrderItemDetail()">{{::item.code}}</span> \
                                 <span class="code" ng-show="modeCheckOrderItemDetail(\'printer\')">{{::item.printer}}</span> \
                             </small> \
                             <div class="price">{{ ::item.unitprice / 100 | currency :INFO.currency :1 }}</div> \
                   </button></div> \
                    \
                   \
                  <div class="col s10-2 dummy-grid" ng-if="noOfOptionPage == currentPage" ng-repeat="i in reminderPage track by $index"><span></span></div> \
                  \
                  <!-- temp measure -->\
                  <pager-custom icon-size="\'dp56\'" template="tmpl/pager-default-materialize.html" total-items="MainService.food.kitchenmsg.length" items-per-page="optionPerPage" ng-model="currentPage"></pager-custom> \
                </div><!-- row --> \
              </div> \
                <div class="materialize-keypad col-1x5" style="height: 10%"> \
                  <div class="row" style="margin-bottom: 0; width 75%"> \
                    <div class="col s10-2"><button class="cancel md-button md-grid-button icon-with-text" ng-click="delete($event);"><i class="md-icon dp45">clear</i>{{::ui[\'delete\']}}</button></div>\
                    <div class="col s10-2 dummy-grid"><span></span></div>\
                    <div class="col s10-2 dummy-grid"><span></span></div>\
                    <div class="col s10-2 dummy-grid"><span></span></div>\
                    <div class="col s10-2"><button class="proceed md-button md-grid-button icon-with-text" ng-click="cancel();"><i class="md-icon dp45">done</i>{{::ui[\'done\']}}</button></div>\
                  </div><!-- row --> \
                </div> \
          </div> \
      </div><!-- report-menu -->');
    }]);

    angular.module('tmpl/kitchenMessage.html', []).run(["$templateCache", function ($templateCache) {
        $templateCache.put('tmpl/kitchenMessage.html',
          '<div id="kitchen-message-menu" class="menu-area" layout="row" ng-controller="kitchenController as kitchen"> \
          <div id="kitchen-message-option" flex class="right-area md-grid-container" style="position: relative; width: 100%; height: 100%;"> \
              <h2 class="header fill-height ng-binding" style="">{{::ui[\'select_message\']}}</h2> \
              <div class="kitchen-message materialize-keypad col-5x5" style="height: 84%"> \
                <div class="row" style="margin-bottom: 0;"> \
                  <div class="col s10-2" ng-repeat="option in MainService.food.kitchenmsg | startFrom:(currentPage - 1) * optionPerPage | limitTo: optionPerPage"><button class="btn md-button md-grid-button" ng-click="MainService.Cart.addKitchenMsg(option, $event)">{{::option.label}}</button></div> \
                   \
                  <div class="col s10-2 dummy-grid" ng-if="noOfOptionPage == currentPage" ng-repeat="i in reminderPage track by $index"><span></span></div> \
                  \
                  <!-- temp measure -->\
                  <pager-custom icon-size="\'dp56\'" template="tmpl/pager-default-materialize.html" total-items="MainService.food.kitchenmsg.length" items-per-page="optionPerPage" ng-model="currentPage"></pager-custom> \
                </div><!-- row --> \
              </div> \
                <div class="materialize-keypad col-1x5" style="height: 10%"> \
                  <div class="row" style="margin-bottom: 0; width 75%"> \
                    <!-- <div class="col s3 dummy-grid"><span></span></div> --> \
                    <div class="col s10-2"><button class="btn md-button md-grid-button" ng-click="toggleAll()">{{::ui[\'select_all\']}}</button></div> \
                    <div class="col s10-2"><button class="btn md-button md-grid-button" ng-click="MainService.Cart.clearKitchenMsg()">{{::ui[\'clear_message\']}}</button></div> \
                    \
                    <div class="col s10-2 dummy-grid"><span></span></div> \
                    \
                    <!--<div class="col s10-2 dummy-grid" ng-show="modeCheckOrder(\'specialMessage\')"><span></span></div>--> \
                    \
                    <div class="col s10-2" ng-show="modeCheckFloor(\'kitchenMessage\') || modeCheckOrder(\'specialMessage\')"><button class="cancel md-button md-grid-button icon-with-text" ng-click="cancel()"><i class="md-icon dp45">clear</i>{{::ui[\'cancel_back\']}}</button></div>\
                    <div class="col s10-2" ng-if="!MainService.checkIsNotTakeAwayMode()"><button class="proceed md-button md-grid-button icon-with-text" ng-click="MainService.Cart.cancelOrder2();"><i class="md-icon dp45">done</i>{{::ui[\'done\']}}</button></div>\
					<div class="col s10-2" ng-if="MainService.checkIsNotTakeAwayMode()"><button class="proceed md-button md-grid-button icon-with-text" ng-click="MainService.submitCart()"><i class="md-icon dp45">done</i>{{::ui[\'done\']}}</button></div>\
                  </div><!-- row --> \
                </div> \
          </div> \
      </div><!-- report-menu -->');
    }]);

    angular.module('tmpl/clearanceScene.html', []).run(["$templateCache", function ($templateCache) {
        $templateCache.put('tmpl/clearanceScene.html',
          '<div id="report-menu" class="menu-area" layout="row" ng-controller="clearanceController as clearance"> \
          <!-- sidebar column --> \
          \
            <div flex="45" class="report-content left-area" ng-hide="currentReport==\'\'"> \
              <ng-include include-replace src="\'tmpl/report_template_daily.html\'" ng-if="currentReport==\'daily_report\'"></ng-include> \
                <ng-include include-replace src="\'tmpl/report_template_daily_void_ECR.html\'" ng-if="config.ecrPayment && currentReport==\'daily_report\'"></ng-include> \
              <ng-include include-replace src="\'tmpl/report_template_sales.html\'" ng-if="currentReport==\'sales_report\'"></ng-include> \
            </div> \
            \
            <div id="report-option" flex class="right-area md-grid-container" style="position: relative; width: 100%; height: 100%;"> \
                <h2 class="header left-edge-margin fill-height ng-binding" style="">{{::ui[\'report\']}}<div class="remark"><span ng-if="selectedDateRange.length > 1">{{::ui[\'selected_period\'] + ui[\'colon\']}}</span><span ng-if="selectedDateRange.length == 1">{{::ui[\'selected_day\'] + ui[\'colon\']}}</span>{{dateFrom}}<span ng-if="selectedDateRange.length > 1"> {{::ui[\'to\']}} </span>{{dateTo}}</div></h2> \
                <div class="materialize-keypad col-2x5" style="height: 82%"> \
                  <div class="row" style="margin-bottom: 0;"> \
                    <div class="col s10-5"><button class="proceed2 btn md-button md-grid-button" ng-click="dailyClearance()">{{::ui[\'daily_clearance\']}}</button></div> \
                     \
                    <div class="col s10-5 dummy-grid" ng-repeat="i in ::$parent.numberToArray( 3 ) track by $index"><span></span></div> \
                  </div><!-- row --> \
                </div> \
                  <div class="materialize-keypad col-1x5" style="height: 12%"> \
                    <div class="row" style="margin-bottom: 0;"> \
                      <div class="col s12"><button class="proceed btn md-button md-grid-button" ng-click="MainService.mode = MainService.schema.mode.main">{{::ui[\'done\']}}</button></div> \
                    </div><!-- row --> \
                  </div> \
            </div> \
      </div><!-- report-menu -->');
    }]);

    angular.module('tmpl/calendar.html', []).run(["$templateCache", function ($templateCache) {
        $templateCache.put('tmpl/calendar.html',
          '<md-dialog aria-label="calendar" class="calendar"> \
            <md-dialog-content class="sticky-container calendar-html"> \
                <div class="clearfix calendar-container" ng-class="{ \'extend-w\' : selectedDateRange.length > 0, \'extend-w\' : selectedDateRange.length > 0, \'extend-h\' : (ctrl.preDays.length + ctrl.days.length + ctrl.postDays.length) > 35}"> \
                  <calendar></calendar> \
                  <section class="date-range" ng-show="selectedDateRange.length > 0" ng-class="{\'show\' : selectedDateRange.length > 0}"> \
                      <h5 style="display: inline;"><span ng-if="selectedDateRange.length > 1">{{::ui[\'selected_period\']}}</span><span ng-show="selectedDateRange.length == 1">{{::ui[\'selected_day\']}}</</span></h5>\
                      <div class="date from flex-center"><span ng-show="selectedDateRange.length > 1">由：</span>{{dateFrom}}</div> \
                      <div class="date to flex-center" ng-show="selectedDateRange.length > 1"><span>至：</span>{{dateTo}}</div> \
                      <div class="materialize-keypad" style="height: 50px"> \
                        <div class="row" style="height: 100%"> \
                          <div class="col s10-10" style="height: 100%;"><button class="proceed btn md-button md-grid-button" ng-click="setReport()">{{::ui[\'done\']}}</button></div> \
                        </div><!-- row --> \
                      </div> \
                  </section> \
                </div> \
            </md-dialog-content> \
        </md-dialog>');
    }]);

    angular.module("tmpl/settings.html", []).run(["$templateCache", function ($templateCache) {
        $templateCache.put("tmpl/settings.html",
          '<div id="settings-page" class="friendly menu-area auto-scroll"> \
        <div flex="50" class="option-container">\
        <!-- {{ui[\'user_permission\']}} -->\
          <div class="custom-subhead">{{ui[\'settings\']}}</div>\
          <div class="checkbox-container">\
            <div class="mcss-checkbox all"> \
                <input type="checkbox" id="bill-print-twice" class="filled-in" ng-click="toggle(\'billPrintTwice\')" ng-model="settings.billPrintTwice"/> \
                <label for="bill-print-twice">{{::ui[\'bill_print_twice\']}}</label> \
            </div> \
            <div class="mcss-checkbox all"> \
                <input type="checkbox" id="payment-print-twice" class="filled-in" ng-click="toggle(\'paymentPrintTwice\')" ng-model="settings.paymentPrintTwice"/> \
                <label for="payment-print-twice">{{::ui[\'payment_print_twice\']}}</label> \
            </div> \
            <div class="mcss-checkbox all"> \
                <input type="checkbox" id="use-kds" class="filled-in" ng-click="toggle(\'useKds\')" ng-model="settings.useKds"/> \
                <label for="use-kds">{{::ui[\'use_kds\']}}</label> \
            </div> \
            <div class="mcss-checkbox all"> \
                <input type="checkbox" id="takeAwayPrintTwice" class="filled-in" ng-click="toggle(\'takeAwayPrintTwice\')" ng-model="settings.takeAwayPrintTwice"/> \
                <label for="takeAwayPrintTwice">{{::ui[\'txt_takeaway_print_twice\']}}</label> \
            </div> \
            <!--<div class="mcss-checkbox all"> \
                <input type="checkbox" id="takeAwayMode" class="filled-in" ng-click="toggle(\'takeAwayMode\')" ng-model="settings.takeAwayMode"/> \
                <label for="takeAwayMode">{{::ui[\'use_takeAwayMode\']}}</label> \
            </div> -->\
          </div>\
        </div>\
        \
        <md-button md-no-ink="true" class="cancel md-raised submit" ng-click="cancel()">{{::ui["cancel_back"] || "取消/返回"}}</md-button> \
        <md-button md-no-ink="true" class="proceed md-raised submit" ng-click="submit()">{{::ui["done"] || "完成"}}</md-button> \
      </div>\
      ');
    }]);
})();
