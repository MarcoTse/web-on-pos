(function () {
    var webOn = {};
    webOn.RARE = {
        version: 0.12, // where 12 > 8 based on months in 2015
        update: 20151207
    }

    var app = angular.module('rms', ['ngTouch', 'ngMessages', 'ngMaterial', 'ui.bootstrap', 'rms.controllers', 'rms.directives', 'common.services', 'takeOutAndDeliveryModule', 'depositModule', 'multiPaymentModule', 'tableViewModuel']).run(function (MainService) {
        MainService.promise;
    })
    // .provider('myProvider', function ($log ) {
    //     var $log =  angular.injector(['ng']).get('$log');
    //     $log.log("Aloha!"); 

    //     this.$get = function($log) {
    //         $log.log("Hello!"); // Everything is ok here
    //     };
    // });
    app.config(function ($mdThemingProvider, $locationProvider, $provide, $httpProvider, $logProvider) {
        $locationProvider.html5Mode({ enabled: true, requireBase: false, rewriteLinks: true });

        $provide.constant('INFO', INFO);
        $provide.constant('uiLanguage', language);
        $provide.constant('keyMap', keyMap);
        $provide.constant('userConfig', userConfig);
        $httpProvider.useApplyAsync(true); // put all http request into one digest cycle for speed optimization
        $logProvider.debugEnabled(true);
    });

    app.filter('startFrom', function () {
        return function (input, start) {
            start = +start; //parse to int
            if (input == undefined) return 0;
            return input.slice(start);
        }
    })
        //take all whitespace out of string
        .filter('nospace', function () {
            return function (value) {
                return (!value) ? '' : value.replace(/ /g, '');
            };
        })
        //replace uppercase to regular case
        .filter('humanizeDoc', function () {
            return function (doc) {
                if (!doc) return;
                if (doc.type === 'directive') {
                    return doc.name.replace(/([A-Z])/g, function ($1) {
                        return '-' + $1.toLowerCase();
                    });
                }

                return doc.label || doc.name;
            };
        });

    app.factory('SocketFactory', function ($rootScope, $timeout, INFO) {
        console.debug(INFO.socketPort);
        var socketConnection = 'http://' + location.hostname + ':' + INFO.socketPort;
        var socket = io.connect(socketConnection);
        io.Manager(socketConnection, { reconnect: true });
        var asyncAngularify = function (socket, callback) {
            return callback ? function () {
                var args = arguments;
                // console.log('arguments******************************');
                // console.log(arguments);
                // console.log('socket******************************');
                // console.log(socket);
                // console.log('callback******************************');
                // console.log(callback);

                $timeout(function () {
                    callback.apply(socket, args);
                }, 0);
            } : angular.noop;
        };
        return {
            on: function (eventName, callback) {
                //console.log(eventName);
                socket.on(eventName, function () {
                    var args = arguments;
                    $rootScope.$apply(function () {
                        callback.apply(socket, args);
                    });
                });
                /*
                socket.io.on("connect_error", function (e) {
                    console.log('socket on connect_error');
                })
                socket.on('connect_failed', function (e) {
                    console.log('socket on connect_failed');
                    console.log(e);
                });
                socket.on('disconnect', function (e) {
                    console.log('socket on disconnect');
                    console.log(e);
                });
                socket.on('error', function (e) {
                    console.log('socket on error');
                    console.log(e);
                });
                */
            },
            emit: function (eventName, data, callback) {
                //console.log(eventName);
                var lastIndex = arguments.length - 1;
                var callback = arguments[lastIndex];
                if (typeof callback == 'function') {
                    callback = asyncAngularify(socket, callback);
                    arguments[lastIndex] = callback;
                }
                return socket.emit.apply(socket, arguments);
            }
        };
    });

    app.factory('MainService', function (SocketFactory, INFO, $http, $q, $mdDialog, $timeout, $filter, uiLanguage, userConfig, $rootScope, $compile, depositManager) {


        SocketFactory.on('refreshItemQty', function (data) {
            console.log('refreshItemQty done');
            console.log(data);
            obj.stock.updateItem(JSON.parse(data));
            //initTableScene();
            // showMessage("#status-message", scope.RMSTable.messages.table_refresh, "", "warn", 30000);
        });

        SocketFactory.on('activeLockTable', function (tableCode) {
            obj.lockTable.activeLockTable(["L" + tableCode]);
        });

        SocketFactory.on('releaseLockTable', function (tableCode) {
            obj.lockTable.releaseLockTable("L" + tableCode);
        });

        SocketFactory.on('checkIsLockTable', function (d) {
            console.debug('checking lock table');
            if (MainService.mode != MainService.schema.mode.order && obj.lockTable.tableNum) {
                if (obj.Cart.tableNo == d)
                    obj.lockTable.toReleaseLockTable(obj.lockTable.tableNum);
            }
        });

        SocketFactory.on('refreshCategory', function (data) {
            data = JSON.parse(data)
            console.log('refreshCategory', data);
            switch (data.filename) {
                case 'takeaway-category':
                    //otherWriteOrderService.category[obj.schema.modeOtherWriteOrderService.takeAway] = data.category;
                    for (key in otherWriteOrderService.category) {
                        otherWriteOrderService.category[key] = data.category;
                    }
                    otherWriteOrderService.category.forEach(function (c) {
                        c = data.category;
                    })
                    break;
                case 'delivery-category':
                    obj.deliveryOrderService.Category = data.category;
                    break;
                case 'eat-category':
                    obj.eat.Category = data.category;
                    break;
            }

        });


        var charArray = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".split('');
        var obj = {};


        SocketFactory.emit("getClientIp", function (result) {
            obj.clientIp = result;
        })

        obj.deposit = depositManager;

        obj.label = [];
        obj.label = uiLanguage[INFO.lang];
        obj.schema = {};
        obj.schema.mode = {};
        obj.schema.mode.floorPlan = 0;
        obj.schema.mode.order = 1;
        obj.schema.mode.main = 2;
        obj.schema.mode.report = 3;
        obj.schema.mode.login = 4;
        obj.schema.mode.userManagement = 5;
        obj.schema.mode.dailyClearance = 6;
        obj.schema.mode.cashManagement = 7;
        obj.schema.mode.userGroupManagement = 8;
        obj.schema.mode.userClockManagement = 9;
        obj.schema.mode.tableList = 10;
        obj.schema.mode.settings = 11;
        obj.schema.mode.takeAway = 12;
        obj.schema.mode.scanQrcode = 13;
        obj.schema.mode.home = 14;
        obj.schema.mode.deliveryOrder = 15;
        obj.schema.mode.categorySetting = 16;
        obj.schema.mode.fastFood = 17;
        obj.schema.mode.buildBillFromVoid = 18;
        obj.schema.mode.deposit = 19;

        obj.schema.modeMain = {};
        obj.schema.modeMain.normal = 0;
        obj.schema.modeMain.cashbox = 0;
        obj.schema.modeMain.watch = 1;

        obj.schema.modeUserGroup = {};
        obj.schema.modeUserGroup.list = 0;
        obj.schema.modeUserGroup.detail = 1;
        obj.schema.modeCash = {};
        obj.schema.modeCash.normal = 0;
        obj.schema.modeCash.open = 1;
        obj.schema.modeCash.petty = 2;


        obj.schema.modeOtherWriteOrderService = {};
        obj.schema.modeOtherWriteOrderService.takeAway = 0;
        obj.schema.modeOtherWriteOrderService.fastFood = 1;
        obj.schema.modeOtherWriteOrderService.deliveryOrder = 2;
        obj.schema.modeOtherWriteOrderService.buildBillFromVoid = 3;

        // Build Bill mode function mode
        obj.schema.modeBuildBillFromVoid = {};
        obj.schema.modeBuildBillFromVoid.normal = 0;
        obj.schema.modeBuildBillFromVoid.order = 1;
        obj.schema.modeBuildBillFromVoid.printBill = 2;
        obj.schema.modeBuildBillFromVoid.voidBill = 3;
        obj.schema.modeBuildBillFromVoid.searchBill = 4;
        obj.schema.modeBuildBillFromVoid.writeOrder = 5;
        obj.schema.modeBuildBillFromVoid.amendBill = 6;
        // fast food mode function mode
        obj.schema.modeFastFood = {};
        obj.schema.modeFastFood.normal = 0;
        obj.schema.modeFastFood.order = 1;
        obj.schema.modeFastFood.printBill = 2;
        obj.schema.modeFastFood.voidBill = 3;
        obj.schema.modeFastFood.searchBill = 4;
        obj.schema.modeFastFood.writeOrder = 5;
        obj.schema.modeFastFood.amendBill = 6;
        // take away mode function mode
        obj.schema.modeTakeAway = {};
        obj.schema.modeTakeAway.normal = 0;
        obj.schema.modeTakeAway.order = 1;
        obj.schema.modeTakeAway.printBill = 2;
        obj.schema.modeTakeAway.voidBill = 3;
        obj.schema.modeTakeAway.searchBill = 4;
        obj.schema.modeTakeAway.writeOrder = 5;
        obj.schema.modeTakeAway.amendBill = 6;
        obj.schema.modeTakeAway.payLater = 7;
        // delivery Order mode function mode
        obj.schema.modeDeliveryOrder = {};
        obj.schema.modeDeliveryOrder.normal = 0;
        obj.schema.modeDeliveryOrder.writeOrder = 1;
        obj.schema.modeDeliveryOrder.printBill = 2;
        obj.schema.modeDeliveryOrder.voidBill = 3;
        obj.schema.modeDeliveryOrder.searchBill = 4;
        obj.schema.modeDeliveryOrder.writeOrder = 5;
        obj.schema.modeDeliveryOrder.amendBill = 6;
        // deposit mode 
        obj.schema.modeDeposit = {}
        obj.schema.modeDeposit.input = 0;
        obj.schema.modeDeposit.read = 1;
        obj.schema.modeDeposit.summary = 2;

        // floor plan function mode
        obj.schema.modeFloorPlan = {};
        obj.schema.modeFloorPlan.order = 0;
        obj.schema.modeFloorPlan.printOrder = 1;
        obj.schema.modeFloorPlan.changeTable = 2;
        obj.schema.modeFloorPlan.printBill = 3;
        obj.schema.modeFloorPlan.split = 4;
        // obj.schema.modeFloorPlan.discount = 5;
        obj.schema.modeFloorPlan.billing = 10;
        obj.schema.modeFloorPlan.kitchenMessage = 11;
        obj.schema.modeFloorPlan.editTable = 12;

        // top menu
        obj.schema.modeTable = {};
        obj.schema.modeTable.svg = 0;
        obj.schema.modeTable.tableView = 1;
        obj.schema.modeNavMenu = {};
        obj.schema.modeNavMenu.normal = 0;
        obj.schema.modeNavMenu.editTable = 1;
        obj.schema.modeNavMenu.takeAway = 2;
        obj.schema.modeNavMenu.takeAwayInOrder = 3;
        obj.schema.modeNavMenu.deliveryOrder = 4;
        obj.schema.modeNavMenu.deliveryOrderInOrder = 5;
        obj.schema.modeNavMenu.fastFood = 6;
        obj.schema.modeNavMenu.fastFoodInOrder = 7;

        // order item, option, menu mode
        obj.schema.modeItem = {};
        obj.schema.modeItem.normal = 0;
        obj.schema.modeItem.option = 1;
        obj.schema.modeItem.cancel = 2;
        obj.schema.modeItem.modifier = 3;
        obj.schema.modeItem.optionFoodControl = 4;
        obj.schema.modeItem.compositeItem = 5;

        obj.schema.modeItemDetail = {};
        obj.schema.modeItemDetail.code = 0;
        obj.schema.modeItemDetail.printer = 1;

        obj.schema.modeOption = {};
        obj.schema.modeOption.normal = 0;
        obj.schema.modeOption.cancel = 1;
        obj.schema.modeOrder = {};
        obj.schema.modeOrder.normal = 0;
        obj.schema.modeOrder.detail = 1; // deprecated => modeItemListDisplay
        obj.schema.modeOrder.foodControl = 2;
        obj.schema.modeOrder.transfer = 3;
        obj.schema.modeOrder.billing = 5;
        obj.schema.modeOrder.billingSummary = 51;
        obj.schema.modeOrder.searchBill = 6;
        obj.schema.modeOrder.amendBill = 7;
        obj.schema.modeOrder.amendBillSummary = 71; // sumamry after amending a bill
        obj.schema.modeOrder.printBill = 8; // for printing bill within order mode
        obj.schema.modeOrder.split = 9;
        obj.schema.modeOrder.specialMessage = 10;
        obj.schema.modeOrder.zeroItem = 11; // deprecated => modeItemListDisplay
        obj.schema.modeOrder.deposit = 12;
        // obj.schema.modeOrder.takeAway = 12; // deprecated => modeItemListDisplay

        // because in order mode( right hand side menu can be food order or bill search/print bill...etc, menu is different)
        obj.schema.modeOrderMenu = {};
        obj.schema.modeOrderMenu.normal = 0; // food menu
        // obj.schema.modeOrderMenu.foodMenu = 1;
        obj.schema.modeOrderMenu.billMenu = 1;
        // obj.schema.modeOrderMenu.takeAwayMenu = 2;

        // mode item display mode, display, display2 like channel, if display is independent, use different channel, otherwise, put into the same group
        obj.schema.modeItemListDisplay = {};
        obj.schema.modeItemListDisplay.normal = 0;
        // obj.schema.modeItemListDisplay.detail = 1; // deprecated, moved to display2
        obj.schema.modeItemListDisplay.zeroItem = 1;

        obj.schema.modeItemListDisplay2 = {};
        obj.schema.modeItemListDisplay2.normal = 0;
        obj.schema.modeItemListDisplay2.detail = 1;
        obj.schema.modeItemListDisplay2.zeroItem = 2;

        obj.schema.permission = {};
        obj.schema.permission.openTableAndOrder = 'p1';
        obj.schema.permission.printBill = 'p2';
        obj.schema.permission.billing = 'p3';
        obj.schema.permission.voidItem = 'p4';
        obj.schema.permission.changeTender = 'p5';
        obj.schema.permission.report = 'p6';
        obj.schema.permission.userManagement = 'p7';
        obj.schema.permission.checkBill = 'p8';
        obj.schema.permission.dailyClearance = 'p9';
        obj.schema.permission.cashManagement = 'p10';
        obj.schema.permission.cashbox = 'p11';
        obj.schema.permission.changeBillAmount = 'p12';
        obj.schema.permission.tableManagement = 'p13';
        obj.schema.permission.voidBill = 'p14';
        obj.schema.permission.foodManagement = 'p15';
        obj.schema.permission.paymentPermission = 'p16';

        obj.dailyClearanceCheck = false;
        obj.octopusBlacklistOutdated = false;
        obj.getTenderList = function () { // for dine in mode without show pay later tender
            var tmp = [];

            if (MainService.mode == MainService.schema.mode.deposit) {
                tmp = obj.tender.filter(function (t) {
                    return t.code != 'payLater' && t.code != 'multipay' && t.code != 'deposit' && t.isEnt == false;
                })
                return tmp;
            }

            if (MainService.getOtherWriteOrderService() != undefined && MainService.modeOrder != MainService.schema.modeOrder.amendBill && MainService.getOtherWriteOrderService().modeSetting.mode.value == 'takeAway' && !MainService.checkIsPayLaterMode()) {
                tmp = obj.tender;
                return tmp;
            } else {
                tmp = obj.tender.filter(function (t) {
                    return t.code != 'payLater';
                })
                if (MainService.modeOrder == MainService.schema.modeOrder.amendBill || MainService.checkIsPayLaterMode()) {
                    tmp = tmp.filter(function (t) {
                        return t.code != 'deposit';
                    })
                }
                return tmp;
            }
        }

        obj.resetTitle = function (title) {
            var title = title ? title : '';
            this.currentPageTitle = uiLanguage[INFO.lang][title] ? uiLanguage[INFO.lang][title] : title;
        }

        obj.resetDefaultMode = function (option) {
            var option = option ? option : 'reset';

            obj.voidIndex = -1;

            // first time only
            if (option === 'init') {
                obj.mode = obj.schema.mode.main;
                if (option === 'init') {
                    obj.mode = obj.schema.mode.main;
                    if (!userConfig.defaultShowZeroPriceItem)
                        obj.modeItemListDisplay2 = obj.schema.modeItemListDisplay2.zeroItem;
                    else obj.modeItemListDisplay2 = obj.schema.modeItemListDisplay2.normal;
                    //obj.mode = obj.schema.mode.home;
                }
                //obj.mode = obj.schema.mode.home;
            }

            obj.modeMain = obj.schema.modeMain.normal;
            obj.modeItem = obj.schema.modeItem.normal;
            obj.modeItemListDisplay = obj.schema.modeItemListDisplay.normal;
            //obj.modeItemListDisplay2 = obj.schema.modeItemListDisplay2.normal;
            obj.modeTakeAway = obj.schema.modeTakeAway.normal;
            obj.modeFastFood = obj.schema.modeFastFood.normal;
            obj.modeBuildBillFromVoid = obj.schema.modeBuildBillFromVoid.normal;
            obj.modeDeliveryOrder = obj.schema.modeDeliveryOrder.normal;
            obj.modeNavMenu = obj.schema.modeNavMenu.normal;
            obj.modeOrderMenu = obj.schema.modeOrderMenu.normal;
            obj.modeCash = obj.schema.modeCash.normal;
            obj.modeOrder = obj.schema.modeOrder.normal;
            obj.modeFloorPlan = obj.schema.modeFloorPlan.order;
            obj.modeMultiSelectItem = false;
            obj.modeItemDetail = obj.schema.modeItemDetail.code;


            if (option == 'takeAway') {
                //obj.currentPageTitle = uiLanguage[INFO.lang]['take_away'];
                //obj.mode = obj.schema.mode.home;
                obj.mode = obj.schema.mode.main;
                //obj.modeNavMenu = obj.schema.modeNavMenu.takeAway;
                //return;
            }
            // this.goToMode2({modeItem: 'normal', mode: 'main', modeOrder: 'normal', modeFloorPlan: 'order'});
        }

        // page function
        obj.resetPage = {}; // then define in each controller for extra function such as cash management
        obj.sceneInitStatus = {}; // then define in each controller for extra function such as cash management

        obj.pettyReport = [{ name: 'Petty Cash' }, { name: 'P2' }]
        obj.pettyPerPage = 8;
        obj.noOfPettyPage = Math.ceil(obj.pettyReport.length / obj.pettyPerPage);
        obj.currentPettyPage = 1;

        // console.log(obj.schema.modeOrder.printBill);

        obj.schema.member = {
            name: function () { return this.surname + ' ' + this.givenname },
            dob: function () { return this.dob_yyyy + '/' + this.dob_mm + '/' + this.dob_dd },
        }

        obj.schema.unselectOption = {
            "name1": "未選擇菜單",
            "name2": "unselect Option",
            "name3": "未選擇菜單",
            "optionCode": "O9999",
            "unitprice": 0,
            "name": {
                "001": "未選擇菜單",
                "002": "未選擇菜單",
                "003": "unselect Option"
            },
            "code": "O9999",
            "qty": 0,
            "show": false
        };

        obj.schema.baseDiscount = {
            getName: function () {
                if (this.name != undefined) {
                    return obj.fn.shorten(this.name[INFO.lang], 15);
                }
                else {
                    var fieldName = "name1";
                    switch (INFO.lang) {
                        case "003":
                            fieldName = "name2";
                            break;
                    }
                    return obj.fn.shorten(this[fieldName], 15);
                }
            },
            qty: 1
        };

        // for existing item
        obj.schema.baseItem = {
            init: function () {
                if (this.option != undefined) {
                    $.each(this.option, function (optionIndex, option) {
                        $.extend(true, option, obj.schema.baseItem);
                        //console.log('option.items', option.items);

                        $.each(option.items, function (optionItemIndex, optionItem) {
                            $.extend(true, optionItem, obj.schema.baseOption, { qty: 0, _qty: 0 });
                            //console.log(optionItem);
                        })
                    });
                }
                if (this.subitem != undefined) {
                    $.each(this.subitem, function (idx, subitem) {
                        $.extend(true, subitem, obj.schema.baseItem);
                    });
                }
            }, getJson: function () {
                var list = [];
                var itemType = "I"
                var item = this;
                if (item.type) {
                    if (item.type == "T") {
                        itemType = "T";
                    }
                }

                list.push({
                    code: [item.code], qty: [item.qty], unitPrice: [item.getUnitPrice() * 100], voidIndex: [item.voidIndex], voidRemark: [item.voidRemark], type: [itemType], desc1: [item.getName()]
                    , customName: [item.customName], voidApproveStaff: [item.voidApproveStaff], seq: [item.index]
                });
                if (item.discount.length != 0) {
                    $.each(item.discount, function (i, o) {
                        list.push({
                            code: [o.coupon_alias], qty: [1], unitPrice: [o.amount * 100], voidIndex: [-1], voidRemark: [""], type: ["D"], desc1: [o.getName()]
                        , customName: [""], voidApproveStaff: [""], seq: [item.index]
                        });
                    });
                }

                if (item.modifier.length != 0) {
                    $.each(item.modifier, function (modifierIndex, modifier) {
                        list.push({
                            desc1: [modifier.label],
                            code: [modifier.code],
                            qty: [1],
                            unitPrice: [0],
                            voidIndex: [-1],
                            voidRemark: [''],
                            voidApproveStaff: [undefined],
                            type: "M", seq: [item.index]
                        });
                    })
                }
                if (item.kitchenMsg.length != 0) {

                    $.each(item.kitchenMsg, function (kitchenMsgIndex, kitchenMsg) {
                        list.push({
                            desc1: [kitchenMsg.label],
                            code: [kitchenMsg.code],
                            qty: [1],
                            unitPrice: [0],
                            voidIndex: [-1],
                            voidRemark: [''],
                            voidApproveStaff: [undefined],
                            type: "K", seq: [item.index]
                        });
                    })
                }
                if (item.option != undefined && item.option.length != 0) {
                    $.each(item.option, function (optionIndex, option) {
                        $.each(option.items, function (optionItemIndex, optionItem) {
                            if (optionItem.qty > 0) {
                                list.push({
                                    desc1: [optionItem.getName()],
                                    code: [optionItem.code],
                                    qty: [optionItem.getQty()],
                                    unitPrice: [optionItem.getUnitPrice() * 100],
                                    voidIndex: [optionItem.voidIndex],
                                    voidRemark: [optionItem.voidRemark],
                                    voidApproveStaff: [optionItem.voidApproveStaff],
                                    type: "O", seq: [optionItem.index]
                                });
                                $.each(optionItem.modifier, function (modifierIndex, modifier) {
                                    list.push({
                                        desc1: [modifier.label],
                                        code: [modifier.code],
                                        qty: [1],
                                        unitPrice: [0],
                                        voidIndex: [-1],
                                        voidRemark: [''],
                                        voidApproveStaff: [undefined],
                                        type: "M", seq: [optionItem.index]
                                    });
                                })
                                $.each(optionItem.kitchenMsg, function (kitchenMsgIndex, kitchenMsg) {
                                    list.push({
                                        desc1: [kitchenMsg.label],
                                        code: [kitchenMsg.code],
                                        qty: [1],
                                        unitPrice: [0],
                                        voidIndex: [-1],
                                        voidRemark: [''],
                                        voidApproveStaff: [undefined],
                                        type: "K", seq: [optionItem.index]
                                    });
                                })
                            }
                        })
                    });
                }
                if (item.subitem != undefined && item.subitem.length != 0) {
                    $.each(item.subitem, function (subitemIndex, subitem) {
                        list.push({
                            desc1: [subitem.getName()],
                            code: [subitem.code],
                            qty: [subitem.getQty()],
                            unitPrice: [subitem.getUnitPrice() * 100],
                            voidIndex: [subitem.voidIndex],
                            voidRemark: [subitem.voidRemark],
                            voidApproveStaff: [subitem.voidApproveStaff],
                            type: "S", seq: [subitem.index]
                        });
                    });
                }
                return list;
            },
            getName: function () {
                if (this.customName != undefined) {
                    if (this.customName != "") {
                        return obj.fn.shorten(this.customName, 15);
                    }
                }
                if (this.name != undefined) {
                    return obj.fn.shorten(this.name[INFO.lang], 15);
                }
                else {
                    // console.log('getName ar');
                    //console.log(this.desc1);
                    var fieldName = "desc1";
                    switch (INFO.lang) {
                        case "003":
                            fieldName = "desc2";
                            break;
                    }
                    return obj.fn.shorten(this[fieldName], 15);
                }
            },
            getIndex: function () {
                return this.index;
            },
            getQty: function () {
                return parseInt(this.qty);
            },
            getQtyWithVoid: function () {
                //console.log('qty:' + this.qty);
                //console.log('voidQty:' + this.voidQty);
                return parseInt(this.qty) + parseInt(this.voidQty);
            },
            getUnitPrice: function () {
                /*
                  unitPrice from db tableorderb
                  unitprice from db tableorderb
                */
                if (this.unitPrice != undefined) {
                    return parseFloat(this.unitPrice);
                } else
                    if (this.unitprice != undefined) {
                        return (this.unitprice / 100);
                    }

            },
            getItemUnitPrice: function () {
                var optionPrice = 0
                if (this.option != undefined && this.option.length > 0) {
                    $.each(this.option, function (optionIndex, option) {
                        $.each(option.items, function (itemIndex, item) {
                            if (item.qty != undefined) {
                                //console.log("qty:" + item.getQty());
                                //console.log("getUnitPrice:" + item.getUnitPrice());
                                //console.log(item);
                                if (item.getQty() > 0) {
                                    optionPrice += item.getUnitPrice() * item.getQty();
                                }
                            }
                        })
                    });
                }
                return optionPrice + this.getUnitPrice();
            },
            getTotalPrice: function (filter) {
                if (filter) {
                    // console.log('format');
                    return $filter('currency')(this.getTotalPrice(), INFO.currency, 1)
                }
                else {
                    // console.log('not format');
                    // console.log(this.getItemUnitPrice() * this.getQty());
                    var p = 0;
                    var self = this;
                    if (self.isHaveCompositeOptionGroup() && obj.combo.checkIsFastFoodMode()) {
                        var optionPrice = 0;
                        this.option.forEach(function (optionGroup) {
                            if (optionGroup.compositeOptionGroup) {
                                return;
                            }
                            optionGroup.items.forEach(function (item) {
                                if (item.getQty() > 0) {
                                    if (!optionGroup.compositeOptionGroup)
                                        optionPrice += item.getUnitPrice() * item.getQty() * self.getQty();
                                    else
                                        optionPrice += item.getUnitPrice() * item.getQty();
                                }
                            })
                        })
                        //p = this.getUnitPrice() * this.getQty() + optionPrice;
                        p = this.getUnitPrice() * this.getQty() + optionPrice;
                    }
                    else if (this.comboItemKey != undefined && this.comboItemKey != "") {
                        p = this.optionUnitPrice * self.getQty();
                    }
                    else {
                        p = this.getItemUnitPrice() * this.getQty();
                    }
                    if (this.discount.length != 0) {
                        $.each(this.discount, function (i, v) {
                            p -= v.amount;
                        })
                    }
                    return p;
                }
                // return this.getItemUnitPrice() * this.getQty();
                //return this.getUnitPrice() ;
            },
            getPriceForSC: function () {
                switch (this.serviceCharge) {
                    case undefined:
                    case true:
                        return this.getTotalPrice();
                    default:
                        return 0;

                }
            },
            getTotalPriceWithVoid: function (filter) {
                var p = this.getTotalPrice();
                if (this.voidQty != 0) {
                    p -= -(this.getItemUnitPrice() * this.voidQty);
                }
                // return p;
                if (filter)
                    return $filter('currency')(p, INFO.currency, 1);
                else
                    return p;
            },
            editItem: function ($event) {
                console.log('baseItem / editItem');
                //// no behaviour in non-kitchen mode during order but still need to transfer item
                //console.log(obj.modeMultiSelectItem);
                //console.log(this.isEdit);
                //console.log('obj.modeItem');
                //console.log(obj.modeItem);
                if (!obj.modeMultiSelectItem)
                    obj.switchMode2({ modeItem: 'normal' });
                //if (obj.modeItem === obj.schema.modeItem.modifier) {
                //    console.log('modifierChanges start');
                //    $rootScope.$emit('modifierChanges', []);

                //} else

                if (obj.modeOrder == obj.schema.modeOrder.amendBill || obj.modeOrder == obj.schema.modeOrder.amendBillSummary
                    //|| obj.modeOrder == obj.schema.modeOrder.printBill
                    || obj.modeOrder == obj.schema.modeOrder.searchBill) {
                    return;
                } else if (obj.modeMultiSelectItem) {
                    console.log('baseItem / editItem / 0');

                    if (this.editOption) {
                        this.isEdit = true;
                        this.editOption = false;
                    } else {
                        this.isEdit = !this.isEdit;
                    }

                } else if (this.isEdit) {
                    // obj.Cart.currentOption = null;
                    console.log(obj.childEdit);
                    if (obj.childEdit) {
                        return;
                    }

                    console.log('baseItem / editItem / 1');
                    obj.Cart.currentOptionList = [];
                    obj.Cart.currentOptionIndex = -1;
                    obj.Cart.resetEditItem();
                    obj.modeItem = obj.schema.modeItem.normal;
                    return;
                } else {
                    console.log('baseItem / editItem / 2');
                    obj.Cart.currentOptionList = [];
                    obj.Cart.currentOptionIndex = -1;

                    console.log('reset edit item');
                    obj.Cart.resetEditItem();
                    this.isEdit = true;
                }
                //obj.toggleActive($event, function () { });
                // if ($event) {
                //     $event.stopPropagation();
                // }
            },
            selectAllItem: function ($event) {
                $.each(obj.Cart.orderList, function (idx, val) {
                    // console.log(this);

                    this.isEdit = true;

                    //val.option.forEach(function (o) {
                    //    o.items[0].isEdit = true;
                    //})
                })
            },
            toggleAllItem: function ($event) {
                $.each(obj.Cart.orderList, function (idx, val) {
                    // console.log(this);
                    this.isEdit = !this.isEdit;
                })
            },
            unselectAllItem: function ($event) {
                $.each(obj.Cart.orderList, function (idx, val) {
                    // console.log(this);
                    this.isEdit = false;
                })
            },
            deleteItem: function ($event, index) {
                console.log('baseItem / deleteItem');
                obj.Cart.resetEditItem();
                this.isEdit = !this.isEdit;
                obj.modeItem = obj.schema.modeItem.cancel;
                //obj.modeMultiSelectItem = true; ??

                if ($event) {
                    $event.stopPropagation();
                }
            },
            qty: 1,
            voidQty: 0,
            voidIndex: -1,
            orgIndex: -1, // not save to DB, only work for locally (split item)
            voidRemark: '',
            isDelete: false,
            isEdit: false,
            modifier: [],
            kitchenMsg: [],
            kitchenMsgOld: [],
            discount: [],
            checkIsFireItem: function (kitchenMsg) {
                if (!kitchenMsg)
                    return this.kitchenMsgOld.filter(function (elem) {
                        return elem.code == obj.fireCode;
                    }).length != 0;
                else {
                    return this.kitchenMsg.filter(function (elem) {
                        return elem.code == obj.fireCode;
                    }).length != 0;
                }
            },
            isHaveCompositeOptionGroup: function () {
                var result = false;
                if (this.option != undefined && this.option.length > 0) {
                    $.each(this.option, function (optionIndex, option) {
                        if (option.compositeOptionGroup) {
                            result = true;
                            return false;
                        }
                    })
                }
                return result;
            },
            getCompositeItemCode: function () { //never used
                var codeList = [];
                this.items.forEach(function (o) {
                    if (o.plu) codeList.push(o.plu);
                });
                return codeList;
            }
        }


        // for option extensibility
        obj.schema.baseOption = $.extend(true, {}, obj.schema.baseItem, {
            edit: function ($event, idx, editItem, optionList) {
                console.log('option edit');
                if (editItem.voidIndex > -1) return;

                editItem.isEdit = true;

                if (!obj.modeMultiSelectItem)
                    angular.forEach(obj.Cart.cartList, function (item) {
                        if (item != editItem) item.isEdit = false;
                        angular.forEach(item.option, function (option) {
                            angular.forEach(option.items, function (optItem) {
                                if (optItem != this)
                                    optItem.isEdit = false;
                            });
                        });
                    });

                this.isEdit = !this.isEdit; // toggle
                if (!obj.isModeItem('modifier'))
                    if (this.isEdit) {
                        if (obj.modeItem != obj.schema.modeItem.modifier) {
                            obj.Cart.currentOptionIndex = idx;
                            obj.Cart.currentOptionList = optionList;
                            obj.modeItem = obj.schema.modeItem.option;
                        }
                    } else {
                        obj.Cart.currentOptionList = [];
                        obj.Cart.currentOptionIndex = -1;
                        obj.modeItem = obj.schema.modeItem.normal;
                    }
                //console.log(this);
                //console.log(obj.Cart.currentOptionList);
                if ($event) {
                    $event.stopPropagation();
                }
            }
        });

        // for new item
        obj.schema.baseCartItem = $.extend(true, {}, obj.schema.baseItem, {
            deleteItem: function ($event, index) {
                var isCombo = obj.Cart.cartList[index].option;
                var itemKey = obj.Cart.cartList[index].itemKey;
                var voidedItem = obj.Cart.orderList.filter(function (x) { x.index == obj.Cart.cartList[index].voidIndex; });
                if (voidedItem.length) {
                    voidedItem.forEach(function (item) {
                        item.lock = false;
                    });
                }
                obj.Cart.cartList.splice(index, 1);

                if (isCombo)
                    obj.combo.deleteCombo(itemKey);

                else
                    obj.combo.deleteItem(itemKey);

                // when delete item, reset to default mode, such as modeOrder to normal while in option
                obj.modeOrder = obj.schema.modeOrder.normal;
                obj.modeItem = obj.schema.modeItem.normal;
                // obj.modeItemDetail = obj.schema.modeItemDetail.code;
                obj.Cart.currentOptionList = [];
                obj.Cart.currentOptionIndex = -1;

                if ($event) {
                    $event.stopPropagation();
                }
            },
            editItem: function ($event, focus) {


                console.log('baseCartItem / editItem');
                // selected but option is not shown (such as selected food control but then want to select option, when click, direct show option instead of toggle)
                // console.log('this.isEdit ' + this.isEdit);
                // console.log('obj.modeMultiSelectItem ' + obj.modeMultiSelectItem);
                // console.log('obj.modeItem != obj.schema.modeItem.option ' + obj.modeItem != obj.schema.modeItem.option);
                var doScrollBottom = true;
                var self = this;
                var idx = -1;


                obj.Cart.cartList.forEach(function (i, index) {
                    if (i.itemKey == self.itemKey) { idx = index; return false; }
                })
                localStorage.setItem("scrollToTarget", this.itemKey);
                if (idx != obj.Cart.cartList.length - 1) {
                    doScrollBottom = false;
                }

                if (this.isEdit && !obj.modeMultiSelectItem && obj.modeItem != obj.schema.modeItem.option) {
                    obj.Cart.currentOptionList = [];
                    obj.Cart.currentOptionIndex = -1;
                    obj.Cart.resetEditItem(); // for multi-selection
                    this.isEdit = true;
                }
                else {

                    if (obj.modeMultiSelectItem) {
                        // console.log(1);
                        this.isEdit = !this.isEdit;
                        if (obj.isModeItem('modifier')) return;
                        console.log('a obj.modeMultiSelectItem')
                    } else if (this.isEdit && !obj.isModeItem('modifier')) {
                        // deselect
                        // obj.Cart.currentOption = null;
                        obj.Cart.currentOptionList = [];
                        obj.Cart.currentOptionIndex = -1;
                        obj.Cart.resetEditItem();
                        obj.modeItem = obj.schema.modeItem.normal;
                        console.log('b else if (this.isEdit)')
                        return;
                    } else {
                        obj.Cart.currentOptionList = [];
                        obj.Cart.currentOptionIndex = -1;
                        obj.Cart.resetEditItem(); //hidden for multi-selection

                        this.isEdit = true;

                        console.log('c else ')
                        //doScrollBottom = false;
                    }
                }


                //obj.toggleActive($event, function () { });
                if ($event) {
                    $event.stopPropagation();
                }
                console.log("this.voidIndex", this.voidIndex);

                if (this.voidIndex == -1 && this.option != undefined && this.option) {
                    // if (this.option) {
                    obj.modeItem = obj.schema.modeItem.option;
                    obj.Cart.currentOption = this.option;
                    // obj.Cart.currentOptionIndex = 0;
                    // obj.Cart.currentOptionList = obj.Cart.currentOption[obj.Cart.currentOptionIndex];

                    // obj.Cart.showNextOption();

                    // show all option in item list
                    // console.log(item.option);
                    console.log('prepare extended option');
                    this.selectedOption = [];
                    var self = this;
                    $.each(this.option, function (idx, optionList) {
                        obj.addUnselectOption(optionList, idx, self);
                    });
                    obj.Cart.showNextOption();

                    //newItem.option = $.extend(true, {}, item.option , obj.schema.baseOption);
                    //newItem.option.edit(null, '');
                    //console.log(newItem.option);
                    // console.log(newItem);
                    // }



                }
                else if (focus == undefined && this.qty > 0 && !obj.modeCheckOrder('transfer') && !obj.modeCheckOrder('specialMessage')) {
                    obj.modeItem = obj.schema.modeItem.modifier;
                    //obj.modeMultiSelectItem = true;
                }

                if (doScrollBottom)
                    $timeout(function () {
                        obj.scrollToBottom();
                        //obj.scrollToActive();
                    }, 10);
            }
        });

        obj.addUnselectOption = function (optionList, idx, item) {
            console.log(optionList.minQty);
            for (var i = 0; i <= optionList.minQty; i++) {
                var fillOption = $.extend(true, {}, obj.schema.unselectOption, obj.schema.baseOption, { qty: 1, _qty: 1, color: 'rms-palette-lemon' });
                if (idx == 0 && i == 0) {
                    fillOption.isEdit = true;
                }
                var exists = false;
                var defaultItem = null;
                angular.forEach(optionList.items, function (item) {
                    if (item.optionCode === fillOption.optionCode) {
                        exists = true;
                    }
                    if (item.isDefault === true && defaultItem === null) {
                        defaultItem = item;
                    }
                });
                if (!exists) {
                    if (defaultItem) {
                        defaultItem.qty = optionList.minQty;
                        defaultItem._qty = optionList.minQty;
                        fillOption.qty = 0;
                        fillOption._qty = 0;
                    }

                    if (optionList.compositeOptionGroup && obj.combo.checkIsFastFoodMode()) {
                        console.debug(item);
                        console.debug(item.code);
                        fillOption.qty = 0; // unselected option's qty to 0
                        fillOption._qty = 0; // unselected option's qty to 0
                        optionList.items.push(fillOption);
                        $.extend(true, optionList, obj.combo.extendOptionGroup);
                        obj.combo.addOptionGroup(item.itemKey, idx, optionList);
                        continue;
                    }
                    optionList.items.push(fillOption);
                }

            }
            /** for put the not select option to first (display only)**/
            if (optionList.items[optionList.items.length - 1].code == obj.schema.unselectOption.code) {
                var notSelectItemOptionObj = optionList.items.splice(optionList.items.length - 1, 1);
                if (notSelectItemOptionObj[0].code == obj.schema.unselectOption.code)
                    optionList.items.splice(0, 0, notSelectItemOptionObj[0]);
            }
        }

        obj.splitListUpdatePrice = function () {
            console.log('splitListUpdatePrice');
            var price = 0;
            if (obj.Cart.allSplitList[obj.Cart.splitListLIndex] == undefined) return;
            if (obj.Cart.allSplitList[obj.Cart.splitListLIndex].item) {
                //$.each(obj.Cart.allSplitList[obj.Cart.splitListLIndex].item, function (i, v) {
                //    //if (v.getQty() > 0 && !v.isDelete)
                //    price += v.getTotalPrice();
                //});
                console.log('lindex');
                console.log(obj.Cart.allSplitList[obj.Cart.splitListLIndex].item);
                var tempItemList = {};

                angular.copy(obj.Cart.allSplitList[obj.Cart.splitListLIndex], tempItemList);
                console.log(' angular.copy(obj.Cart.allSplitList[obj.Cart.splitListLIndex]', tempItemList);
                tempItemList.item.forEach(function (item) {
                    if (item.voidItemList != undefined) {
                        var voidItemObj = $.grep(tempItemList.item, function (elem) {
                            return elem.voidIndex == item.index;
                        });
                        if (voidItemObj.length != 0) {
                            voidItemObj[0].qty += item.voidItemNotTransferQty;
                        } else {
                            if (item.voidIndex == -1) {
                                var newTemplVoidItem = {};
                                angular.copy(item, newTemplVoidItem);
                                delete newTemplVoidItem.voidItemList;
                                newTemplVoidItem.qty = newTemplVoidItem.voidItemNotTransferQty;
                                tempItemList.item.push(newTemplVoidItem);
                            }
                        }
                    }
                });
                var priceObj = obj.calcPrice([tempItemList.item]);
                //var priceObj = obj.calcPrice([obj.Cart.allSplitList[obj.Cart.splitListLIndex].item]);

                obj.Cart.splitListLPrice = priceObj.price;
                obj.Cart.splitListLServiceCharge = priceObj.serviceCharge;
                obj.Cart.splitListLRemainings = priceObj.remainings;
                obj.Cart.splitListLTotalPrice = priceObj.totalPrice;

                //var scObj = obj.calcServiceCharge(price);
                //obj.Cart.splitListLServiceCharge = scObj.serviceCharge;
                //obj.Cart.splitListLRemainings = scObj.remainings;
                //obj.Cart.splitListLTotalPrice = price + obj.Cart.splitListLServiceCharge;
            }
            price = 0;
            if (angular.isDefined(obj.Cart.allSplitList[obj.Cart.splitListRIndex]) && obj.Cart.allSplitList[obj.Cart.splitListRIndex].item) {
                //$.each(obj.Cart.allSplitList[obj.Cart.splitListRIndex].item, function (i, v) {
                //    price += v.getTotalPrice();
                //});
                //obj.Cart.splitListRPrice = price;

                console.log('rindex');


                //console.log(obj.Cart.allSplitList[obj.Cart.splitListRIndex].item);

                console.log(obj.Cart.allSplitList[obj.Cart.splitListRIndex].item);
                var tempItemList = {};

                angular.copy(obj.Cart.allSplitList[obj.Cart.splitListRIndex], tempItemList);

                tempItemList.item.forEach(function (item) {
                    if (item.voidItemList != undefined) {
                        var voidItemObj = $.grep(tempItemList.item, function (elem) {
                            return elem.voidIndex == item.index;
                        });
                        if (voidItemObj.length != 0) {
                            voidItemObj[0].qty += item.voidItemNotTransferQty;
                        } else {
                            if (item.voidIndex == -1) {
                                var newTemplVoidItem = {};
                                angular.copy(item, newTemplVoidItem);
                                delete newTemplVoidItem.voidItemList;
                                newTemplVoidItem.qty = newTemplVoidItem.voidItemNotTransferQty;
                                tempItemList.item.push(newTemplVoidItem);
                            }
                        }
                    }
                });

                var priceObj = obj.calcPrice([tempItemList.item]);
                //var priceObj = obj.calcPrice([obj.Cart.allSplitList[obj.Cart.splitListRIndex].item]);
                console.log(priceObj);
                obj.Cart.splitListRPrice = priceObj.price;
                obj.Cart.splitListRServiceCharge = priceObj.serviceCharge;
                obj.Cart.splitListRRemainings = priceObj.remainings;
                obj.Cart.splitListRTotalPrice = priceObj.totalPrice;

                //var scObj = obj.calcServiceCharge(price);
                //obj.Cart.splitListRServiceCharge = scObj.serviceCharge;
                //obj.Cart.splitListRRemainings = scObj.remainings;
                //obj.Cart.splitListRTotalPrice = price + obj.Cart.splitListRServiceCharge;
            }
        }

        obj.splitListClick = function (currentIdx, qty) {
            if (obj.Cart.allSplitList[0] == undefined) return;
            console.log('currentIdx, qty', currentIdx, qty);
            if (angular.isDefined(obj.Cart.allSplitList[obj.Cart.splitListRIndex]))
                console.log(obj.Cart.allSplitList[obj.Cart.splitListRIndex].item);

            var checkList = obj.Cart.splitListLIndex == currentIdx ? obj.Cart.allSplitList[obj.Cart.splitListRIndex].item : obj.Cart.allSplitList[obj.Cart.splitListLIndex].item;
            var currentList = obj.Cart.allSplitList[currentIdx].item;
            var swapList = [];
            var fromIndex = currentIdx != obj.Cart.splitListLIndex ? obj.Cart.splitListLIndex : obj.Cart.splitListRIndex;
            var toIndex = currentIdx == obj.Cart.splitListLIndex ? obj.Cart.splitListLIndex : obj.Cart.splitListRIndex;

            var checkVoidItem = function (item) {
                console.log('checkVoidItem', item);
                $.each(checkList, function (checkidx, checkitem) {
                    if (item.index == checkitem.voidIndex) {
                        var cloneItem = {};
                        angular.copy(checkList[checkidx], cloneItem);
                        checkList.splice(checkidx, 1)
                        swapList.push(cloneItem);
                        checkVoidItem(item);
                        return false;
                    }
                })
                //checkFunction();
            }

            //qty = parseInt(qty);

            if (qty != undefined) {
                qty = parseInt(qty);
            }
            console.log('checklist');

            console.log('fromIndex', fromIndex);
            obj.Cart.allSplitList[currentIdx].item.forEach(function (item) {
                if (item.holdIndex == undefined) {
                    item.holdIndex = currentIdx;
                }

            });

            var originalCheckList = [];
            angular.copy(checkList, originalCheckList);

            var checkFunction = function () {

                // console.log('checkFunction ing ============================================================')
                var checkListSpliceList = [];
                $.each(checkList, function (idx, item) {
                    if (item == undefined) return true;
                    if (item.isEdit && item.voidIndex == -1) {
                        if (item.qty < 0) return true;
                        var cloneItem = {};
                        //checkList[idx].isEdit = false;
                        item.isEdit = false;
                        angular.copy(checkList[idx], cloneItem);

                        var isCheckVoidItem = true;
                        if (qty == undefined) {
                            checkList.splice(idx, 1); // need
                            cloneItem.isAll = true;
                            swapList.push(cloneItem);

                            var voidObjIndex = -1;
                            var voidObjTempl = $.grep(checkList, function (elem, index) {
                                var result = false;
                                if (elem.voidItemList == undefined && elem.index == 0 && elem.voidIndex == item.index && item.voidQty == elem.qty) {
                                    voidObjIndex = index;
                                    result = true;
                                }
                                else if (elem.voidItemList != undefined && elem.index == 0 && elem.voidIndex == item.index && (item.voidQty - item.voidItemNotTransferQty) == elem.qty) {
                                    voidObjIndex = index;
                                    result = true;
                                }
                                return result;
                            });
                            if (voidObjTempl.length != 0) {
                                checkList.splice(voidObjIndex, 1);//need;
                                //voidObjTempl[0].isVoid = true;
                                //swapList.push(voidObjTempl[0]);
                            }
                        }
                        else if (qty >= item.getQtyWithVoid()) { // getQtyWithVoid = max qty of item
                            checkList.splice(idx, 1); // need
                            cloneItem.isAll = true;
                            swapList.push(cloneItem);

                            var voidObjIndex = -1;
                            var voidObjTempl = $.grep(checkList, function (elem, index) {
                                var result = false;
                                if (elem.voidItemList == undefined && elem.index == 0 && elem.voidIndex == item.index && item.voidQty == elem.qty) {
                                    voidObjIndex = index;
                                    result = true;
                                }
                                else if (elem.voidItemList != undefined && elem.index == 0 && elem.voidIndex == item.index && (item.voidQty - item.voidItemNotTransferQty) == elem.qty) {
                                    voidObjIndex = index;
                                    result = true;
                                }
                                return result;
                            });
                            if (voidObjTempl.length != 0) {
                                checkList.splice(voidObjIndex, 1);//need;
                                //voidObjTempl[0].isVoid = true;
                                //swapList.push(voidObjTempl[0]);
                            }
                        }
                        else {
                            if (item.holdIndex != fromIndex) return true;
                            qty = parseInt(qty);
                            isCheckVoidItem = false;
                            delete cloneItem["voidItemList"];
                            delete cloneItem["voidItemNotTransferQty"];
                            cloneItem.holdIndex = item.holdIndex;
                            cloneItem.qty = qty;
                            cloneItem.voidQty = 0;
                            cloneItem.orgIndex = cloneItem.index;
                            cloneItem.index = item.index;
                            cloneItem.fromIndex = fromIndex;
                            cloneItem.toIndex = toIndex;


                            var templVoidItemIndex = -1;
                            var templVoidItem = $.grep(checkList, function (voidItem, index) {
                                if (voidItem.voidIndex == item.index) {
                                    templVoidItemIndex = index;
                                    return true;
                                }
                            });
                            if (templVoidItem.length == 0) {
                                var cloneVoidItem = {};
                                angular.copy(checkList[idx], cloneVoidItem);
                                cloneVoidItem.orgIndex = cloneVoidItem.index;
                                cloneVoidItem.index = 0;
                                cloneVoidItem.voidQty = 0;
                                cloneVoidItem.qty = -1 * qty;
                                cloneVoidItem.voidIndex = item.index;
                                cloneVoidItem.voidRemark = uiLanguage[INFO.lang]['item_transfer']; // item_transfer

                                checkList.splice(idx + 1, 0, cloneVoidItem);
                                //checkList.push(cloneVoidItem);
                            } else {
                                var voidObjTempl = $.grep(checkList, function (elem, index) {
                                    return elem.index == 0 && elem.voidIndex == item.index;
                                });
                                voidObjTempl[0].qty -= qty;

                            }

                            item.voidQty -= qty;
                            swapList.push(cloneItem);
                        }
                        checkFunction();
                        return false;
                    }
                });
            }
            //console.log('checkFunction First ============================================================')
            checkFunction();
            //if ($event) {
            //    $event.stopPropagation();
            //}
            //console.log('checkFunction after ============================================================')
            var templConsole = [];
            angular.copy(swapList, templConsole);
            console.debug('swap list', templConsole);
            var debugAlert = false;
            if (swapList.length != 0) {
                while (swapList.length != 0) {

                    for (var i = 0; i < swapList.length; i++) {
                        console.debug("swap Action ====================================", swapList.length);
                        swapList[i].isEdit = false;
                        var isAddtoCurrentList = true;
                        //if (swapList[i].orgIndex) {
                        for (var j = currentList.length - 1 ; j >= 0; j--) {

                            if (swapList[i].index == currentList[j].index) {
                                //back the table which is hold the item; swapList[i].holdIndex == currentIdx
                                if (false) {
                                    isAddtoCurrentList = false;
                                    currentList[j].voidQty += swapList[i].qty;
                                    //check if the item qty is equal original qty
                                    checkNeedVoidItem();
                                    swapList.splice(i, 1);
                                    break; //need
                                }
                                else if (true) {
                                    isAddtoCurrentList = false;
                                    var removeTransferVoidItem = false;
                                    if (swapList[i].voidQty == 0) {

                                        var templVoidItemIndex = -1;
                                        var templVoidItem = $.grep(currentList, function (voidItem, index) {
                                            if (voidItem.voidIndex == swapList[i].index) {
                                                templVoidItemIndex = index;
                                                return true;
                                            }
                                        });
                                        if (templVoidItem.length != 0) {
                                            //a-> b , b->a
                                            if (debugAlert) alert('a-> b , b->a');
                                            currentList[j].voidQty += swapList[i].qty;
                                            templVoidItem[0].qty += swapList[i].qty;
                                            if (templVoidItem[0].qty == 0) {
                                                delete currentList[j].hasTransfer;
                                                currentList.splice(templVoidItemIndex, 1);
                                            }
                                        }
                                        else {
                                            //a-> b , a-> b
                                            if (debugAlert) alert('a-> b , a-> b');
                                            currentList[j].qty += swapList[i].qty;
                                        }
                                    }
                                    else if (swapList[i].voidItemList != undefined && (swapList[i].voidQty + swapList[i].voidItemNotTransferQty == 0)) {
                                        console.log('a-> b');
                                        var templVoidItemIndex = -1;
                                        var templVoidItem = $.grep(currentList, function (voidItem, index) {
                                            if (voidItem.voidIndex == swapList[i].index) {
                                                templVoidItemIndex = index;
                                                return true;
                                            }
                                        });
                                        if (templVoidItem.length != 0) {
                                            //a-> b , b->a
                                            currentList[j].voidQty += swapList[i].qty;
                                            templVoidItem[0].qty += swapList[i].qty;
                                            if (templVoidItem[0].qty == 0) {
                                                delete currentList[j].hasTransfer;
                                                currentList.splice(templVoidItemIndex, 1);
                                            }
                                        }
                                        else {
                                            //a-> b , a-> b
                                            if (debugAlert) alert('test2');
                                            currentList[j].qty += swapList[i].qty;
                                        }
                                    }
                                    else {
                                        if (debugAlert) alert('a-> b , a-> b & void // a->b, a->c, a->b &void');
                                        var existItem = currentList[j].qty;
                                        angular.copy(swapList[i], currentList[j]);
                                        currentList[j] = swapList[i];
                                        currentList[j].voidQty += existItem;

                                        if (currentList[j].voidItemList == undefined && currentList[j].voidQty != 0) {
                                            if (debugAlert) alert('1');
                                            var cloneVoidItem = {};
                                            angular.copy(currentList[j], cloneVoidItem);
                                            cloneVoidItem.orgIndex = cloneVoidItem.index;
                                            cloneVoidItem.index = 0;
                                            cloneVoidItem.voidQty = 0;
                                            cloneVoidItem.qty = currentList[j].voidQty;
                                            cloneVoidItem.voidIndex = currentList[j].index;
                                            cloneVoidItem.voidRemark = uiLanguage[INFO.lang]['item_transfer']; // item_transfer
                                            currentList.splice(j + 1, 0, cloneVoidItem);
                                        }
                                        else if (currentList[j].voidItemList != undefined && (-1 * currentList[j].voidQty + currentList[j].voidItemNotTransferQty != 0)) {
                                            console.debug(-1 * currentList[j].voidQty + currentList[j].voidItemNotTransferQty != 0);
                                            console.debug(currentList[j].voidQty, currentList[j].voidItemNotTransferQty);
                                            if (debugAlert) alert('2');
                                            var cloneVoidItem = {};
                                            angular.copy(currentList[j], cloneVoidItem);
                                            cloneVoidItem.orgIndex = cloneVoidItem.index;
                                            cloneVoidItem.index = 0;
                                            cloneVoidItem.voidQty = 0;
                                            cloneVoidItem.qty = currentList[j].voidQty - currentList[j].voidItemNotTransferQty;
                                            cloneVoidItem.voidIndex = currentList[j].index;
                                            cloneVoidItem.voidRemark = uiLanguage[INFO.lang]['item_transfer']; // item_transfer
                                            currentList.splice(j + 1, 0, cloneVoidItem);
                                        }


                                        //checkNeedVoidItem();
                                        //if (swapList.hasTransfer) {
                                        //    currentList[j] = swapList[i];
                                        //    currentList[j].voidQty += existItem;
                                        //}
                                        //else {
                                        //    currentList[j].voidQty += swapList[i].qty;
                                        //}
                                        //if (currentList[j].voidItemList == undefined) {
                                        //    if (currentList[j].voidQty == 0) {
                                        //        delete currentList[j].hasTransfer;
                                        //        removeTransferVoidItem = true;
                                        //    }
                                        //    else {
                                        //        var cloneVoidItem = {};
                                        //        angular.copy(currentList[j], cloneVoidItem);
                                        //        currentList[j].hasTransfer = true;
                                        //        cloneVoidItem.orgIndex = cloneVoidItem.index;
                                        //        cloneVoidItem.index = 0;
                                        //        cloneVoidItem.voidQty = 0;
                                        //        cloneVoidItem.qty = currentList[j].voidQty - currentList[j].voidItemNotTransferQty;
                                        //        cloneVoidItem.voidIndex = currentList[j].index;
                                        //        cloneVoidItem.voidRemark = uiLanguage[INFO.lang]['item_transfer']; // item_transfer
                                        //        currentList.splice(j + 1, 0, cloneVoidItem);
                                        //    }
                                        //}
                                        //else {
                                        //    console.debug('void not 1');
                                        //    if (currentList[j].voidQty == currentList[j].voidItemNotTransferQty) {
                                        //        delete currentList[j].hasTransfer;
                                        //        removeTransferVoidItem = true;
                                        //    } else {
                                        //        var cloneVoidItem = {};
                                        //        angular.copy(currentList[j], cloneVoidItem);
                                        //        currentList[j].hasTransfer = true;
                                        //        cloneVoidItem.orgIndex = cloneVoidItem.index;
                                        //        cloneVoidItem.index = 0;
                                        //        cloneVoidItem.voidQty = 0;
                                        //        cloneVoidItem.qty = currentList[j].voidQty - currentList[j].voidItemNotTransferQty;
                                        //        cloneVoidItem.voidIndex = currentList[j].index;
                                        //        cloneVoidItem.voidRemark = uiLanguage[INFO.lang]['item_transfer']; // item_transfer
                                        //        currentList.splice(j + 1, 0, cloneVoidItem);
                                        //    }
                                        //}
                                        //remove transfer void Item

                                    }

                                    //if (removeTransferVoidItem) {
                                    //    var templVoidItemIndex = -1;
                                    //    var templVoidItem = $.grep(currentList, function (item, index) {
                                    //        if (item.voidIndex == swapList[i].index) {
                                    //            templVoidItemIndex = index;
                                    //            return true;
                                    //        }
                                    //    });
                                    //    if (templVoidItem.length != 0) {
                                    //        templVoidItem[0].qty += swapList[i].qty;
                                    //        if (templVoidItem[0].qty == 0) {
                                    //            delete currentList[j].hasTransfer;
                                    //            currentList.splice(templVoidItemIndex, 1);
                                    //        }
                                    //    }
                                    //}

                                    swapList.splice(i, 1);
                                    break; //need

                                    //if (swapList[i].isAll) {
                                    //    if (swapList[i].voidIndex == -1 && swapList[i].index == currentList[j].index) {
                                    //        console.debug('swapList isAll =========================', swapList[i])
                                    //        swapList[i].voidQty += currentList[j].qty;
                                    //        currentList[j] = swapList[i];
                                    //        isAddtoCurrentList = false;
                                    //        swapList.splice(i, 1);
                                    //        break; //need
                                    //    }
                                    //}
                                    //if (swapList[i].isVoid) {
                                    //    if (swapList[i].voidIndex == currentList[j].index) {
                                    //        console.debug('swapList isVoid =========================', swapList[i])
                                    //        if (currentList[j].voidItemList == undefined && currentList[j].qty == currentList[j].getQtyWithVoid()) {
                                    //            delete currentList[j].hasTransfer;
                                    //            isAddtoCurrentList = false;
                                    //            swapList.splice(i, 1);
                                    //            break; //need
                                    //        }
                                    //        else if (currentList[j].voidItemList != undefined && (currentList[j].qty + currentList[j].voidItemNotTransferQty) == currentList[j].getQtyWithVoid()) {
                                    //            delete currentList[j].hasTransfer;
                                    //            isAddtoCurrentList = false;
                                    //            swapList.splice(i, 1);
                                    //            break; //need
                                    //        }
                                    //    }
                                    //}
                                    //if (swapList[i].index == currentList[j].index) {
                                    //    console.debug('swapList else =========================', swapList[i])
                                    //    currentList[j].qty += swapList[i].qty;
                                    //    isAddtoCurrentList = false;
                                    //    swapList.splice(i, 1);
                                    //    break; //need
                                    //}


                                }

                            }
                            else {
                                //checkNeedVoidItem();
                                //swapList.splice(i, 1);
                                //break; //need
                            }


                        }

                        //}
                        if (isAddtoCurrentList) {
                            console.debug('swapList insert =========================', swapList[i])
                            if (swapList[i].isAll) delete swapList[i].isAll;
                            currentList.push(swapList[i]);
                            if (currentList[currentList.length - 1].voidItemList == undefined) {
                                if (currentList[currentList.length - 1].voidQty != 0) {
                                    if (debugAlert) alert('swap != current 1')
                                    var cloneVoidItem = {};
                                    angular.copy(currentList[currentList.length - 1], cloneVoidItem);
                                    currentList[currentList.length - 1].hasTransfer = true;
                                    cloneVoidItem.orgIndex = cloneVoidItem.index;
                                    cloneVoidItem.index = 0;
                                    cloneVoidItem.voidQty = 0;
                                    cloneVoidItem.qty = currentList[currentList.length - 1].voidQty;
                                    cloneVoidItem.voidIndex = currentList[currentList.length - 1].index;
                                    cloneVoidItem.voidRemark = uiLanguage[INFO.lang]['item_transfer']; // item_transfer
                                    currentList.splice(currentList.length, 0, cloneVoidItem);
                                }
                            }
                            else {
                                if (currentList[currentList.length - 1].voidQty < currentList[currentList.length - 1].voidItemNotTransferQty) {
                                    if (debugAlert) alert('swap != current 2')
                                    var cloneVoidItem = {};
                                    angular.copy(currentList[currentList.length - 1], cloneVoidItem);
                                    currentList[currentList.length - 1].hasTransfer = true;
                                    cloneVoidItem.orgIndex = cloneVoidItem.index;
                                    cloneVoidItem.index = 0;
                                    cloneVoidItem.voidQty = 0;
                                    cloneVoidItem.qty = currentList[currentList.length - 1].voidQty - currentList[currentList.length - 1].voidItemNotTransferQty;
                                    cloneVoidItem.voidIndex = currentList[currentList.length - 1].index;
                                    cloneVoidItem.voidRemark = uiLanguage[INFO.lang]['item_transfer']; // item_transfer
                                    currentList.splice(currentList.length, 0, cloneVoidItem);
                                }

                            }
                            swapList.splice(i, 1);
                            break; //need
                        }

                        //if (i == swapList.length - 1) obj.splitListUpdatePrice();
                    }
                } //end while


                //currentList.sort(function (a, b) {
                //    if (parseInt(a.index) != parseInt(b.index)) return a.index - b.index;
                //});

                //obj.Cart.splitListR.concat(swapList)
                //console.log(obj.Cart.splitListR.length);

                obj.splitListUpdatePrice();


                $.each(obj.Cart.allSplitList[obj.Cart.splitListRIndex].item, function (idx, item) {
                    item.isEdit = false;
                })
                $.each(obj.Cart.allSplitList[obj.Cart.splitListLIndex].item, function (idx, item) {
                    item.isEdit = false;
                })
                return true;
            }
            else {
                obj.Cart.currentSelectedSplitIndex = currentIdx;
                return false;
            }
        }

        obj.schema.baseSplitItem = $.extend(true, {}, obj.schema.baseItem, {
            editItem: function ($event, currentIdx) {
                console.log('obj.schema.baseSplitItem / editItem');
                if (obj.splitListClick(currentIdx)) {

                } else {
                    console.log(obj.modeMultiSelectItem);
                    if (obj.modeMultiSelectItem) {
                        // console.log(1);
                        this.isEdit = !this.isEdit;
                    } else if (this.isEdit) {
                        // obj.Cart.currentOption = null;
                        obj.Cart.currentOptionList = [];
                        obj.Cart.currentOptionIndex = -1;
                        obj.Cart.resetEditItem();
                        obj.modeItem = obj.schema.modeItem.normal;
                        return;
                    } else {
                        // console.log(2);
                        obj.Cart.currentOptionList = [];
                        obj.Cart.currentOptionIndex = -1;
                        obj.Cart.resetEditItem();
                        this.isEdit = true;
                    }
                };
                if ($event) {
                    $event.stopPropagation();
                }
            }
        });

        // default settings
        /*obj.mode = obj.schema.mode.main;
        obj.modeMain = obj.schema.modeMain.normal;
        obj.modeItem = obj.schema.modeItem.normal;
        obj.modeItemListDisplay = obj.schema.modeItemListDisplay.normal;
        obj.modeItemListDisplay2 = obj.schema.modeItemListDisplay2.normal;
        obj.modeNavMenu = obj.schema.modeNavMenu.normal;
        obj.modeOrderMenu = obj.schema.modeOrderMenu.normal;
        obj.modeCash = obj.schema.modeCash.normal;
        //obj.mode = obj.schema.mode.login;
        //obj.mode = obj.schema.mode.floorPlan;
        //obj.mode = obj.schema.mode.order;
        obj.modeOrder = obj.schema.modeOrder.normal;
        obj.modeFloorPlan = obj.schema.modeFloorPlan.order;
        obj.modeMultiSelectItem = false;
        obj.modeItemDetail = obj.schema.modeItemDetail.code;*/
        obj.resetDefaultMode('init');

        obj.itemsPerPage = 18 - 5;
        obj.catsPerPage = 8;
        obj.cancelOptionPerPage = 13;
        obj.cancelOptionTotalItems = INFO.cancelRemark.length;
        obj.noOfCancelOptionPage = Math.ceil(obj.cancelOptionTotalItems / obj.cancelOptionPerPage);
        obj.cancelOptionCurrentPage = 1;
        obj.modeItemDetail = obj.schema.modeItemDetail.code;

        // version 1, single mode toggling between selected modes and normal
        obj.singleModeToggle = function (type, mode) {

            console.debug('singleModeToggle');
            if (angular.isUndefined('type') || angular.isUndefined('mode'))
                return;

            // if want detail and 0 display at the same time, use different "channel"(different mode container)
            // force to change mode if it is not normal instead of toggle
            if (obj[type] != obj.schema[type][mode] && obj[type] != obj.schema[type].normal) {
                obj[type] = obj.schema[type][mode];
                return;
            }

            console.log(mode);
            console.log(type);
            console.log(obj[type]);
            console.log(obj.schema[type].normal);
            console.log(obj.schema[type][mode]);
            obj[type] = obj[type] == obj.schema[type].normal ? obj.schema[type][mode] : obj.schema[type].normal;
            console.log(obj[type]);
        }

        obj.modeToggleItemDetail = function (element) {
            obj.modeItemDetail = obj.modeItemDetail == obj.schema.modeItemDetail.code ? obj.schema.modeItemDetail.printer : obj.schema.modeItemDetail.code;
        }

        obj.resizeOrderListHeight = function (element) {
            var target = element ? element : "order-scene .order-list-wrapper";
            // total height of display area (exclude top and status bar)
            var maxHeight = angular.element(".menu-area").height();

            // total height of account summary
            var totalHeight = 0;
            angular.element(".account-summary .row").each(
             function (index, ele) {
                 totalHeight = totalHeight + angular.element(ele).height();
                 console.log(angular.element(ele).height());
             }
            );
            // order list maximum height
            var orderListMaxHeight = maxHeight - totalHeight;
            angular.element(element).css("max-height", orderListMaxHeight);
        }

        obj.modeToggleModifier = function () {
            obj.modeOrder = obj.schema.modeOrder.normal;
            obj.modeItem = obj.modeItem == obj.schema.modeItem.modifier ? obj.schema.modeItem.normal : obj.schema.modeItem.modifier
            obj.modeMultiSelectItem = true;
        }

        obj.modeToggleFoodControl = function () {
            obj.modeFloorPlan = obj.schema.modeFloorPlan.order;
            obj.modeItem = obj.schema.modeItem.normal;
            obj.modeOrder = obj.modeOrder == obj.schema.modeOrder.foodControl ? obj.schema.modeOrder.normal : obj.schema.modeOrder.foodControl;
        }

        obj.modeToggleSpecialMessage = function () {
            obj.modeItem = obj.schema.modeItem.normal;
            obj.modeOrder = obj.schema.modeOrder.specialMessage;
            //switchMode2({ modeOrder: 'specialMessage', modeItem: 'normal' }, true, 'special_message', 'special_message');
        }

        // obj.modeToggleBilling = function () {
        //     console.log('toggle billing');
        //     obj.modeOrder = obj.modeOrder == obj.schema.modeOrder.billing ? obj.schema.modeOrder.billing : obj.schema.modeOrder.billing
        //     obj.schema.billing = true;
        // }

        obj.modeToggleOrder = function (modeOrder) {
            switch (modeOrder) {
                default:
                    obj.modeOrder = obj.modeOrder == obj.schema.modeOrder.normal ? obj.schema.modeOrder.detail : obj.schema.modeOrder.normal

                    break;
                case 'zeroItem':
                    // console.log('zeroItem');
                    obj.modeOrder = obj.modeOrder == obj.schema.modeOrder.normal ? obj.schema.modeOrder.zeroItem : obj.schema.modeOrder.normal
                    break;
            }
            console.log('toggle order ' + modeOrder);
        }

        // for legacy RMSTable function compatibility purpose, deprecated, please merge into better code later
        // deprecated
        // obj.RMSTable = {
        //     editMode : "order",
        //     changeMode : function( mode ) {
        //         this.editMode = lowerFirstLetter( mode );
        //         console.log(lowerFirstLetter( mode ));
        //         console.log(this.editMode);
        //         showMessage('#status-message', uiLanguage[INFO.lang][lowerFirstLetter( mode )] + ' ' + uiLanguage[INFO.lang]['mode'], '', 'warn', 'stick');
        //     }
        // };
        // console.log(obj.RMSTable);

        obj.showStatusMessage = function (message) {
            showMessage('#status-message', message, '', 'warn', 'stick');
        }

        obj.UserManager = {};
        obj.UserManager.LoginRedirectToTable = '';
        obj.UserManager.LoginRedirectTo = {};
        obj.UserManager.staffName = '';
        obj.UserManager.staffCode = '';
        obj.UserManager.staffDetails = '';
        obj.UserManager.isLogin = false;
        obj.UserManager.loginSuccess = false;
        obj.UserManager.loginCallback = function () {

            console.log('login callback');
            if (!obj.UserManager.loginSuccess) {
                obj.UserManager.loginSuccess = true;
                $timeout(function () {
                    obj.UserManager.loginSuccess = false;
                }, 500);
            } else {
                console.log('login return;');
                return;
            }


            // var redirectMode = obj.UserManager.LoginRedirectToMode;
            // if ( angular.isDefined( redirectMode ) && Object.size( redirectMode ) != 0 ) {
            //     obj.goToMode( redirectMode.type, redirectMode.mode );
            //     // obj.goToMode2( redirectMode );

            //     // reset
            //     obj.UserManager.LoginRedirectTo = {};
            //     obj.UserManager.LoginRedirectToMode = {};
            // }

            var redirectMode2 = obj.UserManager.LoginRedirectToMode2;
            var eventEle = obj.UserManager.LoginRedirectToCaller;
            if (angular.isDefined(redirectMode2) && Object.size(redirectMode2) != 0) {
                // console.log('redirect mode v2');
                obj.goToMode2(redirectMode2, eventEle);

                // reset
                obj.UserManager.LoginRedirectTo = {};
                obj.UserManager.LoginRedirectToMode2 = {};
                obj.UserManager.LoginRedirectToCaller = {};
            }

            if (obj.UserManager.LoginRedirectToTable != '') {
                //alert(MainService.modeFloorPlan)
                if (obj.modeFloorPlan == obj.schema.modeFloorPlan.billing) {
                    obj.billTable(obj.UserManager.LoginRedirectToTable);
                } else
                    if (obj.modeFloorPlan == obj.schema.modeFloorPlan.split) {
                        console.log('split table');
                        obj.splitTableNo(obj.UserManager.LoginRedirectToTable);
                    } else {
                        obj.assignTableNo(obj.UserManager.LoginRedirectToTable, function (cb) {
                            obj.selectTable(cb);
                            // if (angular.element('md-dialog[aria-label="Bill Option"]').scope() == undefined) {
                            //     $mdDialog.show({
                            //         controller: function ($scope) {
                            //             $scope.order = cb;
                            //             $scope.assignTableNo = function (t) {
                            //                 obj.assignTableNo(t,null,null,true);
                            //                 $scope.closeDialog();
                            //             }
                            //             $scope.closeDialog = function () {
                            //                 $mdDialog.hide();
                            //             }
                            //         },
                            //         templateUrl: 'tmpl/billOption.html'
                            //     })
                            // } else {
                            //     $mdDialog.hide();
                            // }
                        });
                    }
                obj.UserManager.LoginRedirectToTable = '';
            }
        }
        obj.UserManager.checkLogin = function () {
            if (!obj.UserManager.isLogin) {
                if (angular.element('md-dialog[aria-label="Login"]').scope() == undefined) {
                    $mdDialog.show({
                        templateUrl: 'tmpl/login.html'
                    })
                } else {
                    $mdDialog.hide();
                }
            }
        };
        obj.UserManager.logout = function (username, password) {
            // console.log(obj.modeOrder);
            console.log('logout ar!');
            //obj.pageTitle = "";
            obj.UserManager.isLogin = false;
            console.log(obj.UserManager.isLogin)
            obj.UserManager.staffName = '';
            console.log(obj.stopLogoutCountDown)
            obj.stopLogoutCountDown();
            console.log('after stop function')
            // for the following pages, redirect to floorplan
            // console.log(this.modeOrder);


            // console.log(obj.modeOrder);
            if (obj.modeOrder == obj.schema.modeOrder.searchBill) {
                console.log('search bill out?');
                obj.modeOrder = {};
                obj.mode = obj.schema.mode.main;
            }

            //console.log('obj.mode', obj.mode);
            if (obj.modeChecking([{ 'mode': ['report', 'main', 'userManagement', 'dailyClearance', 'cashManagement', 'userClockManagement', 'userGroupManagement', 'takeAway', 'categorySetting', 'settings', 'deposit'] }]) || obj.modeCheckOrder('searchBill') || obj.getOtherWriteOrderService()) {
                obj.mode = obj.schema.mode.main;
            }
            else {
                obj.mode = obj.schema.mode.floorPlan;
            }

            obj.resetDefaultMode();

            $mdDialog.show({
                templateUrl: 'tmpl/login.html',
            })
            //obj.mode = obj.schema.mode.home;
        }
        obj.UserManager.login = function (username, password) {
            var deferred = $q.defer();
            SocketFactory.emit('login', { u: username, p: password }, function (r) {
                //r = JSON.parse(r);
                console.log(r);
                if (r.success) {
                    obj.UserManager.staffName = r.staffName;
                    obj.UserManager.staffCode = username;
                    obj.UserManager.staffDetails = r.staffDetails;
                    obj.UserManager.isLogin = true;

                    // console.log('success?');
                    obj.startLogoutCountDown();
                }
                deferred.resolve(r);
            });
            return deferred.promise;
        }
        obj.UserManager.requestApproval = function (username, password) {
            var deferred = $q.defer();
            SocketFactory.emit('login', { u: username, p: password }, function (r) {
                if (r.success) {
                    //return r;
                }
                deferred.resolve(r);
            });
            return deferred.promise;
        }


        obj.fn = {};
        obj.fn.shorten = function (text) {
            text = angular.isUndefined(text) ? '' : text;
            return text;
            // if (text)
            //     return shortenText(text, 15);
            // else
            //     return "";
        }

        obj.showAlert = function (content, title) {
            title = uiLanguage[INFO.lang][title] ? uiLanguage[INFO.lang][title] : title || uiLanguage[INFO.lang]['alert'],
            content = uiLanguage[INFO.lang][content] ? uiLanguage[INFO.lang][content] : content || 'Please provide content';

            $mdDialog.show(
              $mdDialog.alert()
                .parent(angular.element(document.body))
                .title(title)
                .content(content)
                .ok(uiLanguage[INFO.lang]["affirmative"])
            );
        }

        obj.showConfirm = function (content, title, okCallback, cancelCallback) {
            var title = uiLanguage[INFO.lang][title] ? uiLanguage[INFO.lang][title] : title || uiLanguage[INFO.lang]['alert'],
            content = uiLanguage[INFO.lang][content] ? uiLanguage[INFO.lang][content] : content || 'Please provide content';
            var confirm = $mdDialog.confirm()
                  .parent(angular.element(document.body))
                  .title(title)
                  .content(content)
                  // .content('Are you sure to delete the groups?')
                  .ok(uiLanguage[INFO.lang]['yes'])
                  .cancel(uiLanguage[INFO.lang]['cancel'])
            //.targetEvent(ev);
            $mdDialog.show(confirm).then(function () {
                //alert('You decided to get rid of your debt.');
                //if()
                okCallback();
            }, function () {
                //alert('You decided to keep your debt.');

                cancelCallback();
            });
        }

        /*function (cb) {
            //callback(cb);
            if (angular.element('md-dialog[aria-label="Bill Option"]').scope() == undefined) {
                console.log('clickTable - assignTableNo');
                $mdDialog.show({
                    controller: function ($scope) {
                        $scope.order = cb;
                        $scope.assignTableNo = function (t) {
                            obj.assignTableNo(t,null,null,true);
                            $scope.closeDialog();
                        }
                        $scope.closeDialog = function () {
                            $mdDialog.hide();
                        }
                    },
                    templateUrl: 'tmpl/billOption.html'
                })
            } else {
                $mdDialog.hide();
            }
        }*/
        obj.selectTable = function (cb, tableMode) {
            tableMode = tableMode ? tableMode : null;

            if (angular.element('md-dialog[aria-label="Bill Option"]').scope() == undefined) {
                console.log('clickTable - assignTableNo');
                $mdDialog.show({
                    controller: function ($scope, userConfig, uiLanguage, INFO) {
                        // console.log($scope);
                        $scope.order = cb;
                        $scope.uc = userConfig;
                        $scope.ui = uiLanguage[INFO.lang];
                        $scope.returnStatus = function (status) {
                            if (status == 4) {
                                return $scope.ui['billed'];
                            }
                        }
                        $scope.lockTableList = [];
                        $scope.lockStatus = function (order) {
                            var checking = $.grep($scope.lockTableList, function (lockTableNo) {
                                return lockTableNo == "L" + order.header.tableNum;
                            });
                            return checking.length != 0;
                        }

                        SocketFactory.emit('getAllLockedTableNo', function (result) {
                            $scope.lockTableList = result;
                        });

                        // all action decisions here
                        $scope.assignTableNo = function (t, status, $event) {
                            status = status ? status : '';
                            if (status == 4) {
                                return;
                            }

                            obj.assignTableNo(t, null, tableMode, true, $event);
                            $scope.closeDialog();
                        }
                        $scope.closeDialog = function () {
                            $mdDialog.hide();
                        }
                    },
                    templateUrl: 'tmpl/billOption.html'
                })
            } else {
                $mdDialog.hide();
            }
        }

        obj.clickTable = function (tableNo, ele, callback) {
            //console.log('click me clickTable');
            //console.log(this.modeFloorPlan);
            console.log(ele);
            console.debug('tableNo', tableNo);

            if (!userConfig.defaultShowZeroPriceItem)
                obj.modeItemListDisplay2 = obj.schema.modeItemListDisplay2.zeroItem;
            else obj.modeItemListDisplay2 = obj.schema.modeItemListDisplay2.normal;

            switch (this.modeFloorPlan) {
                case this.schema.modeFloorPlan.order:
                    console.log('this.schema.modeFloorPlan.order');
                    obj.modeItem = obj.schema.modeItem.normal;


                    this.assignTableNo(tableNo, obj.selectTable);
                    break;
                case this.schema.modeFloorPlan.split:
                    this.voidIndex = -1; // starting
                    this.splitTableNo(tableNo);
                    break;
                case this.schema.modeFloorPlan.billing:
                    console.log('test billing');
                    this.appliedPaymentMethod = [];

                    this.appliedDiscount = [];

                    // alert(status);
                    // if( status == 2 ) {
                    //     // should print first
                    //     obj.showAlert('please_print_bill_first');
                    //     return;
                    // }
                    // if( status == 3 ) {
                    //     this.billTable(tableNo, 'billing');
                    //     return;
                    // } else {
                    //     obj.showAlert('no_order');
                    //     return;
                    // }
                    this.billTable(tableNo, 'billing');
                    break;
                case this.schema.modeFloorPlan.printOrder:
                    //做檯紙
                    //this.printOrder(tableNo,'order');

                    this.billTable(tableNo, 'printOrder');
                    break;
                case this.schema.modeFloorPlan.printBill:
                    // 印單
                    console.log("printing bill");
                    // this.printOrder(tableNo,'bill'); // put in billTable() after yes
                    // return;
                    // console.log(status)
                    // only table with order or print billed data is eligible to print
                    // obj.Cart.tableNo = tableNo;
                    // var status = Snap("#svg-canvas").select('#table_1418785047317').data().loadedData.status;
                    // if( status == 2 || status == 3 ) {
                    //if( status == 2  ) {'
                    // 靠 tableMode param trigger, 所以必要
                    // this.billTable(tableNo, 'printBill');

                    // this.assignTableNo(tableNo, obj.selectTable, 'printBill');
                    this.billTable(tableNo, 'printBill');

                    // this.printOrder(tableNo,'bill'); // put in billTable() after yes
                    //}
                    //if( status == 3 ) {
                    //    // printed bill
                    //    var confirm = $mdDialog.confirm()
                    //          .parent(angular.element(document.body))
                    //          .title(uiLanguage[INFO.lang]['alert'])
                    //          .content('決定重覆打印賬單？')
                    //          .ok(uiLanguage[INFO.lang]['yes'])
                    //          .cancel(uiLanguage[INFO.lang]['cancel']);
                    //    $mdDialog.show(confirm).then(function() {
                    //      console.log('print again');
                    //    }, function() {
                    //      console.log('print again cancel');

                    //    });
                    //}
                    // } else {
                    // obj.showAlert('no_order');
                    // return;
                    // }
                    // console.log('should not see me');
                    break;
                case this.schema.modeFloorPlan.kitchenMessage:
                    console.log('this.schema.modeFloorPlan.kitchenMessage');
                    this.tableKitchenMessage(tableNo, 'kitchenMessage');
                    break;
                case this.schema.modeFloorPlan.changeTable:
                    console.log('clickTable / changeTable');

                    this.assignTableNo(tableNo, obj.selectTable, 'changeTable', null, ele);
                    break;
            }
        };

        obj.switchModeItem = function (modeChange) {

        }

        obj.recoverPaymentMethodInAmendBill = function () {
            console.debug('recoverPaymentMethodInAmendBill');
            obj.appliedPaymentMethod = [];
            if (obj.cloneAppliedPaymentMethod.length != 0)
                angular.copy(obj.cloneAppliedPaymentMethod, obj.appliedPaymentMethod);
            //obj.remainerAmount = "0.0";
            obj.remainder();
            //obj.calcPrice();
        }

        obj.resetPaymentSettings = function () {
            this.appliedPaymentMethod = []; // reset for next round
            this.remainerAmount = 0; // reset for next round

            if (otherWriteOrderService.children[0])
                if (otherWriteOrderService.children[0].modeSetting.modeInService.isMode('writeOrder')) {
                    otherWriteOrderService.children[0].resetCartList();
                    otherWriteOrderService.children[0].backTakeAwayOrderMode();
                    return;
                }

            //if (obj.isModeDeliveryOrder('writeOrder')) {
            //    obj.deliveryOrderService.resetCartList();
            //    obj.deliveryOrderService.takeOrderMode();
            //    return;
            //}
        }

        // logic:
        // add multiple payment method
        // when add once, payment method will gain one into array
        // default: cash + exact amount (if enter but not show in screen, avoid checking)
        /*
            default: store payment method into array (cash)
            [cash]
            if press enter -> submit assume exact amount with cash method (skip total amount input check)
            if press any number key -> assume custom input cash amount, when submit, check input amount
            if press cash on top of default with some amount input already (cash + amount then add cash again) and
            that amount < total required amount -> prompt to input/cannot proceed, if >= required amount, proceed
            and the new cash method is stored into array [cash, cash]
            assumed 2 or more method is added such as [cash, cash, visa]
            then these method will be displayed with method name and cash

            tips: use smaller font to show the payment method first to pass the deadline demo
        */

        // obj.paymentMethod = uiLanguage[INFO.lang]['cash'];
        // multiple payment method support
        obj.checkPaymentTips = function (forceAdd) {
            console.log('checkPaymentTips')
            this.isCash = false;
            this.isCard = false;
            if (angular.isUndefined(this.appliedPaymentMethod) || this.appliedPaymentMethod.length === 0) {

                ////if (forceAdd == undefined) {
                ////    this.appliedPaymentMethod = [
                ////    // set default if no selected method
                ////    {
                ////        'method': 'cash',
                ////        'amount': this.Cart.totalPrice
                ////    }];
                ////    // this.isDefaultPaymentMethod = true;
                ////    // this.isNotDefaultPaymentMethod = false;
                ////    this.isCash = true;
                ////}
                //console.debug('detect', (obj.modeOrder != obj.schema.modeOrder.printBill) && obj.tender != undefined);
                if ((obj.modeOrder != obj.schema.modeOrder.printBill) && obj.tender != undefined) {
                    var cash = obj.tender.filter(function (t) {
                        return t.code == 'cash';
                    })
                    //console.debug(cash);
                    if (obj.deposit.appliedDeposit.length != 0) return;
                    obj.setPaymentMethod(cash[0]);
                }

            }
            if (this.appliedPaymentMethod) {
                this.appliedPaymentMethod.forEach(function (v, i) {
                    if (v.method === 'cash') {
                        if (i == 0 && MainService.deposit.appliedDeposit.length != 0 && MainService.deposit.appliedDeposit[0].refNo != null) {
                            if (MainService.deposit.appliedDeposit[0].code == "cash") return true;
                        } else
                            obj.isCash = true;
                    }
                    if (v.tenderType === 'card') {
                        obj.isCard = true;
                    }
                })
            }
        }
        obj.setPaymentMethod = function (method) {
            // reset default payment method on first click
            // check if first click or not
            this.inputPrice = 0; // reset for each new method
            console.log('method', method);

            if (method.code == "multipay") {
                obj.multiPayment.show();
                return;
            }

            if (method.code == "payLater") {
                obj.getOtherWriteOrderService().takeOutAndDelivery.payLater();
                return;
            }

            if (method.code == "deposit") {

                obj.checkPermission('changeTender', function () {
                    obj.switchMode2({ modeOrder: "deposit" });
                }, function () { obj.showAlert('no_permission'); });
                //depositManager.show();
                return;
            }
            if (!obj.appliedPaymentMethod) obj.appliedPaymentMethod = [];

            var isExistPaymentMethod = obj.appliedPaymentMethod.filter(function (p) {
                return p.method == method.code;
            });


            if (method.code != 'cashCoupon')
                if (isExistPaymentMethod.length >= 1 && userConfig.page.individualPayment) {
                    if (isExistPaymentMethod.length == 1 && MainService.deposit.appliedDeposit.length == 1) { }
                    else return;
                }

            var langNameField = "";

            switch (INFO.lang) {
                case "003":
                    langNameField = "name2";
                    break;
                default:
                    langNameField = "name1";
            }




            function defaultTotalAmount() {
                var result = false;
                switch (method.code) {
                    case "visa":
                    case "master":
                    case "unionpay":
                    case "eps":
                    case "octopus2":
                        result = true;
                        break;


                    default: result = false;
                }

                return result;
            }

            var amount = 0;
            if (method.tenderType === 'cashCoupon') {
                amount = method.amount;
                obj.remainerAmount += amount;
            }

            if (defaultTotalAmount() && false) {
                //this.appliedPaymentMethod = [];
                amount = Math.abs(obj.remainerAmount);
                obj.remainerAmount = 0;
                //obj.remainder();
            }
            //obj.tender
            //console.debug('setPaymentMethod', this.appliedPaymentMethod.length);
            if (this.appliedPaymentMethod.length === 0) {
                // init method array
                this.appliedPaymentMethod = [];
                this.appliedPaymentMethod.push(
                        {
                            method: method.code,
                            name: method[langNameField],
                            tenderType: method.tenderType,
                            amount: amount // change by input

                        }
                    )
            } else {
                // add method to array if previous method have value, if not, replace current method
                if (this.appliedPaymentMethod[this.appliedPaymentMethod.length - 1].amount === 0) {
                    /*$mdDialog.show(
                        $mdDialog.alert()
                        .parent(angular.element(document.body))
                        .title(uiLanguage[INFO.lang]['alert'])
                        .content(uiLanguage[INFO.lang]['current_method_no_amount'])
                        .ok(uiLanguage[INFO.lang]['ok'])
                    );
                    return;*/
                    /*if (userConfig.page.individualPayment) {
                        return;
                    }*/

                    this.appliedPaymentMethod[this.appliedPaymentMethod.length - 1] = {
                        method: method.code,
                        name: method[langNameField],
                        tenderType: method.tenderType,
                        amount: amount // change by input
                    };
                } else {


                    if (userConfig.page.individualPayment) {
                        if (userConfig.page.individualPayment) {
                            if (MainService.deposit.appliedDeposit.length == 0) {
                                if (isExistPaymentMethod.length >= 1) return;
                                else if (MainService.appliedPaymentMethod.length >= 1) return;
                            }
                            else {
                                if (MainService.deposit.appliedDeposit[0].refNo == null) {
                                    if (isExistPaymentMethod.length >= 1) return;
                                    else if (MainService.appliedPaymentMethod.length >= 1) return;
                                }
                                else {
                                    if (isExistPaymentMethod.length >= 2) return;
                                    else if (MainService.appliedPaymentMethod.length >= 2) return;
                                }
                            }
                        }
                    }


                    if (userConfig.ecrPayment)
                        if (method.tenderType == 'card') {
                            var isExistCard = obj.appliedPaymentMethod.filter(function (p) {
                                return p.tenderType == 'card';
                            });
                            if (isExistCard.length > 0) return;
                        }


                    this.appliedPaymentMethod.push(
                        {
                            method: method.code,
                            name: method[langNameField],
                            tenderType: method.tenderType,
                            amount: amount // change by input
                        }
                    );
                }
            }

            if (userConfig.ecrPayment)
                this.appliedPaymentMethod[this.appliedPaymentMethod.length - 1].isNewAdd = true;

            this.checkPaymentTips();
            //octopusDineinPayment
            //if (methodName === "octopus") {
            //    var p = { "s": "123" };
            //    SocketFactory.emit('octopusDineinPayment', p, function (r) {
            //        console.log(r);
            //        if (r.result == 'ERROR') {
            //            obj.appliedPaymentMethod.pop();
            //        } else {
            //            obj.appliedPaymentMethod[0].remark = r.cardBalance + "|" + r.cardId + "|" + r.machineId;
            //            obj.appliedPaymentMethod[0].amount = obj.Cart.totalPrice
            //            obj.submitCart();
            //        }
            //    });
            //}
        }

        obj.checkPaymentMethod = function () {
            // set default price for billing, set switch for extension
            //console.debug('checkPaymentMethod')
            if (obj.deposit.appliedDeposit.length != 0) {
                return false;
            }
            else if (this.appliedPaymentMethod.length === 0) {
                this.appliedPaymentMethod = [
                // set default if no selected method
                {
                    'method': 'cash',
                    'amount': this.Cart.totalPrice
                }
                ];

                var cash = obj.tender.filter(function (t) {
                    return t.code == 'cash';
                });

                this.appliedPaymentMethod[0].name = cash[0].name1;
                // this.isDefaultPaymentMethod = true;
                // this.isNotDefaultPaymentMethod = false;
                return true;
            } else {
                // this.isDefaultPaymentMethod = false;
                // this.isNotDefaultPaymentMethod = true;
                return false;
            }
        }

        obj.deletePaymentMethod = function (index) {
            //console.debug('deletePaymentMethod', this.appliedPaymentMethod);
            this.appliedPaymentMethod.splice(index, 1);
            this.remainder();
            this.checkPaymentTips(false);

            if (obj.getOtherWriteOrderService() && !userConfig.ecrPayment) {
                if (obj.getOtherWriteOrderService().mode == obj.getOtherWriteOrderService().schema.viewPage) {
                    if (obj.cloneAppliedPaymentMethod) {
                        obj.cloneAppliedPaymentMethod[index].void = 1;
                    }
                }
            }

        }

        obj.updatePaymentAmount = function (amount) {
            //console.debug('updatePaymentAmount');
            if (this.appliedPaymentMethod == undefined) return;
            if (this.appliedPaymentMethod.length > 0) {
                // last/latest method

                /*if (this.deposit.appliedDeposit.length != 0 && this.deposit.appliedDeposit[0].refNo != null) {
                    this.appliedPaymentMethod[this.appliedPaymentMethod.length - 1].amount = obj.remainerAmount;
                    return;
                }*/

                var checkForECR = this.appliedPaymentMethod[this.appliedPaymentMethod.length - 1].isNewAdd;
                if (!userConfig.ecrPayment) {
                    checkForECR = true;
                }

                if (this.appliedPaymentMethod[this.appliedPaymentMethod.length - 1].tenderType != "cashCoupon" && checkForECR) {
                    this.appliedPaymentMethod[this.appliedPaymentMethod.length - 1].amount = parseFloat(amount);
                }

            }
        }

        // obj.appliedDiscount
        obj.availableDiscount = []; // by server?
        obj.availableDiscount = [
            //{
            //    coupon_alias: "cash_coupon_100",
            //    amount: 100,
            //    type: 'exact'
            //},
            //{
            //    coupon_alias: "cash_coupon_200",
            //    amount: 200,
            //    type: 'exact'
            //},
            //{
            //    coupon_alias: "cash_coupon_500",
            //    amount: 500,
            //    type: 'exact'
            //},
            {
                coupon_alias: "off_10",
                amount: 0.9,
                type: 'percentage'
            }
            ,
            {
                coupon_alias: "off_20",
                amount: 0.8,
                type: 'percentage'
            },
            {
                coupon_alias: "off_30",
                amount: 0.7,
                type: 'percentage'
            }

            // next phase
            /*,
            {
                coupon_alias: "deposit",
                amount: 0, // drawn from system, input reference code to load when click
                type: 'deposit' // will determine what to do for the "click" event
            },
            {
                coupon_alias: "custom",
                amount: 0, // drawn from system, input reference code to load when click
                type: 'custom' // will determine what to do for the "click" event
            }*/
        ];
        obj.discounPerPage = 13;
        obj.noOfPage_AvailableDiscount = Math.ceil(obj.availableDiscount.length / obj.discounPerPage);

        // return selected coupon obj with alias
        // @param string (coupon_alias)
        // @return object
        function getCouponObj(coupon_alias) {
            for (var i = 0; i < obj.availableDiscount.length; i++) {
                if (obj.availableDiscount[i].coupon_alias === coupon_alias) {
                    //console.log('coupon', obj.availableDiscount[i]);
                    //if (obj.availableDiscount[i].type == "amount") obj.availableDiscount[i].price = obj.availableDiscount[i].amount;
                    return obj.availableDiscount[i];
                }
            }
        }

        obj.setDiscount = function (coupon_alias, ev) {
            var confirm = $mdDialog.confirm()
                   .parent(angular.element(document.body))
                   .title(uiLanguage[INFO.lang]['alert'])
                   .content(uiLanguage[INFO.lang]['confirm_set_discount'])
                   // .content('Are you sure to delete the groups?')
                   .ok(uiLanguage[INFO.lang]['yes'])
                   .cancel(uiLanguage[INFO.lang]['cancel'])
                   .targetEvent(ev);
            $mdDialog.show(confirm).then(function () {
                var couponObj = getCouponObj(coupon_alias);
                //console.log('set discount');
                console.log(couponObj);
                //obj.appliedDiscount = [couponObj];
                //console.log(this.inputPrice);
                //console.log(couponObj)
                //var priceObj = obj.calcPrice([obj.Cart.orderList])
                //console.log(obj.Cart.couponObj.push);
                //obj.Cart.couponObj = [couponObj];
                //var _o = {};
                //$.extend(true, _o, couponObj, obj.schema.baseDiscount);
                $.extend(true, couponObj, obj.schema.baseDiscount);
                if (couponObj.type === 'item') {
                    $.each(obj.Cart.orderList, function (idx, item) {
                        if (item.isEdit) {
                            item.discount.push(couponObj);
                            console.log(couponObj);
                            //console.log(couponObj);
                            //console.log(item);
                        }
                    });
                    return;
                }
                if (couponObj.type === 'percentage' || couponObj.type === 'serviceCharge') {
                    for (var i = 0; i < obj.Cart.couponObj.length; i++) {
                        if (obj.Cart.couponObj[i].type === 'percentage' || obj.Cart.couponObj[i].type === 'serviceCharge') {
                            obj.deleteDiscount(i);
                            break;
                        }
                    }
                }
                obj.Cart.couponObj.push(couponObj);
                //console.log(obj.Cart.couponObj);
                // exact amount add later
                // this.addAmount( couponObj.amount ) // is numeric
                obj.remainder();
                obj.checkPaymentTips(false);

            }, function () {
                //alert('You decided to keep your debt.');
            });
        };

        obj.deleteDiscount = function (index) {
            //obj.Cart.couponObj.pop();
            obj.Cart.couponObj.splice(index, 1);
            //this.appliedDiscount.pop();
            this.remainder();
        }

        // old method: change text only, no multiple
        // obj.selectPaymentMethod = function( methodName ) {
        //     console.log('methodName ' + methodName);
        //     if(angular.isUndefined( methodName ))
        //         this.paymentMethod = uiLanguage[INFO.lang]['no_selection'];
        //     this.paymentMethod = uiLanguage[INFO.lang][methodName];
        // }

        obj.addAmount = function (amount) {
            if (isNaN(this.inputPrice) || this.inputPrice == "") {
                this.inputPrice = 0;
            }
            //alert(this.inputPrice);
            console.log(this.inputPrice); // before modification
            this.inputPrice = String(parseFloat(this.inputPrice) + amount);
            this.updatePaymentAmount(this.inputPrice);
            this.remainder();
        }

        obj.inputExact = function (option) {

            if (MainService.deposit.appliedDeposit.length != 0) {
                if (MainService.deposit.appliedDeposit[0].refNo != null) {
                    if (MainService.appliedPaymentMethod.length == 1) return;
                    if (MainService.multiPayment.isEnable() && MainService.appliedPaymentMethod.length > 2) return;
                } else {
                    if (MainService.multiPayment.isEnable() && MainService.appliedPaymentMethod.length >= 2) return;
                }
            } else {
                if (MainService.multiPayment.isEnable() && MainService.appliedPaymentMethod.length >= 2) return;
            }


            //if (MainService.multiPayment.isEnable() && MainService.appliedPaymentMethod.length >= 2) return;

            switch (option) {
                default:
                    this.inputPrice = String(this.Cart.totalPrice);
                    console.debug(this.inputPrice);
                    this.updatePaymentAmount(this.inputPrice);
                    this.remainder();
                    break;
                case 'matchTotal':
                    this.inputPrice = this.Cart.totalPrice;
                    if (this.appliedPaymentMethod) {
                        if (this.appliedPaymentMethod.length > 1) {
                            for (var i = 0; i < this.appliedPaymentMethod.length - 1; i++) {
                                if (i == 0 && this.deposit.appliedDeposit.length != 0 && this.deposit.appliedDeposit[0].refNo != null) continue;
                                this.inputPrice = this.inputPrice - this.appliedPaymentMethod[i].amount;
                                console.log('this.appliedPaymentMethod[i].amount; ' + this.appliedPaymentMethod[i].amount);
                                console.log('totalPriceInput ' + this.inputPrice);
                            }
                            // console.log('this.Cart.totalPrice ' + this.Cart.totalPrice);
                            // console.log('this.inputPrice ' + this.inputPrice);
                        }

                        if (this.appliedPaymentMethod.length == 1) {
                            this.inputPrice = this.Cart.totalPrice;
                        }

                        if (this.inputPrice != 0) {
                            if (this.deposit.appliedDeposit.length != 0 && this.deposit.appliedDeposit.refNo == null) {
                                this.inputPrice -= this.deposit.appliedDeposit[0].paymentPrice;
                            }
                            this.updatePaymentAmount(String(this.inputPrice));
                            this.remainder();
                        }
                    }



                    break;
            }
        }

        obj.inputPrice = '';
        obj.floatingInput = false;
        obj.appendAmount = function (amount) {
            console.log(amount);
            // do nothing if floating point is already input
            if (amount === '.' && this.inputPrice[this.inputPrice.length - 1] === '.') {
                console.log('0');
                return;
            }

            // if floating point is activated, not allow to add one character
            // this.inputPrice += '';
            this.inputPrice = String(this.inputPrice); // type aware method and meaning is obvious without comment
            if (this.inputPrice.indexOf('.') === -1 && amount === '.') {
                this.floatingInput = true;
                // not necessary because " ", it will make it .# instead of 0.# when it is 0
                // if (this.inputPrice.trim() == "0") { // input 7, result: .7 instead of 0.7
                //     console.log('1-1');
                //     console.log(this.inputPrice.trim());
                //     this.inputPrice = amount;
                // }else{
                //     console.log('1-2');
                //     this.inputPrice = this.inputPrice.concat(amount);
                // }
                this.inputPrice = this.inputPrice.trim().concat(amount);
                console.log('1');
                return;
            }

            if (this.inputPrice.indexOf('.') != -1 && amount === '.') {
                console.log('2');
                return
            }

            // console.log(this.inputPrice.indexOf('.'));
            // console.log( !isNaN(parseInt(this.inputPrice[this.inputPrice.length - 1])));
            if (this.inputPrice.indexOf('.') != -1 && !isNaN(parseInt(this.inputPrice[this.inputPrice.length - 1]))) {
                console.log("3");
                return;
            }
            // console.log(typeof this.inputPrice);
            // console.log(typeof this.inputPrice.trim());
            // console.log('when is this.inputPrice.trim() == 0 ' + this.inputPrice.trim());

            if (this.inputPrice.trim() == "0") { // 排除預設等值有space的障礙
                console.log("4");
                this.inputPrice = amount;
            } else {
                console.log("5");
                this.inputPrice = this.inputPrice.concat(amount);
            }

            ////this.inputPrice = this.inputPrice.concat(amount);
            this.updatePaymentAmount(this.inputPrice);
            this.remainder();
            //// default
        }

        obj.remainerAmount = '0.0';
        // obj.totalPriceInput = '0.0';
        obj.remainder = function () {

            if (obj.mode == obj.schema.mode.deposit && obj.modeDeposit == obj.schema.modeDeposit.input) return;

            console.log('remainder');
            // support multiple payment method
            var totalPriceInput = 0,
                totalCouponAmount = 0;

            // exact amount coupon add later
            /*// if after applied coupon, remainer >= 0, then no need for "payment change" and not require any payment method (cash will be set, 0 will be assumed)
            for (var i = 0; i < this.appliedDiscount.length; i++) {
                totalCouponAmount += this.appliedDiscount[i].amount;
            }
            this.remainerAmount = parseFloat(totalCouponAmount) - parseFloat(this.Cart.totalPrice);*/

            //alert(this.Cart.totalPrice)
            //if( this.remainerAmount < 0 ) {
            //console.log('.............');
            //console.log(this.appliedPaymentMethod);
            // console.log(this.appliedPaymentMethod);
            // console.log(totalPriceInput);
            // this.appliedPaymentMethod = [];
            if (this.appliedPaymentMethod) {
                console.log('appliedPaymentMethod');

                for (var i = 0; i < this.appliedPaymentMethod.length; i++) {
                    console.log(this.appliedPaymentMethod[i].amount);
                    obj.totalPriceInput = totalPriceInput += this.appliedPaymentMethod[i].amount;
                }
                this.remainerAmount = parseFloat(totalPriceInput) - parseFloat(this.Cart.totalPrice);
            }

            if (obj.deposit)
                obj.deposit.calcPrice();
            // this.totalPriceInput = String( totalPriceInput ); // for truncate...etc operation

            // old: only 1 inputPrice
            // this.remainerAmount = parseFloat(this.inputPrice) - parseFloat(this.Cart.totalPrice);

            //console.log(parseFloat(this.inputPrice));
            //console.log(parseFloat(this.Cart.price));
            //console.log(parseFloat(this.Cart.serviceCharge));

            if (isNaN(this.remainerAmount))
                this.remainerAmount = 0;
            this.checkPaymentTips();
        }

        obj.truncateAmount = function () {
            if (this.inputPrice[this.inputPrice.length - 1] === '.') {
                this.floatingInput = false;
            }
            // convert it to string or it will get error when it is 0 and use backspace
            this.inputPrice = String(this.inputPrice);
            this.inputPrice = this.inputPrice.substring(0, this.inputPrice.length - 1);

            if (isNaN(this.inputPrice) || this.inputPrice === '') {
                this.inputPrice = 0;
            }
            this.updatePaymentAmount(this.inputPrice);
            this.remainder();
        }



        // old method
        /*obj.truncateAmount = function() {
            if( this.inputPrice[this.inputPrice.length - 1] === '.' ) {
                this.floatingInput = false;
            }
            // convert it to string or it will get error when it is 0 and use backspace
            this.inputPrice = String( this.inputPrice );
            this.inputPrice = this.inputPrice.substring(0, this.inputPrice.length - 1);

            if( isNaN(this.inputPrice) || this.inputPrice === '' ) {
                this.inputPrice = 0;
            }
            this.remainder();
        }*/

        /*
            this newly created mode switcher is intended for generic purpose, lesser code and used in main.html first for
            login checking
            mode: main, floor, order, item
        */
        // deprecated, replaced by v2
        /*obj.goToMode = function( modeType, modeName ) {
            console.log('change to mode type ' + modeType);
            console.log('change to mode ' + modeName);

            if (modeName == "userManagement" || modeName == "report" || modeName == "order") {
                var p = modeName;
                switch (modeName) {
                    case "userManagement": p = 'userManagement'; break;
                    case "report": p = 'report'; break;
                    case "order": p = 'checkBill'; break;
                }
                obj.checkPermission(p, function () {
                    obj[modeType] = obj.schema[modeType][modeName];
                })
            } else {
                obj[modeType] = obj.schema[modeType][modeName];
            }

            // switch( modeType ) {
            //     case 'main': // mode (main mode)
            //         console.log('change mode');
            //         // this.mode = this.schema.mode[modeName];

            //         break;
            //     case 'floor': // modeFloorPlan

            //         break;
            //     case 'order': // modeOrder

            //         break;
            //     case 'item': // modeItem

            //         break;
            // }
        }*/
        // deprecated, replaced by v2
        /*obj.switchMode = function( modeType, modeName, requiredLogin ) {
            // all mode changed here required login by default
            //alert(1);
            var requiredLogin = ( angular.isDefined(requiredLogin) ) ? requiredLogin : true;

            if( requiredLogin && !obj.UserManager.isLogin ) {
                this.UserManager.checkLogin();
                obj.UserManager.LoginRedirectToMode = {
                    type: modeType,
                    mode: modeName
                }
            } else {
                this.goToMode( modeType, modeName );
            }
        }*/

        // test for multiple mode changing method, take modeOption with multiple modes at once inside
        // for future version
        // usage: obj.goToMode2({mode: 'save', mode: 'save2', mode4: 'save1'})
        // support change multiple mode at once
        obj.goToMode2 = function (modeOption, $event, setMessage) {
            setMessage = setMessage ? setMessage : true;
            // console.log($event);
            // console.log(modeOption.mode == 'dailyClearance');
            // reset respective page to default
            // console.log(this.modeCheckBase('cashManagement'));
            //console.log('****************************');
            //console.log('****************************');
            //console.log(modeOption.mode);
            //alert(obj.dailyClearanceCheck)

            if (modeOption.modeFloorPlan != 'editTable') {
                removeAllHandles();
            }

            console.log("goToMode2", modeOption.mode);
            if (modeOption.mode) {
                if ((modeOption.mode.toLowerCase() === "floorplan" || modeOption.mode.toLowerCase() === "takeaway" || modeOption.mode.toLowerCase() === "fastfood")) {
                    if (obj.dailyClearanceCheck) {
                        obj.showAlert('remind_daily_clearance');
                        obj.dailyClearanceCheck = false;
                        obj.mode = obj.schema.mode.main;
                        return;
                    }
                    if (obj.octopusBlacklistOutdated) {
                        //obj.showAlert('octopus_warning_blkOutated');
                        obj.octopusBlacklistOutdated = false;
                        return;
                    }
                }
                //if ((modeOption.mode.toLowerCase() === "floorplan" || modeOption.mode.toLowerCase() === "takeaway") && obj.dailyClearanceCheck) {
                //    obj.showAlert('remind_daily_clearance');
                //    obj.dailyClearanceCheck = false;
                //    return;
                //}
                //if ((modeOption.mode.toLowerCase() === "floorplan" || modeOption.mode.toLowerCase() === "takeaway") && obj.octopusBlacklistOutdated) {
                //    obj.showAlert('octopus_warning_blkOutated');
                //    obj.octopusBlacklistOutdated = false;
                //    return;
                //}
            }
            // console.log(modeOption);

            if (modeOption.mode == 'cashManagement' && this.modeCash != this.schema.modeCash.normal) {
                // console.log(this.resetPage);
                // this.initCashControl();
                // this.resetPage['cashManagement']();
            }

            if (modeOption.mode == 'dailyClearance' && this.sceneInitStatus['dailyClearance']) {
                this.resetPage['dailyClearance']();
            }

            if (modeOption.mode == 'report' && this.sceneInitStatus['report']) {
                this.initReport();
                // console.log(this.resetPage);
            }

            if (modeOption.modeFloorPlan == 'editTable') {
                obj.setActive(angular.element('.btn-edit-table'));
            } else {
                // place here to control the nav menu, avoid too much settings in controller
                // obj.modeNavMenu = obj.schema.modeNavMenu.normal;
            }


            // if( obj.isModeNavMenu() ) {
            //     obj.modeNavMenu = obj.schema.modeNavMenu.normal;
            // }


            if (modeOption.modeFloorPlan == 'order') {
                // console.log('called me');
                // console.log(Snap('#svg-canvas').selectAll('.modified'));
                // var modified = Snap('#svg-canvas').selectAll('.modified');
                // console.log(modified);
                // console.log($('#svg-canvas .modified'));
                Snap('#svg-canvas').selectAll('.modified').forEach(function (element) {
                    if (element.freeTransform) {
                        element.freeTransform.unplug();
                    }
                    element.removeClass('modified')

                });
                obj.setActive(angular.element('.btn-order'));
            }


            // console.log('test test');
            if (modeOption.modeMain === 'cashbox') {
                obj.checkPermission('cashbox', function () {
                    SocketFactory.emit('openCashBox', { 'till': userConfig.page.till }, function (r) { });
                }, function () { obj.showAlert('no_permission'); });
            }
            //watch management
            if (modeOption.modeMain === 'watch') {
                //'<button class="md-raised md-button-text md-button" style="background-color:#2B2B8C; color:white;" >' +
                //'<span class="ng-binding ng-scope"> ' + uiLanguage[INFO.lang]["watch_management_unregister"] + ' </span></button>'

                var alert = $mdDialog.show({
                    template:
                  '<md-dialog aria-label="List dialog" ng-controller="watchManagemenetController as wm" ng-init="init();">' +
                  '<div class="md-toolbar-tools" style="background-color:rgb(121,85,72); color:white;" >   ' +
                  '<h2>' + uiLanguage[INFO.lang]["watch_management"] + '</h2>' +
                        '<span flex=""></span>  ' +
                           ' <button class="md-icon-button md-button md-default-theme" ng-click="closeDialog();" > ' +
                           '<i class="md-icon dp45 ng-scope">clear</i> ' +
                           '</button> ' +
                    '</div>' +
                  '  <md-dialog-content>' +
                  '<button class="md-raised md-button-text md-button" ng-click="callKeypad($event)" style="background-color:#0D752D; color:white;width:100%;margin:0" >' +
                   '<span class="ng-binding ng-scope"> ' + uiLanguage[INFO.lang]["watch_management_register"] + ' </span></button>' +
                  '<p></p>' +
                  '</md-dialog-content>' +
                  '</md-dialog>',
                    controller: function ($scope, $rootScope) {
                        $scope.init = function () {
                            delete $rootScope.watchManagement;
                        }

                        $scope.closeDialog = function () {
                            $mdDialog.hide()
                        }
                    }
                });
            }

            //     this.switchMode2({modeCash:'normal'});
            // console.log(modeOption);
            for (var key in modeOption) {
                // console.log('mode type ' + key);
                // console.log('mode name ' + modeOption[key]);
                obj[key] = obj.schema[key][modeOption[key]];
                //obj[key] = obj.schema[key][modeOption[key]];
            }

            // if( typeof modeOption.modeFloorPlan != 'undefined' && modeOption.modeFloorPlan === 'order' ) {
            //     this.setActive( angular.element('.btn-order') );
            // }

            // cancel transfer table
            this.cancelTransfer();

            if (setMessage) {
                // only really switch after login or direct or execute here, title would be changed
                this.currentPageTitle = this.pageTitle;
                // console.log(this.statusMessage);
                this.showStatusMessage(this.statusMessage);
            }


            // if( typeof $event != 'undefined' ) {
            //     this.setActive( $event );
            // }
        }

        obj.switchMode2 = function (modeOption, requiredLogin, pageTitle, message, $event) {
            // all mode changed here required login by default
            var requiredLogin = (angular.isDefined(requiredLogin)) ? requiredLogin : true,
                pageTitle = (angular.isDefined(pageTitle)) ? pageTitle : '',
                message = (angular.isDefined(message) && message != '') ? uiLanguage[INFO.lang][lowerFirstLetter(message)] : uiLanguage[INFO.lang][pageTitle];

            this.pageTitle = uiLanguage[INFO.lang][pageTitle];
            this.statusMessage = message;
            // console.log(pageTitle);
            // this.showStatusMessage( message );

            if (requiredLogin && !obj.UserManager.isLogin) {
                this.UserManager.checkLogin();
                obj.UserManager.LoginRedirectToMode2 = modeOption;
                obj.UserManager.LoginRedirectToCaller = $event;
            } else {
                this.goToMode2(modeOption, $event);
            }

            // console.log(arguments);
            // console.log("message " + message);
            // show status

            // if( angular.isDefined( message ) ) {
            // console.log('show message?');
            //     this.showStatusMessage( message );
            // }
        }


        // display mode organizing in consolidated functions, try to group here to see how to generalize it later
        // eg modeChecking( [{'modeItem': ['mode', 'normal']}, {'modeOrder': ['normal', 'detail', 'foodControl']}] )
        obj.modeChecking = function (option) {
            // this = obj

            var eachModeCheck = [], // for || checking
                displayCheck = true; // for && checking
            for (var i = 0; i < option.length; i++) {
                // console.log('mode testing inside array');



                // console.log('------------------------------------------------');
                for (var key in option[i]) {
                    for (var m = 0; m < option[i][key].length; m++) {
                        eachModeCheck[i] = eachModeCheck[i] || this[key] == this.schema[key][option[i][key][m]];
                        // console.log('this[key] ' + this[key] + ' vs ' + this.schema[key][option[i][key][m]]);
                        // console.log(this[key] == this.schema[key][option[i][key][m]]);
                    }
                }
                // console.log('------------------------------------------------');
                for (var c = 0; c < eachModeCheck.length; c++) {
                    displayCheck = displayCheck && eachModeCheck[c];
                }
            }
            // obj.modeCheck = displayCheck;
            // return displayCheck;
            // console.log(option);
            // console.log(displayCheck); // check
            return displayCheck;
        }

        obj.modeCheckUserGroup = function (option) {
            var option = angular.isUndefined(option) ? 'list' : option;
            // MainService.mode == MainService.schema.mode.floorPlan
            return this.modeChecking([{ mode: [option] }]);
        }

        // default: floorplan, tablelist
        obj.modeCheckBase = function (option) {
            var option = angular.isUndefined(option) ? ['floorPlan', 'tableList'] : [option];
            // MainService.mode == MainService.schema.mode.floorPlan
            return this.modeChecking([{ mode: option }]);
        }
        obj.isMode = obj.modeCheckBase;
        obj.isNotMode = function () {
            return !obj.modeCheckBase.apply(obj, arguments);
        }

        obj.isModeFastFood = function (mode) {
            var mode = angular.isUndefined(mode) ? ['normal'] : mode,
                mode = angular.isArray(mode) ? mode : [mode];

            return obj.modeChecking.apply(obj, [[{ modeFastFood: mode }]]);
        }

        obj.isModeBuildBillFromVoid = function () {
            var mode = angular.isUndefined(mode) ? ['normal'] : mode,
               mode = angular.isArray(mode) ? mode : [mode];

            return obj.modeChecking.apply(obj, [[{ modeBuildBillFromVoid: mode }]]);
        }

        obj.isModeTakeAway = function (mode) {
            var mode = angular.isUndefined(mode) ? ['normal'] : mode,
                mode = angular.isArray(mode) ? mode : [mode];

            return obj.modeChecking.apply(obj, [[{ modeTakeAway: mode }]]);
        }

        obj.isModeDeliveryOrder = function (mode) {
            var mode = angular.isUndefined(mode) ? ['normal'] : mode,
                mode = angular.isArray(mode) ? mode : [mode];
            return obj.modeChecking.apply(obj, [[{ modeDeliveryOrder: mode }]]);
        }

        obj.isNotModeTakeAway = function () {
            return !obj.isModeNavMenu.apply(obj, arguments);
        }

        obj.isModeNavMenu = function (mode) {
            var mode = angular.isUndefined(mode) ? ['normal'] : mode,
                mode = angular.isArray(mode) ? mode : [mode];

            return obj.modeChecking.apply(obj, [[{ modeNavMenu: mode }]]);
        }

        obj.isNotModeNavMenu = function () {
            return !obj.isModeNavMenu.apply(obj, arguments);
        }

        obj.modeCheckOrder = function (option) {
            var option = angular.isUndefined(option) ? 'normal' : option;
            // {\'mode\': [\'order\']},\'modeOrder\': [\'normal\']}
            // MainService.mode == MainService.schema.mode.order && MainService.modeOrder == MainService.schema.modeOrder.normal
            // MainService.modeOrder === MainService.schema.modeOrder.transfer
            // MainService.modeOrder == MainService.schema.modeOrder.detail
            switch (option) {
                default:
                    option = angular.isArray(option) ? option : [option];
                    return this.modeChecking([{ mode: ['order'] }, { modeOrder: option }]);
                    break;
                case 'order':
                    return this.modeChecking([{ mode: ['order'] }, { modeOrder: ['normal', 'detail', 'specialMessage', 'transfer'] }]);
                    break;
                case 'detailNormal':
                    return this.modeChecking([{ mode: ['order'] }, { modeOrder: ['normal', 'detail'] }]);
                    break;
                case 'foodMenu':
                    return this.modeChecking([{ mode: ['order'] }, { modeOrder: ['normal', 'detail', 'foodControl'] }]);
                    break;
                case 'all':
                    return this.modeChecking([{ mode: ['order'] }]);
                    break;
            }
        }

        obj.isModeOrder = obj.modeCheckOrder; // for better semantic
        obj.isNotModeOrder = function () {
            return !obj.isModeOrder.apply(obj, arguments);
        }; // for better semantic

        obj.modeCheckOrderItem = function (option) {
            var option = angular.isUndefined(option) ? ['normal'] : option;
            option = angular.isArray(option) ? option : [option];

            // {\'mode\': [\'order\']},\'modeOrder\': [\'normal\']}
            // MainService.mode == MainService.schema.mode.order && MainService.modeOrder == MainService.schema.modeOrder.normal
            // MainService.modeOrder === MainService.schema.modeOrder.transfer
            // MainService.modeOrder == MainService.schema.modeOrder.detail
            // return this.modeChecking([{mode: ['order']}, {modeItem: [option]}]);
            switch (option) {
                default:
                    return this.modeChecking([{ mode: ['order'] }, { modeItem: option }]);
                    break;
                case 'normalOption':
                    // MainService.modeItem != MainService.schema.modeItem.cancel && MainService.modeItem != MainService.schema.modeItem.modifier
                    return this.modeChecking([{ mode: ['order'] }, { modeItem: ['option', 'normal'] }]);
                    break;
            }
        }

        obj.modeCheckOrderItem2 = function (option) {
            var option = angular.isUndefined(option) ? ['normal', 'option'] : option;
            option = angular.isArray(option) ? option : [option];

            // {\'mode\': [\'order\']},\'modeOrder\': [\'normal\']}
            // MainService.mode == MainService.schema.mode.order && MainService.modeOrder == MainService.schema.modeOrder.normal
            // MainService.modeOrder === MainService.schema.modeOrder.transfer
            // MainService.modeOrder == MainService.schema.modeOrder.detail
            // return this.modeChecking([{mode: ['order']}, {modeItem: [option]}]);
            return this.modeChecking([{ mode: ['order'] }, { modeItem: option }]);
        }

        obj.isModeItem = obj.modeCheckOrderItem2;
        obj.isNotModeItem = function () {
            return !obj.modeCheckOrderItem2.apply(obj, arguments);
        };

        obj.modeCheckOrderItemDetail = function (option) {
            var option = angular.isUndefined(option) ? 'code' : option;
            return this.modeChecking([{ mode: ['order'] }, { modeItemDetail: [option] }]);
        }

        obj.isModeItemDetail = obj.modeCheckOrderItemDetail;
        obj.isNotModeItemDetail = function () {
            return !obj.modeCheckOrderItemDetail.apply(obj, arguments);
        }

        obj.modeCheckBill = function (option) {
            switch (option) {
                default:
                case 'all':
                    //obj.appliedPaymentMethod = [];
                    return this.modeChecking([{ 'mode': ['order'] }, { 'modeOrder': ['amendBill', 'searchBill', 'printBill', 'billing'] }]); //all
                    break;
                case 'searchAmendBill':
                    return this.modeChecking([{ 'mode': ['order'] }, { 'modeOrder': ['amendBill', 'searchBill', 'billing'] }]); // searchAmendBill
                    break;
                case 'printAmendBill':
                    return this.modeChecking([{ 'mode': ['order'] }, { 'modeOrder': ['amendBill', 'printBill', 'billing'] }]); // printAmendBill
                    break;
                case 'billPrint':
                    return this.modeChecking([{ 'mode': ['order'] }, { 'modeOrder': ['printBill', 'billing'] }]); // printAmendBill
                    break;
                case 'billOnly':
                    return this.modeChecking([{ 'mode': ['order'] }, { 'modeOrder': ['billing'] }]); // printAmendBill
                    break;
                case 'summary':
                    return this.modeChecking([{ 'mode': ['order'] }, { 'modeOrder': ['billingSummary'] }]); // printAmendBill
                    break;
                case 'searchBill':
                    return this.modeChecking([{ 'mode': ['order'] }, { 'modeOrder': ['searchBill'] }]); // printAmendBill
                    break;
                case 'amendBill':
                    return this.modeChecking([{ 'mode': ['order'] }, { 'modeOrder': ['amendBill'] }]); // printAmendBill
                    break;
                case 'deposit':
                    return this.modeChecking([{ 'mode': ['order'] }, { 'modeOrder': ['deposit'] }]);
                    break;
            }
            // return this.modeChecking([{'mode': ['order']}, {'modeOrder': ['printBill', 'billing']}]); // printBill
        }

        obj.isModeBill = obj.modeCheckBill;
        obj.isNotModeBill = function () {
            return !obj.modeCheckBill.apply(obj, arguments);
        };

        // replaced by modeOrderMenu
        obj.modeCheckOrderFood = function () {
            // this.schema.modeItem.normal,
            // this.schema.modeItem.option
            // ],
            // condArr2 = [
            // this.schema.modeOrder.normal,
            // this.schema.modeOrder.detail,
            // this.schema.modeOrder.foodControl
            // ];
            // conditions
            // console.log(typeof this.modeChecking([{'mode': ['report']}]));
            return this.modeChecking([{ 'mode': ['order'] }, { 'modeItem': ['option', 'normal', 'optionFoodControl'] }, { 'modeOrder': ['foodControl', 'detail', 'normal', 'zeroItem'] }]);
        }

        obj.isModeOrderMenu = function (option) {
            option = option ? option : 'normal';
            // console.log('isModeOrderMenu / ' + typeof option);
            // console.log('isModeOrderMenu / ' + option);
            // obj.modeOrderMenu = obj.schema.modeOrderMenu.normal;
            return this.modeChecking([{ 'mode': ['order'] }, { 'modeOrderMenu': [option] }]);
        }
        obj.isNotModeOrderMenu = function () {
            return !obj.isModeOrderMenu.apply(obj, arguments);
        }

        obj.modeCheckFloor = function (option) {
            return this.modeChecking([{ 'modeFloorPlan': [option] }]);
        }

        obj.isModeFloor = obj.modeCheckFloor;
        obj.isNotModeFloor = function () {
            return !obj.modeCheckFloor.apply(obj, arguments);
        };

        // obj.checkItemDeleteDisplay2  = function() {
        //     return this.modeChecking([{'mode': ['order']}, {'modeOrder': ['detail', 'normal']}]);
        // }

        /*obj.checkItemDeleteDisplay  = function() {
            var conditions1 = this.mode == this.schema.mode.order,
                conditions2 = this.modeOrder == this.schema.modeOrder.normal,
                conditions3 = this.modeOrder == this.schema.modeOrder.detail

            return (conditions1) && (conditions2 || conditions3);
        }*/

        /*obj.checkFoodMenuDisplay = function() {
            // conditions
            // MainService.modeItem != MainService.schema.modeItem.cancel && MainService.modeItem != MainService.schema.modeItem.modifier && MainService.modeOrder == MainService.schema.modeOrder.normal

            var condArr1 = [
                this.schema.modeItem.normal,
                this.schema.modeItem.option
                ],
                condArr2 = [
                this.schema.modeOrder.normal,
                this.schema.modeOrder.detail,
                this.schema.modeOrder.foodControl
                ];

            if( condArr1.indexOf(this.modeItem) >= 0 && condArr2.indexOf(this.modeOrder) >= 0 ) {
                // console.log('yes');
                return true;
            }

            // console.log(this.schema.modeItem.normal);
            // console.log(this.schema.modeItem.option);
            // console.log(this.schema.modeOrder.normal);
            // console.log(this.schema.modeOrder.detail);
        }*/

        // for switch between floor plan/order functions and main menu functions
        /*obj.resetAllSubMode = function() {
            this.modeOrder = {};
            this.modeItem = {};
            this.modeItemDetail = {};
            this.modeFloorPlan = {};
        }*/

        obj.switchModeFloorPlan = function (modeChange) {
            // console.log('called me');
            obj.UserManager.checkLogin();
            this.modeFloorPlan = modeChange;
            //console.log("%%% modeFloorPlan mode: " + this.modeFloorPlan);
            //console.log("%%% modeOrder mode: " + this.schema.modeOrder);
            // reset payment method to nothing then add cash as default in submitcart calculation
        }

        obj.printOrder = function (tableNo, type) {
            //console.log('print ' + type + ' here');
            // add item discount
            var itemarray = [];

            angular.forEach(obj.Cart.orderList, function (item) {
                itemarray = itemarray.concat(item.getJson());
            });

            SocketFactory.emit('print', { "tableNo": tableNo, "action": type, discount: obj.Cart.couponObj, "user": obj.UserManager.staffDetails.staffId, printer: userConfig.page.printer, "item": itemarray });

            if (type == "bill") {
                console.debug('type == "bill"', tableNo);
                obj.lockTable.toReleaseLockTable(tableNo);
            }

        }

        obj.findItemFromFood = function (itemCode) {
            var found = {};
            $.each(obj.food.category, function (catIndex, cat) {
                $.each(cat.items, function (itemIndex, item) {
                    if (item.code == itemCode) {
                        $.extend(true, found, item);
                    }
                })
            });
            return found;
        }

        obj.Cart = {
            checkIsExistTItem: function () {
                if (obj.Cart.orderList.length > 0) {
                    var templItem = $.grep(obj.Cart.orderList, function (order) {
                        return order.type == "T";
                    })
                    return templItem.length != 0;
                } else
                    return false;
            },
            cartList: [], orderList: [], splitListRPrice: 0, splitListRServiceCharge: 0, splitListRTotalPrice: 0, splitListRNoofpeople: 0, splitListLPrice: 0, splitListLServiceCharge: 0, splitListLTotalPrice: 0, splitListLNoofpeople: 0, splitListRIndex: 1, splitListLIndex: 0, allSplitList: [], tableNo: '', noOfPeople: '', totalPrice: 0, serviceCharge: 0, price: 0, currentSelectedSplitIndex: 0, member: {}, refNo: '',
            currentOption: null, currentOptionIndex: -1, currentOptionList: { "items": [] }, voidItemCount: uiLanguage[INFO.lang]['all'].toLowerCase(), remainings: 0,
            cancelPackage: function () {
                obj.modeItem = obj.schema.modeItem.normal;
                obj.Cart.cartList.pop();
            },
            assignMemberToOrder: function (member) {
                console.log(member);
                obj.Cart.member = member;
                $mdDialog.cancel();
            },
            addModifier: function (m, $event) {
                console.log('addModifier');
                console.log(m);
                console.log(obj.Cart.cartList);
                $.each(obj.Cart.cartList, function (idx, item) {

                    // option list
                    var optionIsEdit = false;


                    if (item.option != undefined) {
                        $.each(item.option, function (idx, optionList) {
                            $.each(optionList.items, function (oidx, option) {

                                // O9999 is unselected option
                                if (option.isEdit && option.code != 'O9999') {
                                    optionIsEdit = true;
                                    console.log(option);
                                    var foundModifierIdx = -1;
                                    $.each(option.modifier, function (midx, mitem) {
                                        if (mitem.code == m.code) {
                                            foundModifierIdx = midx
                                        }
                                    })
                                    if (foundModifierIdx == -1) {
                                        option.modifier.push(m);
                                    }
                                    else {
                                        option.modifier.splice(foundModifierIdx, 1);
                                    }
                                }
                            });
                            //alert(option.modifier.length)
                        });
                    }

                    // subitem list
                    var subitemIsEdit = false;
                    if (item.subitem != undefined) {
                        console.log('subitem is not undefined');
                        $.each(item.subitem, function (sidx, subitem) {
                            // $.each(optionList.items, function(oidx, option) {

                            if (subitem.isEdit) {
                                subitemIsEdit = true;
                                console.log(subitem);
                                var foundModifierIdx = -1;
                                $.each(subitem.modifier, function (midx, mitem) {
                                    if (mitem.code == m.code) {
                                        foundModifierIdx = midx
                                    }
                                })
                                if (foundModifierIdx == -1) {
                                    console.log('new modifier');
                                    subitem.modifier.push(m);
                                }
                                else {
                                    subitem.modifier.splice(foundModifierIdx, 1);
                                }
                            }
                            // });
                            //alert(subitem.modifier.length)
                        });
                    }


                    // option/subitem is not in edit mode + no option and no subitem => add modifier into parent item
                    if (!optionIsEdit && !subitemIsEdit) {
                        // item is in edit mode (item is option's parent item)
                        if (item.isEdit) {
                            //console.debug('item.isEdit')
                            if (item.option == undefined && item.subitem == undefined || true) {
                                var foundModifierIdx = -1;
                                $.each(item.modifier, function (midx, mitem) {
                                    if (mitem.code == m.code) {
                                        foundModifierIdx = midx
                                    }
                                })
                                if (foundModifierIdx == -1) {
                                    item.modifier.push(m);
                                } else {
                                    item.modifier.splice(foundModifierIdx, 1);
                                }
                            }
                        }
                    }
                });

                if ($event) {
                    $event.stopPropagation();
                }
            },
            deleteModifier: function (modifierList, mIdx) {
                modifierList.splice(mIdx, 1);
            },
            addKitchenMsg: function (k, $event, idx) {
                // console.log(k); // the new kitchen message object just click to add
                // cartList = new order list
                // orderList = existing order list

                // console.log(m);
                console.log('addKitchenMsg', [obj.Cart.cartList, obj.Cart.orderList]);

                // control when it use existing order or new order list
                var orderCart = [];
                if (obj.modeFloorPlan == obj.schema.modeFloorPlan.kitchenMessage) {
                    orderCart = [obj.Cart.orderList]; // existing order
                } else {
                    orderCart = [obj.Cart.cartList]; // new order
                }

                // consolidate all order list(new/existing) then iterate each order list
                $.each(orderCart, function (i, list) {

                    // first new order list, then existing order list
                    $.each(list, function (idx, item) {
                        // "item" means each food item(a food item include modifiers, options and subitems)
                        console.log('addKitchenMsg', item);
                        console.log('kitchenMsg', k);

                        var target = obj.combo.selectMode.allItemList.filter(function (f) {
                            return f.itemId == item.code;
                        })

                        if (target.length == 0) target[0] = item;
                        target = target[0];

                        var optionIsEdit = false;
                        var subItemIsEdit = false;
                        if (!target.holdKitchenPrinter && k.code == MainService.fireCode) {
                            console.log(target);
                            console.log('return fireMessage');
                            return true;
                        }
                        // Option / Type: Array(include sub list)
                        if (item.option != undefined) {

                            // iterate all options in the Array
                            $.each(item.option, function (idx, optionList) {

                                // in each option, it can be multiple items
                                // optionList.items / Type: Array, name example: "飛魚籽魷魚丼", code is unique
                                $.each(optionList.items, function (oidx, option) {
                                    console.log('option.isEdit ' + option.isEdit);
                                    if (option.isEdit && option.code != 'O9999') {
                                        optionIsEdit = true;
                                        console.log(option);
                                        var foundModifierIdx = -1;
                                        $.each(option.kitchenMsg, function (midx, mitem) {
                                            if (mitem.code == k.code) {
                                                foundModifierIdx = midx
                                            }
                                        });

                                        $.each(option.kitchenMsgOld, function (kOldidx, kOlditem) {
                                            console.log('kOlditem.code ', kOlditem.code);
                                            console.log('k.code ', k.code);
                                            if (kOlditem.code == k.code) {
                                                foundModifierIdx = kOldidx;
                                            }
                                        });

                                        if (foundModifierIdx == -1) {
                                            option.kitchenMsg.push(k);
                                        }
                                        // else {
                                        //     option.kitchenMsg.splice(foundModifierIdx, 1);
                                        // }
                                    }
                                });
                                //alert(option.kitchenMsg.length)
                            });
                        }

                        // Subitem / Type: Array
                        if (item.subitem != undefined) {
                            console.log('add kitchen message into subitem');

                            // iterate all options in the Array
                            $.each(item.subitem, function (idx, subitem) {

                                // each subitem
                                console.log('subitem.isEdit ' + subitem.isEdit);
                                if (subitem.isEdit) {
                                    subItemIsEdit = true;
                                    console.log(subitem);
                                    var foundModifierIdx = -1;
                                    $.each(subitem.kitchenMsg, function (midx, mitem) {
                                        if (mitem.code == k.code) {
                                            foundModifierIdx = midx
                                        }
                                    });

                                    $.each(subitem.kitchenMsgOld, function (kOldidx, kOlditem) {
                                        if (kOlditem.code == k.code) {
                                            foundModifierIdx = kOldidx;
                                        }
                                    });

                                    if (foundModifierIdx == -1) {
                                        subitem.kitchenMsg.push(k);
                                    }
                                    // else {
                                    //     subitem.kitchenMsg.splice(foundModifierIdx, 1);
                                    // }
                                }
                                //alert(option.kitchenMsg.length)
                            });
                        }

                        // if no option/subitem is in edit mode(no child is selected)
                        if (!optionIsEdit && !subItemIsEdit) {
                            // item is in edit mode, being selected (item is option's parent item)
                            if (item.isEdit) {
                                // item option does not exist
                                if (item.option == undefined && item.subitem == undefined || true) {
                                    var foundIdx = -1;
                                    $.each(item.kitchenMsg, function (kidx, kitem) {
                                        // console.log('kitem');
                                        // console.log(kitem);
                                        if (kitem.code == k.code) {
                                            foundIdx = kidx;
                                        }
                                    });
                                    $.each(item.kitchenMsgOld, function (kOldidx, kOlditem) {
                                        // console.log('kOlditem');
                                        // console.log(kOlditem);
                                        if (kOlditem.code == k.code) {
                                            foundIdx = kOldidx;
                                        }
                                    });
                                    if (foundIdx == -1) {
                                        console.log('not found in any kitchen message');
                                        item.kitchenMsg.push(k);
                                    }
                                } else {
                                    // item option exist
                                    console.log('all kitchen message to all options and subitems');
                                    $.each(item.option, function (idx, optionList) {
                                        $.each(optionList.items, function (oidx, option) {
                                            // O9999 is unselected option
                                            if (option.code != 'O9999') {
                                                optionIsEdit = true;
                                                //console.log(option);
                                                var foundModifierIdx = -1;
                                                $.each(option.kitchenMsg, function (midx, mitem) {
                                                    console.log('found kitchenMsg');
                                                    if (mitem.code == k.code) {
                                                        foundModifierIdx = midx
                                                    }
                                                });

                                                $.each(option.kitchenMsgOld, function (mOldidx, mOlditem) {
                                                    console.log('found kitchenMsg old');
                                                    if (mOlditem.code == k.code) {
                                                        foundModifierIdx = mOldidx;
                                                    }
                                                });

                                                if (foundModifierIdx == -1) {
                                                    console.log('not found kitchenMsg => push');
                                                    option.kitchenMsg.push(k);
                                                }
                                                // else {
                                                //     option.kitchenMsg.splice(foundModifierIdx, 1);
                                                // }
                                            }
                                        });
                                        //alert(option.kitchenMsg.length)
                                    });

                                    $.each(item.subitem, function (idx, subitem) {
                                        console.log(subitem);
                                        // optionIsEdit = true;
                                        //console.log(option);
                                        var foundModifierIdx = -1;
                                        $.each(subitem.kitchenMsg, function (sidx, sitem) {
                                            console.log('found kitchenMsg / subitem');
                                            if (sitem.code == k.code) {
                                                foundModifierIdx = sidx
                                            }
                                        });

                                        $.each(subitem.kitchenMsgOld, function (mOldidx, mOlditem) {
                                            console.log('found kitchenMsg old');
                                            if (mOlditem.code == k.code) {
                                                foundModifierIdx = mOldidx;
                                            }
                                        });

                                        if (foundModifierIdx == -1) {
                                            console.log('not found kitchenMsg / subitem => push');
                                            subitem.kitchenMsg.push(k);
                                        }
                                        // else {
                                        //     option.kitchenMsg.splice(foundModifierIdx, 1);
                                        // }
                                        //alert(option.kitchenMsg.length)
                                    });
                                }
                                // else {
                                //     item.kitchenMsg.splice(foundIdx, 1);
                                // }
                            } else {
                                // console.log('item is not selected');
                            }
                        }

                        // if (!subItemIsEdit) {
                        //     // item is in edit mode, being selected (item is subitem's parent item)
                        //     if (item.isEdit) {
                        //         // item subitem does not exist
                        //         if (item.subitem == undefined) {
                        //             // var foundIdx = -1;
                        //             // $.each(item.kitchenMsg, function (sidx, sitem) {
                        //             //     // console.log('kitem');
                        //             //     // console.log(kitem);
                        //             //     if (kitem.code == k.code) {
                        //             //         foundIdx = kidx;
                        //             //     }
                        //             // });
                        //             // if (foundIdx == -1) {
                        //             //     console.log('not found in any kitchen message');
                        //             //     item.kitchenMsg.push(k);
                        //             // }
                        //         } else {
                        //             // item subitem exist
                        //             console.log('item subitem exist');
                        //             $.each(item.subitem, function (idx, subitem) {
                        //                 console.log(subitem);
                        //                 // $.each(subitemList.items, function (sidx, subitem) {
                        //                     // O9999 is unselected option
                        //                     // if (subitem.code != 'O9999') {
                        //                         // optionIsEdit = true;
                        //                         //console.log(option);
                        //                         var foundModifierIdx = -1;
                        //                         $.each(subitem.kitchenMsg, function (sidx, sitem) {
                        //                             console.log('found kitchenMsg / subitem');
                        //                             if (sitem.code == k.code) {
                        //                                 foundModifierIdx = sidx
                        //                             }
                        //                         });

                        //                         if (foundModifierIdx == -1) {
                        //                             console.log('not found kitchenMsg / subitem => push');
                        //                             subitem.kitchenMsg.push(k);
                        //                         }
                        //                         // else {
                        //                         //     option.kitchenMsg.splice(foundModifierIdx, 1);
                        //                         // }
                        //                     // }
                        //                 // });
                        //                 //alert(option.kitchenMsg.length)
                        //             });
                        //         }
                        //         // else {
                        //         //     item.kitchenMsg.splice(foundIdx, 1);
                        //         // }
                        //     } else {
                        //         // console.log('item is not selected');
                        //     }
                        // }

                        // currently subitem does not provide edit mode

                    });
                });
            },
            deleteKitchenMsg: function (kitchenMsgList, kIdx, $event) {
                kitchenMsgList.splice(kIdx, 1);
                if ($event) $event.stopPropagation();
            },
            clearKitchenMsg: function () {
                $.each([obj.Cart.cartList, obj.Cart.orderList], function (i, list) {
                    $.each(list, function (idx, item) {
                        console.log('clearKitchenMsg', item);
                        var optionIsEdit = false,
                            subitemIsEdit = false;

                        if (item.isEdit)
                            item.kitchenMsg = [];

                        if (item.option != undefined) {
                            $.each(item.option, function (idx, optionList) {
                                // console.log(optionList);
                                $.each(optionList.items, function (oidx, option) {
                                    if (option.isEdit) {
                                        optionIsEdit = true;
                                        console.log('clear kitchen msg');
                                        option.kitchenMsg = [];
                                    }
                                });
                            });
                        }

                        if (item.subitem != undefined) {
                            $.each(item.subitem, function (idx, subitem) {
                                if (subitem.isEdit) {
                                    subitemIsEdit = true;
                                    console.log('clear kitchen msg');
                                    subitem.kitchenMsg = [];
                                }
                            });
                        }

                        if (!optionIsEdit && !subitemIsEdit) {
                            console.log('clear all kitchen message');
                            if (item.option != undefined) {
                                $.each(item.option, function (idx, optionList) {
                                    $.each(optionList.items, function (oidx, option) {
                                        option.kitchenMsg = [];
                                    });
                                });
                            }

                            if (item.subitem != undefined) {
                                $.each(item.subitem, function (idx, subitem) {
                                    subitem.kitchenMsg = [];
                                });
                            }
                        }
                    });
                });
            },
            checkItemStock: function (item, qty, prevQty) {
                prevQty = prevQty ? prevQty : 0;
                var totalqty = qty == undefined ? 1 : qty;
                console.log('totalqty:' + totalqty);
                console.log('prevQty:' + prevQty);
                totalqty -= prevQty;
                var isValid = true;
                console.log('totalqty:' + totalqty);
                $.each(obj.Cart.cartList, function (idx, citem) {
                    if (citem.getQty() >= 0 && item.code == citem.code) {
                        totalqty += citem.getQty();
                    }
                });
                console.log('totalqty:' + totalqty);
                $.each(obj.stock.list, function (idx, sitem) {
                    if (sitem.code == item.code) {
                        if (sitem.stock <= 0) {
                            if (sitem.stock == -1)
                                obj.showAlert('no_stock');
                            if (sitem.stock == -3)
                                obj.showAlert('preparing');
                            isValid = false;
                            return;
                        } else if (sitem.stock < totalqty) {
                            var message = uiLanguage[INFO.lang]['stock_reminder'].replace("[TOTAL_QTY]", totalqty).replace("[CURRENT_STOCK]", sitem.stock);

                            obj.showAlert(message);
                            isValid = false;
                            return;
                        }
                    }
                });
                return isValid;

            },
            addItem: function (item, qty, isEdit) {
                console.log('obj.Cart  / addItem');
                console.log('item.voidIndex', item.voidIndex);
                var newItem = $.extend(true, {}, item, obj.schema.baseCartItem, { "voidIndex": item.voidIndex });
                console.log('newItem.voidIndex', newItem.voidIndex);
                if (item.voidIndex > 0) {
                    console.log('item.voidIndex > 0');
                    this.cartList.push(newItem);
                    newItem.editItem(null, false);
                    return newItem; // void item
                }
                newItem.init();
                if (obj.modeOrder != obj.schema.modeOrder.foodControl) {
                    // not food control
                    if (!obj.Cart.checkItemStock(newItem, 1)) {
                        // check cart stock
                        return false;
                    } else {
                        newItem.itemKey = ++obj.combo.itemKey;
                        this.cartList.push(newItem);
                        if (qty) {
                            newItem.qty = qty;
                        }
                        //alert(isEdit == undefined || isEdit);
                        if (isEdit == undefined || isEdit) {
                            newItem.editItem(null, false);
                        }
                        obj.combo.cartToMapOption(newItem);
                        if (newItem.comboItemKey)
                            obj.combo.scrollToCompositeItem(newItem.comboItemKey);

                        return newItem;
                    }
                }
                else if (obj.modeOrder == obj.schema.modeOrder.foodControl) {
                    console.log('foot control after newItem init');
                    console.log(newItem);
                    // food control
                    var foundItem = null;
                    $.each(obj.stock.list, function (idx, item) {
                        if (item.code == newItem.code) {
                            foundItem = item;
                        }
                    });
                    if (foundItem != null) {
                        foundItem.qty = '';
                        foundItem.editItem();
                        $timeout(function () {
                            obj.scrollToBottom();
                        }, 10);
                        return foundItem;
                    } else {
                        newItem.qty = '';
                        obj.stock.list.push(newItem);
                        newItem.editItem();
                        $timeout(function () {
                            obj.scrollToBottom();
                        }, 10);
                        return newItem;
                    }
                }
            },
            loadOption: function (item) {
                obj.modeItem = obj.schema.modeItem.optionFoodControl;
                // obj.Cart.currentOption = item.option;

                // show all option in item list
                // console.log(item.option);
                console.log('prepare extended option');

                var allOptions = [],
                    tmpOption = {},
                    optionCodeList = [];
                $.each(item.option, function (idx, optionList) {
                    if ($.inArray(optionList.optionGroupCode, optionCodeList) === -1) {
                        optionCodeList.push(optionList.optionGroupCode);
                        angular.forEach(optionList.items, function (item) {
                            // tmpOption = $.extend(true, {}, item, obj.schema.baseItem);
                            allOptions.push(item);
                        });
                    }
                })
                this.optionControlList = allOptions;
                console.log(allOptions);

                // this.selectedOption = [];
                // $.each(item.option, function (idx, optionList) {
                //     console.log(optionList.minQty);
                //     var fillOption = $.extend(true, {}, obj.schema.unselectOption, obj.schema.baseItem, { qty: 1, _qty: 1 });
                //     // if (idx == 0 && i == 0) {
                //     //     fillOption.isEdit = true;
                //     // }
                //     console.log(fillOption);
                //     var exists = false;
                //     angular.forEach(optionList.items, function(item) {
                //         if (item.optionCode === fillOption.optionCode) {
                //             exists = true;
                //         }
                //     });
                //     if (!exists) optionList.items.push(fillOption);
                //     console.log(this.currentOptionList);
                // });

                // var that = this;
                // var selectQty = this.selectQty ? this.selectQty : 0;
                // var unselectedCount = 0;
                // var callback = function(optionIndex, optionItem) {
                //     console.log("callback", that);
                //     if (that.currentOptionIndex == -1) {
                //         that.currentOptionIndex = optionIndex;
                //         that.currentOptionList = that.currentOption[optionIndex];
                //         optionItem.isEdit = true;
                //     }
                // }

                // $.each(this.currentOption, function(optionIndex, optionList) {
                //     $.each(optionList.items, function(optionItemIndex, optionItem) {
                //         // console.log(12345, optionItem.qty, optionItem.code);
                //         if (optionItem.qty > 0 && optionItem.code === 'O9999') {
                //             callback(optionIndex, optionItem);
                //             unselectedCount++;
                //         }
                //     });
                // });
                console.log(item);

            },

            /* 
                for new item (new cart list)
                new item => option
                feature: 
                    toggle selection
                    when option is selected, in modifer option list => click modifier => add modifier
            */
            cartListSelectOption: function ($event, idx, editItem, optionList, editOption) {
                console.log('cartListSelectOption');
                console.log(editOption);

                // console.log(this); // obj.Cart
                if (editItem.voidIndex > -1) return;
                editItem.isEdit = true; // toggle is not preferred

                // // if (!editItem.isEdit) editItem.isEdit = true;

                angular.forEach(this.cartList, function (item) {
                    if (item != editItem) item.isEdit = false;
                    angular.forEach(item.option, function (option) {
                        if (option != editOption) {
                            option.isEdit = false;
                        }
                        angular.forEach(option.items, function (optItem) {
                            if (optItem != editOption)
                                optItem.isEdit = false;
                        });
                    });
                });

                // //angular.forEach(editItem.option, function (option) {
                // //    if (option != editOption) {
                // //        option.isEdit = false;
                // //    }
                // //    angular.forEach(option.items, function(optItem) {
                // //        if (optItem != editOption)
                // //        optItem.isEdit = false;
                // //    });
                // //});
                // // optionList.isEdit = !optionList.isEdit;
                editOption.isEdit = !editOption.isEdit; // toggle
                this.editOption = editOption.isEdit;

                if (editOption.isEdit) {

                    this.editOptionCode = editOption.code;

                    if (obj.modeItem != obj.schema.modeItem.modifier) {
                        //this.currentOption = option;
                        this.currentOptionIndex = idx;
                        this.currentOptionList = optionList;
                        obj.modeItem = obj.schema.modeItem.option;
                    }
                }
                else {
                    // this.currentOption = null;
                    this.editOptionCode = '';
                    this.currentOptionList = [];
                    this.currentOptionIndex = -1;
                    // this.resetEditItem();
                    obj.modeItem = obj.schema.modeItem.normal;
                }

                if ($event) {
                    $event.stopPropagation();
                }
            },
            // for item in exiting order list(aka existing cart list, means ordered already)
            orderListEditOption: function ($event, idx, editItem, optionList, editOption) {

                console.log('cartListEditOption');
                //obj.Cart.resetEditItem();

                console.log(editItem);
                console.log($.extend(true, {}, optionList));
                console.log(editOption);
                editItem.isEdit = true;

                if (editItem.voidIndex > -1) return;
                //if (!editItem.isEdit) editItem.isEdit = true;
                console.debug(editOption.items);

                editOption.isEdit = !editOption.isEdit;

                if (!obj.modeMultiSelectItem) {
                    //obj.Cart.orderList.forEach(function (t) {
                    //    t.isEdit = false;
                    //}); // updated at 2016/06/20

                    obj.Cart.cartList.forEach(function (item) {
                        item.isEdit = false;
                        if (item.option)
                            item.option.forEach(function (optionList) {
                                optionList.items.forEach(function (optionItem) {
                                    optionItem.isEdit = false;
                                })
                            });
                    });
                    obj.Cart.orderList.forEach(function (item) {
                        if (item != editItem) item.isEdit = false;
                        if (item.option)
                            item.option.forEach(function (option) {
                                option.items.forEach(function (optionItem) {
                                    if (optionItem != editOption) optionItem.isEdit = false;
                                })
                            });
                    });

                    if (editItem.hasUnselectOption) {

                        /*if (editOption.code != obj.schema.unselectOption.code) {
                            console.debug("return O9999?");
                            return;
                        }*/

                        if (optionList.index == undefined) {
                            console.debug("return O9999?");
                            return;
                        }


                        console.log(optionList);
                        if (!optionList.itemCode) {
                            var item = obj.findItemFromFood(editItem.code);
                            console.log("item", item);
                            if (item.option) {
                                item.option.forEach(function (option) {
                                    if (option.optionGroupCode == editOption.optionGroupCode) {
                                        var fullOptionList = $.extend(true, {}, option);
                                        fullOptionList.items.splice(0, 0, optionList.items[0]);
                                        $.extend(true, optionList, fullOptionList);
                                    }
                                });
                            }
                            if (optionList) {
                                optionList.items.forEach(function (item) {
                                    if (item.code !== obj.schema.unselectOption.code)
                                        $.extend(true, item, obj.schema.baseOption, { qty: 0, _qty: 0 });
                                });
                                //if (optionList.items[0].code !== obj.schema.unselectOption.code)
                                //obj.addUnselectOption(optionList, idx);
                                //editOption = $.extend(true, optionList.items[0], editOption);

                                if (optionList.items[0].code === obj.schema.unselectOption.code) {
                                    var fillOption = $.extend(true, {}, obj.schema.unselectOption, obj.schema.baseOption, { qty: 1, _qty: 1, color: 'rms-palette-lemon' });
                                    $.extend(true, optionList.items[0], fillOption);
                                }
                                optionList.items[0].isEdit = true;
                                console.log(optionList);
                            }
                        }
                        if (editOption.isEdit) {
                            if (obj.modeItem != obj.schema.modeItem.modifier) {
                                obj.Cart.currentOptionIndex = idx;
                                obj.Cart.currentOptionList = optionList;
                                obj.Cart.currentOption = editItem.option;
                                obj.modeItem = obj.schema.modeItem.option;
                            }
                        } else {
                            obj.Cart.currentOptionList = [];
                            obj.Cart.currentOptionIndex = -1;
                            obj.modeItem = obj.schema.modeItem.normal;
                        }
                    }
                    else {
                        obj.Cart.currentOptionList = [];
                        obj.Cart.currentOptionIndex = -1;
                        obj.modeItem = obj.schema.modeItem.normal;
                    }
                }
                // this.isChildEdit = true;

                // if( editItem.isEdit && !this.isChildEdit || editItem.isEdit && this.isChildEdit ) {
                //     editOption.isEdit = !editOption.isEdit;
                //     this.isChildEdit = true;
                // } else {

                // }

                this.editOption = editOption.isEdit;
                obj.childEdit = editOption.isEdit;

                // console.log(obj.childEdit);


                if ($event) {
                    $event.stopPropagation();
                }
            },
            showNextOption: function (next) {
                console.log("showNextOption", next, this.currentOptionIndex, this.currentOption);
                // console.log("currentOptionIndex", this.currentOptionIndex, this.currentOption); // index option on the cart list

                if (this.currentOptionIndex > -1) {
                    // if there is anything selected, run here
                    // var selectQty = 0;
                    var selectQty = this.selectQty ? this.selectQty : 0;
                    $.each(this.currentOptionList.items, function (optionItemIndex, optionItem) {
                        if (optionItem.qty > 0) {
                            selectQty += optionItem.qty;
                        };
                    });
                    console.log(selectQty, this.currentOptionList);

                    if (next || !(selectQty < this.currentOptionList.maxQty)) {
                        this.currentOptionIndex++;
                        if (!this.currentOption) {
                            obj.modeItem = obj.schema.modeItem.normal;
                            return;
                        }


                        if (this.currentOptionIndex < this.currentOption.length) {
                            this.currentOptionList = this.currentOption[this.currentOptionIndex];
                            var selected = false;
                            $.each(this.currentOptionList.items, function (optionItemIndex, optionItem) {
                                if (optionItem.qty > 0 && optionItem.code === 'O9999') {
                                    optionItem.isEdit = true;
                                    selected = true;
                                };
                            });
                            if (!selected) {
                                obj.modeItem = obj.schema.modeItem.normal;
                            }
                        }
                        else {
                            obj.modeItem = obj.schema.modeItem.normal;
                        }
                    }
                    // this.selectQty = selectQty;
                }
                else {
                    // if there is nothing selected, run here
                    var that = this;
                    var selectQty = this.selectQty ? this.selectQty : 0;
                    var unselectedCount = 0;
                    var callback = function (optionIndex, optionItem) {
                        console.log("callback", that);
                        if (that.currentOptionIndex == -1) {
                            that.currentOptionIndex = optionIndex;
                            that.currentOptionList = that.currentOption[optionIndex];
                            optionItem.isEdit = true;
                        }
                    }


                    $.each(this.currentOption, function (optionIndex, optionList) {
                        $.each(optionList.items, function (optionItemIndex, optionItem) {
                            // console.log(12345, optionItem.qty, optionItem.code);
                            if (optionItem.qty > 0 && optionItem.code === 'O9999') {
                                // obj.Cart.currentOptionIndex = optionIndex;
                                // obj.Cart.currentOptionList = obj.Cart.currentOption[optionIndex];
                                // optionItem.isEdit = true;
                                callback(optionIndex, optionItem);
                                // obj.modeItem = obj.schema.modeItem.option;
                                unselectedCount++;
                            }
                        });
                    });
                    // this.currentOptionIndex = 0;
                    // this.currentOptionList = this.currentOption[this.currentOptionIndex];
                    // $.each(this.currentOptionList.items, function (optionItemIndex, optionItem) {
                    //     if (optionItem.qty > 0) {
                    //         optionItem.isEdit = true;
                    //     }
                    // });
                }

                // console.log(unselectedCount);
                // all selected, and master/parent item is clicked
                console.debug(4208, unselectedCount);
                console.debug(4209, this.currentOptionIndex);
                if ((this.currentOptionIndex == -1 && unselectedCount == 0) || this.currentOptionIndex == undefined) {
                    obj.modeItem = obj.schema.modeItem.normal;
                }
            },
            addOption: function (option, qty) {
                console.debug(4123, option);

                // var prevQty = prevQty ? prevQty : 0;
                // var totalqty = qty == undefined ? 1 : qty;
                // console.log('totalqty:' + totalqty);
                // console.log('prevQty:' + prevQty);
                // totalqty -= prevQty;
                // var isValid = true;
                // console.log('totalqty:' + totalqty);
                // $.each(obj.Cart.cartList, function (idx, citem) {
                //     console.log(citem);
                //     if (citem.getQty() >= 0 && option.code == citem.code) {
                //         totalqty += citem.getQty();
                //     }
                // });
                // console.log('totalqty:'+totalqty);
                // $.each(obj.stock.list, function (idx, sitem) {
                //     console.log(sitem);
                //     if (sitem.code == option.code) {
                //         if (sitem.stock <= 0) {
                //             obj.showAlert('no_stock');
                //             isValid = false;
                //             return;
                //         } else if (sitem.stock < totalqty) {
                //             var message = uiLanguage[INFO.lang]['stock_reminder'].replace("[TOTAL_QTY]", totalqty).replace("[CURRENT_STOCK]", sitem.stock);

                //             obj.showAlert(message);
                //             isValid = false;
                //             return;
                //         }
                //     }
                // });
                // return isValid;
                // return;
                // obj.Cart.changeItemQty(option.qty)
                // MainService.Cart.changeItemQty(option.name)
                // console.log(option)
                // var selectQty = 0;
                var selectQty = this.selectQty ? this.selectQty : 0;

                if (!obj.Cart.checkItemStock(option, 1)) {
                    // check cart stock
                    return false;
                } else {
                    console.log("addOption currentOptionIndex", this.currentOptionIndex);
                    console.log("addOption currentOptionList", this.currentOption);
                    var selectOption = false;
                    if (option.code != 'O9999') {
                        selectOption = true;
                    }

                    var tmpl = this.currentOptionList;
                    $.each(this.currentOptionList.items, function (optionItemIndex, optionItem) {

                        //console.debug(4265, tmpl);
                        if (selectOption) {
                            if (optionItem.code == 'O9999') {
                                optionItem.qty = 0;
                                optionItem.isEdit = false;
                            } else {
                                selectQty += optionItem.qty;
                            }
                        } else if (optionItem.qty > 0) {
                            optionItem.qty = 0;
                        };
                    });
                    if (selectQty >= this.currentOptionList.maxQty) {
                        //var eoc = this.editOptionCode;
                        $.each(this.currentOptionList.items, function (optionItemIndex, optionItem) {
                            if (optionItem.isEdit) {
                                optionItem.qty = 0;
                            }
                        });
                    }
                    var isAdd = true;
                    // if (selectQty >= this.currentOptionList.minQty) {
                    //     if (option.code == 'O9999') {
                    //         isAdd = false;
                    //     }
                    // }
                    if (isAdd) {
                        $.each(this.currentOptionList.items, function (optionItemIndex, optionItem) {
                            if (option.code == optionItem.code) {
                                option.qty += 1;
                                option.isEdit = false;
                            }
                        });
                        this.showNextOption();
                    } else {
                        this.showNextOption(true);
                    }
                }

                $timeout(function () {
                    obj.scrollToBottom();
                    //obj.scrollToActive();
                }, 10);

                // this.selectQty = selectQty;
                //console.log(obj.Cart.orderList);
            },
            selectOption: function ($event, idx, option, item) {
                console.log(idx);
                console.log(option);
                console.log(item.option);

                obj.modeItem = obj.schema.modeItem.option;
                // console.log(idx);
                // console.log(option);
                this.currentOption = item.option;
                this.currentOptionIndex = 0;
                this.currentOptionList = this.currentOption[this.currentOptionIndex];

                // show first option
                // this.showNextOption();
                if ($event) {
                    $event.stopPropagation();
                }
            },

            /* 
                for new item (new cart list), based on cartListSelectOption()
                new item => subitem
                feature: 
                    toggle selection
                    when option is selected, in modifer option list => click modifier => add modifier
            */
            cartListSelectSubitem: function ($event, idx, editItem, subitemList, editSubitem) {
                console.log('cartListSelectSubitem');
                console.log(editSubitem);
                if (editItem.voidIndex > -1) return;

                if (!editItem.isEdit) editItem.isEdit = true;

                angular.forEach(this.cartList, function (item) {
                    if (item != editItem) item.isEdit = false;
                    angular.forEach(item.subitem, function (subitem) {
                        // console.log(subitem);
                        if (subitem != editSubitem) {
                            subitem.isEdit = false;
                        }
                        // angular.forEach(option.items, function (optItem) {
                        //     if (optItem != editSubitem)
                        //         optItem.isEdit = false;
                        // });
                    });
                });

                editSubitem.isEdit = !editSubitem.isEdit; // toggle
                this.editSubitem = editSubitem.isEdit;
                if (editSubitem.isEdit) {

                    this.editSubitemCode = editSubitem.code;

                    // if (obj.modeItem != obj.schema.modeItem.modifier) {
                    //     //this.currentOption = option;
                    //     this.currentSubitemIndex = idx;
                    //     this.currentSubitemList = subitemList;
                    //     obj.modeItem = obj.schema.modeItem.option;
                    // }

                    if ($event) {
                        $event.stopPropagation();
                    }
                }
                else {
                    // this.currentOption = null;
                    this.editSubitemCode = '';
                    this.currentSubitemList = [];
                    this.currentSubitemIndex = -1;
                    this.resetEditItem();
                    obj.modeItem = obj.schema.modeItem.normal;
                }
            },
            // for subitem in exiting order list(aka existing cart list, means ordered already)
            cartListEditSubitem: function ($event, idx, editItem, subitemList, editSubitem) {
                console.log('cartListEditSubitem');
                console.log(editItem);
                console.log(editSubitem);
                editItem.isEdit = true;

                if (editItem.voidIndex > -1) return;
                if (!editItem.isEdit) editItem.isEdit = true;

                editSubitem.isEdit = !editSubitem.isEdit;

                this.editSubitem = editSubitem.isEdit;
                obj.childEdit = editSubitem.isEdit;

                console.log(obj.childEdit);

                if ($event) {
                    $event.stopPropagation();
                }
            },

            // deprecated
            cancelOrder: function (option) {
                this.cancelOrder2.apply(this, arguments);
                /*console.log('can u see me?');
                console.log(option);
                var option = typeof option === 'object' ? option : {
                    modeOrder: false,
                    modeItem: false,
                    modeBase: true
                };
                var choices = $.extend(true, {}, option);
                console.log(choices);

                if(choices.modeOrder) {
                    console.log('restore modeOrder');
                    switch(obj.modeOrder) {
                        case obj.schema.modeOrder.normal:
                            obj.mode = obj.schema.mode.floorPlan;
                            break;
                        case obj.schema.modeOrder.amendBill:
                            obj.modeOrder = obj.schema.modeOrder.searchBill;
                            break;
                        case obj.schema.modeOrder.foodControl:
                            obj.modeOrder = obj.schema.modeOrder.normal;
                            break;
                    }
                }


                if(choices.modeItem) {
                    console.log('restore modeItem');
                    console.log(obj.modeItem);
                    console.log(obj.schema.modeItem.option);
                    switch(obj.modeItem) {
                        case obj.schema.modeItem.option:
                            // console.log('mode restore 2');
                            obj.modeItem = obj.schema.modeItem.normal;
                            break;
                    }
                }


                if(choices.mode) {
                    console.log('restore mode base');
                    obj.mode = obj.schema.mode.floorPlan;
                }



                // if (obj.modeOrder == obj.schema.modeOrder.amendBill) {
                //     obj.modeOrder = obj.schema.modeOrder.searchBill;

                // }else if (obj.modeOrder == obj.schema.modeOrder.foodControl) {
                //     obj.modeOrder = obj.schema.modeOrder.normal;
                // } else {
                //     obj.mode = obj.schema.mode.floorPlan;
                // }*/
            },
            cancelOrder2: function (option) {
                var c = false;

                if (otherWriteOrderService.children[0])
                    otherWriteOrderService.children[0].btnCancelAction(function (r) {
                        if (r == 'OK') c = true;
                    });

                //obj.deliveryOrderService.btnCancelAction(function (r) {
                //    if (r == 'OK') c = true;
                //});

                if (c) return;

                if (obj.modeCheckOrderItem('option')) {
                    // console.log('1');
                    obj.switchMode2({ modeItem: 'normal' });
                    $rootScope.$emit('returnToOrder', []);
                    return;
                }

                if (obj.modeCheckOrder('normal') && obj.modeCheckOrderItem('normal')) {
                    console.log('2');
                    obj.lockTable.toReleaseLockTable(obj.lockTable.tableNum);
                    obj.switchMode2({ mode: 'floorPlan' });
                    $rootScope.$emit('cancelOrder', []);
                    return;
                }

                if (obj.modeCheckOrder('specialMessage')) {
                    obj.switchMode2({ modeOrder: 'normal' });
                    $rootScope.$emit('returnToOrder', []);
                    return;
                }

                if (obj.modeCheckOrder('kitchenMessage')) {
                    console.log('4');
                    obj.lockTable.toReleaseLockTable(obj.lockTable.tableNum);
                    obj.switchMode2({ modeOrder: 'normal' });
                    $rootScope.$emit('returnToOrder', []);
                    return;
                }

                if (obj.modeCheckBill('amendBill')) {



                    if (obj.cloneAppliedPaymentMethod.length != 0) {
                        obj.recoverPaymentMethodInAmendBill();
                    }
                    obj.switchMode2({ modeOrder: 'searchBill' });

                    //$rootScope.$emit('cancelOrder', []);
                    //$rootScope.$emit('returnToOrder', []);
                    return;
                }

                if (obj.modeCheckOrder('foodControl')) {
                    console.log('6');
                    obj.switchMode2({ modeOrder: 'normal' });
                    $rootScope.$emit('returnToOrder', []);
                    return;
                }

                if (obj.modeFloorPlan == obj.schema.modeFloorPlan.split)
                    obj.lockTable.toReleaseLockTable(obj.lockTable.tableNum);

                if (obj.modeFloorPlan == obj.schema.modeFloorPlan.billing && obj.mode == obj.schema.mode.order) {
                    obj.lockTable.toReleaseLockTable(obj.lockTable.tableNum);
                }

                console.log('default cancel');
                obj.voidIndex = -1; // starting

                // default
                obj.mode = obj.schema.mode.floorPlan;
                $rootScope.$emit('cancelOrder', []);
                return;

                // if (obj.modeOrder == obj.schema.modeOrder.amendBill) {
                //     obj.modeOrder = obj.schema.modeOrder.searchBill;

                // }else if (obj.modeOrder == obj.schema.modeOrder.foodControl) {
                //     obj.modeOrder = obj.schema.modeOrder.normal;
                // } else {
                //     obj.mode = obj.schema.mode.floorPlan;
                // }
            },
            voidItem: function (voidRemark) {
                var that = this;
                var resultItemList = [];
                obj.pendingVoidItem = [];
                $.each(that.orderList, function (idx, item) {
                    if (item.isEdit && item.getQty() > 0) {
                        var finalCheckQty = item.getQty();
                        $.each(obj.Cart.orderList, function (checkidx, checkItem) {
                            console.log(checkItem.voidIndex);
                            if (checkItem.voidIndex == item.index) {
                                finalCheckQty += checkItem.getQty();
                            }
                        });
                        $.each(obj.Cart.cartList, function (checkidx, checkItem) {
                            if (checkItem.voidIndex == item.index) {
                                finalCheckQty += checkItem.getQty();
                            }
                        });
                        if (finalCheckQty > 0) {
                            item.lock = true;
                            //if (item.option)
                            //    item.option.forEach(function (optionList) {
                            //        optionList.items.forEach(function (option) {
                            //            if (item.hasUnselectOption) {
                            //                if (option.code === obj.schema.unselectOption.code)
                            //                    option.qty = 1;
                            //                else
                            //                    option.qty = 0;
                            //            }
                            //        });
                            //    });
                            var cloneItem = {};
                            angular.copy(item, cloneItem);
                            var _voidqty = obj.Cart.voidItemCount;
                            if (String(_voidqty).toLowerCase() == "all" || String(_voidqty) == "所有") {
                                _voidqty = finalCheckQty;
                            } else if (_voidqty > finalCheckQty) {
                                _voidqty = finalCheckQty;
                            }

                            cloneItem.qty = _voidqty * -1
                            cloneItem.voidIndex = item.index;
                            cloneItem.voidRemark = voidRemark;
                            //if (cloneItem.option)
                            //    cloneItem.option.forEach(function (optionList) {
                            //        optionList.items.forEach(function (option) {
                            //            if ((option.qty > 0 && option.code === obj.schema.unselectOption.code)) {
                            //                option.qty = 0;
                            //            }
                            //        });
                            //    });

                            resultItemList.push(cloneItem);
                            //resultItem = obj.Cart.addItem(cloneItem);
                            //resultItem.qty = _voidqty * -1
                            //resultItem.voidIndex = item.index;
                            //resultItem.voidRemark = voidRemark;
                        }
                    }
                });
                console.log(resultItemList)
                if (resultItemList.length == 0) {
                    obj.showAlert('please_select_food');
                    return;
                }


                obj.checkPermission('voidItem', function () {
                    for (var i = 0; i < resultItemList.length; i++) {
                        var oItem = {};
                        angular.copy(resultItemList[i], oItem);

                        var rItem = obj.Cart.addItem(resultItemList[i]);
                        rItem.qty = oItem.qty;
                        rItem.voidIndex = oItem.voidIndex;
                        rItem.voidRemark = oItem.voidRemark;
                    }
                    obj.Cart.resetEditItem();
                    obj.modeItem = obj.schema.modeItem.normal;
                    obj.modeMultiSelectItem = false;
                    $timeout(function () {
                        obj.scrollToBottom();
                    }, 10);
                }, function () {
                    // pop up username and password
                    // simon
                    obj.pendingVoidItem = resultItemList;
                    if (angular.element('md-dialog[aria-label="Manager Login"]').scope() == undefined) {
                        $mdDialog.show({
                            templateUrl: 'tmpl/voidItemApproval.html'
                        })
                    } else {
                        $mdDialog.hide();
                    }
                });


            },
            resetEditItem: function () {
                console.log('obj.cart.resetEditItem');
                console.log('clear order list');
                $.each(obj.Cart.orderList, function (i, v) {
                    v.isEdit = false;
                    if (v.option != undefined)
                        $.each(v.option, function (k, optionList) {
                            $.each(optionList.items, function (l, optionItem) {
                                optionItem.isEdit = false;
                            })
                        });
                });

                console.log('clear cart list');
                $.each(obj.Cart.cartList, function (i, v) {
                    v.isEdit = false;
                    if (v.option != undefined)
                        $.each(v.option, function (k, optionList) {
                            $.each(optionList.items, function (l, optionItem) {
                                optionItem.isEdit = false;
                            })
                        });
                });

                console.log('clear stock list');
                $.each(obj.stock.list, function (i, v) { v.isEdit = false; });
            },
            changeItemQty: function (qty) {

                if (obj.modeOrder == obj.schema.modeOrder.foodControl) {
                    obj.stock.updateStock(qty);
                } else {

                    $.each(obj.Cart.cartList, function (i, v) {
                        if (v.isEdit && v.qty > 0) {
                            if (obj.Cart.checkItemStock(v, qty, v.getQty())) {
                                console.log(v);
                                if (!v.comboItemKey)
                                    v.qty = qty;
                                else {
                                    obj.combo.resetItemQty(v, qty);
                                }

                                if (v.isHaveCompositeOptionGroup()) {
                                    obj.combo.resetComboQty(v);
                                }
                            } else {
                                return;
                            }
                        }
                    });
                }
                obj.modeItem = obj.schema.modeItem.normal;
            },
            init: function () {
                this.tableNo = '';
                this.member = {};
                this.cartList = [];
                this.orderList = [];
                this.couponObj = [];
                this.noOfPeople = 0;
                this.member = {};
                this.refNo = {};
                this.currentOption = null;
                this.currentOptionIndex = -1;
                this.currentOptionList = { "items": [] };
                obj.appliedPaymentMethod = [];
                obj.appliedDiscount = [];

            }
        };

        obj.combo = {
            itemKey: 0,
            extendOptionGroup: {
                compositeOptionQty: {
                    current: 0,
                    total: 1
                },
                checkIsCompleted: function () {
                    return this.compositeOptionQty.current == this.compositeOptionQty.total; // to filter out if completed
                }
            },
            checkIsFastFoodMode: function () {
                if (!obj.checkIsNotTakeAwayMode()) {
                    if (obj.getOtherWriteOrderService().modeSetting.mode.value == 'fastFood') {
                        return true;
                    }
                }
                return false;
            },
            checkIsCompositeItem: function () {

            },
            compositeOptionGroupKey: [], // hold key && index same
            compositeOptionGroup: [], // hold optionGroup && index same && more than one optionGroup of one combo
            compositeItemList: [], // hold item which is mapped to option item && index same
            addOptionGroup: function (itemKey, optionGroupIdx, optionGroup) {
                if (!this.checkIsFastFoodMode()) return;
                var keySchema = {
                    itemKey: itemKey, //unique code
                    optionGroupIndex: optionGroupIdx, // which optionGroup of combo 
                    optionItemIndex: [], // map which optionItem of optionGroup
                    //completed: false // to filter out if completed
                }
                this.compositeOptionGroupKey.push(keySchema);
                //make sure 09999 index at 0;
                if (optionGroup.items[optionGroup.items.length - 1].code == "O9999") {
                    var notSelectItemOptionObj = optionGroup.items.splice(optionGroup.items.length - 1, 1);
                    if (notSelectItemOptionObj[0].code == "O9999")
                        optionGroup.items.splice(0, 0, notSelectItemOptionObj[0]);
                }
                this.compositeOptionGroup.push(optionGroup);
                this.compositeItemList.push({ items: [] });
                this.cartToMapOption();
            },
            cartToMapOption: function () {
                if (!this.checkIsFastFoodMode()) return;
                var self = this;
                var itemList = [];
                var maxPrice = 0;
                self.mapOption();
                //var indexOfMaxPrice = -1;
                //obj.Cart.cartList.forEach(function (food) {
                //    if (food.option) return true;
                //    //if (food.comboItemKey) return true;
                //    if (food.comboItemKey) {
                //        food.comboItemKey = null;
                //        food.unitprice = food.itemUnitPrice;
                //    };
                //    itemList.push(food);

                //    //food.code.indexOf()
                //    if (false) {
                //        itemList.push(food);
                //        if (food.unitprice > maxPrice) {
                //            maxPrice = food.unitprice;
                //            indexOfMaxPrice = itemList.length - 1;
                //        }
                //    }


                //})

                //console.debug('self.mapOption(itemList);', itemList);
                //self.mapOption(itemList);

                obj.combo.sortCartList();

                ////priority price first
                //if (false) {
                //    if (itemList.length <= 0) return;
                //    self.mapOption(itemList[indexOfMaxPrice]);
                //}

            },
            mapOption: function (items) {

                if (!this.checkIsFastFoodMode()) return;
                console.debug('mapOption');
                var self = this;

                /*
                items.forEach(function (item) {
                    var stop = false;
                    self.compositeOptionGroupKey.forEach(function (key, idx) {
                        if (stop) return false;
                        //if (key.completed) return true;

                        self.compositeOptionGroup[idx].items.forEach(function (optionItem, optionItemIndex) {
                            var optionGroup = self.compositeOptionGroup[idx];
                            if (optionGroup.checkIsCompleted()) return true;
                            //console.debug(optionItemIndex, optionItem);
                            if (optionItem.plu == item.code) {
                                if (optionGroup.compositeOptionQty.current + item.qty > optionGroup.compositeOptionQty.total) return false;

                                var isExist = self.compositeItemList[idx].items.filter(function (i) {
                                    return i.itemKey == item.itemKey;
                                })
                                if (isExist.length != 0) return false;

                                optionGroup.compositeOptionQty.current += item.qty;
                                console.debug('add', optionItem.name1);
                                optionItem.qty += item.qty;
                                optionItem.composite = true;
                                item.itemUnitPrice = item.unitprice;
                                item.unitprice = 0;
                                item.comboItemKey = key.itemKey;
                                //if (optionGroup.compositeOptionQty.current >= optionGroup.compositeOptionQty.total) key.completed = true;
                                stop = true;
                                self.compositeItemList[idx].items.push(item);
                                key.optionItemIndex.push(optionItemIndex);
                                return false;
                            }
                        })
                        console.log(stop);
                    });
                })
                */

                //items.forEach(function (item) {

                //var stop = false;
                self.compositeOptionGroupKey.forEach(function (key, idx) {

                    var optionGroup = self.compositeOptionGroup[idx];
                    var pendingCompositeItem = [];

                    optionGroup.compositeOptionQty.current = 0;
                    //optionGroup.compositeOptionQty.itemQty = obj.Cart.cartList.find(function (item) { return item.itemKey === key.itemKey }).getQty();
                    var itemqty = 0;
                    if (obj.Cart.cartList.find(function (item) { return item.itemKey === key.itemKey })) {
                        itemqty = obj.Cart.cartList.find(function (item) { return item.itemKey === key.itemKey }).getQty();
                    }
                    optionGroup.compositeOptionQty.itemQty = itemqty;
                    optionGroup.items.forEach(function (optionItem, optionItemIndex) {
                        if (optionItem.qty > 0) {
                            optionItem.qty = 0;
                            optionItem.composite = false;
                        }
                    });
                    obj.Cart.cartList.filter(function (item) { return item.comboItemKey === key.itemKey }).forEach(function (item) {
                        item.unitprice = item.itemUnitPrice;
                        delete item.comboItemKey;
                    });
                });

                var newItem = null;
                self.compositeOptionGroupKey.forEach(function (key, idx) {
                    var optionGroup = self.compositeOptionGroup[idx];
                    var pendingCompositeItem = [];
                    self.compositeItemList[idx].items = [];
                    obj.Cart.cartList.forEach(function (item) {
                        if (item.option) return false;

                        //if (stop) return false;
                        //if (key.completed) return true;

                        console.debug(idx);
                        self.compositeOptionGroup[idx].items.forEach(function (optionItem, optionItemIndex) {
                            //if (optionGroup.checkIsCompleted()) return true;
                            //console.debug(optionItemIndex, optionItem);
                            if (optionItem.plu == item.code && item.comboItemKey == null) {
                                optionItem.qty = 0;
                                pendingCompositeItem.push({ val: item.unitprice - optionItem.unitprice, key: item.itemKey, optionItemIndex: optionItemIndex, item: item, optionItem: optionItem });
                                return false;
                            }
                        })
                    });

                    pendingCompositeItem.sort(function (a, b) {
                        return b.val - a.val;
                    });

                    pendingCompositeItem.forEach(function (pci, pciidx) {
                        console.log("optionGroup.compositeOptionQty.current", optionGroup.compositeOptionQty.current);
                        var optionGroupTotalQty = optionGroup.maxQty * optionGroup.compositeOptionQty.itemQty;
                        console.log("optionGroupTotalQty", optionGroupTotalQty);
                        //alert((optionGroup.compositeOptionQty.current < optionGroupTotalQty))
                        //if (optionGroup.compositeOptionQty.current < optionGroupTotalQty && key.optionGroupIndex == ) {
                        console.log(pci.item);
                        if (newItem != null) return;
                        if (optionGroup.compositeOptionQty.current < optionGroupTotalQty && pci.item.comboItemKey == null) {

                            if (pci.item.qty + optionGroup.compositeOptionQty.current <= optionGroupTotalQty) {
                                optionGroup.compositeOptionQty.current += pci.item.qty;
                                pci.optionItem.qty += pci.item.qty;
                                pci.optionItem.composite = true;
                                pci.item.itemUnitPrice = pci.item.unitprice;
                                pci.item.optionUnitPrice = pci.optionItem.getUnitPrice();
                                pci.item.unitprice = 0;

                                pci.item.comboItemKey = key.itemKey;

                                //obj.Cart.cartList.find(function (item) { return item.itemKey === key.itemKey });

                                var compositeCombo;

                                compositeCombo = obj.Cart.cartList.find(function (item) { return item.itemKey === key.itemKey });

                                var remainingQty = compositeCombo.qty - pci.item.qty;
                                compositeCombo.qty = pci.item.qty;
                                console.log(4867, compositeCombo);

                                console.log(4867, pci.item.qty);
                                console.log(4867, remainingQty);
                                if (remainingQty != 0) {
                                    //var pendindCompositeCombo = null;
                                    //compositeCombo = obj.food.category.forEach(function (c) {
                                    //    if (pendindCompositeCombo != null) { return; }
                                    //    c.items.forEach(function (i) {
                                    //        if (pendindCompositeCombo != null) { return; }
                                    //        if (i.code == compositeCombo.code) {
                                    //            pendindCompositeCombo = i;
                                    //        }

                                    //    })
                                    //})

                                    //newItem = $.extend(true, {}, pendindCompositeCombo);
                                    newItem = $.extend(true, {}, compositeCombo);
                                    newItem.qty = remainingQty;
                                }

                                self.compositeItemList[idx].items.push(pci.item);
                                key.optionItemIndex.push(pci.optionItemIndex);
                            } else {

                                var newItemQty = pci.item.qty - (optionGroupTotalQty - optionGroup.compositeOptionQty.current);
                                var currnetItemQty = pci.item.qty - newItemQty;
                                pci.item.qty = currnetItemQty;


                                newItem = $.extend(true, {}, pci.item);
                                newItem.qty = newItemQty;
                                //newItem.qty = newItemQty;
                                optionGroup.compositeOptionQty.current += currnetItemQty;
                                console.log(pci);
                                //alert(pci.item.comboItemKey == null);
                                //alert('b0:' + pci.optionItem.qty);
                                pci.optionItem.qty += currnetItemQty;
                                //alert('b1:' + pci.optionItem.qty);

                                pci.optionItem.composite = true;
                                //pci.item.itemUnitPrice = pci.item.unitprice;

                                pci.item.optionUnitPrice = pci.optionItem.getUnitPrice();
                                pci.item.unitprice = 0;
                                pci.item.comboItemKey = key.itemKey;
                                self.compositeItemList[idx].items.push(pci.item);
                                key.optionItemIndex.push(pci.optionItemIndex);

                            }
                        }
                    });


                    //console.log(pendingCompositeItem);
                })

                if (newItem != null) {
                    var itemqty = newItem.qty;
                    //if (newItem.option) {
                    //    newItem.option.forEach(function (optionList, idx) {
                    //        if (optionList.compositeOptionGroup) {
                    //            obj.combo.addOptionGroup(newItem.itemKey, idx , optionList);
                    //            optionList.compositeOptionQty.current = 0;
                    //            optionList.compositeOptionQty.total = 1;
                    //        }
                    //    })
                    //}
                    //obj.Cart.addItem(newItem, itemqty, false);
                    var newnewItem = obj.Cart.addItem(newItem, itemqty, false);
                    if (newnewItem.option) {
                        newnewItem.option.forEach(function (optionList, idx) {
                            //obj.addUnselectOption(optionList, idx, newItem);

                            obj.combo.addOptionGroup(newnewItem.itemKey, idx, optionList);
                        });
                    }

                    //obj.Cart.cartList[obj.Cart.cartList.length - 1].qty = itemqty;
                    //obj.Cart.currentOptionList = [];
                    //obj.Cart.currentOptionIndex = -1;
                    //obj.Cart.resetEditItem();
                }



                obj.combo.sortCartList();
                /*if (!stop) {
                    self.compositeOptionGroupKey.forEach(function (key, idx) {
                        if (stop) return false;
                        self.compositeOptionGroup[idx].items.forEach(function (elem, optionItemIndex) {
                            //console.debug(optionItemIndex, elem);

                        })
                    });
                }*/



            },
            sortCartList: function () {

                /*var a_sortKey = -1;
                    var a_typeKey = -1;
                    var b_typeKey = -1;
                    var b_sortkey = -1;


                    if (a.option) {
                        a_sortKey = a.itemKey;
                        a_typeKey = 0;
                    }
                    else if (a.comboItemKey) {
                        a_sortKey = a.comboItemKey;
                        a_typeKey = 1;
                    }
                    else {
                        a_sortKey = a.itemKey;
                        a_typeKey = 2
                    }

                    if (b.option) {
                        b_sortKey = b.itemKey;
                        b_typeKey = 0;
                    }
                    else if (b.comboItemKey) {
                        b_sortKey = b.comboItemKey;
                        b_typeKey = 1;
                    }
                    else {
                        b_sortKey = b.itemKey;
                        b_typeKey = 2
                    }
                    if (a_sortKey < b_sortkey) return -1;
                    else if (a_sortKey > b_sortkey) return 1;
                    else {
                        if (a_typeKey < b_typeKey) return 1;
                        else if (a_typeKey > b_typeKey) return -1;
                        else return 0;
                    }
                    
                    
                    */
                var comboSortSeq = 1000;
                var itemSortSeq = 1;
                obj.Cart.cartList.forEach(function (i) { delete i.sortSeq; });
                obj.Cart.cartList.forEach(function (i) {
                    if (obj.Cart.cartList.find(function (c) { return i.itemKey == c.comboItemKey }) != undefined) {
                        i.sortSeq = comboSortSeq;
                        i.isCompositeCombo = true;
                        comboSortSeq -= 0.1;
                        obj.Cart.cartList.filter(function (c) { return i.itemKey == c.comboItemKey }).forEach(function (ci) {
                            ci.isCompositeCombo = true;
                            ci.sortSeq = comboSortSeq;
                            comboSortSeq -= 0.1;
                        });
                    }
                });
                obj.Cart.cartList.forEach(function (i) {
                    if (i.sortSeq == undefined) {
                        i.sortSeq = itemSortSeq;
                        itemSortSeq -= 0.1;
                    }
                });



                //obj.Cart.cartList.forEach(function (i) {
                //    if (obj.Cart.cartList.find(function (c) { return c.itemKey == i.comboItemKey }) != undefined) {
                //        i.sortSeq = comboSortSeq;
                //        comboSortSeq += 10;
                //    }
                //});
                obj.Cart.cartList.sort(function (a, b) {
                    //if (obj.Cart.cartList.find(function (c) { return a.itemKey == c.comboItemKey }) != undefined) {

                    //    return -1;
                    //};
                    //if (obj.Cart.cartList.find(function (c) { return b.itemKey == c.comboItemKey }) != undefined) {
                    //    return 1;
                    //};
                    return b.sortSeq - a.sortSeq;
                });

                //obj.Cart.cartList.sort(function (a, b) {
                //    var a_type = -1;
                //    var a_key = -1;
                //    var b_type = -1;
                //    var b_key = -1;
                //    a_key = a.itemKey;
                //    b_key = b.itemKey;

                //    if (a.option) a_type = 0;
                //    else if (a.comboItemKey) {
                //        a_type = 1;
                //        a_key = a.comboItemKey;
                //    }
                //    else a_type = 2;

                //    if (b.option) b_type = 0;
                //    else if (b.comboItemKey) {
                //        b_type = 1;
                //        b_key = b.comboItemKey;
                //    }
                //    else b_type = 2;

                //    if (a_type == 2) return 1;
                //    if (b_type == 2) return -1;



                //    //if (a_key < b_key) return -1;
                //    //else if (a_key > b_key) return 1;
                //    //else {
                //        if (a_type < b_type) return -1;
                //        else if (a_type > b_type) return 1;
                //        else {
                //            return a.itemKey - b.itemKey;
                //        }
                //    //}

                //});
            },
            deleteItem: function (itemKey) {
                if (!this.checkIsFastFoodMode()) return;
                var self = this;
                var stop = false;
                //one composite only bind one combo so find the compositeItem at compositeItemList
                self.compositeItemList.forEach(function (compositeItem, idx) {

                    if (stop) return false;
                    compositeItem.items.forEach(function (item, itemIndex) {
                        if (item.itemKey == itemKey) {
                            console.debug('delete item', item.itemKey);
                            console.debug('compositeItem index', idx);
                            console.debug('item index', itemIndex);
                            var optionGroupKey = self.compositeOptionGroupKey[idx]
                            var optionItemIndex = optionGroupKey.optionItemIndex[itemIndex];
                            console.debug('delete', self.compositeOptionGroup[idx].items[optionItemIndex].name1);
                            var optionGroup = self.compositeOptionGroup[idx];
                            optionGroup.compositeOptionQty.current -= item.qty;
                            optionGroup.items[optionItemIndex].qty -= item.qty;
                            if (optionGroup.items[optionItemIndex].qty <= 0)
                                delete self.compositeOptionGroup[idx].items[optionItemIndex].composite;
                            compositeItem.items.splice(itemIndex, 1);
                            optionGroupKey.optionItemIndex.splice(itemIndex, 1);
                            //optionGroupKey.completed = false;
                            return false;
                        }
                    })
                    //for (var a = 0; a < compositeItem.items.length; a++) {
                    //    if (compositeItem.items[a].itemKey == itemKey) {
                    //        var optionGroupKey = self.compositeOptionGroupKey[idx]
                    //        var optionItemIndex = optionGroupKey.optionItemIndex[a];
                    //        console.debug('delete', self.compositeOptionGroup[idx].items[optionItemIndex].name1);
                    //        var optionGroup = self.compositeOptionGroup[idx];
                    //        optionGroup.compositeOptionQty.current -= compositeItem.items[a].qty;
                    //        optionGroup.items[optionItemIndex].qty -= compositeItem.items[a].qty;
                    //        if (optionGroup.items[optionItemIndex].qty <= 0) delete self.compositeOptionGroup[idx].items[optionItemIndex].composite;
                    //        compositeItem.items.splice(a, 1);
                    //        optionGroupKey.optionItemIndex.splice(a, 1);
                    //        //optionGroupKey.completed = false;
                    //        break;
                    //    }
                    //}
                });
                self.cartToMapOption();

            },
            deleteCombo: function (itemKey, isResetQty) {
                if (!this.checkIsFastFoodMode()) return;

                var self = this;
                var stop = false;

                self.compositeItemList.forEach(function (scil) {
                    scil.items.forEach(function (item) {
                        if (item.comboItemKey == itemKey) {
                            item.unitprice = item.itemUnitPrice;
                            console.log(item)
                            delete item.comboItemKey;
                            //delete item.itemUnitPrice;
                        }
                    });
                })

                while (!stop) {
                    console.debug('delete combo');
                    if (self.compositeOptionGroupKey.length == 0) stop = true;
                    self.compositeOptionGroupKey.forEach(function (key, idx) {
                        if (idx == self.compositeOptionGroupKey.length - 1) { stop = true; }
                        if (key.itemKey == itemKey) {

                            /*
                            self.compositeItemList[idx].items.forEach(function (item) {

                                //var target = obj.Cart.cartList.filter(function (cartItem) {
                                //    return cartItem.itemKey == item.itemKey;
                                //})
                                //if (target.length != 0) {
                                //    target[0].unitprice = target[0].itemUnitPrice;
                                //    delete target[0].comboItemKey;
                                //    delete target[0].itemUnitPrice;
                                //    return false;
                                //}

                                item.unitprice = item.itemUnitPrice;
                                console.log(item)
                                delete item.comboItemKey;
                                delete item.itemUnitPrice;
                            })
                            */
                            if (!isResetQty) {
                                self.compositeItemList.splice(idx, 1);
                                self.compositeOptionGroup.splice(idx, 1);
                                self.compositeOptionGroupKey.splice(idx, 1);
                            } else {
                                self.compositeItemList[idx].items = [];
                                for (var a = 0; a < key.optionItemIndex.length; a++) {
                                    var optionItemIndex = key.optionItemIndex[a];
                                    self.compositeOptionGroup[idx].items[optionItemIndex].qty = 0;
                                }
                                key.optionItemIndex = [];

                            }
                            return false; //one combo may add more than one optionGroup so need recursive to remove
                        }


                    });

                }

                self.cartToMapOption();

            },
            resetComboQty: function (item) {
                if (!this.checkIsFastFoodMode()) return;
                var self = this;
                item.option.forEach(function (optionGroup) {
                    if (optionGroup.compositeOptionGroup) {
                        self.deleteCombo(item.itemKey, true);
                        //if (item.qty > optionGroup.compositeOptionQty.total) {
                        //    optionGroup.compositeOptionQty.total = item.qty;
                        //    self.cartToMapOption();
                        //}
                        //else if (item.qty < optionGroup.compositeOptionQty.total) {
                        //    optionGroup.compositeOptionQty.total = item.qty;
                        //    optionGroup.compositeOptionQty.current = 0;
                        //    self.deleteCombo(item.itemKey, true);
                        //    self.cartToMapOption();
                        //    /*
                        //    for (optionGroup.compositeOptionQty.current; optionGroup.compositeOptionQty.current > optionGroup.compositeOptionQty.total; optionGroup.compositeOptionQty.current--) {
                        //        self.compositeOptionGroupKey.forEach(function (key, idx) {
                        //            if (key.itemKey == item.itemKey) {
                        //                var compositeOptionGroup = self.compositeOptionGroup[idx];
                        //                var items = self.compositeItemList[idx].items;
                        //                if (items.length == 0) return true;
                        //                var lastItemIndex = items.length - 1;
                        //                var compositeItem = items[lastItemIndex];
                        //                compositeItem.unitprice = compositeItem.itemUnitPrice;
                        //                delete compositeItem.comboItemKey;
                        //                delete compositeItem.itemUnitPrice;
                        //                var optionItemIndex = key.optionItemIndex[lastItemIndex];
                        //                compositeOptionGroup.items[optionItemIndex].qty -= 1;
                        //                key.optionItemIndex.splice(lastItemIndex, 1);
                        //                items.splice(lastItemIndex, 1);
                        //            }
                        //        })
                        //    }
                        //    */
                        //}
                    }
                })

                self.scrollToCompositeItem(item.itemKey);
            },
            resetItemQty: function (item, qty) {
                var self = this;
                var stop = false;
                item.qty = qty;
                this.mapOption();
                //self.compositeItemList.forEach(function (compositeItems, compositeItemsIndex) {
                //    if (stop) return false;
                //    compositeItems.items.forEach(function (compositeItem, compositeItemIndex) {
                //        if (compositeItem.itemKey == item.itemKey) {
                //            var optionGroup = self.compositeOptionGroup[compositeItemsIndex];
                //            if (optionGroup.compositeOptionQty.total < optionGroup.compositeOptionQty.current - item.qty + qty) {
                //                obj.showAlert("超出套餐數量", "警告");
                //                stop = true;
                //                return false;
                //            } else {
                //                var optionGroupItemIndex = self.compositeOptionGroupKey[compositeItemsIndex].optionItemIndex[compositeItemIndex];
                //                optionGroup.items[optionGroupItemIndex].qty = optionGroup.compositeOptionQty.current - item.qty + qty;
                //                optionGroup.compositeOptionQty.current = optionGroup.compositeOptionQty.current - item.qty + qty
                //                item.qty = qty;
                //                self.cartToMapOption();
                //                stop = true;
                //                return false;
                //            }
                //        }
                //    })
                //})
            },
            getSubmitOrder: function () {
                var self = this;

                this.compositeOptionGroup.forEach(function (optionGroup) {
                    if (optionGroup.compositeOptionQty.current == 0) {
                        if (optionGroup.items[0].code != 'O9999') return true;
                        optionGroup.items[0].qty = 1;
                    }
                })


                var comboList = [];
                var newComboList = [];
                var compositeItemList = [];
                var itemList = [];

                obj.Cart.cartList.forEach(function (item) {
                    if (item.isHaveCompositeOptionGroup()) { comboList.push(item); return true; }
                    if (item.comboItemKey) { compositeItemList.push(item); return true; }
                    itemList.push(item);
                });
                //console.log('*********************************************');

                //console.log(comboList);

                function sortCompositeItemList() {
                    compositeItemList.sort(function (a, b) {
                        return a.qty - b.qty;
                    });
                }

                sortCompositeItemList();

                console.log(compositeItemList);

                self.compositeItemList.forEach(function (scil, idx) {
                    scil.items.forEach(function (scilItems) {
                        scilItems.submitqty = scilItems.qty;
                    });
                });


                self.compositeItemList.forEach(function (scil, idx) {
                    scil.items.sort(function (a, b) {
                        return a.submitqty - b.submitqty;
                    });
                    scil.items.forEach(function (scilItems) {
                        if (scilItems.submitqty == 0) return;
                        var currentCombo = comboList.find(function (c) { return c.itemKey == scilItems.comboItemKey });
                        var otherOptionList = null;
                        self.compositeItemList.forEach(function (scilothers, secondIdx) {
                            if (secondIdx != idx) {
                                if (scilothers.items.length != 0) {
                                    if (scilItems.comboItemKey == scilothers.items[0].comboItemKey) {
                                        otherOptionList = scilothers;
                                    }
                                }
                                //
                            }
                        });



                        function appendCompositeCombo(appendCombo, appendComboCompositItemlist) {
                            if (currentCombo.submitQty == undefined) {

                                currentCombo.submitQty = currentCombo.qty;
                            }
                            currentCombo.submitQty -= appendCombo.qty;
                            console.log('999999', 'appendCompositeCombo');
                            console.log('appendComboCompositItemlist', appendComboCompositItemlist);

                            appendCombo.option.forEach(function (optionGroup, optionGroupIndex) {
                                console.log('optionGroup', optionGroup);
                                if (optionGroup.compositeOptionGroup) {
                                    optionGroup.items.forEach(function (oi) {
                                        var foundMatchItem = false;
                                        console.log('appendCombo.option', appendCombo.option);
                                        appendComboCompositItemlist.forEach(function (checkItem) {
                                            if (oi.plu == checkItem.plu) {
                                                console.log(checkItem.submitqty + ' ' + (checkItem.submitqty / appendCombo.qty));
                                                oi.qty = checkItem.submitqty / appendCombo.qty;
                                                oi.kitchenMsg = checkItem.kitchenMsg;
                                                oi.modifier = checkItem.modifier;
                                                console.log('888888', oi);
                                                foundMatchItem = true;
                                            }
                                        })
                                        if (!foundMatchItem) {
                                            oi.qty = 0;
                                        }
                                    });
                                }
                            })

                            //appendCombo.option.forEach(function (optionGroup, optionGroupIndex) {
                            //    if (optionGroup.compositeOptionGroup) {
                            //        optionGroup.items.forEach(function (oi) {
                            //            if (oi.code == scilItems.code) {
                            //                oi.qty = scilItems.qty;
                            //            }
                            //        })
                            //    }
                            //});

                            newComboList.push(appendCombo);
                        }

                        function onlyOneComposite() {
                            var appendCombo = $.extend(true, {}, currentCombo);
                            appendCombo.qty = scilItems.submitqty;

                            appendCompositeCombo(appendCombo, [scilItems]);
                        }


                        if (otherOptionList != null) {

                            otherOptionList.items = otherOptionList.items.filter(function (i) {
                                return i.submitqty != 0;
                            });
                            if (otherOptionList.items.length != 0) {
                                otherOptionList.items.sort(function (a, b) {
                                    return a.submitqty - b.submitqty;
                                });
                                if (otherOptionList.items[0].submitqty > scilItems.submitqty) {
                                    var appendCombo = $.extend(true, {}, currentCombo);
                                    appendCombo.qty = scilItems.submitqty;
                                    var appendComboCompositItemlist = [scilItems];
                                    var secondOption = $.extend(true, {}, otherOptionList.items[0]);
                                    secondOption.submitqty = scilItems.submitqty;
                                    otherOptionList.items[0].submitqty -= scilItems.submitqty;
                                    appendComboCompositItemlist.push(secondOption);
                                    appendCompositeCombo(appendCombo, appendComboCompositItemlist);
                                    scilItems.submitqty = 0;

                                } else if (otherOptionList.items[0].submitqty == scilItems.submitqty) {

                                    var appendCombo = $.extend(true, {}, currentCombo);
                                    appendCombo.qty = scilItems.submitqty;
                                    var appendComboCompositItemlist = [scilItems];
                                    var secondOption = $.extend(true, {}, otherOptionList.items[0]);
                                    secondOption.submitqty = scilItems.submitqty;
                                    //otherOptionList.items[0].qty -= scilItems.qty;

                                    appendComboCompositItemlist.push(secondOption);
                                    appendCompositeCombo(appendCombo, appendComboCompositItemlist);
                                    otherOptionList.items[0].submitqty = 0;

                                    //scilItems.qty = 0;
                                } else {
                                    var appendCombo = $.extend(true, {}, currentCombo);
                                    var clearCurrentQty = false;
                                    otherOptionList.items.forEach(function (oi) {
                                        if (clearCurrentQty) return;
                                        if (scilItems.submitqty > oi.submitqty) {
                                            appendCombo.qty = oi.submitqty;
                                            var appendComboCompositItemlist = [oi];

                                            var secondOption = $.extend(true, {}, scilItems);
                                            secondOption.submitqty = oi.submitqty;
                                            appendComboCompositItemlist.push(secondOption);

                                            appendCompositeCombo(appendCombo, appendComboCompositItemlist);

                                            scilItems.submitqty -= oi.submitqty;
                                            oi.submitqty = 0;
                                        } else if (scilItems.submitqty == oi.submitqty) {
                                            appendCombo.qty = oi.submitqty;
                                            var appendComboCompositItemlist = [oi];

                                            var secondOption = $.extend(true, {}, scilItems);
                                            secondOption.submitqty = oi.submitqty;
                                            appendComboCompositItemlist.push(secondOption);

                                            appendCompositeCombo(appendCombo, appendComboCompositItemlist);
                                            oi.submitqty = 0;

                                            clearCurrentQty = true;
                                        } else {
                                            appendCombo.qty = scilItems.submitqty;
                                            var appendComboCompositItemlist = [scilItems];

                                            var secondOption = $.extend(true, {}, oi);
                                            secondOption.submitqty = scilItems.submitqty;
                                            appendComboCompositItemlist.push(secondOption);

                                            appendCompositeCombo(appendCombo, appendComboCompositItemlist);

                                            oi.submitqty -= scilItems.submitqty;
                                            scilItems.submitqty = 0;
                                            clearCurrentQty = true;
                                        }
                                    });
                                    if (!clearCurrentQty) {
                                        onlyOneComposite();
                                    }
                                }
                            } else {
                                onlyOneComposite();
                            }

                        } else {
                            onlyOneComposite();
                        }



                        //cil.comboItemKey



                        //currentCombo.qty -= 
                        //appendCombo

                    })
                })

                console.debug(5393, comboList);
                comboList.forEach(function (combo) {
                    console.debug(5395, combo);

                    if (combo.submitQty != 0) {
                        var appendCombo = $.extend(true, {}, combo);

                        if (combo.submitQty != undefined)
                            appendCombo.qty = combo.submitQty;

                        appendCombo.option.forEach(function (optionGroup, optionGroupIndex) {
                            if (optionGroup.compositeOptionGroup) {
                                optionGroup.items.forEach(function (oi) {
                                    //if (oi.code == scilItems.code) {
                                    oi.qty = 0;
                                    //}
                                })
                            }
                        });

                        newComboList.push(appendCombo);
                    }
                });


                /*
                comboList.forEach(function (combo) {
                    var currentCombo = {};
                    $.extend(true, currentCombo, combo);
                    var comboqty = combo.qty;

                    

                    //console.log('*********************************************');
                    //combo.option.forEach(function (optionGroup, optionGroupIndex) {
                    //    if (optionGroup.compositeOptionGroup) {

                    //    }
                    //});

                    combo.option.forEach(function (optionGroup, optionGroupIndex) {
                        if (optionGroup.compositeOptionGroup) {
                            if (optionGroup.compositeOptionQty.current == 0) {
                                //newComboList.push(combo);
                                return true;
                            }

                            var keyIndex = -1;
                            self.compositeOptionGroupKey.forEach(function (key, idx) {
                                if (key.itemKey == combo.itemKey && key.optionGroupIndex == optionGroupIndex) {
                                    keyIndex = idx;
                                    return false;
                                }
                            });

                            //assume one compositeOptionGroup
                            self.compositeItemList[keyIndex].items.forEach(function (item, itemIndex) {
                                //var newCombo = {};
                                //$.extend(true, newCombo, combo);
                                //newCombo.qty = item.qty;

                                //reset all qty = 0
                                console.log(currentCombo);
                                currentCombo.option[optionGroupIndex].items.forEach(function (i) {
                                    i.qty = 0;
                                })

                                var targetOptionItem = currentCombo.option[optionGroupIndex].items[self.compositeOptionGroupKey[keyIndex].optionItemIndex[itemIndex]];
                                targetOptionItem.qty = 1;
                                targetOptionItem.kitchenMsg = item.kitchenMsg;
                                targetOptionItem.modifier = item.modifier;
                                //newComboList.push(newCombo);
                            });
                            /*
                            if (!optionGroup.checkIsCompleted()) {
                                var newCombo = {};
                                $.extend(true, newCombo, combo);
                                newCombo.qty = optionGroup.compositeOptionQty.total - optionGroup.compositeOptionQty.current;
                                newCombo.option[optionGroupIndex].items.forEach(function (i) {
                                    i.qty = 0;
                                })
                                if (newCombo.option[optionGroupIndex].items[0].code != "O9999") return true;
                                newCombo.option[optionGroupIndex].items[0].qty = 1;
                                //newComboList.push(newCombo);

                            }
                            //
                        }
                    });
                    //
                    newComboList.push(currentCombo);

                })
                */
                newComboList = newComboList.concat(itemList);
                newComboList.sort(function (a, b) {
                    return a.itemKey - b.itemKey;
                });

                console.debug(5488, newComboList);
                return newComboList;
            },
            resetAll: function () {
                this.itemKey = 0;
                this.compositeOptionGroupKey = [];
                this.compositeOptionGroup = [];
                this.compositeItemList = [];
            },
            selectMode: {
                optionGroupIndex: -1,
                itemIndex: -1,
                itemList: [],
                allItemList: [],

            },
            switchCompositeItemSelectionMode: function (elem) {
                var self = this;
                elem.editItem(null, false);
                if (!elem.isEdit) {
                    obj.switchMode2({ modeOrder: "normal" });
                    return;
                }
                var itemKey = elem.itemKey;
                self.selectMode.optionGroupIndex = -1;
                self.selectMode.itemIndex = -1;
                self.selectMode.itemList = [];

                var optionGroupIndex = -1;
                var itemIndex = -1;
                var stop = false;
                self.compositeItemList.map(function (itemList, optionGroupIdx) {
                    if (stop) return true;
                    itemList.items.map(function (item, itemIdx) {
                        if (item.itemKey == itemKey) {
                            optionGroupIndex = optionGroupIdx;
                            itemIndex = itemIdx;
                            stop = true;
                            return false;
                        }
                    })
                })

                if (optionGroupIndex == -1 || itemIndex == -1) return;

                var optionGroupCode = self.compositeOptionGroup[optionGroupIndex].optionGroupCode;
                var _idx = obj.food.compositeOptionGroup.optionGroupList.indexOf(optionGroupCode);
                if (_idx == -1) return;

                self.selectMode.optionGroupIndex = optionGroupIndex;
                self.selectMode.itemIndex = itemIndex;

                //self.selectMode.itemList = obj.food.compositeOptionGroup.itemList[_idx].items;

                obj.food.compositeOptionGroup.itemList[_idx].items.forEach(function (i) {
                    var target = $.grep(self.selectMode.allItemList, function (t) {
                        return t.itemId == i.itemId;
                    })

                    if (target.length > 0) {
                        self.selectMode.itemList.push(target[0]);
                    }
                })

                if (self.selectMode.itemList.length > 0) {
                    $rootScope.$emit('resetCompositeItem');
                    $timeout(function () {
                        obj.modeItem = obj.schema.modeItem.compositeItem;
                    }, 100)

                }

                //obj.switchMode2({ modeOrder: "compositeItem" });
            },
            scrollToCompositeItem: function (comboItemKey) {
                $timeout(function () {
                    var container = $("#order-list"), scrollTo = $('.list-group-item.new-item[itemid="' + comboItemKey + '"]');
                    container.animate({
                        scrollTop: scrollTo.offset().top - container.offset().top + container.scrollTop()
                    }, "fast");

                    localStorage.setItem("scrollToTarget", comboItemKey);

                }, 100)
            }
        }

        obj.mifarePayment = {
            tableNo: "",
            isEnable: false,
            createEvent: function () {
                if (!obj.food.mifarePayment) return;
                //$compile('<rf-payment></rf-payment')($rootScope)
                //$('table-scene').prepend($compile(tmpl)($rootScope));

                //$('table-scene').prepend($compile(tmpl)($rootScope)); style='position: absolute;z-index: -1;' login-input-enter='MainService.mifarePayment.test2();'
                //$('table-scene').prepend($compile("<input id='rf-trigger' autofocus custom-always-focus='true' ng-change='mifarePayment.test2()' ng-model='mifarePayment.rfid' />")($rootScope));

                //$('table-scene').find
                //$("<input id='rf-trigger' autofocus style='position: absolute;z-index: -1;' />").insertBefore($("table-scene"));

                $('#rf-trigger').remove();
                if (!this.isEnable) return;

                $('body').prepend("<input id='rf-trigger' autofocus style='position: absolute;z-index: -1;' />");

                $('#rf-trigger').focus();
                $('#rf-trigger').on('blur', function () {
                    setTimeout(function () {
                        $('#rf-trigger').focus();
                    })
                })

                $('#rf-trigger').on('keydown', function (e) {
                    var key = e.which || e.keyCode;
                    if (key == 9) e.preventDefault();
                    if (key == 13) {
                        obj.mifarePayment.submitEvent();
                    }
                })

            },
            submitEvent: function () {
                if (MainService.modeFloorPlan != MainService.schema.modeFloorPlan.billing || MainService.mode != MainService.schema.mode.floorPlan) return;
                var rfid = $('#rf-trigger').val();
                //$('#rf-trigger').remove();
                console.debug(rfid);
                var self = this;

                self.tableNo = null;
                $('#rf-trigger').val("");

                SocketFactory.emit('getTableCodeByCardId', { cardId: rfid }, function (result) {
                    console.debug('getTableCodeByCardId', result);
                    if (result[0]) {
                        self.tableNo = result[0].tableCode;
                        if (result[0].tableSubCode) self.tableNo += "_" + result[0].tableSubCode;
                        obj.inputPrice = 0;
                        obj.remainder();
                        obj.assignTableNo(self.tableNo, null, "billing", true, null);
                    }



                    //console.debug(result);
                });

                //obj.clickTable("301_B", null, null)   


                //obj.inputPrice = 0;
                //obj.remainder();
                //console.log('obj.mode ' + obj.mode);
                //console.log('obj.modeOrder ' + obj.modeOrder);
                // obj.loadTableOrder(tableNo, tableMode);
                //console.log('obj.selectTable', tableNo, obj.selectTable, tableMode);
                //obj.assignTableNo("301_B", null, "billing");

            }
        }

        obj.pendingVoidItem = [];
        obj.showPendingVoidItemList = function () {
            var text = "";

            for (var i = 0; i < obj.pendingVoidItem.length; i++) {
                var item = obj.pendingVoidItem[i];
                text += item.getName() + ' ' + (item.qty * -1) + ' ' + item.voidRemark + '\n';
            }

            console.log(text);
            return text;
        }

        obj.scrollToBottom = function (element) {
            console.log('scrollToBottom');
            var target = element ? element : "order-scene .order-list-wrapper";
            var maxScrollable = angular.element(target)[0].scrollHeight - angular.element(target).height();
            angular.element(target).scrollTop(maxScrollable);
        }

        //obj.scrollToActive = function (element) {
        //    if (angular.element(".list-group-item.active").length) {
        //        console.log('scrollToActive');
        //        var target = element ? element : "order-scene .order-list-wrapper";
        //        var scrollTo = angular.element(".list-group-item.active").offset().top;
        //        angular.element(target).scrollTop(scrollTo);
        //    }
        //    else {
        //        obj.scrollToBottom(element);
        //    }
        //}

        obj.getSubmitItemList = function (foodList) {

        }

        // food control -> cancel food control
        // billing mode, amend bill -> submit bill

        obj.lockTable = {
            tableNum: "",
            orderNo: "",
            getAllLockTable: function () {
                SocketFactory.emit('getAllLockedTableNo', function (result) {

                    result.forEach(function (code, index) {
                        obj.lockTable.releaseLockTable(code);
                        if (index == result.length - 1) {
                            obj.lockTable.activeLockTable(result);
                        }
                    })

                });
            },
            toActiveLockTable: function () {
                SocketFactory.emit('lockTableByTableNo', { tableCode: obj.lockTable.tableNum.toString(), orderCode: obj.lockTable.orderNo.toString() }, function (r) {
                    //angular.element('order-scene').on('mousedown keyup touch', function (e) {
                    //    obj.lockTable.reActiveLockTable();
                    //});
                });
            },
            activeLockTable: function (CodeList) {
                CodeList.forEach(function (code) {
                    code = code.replace("L", "");
                    var templObj = $.grep($('.table'), function (item) {
                        return $(item).find('text').text() == code;
                    })
                    if (templObj.length != 0)
                        $(templObj[0]).attr('class', $(templObj[0]).attr('class') + " lock ");
                    var tableViewObj = $.grep($('button.table-model'), function (item) {
                        return $(item).attr('table-model') == code;
                    })

                    if (tableViewObj.length != 0) {
                        $(tableViewObj[0]).addClass('lock');
                        //$(tableViewObj[0]).find('div').css('color', 'white');
                    }
                });
            },
            reActiveLockTable: function () {
                SocketFactory.emit('reActiveLockedTable', { tableCode: obj.lockTable.tableNum.toString() }, function (r) {
                });
            },
            releaseLockTable: function (code) {
                code = code.replace("L", "");

                var templObj = $.grep($('.table'), function (item) {
                    return $(item).find('text').text() == code;
                })

                if (templObj.length != 0)
                    $(templObj[0]).attr('class', $(templObj[0]).attr('class').replace(new RegExp(" lock ", "g"), ""));
                var tableViewObj = $.grep($('button.table-model'), function (item) {
                    return $(item).attr('table-model') == code;
                })

                if (tableViewObj.length != 0) {
                    $(tableViewObj[0]).removeClass('lock');
                    //$(tableViewObj[0]).find('div').css('color', 'black');
                }
            },
            toReleaseLockTable: function (code) {

                //type == "order" || type == "table";

                var postArgs = {}
                postArgs.tableCode = code == this.orderNo ? this.tableNum : code;
                SocketFactory.emit('releaseLockedTable', postArgs, function (result) {
                    if (result == obj.lockTable.tableNum) {
                        obj.lockTable.tableNum = "";
                        //angular.element('order-scene').off('mousedown keyup touch');
                    }

                    obj.lockTable.releaseLockTable(result);

                });
            }
        }

        obj.eat = {
            Category: [],
            allCategory: [],
            updateCategoryList: function () {
                obj.food.category = [];
                //console.log('obj.eat.Category', obj.eat.Category);
                var self = this;
                obj.eat.Category.forEach(function (item) {
                    self.allCategory.forEach(function (cate) {
                        if (cate.code == item.code) {
                            //console.log('cate.code == item.code', cate);
                            var tmpl = {}
                            angular.extend(tmpl, cate);
                            obj.food.category.push(tmpl);
                            return false;
                        }
                    })
                })

                obj.food.category.forEach(function (c) {
                    c.items = c.items.filter(function (i) { return !(i.suspend === 0) });
                });

                obj.food.category.sort((a, b) => {
                    if (a.seq != b.seq) { return a.seq - b.seq }
                })

                obj.currentCategory = obj.food.category[0] || { items: [] };
                obj.noOfCategoryPage = Math.ceil(obj.food.category.length / obj.catsPerPage);
                obj.totalItems = obj.currentCategory.items.length;
                obj.noOfItemPage = Math.ceil(obj.totalItems / obj.itemsPerPage);
                obj.totalCats = obj.food.category.length;
            },
            updateCategoryListByTime: function (time) {
                var t = moment(time);
                console.debug(t.format('YYYY-MM-DD HH:mm:ss'));
                var stop = false;
                while (!stop) {

                    for (var z = 0; z < obj.food.category.length; z++) {

                        if (z == obj.food.category.length - 1) stop = true;

                        var c = obj.food.category[z];

                        c.items.forEach(function (i) {
                            var isIncludeTimeRange = false;
                            i.timeRangeList.forEach(function (tr) {
                                var timeStart = moment(new Date(tr.timeStart)).format('HHmmss');
                                var timeEnd = moment(new Date(tr.timeEnd)).format('HHmmss');
                                var currentTime = moment(new Date(t)).format('HHmmss');
                                if (currentTime > timeStart && currentTime <= timeEnd) isIncludeTimeRange = true;
                            })

                            if (i.timeRangeList.length == 0) isIncludeTimeRange = true;
                            i.isIncludeTimeRange = isIncludeTimeRange;
                            //if (!isIncludeTimeRange) notIncludeTimeRangeItem.push(idx);
                        })

                        c.items = c.items.filter(function (i) {
                            return i.isIncludeTimeRange;
                        })

                        if (c.items.length == 0) {
                            obj.food.category.splice(z, 1);
                            break;
                        }
                    }

                }

                obj.currentCategory = obj.food.category[0];
                obj.noOfCategoryPage = Math.ceil(obj.food.category.length / obj.catsPerPage);
                obj.totalItems = obj.currentCategory.items.length;
                obj.noOfItemPage = Math.ceil(obj.totalItems / obj.itemsPerPage);
                obj.totalCats = obj.food.category.length;
            }
        }

        obj.isCustomerDisplay = false;

        function otherWriteOrderService() {

            /*=================================================================*/

            /*variable*/
            otherWriteOrderService.prototype.init = function () {
                this.modeSetting = {
                    mode: {
                        "value": "takeAway",
                    },
                    modeInService: {
                        "value": "modeTakeAway",
                        "isMode": function (mode) {
                            return obj.isModeTakeAway(mode);
                        }
                    },
                    modeNavMenu: {
                        "value": "takeAwayInOrder"
                    }
                }
                this.isFirst = 1;
                this.mode = -1;
                this.modeOtherWriteOrderService = -1;
                this.schema = {
                    orderPage: 0,
                    paymentPage: 1,
                    viewPage: 2
                };
                this.type = { delivery: 0 };
                this.till = userConfig.page.till;
                this.allCategory = [];
                this.Category = [];
                this.cloneCart = [];
                this.customerDisplay = {};
                this.currentCategory = null;
            }
            this.init();

            /*=================================================================*/

            /*function*/
            otherWriteOrderService.prototype.submitCartInTakeOut = function () {
                if (obj.Cart.cartList.length == 0) return;

                //$rootScope.$emit('openKeypadByPayment');
                //return;

                this.mode = this.schema.paymentPage;
                this.cloneCart = obj.Cart.cartList;
                obj.mode = obj.schema.mode.order;
                tableMode = "billing";
                obj.modeOrder = obj.schema.modeOrder[tableMode];
                obj.modeMultiSelectItem = false;
                obj.assign2OrderList([], obj.Cart.orderList);
                obj.checkPaymentTips();
                obj.remainder();
            }
            otherWriteOrderService.prototype.btnCancelAction = function (callback) {
                if (this.modeSetting.modeInService.isMode('writeOrder')) {
                    var modeFieldName = MainService.getOtherWriteOrderService().getModeInServiceValue();
                    var checkMode = {}
                    checkMode[modeFieldName] = 'writeOrder';


                    if (obj.isModeOrder('normal')) {
                        //obj.switchMode2(checkMode, false);
                        obj[modeFieldName] = obj.schema[modeFieldName].writeOrder;
                        if (obj.isModeItem('modifier')) {
                            this.cloneCart = obj.Cart.cartList;
                            this.backTakeAwayOrderMode();
                        } else {
                            this.backTakeAwayViewMode();
                        }

                        callback('OK');
                    } else if (obj.isModeOrder('billing')) {
                        //obj.switchMode2({ modeItem: 'normal' });


                        //obj.switchMode2(checkMode, false);
                        obj[modeFieldName] = obj.schema[modeFieldName].writeOrder;
                        this.backTakeAwayOrderMode();
                        //$rootScope.$emit('returnToOrder', []);
                        callback('OK');
                    } else if (obj.isModeOrder('specialMessage')) {


                        //obj.switchMode2(checkMode, false);
                        obj[modeFieldName] = obj.schema[modeFieldName].writeOrder;
                        this.cloneCart = obj.Cart.cartList;
                        this.backTakeAwayOrderMode();
                        callback('OK');
                    }
                    //return;
                }
                else if (this.modeSetting.modeInService.isMode('amendBill')) {
                    //console.debug('obj.ecrChangePaymentMode', obj.ecrChangePaymentMode);
                    if (obj.cloneAppliedPaymentMethod.length != 0) {
                        obj.recoverPaymentMethodInAmendBill();
                    }
                    if (obj.ecrChangePaymentMode == 1) {

                        obj.ecrChangePaymentMode = 0;
                        callback('OK');
                        return;
                    }
                    this.searchBillMode();
                    callback('OK');

                }
                else if (this.modeSetting.modeInService.isMode('payLater')) {
                    this.backTakeAwayViewMode();
                    callback('OK');
                    return;
                }
            }
            otherWriteOrderService.prototype.backOptionPage = function () {
                MainService.modeItem = MainService.schema.modeItem.normal;
            }
            /*=================================================================*/
            otherWriteOrderService.prototype.updateCategoryList = function () {
                obj.food.category = [];
                this.Category = angular.copy(otherWriteOrderService.category[this.modeOtherWriteOrderService]);
                //console.debug(this.Category);
                var allCategory = otherWriteOrderService.allCategory;
                this.Category.forEach(function (item) {
                    allCategory.forEach(function (cate) {
                        if (cate.code == item.code) {
                            obj.food.category.push(cate);
                            return false;
                        }
                    })
                })

                obj.food.category.forEach(function (c) {
                    c.items = c.items.filter(function (i) { return !(i.suspend === 0) });
                });

                obj.food.category.sort((a, b) => {
                    if (a.seq != b.seq) { return a.seq - b.seq }
                })

                obj.currentCategory = obj.food.category[0] || { items: [] };
                obj.noOfCategoryPage = Math.ceil(obj.food.category.length / obj.catsPerPage);
                obj.totalItems = obj.currentCategory.items.length;
                obj.noOfItemPage = Math.ceil(obj.totalItems / obj.itemsPerPage);
                obj.totalCats = obj.food.category.length;

            }
            otherWriteOrderService.prototype.ResetCategoryList = function () {
                obj.food.category = otherWriteOrderService.allCategory;
                obj.currentCategory = obj.food.category[0];
                obj.noOfCategoryPage = Math.ceil(obj.food.category.length / obj.catsPerPage);
                obj.totalItems = obj.currentCategory.items.length;
                obj.noOfItemPage = Math.ceil(obj.totalItems / obj.itemsPerPage);
                obj.totalCats = obj.food.category.length;
            }
            /*=================================================================*/
            otherWriteOrderService.prototype.backTakeAwayViewMode = function () {
                this.cloneCart = [];
                this.mode = this.schema.viewPage;
                obj.modeItem = obj.schema.modeItem.normal;
                //obj.switchMode2({ modeItem: 'normal' });
                obj.mode = obj.schema.mode[this.modeSetting.mode.value];
                //obj.switchMode2({ mode: this.modeSetting.mode.value });
                obj.modeNavMenu = obj.schema.modeNavMenu[this.modeSetting.mode.value];
                //obj.switchMode2({ modeNavMenu: this.modeSetting.mode.value });

            }
            otherWriteOrderService.prototype.backTakeAwayOrderMode = function () {
                //if (this.mode == this.schema.viewPage) this.resetCartList();
                otherWriteOrderService.isUse = true;
                this.till = userConfig.page.till;

                //this.updateCategoryList();
                var cloneCartList = false;
                if (obj.modeOrder == obj.schema.modeOrder.specialMessage) cloneCartList = true;
                obj.modeItem = obj.schema.modeItem.normal;
                //obj.switchMode2({ modeItem: 'normal' });
                //obj.modeOrder = obj.schema.modeOrder.normal;
                //obj.switchMode2({ modeOrder: 'normal' });
                obj.modeFloorPlan = obj.schema.modeFloorPlan.order;
                //obj.switchMode2({ modeFloorPlan: 'order' });
                obj.modeOrderMenu = obj.schema.modeOrderMenu.normal;
                //obj.switchMode2({ modeOrderMenu: 'normal' });
                obj.modeNavMenu = obj.schema.modeNavMenu[this.modeSetting.modeNavMenu.value];
                //obj.switchMode2({ modeNavMenu: this.modeSetting.modeNavMenu.value });
                var eleName = ".btn-" + this.modeSetting.mode.value.toLocaleLowerCase() + "-order"
                obj.setActive(angular.element(eleName));
                //var globalTill = this.till;


                obj.mode = obj.schema.mode.order;
                var tableMode = "normal";
                obj.modeOrder = obj.schema.modeOrder[tableMode];
                obj.modeMultiSelectItem = false;
                obj.Cart.init();
                obj.Cart.tableNo = this.till;
                obj.Cart.noOfPeople = 1;
                //obj.Cart.tableNo = tableNo;
                //obj.Cart.tableNo = "";
                //obj.Cart.noOfPeople = 0;
                obj.Cart.member = $.extend(true, "", obj.schema.member);
                otherWriteOrderService.children[0].cloneCartList();
                this.mode = this.schema.orderPage;
                obj.assign2OrderList([], obj.Cart.orderList);
                obj.deposit.appliedDeposit = [];
                this.updateCategoryList();
                if (this.currentCategory) MainService.selectCategory(this.currentCategory);

                //$timeout(function () {
                //    obj.assignTableNo(globalTill);
                //}, 500)

                //if (callback != null) callback();
            }
            /*=================================================================*/
            otherWriteOrderService.prototype.cloneCartList = function () {
                if (this.cloneCart.length != 0) {
                    obj.Cart.cartList = this.cloneCart;
                }
            }
            otherWriteOrderService.prototype.resetCartList = function (click) {
                var self = this
                if (click) {
                    var confirm = $mdDialog.confirm()
                              .title(uiLanguage[INFO.lang]['alert'])
                              .content(obj.label.txt_order_reset)
                              .ok(uiLanguage[INFO.lang]['yes'])
                              .cancel(uiLanguage[INFO.lang]['cancel']);
                    $mdDialog.show(confirm).then(function () {
                        self.cloneCart = [];
                        obj.Cart.cartList = self.cloneCart;
                        obj.modeItem = obj.schema.modeItem.normal;
                        if (obj.combo.checkIsFastFoodMode()) obj.combo.resetAll();
                    });
                } else {
                    self.cloneCart = [];
                    obj.Cart.cartList = self.cloneCart;
                    obj.modeItem = obj.schema.modeItem.normal;
                    if (obj.combo.checkIsFastFoodMode()) obj.combo.resetAll();
                }
            }
            /*=================================================================*/
            otherWriteOrderService.prototype.isOrderPage = function () {
                return (this.modeSetting.modeInService.isMode('writeOrder') && this.mode == this.schema.orderPage);
            }
            otherWriteOrderService.prototype.amendBillMode = function () {
                obj.modeOrder = obj.schema.modeOrder.amendBill;
                obj[this.getModeInServiceValue()] = obj.schema[this.getModeInServiceValue()].amendBill;
            }
            otherWriteOrderService.prototype.searchBillMode = function () {
                obj.modeOrder = obj.schema.modeOrder.searchBill;
                obj[this.getModeInServiceValue()] = obj.schema[this.getModeInServiceValue()].searchBill;
            }
            /*=================================================================*/
            otherWriteOrderService.prototype.getModeInServiceValue = function () {
                return this.modeSetting.modeInService.value;
            }
            otherWriteOrderService.prototype.successPaymentAndBackToWriteOrder = function () {
                var modeFieldName = this.getModeInServiceValue();
                //var checkMode = {}
                //checkMode[modeFieldName] = 'writeOrder'
                obj[modeFieldName] = obj.schema[modeFieldName].writeOrder;
                obj.resetPaymentSettings();
            }
            /*=================================================================*/


        }

        /*=================================================================*/
        /**static**/

        /** hold instance **/
        otherWriteOrderService.children = [];
        /** hold all category without filter out **/
        otherWriteOrderService.allCategory = [];
        /** hold all child's category **/
        otherWriteOrderService.category = [];
        otherWriteOrderService.addCategoryToChild = function (mode, cate) {
            otherWriteOrderService.category[mode] = angular.copy(cate);
            console.debug(5983, otherWriteOrderService.category);
        }
        /** for paymentKeypad **/
        otherWriteOrderService.isCardManually = 0;
        otherWriteOrderService.openPaymentKeypad = function (tender) {
            $rootScope.$emit('openKeypadByPayment', tender);
        }

        otherWriteOrderService.paymentKeypadSubmit = function (elem) {
            console.log(elem);
            otherWriteOrderService.paymentKeypadCurrentPostData.data.isUseECR = false;
            otherWriteOrderService.paymentKeypadCurrentPostData.data.isUseKeypad = true;
            otherWriteOrderService.paymentKeypadCurrentPostData.data.keypad = {};
            otherWriteOrderService.paymentKeypadCurrentPostData.data.keypad.traceNumber = elem.traceNumber;
            otherWriteOrderService.paymentKeypadCurrentPostData.data.keypad.batchNumber = elem.batchNumber;
            otherWriteOrderService.paymentKeypadCurrentPostData.data.keypad.amount = elem.amount;
            var socketAction = otherWriteOrderService.paymentKeypadCurrentPostData.action
            console.log(socketAction);
            SocketFactory.emit(socketAction, otherWriteOrderService.paymentKeypadCurrentPostData.data, function (r) {
                r = JSON.parse(r);
                console.log(r);
                console.log(r.refNo);
                $mdDialog.cancel();
                if (r.result === "OK") {
                    if (socketAction == 'amendBill' || socketAction == 'insertPayment') {
                        obj.cloneAppliedPaymentMethod = [];
                        if (otherWriteOrderService.children[0])
                            if (otherWriteOrderService.children[0].modeSetting.modeInService.isMode('amendBill')) {
                                if (userConfig.ecrPayment) {
                                    MainService.ecrChangePaymentMode = 0;
                                    MainService.cloneAppliedPaymentMethod = [];
                                    MainService.appliedPaymentMethod.forEach(function (apm) {
                                        delete apm.isNewAdd;
                                    })
                                    //console.debug("refreshPaymentList", r.resultList);
                                    $rootScope.$emit('refreshPaymentList', r.resultList);
                                    //return;
                                }
                                otherWriteOrderService.children[0].searchBillMode();
                                obj.modeOrder = obj.schema.modeOrder.searchBill;
                                return;
                            }
                        //if (obj.isModeDeliveryOrder('amendBill')) {
                        //    obj.deliveryOrderService.searchBillMode();
                        //    return;
                        //}
                    }
                    else {
                        obj.lastBillReference = r.refNo;
                        obj.lastTableCode = r.tableCode;
                        obj.lastTableSubCode = r.tableSubCode;

                        if (!obj.checkIsNotTakeAwayMode()) {
                            obj.lastTableCode = "";
                            obj.lastTableSubCode = "";
                            obj.modeOrder = obj.schema.modeOrder.billingSummary;
                            //MainService.mode = MainService.schema.mode.takeAway;
                        }
                        // jump to summary page instead of floor plan first
                        obj.modeOrder = obj.schema.modeOrder.billingSummary;
                    }
                    // obj.mode = obj.schema.mode.floorPlan;
                    console.log('obj.mode ');
                    console.log(obj.mode);
                }
                else if (r.result === "ERROR" || r.result === "fail") {

                    obj.showAlert('octopus_error_' + r.note);
                    //var confirm = $mdDialog.confirm()
                    //           .title(uiLanguage[INFO.lang]['alert'])
                    //           .content("octopus errer " + r.note)
                    //           .ok(uiLanguage[INFO.lang]['yes'])
                    //$mdDialog.show(confirm);
                }
                else if (r.result === 'failInCard') {
                    obj.showAlert('付款失敗');
                }
                /// todo: handle octopus error message

                checkSubmitCart = false;
            });

        }

        otherWriteOrderService.paymentKeypadCurrentPostData = {};

        /*=================================================================*/


        obj.getOtherWriteOrderServiceFullDetail = function () {
            return otherWriteOrderService;
        }

        obj.getOtherWriteOrderService = function () {
            if (otherWriteOrderService.children[0]) return otherWriteOrderService.children[0];
            else return undefined;
        }


        /*=================================================================*/

        //obj.category = [];
        //obj.allCategory = [];

        if (false) {
            obj.takeOutInOrder = {
                isFirst: 1,
                mode: -1,
                schema: {
                    orderPage: 0,
                    paymentPage: 1,
                    viewPage: 2
                },
                type: {
                    delivery: 0
                },
                allCategory: [],
                Category: [],
                submitCartInTakeOut: function () {
                    if (obj.Cart.cartList.length == 0) return;
                    this.mode = this.schema.paymentPage;
                    this.cloneCart = obj.Cart.cartList;
                    obj.mode = obj.schema.mode.order;
                    tableMode = "billing";
                    obj.modeOrder = obj.schema.modeOrder[tableMode];
                    obj.modeMultiSelectItem = false;
                    obj.assign2OrderList([], obj.Cart.orderList);
                    obj.checkPaymentTips();
                    obj.remainder();
                    //obj.modeItemListDisplay = obj.schema.modeItemListDisplay2.detail;

                },
                till: userConfig.page.till,
                backTakeAwayViewMode: function () {

                    //obj.switchMode2({ modeFloorPlan: 'order', mode: 'floorPlan', modeItemListDisplay: 'normal', modeItemListDisplay2: 'normal' }, false, 'order_mode', 'order_mode');
                    obj.takeOutInOrder.mode = this.schema.viewPage;
                    obj.modeItem = obj.schema.modeItem.normal;
                    //modeCheckOrder('specialMessage') || modeCheckFloor('kitchenMessage');
                    obj.mode = obj.schema.mode.takeAway;
                    obj.modeNavMenu = obj.schema.modeNavMenu.takeAway;
                    this.cloneCart = [];

                },
                isOrderPage: function () {
                    return (obj.isModeTakeAway('writeOrder') && obj.takeOutInOrder.mode == obj.takeOutInOrder.schema.orderPage);
                },
                updateCategoryList: function () {
                    obj.food.category = [];
                    obj.takeOutInOrder.Category.forEach(function (item) {
                        console.log('obj.takeOutInOrder.Category', item)
                        obj.takeOutInOrder.allCategory.forEach(function (cate) {
                            if (cate.code == item.code) {
                                console.log('cate.code == item.code', cate);
                                obj.food.category.push(cate);
                                return false;
                            }
                        })
                    })

                    obj.food.category.forEach(function (c) {
                        c.items = c.items.filter(function (i) { return !(i.suspend === 0) });
                    });

                    obj.food.category.sort((a, b) => {
                        if (a.seq != b.seq) { return a.seq - b.seq }
                    })

                    console.log('obj.food.category', obj.food.category);
                    obj.currentCategory = obj.food.category[0] || { items: [] };
                    obj.noOfCategoryPage = Math.ceil(obj.food.category.length / obj.catsPerPage);
                    obj.totalItems = obj.currentCategory.items.length;
                    obj.noOfItemPage = Math.ceil(obj.totalItems / obj.itemsPerPage);
                    obj.totalCats = obj.food.category.length;
                },
                ResetCategoryList: function () {
                    obj.food.category = obj.takeOutInOrder.allCategory;
                    obj.currentCategory = obj.food.category[0];
                    obj.noOfCategoryPage = Math.ceil(obj.food.category.length / obj.catsPerPage);
                    obj.totalItems = obj.currentCategory.items.length;
                    obj.noOfItemPage = Math.ceil(obj.totalItems / obj.itemsPerPage);
                    obj.totalCats = obj.food.category.length;
                },
                backTakeAwayOrderMode: function () {
                    this.till = userConfig.page.till;
                    this.updateCategoryList();
                    var cloneCartList = false;
                    console.log('', obj.modeOrder)
                    if (obj.modeOrder == obj.schema.modeOrder.specialMessage) cloneCartList = true;
                    console.log('modeOrder', cloneCartList);
                    obj.modeItem = obj.schema.modeItem.normal;
                    obj.modeOrder = obj.schema.modeOrder.normal;
                    obj.modeFloorPlan = obj.schema.modeFloorPlan.order;
                    obj.modeOrderMenu = obj.schema.modeOrderMenu.normal;
                    obj.modeNavMenu = obj.schema.modeNavMenu.takeAwayInOrder;
                    obj.setActive(angular.element('.btn-takeaway-order'));
                    //if (takeAwayType != undefined) {
                    //    switch (takeAwayType) {
                    //        case obj.takeOutInOrder.type.delivery:
                    //            obj.modeNavMenu = obj.schema.modeNavMenu.deliveryOrderInOrder;
                    //            obj.modeTakeAway = obj.schema.modeTakeAway.writeOrder;
                    //            break;
                    //    }
                    //}

                    obj.assignTableNo(obj.takeOutInOrder.till, null, null, false, null);
                    this.mode = this.schema.orderPage;
                },
                cloneCart: [],
                cloneCartList: function () {
                    if (this.cloneCart.length != 0) {
                        //console.log('copy clone cart list')
                        obj.Cart.cartList = this.cloneCart;
                    }
                },
                resetCartList: function () {
                    this.cloneCart = [];
                    obj.Cart.cartList = this.cloneCart;
                    obj.modeItem = obj.schema.modeItem.normal;
                },
                amendBillMode: function () {
                    //obj.mode = obj.schema.mode.takeAway;
                    obj.modeOrder = obj.schema.modeOrder.amendBill;
                    obj.modeTakeAway = obj.schema.modeTakeAway.amendBill;
                },
                searchBillMode: function () {
                    obj.modeOrder = obj.schema.modeOrder.searchBill;
                    obj.modeTakeAway = obj.schema.modeTakeAway.searchBill;
                },
                btnCancelAction: function (callback) {
                    if (obj.isModeTakeAway('writeOrder')) {

                        if (obj.isModeOrder('normal')) {
                            obj.switchMode2({ modeTakeAway: 'writeOrder' }, false, 'take_away');
                            if (obj.isModeItem('modifier')) {
                                obj.takeOutInOrder.cloneCart = obj.Cart.cartList;
                                obj.takeOutInOrder.backTakeAwayOrderMode();
                            } else {
                                obj.takeOutInOrder.backTakeAwayViewMode();
                            }

                            callback('OK');
                        } else if (obj.isModeOrder('billing')) {
                            //obj.switchMode2({ modeItem: 'normal' });
                            obj.switchMode2({ modeTakeAway: 'writeOrder' }, false, 'take_away');
                            obj.takeOutInOrder.backTakeAwayOrderMode();
                            //$rootScope.$emit('returnToOrder', []);
                            callback('OK');
                        } else if (obj.isModeOrder('specialMessage')) {
                            obj.switchMode2({ modeTakeAway: 'writeOrder' }, false, 'take_away');
                            obj.takeOutInOrder.cloneCart = obj.Cart.cartList;
                            obj.takeOutInOrder.backTakeAwayOrderMode();
                            callback('OK');
                        }
                        //return;
                    } else if (obj.isModeTakeAway('amendBill')) {
                        obj.takeOutInOrder.searchBillMode();
                        callback('OK');
                    }
                },
                customerDisplay: {
                    resetCartList: function () {
                        localStorage.setItem("customer-display-cart", "");
                    },
                    activeMode: function () {
                        localStorage.setItem('display-mode', 'takeAway');
                    }

                }
            }

        }
        else {
            //function takeOutInOrder() {
            //    var temp = new otherWriteOrderService();
            //    //temp.modeSetting.mode.value = "a"
            //    return temp;
            //}
            //obj.takeOutInOrder = takeOutInOrder();
            //otherWriteOrderService.children = [];
            //otherWriteOrderService.children.push(obj.takeOutInOrder);

            //obj.takeOutInOrder = new takeOutInOrder();
            //obj.takeOutInOrder.prototype = new otherWriteOrderService();
            //console.debug(obj.takeOutInOrder instanceof otherWriteOrderService);
            //console.debug(obj.takeOutInOrder.isOrderPage());
        }

        /******************************************************************/
        /** init service instance **/

        obj.initOtherWriteOrderService = function (mode) {
            //console.debug('mode', mode);
            switch (mode) {
                case obj.schema.modeOtherWriteOrderService.takeAway:
                    function takeOutInOrder() {
                        var temp = new otherWriteOrderService();
                        temp.modeOtherWriteOrderService = obj.schema.modeOtherWriteOrderService.takeAway;
                        temp.takeOutAndDelivery = userConfig.takeOutAndDelivery;
                        temp.allCategory = angular.copy(otherWriteOrderService.allCategory);
                        if (temp.takeOutAndDelivery) {
                            //console.debug(6746, takeOutAndDelivery.prototype);
                            //console.debug(6746, takeOutAndDelivery.prototype.showInputInfoDialog);
                            $mdDialog.cancel();
                            $mdDialog.hide();
                            temp.takeOutAndDelivery = new takeOutAndDelivery($mdDialog, $compile);
                            temp.takeOutAndDelivery.setSocketFactory(SocketFactory);
                        }
                        //temp.modeSetting.mode.value = "a"
                        return temp;
                    }
                    //obj.takeOutInOrder = takeOutInOrder();
                    otherWriteOrderService.children = [];
                    otherWriteOrderService.children.push(takeOutInOrder());
                    break;
                case obj.schema.modeOtherWriteOrderService.fastFood:
                    function fastFood() {
                        var temp = new otherWriteOrderService();
                        temp.modeSetting.mode.value = "fastFood";
                        temp.modeSetting.modeInService.value = "modeFastFood";
                        temp.modeSetting.modeNavMenu.value = "fastFoodInOrder";
                        temp.modeSetting.modeInService.isMode = function (mode) {
                            return obj.isModeFastFood(mode);
                        }
                        temp.modeSetting.isTakeout = 0;
                        temp.successPaymentAndBackToWriteOrder = function () {
                            otherWriteOrderService.prototype.successPaymentAndBackToWriteOrder.call(this);
                            temp.modeSetting.isTakeout = 0;
                        }
                        temp.backTakeAwayViewMode = function () {
                            otherWriteOrderService.prototype.backTakeAwayViewMode.call(this);
                            temp.modeSetting.isTakeout = 0;
                        }
                        temp.modeOtherWriteOrderService = obj.schema.modeOtherWriteOrderService.fastFood;
                        temp.allCategory = angular.copy(otherWriteOrderService.allCategory);
                        //temp.modeSetting.mode.value = "a"
                        return temp;
                    }
                    //obj.takeOutInOrder = takeOutInOrder();
                    otherWriteOrderService.children = [];
                    otherWriteOrderService.children.push(fastFood());
                    break;
                case obj.schema.modeOtherWriteOrderService.buildBillFromVoid:
                    function buildBillFromVoid() {
                        var temp = new otherWriteOrderService();
                        temp.modeSetting.mode.value = "buildBillFromVoid";
                        temp.modeSetting.modeInService.value = "modeBuildBillFromVoid";
                        temp.modeSetting.modeNavMenu.value = "buildBillFromVoidInOrder";
                        temp.modeSetting.modeInService.isMode = function (mode) {

                            return obj.modeBuildBillFromVoid == obj.schema.modeBuildBillFromVoid[mode];
                            //return obj.isModeBuildBillFromVoid(mode);
                        }
                        temp.modeSetting.isTakeout = 0;
                        temp.successPaymentAndBackToWriteOrder = function () {
                            otherWriteOrderService.prototype.successPaymentAndBackToWriteOrder.call(this);
                            temp.modeSetting.isTakeout = 0;
                        }
                        temp.backTakeAwayViewMode = function () {
                            otherWriteOrderService.prototype.backTakeAwayViewMode.call(this);
                            temp.modeSetting.isTakeout = 0;
                        }
                        temp.modeOtherWriteOrderService = obj.schema.modeOtherWriteOrderService.buildBillFromVoid;
                        temp.allCategory = angular.copy(otherWriteOrderService.allCategory);
                        //temp.modeSetting.mode.value = "a"
                        return temp;
                    }
                    otherWriteOrderService.children = [];
                    otherWriteOrderService.children.push(buildBillFromVoid());
                    break;
                case obj.schema.modeOtherWriteOrderService.deliveryOrder:
                    function deliveryOrder() {
                        var temp = new otherWriteOrderService();
                        temp.modeSetting.mode.value = "deliveryOrder";
                        temp.modeSetting.modeInService.value = "modeDeliveryOrder";
                        temp.modeSetting.modeNavMenu.value = "deliveryOrderInOrder";
                        temp.modeSetting.modeInService.isMode = function (mode) {
                            return obj.isModeDeliveryOrder(mode);
                        }

                        temp.modeOtherWriteOrderService = obj.schema.modeOtherWriteOrderService.deliveryOrder;
                        temp.allCategory = angular.copy(otherWriteOrderService.allCategory);
                        //temp.modeSetting.mode.value = "a"
                        return temp;
                    }
                    //obj.takeOutInOrder = takeOutInOrder();
                    otherWriteOrderService.children = [];
                    otherWriteOrderService.children.push(deliveryOrder());
                    break;
            }

        }
        //obj.initOtherWriteOrderService(obj.schema.modeOtherWriteOrderService.takeAway);


        obj.deliveryOrderService = {
            mode: -1,
            schema: {
                orderPage: 0,
                paymentPage: 1,
                viewPage: 2
            },
            discount: userConfig.delivery_charge,
            isFirst: 1,
            till: userConfig.page.till,
            allCategory: [],
            Category: [],
            updateCategoryList: function () {
                obj.food.category = [];
                obj.deliveryOrderService.Category.forEach(function (item) {
                    obj.deliveryOrderService.allCategory.forEach(function (cate) {
                        if (cate.code == item.code) {
                            console.log('cate.code == item.code', cate);
                            obj.food.category.push(cate);
                            return false;
                        }
                    })
                })

                obj.food.category.forEach(function (c) {
                    c.items = c.items.filter(function (i) { return !(i.suspend === 0) });
                });

                obj.food.category.sort((a, b) => {
                    if (a.seq != b.seq) { return a.seq - b.seq }
                })

                console.log('obj.food.category', obj.food.category);
                obj.currentCategory = obj.food.category[0] || { items: [] };
                obj.noOfCategoryPage = Math.ceil(obj.food.category.length / obj.catsPerPage);
                obj.totalItems = obj.currentCategory.items.length;
                obj.noOfItemPage = Math.ceil(obj.totalItems / obj.itemsPerPage);
                obj.totalCats = obj.food.category.length;
            },
            ResetCategoryList: function () {
                obj.food.category = obj.deliveryOrderService.allCategory;
                obj.currentCategory = obj.food.category[0];
                obj.noOfCategoryPage = Math.ceil(obj.food.category.length / obj.catsPerPage);
                obj.totalItems = obj.currentCategory.items.length;
                obj.noOfItemPage = Math.ceil(obj.totalItems / obj.itemsPerPage);
                obj.totalCats = obj.food.category.length;
            },
            takeOrderMode: function () {
                this.till = userConfig.page.till;
                this.updateCategoryList();
                var cloneCartList = false;
                console.log('', obj.modeOrder)
                if (obj.modeOrder == obj.schema.modeOrder.specialMessage) cloneCartList = true;
                obj.modeItem = obj.schema.modeItem.normal;
                obj.modeOrder = obj.schema.modeOrder.normal;
                obj.modeFloorPlan = obj.schema.modeFloorPlan.order;
                obj.modeOrderMenu = obj.schema.modeOrderMenu.normal;
                obj.modeNavMenu = obj.schema.modeNavMenu.deliveryOrderInOrder;
                obj.setActive(angular.element('.btn-takeaway-order'));
                obj.assignTableNo(obj.deliveryOrderService.till, null, null, false, null);
                obj.deliveryOrderService.mode = obj.deliveryOrderService.schema.orderPage;
            },
            isOrderPage: function () {
                return (obj.isModeDeliveryOrder('writeOrder') && obj.deliveryOrderService.mode == obj.deliveryOrderService.schema.orderPage);
            },
            viewOrderMode: function () {
                this.mode = this.schema.viewPage;
                //obj.switchMode2({ modeFloorPlan: 'order', mode: 'floorPlan', modeItemListDisplay: 'normal', modeItemListDisplay2: 'normal' }, false, 'order_mode', 'order_mode');
                obj.modeItem = obj.schema.modeItem.normal;
                //modeCheckOrder('specialMessage') || modeCheckFloor('kitchenMessage');
                obj.mode = obj.schema.mode.deliveryOrder;
                obj.modeNavMenu = obj.schema.modeNavMenu.deliveryOrder;
                this.cloneCart = [];
            },
            amendBillMode: function () {
                //obj.mode = obj.schema.mode.takeAway;
                obj.modeOrder = obj.schema.modeOrder.amendBill;
                obj.modeDeliveryOrder = obj.schema.modeDeliveryOrder.amendBill;
            },
            searchBillMode: function () {
                obj.modeOrder = obj.schema.modeOrder.searchBill;
                obj.modeDeliveryOrder = obj.schema.modeDeliveryOrder.searchBill;
            },
            resetCartList: function () {
                this.cloneCart = [];
                obj.Cart.cartList = this.cloneCart;
                obj.modeItem = obj.schema.modeItem.normal;
            },
            cloneCart: [],
            cloneCartList: function () {
                if (this.cloneCart.length != 0) {
                    //console.log('copy clone cart list')
                    obj.Cart.cartList = this.cloneCart;
                }
            },
            submitCartInTakeOut: function () {
                if (obj.Cart.cartList.length == 0) return;
                this.mode = this.schema.paymentPage;
                this.cloneCart = obj.Cart.cartList;
                obj.mode = obj.schema.mode.order;
                tableMode = "billing";
                obj.modeOrder = obj.schema.modeOrder[tableMode];
                obj.modeMultiSelectItem = false;
                obj.assign2OrderList([], obj.Cart.orderList);
                //obj.modeItemListDisplay = obj.schema.modeItemListDisplay2.detail;
            },
            btnCancelAction: function (callback) {
                if (obj.isModeDeliveryOrder('writeOrder')) {
                    if (obj.isModeOrder('normal')) {
                        obj.switchMode2({ modeDeliveryOrder: 'writeOrder' }, false, 'delivery_order');
                        if (obj.isModeItem('modifier')) {
                            obj.deliveryOrderService.cloneCart = obj.Cart.cartList;
                            obj.deliveryOrderService.takeOrderMode();
                        } else {
                            obj.deliveryOrderService.viewOrderMode();
                        }

                        callback('OK');
                    } else if (obj.isModeOrder('billing')) {
                        //obj.switchMode2({ modeItem: 'normal' });
                        obj.switchMode2({ modeDeliveryOrder: 'writeOrder' }, false, 'delivery_order');
                        obj.deliveryOrderService.takeOrderMode();
                        //$rootScope.$emit('returnToOrder', []);
                        callback('OK');
                    } else if (obj.isModeOrder('specialMessage')) {
                        obj.switchMode2({ modeDeliveryOrder: 'writeOrder' }, false, 'delivery_order');
                        obj.deliveryOrderService.cloneCart = obj.Cart.cartList;
                        obj.deliveryOrderService.takeOrderMode();
                        callback('OK');
                    }
                    //return;
                } else if (obj.isModeDeliveryOrder('amendBill')) {
                    obj.deliveryOrderService.searchBillMode();
                    callback('OK');
                }
            }
        }

        //obj.checkIsNotTakeAwayMode = function () {
        //    return !(obj.isModeTakeAway('writeOrder') || obj.isModeDeliveryOrder('writeOrder'));
        //}

        obj.checkIsNotTakeAwayMode = function () {
            //return !(obj.isModeTakeAway('writeOrder') || obj.isModeDeliveryOrder('writeOrder'));
            if (otherWriteOrderService.children[0]) {
                return !otherWriteOrderService.children[0].modeSetting.modeInService.isMode('writeOrder') && !obj.checkIsPayLaterMode();
            } else return true;
        }

        obj.checkIsPayLaterMode = function () {
            if (otherWriteOrderService.children[0]) {
                return otherWriteOrderService.children[0].modeSetting.modeInService.isMode('payLater');
            } else return false;
        }

        obj.ResetFirstInTakeAway = function () {
            //obj.selectCategory(MainService.food.category[0])
            $rootScope.$emit('resetCategoryCurrentPage', []);
            console.debug('ResetFirstInTakeAway');
            obj.modeOrder = obj.schema.modeOrder.normal;
            if (otherWriteOrderService.children[0]) {
                var modeName = otherWriteOrderService.children[0].getModeInServiceValue();
                obj[modeName] = obj.schema[modeName].normal;
                otherWriteOrderService.children[0].cloneCart = [];
                otherWriteOrderService.children[0].isFirst = 1;
                otherWriteOrderService.children[0].ResetCategoryList();
                otherWriteOrderService.children = [];
                //MainService.noOfCategoryPage = 1
                return;
            }
            obj.combo.resetAll();
            //obj.modeTakeAway = obj.schema.modeTakeAway.normal;
            //obj.takeOutInOrder.cloneCart = [];
            //obj.takeOutInOrder.isFirst = 1;
            //obj.modeDeliveryOrder = obj.schema.modeDeliveryOrder.normal;
            //obj.deliveryOrderService.cloneCart = [];
            //obj.deliveryOrderService.isFirst = 1;
            //obj.takeOutInOrder.ResetCategoryList();
            //obj.deliveryOrderService.ResetCategoryList();
        }

        obj.enquiryOctopus = function () {
            postData = {
                "user": {
                    "$value": obj.UserManager.staffDetails.staffId
                },
                "refNo": {
                    "$value": obj.Cart.refNo
                },
                "printer": {
                    "$value": userConfig.page.printer
                },
                "till": {
                    "$value": userConfig.page.till
                }
            };
            console.log('enquiryOctopus', postData);

            var content = uiLanguage[INFO.lang]['octopus_waiting_msg'];
            var title = uiLanguage[INFO.lang]['alert'];

            var alert = $mdDialog.show({
                template:
                  '<md-dialog aria-label="List dialog">' +
                  '  <md-dialog-content>' +
                  '<h2 class="md-title">' + title + '</h2>' +
                  '<p>' + content + '</p>' +
                  '  </md-dialog-content>' +
                  '</md-dialog>'
            });

            SocketFactory.emit('enquiryOctopus', postData, function (r) {
                console.log('enquiryOctopus', r);

                obj.Cart.enquiredOctopusBalance = r.cardBalance;
                var title = uiLanguage[INFO.lang]['octopus_wording_enquiry_octopus'];
                if (r.result === "ERROR") {
                    var content = uiLanguage[INFO.lang]['octopus_error_' + r.note];
                }
                else {
                    var content = "<p>" + uiLanguage[INFO.lang]['octopus_wording_octopus_no'] + uiLanguage[INFO.lang]['colon'] + r.cardId + "</p><p>" + uiLanguage[INFO.lang]['octopus_wording_remaining_value'] + uiLanguage[INFO.lang]['colon'] + r.cardBalance + "</p><table>";
                    content += "<thead><tr><th>No.</th><th>Transaction Date Time</th><th>Amount</th><th>Device ID</th></tr></thead><tbody>";
                    r.transactionData.forEach(function (v, i) {
                        if (i < 4) {
                            content += "<tr><td>" + v[0] + "</td><td>" + v[1] + "</td><td>" + v[2] + "</td><td>" + v[3] + "</th></tr>";
                        }
                    });
                    content += "</tbody></table>";
                }
                var alert = $mdDialog.show({
                    template:
                        '<md-dialog aria-label="List dialog">' +
                        '  <md-dialog-content>' +
                        '   <h2 class="md-title">' + title + '</h2>' +
                        '   <p>' + content + '</p>' +
                        '  </md-dialog-content>' +
                        '  <div class="md-actions">' +
                        '    <md-button ng-click="closeDialog()" class="md-primary">' +
                                uiLanguage[INFO.lang]['ok'] +
                        '    </md-button>' +
                        '  </div>' +
                        '</md-dialog>',
                    controller: function DialogController($scope, $mdDialog) {
                        $scope.closeDialog = function () {
                            $mdDialog.hide();
                            postData.cancelTransaction = true;
                            //SocketFactory.emit('cancelIncompleteTransaction', postData, function () { });
                            obj.Cart.enquiredOctopusBalance = null;
                        }
                    }
                });
            });
        }

        var checkSubmitCart = false;
        obj.submitCart = function () {
            if (!obj.checkIsNotTakeAwayMode() && obj.modeOrder != obj.schema.modeOrder.billing) {
                obj.modeItemListDisplay = obj.schema.modeItemListDisplay.normal;

                if (otherWriteOrderService.children[0])
                    if (otherWriteOrderService.children[0].modeSetting.modeInService.isMode('writeOrder')) {
                        otherWriteOrderService.children[0].submitCartInTakeOut();
                        return;
                    }
                //else if (obj.isModeDeliveryOrder('writeOrder')) {
                //    this.deliveryOrderService.submitCartInTakeOut();
                //    return;
                //}
            }
            //console.log('obj.mode beginning');
            //console.log(obj.mode);
            //console.log(obj.modeFloorPlan);

            //alert(obj.modeOrder);
            if (obj.modeOrder == obj.schema.modeOrder.foodControl) {
                obj.modeOrder = obj.schema.modeOrder.normal;
            }
            else {
                console.debug(obj.checkingPaymentPermission);
                if (!checkSubmitCart || obj.checkingPaymentPermission) {
                    checkSubmitCart = true;
                    $timeout(function () {
                        checkSubmitCart = false;
                    }, 3000)
                }
                else {
                    console.log('return checkSubmitCart', checkSubmitCart);
                    return;
                }

                obj.lockTable.toReleaseLockTable(obj.Cart.tableNo);

                if (obj.modeOrder == obj.schema.modeOrder.billing || obj.modeOrder == obj.schema.modeOrder.amendBill) {
                    var priceObj = obj.calcPrice([obj.Cart.cartList, obj.Cart.orderList], obj.Cart.couponObj);

                    // needle
                    // alert(userConfig.page.printer)
                    // return;

                    this.checkPaymentMethod(); // will preset payment method if nothing is selected

                    // calculate total price for verification
                    var totalAmountOfAllPaymentMethod = 0;
                    for (var i = 0; i < this.appliedPaymentMethod.length; i++) {
                        console.log(this.appliedPaymentMethod[i].amount);
                        console.log(this.appliedPaymentMethod[i]);
                        totalAmountOfAllPaymentMethod += this.appliedPaymentMethod[i].amount;
                    }
                    if (obj.deposit) {
                        if (obj.deposit.appliedDeposit.length != 0) {
                            totalAmountOfAllPaymentMethod += obj.deposit.appliedDeposit[0].paymentPrice;
                        }
                    }
                    this.remainder(); // update remainder in case it is default

                    something = this.appliedPaymentMethod; // global debug needle
                    console.log('verification:');
                    console.log('total amount ' + priceObj.totalPrice);
                    console.log('sum of method ' + totalAmountOfAllPaymentMethod);

                    var payLater = false;
                    if (arguments.length != 0) {
                        if (typeof arguments[0] == "object") {
                            if (arguments[0].payLater) {
                                payLater = true;
                            }
                        }
                    }

                    console.debug(payLater);
                    // payment, insufficient fund
                    if (totalAmountOfAllPaymentMethod < obj.Cart.totalPrice && !payLater) {
                        obj.showAlert('please_set_amount');
                        checkSubmitCart = false;
                        return;
                    }
                    // no payment method is selected, default is Cash however, this is a backup test
                    if (obj.appliedPaymentMethod.length === 0 && obj.deposit.appliedDeposit.length == 0 && !payLater) {
                        obj.showAlert('please_select_payment_method');
                        checkSubmitCart = false;
                        return;
                    }



                    console.log('obj.appliedPaymentMethod');
                    console.log(obj.appliedPaymentMethod);
                    console.log('obj.Cart.couponObj');
                    console.log(obj.Cart.couponObj);
                    var postData = {}

                    postData = {
                        "user": {
                            "$value": obj.UserManager.staffDetails.staffId
                        },
                        "tableNum": {
                            "$value": obj.Cart.tableNo
                        },
                        "peopleNum": {
                            "$value": obj.Cart.noOfPeople
                        },
                        "price": {
                            "$value": priceObj.price
                        },
                        "payAmount": {
                            "$value": obj.inputPrice
                        },
                        "payMethod": {
                            "$value": obj.appliedPaymentMethod
                        },
                        "discountMethod": {
                            "$value": obj.Cart.couponObj
                        },
                        "remainings": {
                            "$value": priceObj.remainings
                        },
                        "serviceCharge": {
                            "$value": priceObj.serviceCharge
                        },
                        "refNo": {
                            "$value": obj.Cart.refNo
                        },
                        "printer": {
                            "$value": userConfig.page.printer
                        },
                        "till": {
                            "$value": userConfig.page.till
                        }
                    };
                    //if (obj.Cart.member != null) {
                    //    postData.member = {};
                    //    postData.member.$value = obj.Cart.member;
                    //}
                    //alert(obj.inputPrice)

                    var socketAction = 'billTableOrder';
                    if (obj.modeOrder == obj.schema.modeOrder.amendBill) {
                        socketAction = 'amendBill';
                        postData = {
                            "user": {
                                "$value": obj.UserManager.staffDetails.staffId
                            },
                            "payMethod": {
                                "$value": obj.appliedPaymentMethod
                            },
                            "refNo": {
                                "$value": obj.Cart.refNo
                            },
                            "printer": {
                                "$value": userConfig.page.printer
                            },
                            "till": {
                                "$value": userConfig.page.till
                            },
                            "voidBill": []
                        };


                        if (userConfig.ecrPayment && false) {
                            var ecrVoid = obj.cloneAppliedPaymentMethod.filter(function (c) {
                                return c.void == 1 && c.tenderType == 'card';
                            });
                            postData.voidBill = ecrVoid;
                            var isVoidBill = false;
                            if (postData.voidBill.length != 0) {
                                var content = "等待取消帳單";
                                var title = uiLanguage[INFO.lang]['alert'];

                                var alert = $mdDialog.show({
                                    template:
                                      '<md-dialog aria-label="List dialog">' +
                                      '  <md-dialog-content>' +
                                      '<h2 class="md-title">' + title + '</h2>' +
                                      '<p>' + content + '</p>' +
                                      '  </md-dialog-content>' +
                                      '</md-dialog>'
                                });

                                isVoidBill = true;
                            }

                        }
                        if (userConfig.ecrPayment) {
                            socketAction = "insertPayment";
                        }
                    }



                    postData.isUseECR = userConfig.ecrPayment;

                    //return;

                    if (!obj.checkIsNotTakeAwayMode() && socketAction == "billTableOrder") {

                        socketAction = 'BillTableOrderByTakeOut';
                        var order = { item: [] };

                        var currentCartList = obj.Cart.cartList;
                        if (obj.combo.checkIsFastFoodMode()) currentCartList = obj.combo.getSubmitOrder();

                        angular.forEach(currentCartList, function (item) {
                            var itemType = "I"
                            if (item.type) {
                                if (item.type == "T") {
                                    itemType = "T";
                                }
                            }

                            order.item.push({
                                code: [item.code], qty: [item.qty], unitPrice: [item.getUnitPrice() * 100], voidIndex: [item.voidIndex], voidRemark: [item.voidRemark], type: [itemType], desc1: [item.getName()]
                                , customName: [item.customName], voidApproveStaff: [item.voidApproveStaff],
                            });

                            if (item.modifier.length != 0) {

                                $.each(item.modifier, function (modifierIndex, modifier) {
                                    order.item.push({
                                        desc1: [modifier.label],
                                        code: [modifier.code],
                                        qty: [1],
                                        unitPrice: [0],
                                        voidIndex: [-1],
                                        voidRemark: [''],
                                        voidApproveStaff: [undefined],
                                        type: "M"
                                    });
                                })
                            }
                            if (item.kitchenMsg.length != 0) {

                                $.each(item.kitchenMsg, function (kitchenMsgIndex, kitchenMsg) {
                                    order.item.push({
                                        desc1: [kitchenMsg.label],
                                        code: [kitchenMsg.code],
                                        qty: [1],
                                        unitPrice: [0],
                                        voidIndex: [-1],
                                        voidRemark: [''],
                                        voidApproveStaff: [undefined],
                                        type: "K"
                                    });
                                })
                            }
                            if (item.option != undefined && item.option.length != 0) {
                                $.each(item.option, function (optionIndex, option) {
                                    $.each(option.items, function (optionItemIndex, optionItem) {
                                        if (optionItem.qty > 0) {
                                            order.item.push({
                                                desc1: [optionItem.getName()],
                                                code: [optionItem.code],
                                                qty: [optionItem.getQty()],
                                                unitPrice: [optionItem.getUnitPrice() * 100],
                                                voidIndex: [optionItem.voidIndex],
                                                voidRemark: [optionItem.voidRemark],
                                                voidApproveStaff: [optionItem.voidApproveStaff],
                                                type: "O"
                                            });
                                            $.each(optionItem.modifier, function (modifierIndex, modifier) {
                                                order.item.push({
                                                    desc1: [modifier.label],
                                                    code: [modifier.code],
                                                    qty: [1],
                                                    unitPrice: [0],
                                                    voidIndex: [-1],
                                                    voidRemark: [''],
                                                    voidApproveStaff: [undefined],
                                                    type: "M"
                                                });
                                            })
                                            $.each(optionItem.kitchenMsg, function (kitchenMsgIndex, kitchenMsg) {
                                                order.item.push({
                                                    desc1: [kitchenMsg.label],
                                                    code: [kitchenMsg.code],
                                                    qty: [1],
                                                    unitPrice: [0],
                                                    voidIndex: [-1],
                                                    voidRemark: [''],
                                                    voidApproveStaff: [undefined],
                                                    type: "K"
                                                });
                                            })
                                        }
                                    })
                                });
                            }
                            if (item.subitem != undefined && item.subitem.length != 0) {
                                $.each(item.subitem, function (subitemIndex, subitem) {
                                    order.item.push({
                                        desc1: [subitem.getName()],
                                        code: [subitem.code],
                                        qty: [subitem.getQty()],
                                        unitPrice: [subitem.getUnitPrice() * 100],
                                        voidIndex: [subitem.voidIndex],
                                        voidRemark: [subitem.voidRemark],
                                        voidApproveStaff: [subitem.voidApproveStaff],
                                        type: "S"
                                    });
                                    $.each(subitem.modifier, function (modifierIndex, modifier) {
                                        order.item.push({
                                            desc1: [modifier.label],
                                            code: [modifier.code],
                                            qty: [1],
                                            unitPrice: [0],
                                            voidIndex: [-1],
                                            voidRemark: [''],
                                            voidApproveStaff: [undefined],
                                            type: ["M"]
                                        });
                                    });
                                    /*
                                    $.each(subitem.kitchenMsgOld, function (kitchenMsgIndex, kitchenMsg) {
                                        order.item.push({
                                            desc1: [kitchenMsg.label],
                                            code: [kitchenMsg.code],
                                            qty: [1],
                                            unitPrice: [0],
                                            voidIndex: [-1],
                                            voidRemark: [''],
                                            voidApproveStaff: [undefined],
                                            type: ["K"]
                                        });
                                    });
                                    */
                                    $.each(subitem.kitchenMsg, function (kitchenMsgIndex, kitchenMsg) {
                                        order.item.push({
                                            desc1: [kitchenMsg.label],
                                            code: [kitchenMsg.code],
                                            qty: [1],
                                            unitPrice: [0],
                                            voidIndex: [-1],
                                            voidRemark: [''],
                                            voidApproveStaff: [undefined],
                                            type: ["K"]
                                        });
                                    });
                                });
                            }
                        });

                        postData.itemData = { "json": { "order": order } };
                        console.debug('6839', postData);


                        if (payLater) {
                            obj.appliedPaymentMethod = [];
                            postData.payMethod.$value = [];
                            postData.discountMethod.$value = [];
                            postData.customerData = obj.getOtherWriteOrderService().takeOutAndDelivery.customerData;
                            postData.isPayLater = true;
                            postData.remainings.$value = "0";
                            obj.deposit.appliedDeposit = []
                        }

                        //return;

                        if (obj.getOtherWriteOrderService().modeSetting.modeInService.isMode('payLater')) {
                            postData.setPaymentForPayLater = true;
                            postData.payLaterRefNo = obj.Cart.refNo;
                        }

                        //return;
                        //postData.till = userConfig.page.till;
                        //if (obj.isModeTakeAway('writeOrder'))
                        //    postData.insertType = "takeaway";
                        //else if (obj.isModeDeliveryOrder('writeOrder'))
                        //    postData.insertType = "delivery";

                        if (obj.getOtherWriteOrderService().mode == obj.getOtherWriteOrderService().schema.paymentPage) {
                            postData.insertType = obj.getOtherWriteOrderService().modeSetting.mode.value;

                            //console.debug(postData);
                            //console.debug('BillTableOrderByTakeOut')
                            //return;

                            if (obj.getOtherWriteOrderService().modeSetting.mode.value == 'fastFood') {
                                postData.isTakeout = obj.getOtherWriteOrderService().modeSetting.isTakeout;

                            }
                        }

                        console.log('BillTableOrderByTakeOut', order);
                    }

                    //console.log(postData);
                    //return;

                    var isOpenPaymentKeypad = false;
                    var keypadPaymentTender = {};
                    var showFailAlert = true;
                    var isPermission = false;

                    //check permission first
                    if (!obj.checkingPaymentPermission) {
                        obj.appliedPaymentMethod.forEach(function (payMethod) {
                            var target = obj.tender.filter(function (t) {
                                return t.code == payMethod.method;
                            });

                            if (target[0].permission) {
                                isPermission = true;
                            }
                        });
                    }

                    if (isPermission && !obj.checkingPaymentPermission) {
                        if (!obj.UserManager.staffDetails[obj.schema.permission["paymentPermission"]]) {
                            $mdDialog.show({
                                templateUrl: 'tmpl/paymentPermission.html',
                                targetEvent: event,
                                focusOnOpen: true,
                                bindToController: true,
                                preserveScope: true,
                            })
                            return;
                        }
                    }

                    delete obj.checkingPaymentPermission;

                    if (userConfig.page.multiPaymentPanel && userConfig.page.individualPayment) {

                        if ((obj.appliedPaymentMethod.length > 1 && obj.deposit.appliedDeposit.length == 0) || (obj.appliedPaymentMethod.length > 2 && obj.deposit.appliedDeposit.length != 0)) {
                            var stop = false

                            if ((obj.multiPayment.getAmount('totalAmount', { submitChecking: true }) < obj.Cart.totalPrice)) {
                                obj.showAlert(obj.label.txt_multipay_error1);
                                return;
                            }

                            if ((obj.multiPayment.getAmount('totalAmount', { submitChecking: true }) > obj.Cart.totalPrice)) {
                                obj.showAlert(obj.label.txt_multipay_error2);
                                return;
                            }


                            obj.appliedPaymentMethod.forEach(function (m) {
                                if (m.amount < m.totalAmount) {
                                    obj.showAlert(obj.label.txt_multipay_error3);
                                    stop = true;
                                    return false;
                                }
                                else if ((m.totalAmount == 0 && m.amount != 0)) {
                                    obj.showAlert(obj.label.txt_multipay_error4);
                                    stop = true;
                                    return false;
                                }
                            })

                            if (stop) return;


                        }

                    }

                    obj.appliedPaymentMethod.forEach(function (payMethod) {

                        if (payMethod.tenderType == "card" && userConfig.ecrPayment) {
                            if (socketAction == 'insertPayment') {
                                if (!payMethod.isNewAdd) return true;
                            }


                            if (obj.getOtherWriteOrderServiceFullDetail().isCardManually) {
                                isOpenPaymentKeypad = true;
                                keypadPaymentTender = angular.copy(payMethod);
                                return true;
                            }

                            var content = "等待付款";
                            var title = uiLanguage[INFO.lang]['alert'];

                            var alert = $mdDialog.show({
                                template:
                                    '<md-dialog aria-label="List dialog">' +
                                    '  <md-dialog-content>' +
                                    '<h2 class="md-title">' + title + '</h2>' +
                                    '<p>' + content + '</p>' +
                                    '<button class="md-primary md-button md-default-theme" style="float:right;font-size:18px;margin:0" ng-click="btnClick();">鍵盤輸入</button>' +
                                    '  </md-dialog-content>' +
                                    '</md-dialog>',
                                controller: function ($scope) {
                                    $scope.btnClick = function () {
                                        $mdDialog.cancel();
                                        showFailAlert = false;
                                        keypadPaymentTender = angular.copy(payMethod);
                                        obj.getOtherWriteOrderServiceFullDetail().openPaymentKeypad(keypadPaymentTender);
                                        obj.getOtherWriteOrderServiceFullDetail().paymentKeypadCurrentPostData = { action: socketAction, data: angular.copy(postData) };
                                        return;
                                    }
                                }
                            });



                        }

                        if (payMethod.method === "octopus2") {
                            //var alert = $mdDialog.alert({
                            //    title: uiLanguage[INFO.lang]['alert'],
                            //    content: uiLanguage[INFO.lang]['octopus_waiting_msg']
                            //});
                            //$mdDialog
                            //  .show(alert)
                            //  .finally(function () {
                            //      alert = undefined;
                            //  });

                            var content = uiLanguage[INFO.lang]['octopus_waiting_msg'];
                            var title = uiLanguage[INFO.lang]['alert'];

                            var alert = $mdDialog.show({
                                template:
                                  '<md-dialog aria-label="List dialog">' +
                                  '  <md-dialog-content>' +
                                  '<h2 class="md-title">' + title + '</h2>' +
                                  '<p>' + content + '</p>' +
                                  '  </md-dialog-content>' +
                                  '</md-dialog>'
                            });

                            //$timeout(function () {
                            //    $mdDialog.hide();
                            //},2000)

                        }
                    });

                    if (isOpenPaymentKeypad) {
                        obj.getOtherWriteOrderServiceFullDetail().openPaymentKeypad(keypadPaymentTender);
                        obj.getOtherWriteOrderServiceFullDetail().paymentKeypadCurrentPostData = { action: socketAction, data: angular.copy(postData) };
                        return;
                    }

                    //var confirm = $mdDialog.confirm()
                    //        .title(uiLanguage[INFO.lang]['alert'])
                    //        .content("waiting for octopus")
                    //$mdDialog.show(confirm);

                    postData.token = {};
                    postData.token = obj.clientIp + "-" + moment().format('YYYYMMDDHHmmss');

                    postData.appliedDeposit = {}
                    postData.appliedDeposit.$value = [];
                    if (obj.deposit) {
                        postData.appliedDeposit.$value = obj.deposit.appliedDeposit;
                    }
                    //console.debug(7447, postData);
                    //return;

                    var incompTransactionTime = null;
                    var firstLoopTimeout = 0;
                    var emitData = function () {
                        SocketFactory.emit(socketAction, postData, function (r) {
                            if (r == null) return;
                            r = JSON.parse(r);


                            console.log(r);
                            console.log(r.refNo);
                            $mdDialog.cancel();

                            if (r.result === "OK") {

                                if (socketAction == "BillTableOrderByTakeOut") {
                                    if (obj.getOtherWriteOrderService().takeOutAndDelivery) obj.getOtherWriteOrderService().takeOutAndDelivery.resetCustomerData();
                                }

                                if (obj.combo.checkIsFastFoodMode()) obj.combo.resetAll();
                                if (socketAction == 'amendBill' || socketAction == 'insertPayment') {
                                    obj.cloneAppliedPaymentMethod = [];

                                    if (otherWriteOrderService.children[0]) {

                                        if (otherWriteOrderService.children[0].modeSetting.modeInService.isMode('amendBill')) {
                                            if (userConfig.ecrPayment) {

                                                MainService.ecrChangePaymentMode = 0;
                                                MainService.cloneAppliedPaymentMethod = [];
                                                MainService.appliedPaymentMethod.forEach(function (apm) {
                                                    delete apm.isNewAdd;
                                                })
                                                //console.debug("refreshPaymentList", r.resultList);
                                                $rootScope.$emit('refreshPaymentList', r.resultList);
                                                //return;
                                            }

                                            otherWriteOrderService.children[0].searchBillMode();
                                            obj.modeOrder = obj.schema.modeOrder.searchBill;
                                            return;
                                        }

                                    } else {
                                        obj.switchMode2({ modeOrder: 'searchBill' });
                                    }



                                    //if (obj.isModeDeliveryOrder('amendBill')) {
                                    //    obj.deliveryOrderService.searchBillMode();
                                    //    return;
                                    //}

                                }
                                else {
                                    obj.lastBillReference = r.refNo;
                                    obj.lastTableCode = r.tableCode;
                                    obj.lastTableSubCode = r.tableSubCode;
                                    obj.Cart.octopusBalance = r.octopusBalance;

                                    if (!obj.checkIsNotTakeAwayMode()) {
                                        obj.lastTableCode = "";
                                        obj.lastTableSubCode = "";
                                        obj.modeOrder = obj.schema.modeOrder.billingSummary;
                                        //MainService.mode = MainService.schema.mode.takeAway;
                                    }

                                    //console.debug(7710, r);
                                    MainService.Cart.paymentLater = r.paymentLater;
                                    // jump to summary page instead of floor plan first
                                    obj.modeOrder = obj.schema.modeOrder.billingSummary;
                                }
                                // obj.mode = obj.schema.mode.floorPlan;
                                console.log('obj.mode ');
                                console.log(obj.mode);
                            }
                            else if (r.result === "ERROR" || r.result === "fail") {

                                //var content = uiLanguage[INFO.lang]['octopus_error_' + r.note].replace("[OCTOPUS_NO]", r.cardId);
                                var content = uiLanguage[INFO.lang]['octopus_error_' + r.note];
                                if (typeof content == 'undefined') {
                                    content = uiLanguage[INFO.lang]['octopus_error_others'].replace("[ERROR_CODE]", r.note);
                                }
                                var title = uiLanguage[INFO.lang]['alert'];

                                if ((r.note === "100022" || (postData.previousErrorCode && postData.previousErrorCode === "100022"))) {
                                    obj.withinFirstLoop = true; // first 20 seconds infinitly retry loop
                                    var interval = 20000;
                                    if (!incompTransactionTime) {
                                        incompTransactionTime = new Date(r.timestamp);
                                    }
                                    else {
                                        interval = new Date(r.timestamp) - incompTransactionTime;
                                        if (interval > 20000) {
                                            obj.withinFirstLoop = false;
                                        }
                                    }
                                    if (firstLoopTimeout) {
                                        clearTimeout(firstLoopTimeout);
                                    }
                                    firstLoopTimeout = setTimeout(function () {
                                        obj.withinFirstLoop = false;
                                    }, interval);

                                    postData.token = obj.clientIp + "-" + moment().format('YYYYMMDDHHmmss');
                                    postData.retryRefNo = { "$value": r.invoice };

                                    if (!(postData.previousErrorCode === "100022")) {
                                        postData.previousErrorCode = r.note;
                                    }
                                    content = uiLanguage[INFO.lang]['octopus_error_' + postData.previousErrorCode];
                                    if (r.note === "notsamecard") {
                                        content = uiLanguage[INFO.lang]['octopus_error_' + r.note].replace("[OCTOPUS_NO]", r.cardId);
                                    }
                                    var alert = $mdDialog.show({
                                        template:
                                            '<md-dialog aria-label="List dialog">' +
                                            '  <md-dialog-content>' +
                                            '    <h2 class="md-title">' + title + '</h2>' +
                                            '    <p>' + content + '</p>' +
                                            '  </md-dialog-content>' +
                                            '  <div class="md-actions" ng-if="!withinFirstLoop">' +
                                            '    <md-button ng-click="retry()" class="md-primary">' +
                                                    uiLanguage[INFO.lang]['retry'] +
                                            '    </md-button>' +
                                            '    <md-button ng-click="closeOctopusDialog()" class="md-primary">' +
                                                    uiLanguage[INFO.lang]['cancel'] +
                                            '    </md-button>' +
                                            '  </div>' +
                                          '</md-dialog>',
                                        controller: function DialogController($scope, $mdDialog) {
                                            $scope.withinFirstLoop = obj.withinFirstLoop;
                                            $scope.closeOctopusDialog = function () {
                                                $mdDialog.hide();
                                                SocketFactory.emit('cancelIncompleteTransaction', postData, function () { });
                                            }
                                            $scope.retry = function () {
                                                emitData();
                                            }
                                        }
                                    });

                                    if (obj.withinFirstLoop)
                                        emitData();

                                }
                                else {
                                    obj.showAlert(content);
                                }
                                //var confirm = $mdDialog.confirm()
                                //           .title(uiLanguage[INFO.lang]['alert'])
                                //           .content("octopus errer " + r.note)
                                //           .ok(uiLanguage[INFO.lang]['yes'])
                                //$mdDialog.show(confirm);
                                //if (r.note === "100022" || r.note === "100025") {
                                //    SocketFactory.emit(socketAction, postData, function (r) {
                                //        if (r.result === "ERROR" || r.result === "fail") {
                                //            obj.showAlert(ui['octopus_error_after_retry'].replace('[OCTOPUS_CODE]', r.note));
                                //        }
                                //    });
                                //}
                            }
                            else if (r.result === 'failInCard' && showFailAlert) {
                                obj.showAlert('付款失敗');
                            }
                            /// todo: handle octopus error message

                            checkSubmitCart = false;
                        });
                    }

                    emitData();
                    //emitData();
                }
                else {
                    // var order = { item: [] };
                    // angular.forEach(obj.Cart.cartList, function (item) {
                    //     console.log('item');
                    //     console.log(item);
                    //     var itemType = "I"
                    //     if (item.type) {
                    //         if (item.type == "T") {
                    //             itemType = "T";
                    //         }
                    //     }
                    //     order.item.push({
                    //         code: [item.code], qty: [item.qty], unitPrice: [item.getUnitPrice() * 100], voidIndex: [item.voidIndex], voidRemark: [item.voidRemark], type: [itemType], desc1: [item.getName()]
                    //         , customName: [item.customName] ,voidApproveStaff: [item.voidApproveStaff],
                    //     });
                    //     if (item.option != undefined && item.option.length != 0) {
                    //         $.each(item.option, function (optionIndex, option) {
                    //             $.each(option.items, function (optionItemIndex, optionItem) {
                    //                 if (optionItem.qty > 0) {
                    //                     order.item.push({
                    //                         desc1: [optionItem.getName()],
                    //                         code: [optionItem.code],
                    //                         qty: [optionItem.getQty()],
                    //                         unitPrice: [optionItem.getUnitPrice() * 100],
                    //                         voidIndex: [optionItem.voidIndex],
                    //                         voidRemark: [optionItem.voidRemark],
                    //                         voidApproveStaff: [optionItem.voidApproveStaff],
                    //                         type: "O"
                    //                     });
                    //                     $.each(optionItem.modifier, function (modifierIndex, modifier) {
                    //                         order.item.push({
                    //                             desc1: [modifier.label],
                    //                             code: [modifier.code],
                    //                             qty: [1],
                    //                             unitPrice: [0],
                    //                             voidIndex: [-1],
                    //                             voidRemark: [''],
                    //                             voidApproveStaff: [undefined],
                    //                             type: "M"
                    //                         });
                    //                     })
                    //                     $.each(optionItem.kitchenMsg, function (kitchenMsgIndex, kitchenMsg) {
                    //                         order.item.push({
                    //                             desc1: [kitchenMsg.label],
                    //                             code: [kitchenMsg.code],
                    //                             qty: [1],
                    //                             unitPrice: [0],
                    //                             voidIndex: [-1],
                    //                             voidRemark: [''],
                    //                             voidApproveStaff: [undefined],
                    //                             type: "K"
                    //                         });
                    //                     })
                    //                 }
                    //             })
                    //         });
                    //     }
                    //     if (item.subitem != undefined && item.subitem.length != 0) {
                    //         $.each(item.subitem, function (subitemIndex, subitem) {
                    //             order.item.push({
                    //                 desc1: [subitem.getName()],
                    //                 code: [subitem.code],
                    //                 qty: [subitem.getQty()],
                    //                 unitPrice: [subitem.getUnitPrice() * 100],
                    //                 voidIndex: [subitem.voidIndex],
                    //                 voidRemark: [subitem.voidRemark],
                    //                 voidApproveStaff: [subitem.voidApproveStaff],
                    //                 type: "S"
                    //             });
                    //         });
                    //     }
                    //     if (item.modifier.length != 0) {
                    //         $.each(item.modifier, function (modifierIndex, modifier) {
                    //             order.item.push({
                    //                 desc1: [modifier.label],
                    //                 code: [modifier.code],
                    //                 qty: [1],
                    //                 unitPrice: [0],
                    //                 voidIndex: [-1],
                    //                 voidRemark: [''],
                    //                 voidApproveStaff: [undefined],
                    //                 type: "M"
                    //             });
                    //         })
                    //     }
                    //     if (item.kitchenMsg.length != 0) {
                    //         $.each(item.kitchenMsg, function (kitchenMsgIndex, kitchenMsg) {
                    //             order.item.push({
                    //                 desc1: [kitchenMsg.label],
                    //                 code: [kitchenMsg.code],
                    //                 qty: [1],
                    //                 unitPrice: [0],
                    //                 voidIndex: [-1],
                    //                 voidRemark: [''],
                    //                 voidApproveStaff: [undefined],
                    //                 type: "K"
                    //             });
                    //         })
                    //     }
                    // });

                    if (obj.modeFloorPlan == obj.schema.modeFloorPlan.kitchenMessage) {
                        var existingOrder = { item: [] };
                        var allOrder = { item: [] };
                        console.log(obj.Cart.orderList);
                        angular.forEach(obj.Cart.orderList, function (item) {
                            console.log('item in existingOrder list');
                            console.log(item);
                            var itemType = "I"
                            var addItem = false;
                            if (item.type) {
                                if (item.type == "T") {
                                    itemType = "T";
                                }
                            }

                            // outermost parent item
                            if (item.kitchenMsg.length != 0) {
                                existingOrder.item.push({
                                    code: [item.code], qty: [item.qty], unitPrice: [item.getUnitPrice() * 100], voidIndex: [item.voidIndex], voidRemark: [item.voidRemark], type: [itemType], desc1: [item.getName()]
                                    , customName: [item.customName], voidApproveStaff: [item.voidApproveStaff], isFired: item.checkIsFireItem(true), seq: item.index
                                });
                                addItem = true;
                                $.each(item.kitchenMsg, function (kitchenMsgIndex, kitchenMsg) {
                                    existingOrder.item.push({
                                        desc1: [kitchenMsg.label],
                                        code: [kitchenMsg.code],
                                        qty: [1],
                                        unitPrice: [0],
                                        voidIndex: [-1],
                                        voidRemark: [''],
                                        voidApproveStaff: [undefined],
                                        type: "K",
                                        index: item.index
                                    });
                                })
                            }

                            if (item.option != undefined && item.option.length != 0) {
                                $.each(item.option, function (optionIndex, option) {
                                    $.each(option.items, function (optionItemIndex, optionItem) {
                                        if (optionItem.qty > 0) {
                                            if (optionItem.kitchenMsg.length > 0) {
                                                if (!addItem) {

                                                    existingOrder.item.push({
                                                        code: [item.code], qty: [item.qty], unitPrice: [item.getUnitPrice() * 100], voidIndex: [item.voidIndex], voidRemark: [item.voidRemark], type: [itemType], desc1: [item.getName()]
                                                        , customName: [item.customName], voidApproveStaff: [item.voidApproveStaff],
                                                    });
                                                    addItem = true;
                                                }
                                                existingOrder.item.push({
                                                    code: [optionItem.code], qty: [optionItem.qty], unitPrice: [optionItem.getUnitPrice() * 100], voidIndex: [optionItem.voidIndex], voidRemark: [optionItem.voidRemark], type: ["O"], desc1: [optionItem.getName()]
                                   , customName: [optionItem.customName], voidApproveStaff: [optionItem.voidApproveStaff],
                                                });
                                                $.each(optionItem.kitchenMsg, function (kitchenMsgIndex, kitchenMsg) {
                                                    existingOrder.item.push({
                                                        desc1: [kitchenMsg.label],
                                                        code: [kitchenMsg.code],
                                                        qty: [1],
                                                        unitPrice: [0],
                                                        voidIndex: [-1],
                                                        voidRemark: [''],
                                                        voidApproveStaff: [undefined],
                                                        type: "K",
                                                        index: optionItem.index
                                                    });
                                                })
                                            }
                                        }
                                    });
                                });
                            }

                            if (item.subitem != undefined && item.subitem.length != 0) {
                                $.each(item.subitem, function (subitemIndex, subitem) {
                                    if (subitem.qty > 0) {
                                        console.log('subitem.qty > 0');
                                        if (subitem.kitchenMsg.length > 0) {
                                            if (!addItem) {

                                                existingOrder.item.push({
                                                    code: [item.code], qty: [item.qty], unitPrice: [item.getUnitPrice() * 100], voidIndex: [item.voidIndex], voidRemark: [item.voidRemark], type: [itemType], desc1: [item.getName()]
                                                    , customName: [item.customName], voidApproveStaff: [item.voidApproveStaff],
                                                });
                                                addItem = true;
                                            }
                                            existingOrder.item.push({
                                                code: [subitem.code], qty: [subitem.qty], unitPrice: [subitem.getUnitPrice() * 100], voidIndex: [subitem.voidIndex], voidRemark: [subitem.voidRemark], type: ["S"], desc1: [subitem.getName()]
                                   , customName: [subitem.customName], voidApproveStaff: [subitem.voidApproveStaff],
                                            });

                                            $.each(subitem.kitchenMsg, function (kitchenMsgIndex, kitchenMsg) {
                                                existingOrder.item.push({
                                                    desc1: [kitchenMsg.label],
                                                    code: [kitchenMsg.code],
                                                    qty: [1],
                                                    unitPrice: [0],
                                                    voidIndex: [-1],
                                                    voidRemark: [''],
                                                    voidApproveStaff: [undefined],
                                                    type: "K",
                                                    index: subitem.index
                                                });
                                            })
                                        }
                                    }
                                });
                            }

                            console.log(item);

                            /*
                            existingOrder.item.push({
                                code: [item.code], qty: [item.qty], unitPrice: [item.getUnitPrice() * 100], voidIndex: [item.voidIndex], voidRemark: [item.voidRemark], type: [itemType], desc1: [item.getName()]
                                , customName: [item.customName] ,voidApproveStaff: [item.voidApproveStaff],
                            });
                            if (item.option != undefined && item.option.length != 0) {
                                $.each(item.option, function (optionIndex, option) {
                                    $.each(option.items, function (optionItemIndex, optionItem) {
                                        if (optionItem.qty > 0) {
                                            existingOrder.item.push({
                                                desc1: [optionItem.getName()],
                                                code: [optionItem.code],
                                                qty: [optionItem.getQty()],
                                                unitPrice: [optionItem.getUnitPrice() * 100],
                                                voidIndex: [optionItem.voidIndex],
                                                voidRemark: [optionItem.voidRemark],
                                                voidApproveStaff: [optionItem.voidApproveStaff],
                                                type: "O"
                                            });
                                            $.each(optionItem.modifier, function (modifierIndex, modifier) {
                                                existingOrder.item.push({
                                                    desc1: [modifier.label],
                                                    code: [modifier.code],
                                                    qty: [1],
                                                    unitPrice: [0],
                                                    voidIndex: [-1],
                                                    voidRemark: [''],
                                                    voidApproveStaff: [undefined],
                                                    type: "M"
                                                });
                                            })
                                            $.each(optionItem.kitchenMsg, function (kitchenMsgIndex, kitchenMsg) {
                                                existingOrder.item.push({
                                                    desc1: [kitchenMsg.label],
                                                    code: [kitchenMsg.code],
                                                    qty: [1],
                                                    unitPrice: [0],
                                                    voidIndex: [-1],
                                                    voidRemark: [''],
                                                    voidApproveStaff: [undefined],
                                                    type: "K"
                                                });
                                            })
                                        }
                                    })
                                });
                            }
                            if (item.subitem != undefined && item.subitem.length != 0) {
                                $.each(item.subitem, function (subitemIndex, subitem) {
                                    existingOrder.item.push({
                                        desc1: [subitem.getName()],
                                        code: [subitem.code],
                                        qty: [subitem.getQty()],
                                        unitPrice: [subitem.getUnitPrice() * 100],
                                        voidIndex: [subitem.voidIndex],
                                        voidRemark: [subitem.voidRemark],
                                        voidApproveStaff: [subitem.voidApproveStaff],
                                        type: "S"
                                    });
                                });
                            }
                            if (item.modifier.length != 0) {
    
                                $.each(item.modifier, function (modifierIndex, modifier) {
                                    existingOrder.item.push({
                                        desc1: [modifier.label],
                                        code: [modifier.code],
                                        qty: [1],
                                        unitPrice: [0],
                                        voidIndex: [-1],
                                        voidRemark: [''],
                                        voidApproveStaff: [undefined],
                                        type: "M"
                                    });
                                })
                            }
                            if (item.kitchenMsg.length != 0) {
    
                                $.each(item.kitchenMsg, function (kitchenMsgIndex, kitchenMsg) {
                                    existingOrder.item.push({
                                        desc1: [kitchenMsg.label],
                                        code: [kitchenMsg.code],
                                        qty: [1],
                                        unitPrice: [0],
                                        voidIndex: [-1],
                                        voidRemark: [''],
                                        voidApproveStaff: [undefined],
                                        type: "K"
                                    });
                                })
                            }
                            */
                        });



                        /** for all message **/
                        if (userConfig.page.additionalMessageAll)
                            angular.forEach(obj.Cart.orderList, function (item) {
                                console.log('item');
                                console.log(item);
                                var itemType = "I"
                                if (item.type) {
                                    if (item.type == "T") {
                                        itemType = "T";
                                    }
                                }


                                if (item.kitchenMsg.length != 0) {

                                    allOrder.item.push({
                                        code: [item.code], qty: [item.qty], unitPrice: [item.getUnitPrice() * 100], voidIndex: [item.voidIndex], voidRemark: [item.voidRemark], type: [itemType], desc1: [item.getName()]
             , customName: [item.customName], voidApproveStaff: [item.voidApproveStaff],
                                    });

                                    if (item.modifier.length != 0) {

                                        $.each(item.modifier, function (modifierIndex, modifier) {
                                            allOrder.item.push({
                                                desc1: [modifier.label],
                                                code: [modifier.code],
                                                qty: [1],
                                                unitPrice: [0],
                                                voidIndex: [-1],
                                                voidRemark: [''],
                                                voidApproveStaff: [undefined],
                                                type: ["M"]
                                            });
                                        })
                                    }
                                    if (item.kitchenMsgOld.length != 0) {

                                        $.each(item.kitchenMsgOld, function (kitchenMsgIndex, kitchenMsg) {
                                            allOrder.item.push({
                                                desc1: [kitchenMsg.label],
                                                code: [kitchenMsg.code],
                                                qty: [1],
                                                unitPrice: [0],
                                                voidIndex: [-1],
                                                voidRemark: [''],
                                                voidApproveStaff: [undefined],
                                                type: ["K"]
                                            });
                                        })
                                    }
                                    if (item.kitchenMsg.length != 0) {

                                        $.each(item.kitchenMsg, function (kitchenMsgIndex, kitchenMsg) {
                                            allOrder.item.push({
                                                desc1: [kitchenMsg.label],
                                                code: [kitchenMsg.code],
                                                qty: [1],
                                                unitPrice: [0],
                                                voidIndex: [-1],
                                                voidRemark: [''],
                                                voidApproveStaff: [undefined],
                                                type: ["K"]
                                            });
                                        })
                                    }

                                    if (item.option != undefined && item.option.length != 0) {
                                        $.each(item.option, function (optionIndex, option) {
                                            $.each(option.items, function (optionItemIndex, optionItem) {
                                                if (optionItem.qty > 0) {
                                                    allOrder.item.push({
                                                        desc1: [optionItem.getName()],
                                                        code: [optionItem.code],
                                                        qty: [optionItem.getQty()],
                                                        unitPrice: [optionItem.getUnitPrice() * 100],
                                                        voidIndex: [optionItem.voidIndex],
                                                        voidRemark: [optionItem.voidRemark],
                                                        voidApproveStaff: [optionItem.voidApproveStaff],
                                                        type: ["O"]
                                                    });
                                                    $.each(optionItem.modifier, function (modifierIndex, modifier) {
                                                        allOrder.item.push({
                                                            desc1: [modifier.label],
                                                            code: [modifier.code],
                                                            qty: [1],
                                                            unitPrice: [0],
                                                            voidIndex: [-1],
                                                            voidRemark: [''],
                                                            voidApproveStaff: [undefined],
                                                            type: ["M"]
                                                        });
                                                    })

                                                    $.each(optionItem.kitchenMsgOld, function (kitchenMsgIndex, kitchenMsg) {
                                                        allOrder.item.push({
                                                            desc1: [kitchenMsg.label],
                                                            code: [kitchenMsg.code],
                                                            qty: [1],
                                                            unitPrice: [0],
                                                            voidIndex: [-1],
                                                            voidRemark: [''],
                                                            voidApproveStaff: [undefined],
                                                            type: ["K"]
                                                        });
                                                    });

                                                    $.each(optionItem.kitchenMsg, function (kitchenMsgIndex, kitchenMsg) {
                                                        allOrder.item.push({
                                                            desc1: [kitchenMsg.label],
                                                            code: [kitchenMsg.code],
                                                            qty: [1],
                                                            unitPrice: [0],
                                                            voidIndex: [-1],
                                                            voidRemark: [''],
                                                            voidApproveStaff: [undefined],
                                                            type: ["K"]
                                                        });
                                                    });




                                                }
                                            })
                                        });
                                    }
                                    if (item.subitem != undefined && item.subitem.length != 0) {
                                        $.each(item.subitem, function (subitemIndex, subitem) {
                                            allOrder.item.push({
                                                desc1: [subitem.getName()],
                                                code: [subitem.code],
                                                qty: [subitem.getQty()],
                                                unitPrice: [subitem.getUnitPrice() * 100],
                                                voidIndex: [subitem.voidIndex],
                                                voidRemark: [subitem.voidRemark],
                                                voidApproveStaff: [subitem.voidApproveStaff],
                                                type: ["S"]
                                            });

                                            $.each(subitem.modifier, function (modifierIndex, modifier) {
                                                allOrder.item.push({
                                                    desc1: [modifier.label],
                                                    code: [modifier.code],
                                                    qty: [1],
                                                    unitPrice: [0],
                                                    voidIndex: [-1],
                                                    voidRemark: [''],
                                                    voidApproveStaff: [undefined],
                                                    type: ["M"]
                                                });
                                            });

                                            $.each(subitem.kitchenMsgOld, function (kitchenMsgIndex, kitchenMsg) {
                                                allOrder.item.push({
                                                    desc1: [kitchenMsg.label],
                                                    code: [kitchenMsg.code],
                                                    qty: [1],
                                                    unitPrice: [0],
                                                    voidIndex: [-1],
                                                    voidRemark: [''],
                                                    voidApproveStaff: [undefined],
                                                    type: ["K"]
                                                });
                                            });

                                            $.each(subitem.kitchenMsg, function (kitchenMsgIndex, kitchenMsg) {
                                                allOrder.item.push({
                                                    desc1: [kitchenMsg.label],
                                                    code: [kitchenMsg.code],
                                                    qty: [1],
                                                    unitPrice: [0],
                                                    voidIndex: [-1],
                                                    voidRemark: [''],
                                                    voidApproveStaff: [undefined],
                                                    type: ["K"]
                                                });
                                            });
                                        });
                                    }
                                }

                            });


                        //if (order.item.length > 0) {
                        var postData = {
                            "action": "editOrder",
                            "user": {
                                "$value": obj.UserManager.staffDetails.staffId
                            },
                            "tableNum": {
                                "$value": obj.Cart.tableNo
                            },
                            "peopleNum": {
                                "$value": obj.Cart.noOfPeople
                            },
                            "existingCart": {
                                "json": {
                                    "order": existingOrder
                                }
                            },
                            "allOrderList": allOrder,
                            "till": userConfig.page.till

                        };
                        if (obj.Cart.member != null) {
                            postData.member = {};
                            postData.member.$value = obj.Cart.member;
                        }
                        console.debug('editOrder emit');
                        console.debug(postData);
                        SocketFactory.emit('editOrder', postData, function (r) {
                            r = JSON.parse(r);
                            console.log(r);
                            if (r.result === "OK") {
                                // obj.mode = obj.schema.mode.floorPlan;
                                $rootScope.$emit('returnToOrder', [])
                            } else {

                            }
                            checkSubmitCart = false;
                        });
                    }
                    else {
                        // new order
                        var order = { item: [] };
                        angular.forEach(obj.Cart.cartList, function (item) {
                            console.log('item');
                            console.log(item);
                            var itemType = "I"
                            if (item.type) {
                                if (item.type == "T") {
                                    itemType = "T";
                                }
                            }

                            order.item.push({
                                code: [item.code], qty: [item.qty], unitPrice: [item.getUnitPrice() * 100], voidIndex: [item.voidIndex], voidRemark: [item.voidRemark], type: [itemType], desc1: [item.getName()]
                                , customName: [item.customName], voidApproveStaff: [item.voidApproveStaff], isFired: item.checkIsFireItem(true)
                            });
                            if (item.modifier.length != 0) {

                                $.each(item.modifier, function (modifierIndex, modifier) {
                                    order.item.push({
                                        desc1: [modifier.label],
                                        code: [modifier.code],
                                        qty: [1],
                                        unitPrice: [0],
                                        voidIndex: [-1],
                                        voidRemark: [''],
                                        voidApproveStaff: [undefined],
                                        type: ["M"]
                                    });
                                })
                            }
                            if (item.kitchenMsg.length != 0) {

                                $.each(item.kitchenMsg, function (kitchenMsgIndex, kitchenMsg) {
                                    order.item.push({
                                        desc1: [kitchenMsg.label],
                                        code: [kitchenMsg.code],
                                        qty: [1],
                                        unitPrice: [0],
                                        voidIndex: [-1],
                                        voidRemark: [''],
                                        voidApproveStaff: [undefined],
                                        type: ["K"]
                                    });
                                })
                            }
                            if (item.option != undefined && item.option.length != 0) {
                                $.each(item.option, function (optionIndex, option) {
                                    $.each(option.items, function (optionItemIndex, optionItem) {
                                        if (optionItem.qty > 0) {
                                            order.item.push({
                                                desc1: [optionItem.getName()],
                                                code: [optionItem.code],
                                                optionGroupCode: [option.optionGroupCode],
                                                qty: [optionItem.getQty()],
                                                unitPrice: [optionItem.getUnitPrice() * 100],
                                                voidIndex: [optionItem.voidIndex],
                                                voidRemark: [optionItem.voidRemark],
                                                voidApproveStaff: [optionItem.voidApproveStaff],
                                                type: ["O"]
                                            });
                                            $.each(optionItem.modifier, function (modifierIndex, modifier) {
                                                order.item.push({
                                                    desc1: [modifier.label],
                                                    code: [modifier.code],
                                                    qty: [1],
                                                    unitPrice: [0],
                                                    voidIndex: [-1],
                                                    voidRemark: [''],
                                                    voidApproveStaff: [undefined],
                                                    type: ["M"]
                                                });
                                            })
                                            $.each(optionItem.kitchenMsg, function (kitchenMsgIndex, kitchenMsg) {
                                                order.item.push({
                                                    desc1: [kitchenMsg.label],
                                                    code: [kitchenMsg.code],
                                                    qty: [1],
                                                    unitPrice: [0],
                                                    voidIndex: [-1],
                                                    voidRemark: [''],
                                                    voidApproveStaff: [undefined],
                                                    type: ["K"]
                                                });
                                            });
                                        }
                                    })
                                });
                            }
                            if (item.subitem != undefined && item.subitem.length != 0) {
                                $.each(item.subitem, function (subitemIndex, subitem) {
                                    order.item.push({
                                        desc1: [subitem.getName()],
                                        code: [subitem.code],
                                        qty: [subitem.getQty()],
                                        unitPrice: [subitem.getUnitPrice() * 100],
                                        voidIndex: [subitem.voidIndex],
                                        voidRemark: [subitem.voidRemark],
                                        voidApproveStaff: [subitem.voidApproveStaff],
                                        type: ["S"]
                                    });

                                    $.each(subitem.modifier, function (modifierIndex, modifier) {
                                        order.item.push({
                                            desc1: [modifier.label],
                                            code: [modifier.code],
                                            qty: [1],
                                            unitPrice: [0],
                                            voidIndex: [-1],
                                            voidRemark: [''],
                                            voidApproveStaff: [undefined],
                                            type: ["M"]
                                        });
                                    });

                                    $.each(subitem.kitchenMsg, function (kitchenMsgIndex, kitchenMsg) {
                                        order.item.push({
                                            desc1: [kitchenMsg.label],
                                            code: [kitchenMsg.code],
                                            qty: [1],
                                            unitPrice: [0],
                                            voidIndex: [-1],
                                            voidRemark: [''],
                                            voidApproveStaff: [undefined],
                                            type: ["K"]
                                        });
                                    });
                                });
                            }
                        });

                        // unselect option
                        angular.forEach(obj.Cart.orderList, function (item) {
                            if (!item.hasUnselectOption || item.voidIndex != -1) return;

                            if (item.option != undefined && item.option.length != 0) {
                                $.each(item.option, function (optionIndex, option) {
                                    $.each(option.items, function (optionItemIndex, optionItem) {
                                        if (optionItem.code == obj.schema.unselectOption.code) return;
                                        if (option.index == undefined) return;
                                        if (optionItem.qty > 0) {
                                            order.item.push({
                                                desc1: [optionItem.getName()],
                                                index: [option.index],
                                                code: [optionItem.code],
                                                optionGroupCode: [option.optionGroupCode],
                                                qty: [optionItem.getQty()],
                                                unitPrice: [optionItem.getUnitPrice() * 100],
                                                voidIndex: [optionItem.voidIndex],
                                                voidRemark: [optionItem.voidRemark],
                                                voidApproveStaff: [optionItem.voidApproveStaff],
                                                type: ["O"]
                                            });
                                        }
                                    })
                                });
                            }
                        });
                        console.log('submit order check 1', order);
                        //void combo will be error;
                        //if (order.item.length > 0) {
                        var postData = {
                            "action": "MDSaveTableOrder",
                            "user": {
                                "$value": obj.UserManager.staffDetails.staffId
                            },
                            "tableNum": {
                                "$value": obj.Cart.tableNo
                            },
                            "peopleNum": {
                                "$value": obj.Cart.noOfPeople
                            },
                            "itemData": {
                                "json": {
                                    "order": order
                                }
                            }
                        };
                        if (obj.Cart.member != null) {
                            postData.member = {};
                            postData.member.$value = obj.Cart.member;
                        }
                        console.log('SaveTableOrder', order);

                        postData.till = userConfig.page.till;

                        postData.tableKey = {};
                        //postData.tableKey.$value = "12345"
                        //console.log(7821, 'postData', postData);
                        //return;
                        SocketFactory.emit('saveTableOrder', postData, function (r) {
                            r = JSON.parse(r);
                            console.log(r);
                            if (r.result === "OK") {
                                //obj.mode = obj.schema.mode.floorPlan;
                                //$rootScope.$emit('saveOrder', []);

                                $rootScope.$emit('saveOrder', []);

                            } else {
                                //alert(1);
                            }

                            checkSubmitCart = false;
                        });
                    }
                }
            }

        };



        var LocksubmitSplitTable = false;
        obj.submitSplitTable = function () {
            //if (!LocksubmitSplitTable) {
            //    LocksubmitSplitTable = true;
            //    $timeout(function () {
            //        LocksubmitSplitTable = false;
            //    }, 3000)
            //}
            //else {
            //    return;
            //}
            var orders = [];

            for (var i = 0; i < obj.Cart.allSplitList.length; i++) {
                var voidItemIndex = [];
                obj.Cart.allSplitList[i].item.forEach(function (item, index) {
                    if (item.voidItemList != undefined && item.voidIndex == -1) {
                        voidItemIndex.push(index)
                    }
                });
                for (var a = 0; a < voidItemIndex.length; a++) {
                    obj.Cart.allSplitList[i].item[voidItemIndex[a]].voidItemList.forEach(function (voidItem) {
                        obj.Cart.allSplitList[i].item[voidItemIndex[a]].voidQty += (-1 * voidItem.qty);
                        obj.Cart.allSplitList[i].item.push(voidItem);
                    });
                }
            }

            $.each(obj.Cart.allSplitList, function (idx, list) {
                var order = {
                    'nop': obj.Cart.allSplitList[idx].nop,
                    'item': [],
                    'suffix': obj.Cart.allSplitList[idx].suffix,
                    'status': obj.Cart.allSplitList[idx].status,
                    'cardId': obj.Cart.allSplitList[idx].cardId
                };

                //obj.Cart.allSplitList[idx].item.forEach(function (voidItem) {
                //    if (voidItem.voidIndex != -1) {
                //        var templItem = $.grep(obj.Cart.allSplitList[idx].item, function (item) {
                //            return item.index == voidItem.voidIndex;
                //        });
                //        if (templItem.length != 0) {
                //            if (templItem[0].voidList == undefined) templItem[0].voidList = [];
                //            templItem[0].voidList.push(voidItem);
                //            templItem[0].voidQty += (-1 * voidItem.qty);
                //        }
                //    }
                //});

                $.each(obj.Cart.allSplitList[idx].item, function (index, item) {
                    //if (item.qty <= 0) return true;
                    //console.debug(item);
                    order.item.push({
                        code: [item.code],
                        qty: [item.qty],
                        unitPrice: [item.getUnitPrice() * 100],
                        voidIndex: [item.voidIndex],
                        voidRemark: [item.voidRemark],
                        type: ['I'],
                        desc1: [item.getName()],
                        index: [item.index],
                        orgIndex: [item.orgIndex],
                        serviceCharge: [item.serviceCharge],
                        printer: [item.printer]
                    });

                    if (item.option != undefined && item.option.length != 0) {
                        $.each(item.option, function (optionIndex, option) {
                            $.each(option.items, function (optionItemIndex, optionItem) {
                                if (optionItem.qty > 0) {
                                    order.item.push({
                                        index: [optionItem.index],
                                        orgIndex: [optionItem.orgIndex],
                                        desc1: [optionItem.getName()],
                                        code: [optionItem.code],
                                        qty: [optionItem.getQty()],
                                        unitPrice: [optionItem.getUnitPrice() * 100],
                                        voidIndex: [optionItem.voidIndex],
                                        voidRemark: [optionItem.voidRemark],
                                        voidApproveStaff: [optionItem.voidApproveStaff],
                                        type: "O",
                                        serviceCharge: [optionItem.serviceCharge],
                                        printer: [optionItem.printer]
                                    });

                                    $.each(optionItem.modifier, function (modifierIndex, modifier) {
                                        order.item.push({
                                            index: [modifier.index],
                                            orgIndex: [modifier.orgIndex],
                                            desc1: [modifier.label],
                                            code: [modifier.code],
                                            qty: [1],
                                            unitPrice: [0],
                                            voidIndex: [-1],
                                            voidRemark: [''],
                                            voidApproveStaff: [undefined],
                                            type: "M",
                                            serviceCharge: [optionItem.serviceCharge],
                                            printer: [optionItem.printer]
                                        });
                                    });

                                    // console.log('optionItem.kitchenMsg ', optionItem.kitchenMsg);
                                    console.log('optionItem.kitchenMsgOld ', optionItem.kitchenMsgOld);
                                    console.log('optionItem ', optionItem);
                                    // this is empty since in split item list, kitchenMsg is not new, it is put in optionItem.kitchenMsgOld instead
                                    // $.each(optionItem.kitchenMsg, function (kitchenMsgIndex, kitchenMsg) {
                                    //     order.item.push({
                                    //         index: [kitchenMsg.index],
                                    //         orgIndex: [kitchenMsg.orgIndex],
                                    //         desc1: [kitchenMsg.label],
                                    //         code: [kitchenMsg.code],
                                    //         qty: [1],
                                    //         unitPrice: [0],
                                    //         voidIndex: [-1],
                                    //         voidRemark: [''],
                                    //         voidApproveStaff: [undefined],
                                    //         type: "K"
                                    //     });
                                    // });
                                    $.each(optionItem.kitchenMsgOld, function (kitchenMsgIndex, kitchenMsg) {
                                        order.item.push({
                                            index: [kitchenMsg.index],
                                            orgIndex: [kitchenMsg.orgIndex],
                                            desc1: [kitchenMsg.label],
                                            code: [kitchenMsg.code],
                                            qty: [1],
                                            unitPrice: [0],
                                            voidIndex: [-1],
                                            voidRemark: [''],
                                            voidApproveStaff: [undefined],
                                            type: "K",
                                            serviceCharge: [optionItem.serviceCharge],
                                            printer: [optionItem.printer]
                                        });
                                    });
                                }
                            })
                        });
                    }

                    if (item.subitem != undefined && item.subitem.length != 0) {
                        $.each(item.subitem, function (subitemIndex, subitem) {
                            order.item.push({
                                index: [subitem.index],
                                orgIndex: [subitem.orgIndex],
                                desc1: [subitem.getName()],
                                code: [subitem.code],
                                qty: [subitem.getQty()],
                                unitPrice: [subitem.getUnitPrice() * 100],
                                voidIndex: [subitem.voidIndex],
                                voidRemark: [subitem.voidRemark],
                                voidApproveStaff: [subitem.voidApproveStaff],
                                type: "S",
                                serviceCharge: [subitem.serviceCharge],
                                printer: [subitem.printer]
                            });

                            $.each(subitem.modifier, function (modifierIndex, modifier) {
                                order.item.push({
                                    index: [modifier.index],
                                    orgIndex: [modifier.orgIndex],
                                    desc1: [modifier.label],
                                    code: [modifier.code],
                                    qty: [1],
                                    unitPrice: [0],
                                    voidIndex: [-1],
                                    voidRemark: [''],
                                    voidApproveStaff: [undefined],
                                    type: "M",
                                    serviceCharge: [subitem.serviceCharge],
                                    printer: [subitem.printer]
                                });
                            });

                            $.each(subitem.kitchenMsgOld, function (kitchenMsgIndex, kitchenMsg) {
                                order.item.push({
                                    index: [kitchenMsg.index],
                                    orgIndex: [kitchenMsg.orgIndex],
                                    desc1: [kitchenMsg.label],
                                    code: [kitchenMsg.code],
                                    qty: [1],
                                    unitPrice: [0],
                                    voidIndex: [-1],
                                    voidRemark: [''],
                                    voidApproveStaff: [undefined],
                                    type: "K",
                                    serviceCharge: [subitem.serviceCharge],
                                    printer: [subitem.printer]
                                });
                            });
                        });
                    }

                    if (item.modifier.length != 0) {

                        $.each(item.modifier, function (modifierIndex, modifier) {
                            order.item.push({
                                index: [modifier.index],
                                orgIndex: [modifier.orgIndex],
                                desc1: [modifier.label],
                                code: [modifier.code],
                                qty: [1],
                                unitPrice: [0],
                                voidIndex: [-1],
                                voidRemark: [''],
                                voidApproveStaff: [undefined],
                                type: "M",
                                serviceCharge: [item.serviceCharge],
                                printer: [item.printer]
                            });
                        })
                    }

                    if (item.kitchenMsg.length != 0) {

                        $.each(item.kitchenMsg, function (kitchenMsgIndex, kitchenMsg) {
                            order.item.push({
                                index: [kitchenMsg.index],
                                orgIndex: [kitchenMsg.orgIndex],
                                desc1: [kitchenMsg.label],
                                code: [kitchenMsg.code],
                                qty: [1],
                                unitPrice: [0],
                                voidIndex: [-1],
                                voidRemark: [''],
                                voidApproveStaff: [undefined],
                                type: "K",
                                serviceCharge: [item.serviceCharge],
                                printer: [item.printer]
                            });
                        })
                    }
                });
                orders.push(order);
                // console.log(order);
                // return // break for test
            });
            //if (order.item.length > 0) {
            var postData = {
                "action": "MDSaveTableOrder",
                "user": {
                    "$value": obj.UserManager.staffDetails.staffId
                },
                "tableNum": {
                    "$value": obj.Cart.tableNo
                },
                "peopleNum": {
                    "$value": obj.Cart.noOfPeople
                },
                "itemData": {
                    "json": {
                        "order": orders
                    }
                }
            };

            console.debug('8060', postData);
            //return;

            // return; // for test
            SocketFactory.emit('splitTableOrder', postData, function (r) {
                obj.lockTable.toReleaseLockTable(obj.lockTable.tableNum);
                r = JSON.parse(r);
                console.log(r.result);
                if (r.result === "OK") {
                    obj.mode = obj.schema.mode.floorPlan;
                }
            });
        }

        // change current display split list
        obj.changeSplitList = function (index, status) {
            if (status == 4) {
                return;
            }
            console.log('obj.Cart.splitListLIndex ' + obj.Cart.splitListLIndex);
            console.log('obj.Cart.splitListRIndex ' + obj.Cart.splitListRIndex);
            console.log('obj.Cart.currentSelectedSplitIndex ' + obj.Cart.currentSelectedSplitIndex);
            if (index == obj.Cart.splitListLIndex || index == obj.Cart.splitListRIndex) {

                return;
            }

            //obj.Cart.splitListLIndex = 0;
            if (!(obj.Cart.currentSelectedSplitIndex === index)) {
                //console.log('!(obj.Cart.currentSelectedSplitIndex === index)');
                var targetIndex = obj.Cart.currentSelectedSplitIndex == obj.Cart.splitListLIndex ? obj.Cart.splitListRIndex : obj.Cart.splitListLIndex;

                if (obj.Cart.currentSelectedSplitIndex != obj.Cart.splitListLIndex) {
                    obj.Cart.splitListRIndex = index;
                    obj.Cart.splitListRNoofpeople = obj.Cart.allSplitList[index].nop;
                } else {
                    obj.Cart.splitListLIndex = index;
                    obj.Cart.splitListLNoofpeople = obj.Cart.allSplitList[index].nop;
                }
                //obj.Cart.splitListRIndex = index;
                //obj.Cart.splitListRNoofpeople = obj.Cart.allSplitList[index].nop;

            }



        }

        obj.notServiceChargeInOtherBill = function () {

            var checkOtherService = false;
            if (!obj.getOtherWriteOrderService()) checkOtherService = false;
            else {
                checkOtherService = true;
                if (obj.getOtherWriteOrderService().modeOtherWriteOrderService == obj.schema.modeOtherWriteOrderService.deliveryOrder) checkOtherService = false;
            }

            return ((obj.isMode('takeAway') || obj.isMode('fastFood')) || checkOtherService);
        }

        obj.calcPrice = function (cartObjs, couponObj) {
            //console.log('obj.calcPrice');
            //console.log(cartObjs);
            console.log(cartObjs);
            // console.log(arguments.callee.caller);
            var price = 0;
            var priceWithoutDiscount = 0;
            var priceForSC = 0;
            for (var i = 0; i < cartObjs.length; i++) {
                var cartObj = cartObjs[i];


                for (var j = 0; j < cartObj.length; j++) {
                    if (obj.notServiceChargeInOtherBill()) {
                        cartObj[j].serviceCharge = false;
                    }

                    price += cartObj[j].getTotalPrice();
                    priceForSC += cartObj[j].getPriceForSC();
                }
            }

            //console.log(price);
            //console.log('****************1');
            //console.log(couponObj);
            priceWithoutDiscount = price;
            var serviceCharge = (priceForSC / 10).toFixed(1);

            console.log('ModeDeliveryOrder', obj.modeDeliveryOrder);
            /*if (obj.isModeDeliveryOrder('writeOrder') || obj.isMode('deliveryOrder'))
                serviceCharge = (priceForSC * (obj.deliveryOrderService.discount / 100)).toFixed(1);*/

            //
            if (!obj.checkIsNotTakeAwayMode()) {

                if (obj.isModeDeliveryOrder('writeOrder') || obj.getOtherWriteOrderService().modeSetting.mode.value == 'deliveryOrder') {
                    serviceCharge = (priceForSC * (userConfig.delivery_charge / 100)).toFixed(1);
                }
            }


            if (couponObj) {
                var tmplCouponObj = [];
                angular.extend(tmplCouponObj, couponObj);
                function getIndexOf(discType) {
                    if (userConfig.discountPiority.indexOf(discType) == -1) return userConfig.discountPiority.length;
                    else return userConfig.discountPiority.indexOf(discType);
                }
                tmplCouponObj.sort((a, b) => {
                    return (getIndexOf(a.type) - getIndexOf(b.type));
                });

                console.debug(8870, tmplCouponObj);
                //alert(couponObj)
                for (var i = 0; i < tmplCouponObj.length; i++) {
                    var cobj = tmplCouponObj[i];
                    if (cobj.type == "percentage") {
                        cobj.price = price * (1 - cobj.amount)
                        //price = parseFloat(parseFloat(price) * cobj.amount)
                        price = price - Number(parseFloat(cobj.price).toFixed(1));
                    } else if (cobj.type == "exact") {
                        cobj.price = cobj.amount;
                        if (price + parseFloat(serviceCharge) < cobj.amount) {
                            cobj.price = price + parseFloat(serviceCharge);
                        }
                        price = parseFloat(parseFloat(price) - cobj.amount)
                    } else if (cobj.type == "serviceCharge") {
                        cobj.price = serviceCharge;
                        price = parseFloat(parseFloat(price) - cobj.price)
                    }
                    //else if (cobj.type == "amount") {
                    //    price = parseFloat(parseFloat(price) - cobj.price)
                    //}
                }
            }

            //console.log(price);
            //console.log(serviceCharge);
            //console.log('****************2');

            var priceWithServiceCharge = price + parseFloat(serviceCharge);
            if (priceWithServiceCharge < 0) {
                priceWithServiceCharge = 0;
            }
            var r = (priceWithServiceCharge % 1).toFixed(1);
            if (r > 0.4) {
                r = (1 - r).toFixed(1);
            } else if (r != 0) {
                r = '-' + r;
            }
            obj.remainder();

            //console.log(price);
            //console.log('****************');
            //console.log(priceWithServiceCharge);
            //console.log(r);
            return { serviceCharge: serviceCharge, remainings: r, totalPrice: Math.round(priceWithServiceCharge), price: priceWithoutDiscount };
        }

        obj.removeSplitList = function (ev) {
            if (obj.Cart.allSplitList.length < 3) {
                obj.showAlert('group_reminder');
            } else {
                var lastgroup = obj.Cart.allSplitList[obj.Cart.allSplitList.length - 1];
                if (lastgroup.item.length != 0) {
                    var confirm = $mdDialog.confirm()
                   .parent(angular.element(document.body))
                   .title(uiLanguage[INFO.lang]['alert'])
                   .content(uiLanguage[INFO.lang]['group_delete_confirmation'])
                   // .content('Are you sure to delete the groups?')
                   .ok(uiLanguage[INFO.lang]['yes'])
                   .cancel(uiLanguage[INFO.lang]['cancel'])
                   .targetEvent(ev);
                    $mdDialog.show(confirm).then(function () {
                        //alert('You decided to get rid of your debt.');
                        //if()

                    }, function () {
                        //alert('You decided to keep your debt.');
                    });
                } else {
                    if (obj.Cart.splitListLIndex == obj.Cart.allSplitList.length - 1) {
                        if (obj.Cart.splitListRIndex == obj.Cart.allSplitList.length - 2) {
                            obj.Cart.splitListLIndex = obj.Cart.allSplitList.length - 3;
                        } else {
                            obj.Cart.splitListLIndex = obj.Cart.allSplitList.length - 2;
                        }
                    } else if (obj.Cart.splitListRIndex == obj.Cart.allSplitList.length - 1) {
                        if (obj.Cart.splitListLIndex == obj.Cart.allSplitList.length - 2) {
                            obj.Cart.splitListRIndex = obj.Cart.allSplitList.length - 3;
                        } else {
                            obj.Cart.splitListRIndex = obj.Cart.allSplitList.length - 2;
                        }
                    }
                    obj.Cart.allSplitList.pop();
                }
            }
        }

        obj.addSplitList = function () {
            var targetIndex = obj.Cart.currentSelectedSplitIndex == obj.Cart.splitListLIndex ? obj.Cart.splitListRIndex : obj.Cart.splitListLIndex;

            // always get the highest number of name, since table maybe transferred after split, inter-link maybe broken
            var allSuffix = [];
            console.log(obj.Cart.allSplitList);
            for (var i = 0; i < obj.Cart.allSplitList.length; i++) {
                allSuffix.push(obj.Cart.allSplitList[i].suffix);
            }
            allSuffix.sort();
            var maxPos = -1;
            charArray.forEach(function (charC) {
                allSuffix.forEach(function (charS) {
                    if (charC == charS) {
                        if (charArray.indexOf(charC) > maxPos)
                            maxPos = charArray.indexOf(charC);
                    }
                })
            })

            var lasttableno = obj.Cart.allSplitList[obj.Cart.allSplitList.length - 1].name;
            // console.log('lasttableno ' + lasttableno);
            if (lasttableno.indexOf('_') != -1) {
                lasttableno = lasttableno.slice(0, -1);
            } else {
                lasttableno = lasttableno + '_';
            }

            // lasttableno += charArray[obj.Cart.allSplitList.length];
            // console.log('lasttableno ' + lasttableno);
            lasttableno += charArray[maxPos + 1];

            obj.Cart.allSplitList[obj.Cart.allSplitList.length] = { 'name': lasttableno, 'nop': 1, 'item': [], 'onop': -1, suffix: charArray[maxPos + 1] };

            for (var i = 0; i < obj.Cart.allSplitList.length; i++) {
                if (obj.Cart.allSplitList[i].nop > 1) {
                    obj.Cart.allSplitList[i].nop = obj.Cart.allSplitList[i].nop - 1;
                    if (i == obj.Cart.splitListLIndex) {
                        obj.Cart.splitListLNoofpeople = obj.Cart.allSplitList[i].nop;
                    } else if (i == obj.Cart.splitListLIndex) {
                        obj.Cart.splitListRNoofpeople = obj.Cart.allSplitList[i].nop;
                    }
                    break;
                }
            }

            if (obj.Cart.currentSelectedSplitIndex == obj.Cart.splitListLIndex) {
                obj.Cart.splitListRIndex = obj.Cart.allSplitList.length - 1;
                obj.Cart.splitListRNoofpeople = 1;
            } else {
                obj.Cart.splitListLIndex = obj.Cart.allSplitList.length - 1;
                obj.Cart.splitListLNoofpeople = 1;
            }
        }

        obj.splitTableNo = function (tableNo) {
            console.log('splitTableNo');

            if (!obj.UserManager.isLogin) {
                obj.UserManager.LoginRedirectToTable = tableNo;
                if (angular.element('md-dialog[aria-label="Login"]').scope() == undefined) {
                    $mdDialog.show({
                        templateUrl: 'tmpl/login.html'
                    })
                } else {
                    $mdDialog.hide();
                }
            } else {
                SocketFactory.emit('adminLoadTableOrder', { 'tableNum': { '$value': tableNo }, "tableMode": "splitTable" }, function (r) {
                    MainService.pleaseWait.off();
                    console.log('adminLoadTableOrder r' + r)
                    r = JSON.parse(r);
                    console.log(r)

                    if (r.result === "Locked") {
                        //$.each($('.table'), function () {
                        //    console.debug($(this).find('text').text());
                        //})
                        if (r.lockedTableNo == undefined) {
                            var templObj = $.grep($('.table'), function (item) {
                                return $(item).find('text').text() == tableNo;
                            })

                            $(templObj[0]).attr('class', $(templObj[0]).attr('class') + " Lock ");
                            //alert lock table

                            obj.showAlert(tableNo + uiLanguage[INFO.lang]['txt_lock_table']);
                        } else
                            obj.showAlert(r.lockedTableNo + uiLanguage[INFO.lang]['txt_lock_table']);
                        //alert();
                        //callback();
                        //return;
                    }

                    if (r.result === "OK") {
                        if (r.order[0].header.orderId == '') {
                            obj.showAlert('no_order');
                        } else {

                            if (obj.lockTable.tableNum != tableNo) obj.lockTable.tableNum = tableNo;

                            var allSuffix = [];
                            var maxPos = -1;
                            var suffix = '';

                            console.log("r.order[0].header.tableNum.indexOf('_') " + r.order[0].header.tableNum.indexOf('_'));
                            console.log('r.order.length ' + r.order.length);
                            if (r.order.length > 1 || r.order[0].header.tableNum.indexOf('_') != -1) {
                                for (var i = 0; i < r.order.length; i++) {
                                    console.log('===============================================');
                                    suffix = r.order[0].header.tableNum[r.order[0].header.tableNum.indexOf('_') + 1];
                                    allSuffix.push(suffix);
                                }
                                allSuffix.sort();

                                charArray.forEach(function (charC) {
                                    allSuffix.forEach(function (charS) {
                                        if (charC == charS) {
                                            if (charArray.indexOf(charC) > maxPos)
                                                maxPos = charArray.indexOf(charC);
                                        }
                                    })
                                })
                            }
                            console.log(allSuffix);
                            console.log('maxPos ' + maxPos);


                            // '74_B'['74_B'.indexOf('_')+1], tableNum
                            // if( r.order.length < 2 )
                            obj.Cart.orderList = [];
                            obj.Cart.splitListL = { 'name': '', 'nop': 0, 'item': [] };
                            obj.Cart.splitListR = { 'name': '', 'nop': 0, 'item': [] };
                            obj.Cart.splitListLIndex = 0;
                            obj.Cart.splitListRIndex = 1;
                            obj.Cart.currentSelectedSplitIndex = obj.Cart.splitListLIndex;
                            obj.Cart.allSplitList = [];
                            obj.Cart.billedSplitList = [];

                            // obj.Cart.allSplitList[0] = { 'name':  r.order[0].header.tableNum, 'nop': 0, 'item': [], 'onop': -1, suffix: maxPos < 0 ? r.order[0].header.tableNum + '_' + charArray[0] : r.order[0].header.tableNum[r.order[0].header.tableNum.indexOf('_')+1] };

                            // obj.Cart.allSplitList[1] = { 'name':  r.order[1].header.tableNum, 'nop': 1, 'item': [], 'onop': -1, suffix: maxPos < 1 ? r.order[0].header.tableNum + '_' + charArray[1] : r.order[1].header.tableNum[r.order[1].header.tableNum.indexOf('_')+1] };

                            obj.mode = obj.schema.mode.order;
                            obj.modeOrder = obj.schema.modeOrder.split;
                            obj.modeMultiSelectItem = true;

                            obj.Cart.tableNo = tableNo;
                            var count = 0;
                            var countBilled = 0;
                            var suffix = '';
                            for (var i = 0; i < r.order.length; i++) {
                                var order = r.order[i];
                                if (order.header.status != 4) {
                                    // r.order.length < 2 ? tableNo + '_' + charArray[i]
                                    // if( angular.isUndefined( obj.Cart.allSplitList[count] ) )
                                    // obj.Cart.allSplitList[count] = { 'name': r.order[i].header.tableNum, 'nop': i==0?0:1, 'item': [], 'onop': -1, status: order.header.status, suffix: r.order[i].header.tableNum[r.order[i].header.tableNum.indexOf('_')+1] };
                                    // 'name': r.order[i].header.tableNum, 
                                    obj.Cart.allSplitList[count] = {
                                        'nop': i == 0 ? 0 : 1,
                                        'item': [], 'onop': -1,
                                        status: order.header.status,
                                        cardId: order.header.cardId
                                    };

                                    // first table, one order and maybe or may not have other transferred table
                                    // console.log(i);
                                    // console.log('r.order.length ' + r.order.length);
                                    // console.log('maxPos ' + maxPos);

                                    // regard as newly split table
                                    if (i == 0 && r.order.length === 1 && maxPos === -1) {
                                        // console.log('allSplitList / first ');
                                        suffix = charArray[0];
                                        obj.Cart.allSplitList[count].suffix = suffix;
                                        obj.Cart.allSplitList[count].name = r.order[i].header.tableNum + '_' + suffix;
                                    } else {
                                        // console.log('allSplitList / 2nd ');
                                        suffix = r.order[i].header.tableNum[r.order[i].header.tableNum.indexOf('_') + 1];
                                        obj.Cart.allSplitList[count].suffix = suffix;
                                        obj.Cart.allSplitList[count].name = r.order[i].header.tableNum;
                                    }
                                    // console.log('charArray[0]', charArray[0]);
                                    // console.log(r.order[i].header.tableNum + '_' + suffix);


                                    // if( i <= 1 ) {
                                    //     if( maxPos < 0 ) {

                                    //     } else if ( maxPos < 1 ) {
                                    //         obj.Cart.allSplitList[count].suffix = r.order[i].header.tableNum + '_' + charArray[1];
                                    //     } else {
                                    //         obj.Cart.allSplitList[count].suffix = r.order[i].header.tableNum[r.order[i].header.tableNum.indexOf('_')+1];
                                    //     }
                                    // } else {
                                    //     obj.Cart.allSplitList[count].suffix = r.order[i].header.tableNum[r.order[i].header.tableNum.indexOf('_')+1];
                                    // }

                                    obj.Cart.allSplitList[count].nop = order.header.peopleNum;
                                    obj.Cart.allSplitList[count].onop = order.header.peopleNum;
                                    var itemlist = obj.Cart.allSplitList[count].item;

                                    if (count == 0) {
                                        obj.Cart.splitListLNoofpeople = order.header.peopleNum;
                                    } else if (count == 1) {
                                        obj.Cart.splitListRNoofpeople = order.header.peopleNum;
                                    }

                                    obj.assign2OrderList(order, itemlist);
                                    count++;
                                } else {
                                    obj.Cart.billedSplitList[i] = {
                                        'name': r.order.length < 2 ? tableNo + '_' + charArray[i] : r.order[i].header.tableNum,
                                        'nop': i == 0 ? 0 : 1,
                                        'item': [], 'onop': -1,
                                        status: order.header.status,
                                        suffix: r.order[i].header.tableNum[r.order[i].header.tableNum.indexOf('_') + 1]
                                    };
                                    obj.Cart.billedSplitList[i].nop = order.header.peopleNum;
                                    obj.Cart.billedSplitList[i].onop = order.header.peopleNum;
                                    var itemlist = obj.Cart.billedSplitList[i].item;
                                }
                            }




                            console.log('maxPos ', maxPos);
                            console.log('r.order.length ', r.order.length);
                            //&& maxPos === -1

                            if (r.order.length === 1) {
                                console.log('r.order.length === 1 && maxPos === -1');
                                obj.Cart.allSplitList[1] = {
                                    name: tableNo + '_' + charArray[charArray.indexOf(obj.Cart.allSplitList[0].suffix) + 1],
                                    'nop': i == 0 ? 0 : 1,
                                    'item': [],
                                    'onop': -1, status: 0,
                                    suffix: charArray[charArray.indexOf(obj.Cart.allSplitList[0].suffix) + 1]
                                };

                                //if (obj.Cart.allSplitList[0].suffix == "B") {
                                //    obj.Cart.allSplitList[1].suffix = "C";
                                //    obj.Cart.allSplitList[1].name = tableNo + '_' + charArray[2];
                                //}
                            }




                        }
                        obj.splitListUpdatePrice();
                        if (MainService.Cart.allSplitList[0] == undefined) return;
                        MainService.Cart.billedSplitList = MainService.Cart.billedSplitList.filter(function (order) { return order != undefined })
                        console.log(MainService.Cart.billedSplitList);
                        console.log(MainService.Cart.allSplitList);

                        if (r.order.length == 2) {
                            if (r.order[0].header.status == 4 || r.order[1].header.status == 4) {
                                obj.Cart.splitListLIndex = 0;
                                obj.Cart.splitListRIndex = 1;
                                obj.Cart.currentSelectedSplitIndex = 1;

                                obj.Cart.allSplitList[1] = {
                                    name: tableNo + '_' + charArray[2],
                                    'nop': i == 0 ? 0 : 1,
                                    'item': [],
                                    'onop': -1, status: 0,
                                    suffix: charArray[2]
                                };

                                obj.splitListUpdatePrice();
                            }


                        }

                        MainService.Cart.allSplitList = MainService.Cart.allSplitList.concat(MainService.Cart.billedSplitList);

                        //console.log('------------------------------');
                        //console.log(MainService.Cart.allSplitList);

                        MainService.Cart.allSplitList.forEach(function (subTable) {
                            var voidItemList = [];
                            subTable.item.forEach(function (objItem, index) {
                                if (objItem.voidIndex != -1) {
                                    voidItemList.push(objItem);
                                    //subTable.item.splice(index, 1);
                                }
                            });

                            voidItemList.forEach(function (objVoidItem) {
                                var templ = $.grep(subTable.item, function (objItem) {
                                    return objItem.index == objVoidItem.voidIndex;
                                });
                                if (templ.length != 0) {
                                    if (templ[0].voidItemList == undefined) templ[0].voidItemList = [];
                                    templ[0].voidItemList.push(objVoidItem);


                                    if (templ[0].voidItemNotTransferQty == undefined) templ[0].voidItemNotTransferQty = 0;
                                    templ[0].voidItemNotTransferQty += objVoidItem.qty;


                                }
                            });

                            subTable.item = subTable.item.filter(function (item) {
                                return item.voidIndex == -1;
                            });

                        })


                    }
                });
            }
            // console.log('obj.mode ' + obj.mode);
        };

        obj.loadTableOrder = function (tableNo, tableMode) {
            obj.assignTableNo(tableNo, null, tableMode);
            /*
            SocketFactory.emit('adminLoadTableOrder', { 'tableNum': { '$value': tableNo } }, function (r) {
                console.log('adminLoadTableOrder r' + r)
                //alert(r)
                r = JSON.parse(r);
                console.log(r)
                if (r.result === "OK") {
                    if (r.order.length > 1) {
                        //alert(callback);
                        callback(r.order);
                    } else {
                        var order = r.order[0];
                        console.log(order);
                        obj.modeMultiSelectItem = false;
                        obj.modeOrder = obj.schema.modeOrder[tableMode];
                        obj.mode = obj.schema.mode.order;
                        // obj.modeOrder = obj.schema.modeOrder.normal;
                        console.log(obj.mode);
                        obj.Cart.tableNo = tableNo;
                        obj.Cart.noOfPeople = '';
                        obj.Cart.member = {};
                        //console.log(angular.element('input-keypad').scope());
                        obj.Cart.cartList = [];
                        obj.Cart.orderList = [];
                        obj.Cart.couponObj = [];
                        obj.appliedDiscount = [];
                        obj.appliedPaymentMethod = [];
                        obj.Cart.noOfPeople = order.header.peopleNum;
                        if (order.header.member != undefined) {
                            obj.Cart.member = $.extend(true, order.header.member, obj.schema.member);
                        }
                        angular.forEach(order.item, function (v, k) {
                            console.log(v);
                            //this.push(key + ': ' + value);
                            var item = $.extend(true, {}, obj.schema.baseItem, v);
                            if (item.type === "I") {
                                item.init();
                                console.log('v.voidIndex:' + v.voidIndex);
                                if (v.voidIndex != undefined) {
                                    if (v.voidIndex != -1) {
                                        //obj.Cart.orderList[v.voidIndex].voidQty = v.qty;
                                        $.each(obj.Cart.orderList, function (idx, vitem) {
                                            if (vitem.index == v.voidIndex) {
                                                vitem.voidQty += v.qty;
                                            }
                                        });
                                    }
                                    item.voidRemark = v.voidRemark;
                                }
                                obj.Cart.orderList.push(item);
                            } else if (item.type === "S") {
                                if (obj.Cart.orderList[obj.Cart.orderList.length - 1].option == undefined)
                                    obj.Cart.orderList[obj.Cart.orderList.length - 1].option = [{ items: [] }];
                                obj.Cart.orderList[obj.Cart.orderList.length - 1].option[0].items.push(item);
                            } else if (item.type === "M") {
                                var m = obj.food.modifier.filter(function (matchItem) {
                                    return matchItem.code == item.code;
                                });
                                item.modifier.push(m[0]);
                            }
                        });

                        for (var i = 0; i < order.discount.length; i++) {
                            obj.appliedDiscount.push({ 'coupon_alias': order.discount[i].discountCode, 'amount': order.discount[i].discountAmount, 'type': order.discount[i].discountType, 'price': order.discount[i].discountValue });
                            obj.Cart.couponObj = [{ 'coupon_alias': order.discount[i].discountCode, 'amount': order.discount[i].discountAmount, 'type': order.discount[i].discountType }];
                        }

                        console.log(order);
                        if (order.payment){
                            for (var i = 0; i < order.payment.length; i++) {
                                obj.appliedPaymentMethod.push({ 'method': order.payment[i].paymentType, 'amount': order.payment[i].paymentValue })
                            }
                        }

                        // print order if it is initiated by print order
                        if( obj.modeFloorPlan === obj.schema.modeFloorPlan.printBill ) {
                            console.log('please print bill');
                            // controlled by controller that show the discount page
                        }
                    }
                }
            });
            */
        }

        // create order list object
        obj.assign2OrderList = function (order, itemList) {
            console.log('order itemlist', order, itemList);
            var lastInsertType = "",
                lastInsertIdx = 0,
                lastInsertOptIdx = -1;
            lastInsertSubitemIdx = -1;
            obj.loadedOrder = order;


            angular.forEach(order.item, function (v, k) {
                var item = $.extend(true, {}, obj.schema.baseItem, v);
                //var item = $.extend(true, {}, obj.findItemFromFood(v.code), obj.schema.baseItem, v);
                //console.log("index " + v.index, v.type, v);
                if (item.type === "I" || item.type === "T") {
                    item.init();
                    lastInsertOptIdx = -1;
                    lastInsertSubitemIdx = -1;
                    if (v.voidIndex != undefined) {
                        if (v.voidIndex != -1) {
                            //itemList[v.voidIndex].voidQty = v.qty;
                            $.each(itemList, function (idx, vitem) {
                                if (vitem.index == v.voidIndex) {
                                    vitem.voidQty += v.qty;
                                }
                            });
                        }
                        item.voidRemark = v.voidRemark;
                    }
                    //console.log(item)
                    itemList.push(item);
                }
                else if (item.type === "O") { // option
                    if (lastInsertIdx != item.index)
                        lastInsertOptIdx++;

                    //if (itemList[itemList.length - 1].option == undefined)
                    //    itemList[itemList.length - 1].option = [{ items: [] }];
                    //itemList[itemList.length - 1].option[0].items.push(item);
                    if (itemList[itemList.length - 1].option == undefined)
                        itemList[itemList.length - 1].option = [];
                    if (itemList[itemList.length - 1].option[lastInsertOptIdx] == undefined)
                        itemList[itemList.length - 1].option[lastInsertOptIdx] = { items: [] };
                    itemList[itemList.length - 1].option[lastInsertOptIdx].items.push(item);

                    //if (itemList[itemList.length - 1] != undefined) {
                    //    angular.forEach(itemList[itemList.length - 1].option[lastInsertOptIdx].items, function (optItem) {
                    //        if (optItem.code === item.code) {
                    //            $.extend(optItem, item);
                    //        }
                    //    });
                    //}
                }
                else if (item.type === "S") {
                    /*
                    angular.forEach(itemList[itemList.length - 1].subitem, function (subitem) {
                        if (subitem.code === item.code) {
                            $.extend(subitem, item);
                        }
                    });*/

                    if (lastInsertIdx != item.index) {
                        lastInsertSubitemIdx++;
                    }


                    if (itemList[itemList.length - 1].subitem === undefined) {
                        itemList[itemList.length - 1].subitem = [];
                    }
                    console.log('itemList');
                    console.log(itemList);
                    itemList[itemList.length - 1].subitem.push(item);

                }
                else if (item.type === "M") { // modifier



                    //var m = obj.food.modifiers.filter(function (matchItem) {

                    //    return matchItem.code == item.code;
                    //});

                    var m = obj.food.allModifiers.filter(function (matchItem) {
                        return matchItem.code == item.code;
                    });

                    // if (itemList[itemList.length - 1] != undefined && item.index > 1 && lastInsertIdx == item.index && lastInsertOptIdx > -1) {
                    if (angular.isDefined(itemList[itemList.length - 1])) {
                        console.debug(9464);
                        if (itemList[itemList.length - 1] != undefined && item.index > 1 && lastInsertIdx == item.index && (lastInsertOptIdx > -1 || lastInsertSubitemIdx > -1)) {

                            console.log(9468);
                            //console.log("insert option", itemList[itemList.length - 1].option);
                            //console.log(itemList[itemList.length - 1].option[lastInsertOptIdx].items);
                            // option modifier
                            if (lastInsertOptIdx != -1)
                                angular.forEach(itemList[itemList.length - 1].option[lastInsertOptIdx].items, function (optItem) {
                                    if (optItem.index == item.index) {
                                        optItem.modifier.push(m[0]);
                                    }
                                });

                            if (lastInsertSubitemIdx != -1)
                                // subitem modifier
                                angular.forEach(itemList[itemList.length - 1].subitem, function (subitem) {
                                    console.log('add modifier into subitem / loading');
                                    if (subitem.index == item.index) {
                                        subitem.modifier.push(m[0]);
                                    }
                                });
                        }
                        else {
                            itemList[itemList.length - 1].modifier.push(m[0]);
                        }
                    }
                }
                else if (item.type === "K") { // kitchen message
                    // obj.food.kitchenmsg is preset kitchen message object list (Type: Array, Member: Object, only include name, namek, code, label)


                    var tmp = {};

                    // use a kitchenmsg preset list to match
                    var k = obj.food.kitchenmsg.filter(function (matchItem) {
                        //console.log('matchItem');
                        //console.log(matchItem.code);
                        //console.log(item.code);
                        //console.log(matchItem.code == item.code);
                        return matchItem.code == item.code;
                    });


                    // if (itemList[itemList.length - 1] != undefined && item.index > 1 && lastInsertIdx == item.index && lastInsertOptIdx > -1) {

                    if (angular.isDefined(itemList[itemList.length - 1])) {
                        // put kitchen message into parent item subitem => item.subitem
                        // k message for item's option
                        if (item.index > 1 && lastInsertIdx == item.index && lastInsertSubitemIdx > -1) {
                            console.log('add kitchen message into subitem / loading');

                            angular.forEach(itemList[itemList.length - 1].subitem, function (subitem) {
                                // here, item is kitchen item, when subitem index equal to kitchen item index, add into
                                if (subitem.index == item.index) {
                                    console.log(subitem);
                                    tmp = k[0];
                                    tmp.time = subitem.time;
                                    subitem.kitchenMsgOld.push(tmp);
                                    // subitem.kitchenMsg.push( tmp ); // test
                                }
                            });
                        }

                            // put kitchen message into parent item option's item => item.option.items
                        else if (item.index > 1 && lastInsertIdx == item.index && lastInsertOptIdx > -1) {
                            console.log('add kitchen message into option / loading');
                            //console.log("insert option", itemList[itemList.length - 1].option);
                            //console.log(itemList[itemList.length - 1].option[lastInsertOptIdx].items);

                            // k message for item's option
                            angular.forEach(itemList[itemList.length - 1].option[lastInsertOptIdx].items, function (optItem) {
                                if (optItem.index == item.index) {
                                    // console.log(optItem);
                                    tmp = k[0];
                                    tmp.time = optItem.time;
                                    optItem.kitchenMsgOld.push(tmp);
                                    // optItem.kitchenMsgOld.push(k[0]);
                                }
                            });
                        }

                        else {
                            // put kitchen message into parent item
                            itemList[itemList.length - 1].kitchenMsgOld.push(k[0]);
                        }
                    }


                }
                lastInsertIdx = item.index;
            });
            angular.forEach(itemList, function (order) {
                if (order.option != undefined && order.option) {
                    angular.forEach(order.option, function (optionList) {
                        var selectQty = 0;
                        angular.forEach(optionList.items, function (optItem) {
                            if (optItem.qty > 0) {
                                selectQty += optItem.qty
                            }
                        })
                        if (selectQty == 0) {
                            var fillOption = $.extend(true, {}, obj.schema.unselectOption, obj.schema.baseOption);
                            // var fillOption = $.extend(true, {}, obj.schema.unselectOption, {qty: 1, _qty: 1});

                            optionList.items.push(fillOption);
                        }
                        var hasUnselectOption = optionList.items.filter(function (x) {
                            return x.code === obj.schema.unselectOption.code;
                        });
                        if (hasUnselectOption.length) {
                            optionList.index = hasUnselectOption[0].index;
                            order.hasUnselectOption = true;
                        }
                    })
                }
            });

            if (order.discount) {
                for (var i = 0; i < order.discount.length; i++) {
                    var _obj = { 'coupon_alias': order.discount[i].discountCode, 'amount': order.discount[i].discountAmount, 'type': order.discount[i].discountType, 'price': order.discount[i].discountValue };
                    for (var j = 0; j < obj.availableDiscount.length; j++) {
                        if (_obj.coupon_alias == obj.availableDiscount[j].coupon_alias) {
                            _obj.name1 = obj.availableDiscount[j].name1;
                            _obj.name2 = obj.availableDiscount[j].name2;
                            _obj.name3 = obj.availableDiscount[j].name3;
                        }
                    }
                    obj.Cart.couponObj.push(_obj);
                }
            }

            console.log(obj.Cart.orderList);

            console.log('obj.appliedDiscount');
            console.log(obj.appliedDiscount);
            //console.debug(obj.appliedPaymentMethod, 'test');
            if (!obj.appliedPaymentMethod) obj.appliedPaymentMethod = [];
            if (obj.isModeOrder('billing') && obj.appliedPaymentMethod.length == 0) obj.checkPaymentTips();

            if (order.payment) {
                for (var i = 0; i < order.payment.length; i++) {
                    obj.appliedPaymentMethod.push({ 'method': order.payment[i].paymentType, 'amount': order.payment[i].paymentValue, 'totalAmount': order.payment[i].priceValue });
                }
            }
            console.debug(order);

            if (order.deposit) {
                for (var a = 0 ; a < order.deposit.length; a++) {
                    var tmpl = new depositManager.depositModel();
                    tmpl.setValue(order.deposit[a])
                    obj.deposit.appliedDeposit.push(tmpl);
                }
            }
            obj.currentPage = 1;
            //obj.appliedPaymentMethod = [];
        }

        obj.assignTableNo = function (tableNo, callback, tableMode, exact, ele) {
            // console.log(arguments);
            //$(angular.element.find('.orderPanelCtrl')).removeClass('ng-hide');
            console.log('assign table no(stay) / open order menu(take away)');
            console.debug('tableNo 8847', tableNo);
            obj.removeActive('#order-list li');
            // console.log(obj.UserManager.isLogin);
            if (!obj.UserManager.isLogin) {
                obj.UserManager.LoginRedirectToTable = tableNo;
                if (angular.element('md-dialog[aria-label="Login"]').scope() == undefined) {
                    $mdDialog.show({
                        templateUrl: 'tmpl/login.html'
                    })
                } else {
                    $mdDialog.hide();
                }
            } else {
                console.debug('9665', tableNo);
                var socketObj = { 'tableNum': { '$value': tableNo } };
                if (exact != undefined) {
                    socketObj.exact = { '$value': true }
                }

                if (obj.checkIsNotTakeAwayMode()) {
                    obj.eat.updateCategoryList();
                }

                MainService.deposit.appliedDeposit = [];
                socketObj.tableMode = tableMode == undefined ? "eatWriteOrder" : tableMode;

                if (obj.isModeFloor('changeTable') && MainService.modeTable == MainService.schema.modeTable.tableView) {
                    MainService.pleaseWait.off();


                    // for multiple tables, transfer using DOM
                    if (!$('body').hasClass('transfer') && obj.RMSTable.svgScene.selectAll(".transfer").length === 0) {
                        //alert('DOM transfer 0');
                        // console.log($(ele.currentTarget));

                        if (MainService.RMSTable.Scene[0].tableViewModel[tableNo] == undefined) {
                            //showMessage("#status-message", uiLanguage[INFO.lang]['table_must_be_opened'], "", "warn", 30000);
                            obj.showAlert(uiLanguage[INFO.lang]['table_must_be_opened']);
                            return;
                        }

                        obj.RMSTable.transfer_from = tableNo;

                        $('body').addClass('transfer transfer-from');

                
                        showMessage("#status-message", uiLanguage[INFO.lang]['transfer_to'], "", "warn", 30000);

                        console.log("from " + tableNo);

                        if (MainService.modeTable == MainService.schema.modeTable.tableView) {
                            $('button.table-model[table-model="' + tableNo + '"]').css('border', '4px solid black');
                        }

                    } else {
                        // transfer to DOM
                        // in use table cannot be transferred to
                        if (obj.RMSTable.transfer_from === tableNo) {
                            obj.cancelTransfer();
                            return;
                        }

                        if (MainService.modeTable == MainService.schema.modeTable.tableView) {
                            obj.RMSTable.transfer_to = tableNo;
                            $('button.table-model[table-model="' + tableNo + '"]').css('border', '4px solid black');
                        }

                        var msg = uiLanguage[INFO.lang]['to_be_transferred'];
                        msg = msg.replace("%1", obj.RMSTable.transfer_from);
                        msg = msg.replace("%2", obj.RMSTable.transfer_to);

                        MainService.showConfirm(msg, null, function () {
                            //console.log("to " + tableNum);
                            showMessage("#status-message", msg, "", "warn", 30000);
                            console.log('msg ' + msg);

                            MainService.pleaseWait.on();
                            console.debug(obj.RMSTable);

                            if (obj.RMSTable.transfer_from == 9999 || obj.RMSTable.transfer_to == 9999) {
                                obj.showAlert('錯誤-請重試!');
                                return;
                            }

                            SocketFactory.emit('changeTable', { sourceTableNo: obj.RMSTable.transfer_from, destinationTableNo: obj.RMSTable.transfer_to }, function (data) {

                                console.debug('9729 off');
                                MainService.pleaseWait.off();
                                if (data.result == "Locked") {
                                    obj.showAlert(data.tableOrderCode + uiLanguage[INFO.lang]['txt_lock_table']);
                                    obj.switchMode2({ modeFloorPlan: 'order', mode: 'floorPlan' });
                                }
                                obj.cancelTransfer();


                            });
                        }, function () {
                            obj.showAlert(uiLanguage[INFO.lang]['transfer_cancel']);
                            obj.cancelTransfer();
                        })
                        return;
                    }


                    return;
                }
                  




                SocketFactory.emit('adminLoadTableOrder', socketObj, function (r) {
                    console.debug('closed');
                    MainService.pleaseWait.off();
                    r = JSON.parse(r);
                    console.debug('lock table from ', r)

                    /** **/


                    /** **/

                    if (r.result === "Locked") {
                        //$.each($('.table'), function () {
                        //    console.debug($(this).find('text').text());
                        //})

                        var templObj = $.grep($('.table'), function (item) {
                            return $(item).find('text').text() == tableNo;
                        })

                        $(templObj[0]).attr('class', $(templObj[0]).attr('class') + " Lock ");
                        //alert lock table
                        console.log(r.clientIp);
                        obj.showAlert(tableNo + uiLanguage[INFO.lang]['txt_lock_table']);
                        //alert();
                        //callback();
                        //return;
                    }

                    if (r.result === "OK") {
                        if (r.order.length > 1) {
                            console.log('adminLoadTableOrder / callback');
                            console.log('callback', callback);
                            callback(r.order, tableMode);
                        }
                        else {
                            console.log('r.order[0]', r.order[0]);
                            var order = r.order[0];

                            console.debug(order.header.status);


                            if (obj.checkIsNotTakeAwayMode()) {
                                //the table have open 
                                if (order.header.status != undefined) {
                                    //tableMode == undefined => writeOrder
                                    if (obj.lockTable.tableNum != tableNo && (tableMode == undefined || tableMode == 'kitchenMessage') && (!obj.isModeFloor('changeTable'))) {
                                        obj.lockTable.tableNum = tableNo;
                                        obj.lockTable.orderNo = order.header.tableNum;
                                        //order.header.tableNum == "" ? tableNo : order.header.tableNum;
                                    }
                                    //obj.lockTable.toActiveLockTable()
                                }
                                else if (order.header.status == undefined && obj.isModeFloor('order')) {
                                    obj.lockTable.tableNum = tableNo;
                                    obj.lockTable.orderNo = tableNo;
                                }
                            }


                            if (tableMode === 'QRbilling') { tableMode = 'billing'; order.header.status = 3; }
                            console.log('mainService', order);
                            console.log(order.header.status);

                            // use tableStatus to check(pass or get from order object?)
                            if (obj.isModeFloor('changeTable')) {
                                console.log('+++++++++++++++++++++++++++++++++');

                                console.log(ele);
                                // return
                                // console.log(tableEle);

                                if (angular.isDefined(ele) && angular.isUndefined(ele.which)) {
                                    // for single table, transfer using SVG
                                    if (obj.RMSTable.svgScene.selectAll(".transfer").length === 0 && !$('body').hasClass('transfer')) {
                                        // console.log('transfer SVG 0');
                                        console.log('obj.RMSTable.svgScene.selectAll(".transfer").length = 0');
                                        // console.log(obj.RMSTable.isTableFree(ele));
                                        // var msg = "Please select TO table";
                                        // free table cannot be transferred from
                                        if (obj.RMSTable.isTableFree(ele)) {
                                            showMessage("#status-message", uiLanguage[INFO.lang]['table_must_be_opened'], "", "warn", 30000);
                                            return;
                                        }

                                        //obj.RMSTable.transfer_from = ele.parent().data("label");
                                        obj.RMSTable.transfer_from = order.header.tableNum

                                        ele.addClass("transfer");
                                        ele.ftHighlightBB({ handleFill: "purple" });
                                        showMessage("#status-message", uiLanguage[INFO.lang]['transfer_to'], "", "warn", 30000);

                                        console.log("from " + obj.RMSTable.transfer_from);
                                    }
                                    else {
                                        console.log('obj.RMSTable.svgScene.selectAll(".transfer").length else');
                                        // in use table cannot be transferred to
                                        if (obj.RMSTable.svgScene.select(".transfer") === ele) {
                                            // console.log(scope);
                                            obj.cancelTransfer();
                                            // showMessage("#status-message", scope.RMSTable.messages.table_cannot_be_self, "", "warn", 30000);
                                            return;
                                        }

                                        //if( order.header.status >= 2 ) {
                                        //    obj.showAlert('table_in_use');
                                        //    return;
                                        //}
                                        //console.log('order.header.tableNum', order.header.tableNum)
                                        obj.RMSTable.transfer_to = order.header.tableNum == "" ? ele.parent().data("label") : order.header.tableNum;
                                        //obj.RMSTable.transfer_to = ele.parent().data("label");
                                        var msg = uiLanguage[INFO.lang]['to_be_transferred'];
                                        msg = msg.replace("%1", obj.RMSTable.transfer_from);
                                        msg = msg.replace("%2", obj.RMSTable.transfer_to);

                                        MainService.showConfirm(msg, null, function () {
                                            console.log("to " + obj.RMSTable.transfer_to);
                                            showMessage("#status-message", msg, "", "warn", 30000);
                                            console.log('msg ' + msg);

                                            SocketFactory.emit('changeTable', { sourceTableNo: obj.RMSTable.transfer_from, destinationTableNo: obj.RMSTable.transfer_to }, function (data) {
                                                if (data.result == "Locked") {
                                                    obj.showAlert(data.tableOrderCode + uiLanguage[INFO.lang]['txt_lock_table']);
                                                    obj.switchMode2({ modeFloorPlan: 'order', mode: 'floorPlan' });
                                                }

                                            });
                                        }, function () {
                                            obj.showAlert(uiLanguage[INFO.lang]['transfer_cancel']);
                                            obj.cancelTransfer();
                                        })
                                        return;
                                    }
                                }
                                else {
                                    // for multiple tables, transfer using DOM
                                    if (!$('body').hasClass('transfer') && obj.RMSTable.svgScene.selectAll(".transfer").length === 0) {
                                        //alert('DOM transfer 0');
                                        // console.log($(ele.currentTarget));
                                        if (order.header.status === 0 || order.header.status == undefined) {
                                            showMessage("#status-message", uiLanguage[INFO.lang]['table_must_be_opened'], "", "warn", 30000);
                                            return;
                                        }

                                        obj.RMSTable.transfer_from = order.header.tableNum;
                                        $('body').addClass('transfer transfer-from');
                                        showMessage("#status-message", uiLanguage[INFO.lang]['transfer_to'], "", "warn", 30000);

                                        console.log("from " + order.header.tableNum);

                                        if (MainService.modeTable == MainService.schema.modeTable.tableView) {
                                            console.log($('button.table-model[table-model=' + order.header.tableNum + ']'))
                                            $('button.table-model[table-model="' + order.header.tableNum + '"]').css('border', '4px solid black');
                                        }

                                    } else {
                                        // transfer to DOM
                                        console.log('DOM transfer else');
                                        console.log(order);
                                        // in use table cannot be transferred to
                                        if (obj.RMSTable.transfer_from === order.header.tableNum) {
                                            obj.cancelTransfer();
                                            return;
                                        }

                                        obj.RMSTable.transfer_to = order.header.tableNum;
                                        if (MainService.modeTable == MainService.schema.modeTable.tableView) {
                                            obj.RMSTable.transfer_to = tableNo;
                                            $('button.table-model[table-model="' + tableNo + '"]').css('border', '4px solid black');
                                        }

                                        var msg = uiLanguage[INFO.lang]['to_be_transferred'];
                                        msg = msg.replace("%1", obj.RMSTable.transfer_from);
                                        msg = msg.replace("%2", obj.RMSTable.transfer_to);

                                        MainService.showConfirm(msg, null, function () {
                                            //console.log("to " + tableNum);
                                            showMessage("#status-message", msg, "", "warn", 30000);
                                            console.log('msg ' + msg);

                                            SocketFactory.emit('changeTable', { sourceTableNo: obj.RMSTable.transfer_from, destinationTableNo: obj.RMSTable.transfer_to }, function (data) {
                                                if (data.result == "Locked") {
                                                    obj.showAlert(data.tableOrderCode + uiLanguage[INFO.lang]['txt_lock_table']);
                                                    obj.switchMode2({ modeFloorPlan: 'order', mode: 'floorPlan' });
                                                }


                                            });
                                        }, function () {
                                            obj.showAlert(uiLanguage[INFO.lang]['transfer_cancel']);
                                            obj.cancelTransfer();
                                        })
                                        return;
                                    }
                                }
                                return;
                            }

                            obj.eat.updateCategoryListByTime(r.order[0].header.startTime);


                            function prepareTableOrder() {

                                var header = r.order[0].header;
                                console.log(header.peopleNum);
                                //console.log(prepareTableOrder, tableMode); && tableMode != "takeout"
                                //if (tableMode == "takeout") {
                                //    obj.Cart.noOfPeople = 1;
                                //    obj.setNoOfPeople();
                                //}


                                if (!obj.checkIsNotTakeAwayMode()) {
                                    obj.Cart.noOfPeople = 1;
                                } else {
                                    if (header.peopleNum == 0 && tableNo != "takeout") {
                                        obj.Cart.init();
                                        obj.Cart.tableNo = header.tableNum == "" ? tableNo : header.tableNum;
                                        if (order.header.member != undefined) {
                                            obj.Cart.member = $.extend(true, order.header.member, obj.schema.member);
                                        }
                                        obj.Cart.noOfPeople = 0; // reset
                                        obj.setNoOfPeople();
                                        return;
                                    }
                                }

                                obj.mode = obj.schema.mode.order;
                                tableMode = tableMode || "normal";
                                console.debug(tableMode);
                                obj.modeOrder = obj.schema.modeOrder[tableMode];

                                if (tableMode === 'kitchenMessage')
                                    obj.modeMultiSelectItem = true;
                                else
                                    obj.modeMultiSelectItem = false;

                                obj.Cart.init();
                                //obj.Cart.tableNo = tableNo;
                                obj.Cart.tableNo = header.tableNum == "" ? tableNo : header.tableNum;
                                obj.Cart.noOfPeople = order.header.peopleNum;

                                if (order.header.member != undefined) {
                                    obj.Cart.member = $.extend(true, order.header.member, obj.schema.member);
                                }

                                if (!obj.checkIsNotTakeAwayMode()) {
                                    otherWriteOrderService.children[0].cloneCartList();
                                    //obj.deliveryOrderService.cloneCartList();
                                }

                                // var lastInsertType = "",
                                //     lastInsertIdx = 0,
                                //     lastInsertOptIdx = -1;
                                // angular.forEach(order.item, function (v, k) {
                                //      var item = $.extend(true, {}, obj.schema.baseItem, v);
                                //     //var item = $.extend(true, {}, obj.findItemFromFood(v.code), obj.schema.baseItem, v);
                                //     console.log("index " + v.index, v.type, v);
                                //     if (item.type === "I" || item.type === "T") {
                                //         item.init();
                                //         lastInsertOptIdx = -1;
                                //         if (v.voidIndex != undefined) {
                                //             if (v.voidIndex != -1) {
                                //                 //obj.Cart.orderList[v.voidIndex].voidQty = v.qty;
                                //                 $.each(obj.Cart.orderList, function (idx, vitem) {
                                //                     if (vitem.index == v.voidIndex) {
                                //                         vitem.voidQty += v.qty;
                                //                     }
                                //                 });
                                //             }
                                //             item.voidRemark = v.voidRemark;
                                //         }
                                //         //console.log(item)
                                //         obj.Cart.orderList.push(item);
                                //     } else if (item.type === "O") {
                                //         if (lastInsertIdx != item.index)
                                //             lastInsertOptIdx++;
                                //
                                //          //if (obj.Cart.orderList[obj.Cart.orderList.length - 1].option == undefined)
                                //          //    obj.Cart.orderList[obj.Cart.orderList.length - 1].option = [{ items: [] }];
                                //         //obj.Cart.orderList[obj.Cart.orderList.length - 1].option[0].items.push(item);
                                //         if (obj.Cart.orderList[obj.Cart.orderList.length - 1].option == undefined)
                                //             obj.Cart.orderList[obj.Cart.orderList.length - 1].option = [];
                                //         if (obj.Cart.orderList[obj.Cart.orderList.length - 1].option[lastInsertOptIdx] == undefined)
                                //             obj.Cart.orderList[obj.Cart.orderList.length - 1].option[lastInsertOptIdx] = { items: [] };
                                //          obj.Cart.orderList[obj.Cart.orderList.length - 1].option[lastInsertOptIdx].items.push(item);
                                //
                                //         //if (obj.Cart.orderList[obj.Cart.orderList.length - 1] != undefined) {
                                //         //    angular.forEach(obj.Cart.orderList[obj.Cart.orderList.length - 1].option[lastInsertOptIdx].items, function (optItem) {
                                //         //        if (optItem.code === item.code) {
                                //         //            $.extend(optItem, item);
                                //         //        }
                                //         //    });
                                //         //}
                                //     } else if (item.type === "S") {
                                //         angular.forEach(obj.Cart.orderList[obj.Cart.orderList.length - 1].subitem, function (subitem) {
                                //             if (subitem.code === item.code) {
                                //                 $.extend(subitem, item);
                                //             }
                                //         });
                                //     } else if (item.type === "M") {
                                //         var m = obj.food.modifiers.filter(function (matchItem) {
                                //             return matchItem.code == item.code;
                                //         });
                                //         if (obj.Cart.orderList[obj.Cart.orderList.length - 1] != undefined && item.index > 1 && lastInsertIdx == item.index && lastInsertOptIdx > -1) {
                                //             //console.log("insert option", obj.Cart.orderList[obj.Cart.orderList.length - 1].option);
                                //             //console.log(obj.Cart.orderList[obj.Cart.orderList.length - 1].option[lastInsertOptIdx].items);
                                //             angular.forEach(obj.Cart.orderList[obj.Cart.orderList.length - 1].option[lastInsertOptIdx].items, function (optItem) {
                                //                 if (optItem.index == item.index) {
                                //                     optItem.modifier.push(m[0]);
                                //                 }
                                //             });
                                //         }
                                //         else {
                                //             obj.Cart.orderList[obj.Cart.orderList.length - 1].modifier.push(m[0]);
                                //         }
                                //     } else if (item.type === "K") {
                                //         var k = obj.food.kitchenmsg.filter(function (matchItem) {
                                //             return matchItem.code == item.code;
                                //         });
                                //         if (obj.Cart.orderList[obj.Cart.orderList.length - 1] != undefined && item.index > 1 && lastInsertIdx == item.index && lastInsertOptIdx > -1) {
                                //             //console.log("insert option", obj.Cart.orderList[obj.Cart.orderList.length - 1].option);
                                //             //console.log(obj.Cart.orderList[obj.Cart.orderList.length - 1].option[lastInsertOptIdx].items);
                                //             angular.forEach(obj.Cart.orderList[obj.Cart.orderList.length - 1].option[lastInsertOptIdx].items, function (optItem) {
                                //                 if (optItem.index == item.index) {
                                //                     optItem.kitchenMsgOld.push(k[0]);
                                //                 }
                                //             });
                                //         }
                                //         else {
                                //             obj.Cart.orderList[obj.Cart.orderList.length - 1].kitchenMsgOld.push(k[0]);
                                //         }
                                //     }
                                //     lastInsertIdx = item.index;
                                // });
                                // angular.forEach(obj.Cart.orderList, function(order) {
                                //     if (order.option != undefined && order.option) {
                                //         angular.forEach(order.option, function(optionList) {
                                //             var selectQty = 0;
                                //             angular.forEach(optionList.items, function(optItem) {
                                //                 if (optItem.qty > 0) {
                                //                     selectQty += optItem.qty
                                //                 }
                                //             })
                                //             if (selectQty == 0) {
                                //                 var fillOption = $.extend(true, {}, obj.schema.unselectOption, obj.schema.baseItem);
                                //                 // var fillOption = $.extend(true, {}, obj.schema.unselectOption, {qty: 1, _qty: 1});
                                //                 optionList.items.push(fillOption);
                                //             }
                                //         })
                                //     }
                                // });
                                //
                                // // console.log(obj.Cart.orderList);
                                //
                                // if (order.discount) {
                                //     for (var i = 0; i < order.discount.length; i++) {
                                //
                                //         obj.appliedDiscount.push({ 'coupon_alias': order.discount[i].discountCode, 'amount': order.discount[i].discountAmount, 'type': order.discount[i].discountType, 'price': order.discount[i].discountValue });
                                //         obj.Cart.couponObj = [{ 'coupon_alias': order.discount[i].discountCode, 'amount': order.discount[i].discountAmount, 'type': order.discount[i].discountType }];
                                //     }
                                // }
                                // console.log('obj.appliedDiscount');
                                // console.log(obj.appliedDiscount);
                                // if (order.payment) {
                                //     for (var i = 0; i < order.payment.length; i++) {
                                //         obj.appliedPaymentMethod.push({ 'method': order.payment[i].paymentType, 'amount': order.payment[i].paymentValue })
                                //     }
                                // }

                                if (!obj.checkIsNotTakeAwayMode()) {
                                    obj.assign2OrderList([], obj.Cart.orderList);
                                } else
                                    obj.assign2OrderList(order, obj.Cart.orderList);


                            }

                            // bill mode checking should be placed here
                            // alert(tableStatus);

                            if (tableMode === 'printBill') {
                                if (order.header.status == 2 || order.header.status == 3) {
                                    // obj.billTable(tableNo, 'billing');

                                    if (obj.lockTable.tableNum != tableNo) {
                                        obj.lockTable.tableNum = tableNo;
                                        obj.lockTable.orderNo = order.header.tableNum;
                                    }
                                    prepareTableOrder();

                                    return;
                                } else {
                                    obj.showAlert('no_order');
                                    return;
                                }
                            }

                            if (tableMode === 'printOrder') {
                                if (order.header.status == 2 || order.header.status == 3) {
                                    // obj.billTable(tableNo, 'billing');
                                    //prepareTableOrder();

                                    obj.printOrder(tableNo, 'summary');
                                    return;
                                } else {
                                    obj.showAlert('no_order');
                                    return;
                                }
                            }


                            if (tableMode === 'billing') {
                                console.log('table no to bill ' + order.header.status);

                                //order.header.status = 3;
                                if (obj.mifarePayment.isEnable && tableNo == obj.mifarePayment.tableNo) {
                                    order.header.status = 3;
                                    obj.mifarePayment.tableNo = "";
                                }

                                // flow for billing only
                                if (order.header.status == 2) {

                                    // should print first
                                    obj.showAlert('please_print_bill_first');
                                    return;
                                }
                                if (order.header.status == 3) {
                                    // obj.billTable(tableNo, 'billing');

                                    if (obj.lockTable.tableNum != tableNo) {
                                        obj.lockTable.tableNum = tableNo;
                                        obj.lockTable.orderNo = order.header.tableNum;
                                    }
                                    prepareTableOrder();
                                    return;
                                } else {
                                    obj.showAlert('no_order');
                                    return;
                                }
                            }
                            else {
                                // flow for other cases
                                prepareTableOrder();
                            }
                        }
                    }
                });
            }
        }

        obj.billTable = function (tableNo, tableMode) {
            console.log('tableNo', tableNo, tableMode);

            if (angular.isUndefined(tableMode)) {
                console.log('tableMode is undefined');
                return;
            }

            if (angular.isUndefined(tableNo)) {
                console.log('tableNo is undefined');
                return;
            }

            if (!obj.UserManager.isLogin) {
                obj.UserManager.LoginRedirectToTable = tableNo;
                if (angular.element('md-dialog[aria-label="Login"]').scope() == undefined) {
                    $mdDialog.show({
                        templateUrl: 'tmpl/login.html'
                    })
                } else {
                    $mdDialog.hide();
                }
            } else {
                console.log("tableMode:" + tableMode)
                var p = 'printBill';
                switch (tableMode) {
                    case 'printOrder': p = 'printBill'; break;
                    case 'printBill': p = 'printBill'; break;
                    case 'billing': p = 'billing'; break;
                    case 'QRbilling': p = 'billing'; break;
                }
                console.log(p);
                obj.checkPermission(p, function () {
                    obj.inputPrice = 0;
                    obj.remainder();
                    console.log('obj.mode ' + obj.mode);
                    console.log('obj.modeOrder ' + obj.modeOrder);
                    // obj.loadTableOrder(tableNo, tableMode);
                    console.log('obj.selectTable', tableNo, obj.selectTable, tableMode);
                    obj.assignTableNo(tableNo, obj.selectTable, tableMode);
                });
            }
        };

        obj.tableKitchenMessage = function (tableNo, tableMode) {
            if (angular.isUndefined(tableMode)) {
                console.log('tableMode is undefined');
                return;
            }

            if (angular.isUndefined(tableNo)) {
                console.log('tableNo is undefined');
                return;
            }

            if (!obj.UserManager.isLogin) {
                obj.UserManager.LoginRedirectToTable = tableNo;
                if (angular.element('md-dialog[aria-label="Login"]').scope() == undefined) {
                    $mdDialog.show({
                        templateUrl: 'tmpl/login.html'
                    })
                } else {
                    $mdDialog.hide();
                }
            } else {
                console.log("tableMode:" + tableMode)

                // console.log(p);
                // obj.checkPermission(p, function () {
                //     obj.inputPrice = 0;
                //     obj.remainder();
                //     console.log('obj.mode ' + obj.mode);
                //     console.log('obj.modeOrder ' + obj.modeOrder);

                // obj.loadTableOrder(tableNo, tableMode);
                obj.assignTableNo(tableNo, obj.selectTable, tableMode);
                // });
            }
        };

        obj.checkPermission = function (permission, callback, failCallback) {
            if (obj.UserManager.staffDetails[obj.schema.permission[permission]]) {
                callback();
            } else {
                if (failCallback) {
                    failCallback()
                } else {
                    console.log('no_permission');
                    obj.showAlert('no_permission');
                }
            }
        }

        obj.stock = {
            item: null,
            list: [], updateStock: function (qty) {
                console.log('obj.stock / updateStock');
                console.log(this.list);
                $.each(this.list, function (idx, item) {
                    if (item.isEdit) {
                        //item.qty = qty;
                        console.log(qty);
                        qty = qty == 0 ? -1 : qty;
                        console.log(qty);

                        console.log(item.code);
                        console.log(qty);

                        //var serviceUrl = 'service.ashx';
                        //var a =
                        SocketFactory.emit('setItemQty', { itemCode: item.code, qty: qty, remark: "", staffId: obj.UserManager.staffDetails.staffId }, function (result) {
                            console.log(result);
                            obj.stock.updateItem(JSON.parse(result));
                        });
                        //$http.post(serviceUrl, { action: "MDSetItemQty", data: a })
                        //    .then(function (result) {
                        //        //console.log("Data Saved: ", result);
                        //        //$scope.stock.updateItem(result.data);
                        //    });
                    }
                });
            }, updateStockItem: function (qty, item) {
                console.log(qty);
                qty = qty == 0 ? -1 : qty;
                console.log(qty);
                SocketFactory.emit('setItemQty', { itemCode: item.code, qty: qty, remark: "", staffId: obj.UserManager.staffDetails.staffId }, function (result) {
                    console.log(result);
                    obj.stock.updateItem(JSON.parse(result));
                });
            },
            updateItem: function (data) {
                console.log('data / updateItem / stock ', data);
                // console.log(data);
                var optionGroupCodeList = [],
                    optionCodeList = []; // for prevent duplication
                obj.stock.list = [];
                $.each(obj.food.category, function (catIndex, cat) {
                    $.each(cat.items, function (itemIndex, item) {
                        var found = false;
                        $.each(data, function (i, v) {
                            //console.log(item.code)
                            if (item.code == v.code) {
                                found = true;
                                item.stock = v.qty;
                                //console.log(item);
                                //console.log(item.code)
                                obj.stock.list.push($.extend(true, {}, obj.schema.baseItem, v, item));
                            }
                        });
                        if (!found) {
                            item.stock = '';
                        }
                        if (angular.isDefined(item.option)) {

                            $.each(item.option, function (idx, optionList) {
                                // if it is not yet in option code list
                                //if( $.inArray(optionList.optionGroupCode, optionGroupCodeList) === -1 ) {
                                optionGroupCodeList.push(optionList.optionGroupCode);

                                $.each(optionList.items, function (optionItemIndex, optionItem) {
                                    //if( $.inArray(optionItem.code, optionCodeList) === -1 ) {
                                    optionCodeList.push(optionItem.code);

                                    var foundOption = false;
                                    $.each(data, function (i, v) {
                                        // different group might have same option item, prevent duplication
                                        // console.log('checking');
                                        if (optionItem.code == v.code) {
                                            // console.log('found item option?');
                                            // console.log(optionItem);
                                            // console.log('looking for ' + v.code);
                                            // console.log('food control/option item code ' + optionItem.code)
                                            foundOption = true;
                                            optionItem.stock = v.qty;

                                            console.log(optionItem)
                                            obj.stock.list.push($.extend(true, {}, obj.schema.baseItem, v, optionItem));
                                        }
                                        if (!foundOption) {
                                            optionItem.stock = '';
                                        }
                                    });

                                    //}

                                });
                                //}
                            })

                        }
                    })

                    console.log(cat);
                    console.log('before enter sub cat');
                    if (angular.isDefined(cat.subCat)) {
                        console.log('enter sub cat');
                        $.each(cat.subCat, function (_i, _subcat) {
                            $.each(_subcat.items, function (_subItemIndex, _subItem) {
                                console.log('enter sub cat items');
                                var foundSubcatItem = false;
                                $.each(data, function (i, v) {
                                    if (_subItem.code == v.code) {
                                        foundSubcatItem = true;
                                        _subItem.stock = v.qty;
                                        obj.stock.list.push($.extend(true, {}, obj.schema.baseItem, v, _subItem));
                                    }
                                });
                                if (!foundSubcatItem) {
                                    _subItem.stock = "";
                                }
                            });
                        });
                    }

                });

                function uniqueBy(arr, fn) {
                    var unique = {};
                    var distinct = [];
                    arr.forEach(function (x) {
                        var key = fn(x);
                        //console.log(x);
                        if (!unique[key]) {
                            distinct.push(x);
                            unique[key] = true;
                        }
                    });
                    return distinct;
                }

                obj.stock.list = uniqueBy(obj.stock.list, function (x) { return x.code; });
                // console.log(optionGroupCodeList);
                // console.log(optionCodeList);
                // console.log(obj.stock.list);
            },
            submitItem: function (qty) {
                console.log(qty);
                qty = qty == 0 ? -1 : qty;
                console.log(qty);
                console.log('submitItem')
                var a = { itemCode: obj.stock.item.code, qty: qty, remark: "" }
                $http.post(serviceUrl, { action: "MDSetItemQty", data: a })
                    .then(function (result) {
                        //console.log("Data Saved: ", result);
                        //$scope.stock.updateItem(result.data);
                    });
                $('.calc-modal').modal('hide');
            },
            resetItem: function (item) {
                obj.stock.item = item;
                obj.stock.submitItem(-2);
            },
            reset: function (item) {
                console.debug('reset', item);

                SocketFactory.emit('resetItemQty', {}, function (r) {
                    console.log(r);
                    obj.stock.list = JSON.parse(r);
                    //obj.stock.updateStockItem(item, -1); // reset item stock display
                });
            }
        }

        obj.selectCategory = function (category, mainCat) {
            //console.log(category);
            var showSubCat = true;
            if (category == null) {
                category = mainCat;
                mainCat = null;
                showSubCat = false;
            }
            obj.parentCategory = category;
            obj.currentCategory = category;
            if ((category.subCat.length != 0 || mainCat) && showSubCat) {
                obj.totalSubCats = category.subCat.length + 1;
                obj.showSubcat = true;
                obj.currentSubcatPage = 1;
                if (mainCat != undefined) {
                    obj.parentCategory = mainCat;
                    //obj.currentCategory = category;
                    obj.totalSubCats = mainCat.subCat.length + 1;
                } else {
                    obj.totalSubCats = category.subCat.length + 1;
                }

                obj.noOfSubCategoryPage = Math.ceil(obj.totalSubCats / obj.catsPerPage);

            } else {
                obj.showSubcat = false;
            }
            console.debug(obj.currentCategory);
            console.debug("totalSubCats" + obj.totalSubCats);
            obj.currentPage = 1;
            obj.noOfCategoryPage = Math.ceil(obj.food.category.length / obj.catsPerPage);

            obj.totalItems = obj.currentCategory.items.length;
            obj.noOfItemPage = Math.ceil(obj.totalItems / obj.itemsPerPage);
            obj.subCatsPerPage = obj.catsPerPage - 1;

            obj.noOfSubCategoryPage = Math.ceil(obj.totalSubCats / obj.subCatsPerPage);

            // console.log('obj.noOfItemPage ' + obj.noOfItemPage);
            console.log('obj.totalItems ' + obj.totalItems);
            console.log('obj.itemsPerPage ' + obj.itemsPerPage);
            console.log('obj.catsPerPage ' + obj.catsPerPage);

            console.log('remainder ' + obj.totalItems % obj.itemsPerPage);

            if (MainService.getOtherWriteOrderService()) {
                MainService.getOtherWriteOrderService().currentCategory = MainService.currentCategory;
            }
            // $scope.$emit('reRender');
        }

        obj.getPageNumber = function (pageNum, test) {
            // console.log(pageNum);
            // console.log(typeof pageNum === 'NaN');
            // console.log(typeof pageNum === 'number');
            // console.log(typeof pageNum);
            // console.log(isNaN(pageNum));
            //console.log(pageNum);
            //if (test) console.log(pageNum);
            if (isNaN(pageNum))
                return new Array(3);

            return new Array(pageNum);
        }

        //obj.setModeMultiSelectItem = function (mode) {
        //    var selectMode = mode ? mode : "multi"; // default
        //    this.selectMode = selectMode;
        //    return selectMode;
        //};


        obj.toggleActive = function (element, callback) {
            if (typeof element === 'object') {
                // treat as event object
                var target = element.currentTarget || element.srcElement;
            } else {
                var target = element;
            }

            if (this.modeMultiSelectItem) {
                if (angular.element(target).hasClass("active")) {
                    angular.element(target).removeClass("active");
                } else {
                    angular.element(target).addClass("active");
                }
            } else {
                obj.setActive(element);
            }

            (callback || angular.noop)();
        }

        obj.setActive = function (element, callback) {
            // console.log(element);
            console.log('set active');
            if (typeof element === 'object' && (angular.isDefined(element.currentTarget) || angular.isDefined(element.srcElement))) {
                // treat as event object
                var target = element.currentTarget || element.srcElement;
            } else {
                var target = element;
            }
            // console.log(element);
            // console.log(target);
            angular.element(target).parents('.md-toolbar-item').find('.active').removeClass("active");
            angular.element(target).addClass("active");
            (callback || angular.noop)();
        }

        obj.removeActive = function (element, callback) {
            if (typeof element === 'object') {
                // treat as event object
                var target = element.currentTarget || element.srcElement;
            } else {
                var target = element;
            }
            //console.log(target);
            angular.element(target).removeClass("active");

            (callback || angular.noop)();
        }

        obj.cancelDeleteItem = function () {
            obj.modeMultiSelectItem = false;
            obj.modeItem = obj.schema.modeItem.normal;
        }

        obj.unselectAllItem = function () {
            obj.removeActive('.order-list li', function () { });
        }

        obj.selectAllItem = function () {
            obj.setActive('.order-list li', function () { });
        }

        obj.cancelModifier = function () {
            $rootScope.$emit('modifierClose'); //call modifierGroupCtrl to reset modifier 
            obj.modeMultiSelectItem = false;
            var c = false;
            if (otherWriteOrderService.children[0])
                otherWriteOrderService.children[0].btnCancelAction(function (r) {
                    if (r == 'OK') c = true;
                });

            //obj.deliveryOrderService.btnCancelAction(function (r) {
            //    if (r == 'OK') c = true;
            //});
            obj.modeOrder = obj.schema.modeOrder.normal;
            obj.modeItem = obj.schema.modeItem.normal;
            if (c) return;
            $rootScope.$emit('returnToOrder', []);
        }


        obj.cancelOption = function () {
            obj.modeMultiSelectItem = false;
            obj.modeOrder = obj.schema.modeOrder.normal;
            obj.modeItem = obj.schema.modeItem.normal;

            var c = false;
            if (otherWriteOrderService.children[0])
                otherWriteOrderService.children[0].btnCancelAction(function (r) {
                    if (r == 'OK') c = true;
                });

            //obj.deliveryOrderService.btnCancelAction(function (r) {
            //    if (r == 'OK') c = true;
            //});


            if (c) return;


            $rootScope.$emit('returnToOrder', []);
        }


        obj.foodPromise = $http.get('data/food.json?t=' + createTimeStamp());
        //obj.loadStockPromise = $http.post(serviceUrl, { "action": "MDLoadItemQty" });
        obj.loadStockPromise = function () {
            var deferred = $q.defer();
            SocketFactory.emit('loadItemQty', '', function (r) {
                r = JSON.parse(r);
                deferred.resolve(r);
            });
            return deferred.promise;
        }

        obj.loadDiscountPromise = function () {
            var deferred = $q.defer();
            SocketFactory.emit('loadDiscount', '', function (r) {
                r = JSON.parse(r);
                deferred.resolve(r);
            });
            return deferred.promise;
        }


        obj.loadTenderPromise = function () {
            var deferred = $q.defer();
            SocketFactory.emit('loadTender', '', function (r) {
                r = JSON.parse(r);
                deferred.resolve(r);
            });
            return deferred.promise;
        }

        obj.dailyClearancePromise = function () {
            var deferred = $q.defer();
            //SocketFactory.emit('checkDailyClearance', '', function (r) {
            SocketFactory.emit('checkDailyClearance', '', function (r) {
                r = JSON.parse(r);
                deferred.resolve(r);
            });
            return deferred.promise;
        }

        //obj.octopusBlacklistStatusPromise = function () {
        //    var deferred = $q.defer();
        //    SocketFactory.emit('checkBlacklistStatus', { 'till': userConfig.page.till }, function (r) {
        //        r = JSON.parse(r);
        //        deferred.resolve(r);
        //    });
        //    return deferred.promise;
        //}

        obj.tablePromise = $http.get('data/table_type.json');

        obj.takeawayCategorySettingPromise = $http.get('data/takeaway-category.json');
        obj.eatCategorySettingPromise = $http.get('data/eat-category.json');
        obj.deliveryCategorySettingPromise = $http.get('data/delivery-category.json');


        //obj.promise = $q.all([obj.foodPromise, obj.loadStockPromise(), obj.tablePromise]).then(function (values) {
        obj.promise = $q.all([obj.foodPromise, obj.loadStockPromise(), obj.tablePromise, obj.loadDiscountPromise(), obj.loadTenderPromise(), obj.dailyClearancePromise(), obj.takeawayCategorySettingPromise, obj.eatCategorySettingPromise, obj.deliveryCategorySettingPromise]).then(function (values) {
            console.log('obj.promise--------------------');
            console.log(values);
            obj.table = values[2].data;
            obj.food = values[0].data;

            obj.food.category.forEach(function (c) {
                c.items.forEach(function (i, idx) {
                    obj.combo.selectMode.allItemList.push(i);
                })
                if (!c.subCat) c.subCat = [];
                c.subCat.forEach(function (subCat) {
                    subCat.items.forEach(function (i) {
                        obj.combo.selectMode.allItemList.push(i);
                    })
                })



            })

            obj.fireCode = obj.food.fireCode;
            obj.mifarePayment.isEnable = obj.food.mifarePayment;
            obj.takeAwayCollected = obj.food.takeAwayCollected;

            //console.debug(obj.food);
            obj.food.allModifiers = angular.copy(obj.food.modifiers);

            //$rootScope.$emit('initModifier');


            //obj.category[obj.schema.modeOtherWriteOrderService.takeAway] = angular.copy(values[6].data.category);
            //obj.takeOutInOrder.Category = values[6].data.category;
            //obj.deliveryOrderService.allCategory = obj.food.category;
            //obj.deliveryOrderService.Category = values[8].data.category;


            obj.stock.updateItem(values[1]);


            obj.noOfModifierPage = Math.ceil(obj.food.modifiers.length / obj.cancelOptionPerPage);
            obj.currentCategory = obj.food.category[0];
            obj.noOfCategoryPage = Math.ceil(obj.food.category.length / obj.catsPerPage);
            // console.log('obj.noOfCategoryPage ' + obj.noOfCategoryPage);
            // console.log(obj.food.category);
            // console.log(obj.food.category.length);
            if (!obj.currentCategory.items) obj.currentCategory.items = [];
            obj.totalItems = obj.currentCategory.items.length;
            obj.noOfItemPage = Math.ceil(obj.totalItems / obj.itemsPerPage);
            obj.totalCats = obj.food.category.length;
            console.log('load discount')
            console.log(values[3]);
            // console.log(typeof values[3]);
            // console.log(values[3] === null );

            obj.availableDiscount = [];

            otherWriteOrderService.allCategory = angular.copy(obj.food.category);

            //obj.allCategory = angular.copy(obj.food.category);
            //obj.takeOutInOrder.allCategory = obj.food.category;

            /*otherWriteOrderService.addCategoryToChild(obj.schema.modeOtherWriteOrderService.takeAway, values[6].data.category);
            otherWriteOrderService.addCategoryToChild(obj.schema.modeOtherWriteOrderService.fastFood, values[6].data.category);
            otherWriteOrderService.addCategoryToChild(obj.schema.modeOtherWriteOrderService.deliveryOrder, values[6].data.category);*/

            for (key in obj.schema.modeOtherWriteOrderService) {
                otherWriteOrderService.addCategoryToChild(obj.schema.modeOtherWriteOrderService[key], values[6].data.category);
            }

            obj.eat.allCategory = angular.copy(obj.food.category);
            obj.eat.Category = values[7].data.category;

            if (values[3] != null) {
                for (var i = 0; i < values[3].length; i++) {
                    obj.availableDiscount.push({
                        coupon_alias: values[3][i].discountCode,
                        amount: values[3][i].discountAmount,
                        type: values[3][i].discountType,
                        name1: values[3][i].discountName1,
                        name2: values[3][i].discountName2,
                        name3: values[3][i].discountName3,
                    });
                }
            }
            console.log(values[4]);
            obj.tender = [];
            obj.allTenderForDisplay = [];
            //console.debug(values[4])
            if (values[4] != null) {
                for (var i = 0; i < values[4].length; i++) {

                    var temp = {
                        code: values[4][i].tenderCode,
                        name1: values[4][i].tenderName1,
                        name2: values[4][i].tenderName2,
                        name3: values[4][i].tenderName3,
                        amount: values[4][i].amount,
                        tenderType: values[4][i].tenderType,
                        permission: values[4][i].permission,
                        isEnt: values[4][i].isEnt

                    };

                    obj.allTenderForDisplay.push(temp);
                    if (!values[4][i].hidden) {
                        obj.tender.push(temp);
                    }
                }

                setTimeout(function () {
                    if (userConfig.page.multiPaymentPanel && userConfig.page.individualPayment) {
                        var temp = {
                            code: "multipay",
                            name1: "多重付款",
                            name2: "MultiPay",
                            name3: "MultiPay",
                            amount: 0,
                            tenderType: null,
                            permission: null
                        };
                        obj.allTenderForDisplay.push(temp);
                        obj.tender.push(temp);
                    }

                    if (userConfig.takeOutAndDelivery) {
                        var temp = {
                            code: "payLater",
                            name1: "稍後付款",
                            name2: "PayLater",
                            name3: "PayLater",
                            amount: 0,
                            tenderType: null,
                            permission: null
                        };
                        obj.allTenderForDisplay.push(temp);
                        obj.tender.push(temp);
                    }

                    if (userConfig.deposit) {
                        var temp = {
                            code: "deposit",
                            name1: "Deposit",
                            name2: "Deposit",
                            name3: "Deposit",
                            amount: 0,
                            tenderType: null,
                            permission: null
                        }
                        obj.allTenderForDisplay.push(temp);
                        obj.tender.push(temp);

                    }

                }, 1000)

            }

            //console.log('6666666666');
            //console.log(values[5].result);
            if (values[5].result === "ERROR") {
                obj.dailyClearanceCheck = true;
            }

            //console.log('obj');
            //console.log(obj);
            //alert(globalController);

            //{
            //    //    coupon_alias: "cash_coupon_500",
            //    //    amount: 500,
            //    //    type: 'exact'
            //    //},
            //    {
            //        coupon_alias: "off_10",
            //        amount: 0.9,
            //        type: 'percentage'
            //    }

            //obj.stock = {};
            //obj.stock.updateItem(JSON.parse(values[1].data));
        }, function (e) { console.log(e); })

        // event control, from any action in any controller
        $rootScope.$on('saveOrder', function (event, data) {
            // alert();
            // if( obj.modeCheckFloor('kitchenMessage') ) {
            //     obj.switchMode2({modeFloorPlan: 'order', mode: 'main'}, false, 'order_mode', 'order_mode');
            // }
            obj.switchMode2({ modeFloorPlan: 'order', mode: 'floorPlan', modeItemListDisplay: 'normal', modeItemListDisplay2: 'normal' }, false, 'order_mode', 'order_mode');
            obj.setActive(angular.element('.btn-order'));
            $.extend(obj.Cart, { "cartList": [], "orderList": [] });
        });

        $rootScope.$on('cancelOrder', function (event, data) {
            obj.switchMode2({ modeFloorPlan: 'order', mode: 'floorPlan', modeItemListDisplay: 'normal', modeItemListDisplay2: 'normal' }, false, 'order_mode', 'order_mode');
            obj.setActive(angular.element('.btn-order'));
            $.extend(obj.Cart, { "cartList": [], "orderList": [] });
            MainService.lockTable.toReleaseLockTable(obj.lockTable.tableNum);
        });

        $rootScope.$on('returnToOrder', function (event, data) {
            console.log('event / returnToOrder');
            if (obj.isModeItem('optionFoodControl')) {
                // obj.switchMode2({modeItem: 'normal'});
                console.log(obj.modeItem);
                obj.goToMode2({ modeItem: 'normal' }, null, false);
                console.log(obj.modeItem);
                return;
            }

            if (obj.isModeFloor('kitchenMessage')) {
                // obj.switchMode2({modeItem: 'normal'});



                console.log(obj.modeFloorPlan);
                obj.goToMode2({ mode: 'floorPlan', modeFloorPlan: 'order' }, null, false);
                console.log(obj.modeFloorPlan);
                return;
            }
            // alert();
            obj.switchMode2({ modeOrder: 'normal', mode: 'order', modeItemListDisplay: 'normal', modeItemListDisplay2: 'normal' }, false, 'order_mode', 'order_mode');
        });

        $rootScope.$on('cancelSelectionMode', function (event, data) {
            // alert();

        });

        $rootScope.$on('toggleItem', function (event, data) {

            // alert();
            // new item item check

            // existing item check

        });

        $rootScope.$on('toggleItemOption', function (event, data) {
            // data.modeOrder
            // data.modeItem
            console.log(data);

            // alert();
            // new item item check

            // existing item check

        });


        window['MainService'] = obj;
        return obj;
    })


    //app.service('SocketService', function (SOCKETPORT,$q) {
    //    this.socketConnection = 'http://' + location.hostname + ':' + SOCKETPORT;
    //    this.socket = io.connect(this.socketConnection);
    //    this.socketReady = false;
    //    io.Manager(this.socketConnection, { reconnect: true });

    //    var deferred = $q.defer();
    //    this.getSocket = function () {
    //        return deferred.promise;
    //    };
    //    this.socket.on('connect', function () {
    //        console.log('socket connected');
    //        this.socketReady = true;
    //        deferred.resolve({});
    //    });
    //})

    //app.service('MainService', function ($http) {

    //});
    // performance tweak
    // Remove duplicate watchers

})();
