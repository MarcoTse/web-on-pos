/*
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/mit-license.php
 *
 * Adapted from original source https://github.com/ElbertF/Raphael.FreeTransform
 * to work with Snap.svg
 *
 */
(function (root, factory) {
    if ( typeof define === 'function' && define.amd ) {
        // AMD. Register as an anonymous module.
        define(['snap'], function(Snap) {
            // Use global variables if the locals are undefined.
            return factory(Snap || root.Snap);
        });
    } else {
        factory(Snap);
    }
}(this, function(Snap) {

    // retrofit toFront() and toBack() to Snap
    Snap.plugin(function (Snap, Element, Paper, glob) {
        var elproto = Element.prototype;
        elproto.toFront = function () {
            this.appendTo(this.paper);
            return this;

        };
        elproto.toBack = function () {
            this.prependTo(this.paper);
            return this;
        };
    });

    // Adapt freeTransform() for Snap
    Snap.plugin(function (Snap, Element, Paper, glob) {
        var freeTransform = function(subject, options, callback) {
            // Enable method chaining
            if ( subject.freeTransform ) { return subject.freeTransform; }

            // console.log(subject);
            // console.log(subject.paper);
            var paper = subject.paper,
                bbox  = subject.getBBox(true),
                boundaryBBox = paper.getBBox();

            // console.log(paper);
            // console.log(subject);
            var ft = subject.freeTransform = {
                // Keep track of transformations
                attrs: {
                    x: bbox.x,
                    y: bbox.y,
                    size: { x: bbox.width, y: bbox.height },
                    center: { x: bbox.cx, y: bbox.cy }, // this center is for handles but for transformation restore/save, better separate
                    objCenter: { x: 0, y: 0 }, // this center is for handles but for transformation(rotate, scale) restore/save, better separate
                    rotate: 0,
                    scale: { x: 1, y: 1 },
                    translate: { x: 0, y: 0 },
                    ratio: 1
                },
                axes: null,
                bbox: null,
                callback: null,
                items: [],
                handles: { center: null, x: null, y: null },
                offset: {
                    rotate: 0,
                    scale: { x: 1, y: 1 },
                    translate: { x: 0, y: 0 }
                },
                opts: {
                    animate: false,
                    attrs: { fill: '#red', stroke: '#000' }, // center
                    bboxAttrs: { fill: 'none', stroke: '#000', 'stroke-dasharray': '4, 4', opacity: 0.5}, // unknown yet
                    axesAttrs: { fill: 'blue', stroke: '#000', 'stroke-dasharray': '4, 4', opacity: 0.5}, // handle axes
                    discAttrs: { fill: 'blue', stroke: '#000' }, // handle disc
                    boundary: { x: paper._left || 0, y: paper._top || 0, width: null, height: null },
                    // boundary: { x: 0, y: 0, width: boundaryBBox.width || null, height: boundaryBBox.height || null },
                    distance: 1.3,
                    drag: true,
                    draw: false,
                    keepRatio: false,
                    range: { rotate: [ -180, 180 ], scale: [ -99999, 99999 ] },
                    rotate: true,
                    scale: true,
                    snap: { rotate: 0, scale: 0, drag: 0 },
                    snapDist: { rotate: 0, scale: 0, drag: 7 },
                    size: 5,
                    bodyCursor: 'auto',
                    cornerCursors: ['nwse-resize', 'nesw-resize', 'nwse-resize', 'nesw-resize'],
                    sideCursors: ['ns-resize', 'ew-resize', 'ns-resize', 'ew-resize'],
                    defaultAttrs: {
                        x: bbox.x,
                        y: bbox.y,
                        size: { x: bbox.width, y: bbox.height },
                        // center: { x: bbox.cx, y: bbox.cy },
                        center: { x: bbox.cx, y: bbox.cy },
                        objCenter: { x: 0, y: 0 },
                        rotate: 0,
                        scale: { x: 1, y: 1 },
                        translate: { x: 0, y: 0 },
                        ratio: 1
                    }
                },
                subject: subject,
            };

            // additional
            ft.attrs = ft.opts.defaultAttrs;
            // console.log(ft.attrs);
            // console.log('test ft.opts.defaultAttrs ' + ft.opts.defaultAttrs.center.x);
            // console.log("boundary");
            // console.log(paper);
            // console.log(ft.opts.boundary);
            // console.log(getPaperSize());

            /**
             * if viewBox is changed dynamically, need to recalculate each time to drag and move (generally, will not happen)
             */
            function updateViewBoxRatio() {
                if ( paper.attr("viewBox") ) {
                    paper._viewBox = paper.attr("viewBox").vb.split(" ");
                    var viewBoxAspectRatio = paper._viewBox[2] / paper._viewBox[3];
                    // console.log('viewBoxAspectRatio ');
                    // console.log(viewBoxAspectRatio);

                    // according to experiment and observation, aspect ratio also affect the viewbox/paper dragging distance
                    ft.attrs.viewBoxAspectRatio = {
                        x: paper._viewBox[3] > paper._viewBox[2] ? viewBoxAspectRatio : 1,
                        y: paper._viewBox[2] > paper._viewBox[3] ? viewBoxAspectRatio : 1
                    }

                    ft.attrs.viewBoxAspectRatioSingle = viewBoxAspectRatio;

                    ft.attrs.viewBoxRatio = {
                        x: paper._viewBox[2] / getPaperSize().w,
                        y: paper._viewBox[3] / getPaperSize().h
                    };
                    // console.log(paper._viewBox[2]);
                    // console.log(ft.attrs.viewBoxRatio);
                }
                // console.log("+++++++++++++++++++++++++++++++");
                // console.log(ft.attrs.viewBoxRatio);
                // console.log(getPaperSize());
            }
            // updateViewBoxRatio()

            /**
             * Update handles based on the element's transformations
             */
            ft.updateHandles = function() {
                console.log("update handles");

                console.log(this.attrs.scale);

                if ( ft.handles.bbox || ft.opts.rotate.indexOf('self') >= 0 ) {
                    var corners = getBBox();
                }

                // Get the element's rotation
                var rad = {
                    x: ( ft.attrs.rotate      ) * Math.PI / 180,
                    y: ( ft.attrs.rotate + 90 ) * Math.PI / 180
                };

                var radius = {
                    x: ft.attrs.size.x / 2 * ft.attrs.scale.x,
                    y: ft.attrs.size.y / 2 * ft.attrs.scale.y
                };

                ft.axes.map(function(axis) {
                    if ( ft.handles[axis] ) {

                        var cx = ft.attrs.center.x + ft.attrs.translate.x + radius[axis] * ft.opts.distance * Math.cos(rad[axis]),
                            cy = ft.attrs.center.y + ft.attrs.translate.y + radius[axis] * ft.opts.distance * Math.sin(rad[axis]);


                        // Keep handle within boundaries
                        if ( ft.opts.boundary ) {
                            //cx = Math.max(Math.min(cx, ft.opts.boundary.x + ( ft.opts.boundary.width  || getPaperSize().x )), ft.opts.boundary.x);
                            //cy = Math.max(Math.min(cy, ft.opts.boundary.y + ( ft.opts.boundary.height || getPaperSize().y )), ft.opts.boundary.y);
                        }

                        ft.handles[axis].disc.attr({ 'cx': cx, 'cy': cy });

                        ft.handles[axis].line.toFront().attr({
                            path: [ [ 'M', ft.attrs.center.x + ft.attrs.translate.x, ft.attrs.center.y + ft.attrs.translate.y ], [ 'L', ft.handles[axis].disc.attr('cx'), ft.handles[axis].disc.attr('cy') ] ]
                        });

                        ft.handles[axis].disc.toFront();
                    }
                });

                if ( ft.bbox ) {
                    ft.bbox.toFront().attr({
                        path: [
                            [ 'M', corners[0].x, corners[0].y ],
                            [ 'L', corners[1].x, corners[1].y ],
                            [ 'L', corners[2].x, corners[2].y ],
                            [ 'L', corners[3].x, corners[3].y ],
                            [ 'L', corners[0].x, corners[0].y ]
                        ]
                    });

                    // Allowed x, y scaling directions for bbox handles
                    var bboxHandleDirection = [
                        [ -1, -1 ], [ 1, -1 ], [ 1, 1 ], [ -1, 1 ],
                        [  0, -1 ], [ 1,  0 ], [ 0, 1 ], [ -1, 0 ]
                    ];

                    if ( ft.handles.bbox ) {
                        ft.handles.bbox.map(function (handle, i) {
                            var cx, cy, j, k;

                            if ( handle.isCorner ) {
                                cx = corners[i].x;
                                cy = corners[i].y;
                            } else {
                                j  = i % 4;
                                k  = ( j + 1 ) % corners.length;
                                cx = ( corners[j].x + corners[k].x ) / 2;
                                cy = ( corners[j].y + corners[k].y ) / 2;
                            }

                            handle.element.toFront()
                                .attr({
                                    x: cx - ( handle.isCorner ? ft.opts.size.bboxCorners : ft.opts.size.bboxSides ),
                                    y: cy - ( handle.isCorner ? ft.opts.size.bboxCorners : ft.opts.size.bboxSides )
                                })
                                .transform('R' + ft.attrs.rotate);

                            handle.x = bboxHandleDirection[i][0];
                            handle.y = bboxHandleDirection[i][1];
                        });
                    }
                }

                if ( ft.circle ) {
                    ft.circle.attr({
                        cx: ft.attrs.center.x + ft.attrs.translate.x,
                        cy: ft.attrs.center.y + ft.attrs.translate.y,
                        r:  Math.max(radius.x, radius.y) * ft.opts.distance
                    });
                }

                if ( ft.handles.center ) {
                    ft.handles.center.disc.toFront().attr({
                        cx: ft.attrs.center.x + ft.attrs.translate.x,
                        cy: ft.attrs.center.y + ft.attrs.translate.y
                    });
                }

                if ( ft.opts.rotate.indexOf('self') >= 0 ) {
                    radius = Math.max(
                        Math.sqrt(Math.pow(corners[1].x - corners[0].x, 2) + Math.pow(corners[1].y - corners[0].y, 2)),
                        Math.sqrt(Math.pow(corners[2].x - corners[1].x, 2) + Math.pow(corners[2].y - corners[1].y, 2))
                    ) / 2;
                }

                return ft;
            };
            // ft.updateHandles = function(){}
            /**
             * Add handles
             */
            ft.showHandles = function() {
                console.log("show handles");
                ft.hideHandles();

                // if( !handleGroup ) {
                //     var handleGroup = paper.group().attr({
                //         // "id": "handle-group",
                //         "class": "handle-group"
                //     });
                // }

                ft.axes.map(function(axis) {
                    // console.log("run me");
                    ft.handles[axis] = {};
                    
                    ft.handles[axis].line = paper
                        .path([[ 'M', ft.attrs.center.x, ft.attrs.center.y ]])
                        .attr(ft.opts.axesAttrs);

                    ft.handles[axis].disc = paper
                        .circle(ft.attrs.center.x, ft.attrs.center.y, ft.opts.size.axes * ft.attrs.scale.x )
                        .attr(ft.opts.attrs);
                });

                if ( ft.opts.draw.indexOf('bbox') >= 0 ) {
                    ft.bbox = paper
                        .path('')
                        .attr(ft.opts.bboxAttrs);

                    ft.handles.bbox = [];

                    var i, handle, cursor;

                    for ( i = ( ft.opts.scale.indexOf('bboxCorners') >= 0 ? 0 : 4 ); i < ( ft.opts.scale.indexOf('bboxSides') === -1 ? 4 : 8 ); i ++ ) {
                        handle = {};

                        handle.axis     = i % 2 ? 'x' : 'y';
                        handle.isCorner = i < 4;

                        cursor = (handle.isCorner) ? ft.opts.cornerCursors[i] : ft.opts.sideCursors[i - 4];

                        handle.element = paper
                            .rect(ft.attrs.center.x, ft.attrs.center.y, ft.opts.size[handle.isCorner ? 'bboxCorners' : 'bboxSides' ] * 2, ft.opts.size[handle.isCorner ? 'bboxCorners' : 'bboxSides' ] * 2)
                            .attr(ft.opts.attrs)
                            .attr('cursor', cursor);

                        ft.handles.bbox[i] = handle;
                    }
                }

                if ( ft.opts.draw.indexOf('circle') !== -1 ) {
                    ft.circle = paper
                        .circle(0, 0, 0)
                        .attr(ft.opts.bboxAttrs);
                }

                if ( ft.opts.drag.indexOf('center') !== -1 ) {
                    console.log('drag center');
                    ft.handles.center = {};

                    ft.handles.center.disc = paper
                        .circle(ft.attrs.center.x, ft.attrs.center.y, ft.opts.size.center * ft.attrs.scale.x)
                        .attr(ft.opts.attrs);
                }

                // Drag x, y handles
                ft.axes.map(function(axis) {
                    if ( !ft.handles[axis] ) {
                        return;
                    }

                    var rotate = ft.opts.rotate.indexOf('axis' + axis.toUpperCase()) !== -1,
                        scale  = ft.opts.scale .indexOf('axis' + axis.toUpperCase()) !== -1;

                        var _dragMove = function(dx, dy) {
                            // viewBox might be scaled
                            if ( ft.o.viewBoxRatio ) {
                                dx *= ft.o.viewBoxRatio.x;
                                dy *= ft.o.viewBoxRatio.y;
                            }

                            var cx = dx + parseInt(ft.handles[axis].disc.ox, 10),
                                cy = dy + parseInt(ft.handles[axis].disc.oy, 10);

                            var mirrored = {
                                x: ft.o.scale.x < 0,
                                y: ft.o.scale.y < 0
                            };

                            // console.log(ft.o);

                            if ( rotate ) {
                                var rad = Math.atan2(cy - ft.o.center.y - ft.o.translate.y, cx - ft.o.center.x - ft.o.translate.x);

                                ft.attrs.rotate = rad * 180 / Math.PI - ( axis === 'y' ? 90 : 0 );

                                if ( mirrored[axis] ) {
                                    ft.attrs.rotate -= 180;
                                }
                            }

                            // Keep handle within boundaries
                            // console.log(ft.opts.boundary);
                            if ( ft.opts.boundary ) {
                                cx = Math.max(Math.min(cx, ft.opts.boundary.x + ( ft.opts.boundary.width  || getPaperSize().x )), ft.opts.boundary.x);
                                cy = Math.max(Math.min(cy, ft.opts.boundary.y + ( ft.opts.boundary.height || getPaperSize().y )), ft.opts.boundary.y);
                            }

                            var radius = Math.sqrt(Math.pow(cx - ft.o.center.x - ft.o.translate.x, 2) + Math.pow(cy - ft.o.center.y - ft.o.translate.y, 2));

                            if ( scale ) {
                                ft.attrs.scale[axis] = radius / ( ft.o.size[axis] / 2 * ft.opts.distance );

                                if ( mirrored[axis] ) {
                                    ft.attrs.scale[axis] *= -1;
                                }
                            }

                            applyLimits();

                            // Maintain aspect ratio
                            // if ( ft.opts.keepRatio.indexOf('axis' + axis.toUpperCase()) !== -1 ) {
                                // console.log(ft.opts.keepRatio);
                            if ( ft.opts.keepRatio ) {
                                // console.log(axis);
                                keepRatio(axis);
                            } else {
                                ft.attrs.ratio = ft.attrs.scale.x / ft.attrs.scale.y;
                            }

                            if ( ft.attrs.scale.x && ft.attrs.scale.y ) { ft.apply(); }
                            // console.log('391');

                            asyncCallback([ rotate ? 'rotate' : null, scale ? 'scale' : null ]);
                        }
                        var _dragStart = function() {
                            // Offset values
                            ft.o = cloneObj(ft.attrs);

                            // console.log(getPaperSize().x);
                            // console.log(getPaperSize().y);
                            if ( paper._viewBox ) {
                                ft.o.viewBoxRatio = {
                                    x: paper._viewBox[2] / getPaperSize().x,
                                    y: paper._viewBox[3] / getPaperSize().y
                                };
                            }

                            ft.handles[axis].disc.ox = parseInt(this.attr('cx'), 10);
                            ft.handles[axis].disc.oy = parseInt(this.attr('cy'), 10);

                            asyncCallback([ rotate ? 'rotate start' : null, scale ? 'scale start' : null ]);
                        }
                        var _dragEnd = function() {
                            console.log("roate and scale");
                            asyncCallback([ rotate ? 'rotate end'   : null, scale ? 'scale end'   : null ], subject);
                        }

                        // var _dragMove = function(dx, dy){}
                        // var _dragStart = function(){}
                        // var _dragEnd = function(){}

                    ft.handles[axis].disc.attr(ft.opts.discAttrs)
                    ft.handles[axis].disc.drag(_dragMove, _dragStart, _dragEnd);
                });

                // Drag bbox handles
                if ( ft.opts.draw.indexOf('bbox') >= 0 && ( ft.opts.scale.indexOf('bboxCorners') !== -1 || ft.opts.scale.indexOf('bboxSides') !== -1 ) ) {
                    // console.log('show handles/Drag bbox handles');

                    // document.body.style.cursor = cursor || 'auto'

                    ft.handles.bbox.map(function(handle) {

                        var _dragMove = function(dx, dy) {
                            console.log("handles drag move");
                            // viewBox might be scaled
                            if ( ft.o.viewBoxRatio ) {
                                dx *= ft.o.viewBoxRatio.x;
                                dy *= ft.o.viewBoxRatio.y;
                            }

                            var sin, cos, rx, ry, rdx, rdy, mx, my, sx, sy,
                                previous = cloneObj(ft.attrs);

                            sin = ft.o.rotate.sin;
                            cos = ft.o.rotate.cos;

                            // First rotate dx, dy to element alignment
                            rx = dx * cos - dy * sin;
                            ry = dx * sin + dy * cos;

                            rx *= Math.abs(handle.x);
                            ry *= Math.abs(handle.y);

                            // And finally rotate back to canvas alignment
                            rdx = rx *   cos + ry * sin;
                            rdy = rx * - sin + ry * cos;

                            ft.attrs.translate = {
                                x: ft.o.translate.x + rdx / 2,
                                y: ft.o.translate.y + rdy / 2
                            };

                            // Mouse position, relative to element center after translation
                            mx = ft.o.handlePos.cx + dx - ft.attrs.center.x - ft.attrs.translate.x;
                            my = ft.o.handlePos.cy + dy - ft.attrs.center.y - ft.attrs.translate.y;

                            // Position rotated to align with element
                            rx = mx * cos - my * sin;
                            ry = mx * sin + my * cos;

                            // Maintain aspect ratio
                            if ( handle.isCorner && ft.opts.keepRatio.indexOf('bboxCorners') !== -1 ) {
                                var
                                    ratio = ( ft.attrs.size.x * ft.attrs.scale.x ) / ( ft.attrs.size.y * ft.attrs.scale.y ),
                                    tdy = rx * handle.x * ( 1 / ratio ),
                                    tdx = ry * handle.y * ratio
                                    ;

                                if ( tdx > tdy * ratio ) {
                                    rx = tdx * handle.x;
                                } else {
                                    ry = tdy * handle.y;
                                }
                            }

                            // Scale element so that handle is at mouse position
                            sx = rx * 2 * handle.x / ft.o.size.x;
                            sy = ry * 2 * handle.y / ft.o.size.y;

                            ft.attrs.scale = {
                                x: sx || ft.attrs.scale.x,
                                y: sy || ft.attrs.scale.y
                            };
                            // console.log('here');

                            // Check boundaries
                            if ( !isWithinBoundaries().x || !isWithinBoundaries().y ) { ft.attrs = previous; }

                            applyLimits();

                            // Maintain aspect ratio
                            if ( ( handle.isCorner && ft.opts.keepRatio.indexOf('bboxCorners') !== -1 ) || ( !handle.isCorner && ft.opts.keepRatio.indexOf('bboxSides') !== -1 ) ) {
                                keepRatio(handle.axis);

                                var trans = {
                                    x: ( ft.attrs.scale.x - ft.o.scale.x ) * ft.o.size.x * handle.x,
                                    y: ( ft.attrs.scale.y - ft.o.scale.y ) * ft.o.size.y * handle.y
                                };

                                rx =   trans.x * cos + trans.y * sin;
                                ry = - trans.x * sin + trans.y * cos;

                                ft.attrs.translate.x = ft.o.translate.x + rx / 2;
                                ft.attrs.translate.y = ft.o.translate.y + ry / 2;
                            }

                            ft.attrs.ratio = ft.attrs.scale.x / ft.attrs.scale.y;

                            asyncCallback([ 'scale' ]);

                            ft.apply();
                        }

                        var _dragStart = function() {
                            var rotate = ( ( 360 - ft.attrs.rotate ) % 360 ) / 180 * Math.PI,
                                handlePos = {
                                    x: parseInt(handle.element.attr('x'), 10),
                                    y: parseInt(handle.element.attr('y'), 10)
                                };

                            // Offset values
                            ft.o = cloneObj(ft.attrs);

                            ft.o.handlePos = {
                                cx: handlePos.x + ft.opts.size[handle.isCorner ? 'bboxCorners' : 'bboxSides'],
                                cy: handlePos.y + ft.opts.size[handle.isCorner ? 'bboxCorners' : 'bboxSides']
                            };

                            // Pre-compute rotation sin & cos for efficiency
                            ft.o.rotate = {
                                sin: Math.sin(rotate),
                                cos: Math.cos(rotate)
                            };

                            if ( paper._viewBox ) {
                                ft.o.viewBoxRatio = {
                                    x: paper._viewBox[2] / getPaperSize().x,
                                    y: paper._viewBox[3] / getPaperSize().y
                                };
                            }

                            // store current body cursor; @see _dragEnd()
                            ft.opts.bodyCursor = window.getComputedStyle(document.body).cursor;

                            // make body inherit cursor in case pointer strays while dragging
                            document.body.style.cursor = handle.element.attr('cursor');

                            asyncCallback([ 'scale start' ]);
                        }
                        var _dragEnd = function() {

                            // restore initial body cursor
                            document.body.style.cursor = ft.opts.bodyCursor;

                            asyncCallback([ 'scale end' ], subject);
                        }

                        // var _dragMove = function(){}
                        // var _dragStart = function(){}

                        handle.element.drag(_dragMove, _dragStart, _dragEnd);
                    });
                }

                // Drag element and center handle
                var draggables = [];

                if ( ft.opts.drag.indexOf('self') >= 0 && ft.opts.scale.indexOf('self') === -1 && ft.opts.rotate.indexOf('self') === -1 ) {
                    draggables.push(subject);
                    subject.attr('cursor', 'move')
                }

                if ( ft.opts.drag.indexOf('center') >= 0 ) {
                    draggables.push(ft.handles.center.disc);
                    ft.handles.center.disc.attr('cursor', 'move')
                }

                // console.log('draggables');
                // console.log(draggables);

                draggables.map(function(draggable) {

                    // console.log(draggable.drag);

                    var _dragMove = function(dx, dy) {
                        
                        // follow the bigger ratio, according the drag test in Chrome, it seems the speed and distance of drag follow the bigger side of ratio
                        if( paper._viewBox ) {
                            // viewBox might be scaled
                            // console.log("-----------------------------------------------");
                            // console.log('before change' + dx);
                            // console.log('before change' + dy);
                            // console.log("ratio x " + ft.o.viewBoxRatio.x);
                            // console.log("ratio y " + ft.o.viewBoxRatio.y);

                            // tested with the following case and work
                            // viewBox: 0 0 1920 1080 vs paper size 640 x 360
                            // viewBox: 0 0 1920 1080 vs paper size 360 x 640
                            // viewBox: 0 0 1920 1080 vs paper size 500 x 500
                            // viewBox: 0 0 1080 1920 vs paper size 256 x 256
                            if ( ft.o.viewBoxRatio ) {
                                // console.log("here??---------------------------");
                                // by experiment
                                if( getPaperSize().x == getPaperSize().y && paper._viewBox[2] > paper._viewBox[3]) { 
                                    console.log("case 1");
                                    // true for viewBox 1920 1080 or 1080 1920
                                    dx *= ft.o.viewBoxRatio.x / ft.o.viewBoxAspectRatio.x;
                                    dy *= ft.o.viewBoxRatio.y * ft.o.viewBoxAspectRatio.y;

                                } else if( getPaperSize().x < getPaperSize().y && paper._viewBox[2] > paper._viewBox[3]) {
                                    console.log("case 2");
                                    // console.log('getPaperSize().x < getPaperSize().y');
                                    dx *= ft.o.viewBoxRatio.x;
                                    dy *= ft.o.viewBoxRatio.y * ft.o.viewBoxAspectRatioSingle * getPaperSize().y / getPaperSize().x;

                                } else if( getPaperSize().x > getPaperSize().y && paper._viewBox[2] > paper._viewBox[3]) {
                                    console.log("case 3");
                                    // console.log('getPaperSize().x < getPaperSize().y');
                                    dx *= ft.o.viewBoxRatio.x;
                                    dy *= ft.o.viewBoxRatio.y;

                                } else if( getPaperSize().x > getPaperSize().y && paper._viewBox[3] > paper._viewBox[2] ) {
                                    console.log("case 4");
                                    dx *= ft.o.viewBoxRatio.x / ft.o.viewBoxAspectRatioSingle * getPaperSize().x / getPaperSize().y;
                                    dy *= ft.o.viewBoxRatio.y ; //tried 5 for viewBox: 0 0 1080 1920, paper 640 480

                                } else if( getPaperSize().x < getPaperSize().y && paper._viewBox[3] > paper._viewBox[2] ) {
                                    console.log("case 5");
                                    // getPaperSize().x > getPaperSize().y doesn't need any tweaking
                                    dx *= ft.o.viewBoxRatio.x;
                                    dy *= ft.o.viewBoxRatio.y;
                                }
                            }
                        }

                        // console.log('after change' + dx);
                        // console.log('after change' + dy);
                        // dx = dy = 0;
                        // console.log(getPaperSize().x);
                        // console.log(getPaperSize().y);

                        var gridSize = ft.opts.snap.drag;
                        dx = Snap.snapTo(gridSize, dx, 10000);
                        dy = Snap.snapTo(gridSize, dy, 10000);

                        // console.log("ft.attrs.translate x" + ft.attrs.translate.x);
                        ft.attrs.translate.x = ft.o.translate.x + dx;
                        ft.attrs.translate.y = ft.o.translate.y + dy;
                        // console.log(ft.attrs.translate.x);

                        // commented out, not sure what it is for...
                        // var bbox = cloneObj(ft.o.bbox);
                        // console.log("++++++++++++++++++++++++++++");
                        // console.log("bounding box to bound");
                        // console.log(bbox);

                        // bbox.x += dx;
                        // bbox.y += dy;

                        // applyLimits(bbox);

                        // cause floating points and decimals after apply
                        applyLimits(boundaryBBox);

                        asyncCallback([ 'drag' ]);

                        ft.apply();
                    };

                    var _dragStart = function() {
                        console.log("drag to move start");
                        updateViewBoxRatio();
                        
                        // console.log(ft.opts.snap.drag);
                        // Offset values
                        ft.o = cloneObj(ft.attrs);
                        console.log(ft.o);
                        if ( ft.opts.snap.drag ) {
                            // console.log(ft.opts.snap);
                            ft.o.bbox = subject.getBBox();
                        }

                        // viewBox might be scaled
                        // if ( paper._viewBox ) {
                        //     ft.o.viewBoxRatio = {
                        //         x: paper._viewBox[2] / getPaperSize().x,
                        //         y: paper._viewBox[3] / getPaperSize().y
                        //     };
                        // }

                        ft.axes.map(function(axis) {
                            if ( ft.handles[axis] ) {
                                ft.handles[axis].disc.ox = ft.handles[axis].disc.attr('cx');
                                ft.handles[axis].disc.oy = ft.handles[axis].disc.attr('cy');
                            }
                        });

                        asyncCallback([ 'drag start' ]);
                    };

                    var _dragEnd = function() {
                        // console.log("drag to move end");
                        // console.log("-------------------------------------");
                        // console.log(subject);
                        asyncCallback([ 'drag end' ], subject);
                    };

                    // var _dragMove = function(dx, dy){}
                    // var _dragStart = function(){}
                    // var _dragEnd = function(){}

                    draggable.drag(_dragMove, _dragStart, _dragEnd);
                });

                var rotate = ft.opts.rotate.indexOf('self') >= 0,
                    scale  = ft.opts.scale .indexOf('self') >= 0;

                console.log("ft.opts.scale .indexOf('self')");
                console.log(ft.opts.scale .indexOf('self'));
                if ( rotate || scale ) {
                    subject.drag(function(dx, dy, x, y) {
                        if ( rotate ) {
                            var rad = Math.atan2(y - ft.o.center.y - ft.o.translate.y, x - ft.o.center.x - ft.o.translate.x);

                            ft.attrs.rotate = ft.o.rotate + ( rad * 180 / Math.PI ) - ft.o.deg;
                        }

                        var mirrored = {
                            x: ft.o.scale.x < 0,
                            y: ft.o.scale.y < 0
                        };

                        if ( scale ) {
                            var radius = Math.sqrt(Math.pow(x - ft.o.center.x - ft.o.translate.x, 2) + Math.pow(y - ft.o.center.y - ft.o.translate.y, 2));

                            ft.attrs.scale.x = ft.attrs.scale.y = ( mirrored.x ? -1 : 1 ) * ft.o.scale.x + ( radius - ft.o.radius ) / ( ft.o.size.x / 2 );

                            if ( mirrored.x ) { ft.attrs.scale.x *= -1; }
                            if ( mirrored.y ) { ft.attrs.scale.y *= -1; }
                        }

                        applyLimits();

                        ft.apply();

                        asyncCallback([ rotate ? 'rotate' : null, scale ? 'scale' : null ]);
                    }, function(x, y) {
                        // Offset values
                        ft.o = cloneObj(ft.attrs);

                        ft.o.deg = Math.atan2(y - ft.o.center.y - ft.o.translate.y, x - ft.o.center.x - ft.o.translate.x) * 180 / Math.PI;

                        ft.o.radius = Math.sqrt(Math.pow(x - ft.o.center.x - ft.o.translate.x, 2) + Math.pow(y - ft.o.center.y - ft.o.translate.y, 2));

                        console.log(ft.o);

                        // viewBox might be scaled
                        if ( paper._viewBox ) {
                            ft.o.viewBoxRatio = {
                                x: paper._viewBox[2] / getPaperSize().x,
                                y: paper._viewBox[3] / getPaperSize().y
                            };
                        }

                        asyncCallback([ rotate ? 'rotate start' : null, scale ? 'scale start' : null ]);
                    }, function() {
                        asyncCallback([ rotate ? 'rotate end'   : null, scale ? 'scale end'   : null ], subject);
                    });
                }

                ft.updateHandles();

                return ft;
            };
            // ft.showHandles = function(){}

            /**
             * Remove handles
             */

            /*

            */
            ft.hideHandles = function(opts) {
                console.log("hide handles");
                var opts = opts || {}

                if ( opts.undrag === undefined ) {
                    opts.undrag = true;
                }

                if ( opts.undrag ) {
                    ft.items.map(function(item) {
                        item.el.undrag();
                    });
                }

                if ( ft.handles.center ) {
                    ft.handles.center.disc.remove();

                    ft.handles.center = null;
                }

                [ 'x', 'y' ].map(function(axis) {
                    if ( ft.handles[axis] ) {
                        ft.handles[axis].disc.remove();
                        ft.handles[axis].line.remove();

                        ft.handles[axis] = null;
                    }
                });

                if ( ft.bbox ) {
                    ft.bbox.remove();

                    ft.bbox = null;

                    if ( ft.handles.bbox ) {
                        ft.handles.bbox.map(function(handle) {
                            handle.element.remove();
                        });

                        ft.handles.bbox = null;
                    }
                }

                if ( ft.circle ) {
                    ft.circle.remove();

                    ft.circle = null;
                }

                subject.attr('cursor', '');

                return ft;
            };

            // Override defaults
            ft.setOpts = function(options, callback) {
                console.log("set options");
                if ( callback !== undefined ) {
                    ft.callback = typeof callback === 'function' ? callback : false;
                    // console.log(ft.callback);
                }

                var i, j;

                // var defaultTmp = console.log;
                

                for ( i in options ) {
                    // if( i === 'boundary' ) {
                    //     console.log = defaultTmp;
                    // } else {
                    //     console.log = function(){}
                    // }

                    // console.log(i);

                    if ( options[i] && options[i].constructor === Object ) {
                        if(ft.opts[i] === false){
                            // console.log(ft.opts[i]);
                            ft.opts[i] = {};
                        }
                        for ( j in options[i] ) {
                            // console.log(options[i]);
                            // console.log(j);

                            // console.log(options[i].hasOwnProperty(j));

                            if ( options[i].hasOwnProperty(j) ) {
                                ft.opts[i][j] = options[i][j];
                            }
                        }
                    } else {
                        // console.log(i);
                        ft.opts[i] = options[i];
                    }
                }

                if ( ft.opts.animate   === true ) { ft.opts.animate   = { delay:   700, easing: 'linear' }; }
                if ( ft.opts.drag      === true ) { ft.opts.drag      = [ 'center', 'self' ]; }
                if ( ft.opts.keepRatio === true ) { ft.opts.keepRatio = [ 'bboxCorners', 'bboxSides' ]; }
                if ( ft.opts.rotate    === true ) { ft.opts.rotate    = [ 'axisX', 'axisY' ]; }
                if ( ft.opts.scale     === true ) { ft.opts.scale     = [ 'axisX', 'axisY', 'bboxCorners', 'bboxSides' ]; }

                [ 'drag', 'draw', 'keepRatio', 'rotate', 'scale' ].map(function(option) {
                    if ( ft.opts[option] === false ) {
                        ft.opts[option] = [];
                    }
                });

                ft.axes = [];

                if ( ft.opts.rotate.indexOf('axisX') >= 0 || ft.opts.scale.indexOf('axisX') >= 0 ) { ft.axes.push('x'); }
                if ( ft.opts.rotate.indexOf('axisY') >= 0 || ft.opts.scale.indexOf('axisY') >= 0 ) { ft.axes.push('y'); }

                [ 'drag', 'rotate', 'scale' ].map(function(option) {
                    if ( !ft.opts.snapDist[option] ) {
                        ft.opts.snapDist[option] = ft.opts.snap[option];
                    }
                });

                // Force numbers
                ft.opts.range = {
                    rotate: [ parseFloat(ft.opts.range.rotate[0]), parseFloat(ft.opts.range.rotate[1]) ],
                    scale:  [ parseFloat(ft.opts.range.scale[0]),  parseFloat(ft.opts.range.scale[1])  ]
                };


                ft.opts.snap = {
                    drag:   parseFloat(ft.opts.snap.drag),
                    rotate: parseFloat(ft.opts.snap.rotate),
                    scale:  parseFloat(ft.opts.snap.scale)
                };

                ft.opts.snapDist = {
                    drag:   parseFloat(ft.opts.snapDist.drag),
                    rotate: parseFloat(ft.opts.snapDist.rotate),
                    scale:  parseFloat(ft.opts.snapDist.scale)
                };

                if ( typeof ft.opts.size === 'string' ) {
                    ft.opts.size = parseFloat(ft.opts.size);
                }

                if ( !isNaN(ft.opts.size) ) {
                    ft.opts.size = {
                        axes:        ft.opts.size,
                        bboxCorners: ft.opts.size,
                        bboxSides:   ft.opts.size,
                        center:      ft.opts.size
                    };
                }

                if ( typeof ft.opts.distance === 'string' ) {
                    ft.opts.distance = parseFloat(ft.opts.distance);
                }

                console.log(ft.opts);


                ft.showHandles();

                asyncCallback([ 'init' ]);

                return ft;
            };

            ft.setOpts(options, callback);

            /**
             * Apply transformations, optionally update attributes manually
             */
            ft.apply = function( state ) {
                console.log('ft apply');
                console.log(state);

                if( state === 'limit') {
                    console.log('apply limits too');
                    applyLimits();
                }

                // console.log("apply transformation " + state);
                ft.items.map(function(item, i) {
                    // Take offset values into account
                    var
                        center = {
                            x: ft.attrs.objCenter.x || ft.attrs.center.x + ft.offset.translate.x,
                            y: ft.attrs.objCenter.y || ft.attrs.center.y + ft.offset.translate.y
                        },
                        rotate    = ft.attrs.rotate - ft.offset.rotate,
                        scale     = {
                            x: ft.attrs.scale.x / ft.offset.scale.x,
                            y: ft.attrs.scale.y / ft.offset.scale.y
                        },
                        translate = {
                            x: ft.attrs.translate.x - ft.offset.translate.x,
                            y: ft.attrs.translate.y - ft.offset.translate.y
                        };

                        // console.log(ft.attrs.scale.x);
                        // console.log(ft.offset.scale.x);
                        // console.log(ft.offset.scale);
                        // console.log(scale);

                        /*
                        console.log('ft.attrs.translate.x ' + ft.attrs.translate.x)
                        console.log('ft.attrs.translate.y ' + ft.attrs.translate.y)
                        console.log('ft.offset.translate.x ' + ft.offset.translate.x)
                        console.log('ft.offset.translate.y ' + ft.offset.translate.y)
                        console.log('ft.attrs.objCenter.x || ft.attrs.center.x + ft.offset.translate.x ' + ft.attrs.objCenter.x || ft.attrs.center.x + ft.offset.translate.x)
                        console.log("ft.attrs.center.x + ft.offset.translate.x " + ft.attrs.center.x + ft.offset.translate.x);
                        console.log("ft.attrs.objCenter.x " + ft.attrs.objCenter.x);
                        console.log('ft.attrs.center.x ' + ft.attrs.center.x); // get from bbox
                        console.log('ft.offset.translate.x ' + ft.offset.translate.x);
                        console.log("center.x " + center.x);
                        */
                    if ( ft.opts.animate ) {
                        /*asyncCallback([ 'animate start' ]);

                        item.el.animate(
                            { transform: [
                                'R' + rotate, center.x, center.y,
                                'S' + scale.x, scale.y, center.x, center.y,
                                'T' + translate.x, translate.y
                            ].join(',')},
                            ft.opts.animate.delay,
                            ft.opts.animate.easing,
                            function() {
                                asyncCallback([ 'animate end' ]);

                                ft.updateHandles();
                            }
                        );*/
                    } else {
                        // rotate = 45
                        // center.x = center.y = 0
                        // scale.x = scale.y = 2
                        // translate.x = translate.y = 1
                        // console.log(typeof ft.attrs.objCenter.x);
                        // console.log("center.x " + center.x);
                        // console.log(item.el)

                        item.el.transform([
                            'R' + rotate, center.x, center.y,
                            'S' + scale.x, scale.y, center.x, center.y,
                            'T' + translate.x, translate.y
                        ].join(','));

                        // console.log([
                        //     'R' + rotate, center.x, center.y,
                        //     'S' + scale.x, scale.y, center.x, center.y,
                        //     'T' + translate.x, translate.y
                        // ].join(','));



                        // eleMatrix = new Snap.Matrix();
                        // eleMatrix.translate(translate.x, translate.y);
                        // eleMatrix.scale(scale.x, scale.y, 0, 0);
                        // eleMatrix.rotate(rotate);
                        // item.el.transform(eleMatrix);
                        
                        // console.log(eleMatrix.toTransformString());
                        
                        // console.log(translate.x);
                        // console.log([
                        //     'R' + 1, 0, 0,
                        //     'S' + 3.5, 3.5, 0, 0,
                        //     'T' + translate.x, translate.y
                        // ].join(','));
                        // t313.55,248.82s3.5,3.5,0,0r1,0,0
                        // console.log(item.el);
                        // console.log(Snap("#svg-canvas").select("#table_1418785047328 .transform"));
                        // console.log(Snap("#svg-canvas").select("#table_1418785047328 .transform") == item.el);
                        // item.el.transform("R1,0,0,S3.5,3.5,0,0,T313.55,248.82")
                        // Snap("#svg-canvas").select("#table_1418785047328 .transform").transform("t313.55,248.82s3.5,3.5,0,0r1,0,0")

                        ft.updateHandles();

                        if( state != 'init')
                            asyncCallback([ 'apply' ], item.el);

                    }
                });

                return ft;
            };
            // ft.apply = function() {}

            /**
             * Clean exit
             */
            ft.unplug = function() {
                var attrs = ft.attrs;

                ft.hideHandles();

                // Goodbye
                delete subject.freeTransform;

                return attrs;
            };

            // Store attributes for each item
            function scan(subject) {
                console.log("scan object");
                // console.log([ subject ]);

                ( subject.type === 'set' ? subject.items : [ subject ] ).map(function(item) {
                    // console.log(item.type);
                    if ( item.type === 'set' ) {
                        scan(item);
                    } else {
                        ft.items.push({
                            el: item,
                            attrs: {
                                rotate:    ft.attrs.rotate,
                                scale:     { x: ft.attrs.scale.x, y: ft.attrs.scale.y },
                                translate: { x: ft.attrs.translate.x, y: ft.attrs.translate.y }
                            },
                            transformString: item.transform().toString()
                        });
                        // console.log(item);
                        // console.log(ft.items);
                    }
                });
            }

            scan(subject);

            // Get the current transform values for each item
            ft.items.map(function(item, i) {
                console.log("get transform values for each item/run once");
                if ( item.el._ && item.el._.transform && typeof item.el._.transform === 'object' ) {
                    item.el._.transform.map(function(transform) {
                        if ( transform[0] ) {
                            switch ( transform[0].toUpperCase() ) {
                                case 'T':
                                    ft.items[i].attrs.translate.x += transform[1];
                                    ft.items[i].attrs.translate.y += transform[2];
                                    // console.log('ft.items[i].attrs.translate.x ' + ft.items[i].attrs.translate.x);
                                    // console.log('ft.items[i].attrs.translate.y ' + ft.items[i].attrs.translate.y);

                                    break;
                                case 'S':
                                    ft.items[i].attrs.scale.x *= transform[1];
                                    ft.items[i].attrs.scale.y *= transform[2];

                                    break;
                                case 'R':
                                    ft.items[i].attrs.rotate += transform[1];

                                    break;
                            }
                        }
                    });
                }
            });

            // If subject is not of type set, the first item _is_ the subject
            if ( subject.type !== 'set' ) {
                ft.attrs.rotate    = ft.items[0].attrs.rotate;
                ft.attrs.scale     = ft.items[0].attrs.scale;
                ft.attrs.translate = ft.items[0].attrs.translate;

                ft.items[0].attrs = {
                    rotate:    0,
                    scale:     { x: 1, y: 1 },
                    translate: { x: 0, y: 0 }
                };

                ft.items[0].transformString = '';
            }

            ft.attrs.ratio = ft.attrs.scale.x / ft.attrs.scale.y;

            /**
             * Get rotated bounding box
             */
            function getBBox() {
                console.log("get BBOX");
                var rad = {
                    x: ( ft.attrs.rotate      ) * Math.PI / 180,
                    y: ( ft.attrs.rotate + 90 ) * Math.PI / 180
                };

                var radius = {
                    x: ft.attrs.size.x / 2 * ft.attrs.scale.x,
                    y: ft.attrs.size.y / 2 * ft.attrs.scale.y
                };

                var
                    corners = [],
                    signs   = [ { x: -1, y: -1 }, { x: 1, y: -1 }, { x: 1, y: 1 }, { x: -1, y: 1 } ]
                    ;

                signs.map(function(sign) {
                    corners.push({
                        x: ( ft.attrs.center.x + ft.attrs.translate.x + sign.x * radius.x * Math.cos(rad.x) ) + sign.y * radius.y * Math.cos(rad.y),
                        y: ( ft.attrs.center.y + ft.attrs.translate.y + sign.x * radius.x * Math.sin(rad.x) ) + sign.y * radius.y * Math.sin(rad.y)
                    });
                });

                return corners;
            }

            /**
             * Get dimension of the paper
             */
            function getPaperSize() {
                // console.log("get paper size");

                // TODO: check and remove. Old Raphael shims here
                // var match = {
                //     x: /^([0-9]+)%$/.exec(paper.attr('width')),
                //     y: /^([0-9]+)%$/.exec(paper.attr('height'))
                // };
                //
                //
                // return {
                //     x: match.x ? paper.canvas.clientWidth  || paper.canvas.parentNode.clientWidth  * parseInt(match.x[1], 10) * 0.01 : paper.canvas.clientWidth  || paper.width,
                //     y: match.y ? paper.canvas.clientHeight || paper.canvas.parentNode.clientHeight * parseInt(match.y[1], 10) * 0.01 : paper.canvas.clientHeight || paper.height
                // };
                //

                /* 
                paper size should be width and height of outer most SVG wrapper, if wrapper does not specified width and height, the default is W 300px * H 150px in chrome, probably same in other browser based on W3C, it is true also for either only one side is specified
                so, the width and height determination should be Snap("#wrapper") attribute width and height then fallback to 300 and 150 respectively
                but
                if viewBox is present but no width and height is set, the width and height will be scaled automatically to 100% each
                */


                // return {
                //     /*x: parseInt(paper.node.clientWidth),
                //     y: parseInt(paper.node.clientHeight)*/
                //     // x: subject.paper.attr("width") || 300,
                //     // y: subject.paper.attr("height") || 150,
                //     x: parseInt(paper.node.clientWidth),
                //     y: parseInt(paper.node.clientHeight),
                //     w: parseInt(paper.node.clientWidth),
                //     h: parseInt(paper.node.clientHeight)
                // }

                // firefox have bug in getting SVG clientWidth and ClientHeight (return 0, result in a series of calculation error)
                // console.log(paper.node.getBBox().width);
                // console.log(paper.node.getBBox().height);

                return {
                    /*x: parseInt(paper.node.clientWidth),
                    y: parseInt(paper.node.clientHeight)*/
                    // x: subject.paper.attr("width") || 300,
                    // y: subject.paper.attr("height") || 150,
                    x: parseInt(paper.node.clientWidth) || parseInt(paper.node.getBBox().width),
                    y: parseInt(paper.node.clientHeight) || parseInt(paper.node.getBBox().height),
                    w: parseInt(paper.node.clientWidth) || parseInt(paper.node.getBBox().width),
                    h: parseInt(paper.node.clientHeight) || parseInt(paper.node.getBBox().height)
                }
            }

            /**
             * Apply limits
             */
            function applyLimits(bbox) {
                console.log("apply limits");
                updateViewBoxRatio();

                // console.log(bbox);
                // console.log(ft.opts.snap.drag);
                // Snap to grid
                if ( bbox && ft.opts.snap.drag ) {
                    // console.log("snap to grid");
                    // var
                    //     x    = bbox.x,
                    //     y    = bbox.y,
                    //     dist = { x: 0, y: 0 },
                    //     snap = { x: 0, y: 0 }
                    //     ;

                    // // console.log([ 0, 1 ]);
                    // [ 0, 1 ].map(function() {
                    //     console.log("inside 0 1");
                    //     // Top and left sides first
                    //     dist.x = x - Math.round(x / ft.opts.snap.drag) * ft.opts.snap.drag;
                    //     dist.y = y - Math.round(y / ft.opts.snap.drag) * ft.opts.snap.drag;

                    //     if ( Math.abs(dist.x) <= ft.opts.snapDist.drag ) { snap.x = dist.x; }
                    //     if ( Math.abs(dist.y) <= ft.opts.snapDist.drag ) { snap.y = dist.y; }

                    //     // Repeat for bottom and right sides
                    //     x += bbox.width  - snap.x;
                    //     y += bbox.height - snap.y;
                    // });
                    // ft.attrs.translate.x -= snap.x;
                    // ft.attrs.translate.y -= snap.y;
                    // console.log(dx);
                    // var gridSize = 2;
                    // dx = Snap.snapTo(gridSize, dx, 100);
                    // dy = Snap.snapTo(gridSize, dy, 100);

                    // ft.attrs.translate.x = ft.attrs.translate.x + dx;
                    // ft.attrs.translate.y += dy;
                }

                // Keep center within boundaries
                // console.log(ft.opts.boundary);
                if ( ft.opts.boundary ) {
                    console.log('apply boundary');
                    var b = ft.opts.boundary;
                    // console.log(b);
                    // console.log(b.width);
                    // console.log(getPaperSize().x);
                    // logical not good here for b.width is undefined before and it is not a good idea to update a reference
                    // TODO: use another variable for width and height here for calculation, avoid reference update, hard to trace error

                    // b.width  = ( b.width  || getPaperSize().x );
                    // b.height = ( b.height || getPaperSize().y );

                    // // tempoary measure before a better variable above
                    // if( paper._viewBox) {
                    //     // console.log(ft.attrs.viewBoxRatio.x);
                    //     b.width = getPaperSize().x * ft.attrs.viewBoxRatio.x;
                    //     b.height = getPaperSize().y * ft.attrs.viewBoxRatio.y;
                    // }

                    if( paper._viewBox && ( ft.opts.boundary.width === null && ft.opts.boundary.height === null )) {
                        // console.log(ft.attrs.viewBoxRatio.x);
                        b.width = getPaperSize().x * ft.attrs.viewBoxRatio.x;
                        b.height = getPaperSize().y * ft.attrs.viewBoxRatio.y;
                    } 
                    else {
                        b.width  = ( b.width  || getPaperSize().x );
                        b.height = ( b.height || getPaperSize().y );
                    }

                    

                    console.log(b.width);
                    console.log(getPaperSize());
                    console.log(paper);
                    // tempoary measure before a better variable above
                    // if( paper._viewBox) {
                    //     // console.log(ft.attrs.viewBoxRatio.x);
                    //     b.width = getPaperSize().x * ft.attrs.viewBoxRatio.x;
                    //     b.height = getPaperSize().y * ft.attrs.viewBoxRatio.y;
                    // }
                        // console.log("//////////////////////////////////////////////////");
                    // console.log(b.width);

                    // left edge
                    if ( ft.attrs.center.x + ft.attrs.translate.x < b.x            ) { 
                    //     console.log("//////////////////////////////////////////////////");
                    //     console.log('ft.attrs.translate.x += b.x');
                    //     console.log('-( ft.attrs.center.x + ft.attrs.translate.x )');
                    //     console.log(ft.attrs.translate.x += b.x);
                    //     console.log(( ft.attrs.center.x + ft.attrs.translate.x ));
                    //     console.log("= " + (ft.attrs.translate.x += b.x - ( ft.attrs.center.x + ft.attrs.translate.x )));
                        ft.attrs.translate.x += b.x - ( ft.attrs.center.x + ft.attrs.translate.x ); }

                    // top edge
                    if ( ft.attrs.center.y + ft.attrs.translate.y < b.y            ) { 
                        ft.attrs.translate.y += b.y -            ( ft.attrs.center.y + ft.attrs.translate.y ); }

                    // right edge
                    if ( ft.attrs.center.x + ft.attrs.translate.x > b.x + b.width  ) { 
                        console.log("b.width " + b.width);
                        ft.attrs.translate.x += b.x + b.width  - ( ft.attrs.translate.x + ft.attrs.center.x ); 
                        // ft.attrs.translate.x += b.x + b.width  - ( ft.attrs.center.x + ft.attrs.translate.x ); 
                    }

                    // bottom edge
                    /*console.log('b.y ' + b.y);
                    console.log('b.x ' + b.x);
                    console.log('b.height ' + b.height);*/
                    if ( ft.attrs.center.y + ft.attrs.translate.y > b.y + b.height ) { 
                        ft.attrs.translate.y += b.y + b.height - ( ft.attrs.center.y + ft.attrs.translate.y ); 
                    }
                        // console.log("ft.attrs.translate.y " + ft.attrs.translate.y);
                   /*     console.log("b.y + b.height " + (b.y + b.height));
                        console.log("ft.attrs.translate.x " + ft.attrs.translate.x);

                    // even if not using center, the offset is already their
                    if ( ft.attrs.translate.y > b.y + b.height ) { 
                        ft.attrs.translate.y += b.y + b.height - ( ft.attrs.translate.y ); 
                    }*/
                }

                // Snap to angle, rotate with increments
                dist = Math.abs(ft.attrs.rotate % ft.opts.snap.rotate);
                dist = Math.min(dist, ft.opts.snap.rotate - dist);

                if ( dist < ft.opts.snapDist.rotate ) {
                    ft.attrs.rotate = Math.round(ft.attrs.rotate / ft.opts.snap.rotate) * ft.opts.snap.rotate;
                }

                // Snap to scale, scale with increments
                dist = {
                    x: Math.abs(( ft.attrs.scale.x * ft.attrs.size.x ) % ft.opts.snap.scale),
                    y: Math.abs(( ft.attrs.scale.y * ft.attrs.size.x ) % ft.opts.snap.scale)
                };

                dist = {
                    x: Math.min(dist.x, ft.opts.snap.scale - dist.x),
                    y: Math.min(dist.y, ft.opts.snap.scale - dist.y)
                };

                // cause decimal.....
                // if ( dist.x < ft.opts.snapDist.scale ) {
                //     ft.attrs.scale.x = Math.round(ft.attrs.scale.x * ft.attrs.size.x / ft.opts.snap.scale) * ft.opts.snap.scale / ft.attrs.size.x;
                // }

                // if ( dist.y < ft.opts.snapDist.scale ) {
                //     ft.attrs.scale.y = Math.round(ft.attrs.scale.y * ft.attrs.size.y / ft.opts.snap.scale) * ft.opts.snap.scale / ft.attrs.size.y;
                // }

                // Limit range of rotation
                if ( ft.opts.range.rotate ) {
                    var deg = ( 360 + ft.attrs.rotate ) % 360;

                    if ( deg > 180 ) { deg -= 360; }

                    if ( deg < ft.opts.range.rotate[0] ) { ft.attrs.rotate += ft.opts.range.rotate[0] - deg; }
                    if ( deg > ft.opts.range.rotate[1] ) { ft.attrs.rotate += ft.opts.range.rotate[1] - deg; }
                }

                // Limit scale
                if ( ft.opts.range.scale ) {
                    // console.log('limit scale');
                    if ( ft.attrs.scale.x * ft.attrs.size.x < ft.opts.range.scale[0] ) {
                        // console.log('1');
                        ft.attrs.scale.x = ft.opts.range.scale[0] / ft.attrs.size.x;
                    }

                    if ( ft.attrs.scale.y * ft.attrs.size.y < ft.opts.range.scale[0] ) {
                        // console.log('2');
                        ft.attrs.scale.y = ft.opts.range.scale[0] / ft.attrs.size.y;
                    }

                    if ( ft.attrs.scale.x * ft.attrs.size.x > ft.opts.range.scale[1] ) {
                        // console.log('3');
                        // console.log(ft.attrs.scale.x);
                        // console.log(ft.attrs.size.x);
                        // console.log(ft.opts.range.scale[1]);
                        ft.attrs.scale.x = ft.opts.range.scale[1] / ft.attrs.size.x;
                    }

                    if ( ft.attrs.scale.y * ft.attrs.size.y > ft.opts.range.scale[1] ) {
                        // console.log('4');
                        ft.attrs.scale.y = ft.opts.range.scale[1] / ft.attrs.size.y;
                    }

                    // console.log(ft.attrs.scale.x);
                }
            }

            function isWithinBoundaries() {
                console.log("is Within Boundaries?");
                return {
                    x: ft.attrs.scale.x * ft.attrs.size.x >= ft.opts.range.scale[0] && ft.attrs.scale.x * ft.attrs.size.x <= ft.opts.range.scale[1],
                    y: ft.attrs.scale.y * ft.attrs.size.y >= ft.opts.range.scale[0] && ft.attrs.scale.y * ft.attrs.size.y <= ft.opts.range.scale[1]
                };
            }

            function keepRatio(axis) {
                console.log("keep ratio");
                if ( axis === 'x' ) {
                    ft.attrs.scale.y = ft.attrs.scale.x / ft.attrs.ratio;
                } else {
                    ft.attrs.scale.x = ft.attrs.scale.y * ft.attrs.ratio;
                }
            }

            /**
             * Recursive copy of object
             */
            function cloneObj(obj) {
                // console.log("clone obj");
                var i, clone = {};

                for ( i in obj ) {
                    clone[i] = typeof obj[i] === 'object' ? cloneObj(obj[i]) : obj[i];
                }

                return clone;
            }

            var timeout = false;

            /**
             * Call callback asynchronously for better performance
             */
            function asyncCallback(e, ele) {
                // console.log(arguments);
                if ( ft.callback ) {
                console.log("async callback");
                    // Remove empty values
                    var events = [];

                    e.map(function(e, i) { if ( e ) { events.push(e); } });

                    clearTimeout(timeout);

                    timeout=setTimeout(function() { if ( ft.callback ) { 
                        // console.log("+++++++++++++++++++++++++");
                        // console.log(e);
                        // console.log(events);
                        // console.log(args);
                        // console.log(arguments);
                        // console.log(ft);
                        // console.log(ft.callback.apply( this, [ft, events, ele] ));
                        ft.callback.apply( this, [ft, events, ele] );
                        // ft.callback(ft, events); 
                    } }, 1);
                }
            }

            ft.apply('init'); // init default offset
            ft.updateHandles();


            // Enable method chaining
            return ft;
        };

        Snap.freeTransform = freeTransform

    })
}));
