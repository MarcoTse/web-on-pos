// reference: http://svg.dabbles.info/snaptut-freetransform-vb3.js
(function() {
    var lineAttributes = { stroke: 'red', strokeWidth: 2, strokeDasharray: "5,5" };

    Snap.plugin( function( Snap, Element, Paper, global ) {
        var ftOption = {
            handleFill: "red",
            rotateHandleFill: "green",
            handleStrokeDash: "5,5",
            handleStrokeWidth: "2",
            handleLength: "75",
            handleRadius: "20",
            handleLineWidth: 2,
            handleOpacity: 0.7,
            handleClass: ['-handle-box'],

            // plan to make it more generic
            updateCallback : function( fn ) {
                if( fn.length === 1 ) {
                    console.log( "custom updateCallback" );
                    return fn.apply( this, arguments );
                }

                return function( element ) {
                    // console.log(element);
                    var updatedMatrix = element.attr("transform").totalMatrix.split();
                    element.parent().data( "scale_X", updatedMatrix.scalex );
                    element.parent().data( "scale_Y", updatedMatrix.scaley );
                    element.parent().data( "angle_A", updatedMatrix.rotate );
                    element.parent().data( "angle_B", 0 );
                    element.parent().data( "pos_X", updatedMatrix.dx );
                    element.parent().data( "pos_Y", updatedMatrix.dy );
                    // console.log(updatedMatrix);

                    var updatedData = {};
                    for( var key in element.parent().data() ) {
                        if( key != "id" ) {
                            updatedData[key] = element.parent().data(key);
                        }
                    }
                    // console.log(scope.RMSTable);
                    // console.log(element.parent().data() );
                    // console.log("updating");
                    // RMS.updateDataObject( updatedData );
                }.apply( this, arguments );
            },
            // tempoary writing method, improve it later
            // callBack : function( element ) {
            //     var baseEle = RMS.scene.select( "#layoutScene_" + RMS.currentSceneNo );
            //     detectBoundary( baseEle, element );
            // },
            customMove : function( fn ){
                console.log(this);
                if( fn.length === 5 ) {
                    // var test = Snap._.invTransformPoint;
                    // console.log( test );
                    return fn.apply( this, arguments );
                }

                return function elementDragMove( mainEl, dx, dy, x, y ) {
                    // console.log(arguments);
                    var dragHandle = this;
                    // console.log(dragHandle);
                    // console.log(this.data());
                    var gridSize = 15;
                    dx = Snap.snapTo(gridSize, dx, 100000000);
                    dy = Snap.snapTo(gridSize, dy, 100000000);

                    // console.log(this); // circle element, the one that adding drag event
                    var ip = invTransformPoint( this, dx, dy );
                    this.parent().selectAll('circle').forEach( function( el, i ) {
                        el.attr({ cx: +el.data('ocx') + ip.tx, cy: +el.data('ocy') + ip.ty });
                    } );
                    mainEl.data("tx", mainEl.data("otx") + +ip.tx);
                    mainEl.data("ty", mainEl.data("oty") + +ip.ty);

                    mainEl.ftUpdateTransform();
                    mainEl.ftDrawJoinLine( dragHandle );

                    // console.log("moving");
                    // ftOption.callBack( mainEl );

                    // this.data( "transform", mainEl.attr("transform") );

                    // console.log(mainEl.attr( "transform" ));
                    // console.log(mainEl.data());
                }.apply(this, arguments);
            },

            customDragEnd : function( fn ){
                // console.log(this);
                if( fn.length === 5 ) {
                    // var test = Snap._.invTransformPoint;
                    // console.log( test );
                    return fn.apply( this, arguments );
                }

                return function( mainEl, dx, dy, x, y ) {

                }
            }
        };
        // function test(){
        //     console.log('test');
        //     console.log(ftOption);
        // }

        // test();

        Element.prototype.ftCreateHandles = function( customOptions ) {
            // console.log(ftOption);
            ftOption = checkOption( customOptions, ftOption );
            // console.log(ftOption);

            this.ftInit();
            var freetransEl = this;
            // console.log(freetransEl);
            var bb = freetransEl.getBBox(0);
            console.log(bb);
            var rotateDragger = this.paper.circle(bb.cx + bb.width/2 + ftOption.handleLength, bb.cy, ftOption.handleRadius ).attr({ 
                fill: ftOption.rotateHandleFill,
                opacity: ftOption.handleOpacity
            });
            var translateDragger = this.paper.circle(bb.cx, bb.cy, bb.width/2 ).attr({ 
            // var translateDragger = this.paper.circle(bb.cx, bb.cy, ftOption.handleRadius ).attr({ 
                fill: ftOption.handleFill,
                opacity: ftOption.handleOpacity
            });

            var joinLine = freetransEl.ftDrawJoinLine( rotateDragger );
            var handlesGroup = this.paper.g( joinLine, rotateDragger, translateDragger ).attr({
                class : "activeHandleGroup" 
            });

            freetransEl.data( "handlesGroup", handlesGroup );
            freetransEl.data( "joinLine", joinLine);

            freetransEl.data( "scaleFactor", calcDistance( bb.cx, bb.cy, rotateDragger.attr('cx'), rotateDragger.attr('cy') ) );

            // translateDragger.drag(  elementDragMove.bind(  translateDragger, freetransEl ), 
            translateDragger.drag(  ftOption.customMove.bind(  translateDragger, freetransEl ), 
                        elementDragStart.bind( translateDragger, freetransEl ),
                        ftOption.customDragEnd.bind( translateDragger, freetransEl ) );
                        // elementDragEnd.bind( translateDragger, freetransEl ) );

            freetransEl.undblclick();
            freetransEl.data("dblclick", freetransEl.dblclick( function() {  this.ftRemoveHandles() } ) );

            rotateDragger.drag( 
                dragHandleRotateMove.bind( rotateDragger, freetransEl ), 
                dragHandleRotateStart.bind( rotateDragger, freetransEl  ),
                dragHandleRotateEnd.bind( rotateDragger, freetransEl  ) 
            );
            
            rotateDragger.mouseup(
                function(){
                    console.log("rotate dragger mouse up");
                }
            );

            freetransEl.ftStoreInitialTransformMatrix();

            freetransEl.ftHighlightBB();

            // experimental
            // if parent is a group, put the transformation here instead in global space
            // if there is no group, create one (for later expansion) so that it is generic enough to handle different situation

            // reason for this: because when it scale, there is no way to update back the information and not able to obtain current selected object
            // a group is easier to handle or select using class name
            console.log(this.parent().type);
            return this;
        };

        Element.prototype.ftInit = function() {
            this.data("angle", 0);
            this.data("scale", 1);
            this.data("tx", 0);
            this.data("ty", 0);
            return this;
        };

        Element.prototype.ftCleanUp = function() {
            var myClosureEl = this;
            var myData = ["angle", "scale", "scaleFactor", "tx", "ty", "otx", "oty", "bb", "bbT", "initialTransformMatrix", "handlesGroup", "joinLine"];
            myData.forEach( function( el ) { myClosureEl.removeData([el]) });
            return this;
        };

        Element.prototype.ftStoreStartCenter = function() {
            this.data('ocx', this.attr('cx') );
            this.data('ocy', this.attr('cy') );
            return this;
        }
        
        Element.prototype.ftStoreInitialTransformMatrix = function() {
            this.data('initialTransformMatrix', this.transform().localMatrix );
            return this;
        };

        Element.prototype.ftGetInitialTransformMatrix = function() {
            // console.log('ftGetInitialTransformMatrix' + this.data('initialTransformMatrix').toTransformString() );
            return this.data('initialTransformMatrix');
        };

        Element.prototype.ftRemoveHandles = function() {
            try{
                this.undblclick();
                this.data( "handlesGroup").remove();
                this.data( "bbT" ) && this.data("bbT").remove();
                this.data( "bb" ) && this.data("bb").remove();
                this.dblclick( function() { this.ftCreateHandles() } ) ;
                this.ftCleanUp();
            }catch(e){
                // statements
                // console.log(e);
            }
            
            return this;
        };

        Element.prototype.ftRemoveBB = function() {
            try{
                this.data( "bbT" ) && this.data("bbT").remove();
                this.data( "bb" ) && this.data("bb").remove();
            }catch(e){
                // statements
                console.log(e);
            }
            
            return this;
        };

        Element.prototype.ftDrawJoinLine = function( handle ) { // note, handle could be either dragger or rotater
            var lineAttributes = { stroke: ftOption.handleFill, strokeWidth: ftOption.handleStrokeWidth, strokeDasharray: ftOption.handleStrokeDash };
            var rotateHandle = handle.parent()[1];
            var dragHandle = handle.parent()[2];

            var thisBB = this.getBBox(0);

            if( this.data("joinLine") ) {
                this.data("joinLine").attr({ x1: dragHandle.attr('cx'), y1: dragHandle.attr('cy'), x2: rotateHandle.attr('cx'), y2: rotateHandle.attr('cy') });
            } else {
                return this.paper.line( thisBB.cx, thisBB.cy, handle.attr('cx'), handle.attr('cy') ).attr( lineAttributes );
            };

            return this;
        };

        Element.prototype.ftTransformedPoint = function( x, y ) {
            var transform = this.transform().diffMatrix;
            return { x:  transform.x( x,y ) , y:  transform.y( x,y ) };
        };
        
        Element.prototype.ftUpdateTransform = function() {
            // minimum parameter control here
            var minScale = 3.5;
            // var relativeOrgMatrix = Snap.parseTransformString( this.ftGetInitialTransformMatrix().toTransformString() );
            // var dScale = relativeOrgMatrix[1][1] * this.data("scale" ) < 3.5 ? 0.5 : this.data("scale" );
            // var dScale = relativeOrgMatrix[1][1] * this.data("scale" ) > 2 ? 2 : this.data("scale" );
            // var dScale = relativeOrgMatrix[1][1] * this.data("scale" );
            // console.log(dScale);
            // console.log( relativeOrgMatrix );

            // if(  ) ) 
            // var tstring = "t" + this.data("tx") + "," + this.data("ty") + this.ftGetInitialTransformMatrix().toTransformString() + "r" + this.data("angle") + 'S' + dScale;
            // console.log(this.ftGetInitialTransformMatrix());
            var tstring = "t" + this.data("tx") + "," + this.data("ty") + this.ftGetInitialTransformMatrix().toTransformString() + "r" + this.data("angle") + 'S' + this.data("scale");
            //  + 'S' + this.data("scale")
            // console.log(tstring);
            this.attr({ transform: tstring });

            // var eleMatrix = new Snap.Matrix();
            // eleMatrix.scale(5, 5);
            // this.transform(eleMatrix);

            // not work as expected
            /*var updatedMatrix = this.attr("transform").totalMatrix.split();
            
            // var scaleControl = scaleControl < minScale ? eleMatrix.scale( minScale ) : "" ;

            if( updatedMatrix.scalex < minScale ) {
                var eleMatrix = new Snap.Matrix();
                eleMatrix.translate( updatedMatrix.dx, updatedMatrix.dy )
                eleMatrix.rotate( updatedMatrix.rotate )
                eleMatrix.scale( minScale )
                this.transform( eleMatrix );
                // this.data("bbT") && this.ftHighlightBB();
            }*/
            
            // console.log(Snap.parseTransformString(tstring));
            this.data("bbT") && this.ftHighlightBB();


            // temp method:
            // console.log(this.data("bbT") && this.ftHighlightBB());
            // console.log(this.parent().data());
            /*try{
            }catch(e){
                // console.log(e);
            }*/
            // console.log("scale: " + this.data("scale" ) );
            // console.log("angle: " + this.data("angle"));

            /*var updatedMatrix = this.attr("transform").totalMatrix.split();
            // console.log(updatedMatrix.split());
            // this.parent().data( "scale_X", updatedMatrix.a );
            // this.parent().data( "scale_Y", updatedMatrix.d );
            this.parent().data( "scale_X", updatedMatrix.scalex );
            this.parent().data( "scale_Y", updatedMatrix.scaley );
            this.parent().data( "angle_A", updatedMatrix.rotate );
            this.parent().data( "angle_B", 0 );
            // this.parent().data( "angle_A", updatedMatrix.b );
            // this.parent().data( "angle_B", updatedMatrix.c );
            this.parent().data( "pos_X", updatedMatrix.dx );
            this.parent().data( "pos_Y", updatedMatrix.dy );
            // console.log(this.parent());

            var updatedData = {};
            for( var key in this.parent().data() ) {
                if( key != "id" ) {
                    updatedData[key] = this.parent().data(key);
                }
            }
            // console.log(this.parent().data() );
            // console.log("updating");
            RMS.updateDataObject( updatedData );*/
            

            // experimental, to make it more generic
            ftOption.updateCallback( this ); // "this" is the one being transformed with transform=matrix()

            return this;
        };

        Element.prototype.ftHighlightBB = function( customOptions ) {
            console.log('ftHighlightBB');
            this.data("bbT") && this.data("bbT").remove();
            this.data("bb") && this.data("bb").remove();
            // console.log(customOptions);
            // process custom options if valid
            ftOption = checkOption( customOptions, ftOption );
            // console.log(ftOption);
            
            // var handleStrokeDash = customOptions.handleStrokeDash != undefined ?  customOptions.handleStrokeDash : ftOption.handleStrokeDash;
            // var handleFill = customOptions.handleFill != undefined ?  customOptions.handleFill : ftOption.handleFill;
// console.log(ftOption.handleClass);
// console.log(ftOption.handleClass.join[' ']);

            this.data("bbT", this.paper.rect( rectObjFromBB( this.getBBox(1) ) )
                            .attr({ 
                                fill: "none", 
                                stroke: ftOption.handleFill, 
                                strokeDasharray: ftOption.handleStrokeDash,
                                class: 'bbt' + ftOption.handleClass.join([' '])
                            })
                            .transform( this.transform().local.toString() ) );
            this.data("bb", this.paper.rect( rectObjFromBB( this.getBBox() ) )
                            .attr({ 
                                fill: "none", 
                                stroke: ftOption.handleFill, 
                                strokeDasharray: ftOption.handleStrokeDash,
                                class: 'bb' + ftOption.handleClass.join([' '])
                            }) );
            return this;
        };

        // temp test for moving elements
        Element.prototype.ftHighlightBBCenter = function( customOptions ) {
            console.log('ftHighlightBBCenter');
            this.data("bbT") && this.data("bbT").remove();
            this.data("bb") && this.data("bb").remove();
            // console.log(customOptions);
            // process custom options if valid
            ftOption = checkOption( customOptions, ftOption );
            // console.log(ftOption);


            console.log(this.transform());
            console.log(this.transform().local.toString());
            this.data("bbT", this.paper.rect( rectObjFromBB( this.getBBox(1) ) )
                            .attr({ 
                                fill: "none", 
                                stroke: ftOption.handleFill, 
                                strokeDasharray: ftOption.handleStrokeDash,
                                class: 'bbt' + ftOption.handleClass.join([' '])
                            })
                            .transform( this.transform().toString() ) );
            this.data("bb", this.paper.rect( rectObjFromBB( this.getBBox() ) )
                            .attr({ 
                                fill: "none", 
                                stroke: ftOption.handleFill, 
                                strokeDasharray: ftOption.handleStrokeDash,
                                class: 'bb' + ftOption.handleClass.join([' '])
                            }) );
            return this;
        };
        
    });

    function checkOption( customOptions, defaultOptions ) {
        if( customOptions != undefined && typeof customOptions === "object") {
            // combine default options to custom options
            for( var key in defaultOptions ) {
                if( customOptions[ key ] != undefined ) {
                    defaultOptions[ key ] = customOptions[ key ];
                }
            }
            return defaultOptions;
        }
        return defaultOptions;
        // else {
        //     var customOptions = {};
        // }
    }

    function invTransformPoint( el, x, y ) {
                var tdx, tdy;
                var snapInvMatrix = el.transform().diffMatrix.invert();
                snapInvMatrix.e = snapInvMatrix.f = 0;
                return {
                    tx: snapInvMatrix.x( x,y ),
                    ty: snapInvMatrix.y( x,y )
                }
            };
    Snap._.invTransformPoint = invTransformPoint;
            

    function rectObjFromBB ( bb ) {
        return { x: bb.x, y: bb.y, width: bb.width, height: bb.height } 
    }

    function elementDragStart( mainEl, x, y, ev ) {
        this.parent().selectAll('circle').forEach( function( el, i ) {
                el.ftStoreStartCenter();
        } );
        mainEl.data("otx", mainEl.data("tx") || 0);
        mainEl.data("oty", mainEl.data("ty") || 0);
    };

    // function invTransformPoint( el, x, y ) {
    //     var tdx, tdy;
    //     var snapInvMatrix = el.transform().diffMatrix.invert();
    //     snapInvMatrix.e = snapInvMatrix.f = 0;
    //     return {
    //         tx: snapInvMatrix.x( x,y ),
    //         ty: snapInvMatrix.y( x,y )
    //     }
    // }

    function elementDragMove( mainEl, dx, dy, x, y ) {
        // console.log(arguments);
        var dragHandle = this;
        // console.log(this.data());
        var gridSize = 15;
        dx = Snap.snapTo(gridSize, dx, 100000000);
        dy = Snap.snapTo(gridSize, dy, 100000000);

        var ip = invTransformPoint( this, dx, dy );
        this.parent().selectAll('circle').forEach( function( el, i ) {
            el.attr({ cx: +el.data('ocx') + ip.tx, cy: +el.data('ocy') + ip.ty });
        } );
        mainEl.data("tx", mainEl.data("otx") + +ip.tx);
        mainEl.data("ty", mainEl.data("oty") + +ip.ty);

        mainEl.ftUpdateTransform();
        mainEl.ftDrawJoinLine( dragHandle );

        // console.log("moving");
        // console.log(this);
        // ftOption.callBack( mainEl );

        // this.data( "transform", mainEl.attr("transform") );

        // console.log(mainEl.attr( "transform" ));
        // console.log(mainEl.data());
    }

    // function updateRMSData( mainEl, dx, dy, x, y ) {

    // };

    function elementDragEnd( mainEl, dx, dy, x, y ) {
    };

    function dragHandleRotateStart( mainElement ) {
        this.ftStoreStartCenter();
    };

    function dragHandleRotateEnd( mainElement ) {
    };

    function dragHandleRotateMove( mainEl, dx, dy, x, y, event ) {
        var handle = this;
        var mainBB = mainEl.getBBox();
        var ip = invTransformPoint( this, dx, dy );

        // this is relative angle to current orign, not absolute
        var angleGrid = 45;
        var freeAngle = Snap.angle( mainBB.cx, mainBB.cy, handle.attr('cx'), handle.attr('cy') );
        var angleSnap = Snap.snapTo(angleGrid, freeAngle, 100000000);

        handle.attr({ cx: +handle.data('ocx') + ip.tx, cy: +handle.data('ocy') + ip.ty });

        mainEl.data("angle", angleSnap - 180);

        // scale
        /*var scaleGrid = 0.2;
        var distance = calcDistance( mainBB.cx, mainBB.cy, handle.attr('cx'), handle.attr('cy') );
        var freeScale = distance / mainEl.data("scaleFactor");
        console.log(freeScale);
        var scaleSnap = Snap.snapTo(scaleGrid, freeScale, 100000000);
        console.log(scaleSnap);

        // this is a relative scale.......cannot control the min...
        // var minScale = 3.5;
        // scaleSnap = scaleSnap < minScale ? minScale : scaleSnap;
        mainEl.data("scale", scaleSnap );*/
        var distance = calcDistance( mainBB.cx, mainBB.cy, handle.attr('cx'), handle.attr('cy') );
        mainEl.data("scale", distance / mainEl.data("scaleFactor") );

        mainEl.ftUpdateTransform();
        mainEl.ftDrawJoinLine( handle );    
    };

    function calcDistance(x1,y1,x2,y2) {
        return Math.sqrt( Math.pow( (x1 - x2), 2)  + Math.pow( (y1 - y2), 2)  );
    }


})();
/*
usages:
myEl.ftCreateHandles();
Snap.load( "Dreaming_tux_vb.svg", function( frag ) {
        var g = s.g();
        g.append(frag);
        g.ftCreateHandles(); // <------
        } );

*/




// Snap return selectAll as an Array, there is not method or prototype for this Array, cannot add plugin
// Snap.plugin( function( Snap, Element, Paper, glob, Fragment ) {
//     var elproto = Element.prototype;

//     elproto.each = function ( eleSet ) {
//         console.log(parmas);
//         var el = this,
//             node = el.node;
//         if (!params) {
//             return el;
//         }

//         if (is(params, "object")) {
//             if (arguments.length > 1) {
//                 var json = {};
//                 json[params] = value;
//                 params = json;
//             } else {
//                 return eve("snap.util.getattr." + params, el).firstDefined();
//             }
//         }
//         for (var att in params) {
//             if (params[has](att)) {
//                 eve("snap.util.attr." + att, el, params[att]);
//             }
//         }
//         return el;
//     };
// });