﻿(function () {

    var app = angular.module("tableViewModuel", ['ngMaterial', 'ui.bootstrap']).run(['$templateCache', function ($templateCache) {
        //ng-class="[uc.statusCode[option.status]]" {{t.diffTime | date:"mm:ss"}}
        $templateCache.put('tmp/tableView.html', '\
                                  <div class="menu-container materialize-keypad col-8x8 table-view-mode" layout="column" style="height: 100%;"> \
                                     <div class="row table-list" style="margin-bottom: 0;"> \
                                        <div class="col s8-1" ng-repeat="t in tableView.tmplTableList | startFrom:(tableView.currentPage - 1) * tableView.PerPage | limitTo: tableView.PerPage">\
                                                <button class="btn btn-table md-button md-grid-button table-model" table-model="{{t.tableNo}}" ng-class="t.getColorClass()" ng-click="clickTable(t.tableNo)">\
                                                    <div style="position:absolute;top:5px;font-size:20px;line-height:16px;" class="text">{{t.tableNo}}</div>\
                                                    <div style="font-size:16px;margin-top:15px;line-height:16px;" ng-show="t.status != 0" class="text">人數:{{t.noOfPeople}}</div>\
                                                    <div style="font-size:16px;line-height:16px;" ng-show="t.status != 0" class="text">\
                                                        <span style="float:left;">時間:</span>\
                                                        <span class="time" style="float:left;">{{t.getRemainerTime()}}</span>\
                                                    </div>\
                                                </button>\
                                                </div> \
                                         \
                                        <div class="col s8-1 dummy-grid" ng-if="tableView.noOfPage() == tableView.currentPage && tableView.tmplTableList.length % tableView.PerPage > 0" ng-repeat="i in tableView.reminderPage() track by $index"><span></span></div> \
                                        \
                                        <pager-custom icon-size="\'dp56\'" template="tmpl/pager-default-materialize.html" total-items="tableView.tmplTableList.length" items-per-page="tableView.PerPage" ng-model="tableView.currentPage"></pager-custom> \
                                      </div>\
                                  </div> \
                                  ');



    }]);

    app.directive('pleaseWaitModal', function (MainService) {
        return {
            restrict: "E",
            template: '<div class="modal-loader" ng-show="MainService.pleaseWait.check();">\
                            <div class="modal-loader-content">\
                                <div class="modal-loader-body">\
                                    <div class="loader"></div>\
                                </div>\
                            </div>\
                        </div>',
            link: function (scope, element) {
            },
            controller: function ($scope, $timeout) {
                $scope.pleaseWait = {
                    status: false,
                    timeout: null,
                    timer:null,
                    on: function () {
                        var self = this;

                        if (this.timer != null) {
                            $timeout.cancel(self.timer);
                            self.timer = null;
                        }

                        self.timer = $timeout(function () {
                            self.off();
                        }, 10000)

                        this.timeout = $timeout(function () {
                            self.status = true;
                        }, 100)
                        
                    },
                    off: function () {
                        this.status = false;
                        if (this.timer != null) {
                            $timeout.cancel(self.timer);
                            self.timer = null;
                        }
                        $timeout.cancel(this.timeout);
                        
                    },
                    check: function () {
                        return this.status;
                    }
                }

                MainService.pleaseWait = $scope.pleaseWait;
            }
        }
    });

    app.service("tableModel", function (userConfig) {
        function tableModel() {
            this.tableNo = null;
            this.status = null;
            this.noOfPeople = null;
            this.startTime = null;
            this.lastOrder = null;
            this.diffTime = null;
            this.timer = null;
            this.currentTime = null;
            this.buffetStatus = null;
            
            tableModel.prototype.setValue = function (data) {
                this.tableNo = data.label;
                this.status = data.status;
                this.buffetStatus = data.buffetStatus;
            }

            tableModel.prototype.setServerTime = function (t) {
                this.currentTime = new Date(t);
            }

            tableModel.prototype.updateTableRecords = function (tableRecords) {
                var self = this;
                //if (tableRecords[self.tableNo] == undefined) return;

                //console.debug(tableRecords, t);
                //self.setServerTime(t);
                for (var i = 0; i < tableRecords.length; i++) {
                    var r = tableRecords[i];
                    if (r.tableCode == self.tableNo && !r.tableSubCode) {
                        self.noOfPeople = r.pax;
                        //console.debug(self.noOfPeople);
                        self.status = r.status;
                        self.startTime = new Date(r.startTime);
                        self.lastOrder = new Date(r.leaveTime);
                        self.lastOrder = moment(self.lastOrder).year(self.startTime.getFullYear()).month(self.startTime.getMonth()).date(self.startTime.getDate()).toDate();
                        self.diffTime = moment(self.lastOrder).diff(moment(self.currentTime), 'second');
                        //console.debug(self.diffTime)
                        //console.debug(self.diffTime);
                        if (self.diffTime < 0) self.diffTime = 0;
                        self.diffTime = moment({ hour: 0, minute: 0, second: self.diffTime });
                        //self.setTimer();
                        //self.printTableModelTime();

                        break;
                    }
                }
            }

            tableModel.prototype.getRemainerTime = function () {
                var self = this;
                /*if (!self.diffTime) {
                    console.debug('yes');
                    return "";
                }*/
                var mins = (moment(self.diffTime).get('hour') * 60) + moment(self.diffTime).get('minute');
                var seconds = moment(self.diffTime).get('second');
                //if (!isNaN(mins)) console.debug("yes");
                if (isNaN(mins) && isNaN(seconds)) return "";
                return mins + ":" + seconds;
            }

            tableModel.prototype.getColorClass = function () {
                if (this.status == 2 && this.buffetStatus > 0) {
                    return userConfig.buffetStatusCode[this.buffetStatus];
                }
                else return userConfig.statusCode[this.status];
            }

            tableModel.prototype.subtractSecond = function () {
                var self = this;
				if(self.diffTime == null) return;
                if (moment(self.diffTime).get('hour') == 0 && moment(self.diffTime).get('minute') == 0 && moment(self.diffTime).get('second') == 0) {
                    //self.clearTimer();
                    return;
                }
                self.diffTime = moment(self.diffTime).subtract(1, 'seconds').toDate();
            }

            tableModel.prototype.printTableModelTime = function () {
                var self = this;
                self.subtractSecond();
                $(".btn-table[table-model='" + self.tableNo + "']").find('span.time').html(self.getRemainerTime());
                //console.debug($(".btn-table[table-model='" + self.tableNo + "']"))
            }

            tableModel.prototype.setTimer = function () {
                var self = this;
                self.timer = setInterval(function () {
                    self.printTableModelTime();
                }, 1000)
            }

            tableModel.prototype.clearTimer = function () {
                var self = this;
                clearInterval(self.timer);
            }

        }

        return tableModel;
    })

    app.controller("tableViewController", function ($scope, MainService, SocketFactory, tableModel) {
        //$scope.tmplTableList = [];


        $scope.clickTable = function (tableNo) {
            MainService.pleaseWait.on();
            MainService.clickTable(tableNo + "");
        }


        $scope.tableView = {
            tmplTableList: [],
            PerPage: 8 * 8 - 2,
            currentPage: 1,
            noOfPage: function () {
                var self = this;
                return Math.ceil(self.tmplTableList.length / self.PerPage)
            },
            reminderPage: function () {
                var self = this;
                return MainService.getPageNumber(self.PerPage - (self.tmplTableList.length) % self.PerPage)
            },
            timer: null,
            initTableView: function (SceneList, tableRecords, serverTime) {

                /*this.tmplTableList.forEach(function (t) {
                    t.clearTimer();
                })*/

                MainService.pleaseWait.on();

                if (this.timer != null) {
                    clearInterval(this.timer);
                    this.timer = null;
                }
                
                this.tmplTableList = [];
                var tmplList = [];
                angular.extend(tmplList, SceneList);

                tmplList.sort(function (a, b) {
                    var target1 = parseInt(a.label);
                    if (isNaN(target1)) target1 = tmplList.length;
                    var target2 = parseInt(b.label);
                    if (isNaN(target2)) target2 = tmplList.length;
                    return target1 - target2;
                })

                for (var i = 0; i < tmplList.length; i++) {
                    var tmpl = new tableModel();
                    tmpl.setValue(tmplList[i]);
                    tmpl.setServerTime(serverTime);
                    var t = tmplList[i];
                    if (tableRecords[t.label] != undefined) {
                        tmpl.updateTableRecords(tableRecords[t.label]);
                    }
                    //tmpl.updateTableRecords(tableRecords, serverTime);
                    this.tmplTableList.push(tmpl);
                    if (i == tmplList.length - 1) MainService.pleaseWait.off();
                }

                var self = this;
                this.timer = setInterval(function () {
                    for (var i = 0; i < self.tmplTableList.length; i++) {
                        self.tmplTableList[i].printTableModelTime();
                    }
                }, 1000)
            }
        }

        window['tableView'] = $scope.tableView;
        /*for (var i = 1; i <= 50; i++) {
            var tmpl = {
                tableNo: i,
                status: 'available',
                noOfPeople: toString(5),
                time: "45:09"
            }

            if (i == 3) {
                tmpl.status = "using";
            }

            $scope.tableView.tmplTableList.push(tmpl);
        }*/



    })

    app.controller("tableViewTopBarController", function ($scope, MainService) {
        $scope.changeToTableView = function () {
            MainService.modeTable = MainService.schema.modeTable.tableView;
            if (MainService.modeFloorPlan == MainService.schema.modeFloorPlan.changeTable) {
                MainService.cancelTransfer();
            }
            MainService.modeFloorPlan = MainService.schema.modeFloorPlan.order; // default order mode;
            
        }

        $scope.changeToSVGView = function () {
            MainService.modeTable = MainService.schema.modeTable.svg;
            if (MainService.modeFloorPlan == MainService.schema.modeFloorPlan.changeTable) {
                MainService.cancelTransfer();
            }
            MainService.modeFloorPlan = MainService.schema.modeFloorPlan.order; // default order mode;

        }
    })

   
})()
