﻿//var app = angular.module('rms2', []);

function initAngularModule() {
    var app = angular.module('takeOutAndDeliveryModule', ['tmpl/checkTakeOutAndDelivery.html']);
    app.directive('modal', function ($templateCache, $compile, $rootScope) {
        return {
            restrict: "E",
            template: '<div id="modal1" class="modal"><div class=""></div><div class="modal-content"></div></div>',
            replace: true,
            link: function (scope, element) {
                $rootScope.$on('changeCustomerAddressList', function (event, data) {
                    console.debug(9, element, data);
                    //var tmp = '<div class="modal-content" style="height:500px">##content##</div>';
                    //tmp = tmp.replace('##content##', data.template);
                    element.html($templateCache.get(data.template)).show();
                    $compile(element.contents())(scope);
                    setTimeout(function () {
                        Materialize.updateTextFields();
                    }, 200)
                });
            },
            controller: function ($scope) {
            }
        }



    })

    angular.module('tmpl/checkTakeOutAndDelivery.html', []).run(["$templateCache", function ($templateCache) {
        $templateCache.put('tmpl/checkTakeOutAndDelivery.html',
          '<div id="generic-keypad" class="keypad-container" ng-controller="checkTakeOutAndDeliveryController"> \
            <form>\
          <md-dialog aria-label="keypad" class="keypad" ng-app > \
            \
            <md-toolbar> \
              <div class="md-toolbar-tools"> \
                <h2>{{::ui[\'txt_takeOutAndDelivery_input_phone_no_to_search\']}}</h2> \
                <span flex></span> \
                <!-- <md-button md-no-ink="true" class="md-icon-button" ng-click="closeDialog()"> \
                  <i class="md-icon dp45">clear</i> \
                </md-button> --> \
              </div> \
            </md-toolbar> \
              \
            <md-dialog-content class="sticky-container paymentPermission-html"> \
                     <md-input-container class="login"> \
                       <input class="dialog-close" ng-model="keypadInfo.value" login-input login-input-enter="next()"  ng-trim="false" type="text" custom-always-focus="true" autocomplete="off"> \
                     </md-input-container> \
                            \
                     <md-grid-list md-cols="6" md-row-height="1:1" md-gutter="1px"  class="keypad-grid" style=""> \
                         <md-grid-tile ng-repeat="element in ::keypad" md-colspan="2" md-rowspan="2"> \
                          <md-button md-no-ink="true" class="md-raised" ng-click="input(element.text)" ng-if="!element.useIcon"> \
                            {{::element.text}} \
                          </md-button> \
                            \
                          <md-button md-no-ink="false" class="md-raised" ng-click="reset()" ng-if="element.useIcon && element.text==\'reset\'"> \
                            <i ng-show="keypadInfo.value" class="md-icon dp40">replay</i> \
                            <i ng-hide="keypadInfo.value" class="md-icon dp40">clear</i> \
                          </md-button> \
                            \
                          <md-button md-no-ink="true" class="md-raised" ng-click="backspace()" ng-if="element.useIcon && element.text==\'backspace\'"> \
                            <i class="md-icon dp40">backspace</i> \
                          </md-button> \
                         </md-grid-tile> \
                            \
                         <!-- finish button --> \
                         <!-- ng-show="keypadInfo.value"  --> \
                         <md-grid-tile md-rowspan="2" md-colspan="6" ng-show="keypadInfo.value"> \
                           <md-button md-no-ink="false" class="md-raised submit" ng-click="next($event)">{{::ui["done"] || "完成"}}</md-button> \
                         </md-grid-tile> \
                     </md-grid-list> \
             <!-- </div> --> \
            </md-dialog-content> \
        </md-dialog> \
        </form>\
      </div>');
    }]);

    app.controller("checkTakeOutAndDeliveryController", function ($scope, $controller, $mdDialog, $element, $attrs, $timeout, uiLanguage, INFO, MainService, SocketFactory, $rootScope, $rootElement) {

        $scope.ui = uiLanguage[INFO.lang];
        $scope.keypad = keypad;
        $scope.keypadInfo = {
            value: ''
        };

        //$scope.title = $scope.ui["please_place_the_card"];


        $scope.input = function (val) {
            $scope.keypadInfo.value += '' + val;
        };

        $scope.backspace = function () {
            $scope.keypadInfo.value = $scope.keypadInfo.value.substring(0, $scope.keypadInfo.value.length - 1)
        };

        $scope.reset = function (val) {
            console.log($scope.keypadInfo.last);
            if ($scope.keypadInfo.value == '') {
                $scope.keypadInfo.value = $scope.keypadInfo.last;
                $mdDialog.cancel();
            } else {
                $scope.keypadInfo.value = '';
            }
        };

        $scope.keypressed = function (event) {
            console.log(event);
        };

        $scope.next = function (event) {
            console.debug(114, $scope.keypadInfo.value);
            $rootScope.$emit('getOrderByPhone', { 'phone': $scope.keypadInfo.value });
            $mdDialog.hide();
            /*SocketFactory.emit('checkPaymentPermission', { "userName": $scope.keypadInfo.value }, function (data) {
                $mdDialog.hide();
                if (data[0].count > 0) {
                    MainService.checkingPaymentPermission = true;
                    MainService.submitCart();

                } else {
                    MainService.showAlert('no_permission');
                }
            })*/

            //angular.element($element).blur();
        };

        $scope.close = function () {
            $scope.keypadInfo.value = $scope.keypadInfo.last;
            delete $rootScope.watchManagement;
            $mdDialog.hide();
        };

        $scope.callKeypad = function (event) {
            $scope.keypadInfo.last = $scope.keypadInfo.value;
            $scope.keypadInfo.value = '';

            $mdDialog.show({
                templateUrl: 'tmpl/paymentPermission.html',
                targetEvent: event,
                focusOnOpen: true,
                bindToController: true,
                preserveScope: true,
                locals: {
                    keypadInfo: $scope.keypadInfo,
                }
            })

            $rootElement.on('keyup', function (e) {
                // console.log("its's me");
                console.log($scope.keypadInfo);
                if (e.keyCode === 27) {
                    $timeout($mdDialog.hide);
                    $scope.keypadInfo.value = $scope.keypadInfo.last;
                }
            });
        };

    });

}

if (typeof angular != "undefined") {
    initAngularModule();
}
else if (typeof module != "undefined") {
    module.exports = new takeOutAndDelivery();
}

//app.controller('takeOutAndDelivery', takeOutAndDelivery);

function takeOutAndDelivery($mdDialog) {

    this.SocketFactory = null;
    this.customerData = { phone: "", name: "", time: "", address: "", isDelivery: false, remark:"" };
    this.viewOnly = false;
    this.isModify = false;

    this.testDialog = $mdDialog;

    this.show = {
        isFindingCustomer: false,
        nothingCustomerInfo: false
    }

    this.$rootScope = null;

    this.tmplAddress = [
        { name: "test-user-1", address: "aajdkfskdjsfhksjhdkfjskdjhfksdf" },
        { name: "test-user-2", address: "sdfvxcvvxcvxcvdsfwef" },
        { name: "test-user-3", address: "zxchyjukuikuikuikuikuikuikui" },
    ]

    takeOutAndDelivery.prototype.showCheckBillByPhone = function () {
        $mdDialog.show({
            templateUrl: 'tmpl/checkTakeOutAndDelivery.html',
            targetEvent: event,
            focusOnOpen: true,
            bindToController: true,
            preserveScope: true,
        })
    }

    takeOutAndDelivery.prototype.init = function () {

    }

    takeOutAndDelivery.prototype.setSocketFactory = function (SocketFactory) {
        this.SocketFactory = SocketFactory;
    }


    takeOutAndDelivery.prototype.testing = function () {
        console.log("success");
    }

    takeOutAndDelivery.prototype.closeDialog = function (isUpdated) {
        if (this.viewOnly) this.resetCustomerData();
        if (this.isModify) {
            var self = this;

            if (isUpdated && self.customerData.phone) {
                var postData = {
                    refNo: MainService.Cart.refNo,
                    customerData: self.customerData,
                    staffId: MainService.UserManager.staffDetails.staffId
                }

                this.SocketFactory.emit("updateTakeOutAndDeliveryCustomerData", postData, function (result) {
                    self.resetCustomerData();
                    self.$rootScope.$emit('refreshBillList');
                });
            } else {
                self.$rootScope.$emit('refreshBillList');
                self.resetCustomerData();
            }

            this.isModify = false;
        }

       


        $('#modal1').remove();
        $mdDialog.hide();
    }

    takeOutAndDelivery.prototype.deleteCustomerRecord = function (target) {
        var postData = {
            phone: this.customerData.phone,
            address: target.address
        }
        var self = this;
        this.SocketFactory.emit("deleteCustomerData", postData, function (result) {

            if (result == "OK") {
                self.tmplAddress.forEach(function (a, i) {
                    console.log(a);
                    if (a.address == postData.address) {
                        self.tmplAddress.splice(i, 1);
                        return false;
                    }
                })
            }
        })

    }

    takeOutAndDelivery.prototype.resetCustomerData = function () {
        this.customerData = { phone: "", name: "", time: "", address: "", isDelivery: false, remark: "" };
    }

    takeOutAndDelivery.prototype.checkPhoneTypeInEvent = function ($event) {
        var fnKeyCode = [8, 9, 46, 37, 39];
        var numberKeyCode = [48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 96, 97, 98, 99, 100, 101, 102, 103, 104];

        console.debug($event.keyCode);
        if ($event.keyCode == 229) {
            $event.preventDefault();
            return false;
        }
        if (fnKeyCode.indexOf($event.keyCode) != -1) {

        }
        else if ($event.target.value.length >= 8) {
            console.debug(64, '>=8');
            $event.preventDefault();
            return false;
        }
        else if (numberKeyCode.indexOf($event.keyCode) == -1) {
            $event.preventDefault()
            return false;
        }
    }

    takeOutAndDelivery.prototype.showModal = function () {
        $('#modal1').openModal({
            dismissible: false
        });
    }

    takeOutAndDelivery.prototype.closeModal = function () {
        $('#modal1').closeModal();
    }

    takeOutAndDelivery.prototype.selectAddress = function (addressObj) {
        console.debug(addressObj);
        this.customerData.name = addressObj.name;
        this.customerData.address = addressObj.address;
        var self = this;
        setTimeout(function () {
            Materialize.updateTextFields();
            self.closeModal();
        }, 100)
    }

    takeOutAndDelivery.prototype.searchCustomerInfo = function () {
        console.debug(this.customerData.phone.length);
        if (this.customerData.phone.length < 8) {
            return;
        }

        this.show.nothingCustomerInfo = false;
        this.show.isFindingCustomer = true;
        var self = this;

        this.SocketFactory.emit("getCustomerData", this.customerData, function (result) {
            self.tmplAddress = [];
            result.forEach(function (c) {
                var t = {name:c.name, address:c.address}
                self.tmplAddress.push(t);
            })


            self.show.isFindingCustomer = false;

            if (self.tmplAddress.length != 0) {
                //self.show.nothingCustomerInfo = true;
                self.$rootScope.$emit('changeCustomerAddressList', { "template": "views/modalContentsTemplate" });
                self.showModal();
            } else {
                self.show.nothingCustomerInfo = true;
            }
            
        })
    }

    takeOutAndDelivery.prototype.payLater = function () {
        console.debug(315, !this.customerData.time);
        if (this.customerData.phone.length != 8) {
            $mdDialog.show(
              $mdDialog.alert()
              .parent(angular.element(document.body))
              .title(MainService.label['alert'])
              .content(MainService.label['txt_takeOutAndDelivery_pay_later_no_customer_info_error1'])
              .ok(MainService.label['ok'])
          );
        } else {

            var confirm = $mdDialog.confirm()
                    .title(MainService.label['alert'])
                    .content(MainService.label['txt_takeOutAndDelivery_question_pay_later'])
                    .ok(MainService.label['yes'])
                    .cancel(MainService.label['cancel']);

            $mdDialog.show(confirm).then(function () {
                console.debug(129);
                MainService.submitCart({ "payLater": true });
            });

        }
    }

    takeOutAndDelivery.prototype.showInputInfoDialog = function (viewOnly, customerData) {
        //console.debug($mdDialog);
        var self = this;

        if (viewOnly) {
            self.viewOnly = true;
            self.customerData = customerData;
            self.customerData.time = moment(self.customerData.time).toDate();
        }
        else {
            self.viewOnly = false;
            //self.resetCustomerData();
        }

        var alert = $mdDialog.show({
            templateUrl: '/tmpl/InputCustomerInfo.html',
            escapeToClose: false,
            controller: function ($scope, $compile, $rootScope, $filter, uiLanguage) {
                //var obj = new takeOutAndDelivery($mdDialog);
                var ui = $scope.ui = uiLanguage[INFO.lang];
                var obj = MainService.getOtherWriteOrderService().takeOutAndDelivery;

                obj.show.isFindingCustomer = false;
                obj.show.nothingCustomerInfo = false;


                obj.$rootScope = $rootScope;
                $scope.takeOutAndDelivery = obj;

                /*$scope.$watch('takeOutAndDelivery.customerData.time', function (newValue) {
                    $scope.takeOutAndDelivery.customerData.time = $filter('date')(newValue, 'HH:mm');
                });*/

                $scope.init = function () {
                    if ($('#modal1').length == 0)
                        $('body').append($compile('<modal></modal>')($scope))
                    setTimeout(function () {
                        Materialize.updateTextFields();
                    }, 200)
                }
            }
        });

    }

    takeOutAndDelivery.prototype.toModifyMode = function () {
        this.viewOnly = false;
        this.isModify = true;
    }
}
