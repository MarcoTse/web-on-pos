// for IE or any browser that does not come with console
if(typeof console === 'undefined') {
    var console = {};
    console.log = function(){};
}

// original: http://gomakethings.com/how-to-get-the-value-of-a-querystring-with-native-javascript/
/**
 * Get the value of a querystring
 * @param  {String} field The field to get the value of
 * @param  {String} url   The URL to get the value from (optional)
 * @return {String}       The field value
 */
var getQueryString = function ( field, url ) {
    var href = url ? url : window.location.href;
    var reg = new RegExp( '[?&]' + field + '=([^&#]*)', 'i' );
    var string = reg.exec(href);
    return string ? string[1] : null;
};

// custom functions
var showMessage = function(target, message, orientation, error_type, delay){
    delay = typeof(delay) == "undefined" ? 3000 : delay;
    error_type = typeof(error_type) == "undefined" ? "default" : error_type;

    $(target + " .content").html(message);
    $(target + " .content").addClass(error_type);
    $(target).fadeIn();

    if(orientation == "vertical") {
        $(target).css("display", "table");
    }
    
    $(target).clearQueue();
    if( parseInt(delay) >= 0 ) {
        setTimeout(function(){$(target).fadeOut();}, delay);
    } else {
        
    }
}
        
// usage: showMessage(".message_box", msg, "vertical", "warn");

var confirmAction = function( paramsObj ) {
    if( typeof paramsObj === "object" && paramsObj != undefined ) {
        if ( confirm( paramsObj.confirmMessage ) ) {
            msg = paramsObj.trueMessage;
            paramsObj.trueAction(paramsObj.trueActionParams, paramsObj.targetObj);
        } else {
            msg = paramsObj.falseMessage;
            paramsObj.falseAction(paramsObj.falseActionParams, paramsObj.targetObj);
        }
        // return true;
    } else {
       //  // default object
       // var option = {
       //      defaultfn: function( option.confirmMessage ) {
       //          console.log(arguments);
       //          // alert( this.msg );
       //      },
       //      confirmMessage : "confirmAction() is not setup with proper arguments"
       // };

       // return option.defaultfn.apply( this, arguments );
       //  // msg = "confirmAction() is not setup with proper arguments";
       //  // return false;
    }

    // display result message
    // showMessage("#msg-box", msg, "", "warn", 30000);
}

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function lowerFirstLetter(string) {
  // console.log(string);
    return string.charAt(0).toLowerCase() + string.slice(1);
}

/*
usage:
confirmActionParams = {
    confirmMessage : "Are you sure to REMOVE all the patch boxes?",
    trueAction : (function(paramsObj){
        $('.replacement_map_container.active .img_patch_container ').remove();
        update_patch_box_to_obj('', 'all');
    }),
    trueActionParams : {},
    trueMessage : "Patch boxes are removed.",
    falseAction : function(){},
    falseActionParams : {},
    // targetObj : $(this),
    falseMessage : "All patch box removal is cancelled."
};
confirmAction(confirmActionParams);
*/

function createTimeStamp(){
    return new Date().getTime();
}

function screenRatioDetect() {
    if (screen.width / screen.height > 1.65) {
        // console.log("wide");
        return "wide";
    } else {
        return "square";
    }
}

var shortenText = function(text, maxLength) {
    var ret = text;
    if (ret.length > maxLength) {
        ret = ret.substr(0,maxLength-3) + "...";
    }
    return ret;
}

// clone object, so they do not have linkage
function clone(obj) {
    if (null == obj || "object" != typeof obj) return obj;
    var copy = obj.constructor();
    for (var attr in obj) {
        if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
    }
    return copy;
}


// polyfills
Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};

if (!Object.keys) {
  Object.keys = (function() {
    'use strict';
    var hasOwnProperty = Object.prototype.hasOwnProperty,
        hasDontEnumBug = !({ toString: null }).propertyIsEnumerable('toString'),
        dontEnums = [
          'toString',
          'toLocaleString',
          'valueOf',
          'hasOwnProperty',
          'isPrototypeOf',
          'propertyIsEnumerable',
          'constructor'
        ],
        dontEnumsLength = dontEnums.length;

    return function(obj) {
      if (typeof obj !== 'object' && (typeof obj !== 'function' || obj === null)) {
        throw new TypeError('Object.keys called on non-object');
      }

      var result = [], prop, i;

      for (prop in obj) {
        if (hasOwnProperty.call(obj, prop)) {
          result.push(prop);
        }
      }

      if (hasDontEnumBug) {
        for (i = 0; i < dontEnumsLength; i++) {
          if (hasOwnProperty.call(obj, dontEnums[i])) {
            result.push(dontEnums[i]);
          }
        }
      }
      return result;
    };
  }());
}

var keypad = [];
for(var n=1; n<10; n++) {
    keypad.push({text: n, useIcon: false});
}
keypad.push({ text: 'reset', useIcon: true }, { text: 0, useIcon: false }, { text: 'backspace', useIcon: true });


// jQuery plugin
(function($) {
    // http://stackoverflow.com/questions/4814398/how-can-i-check-if-a-scrollbar-is-visible
    // detect if scrollbar is avaiable
    $.fn.hasScrollBar = function() {
        try{
            return this.get(0).scrollHeight > this.height();
        }catch(e){
            return 'element have no scrollHeight';
            // console.log(e);
        }
    }

    function hasID( $target, id ) {return $target.attr('id') === id;}

    $.fn.hasID = function ( id ) {
      'use strict' // write more secured code

      return $(this).attr('id') === id;
    }
})($);

// angular performance check
// performance tweak
// Remove duplicate watchers
var checkWatcher = function() {
    var root = angular.element(document.getElementsByTagName('body'));

    var watchers = [];

    var f = function (element) {
        angular.forEach(['$scope', '$isolateScope'], function (scopeProperty) { 
            if (element.data() && element.data().hasOwnProperty(scopeProperty)) {
                angular.forEach(element.data()[scopeProperty].$$watchers, function (watcher) {
                    watchers.push(watcher);
                });
            }
        });

        angular.forEach(element.children(), function (childElement) {
            f(angular.element(childElement));
        });
    };

    f(root);

    var watchersWithoutDuplicates = [];
    angular.forEach(watchers, function(item) {
        if(watchersWithoutDuplicates.indexOf(item) < 0) {
             watchersWithoutDuplicates.push(item);
        }
    });

    console.log("watch with duplicates " + watchers.length);
    console.log("watch without duplicates " + watchersWithoutDuplicates.length);
}

function strConvertTo( option, str ) {
  switch( option ) {
    // assume underscore first, may add hypen support later
    case 'camel':
      return str.replace(/_([a-z])/g, function (g) { return g[1].toUpperCase(); });
      break;
    default:
      
      break;
  }
}

// console.log($('#billing-summary .change-container').height());
// $('#billing-summary .btn-proceed').height( $('#billing-summary .change-container').height() )