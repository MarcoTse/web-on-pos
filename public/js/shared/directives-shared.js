// RMS helpers
// addon helpers
/* Create a shape node with the given settings. */
function returnTableSVG(t, s) {
    var tableType = this.tableType;
    return tableType[t].sizeList[s].svgCode;
}

// similar to getTableSVG, without adding group, maybe too redundant with too many functions
function returnTableSVGCode(shape, size) {
    // console.log('shape ' + shape);
    // console.log('size ' + typeof size);
    var tableType = this.tableType;
    var svgCode = "";
    for (var t = 0; t < tableType.length; t++) {
        if (tableType[t].type.toLowerCase() === shape.toLowerCase()) {
            // console.log(tableType[t].type.toLowerCase() === shape.toLowerCase());
            for (var s = 0; s < tableType[t].sizeList.length; s++) {
                if (tableType[t].sizeList[s].noOfPeople === size) {
                    // console.log(tableType[t].sizeList[s].noOfPeople === size);
                    return returnTableSVG(t, s);
                }
            }
        }
    } // for
    return svgCode;
}

/*
    return available size for a shape in table shape object
    return array
*/
function getShapeAllSize(shape) {
    var tableType = scope.tableType;
    var size = [];
    for (var t = 0; t < tableType.length; t++) {
        if (tableType[t].type.toLowerCase() === shape.toLowerCase()) {
            for (var s = 0; s < tableType[t].sizeList.length; s++) {
                size.push(tableType[t].sizeList[s].noOfPeople);
            }
        }
    }
    return size;
}

// want to make it generic but not yet success
function getTableSVG(shape, size, option, callback) {
    var tableType = this.tableType;
    // console.log('tableType inside getTableSVG ' + tableType);
    option = "single";
    switch (option) {
        default:
        case "single":
            for (var t = 0; t < tableType.length; t++) {
                if (tableType[t].type.toLowerCase() === shape.toLowerCase()) {
                    for (var s = 0; s < tableType[t].sizeList.length; s++) {
                        if (tableType[t].sizeList[s].noOfPeople === size) {
                            // var className = shape + "-" + size;
                            // return "<g class='shape " + className + "'>" + returnTableSVG(t, s) + "</g>";
                            return "<g class='shape'>" + returnTableSVG.apply(this, [t, s]) + "</g>";
                        }
                    }
                }
            }

            return "SVG Object Not Found";
            break;
    }
}

/*
    return available size for a shape in table shape object
    return array
*/
function getShapeAllSize(shape) {
    var tableType = scope.RMS.tableTypeJSON.tableType;
    var size = [];
    for (var t = 0; t < tableType.length; t++) {
        if (tableType[t].type.toLowerCase() === shape.toLowerCase()) {
            for (var s = 0; s < tableType[t].sizeList.length; s++) {
                size.push(tableType[t].sizeList[s].noOfPeople);
            }
        }
    }
    return size;
}

/*
    return available shape in table shape object
    return array
*/
function getAllShape() {
    var shape = [];
    var tableType = scope.tableType;
    for (var t = 0; t < tableType.length; t++) {
        shape.push(tableType[t].type);
    }
    return shape;
}

// hide all handles created by snap.freeTransform plugin
function hideTableHandles() {
    Snap("#svg-canvas").selectAll(".transform").forEach(function(element, index, array) {
        if (element.freeTransform) {
            element.freeTransform.hideHandles();
        }
    });
}

// this selection handles is used another plugin in snap.svg-plugin.js because of its flexibility and can be called separately
function removeSelectionBB( ele ) {
  console.log('removeSelectionBB');
    // var svgScene = Snap("#svg-canvas");
    ele.ftRemoveBB();
    ele.removeClass('selected');
}

function toggleSelectionBB( ele, scope ) {
    console.log('toggleSelectionBB / ');

    var eleID = ele.parent().attr('id');
    // console.log(ele.parent().attr('id'));

    if( ele.hasClass('selected') ) {
        removeSelectionBB( ele );
    } else {
        ele.ftHighlightBBCenter({ handleFill: "orange", handleClass: ['-handle-box', eleID ] });
        ele.toggleClass('selected');
    }

    // console.log(scope);
    console.log(ele.data());
}

// return rectangular bounding box from given element bounding box properties
function rectObjFromBB ( bb ) {
    return { x: bb.x, y: bb.y, width: bb.width, height: bb.height } 
}

// ng-click version
function tableMouseDown2(ID, scope) {
    // console.log('tableMouseDown2');
    ele = Snap('#'+ID).select('.transform');
    MainService.RMSTable.currentActiveElementSnap = Snap('#'+ID);

    // console.log(ele);
    // console.log(ele.getBBox());
    // console.log(ID);
    // console.log(Snap('#'+ID));
    // console.log(Snap('#'+ID).getBBox());
    // console.log(ID);
    // console.log(ele.parent().attr('id'));

    if( typeof ele.parent().data('state') === 'undefined' )
        ele.parent().data('state', 'preModify');
    
    // console.log(ele.parent().data());

    $('#current-table-label').val(ele.select('text').attr('text'));

    var className = ele.attr('class') || '';
    // console.log('add class');
    // console.log(className.indexOf('modified'));
    if( className.indexOf('modified') === -1 ) {
        if( className ) {
            ele.attr('class', className + ' modified');
        } else {
            ele.attr('class', 'modified');
        }
    }


    if( scope.isMode('floorPlan') ) {
        switch( MainService.modeNavMenu ) {
            default :
            case MainService.schema.modeNavMenu.normal:
                removeAllHandles();
                scope.RMSTable.makeOrder(ele);
                break;
            case MainService.schema.modeNavMenu.editTable:
                // update current selected item to global variable
                try {
                    // console.log("try 2-1");
                    scope.RMSTable.currentActiveElement = ele.parent().data("id");
                    scope.RMSTable.currentActiveElementSnap = ele.parent();
                    scope.RMSTable.currentTableNo = ele.select("text").attr("text");
                } catch(e){
                    // console.log("e 2-1");
                    try{
                        // console.log("try 2-2");
                        scope.RMSTable.currentActiveElement = ele.attr("id");
                        scope.RMSTable.currentActiveElementSnap = ele;
                    }catch(e){
                        // console.log("e 2-2");
                        scope.RMSTable.currentActiveElement = ele.attr("id");
                        scope.RMSTable.currentActiveElementSnap = ele;
                    }
                }
                // console.log('scope.RMSTable.currentTableNo ' + scope.RMSTable.currentTableNo);
                // console.log(scope.RMSTable.currentActiveElementSnap);

                // hide all handles created by snap.freeTransform plugin
                hideTableHandles();

                // in multiple selection mode is on, will not turn on handle, only input field is valid
                console.log('MainService.RMSTable.groupEdit ' + MainService.RMSTable.groupEdit);
                if( MainService.RMSTable.groupEdit ) {
                    if( ele.freeTransform ) {
                        ele.freeTransform.hideHandles();
                    } else {
                        createHandles();
                        ele.freeTransform.hideHandles();
                    }
                    toggleSelectionBB( ele, scope );
                    return;
                }

                function createHandles() {
                    // var loadedMatrix = ele.attr("transform").totalMatrix.split();
                    var loadedMatrix = Snap.parseTransformString(ele.transform());
                    console.log(loadedMatrix);
                    
                    /*defaultAttrs: {
                        rotate: loadedMatrix.rotate,
                        scale: { x: loadedMatrix.scalex, y: loadedMatrix.scaley },
                        translate: { x: loadedMatrix.dx, y: loadedMatrix.dy },
                        ratio: 1
                    }*/

                    var opts = {};
                    var vpBoundaryBBox = MainService.RMSTable.svgScene.select('.viewport-helper').getBBox();
                    opts = {
                       size: { x: ele.parent().getBBox().width / 2 , y: ele.parent().getBBox().height / 2 },
                       attrs: { fill: '#fff', stroke: '#000', class: 'center' },
                       axesAttrs: { fill: '#fff', stroke: '#000', 'stroke-dasharray': '4, 4', 'stroke-width': '8px', opacity: 0.5, class: 'axes'},
                       discAttrs: { fill: '#fff', stroke: '#000', class: 'disc' },
                       boundary: { x: 0|| 0, y: 0|| 0, width: vpBoundaryBBox.width, height: vpBoundaryBBox.height },
                       keepRatio: true,
                       range: { rotate: userConfig.table.edit.constraint.rotate, scale: userConfig.table.edit.constraint.scale },
                       snap: { 
                           rotate: userConfig.table.edit.snap.rotate, 
                           scale: 1, 
                           drag: userConfig.table.edit.snap.translate
                       },
                       rotate: userConfig.table.edit.allowRotate,
                       scale: userConfig.table.edit.allowScale,
                       snapDist: { rotate: 0, scale: 0, drag: 1},
                       size: userConfig.table.display.handle.size,
                       defaultAttrs: {
                           // rotate: loadedMatrix.rotate,
                           // // center: { x: 16, y: 0 },
                           // scale: { x: loadedMatrix.scalex, y: loadedMatrix.scaley },
                           // translate: { x: loadedMatrix.dx, y: loadedMatrix.dy },
                           rotate: loadedMatrix[0][1],
                           center: { x: loadedMatrix[0][2], y: loadedMatrix[0][3] },
                           scale: { x: loadedMatrix[1][1], y: loadedMatrix[1][2] },
                           translate: { x: loadedMatrix[2][1], y: loadedMatrix[2][2] },
                           // 40 offset x
                           ratio: 1,
                       }
                    }

                    // console.log(opts.defaultAttrs);
                    // if( ele.parent().data("id") == "table_1418785047402") {
                    // }

                    var updateEle = function( ft, events, ele ) {
                        // console.log(arguments);
                        // console.log(events);
                        if( events[0] === 'drag end' ) {
                            console.log("callback after apply");
                            ele.parent().data('modified', true);
                            // console.log(ele.parent());
                            // console.log(ele.parent().data);

                            var updatedMatrix = ele.attr("transform").totalMatrix.split();

                            ele.parent().data('state', 'modified');
                        }
                    }

                    // console.log(opts);
                    // console.log(ele);
                    // console.log(updateEle);
                    console.log(ele.parent().getBBox());
                    Snap.freeTransform( ele, opts, updateEle );
                }

                // show handles for selected element
                if( ele.freeTransform ) {
                    ele.freeTransform.showHandles();
                } else {
                    createHandles();
                }
                break;
            case MainService.schema.modeNavMenu.addTable:
                removeAllHandles();
                break;
        }
    }
}

function removeAllHandles() {
    var svgScene = Snap("#svg-canvas");
    svgScene.selectAll(".tableGroup .transform").forEach(function (element, index, array) {
        element.ftRemoveHandles();
        element.ftRemoveBB();
        MainService.RMSTable.groupEdit = false;
        $('.btn-multi-edit').removeClass('active');
        $('#current-table-label').attr('disabled', false);

        if (element.freeTransform) {
            console.log('remove free transform');
            element.freeTransform.unplug();
        }
    });
}

// clone from snap.freetransform.js
// function updateViewBoxRatio(paper) {
//     if ( paper.attr("viewBox") ) {
//         paper._viewBox = paper.attr("viewBox").vb.split(" ");
//         var viewBoxAspectRatio = paper._viewBox[2] / paper._viewBox[3];
//         // console.log('viewBoxAspectRatio ');
//         // console.log(viewBoxAspectRatio);

//         // according to experiment and observation, aspect ratio also affect the viewbox/paper dragging distance
//         ft.attrs.viewBoxAspectRatio = {
//             x: paper._viewBox[3] > paper._viewBox[2] ? viewBoxAspectRatio : 1,
//             y: paper._viewBox[2] > paper._viewBox[3] ? viewBoxAspectRatio : 1
//         }

//         ft.attrs.viewBoxAspectRatioSingle = viewBoxAspectRatio;

//         ft.attrs.viewBoxRatio = {
//             x: paper._viewBox[2] / getPaperSize().w,
//             y: paper._viewBox[3] / getPaperSize().h
//         };
//         // console.log(paper._viewBox[2]);
//         // console.log(ft.attrs.viewBoxRatio);
//     }
//     // console.log("+++++++++++++++++++++++++++++++");
//     // console.log(ft.attrs.viewBoxRatio);
//     // console.log(getPaperSize());
// }

// function applyLimits(ele) {
//     console.log("apply limits");
//     var paper = ele.paper,
//         bbox = paper.getBBox();
//     updateViewBoxRatio( paper );

//     // Keep center within boundaries
//     // console.log(ft.opts.boundary);
//     if ( ft.opts.boundary ) {
//         console.log('apply boundary');
//         var b = ft.opts.boundary;

//         if( paper._viewBox && ( ft.opts.boundary.width === null && ft.opts.boundary.height === null )) {
//             // console.log(ft.attrs.viewBoxRatio.x);
//             b.width = getPaperSize().x * ft.attrs.viewBoxRatio.x;
//             b.height = getPaperSize().y * ft.attrs.viewBoxRatio.y;
//         } 
//         else {
//             b.width  = ( b.width  || getPaperSize().x );
//             b.height = ( b.height || getPaperSize().y );
//         }

//         // left edge
//         if ( ft.attrs.center.x + ft.attrs.translate.x < b.x            ) { 
//             ft.attrs.translate.x += b.x - ( ft.attrs.center.x + ft.attrs.translate.x ); }

//         // top edge
//         if ( ft.attrs.center.y + ft.attrs.translate.y < b.y            ) { 
//             ft.attrs.translate.y += b.y -            ( ft.attrs.center.y + ft.attrs.translate.y ); }

//         // right edge
//         if ( ft.attrs.center.x + ft.attrs.translate.x > b.x + b.width  ) { 
//             console.log("b.width " + b.width);
//             ft.attrs.translate.x += b.x + b.width  - ( ft.attrs.translate.x + ft.attrs.center.x ); 
//             // ft.attrs.translate.x += b.x + b.width  - ( ft.attrs.center.x + ft.attrs.translate.x ); 
//         }

//         // bottom edge
//         if ( ft.attrs.center.y + ft.attrs.translate.y > b.y + b.height ) { 
//             ft.attrs.translate.y += b.y + b.height - ( ft.attrs.center.y + ft.attrs.translate.y ); 
//         }
//     }

//     // Snap to angle, rotate with increments
//     dist = Math.abs(ft.attrs.rotate % ft.opts.snap.rotate);
//     dist = Math.min(dist, ft.opts.snap.rotate - dist);

//     if ( dist < ft.opts.snapDist.rotate ) {
//         ft.attrs.rotate = Math.round(ft.attrs.rotate / ft.opts.snap.rotate) * ft.opts.snap.rotate;
//     }

//     // Snap to scale, scale with increments
//     dist = {
//         x: Math.abs(( ft.attrs.scale.x * ft.attrs.size.x ) % ft.opts.snap.scale),
//         y: Math.abs(( ft.attrs.scale.y * ft.attrs.size.x ) % ft.opts.snap.scale)
//     };

//     dist = {
//         x: Math.min(dist.x, ft.opts.snap.scale - dist.x),
//         y: Math.min(dist.y, ft.opts.snap.scale - dist.y)
//     };

//     // Limit range of rotation
//     // console.log(ft.opts.range.rotate );
//     if ( ft.opts.range.rotate ) {
//         console.log('limit range rotate');
//         // console.log(ft.opts.range.rotate[0]);
//         // console.log(ft.opts.range.rotate[1]);
//         var deg = ( 360 + ft.attrs.rotate ) % 360;

//         if ( deg > 180 ) { deg -= 360; }

//         if ( deg < ft.opts.range.rotate[0] ) { ft.attrs.rotate += ft.opts.range.rotate[0] - deg; }
//         if ( deg > ft.opts.range.rotate[1] ) { ft.attrs.rotate += ft.opts.range.rotate[1] - deg; }
//     }

//     // Limit scale
//     if ( ft.opts.range.scale ) {
//         // console.log('limit scale');
//         console.log(ft.attrs.size);
//         if ( ft.attrs.scale.x * ft.attrs.size.x < ft.opts.range.scale[0] ) {
//             // console.log('1');
//             ft.attrs.scale.x = ft.opts.range.scale[0] / ft.attrs.size.x;
//         }

//         if ( ft.attrs.scale.y * ft.attrs.size.y < ft.opts.range.scale[0] ) {
//             // console.log('2');
//             ft.attrs.scale.y = ft.opts.range.scale[0] / ft.attrs.size.y;
//         }

//         if ( ft.attrs.scale.x * ft.attrs.size.x > ft.opts.range.scale[1] ) {
//             // console.log('3');
//             // console.log(ft.attrs.scale.x);
//             // console.log(ft.attrs.size.x);
//             // console.log(ft.opts.range.scale[1]);
//             ft.attrs.scale.x = ft.opts.range.scale[1] / ft.attrs.size.x;
//         }

//         if ( ft.attrs.scale.y * ft.attrs.size.y > ft.opts.range.scale[1] ) {
//             // console.log('4');
//             ft.attrs.scale.y = ft.opts.range.scale[1] / ft.attrs.size.y;
//         }

//         // console.log(ft.attrs.scale.x);
//     }
// }

(function () {
    var app = angular.module('shared.directives', []);

    
})();