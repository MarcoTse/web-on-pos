(function () {
    var app = angular.module('shared.templates', ['tmpl/order-list.html', 'tmpl/partials.menu-toggle.html', 'tmpl/partials.menu-link.html', 'tmpl/partials.menu-function.html']);

    angular.module('tmpl/order-list.html', []).run(["$templateCache", function ($templateCache) {
      $templateCache.put("tmpl/order-list.html",
        '<div class="order__list-order-list col order-list">\
		  <ul class="list-group 123">\
		  \
		  <!-- disabled -->\
		  <!-- ng-hide="item.getQtyWithVoid() <= 0 && isNotModeItemDisplay1(\'detail\') || item.getTotalPriceWithVoid() == 0 && item.type != \'T\' && isNotModeItemDisplay2(\'zeroItem\')" -->\
		  <!-- ng-hide="item.getQtyWithVoid() <= 0 && isNotModeItemDisplay1(\'detail\') || item.getTotalPriceWithVoid() == 0 && item.type != \'T\' && isNotModeItemDisplay2(\'zeroItem\')" -->\
		  <!-- disabled -->\
		    <li class="list-group-item for-find-order-list-item" ng-repeat="item in MainService.Cart.orderList track by $index"  ng-click="item.editItem($event);" ng-class="{active : item.isEdit, fire: item.checkIsFireItem()}" ng-hide="(item.getQtyWithVoid() <= 0 && isNotModeItemDisplay1(\'detail\')) || (item.getTotalPriceWithVoid() == 0 && isModeItemDisplay2(\'zeroItem\') && item.type != \'T\')">\
		       \
                <span style="display:none;" ng-repeat="temp in item.checkIsFireItem();"></span>\
		      <div layout="row" class="order-list-item">\
		         <div flex="35" class="index" ng-show="isModeItemDisplay1(\'detail\')">#{{item.index}} <span class="index" ng-show="isModeItemDisplay1(\'detail\') && item.voidIndex != -1">^{{item.voidIndex}}</span></div>\
		         <span flex class="code" ng-show="isModeItemDisplay1(\'detail\')">{{item.code}}</span>\
		         <i class="md-icon delete" ng-click="item.deleteItem($event,\'{{$index}}\')" ng-if="isModeOrder(\'order\')">highlight_off</i>\
		       \
		         <!-- for font:20 and screen in 1024, it is not enough space to display servant and time here -->\
		         \
		         <!-- disabled -->\
		         <!-- <div flex="35" class="order-trace">\
		           <span class="employee">Mui</span>@<span class="time">18:55:55</span>\
		         </div> -->\
		         <!-- disabled -->\
		        \
		      </div>\
		     \
		      <!-- repeatable for food name with price, qty-->\
		      <ul class="ordered__food__info-container no-padding">\
		       \
		          <!-- ng-repeat should go here for more than 1 food -->\
		          <li layout="row" class="food-info">\
		             \
		              <!-- for 1024 space, flex set to 55, overflow hidden, add support to responsive later -->\
		              <div flex="50" layout="column">\
		                  <span class="name"><a href="#">{{item.getName()}}</a></span>\
		                  <ul class="modifier void-remark" ng-show="item.voidRemark !=\'\'">\
		                      <!-- ng-repeat should go here for more than 1 modifier -->\
		                      <li>{{item.voidRemark}}</li>\
		                  </ul>\
		                  <ul class="modifier attention child-list" ng-show="item.modifier.length != 0">\
		                      <!-- ng-repeat should go here for more than 1 modifier -->\
		                      <li ng-repeat="m in item.modifier">{{m.label}}</li>\
		                  </ul>\
		                  <ul class="kitchenMsg warn child-list" ng-show="item.kitchenMsgOld.length != 0">\
		                      <!-- ng-repeat should go here for more than 1 modifier -->\
		                      <li ng-repeat="k in item.kitchenMsgOld track by $index">{{k.label}}</li>\
		                  </ul>\
		                  <ul class="kitchenMsg warn child-list" ng-show="item.kitchenMsg.length != 0">\
		                      <!-- ng-repeat should go here for more than 1 modifier -->\
		                      <li ng-repeat="k in item.kitchenMsg track by $index">{{k.label}}<a href="#" ng-click="MainService.Cart.deleteKitchenMsg(item.kitchenMsg, $index, $event)" ng-show="!MainService.isCustomerDisplay;"><i class="md-icon remove">remove_circle</i></a></li>\
		                  </ul>\
		              </div>\
		              <div flex layout="column">\
		                  <span class="qty">{{item.getQtyWithVoid()}}</span>\
		              </div>\
		             \
		              <!-- display support up to 5 digits XX,XXX -->\
		              <div flex="35" layout="column">\
		                  <span class="price">{{ item.getTotalPriceWithVoid(true) }}</span>\
		              </div>\
		          </li>\
		          <ul class="food_info-option-list">\
                    <li ng-repeat="d in item.discount track by $index">\
                        <ul class="food_info-option_list-option sub-child-list">\
                            <li layout="row" class="order_list-option food-info">\
                                <div flex="50" layout="column">\
		                          <span class="name"><a href="">{{d.getName()}}</a></span>\
		                        </div>\
                                <div flex layout="column">\
		                          <span class="qty">{{d.qty}}</span>\
		                        </div>\
		                      <div flex="35" layout="column">\
		                          <span class="price"></span>\
		                      </div>\
                            <\li>\
                        </ul>\
                    </li>\
		            <li ng-repeat="optionList in item.option track by $index" ng-class="{active : option.isEdit}">\
		              <ul class="food_info-option_list-option sub-child-list">\
		               \
		                <!-- disabled -->\
		                <!--<li layout="row" ng-repeat="option in optionList.items track by $index" class="order_list-option food-info" ng-if="option.qty > 0 && option.code ==\'O9999\'" ng-click="MainService.Cart.selectOption( $event, option.code, option, item);">\
		                  <div flex="50" layout="column">\
		                      <span class="name"><a href="#">{{option.getName()}}</a></span>\
		                      <ul class="option-modifier"></ul>\
		                  </div>\
		                  <div flex layout="column">\
		                      <span class="qty">{{option.qty}}</span>\
		                  </div>\
		                  <div flex="35" layout="column">\
		                      <span class="price"></span>\
		                  </div>\
		                </li>-->\
		                <!-- disabled -->\
		               \
		               <!-- disabled -->\
		                <!--<li layout="row" ng-repeat="option in optionList.items track by $index" class="order_list-option food-info" ng-if="option.qty > 0 && option.code !=\'O9999\'">-->\
		                <!-- disabled -->\
		                \
		                <li layout="row" ng-repeat="option in optionList.items track by $index" class="order_list-option food-info" ng-if="option.qty > 0" ng-class="{active : option.isEdit}">\
		                  <div flex="50" layout="column">\
		                  	<!--  {{option.getIndex()}} -->\
		                      <span class="name option-list-test"><a class="existing__order__list order_list-option-action" href="#" ng-click="MainService.Cart.orderListEditOption($event, option.getIndex(), item, optionList, option);">{{option.getName()}}</a></span>\
		                      <ul class="option-modifier modifier attention child-list" ng-show="option.modifier.length != 0">\
		                          <!-- ng-repeat should go here for more than 1 modifier -->\
		                          <li ng-repeat="m in option.modifier">{{m.label}}</li>\
		                      </ul>\
		                      <ul class="option-kitchenMsg option-kitchenMsg-old warn child-list" ng-show="option.kitchenMsgOld.length != 0">\
		                          <!-- ng-repeat should go here for more than 1 modifier -->\
		                          <li ng-repeat="k in option.kitchenMsgOld track by $index">{{k.label}}</li>\
		                      </ul>\
		                      \
		                      <ul class="option-kitchenMsg option-kitchenMsg-new warn child-list" ng-show="option.kitchenMsg.length != 0">\
		                          <!-- ng-repeat should go here for more than 1 modifier -->\
		                          <li ng-repeat="k in option.kitchenMsg track by $index">{{k.label}}<a href="#" ng-show="!MainService.isCustomerDisplay;" ng-click="MainService.Cart.deleteKitchenMsg(option.kitchenMsg, $index, $event)"><i class="md-icon remove">remove_circle</i></a></li>\
		                      </ul>\
		                  </div>\
		                  <div flex layout="column">\
		                      <span class="qty">{{option.qty}}</span>\
		                  </div>\
		                  <div flex="35" layout="column">\
		                      <span class="price"></span>\
		                  </div>\
		                </li>\
		              </ul>\
		            </li>\
		          </ul>\
		          <ul class="ordered__food__info-subitem-list child-list">\
			          <li layout="row" ng-repeat="subitem in item.subitem track by $index" class="food-info" ng-if="subitem.qty > 0" ng-class="{active : subitem.isEdit}">\
			              <div flex="50" layout="column">\
			                  <span class="name"><a href="#" ng-click="MainService.Cart.cartListEditSubitem($event, subitem.getIndex(), item, item.subitem, subitem);">{{subitem.getName()}}</a></span>\
			                 \
			                  <ul class="subitem-modifier modifier attention child-list" ng-show="subitem.modifier.length != 0">\
			                      <!-- ng-repeat should go here for more than 1 modifier -->\
			                      <li ng-repeat="m in subitem.modifier">{{m.label}}</li>\
			                  </ul>\
			                  <ul class="subitem-kitchenMsg subitem-kitchenMsg-old warn child-list" ng-show="subitem.kitchenMsgOld.length != 0">\
			                      <li ng-repeat="sk in subitem.kitchenMsgOld track by $index">{{sk.label}}</li>\
			                  </ul>\
			                  <ul class="subitem-kitchenMsg subitem-kitchenMsg-new warn child-list" ng-show="subitem.kitchenMsg.length != 0">\
			                      <li ng-repeat="sk in subitem.kitchenMsg track by $index">{{sk.label}}<a href="#" ng-show="!MainService.isCustomerDisplay;" ng-click="MainService.Cart.deleteKitchenMsg(subitem.kitchenMsg, $index, $event)"><i class="md-icon remove">remove_circle</i></a></li>\
			                  </ul>\
			              </div>\
			              <div flex layout="column">\
			                  <span class="qty">{{subitem.qty}}</span>\
			              </div>\
			              <div flex="35" layout="column">\
			                  <span class="price"></span>\
			              </div>\
			          </li>\
		      	  </ul>\
		           \
		      </ul>\
		     \
		      <ul class="details no-padding" layout="column" style="" ng-show="isModeItemDisplay1(\'detail\')">\
		          <li class="order-trace">\
		              <span class="employee">{{item.staff}}</span>@<span class="time">{{item.time}}</span>\
		          </li>\
		      </ul>\
		    </li>\
		    <li class="list-group-item new-item test1" ng-repeat="item in MainService.Cart.cartList track by $index" ng-if="!item.isDelete" ng-class="{active : item.isEdit}" itemid="{{item.itemKey}}">\
		        <!-- one order# per order -->\
		      <div layout="row" class="order-list-item" ng-if="!MainService.checkIsTakeAwayMode();">\
                    <span ng-show="item.comboItemKey" style="color:red">{{ui.txt_composite_option}}:{{item.itemKey}} [{{ui.txt_composite_combo}}:{{item.comboItemKey}}]</span>\
                    <span ng-show="!item.comboItemKey && item.option" style="color:rgb(121,85,72)">{{ui.txt_composite_combo}}:{{item.itemKey}}</span>\
                    <span ng-show="!item.comboItemKey && !item.option" style="color:#26a69a">{{ui.txt_composite_food}}:{{item.itemKey}}</span>\
		         <div flex="35" class="index" ng-show="isModeItemDisplay1(\'detail\')">#{{$index + 1}} <span class="index" ng-show="isModeItemDisplay1(\'detail\') && item.voidIndex != -1">^{{item.voidIndex}}</span></div>\
		         <span flex class="code" ng-show="isModeItemDisplay1(\'detail\')">{{item.code}}</span>\
		         <i class="md-icon delete" ng-click="item.deleteItem($event,\'{{$index}}\')" ng-if="isModeOrder(\'order\')">highlight_off</i>\
		       \
		         <!-- for font:20 and screen in 1024, it is not enough space to display servant and time here -->\
		         <!-- <div flex="35" class="order-trace">\
		           <span class="employee">Mui</span>@<span class="time">18:55:55</span>\
		        </div> -->\
		      </div>\
		        <div layout="row">\
		            <span flex="35" class="index" ng-show="modeCheckOrder(\'detail\')">#0[New]</span>\
		            <span flex class="code" ng-show="modeCheckOrder(\'detail\')">{{item.code}}</span>\
                    \
		            <i class="md-icon delete 12345" ng-click="item.deleteItem($event,\'{{$index}}\')" ng-show="MainService.getOtherWriteOrderService().mode != MainService.getOtherWriteOrderService().schema.paymentPage">highlight_off</i>\
                        \
                    <!--<i class="md-icon delete 23456" ng-click="MainService.combo.switchCompositeItemSelectionMode(item);" ng-if="item.comboItemKey" ng-show="MainService.getOtherWriteOrderService().mode != MainService.getOtherWriteOrderService().schema.paymentPage">highlight_off</i>-->\
		        </div>\
		        <!-- repeatable for food name with price, qty-->\
		        <ul class="new__food__info-container no-padding" ng-click="item.editItem($event)">\
		            <!-- ng-repeat should go here for more than 1 food ng-click="item.editItem($event)" -->\
		            <li layout="row" class="food-info" >\
		                <div flex="50" layout="column">\
		                    <span class="name" ><a href="#" style="color:black;">{{item.getName()}}</a></span>\
		                    <ul class="modifier void-remark" ng-show="item.voidRemark !=\'\'">\
		                        <!-- ng-repeat should go here for more than 1 modifier -->\
		                        <li>{{item.voidRemark}}</li>\
		                    </ul>\
		                    <ul class="modifier attention child-list" ng-show="item.modifier.length != 0">\
		                        <!-- ng-repeat should go here for more than 1 modifier -->\
		                        <li ng-repeat="m in item.modifier">{{m.label}}<a href="#" ng-show="!MainService.isCustomerDisplay;" ng-click="MainService.Cart.addModifier(m, $event)"><i class="md-icon remove" >remove_circle</i></a></li>\
		                    </ul>\
		                    <ul class="kitchenMsg warn child-list" ng-show="item.kitchenMsg.length != 0">\
		                        <!-- ng-repeat should go here for more than 1 modifier -->\
		                        <li ng-repeat="k in item.kitchenMsg">{{k.label}}<a href="#" ng-show="!MainService.isCustomerDisplay;" ng-click="MainService.Cart.deleteKitchenMsg(item.kitchenMsg, $index, $event)"><i class="md-icon remove">remove_circle</i></a></li>\
		                    </ul>\
		                </div>\
		                <div flex layout="column">\
		                    <span class="qty">{{ item.getQty() }}</span>\
		                </div>\
		                <div flex="35" layout="column">\
		                    <span class="price" style="color:red">{{ item.getTotalPrice(true) }} </span>\
		                </div>\
		            </li>\
		            \
		            \
		            <ul class="new__food__info-option-list child-list">\
			            <li ng-repeat="optionList in item.option track by $index" ng-class="{active : option.isEdit}">\
			              <ul class="new__food__info-option-list-option food_info-option_list-option sub-child-list" ng-init="repeatCount = 0; "><!-- -->\
			                <li layout="row" ng-repeat="option in optionList.items track by $index" class="order_list-option food-info " ng-if="option.qty > 0 && !option.composite" ng-class="{active : option.isEdit}" ng-init="$localIdx = counter();" ng-click="option.edit($event,$localIdx, item, optionList)" ng-hide="MainService.isCustomerDisplay && option.code ==\'O9999\'" >\
			                  <div flex="50" layout="column">\
			                      <span class="name"><a href="#" >{{option.getName()}}</a><a href="#" ng-show="MainService.getOtherWriteOrderService().mode != MainService.getOtherWriteOrderService().schema.paymentPage"><i class="md-icon remove">create</i></a></span>\
			                      <ul class="option-modifier modifier attention child-list" ng-show="option.modifier.length != 0">\
			                          <!-- ng-repeat should go here for more than 1 modifier -->\
			                          <li ng-repeat="m in option.modifier">{{m.label}}<a href="#" ng-show="!MainService.isCustomerDisplay;" ng-click="MainService.Cart.deleteModifier(option.modifier, $index)"><i class="md-icon remove">remove_circle</i></a></li>\
			                      </ul>\
			                      <ul class="option-kitchenMsg warn child-list" ng-show="option.kitchenMsg.length != 0">\
			                          <!-- ng-repeat should go here for more than 1 modifier -->\
			                          <li ng-repeat="k in option.kitchenMsg">{{k.label}}<a ng-show="!MainService.isCustomerDisplay;" href="#" ng-click="MainService.Cart.deleteKitchenMsg(option.kitchenMsg, $index, $event)"><i class="md-icon remove">remove_circle</i></a></li>\
			                      </ul>\
			                  </div>\
			                  <div flex layout="column">\
			                      <span class="qty">{{ option.qty }}</span>\
			                  </div>\
			                  <div flex="35" layout="column">\
			                      <span class="price"></span>\
			                  </div>\
			                </li>\
			              </ul>\
			            </li>\
		            </ul>\
		             \
		            <ul class="new__food__info-subitem-list child-list">\
			            <li layout="row" ng-repeat="subitem in item.subitem track by $index" class="food-info" ng-class="{active : subitem.isEdit}" ng-click="MainService.Cart.cartListSelectSubitem( $event, $localIdx, item, item.subitem, subitem );">\
			                <div flex="50" layout="column">\
			                    <span class="name"><a href="#">{{ subitem.getName() }}</a></span>\
			                    <ul class="subitem-modifier modifier attention child-list" ng-show="subitem.modifier.length != 0">\
			                        <!-- ng-repeat should go here for more than 1 modifier -->\
			                        <li ng-repeat="m in subitem.modifier">{{m.label}}<a href="#" ng-show="!MainService.isCustomerDisplay;" ng-click="MainService.Cart.deleteModifier(subitem.modifier, $index)"><i class="md-icon remove">remove_circle</i></a></li>\
			                    </ul>\
			                    <ul class="subitem-kitchenMsg warn child-list" ng-show="subitem.kitchenMsgOld.length != 0">\
			                        <li ng-repeat="sk in subitem.kitchenMsgOld track by $index">{{sk.label}}</li>\
			                    </ul>\
			                    <ul class="subitem-kitchenMsg subitem-kitchenMsg-new warn child-list" ng-show="subitem.kitchenMsg.length != 0">\
			                        <li ng-repeat="sk in subitem.kitchenMsg track by $index">{{sk.label}}<a href="#"  ng-show="!MainService.isCustomerDisplay;" ng-click="MainService.Cart.deleteKitchenMsg(subitem.kitchenMsg, $index, $event)"><i class="md-icon remove">remove_circle</i></a></li>\
			                    </ul>\
			                </div>\
			                <div flex layout="column">\
			                    <span class="qty">{{ subitem.qty }}</span>\
			                </div>\
			                <div flex="35" layout="column">\
			                    <span class="price"></span>\
			                </div>\
			            </li>\
		            </ul>\
		        </ul>\
		    </li>\
		  </ul>\
		</div>');
    }]);
    
   
	// menu_toggle.directive.js
	angular.module('tmpl/partials.menu-toggle.html', []).run(["$templateCache", function ($templateCache) {
	      $templateCache.put('tmpl/partials.menu-toggle.html',
	        '<md-button md-no-ink="true"   ng-if="checkChild(section)" class="md-button-toggle"\n' +
	        '  ng-click="toggle()"\n' +
	        '  aria-controls="docs-menu-{{section.label | nospace}}"\n' +
	        '  flex layout="row"\n' +
	        '  aria-expanded="{{isOpen()}}">\n' +
	        '  {{ui[section.label]}}\n' +
	        '  <span aria-hidden="true" class=" pull-right fa fa-chevron-down md-toggle-icon"\n' +
	        '  ng-class="{\'toggled\' : isOpen()}"></span>\n' +
	        '</md-button>\n' +
	        '<ul ng-show="isOpen()" id="docs-menu-{{section.label | nospace}}" class="menu-toggle-list">\n' +
	        '  <li ng-repeat="page in section.pages">\n' +
	        '    <menu-link section="page" ng-if="page.type === \'link\'"></menu-link>\n' +
	        '    <menu-function section="page" ng-if="page.type === \'function\' && page.config === true"></menu-function>\n' +
	        '  </li>\n' +
	        '</ul>\n' +
	        '');
    }])
	app.directive('menuToggle', function ( $timeout, uiLanguage, INFO ) {
      return {
        scope: {
          section: '='
        },
        templateUrl: 'tmpl/partials.menu-toggle.html',
        link: function (scope, element) {
          scope.ui = uiLanguage[INFO.lang];

          var controller = element.parent().controller();
          // console.log(element);
          // console.log(element.parent());
          // console.log(element.parent().controller());

          scope.isOpen = function () {
          	// console.log(controller.isOpen(scope.section));
            return controller.isOpen(scope.section);
          };
          scope.toggle = function () {
            controller.toggleOpen(scope.section);
          };
          
          var parentNode = element[0].parentNode.parentNode.parentNode;
          if (parentNode.classList.contains('parent-list-item')) {
            var heading = parentNode.querySelector('h2');
            element[0].firstChild.setAttribute('aria-describedby', heading.id);
          }

          scope.checkChild = function (obj) {
              //console.log(":checkChildcheckChildcheckChildcheckChildcheckChildcheckChild");
              //console.log(obj);
              var havaVisibleChild = false;
              for (var i = 0; i < obj.pages.length; i++) {
                  if (obj.pages[i].config) {
                      havaVisibleChild = true;
                      break;
                  }
              }
              return havaVisibleChild;
          }

        }
      };
    });

	// menulink.directive.js
	angular.module('tmpl/partials.menu-link.html', []).run(["$templateCache", function ($templateCache) {
      $templateCache.put('tmpl/partials.menu-link.html',
        '<md-button md-no-ink="true" ng-class="{\'{{section.icon}}\' : true}" ui-sref-active="active" \n' +
        '  ui-sref="{{section.state}}" ng-click="focusSection()">\n' +
        '  {{section | humanizeDoc}}\n' +
        '  <span  class="md-visually-hidden "\n' +
        '    ng-if="isSelected()">\n' +
        '    current page\n' +
        '  </span>\n' +
        '</md-button>\n' +
        '');
    }]);
    app.directive('menuLink', function () {
      return {
        scope: {
          section: '='
        },
        templateUrl: 'tmpl/partials.menu-link.html',
        link: function (scope, element) {
          var controller = element.parent().controller();

          scope.focusSection = function () {
            // set flag to be used later when
            // $locationChangeSuccess calls openPage()
            controller.autoFocusContent = true;
          };
        }
      };
    });

	// menu function.directive
	angular.module('tmpl/partials.menu-function.html', []).run(["$templateCache", function ($templateCache) {
      $templateCache.put('tmpl/partials.menu-function.html',
        '<md-button md-no-ink="true" ng-class="{\'{{section.icon}}\' : true}" \n' +
        '  ng-click="section.func()">\n' +
        '  {{ui[section.label]}}<!--{{section | humanizeDoc}}-->\n' +
        '  <span  class="md-visually-hidden "\n' +
        '    ng-if="isSelected()">\n' +
        '    current page\n' +
        '  </span>\n' +
        '</md-button>\n' +
        '');
    }]);
    app.directive('menuFunction', function (uiLanguage, INFO) {
      return {
        scope: {
          section: '='
        },
        templateUrl: 'tmpl/partials.menu-function.html',
        link: function (scope, element) {
          var controller = element.parent().controller();
          scope.ui = uiLanguage[INFO.lang];

          scope.focusSection = function () {
            // set flag to be used later when
            // $locationChangeSuccess calls openPage()
            controller.autoFocusContent = true;
          };
        }
      };
    });
})();