var $test; // needle to test in console
// make it a global container

function isDefined( obj ) {
    if( typeof obj != 'undefined') {
        return true;
    }
}

function isUndefined() {
    return !isDefined.apply(null, arguments);
}

$rootScope = {};
(function ($) {
    $(document).ready(function ($) {

        //socket version
        socket = io.connect('http://' + location.hostname + ':5000');
        io.Manager('http://' + location.hostname + ':5000', { reconnect: true });
     
          socket.on('connect', function () {
              console.log('connected')
              socket.emit('refreshshopdisplay', { topic: 'allticketno' });
          });
          socket.on('disconnect', function () { console.log('disconnected'); }); //reconnect*/
        // socket.on('refreshTable', function (msg) {
        //     console.log("socket refreshTable incoming");
        // });



        // bind data to form element according to fieldtype, target jQuery object
        // used in loadItemDetails
        function _bindData(targetObj, value, fieldType) {
            // textfield and color area different
            var fieldType = targetObj.attr('type');
            // console.log(fieldType);
            // bail
            if (typeof value === 'undefined' || typeof fieldType === 'undefined' || typeof targetObj != 'object')
                return;

            switch (fieldType) {
                case 'password':
                case 'email':
                case 'text':
                    // console.log("bind value to input");
                    targetObj.val(value);
                    break;
                case 'color-option':
                    // console.log("bind value to color option");
                    // console.log($(targetObj).find('.item-color-option.select-wrapper input.select-dropdown'));
                    updateDisplayField($(targetObj).find('.item-color-option.select-wrapper input.select-dropdown'), value);
                    break;
            }
        }

        // get value from table field in specified format
        // used in button.save
        function getfieldVal(fieldClass) {
            // console.log($(this));
            // console.log($(this).find(fieldClass));
            // console.log($(this).find( fieldClass + ' .value'));
            return $(this).find(fieldClass).text() || $(this).find(fieldClass + ' .value').text() || $(this).find(fieldClass + ' .value').attr('title')
        }

        function bindDataToForm($targetForm) {
            // var $targetForm = $("#item-details");
            if ($targetForm.length === 0)
                return;
            var row = this
            $row = $(this);


            // this is binded to this function with FN.apply( XXX ) where this is the selected table row
            // console.log(this);
            // console.log($(this));
            // console.log( getfieldVal.apply( $row, ['.code'] ) );
            $targetForm.find('form').data('selected-row-id', $row.data('row-pos'));
            // console.log($targetForm.find('form').data());
            _bindData($targetForm.find('#code'), getfieldVal.apply(row, ['.code']));
            _bindData($targetForm.find('#group'), getfieldVal.apply(row, ['.group']));
            _bindData($targetForm.find('#name1'), getfieldVal.apply(row, ['.name1']));
            _bindData($targetForm.find('#name2'), getfieldVal.apply(row, ['.name2']));
            _bindData($targetForm.find('#kitchen-name'), getfieldVal.apply(row, ['.label']));
            _bindData($targetForm.find('#printer'), getfieldVal.apply(row, ['.printer']));
            _bindData($targetForm.find('.item-color-option-container'), getfieldVal.apply(row, ['.color']));

            // load subitem list in THIS item
            // $scope.renderSubItemList( loadedData.subitem );
        }

        function bindDataToTable() {

        }

        // eg. getItemByCode( SO, SO29, [] )
        // return target object
        // console.log(getItemByCode( 'SO29', data.category ));
        function getItemByCode(cat, code, dataArr) {
            // bail immediately if no param
            if (isUndefined(code) || isUndefined(dataArr))
                return;

            // console.log(dataArr);
            // var cat = code.substr(0, 2);
            // console.log(code);
            // console.log(cat);
            console.log(dataArr);

            for (var i = 0; i < dataArr.length; i++) {
                if (dataArr[i].code.toLowerCase() === cat.toLowerCase()) {
                    for (var j = 0; j < dataArr[i].items.length; j++) {
                        // console.log(dataArr[i].items[j]);
                        if (dataArr[i].items[j].code.toLowerCase() === code.toLowerCase()) {
                            // console.log("found me?");
                            // console.log(dataArr[i].items[j]);
                            return dataArr[i].items[j];
                        }
                    }
                    // for( var key in dataArr[i] ) {

                    // }
                    // return itemList[i];
                }
            }
        };

        function getItemObjByRowID(rowID, dataArr) {
            for (var i = 0; i < dataArr.length; i++) {
                for (var j = 0; j < dataArr[i].items.length; j++) {
                    // console.log(dataArr[i].items[j]);
                    if (dataArr[i].items[j].rowID === rowID) {
                        return dataArr[i].items[j];
                    }
                }
            }
        }

        function getEmployeeByCode(code, dataArr) {
            // bail immediately if no param
            if (typeof code === 'undefined' || typeof dataArr != 'object')
                return;

            // console.log(dataArr);
            // var cat = code.substr(0, 2);
            // console.log(code);
            // console.log(cat);
            console.log(dataArr);

            for (var i = 0; i < dataArr.length; i++) {
                if (dataArr[i].XF_STAFFCODE.toLowerCase() === code.toLowerCase()) {
                    return dataArr[i];
                }
            }
        }

        /* 
          make a root so the child can communicate effectively when in need
          scope isolation for easy management
        */

        $rootScope.keymap = {
            ESC: 27,
            ENTER: 13,
            TAB: 9,
            SHIFT: 16, // left and right are same
            CTRL: 17,
            ALT: 18,
            F5: 116,
            F8: 119,
            F11: 122,
            ARROW_LEFT: 37,
            ARROW_UP: 38,
            ARROW_RIGHT: 39,
            ARROW_DOWN: 40,
            INSERT: 45
        };

        $rootScope.helper = (function () {
            var helper = {};

            // add event to rendered fields
            helper.makeFieldEditable = function ($scope, $tableObj) {
                // console.log($scope);
                if (isUndefined($scope) || isUndefined($tableObj))
                    return;

                // console.log('$tableObj ');
                console.log($tableObj);
                var $parent = $tableObj;
                // console.log('makeEditable');
                $tableObj.find('.editable[data-field-type="textfield"]').makeEditable({
                    // rowCallback: foodRowCallback,
                    $parentLock: $parent, // only matched parent will trigger
                    $scope: $scope // for some callbacks using $scope to access original caller scope for data access and sharing
                });

                $tableObj.find('.editable[data-field-type="checkbox"]').makeEditable({
                    // rowCallback: foodRowCallback,
                    $parentLock: $parent, // only matched parent will trigger
                    $scope: $scope // for some callbacks using $scope to access original caller scope for data access and sharing
                });

                // employee category
                $tableObj.find('.editable.group[data-field-style="normal"]').makeEditable({
                    $parentLock: $parent, // only matched parent will trigger
                    dropdown: {
                        cssClass: "group-option", // select css class
                        optionData: $scope.employeeGroup,
                        optionKey: null,
                        type: "employee-group-option",
                        // rowCallback: foodRowCallback,
                        $scope: $scope
                    }
                });

                $tableObj.find('.editable.type[data-field-style="normal"]').makeEditable({
                    $parentLock: $parent, // only matched parent will trigger
                    dropdown: {
                        cssClass: "type-option", // select css class
                        optionData: $scope.employeeType,
                        optionKey: null,
                        type: "employee-type-option",
                        // rowCallback: foodRowCallback,
                        $scope: $scope
                    }
                });

                // item list
                $tableObj.find('.editable[data-field-style="color"]').makeEditable({
                    $parentLock: $parent, // only matched parent will trigger
                    dropdown: {
                        cssClass: "color-option", // select css class
                        optionData: $scope.itemColor,
                        optionKey: "name",
                        type: "item-color-option",
                        // rowCallback: foodRowCallback,
                        $scope: $scope
                    }
                });

                $tableObj.find('.editable.category[data-field-style="normal"]').makeEditable({
                    $parentLock: $parent, // only matched parent will trigger
                    dropdown: {
                        cssClass: "category-option", // select css class
                        optionData: $scope.data.category,
                        optionKey: "code",
                        type: "item-category-option",
                        // rowCallback: foodRowCallback,
                        $scope: $scope
                    }
                });
            }; // makeFieldEditable

            // data manipulation helpers
            // when item is saved, update oval and reset status to reflect the change? Or just refresh screen? (easier)
            helper.updateOVal = function ($target, new_val) {

            };

            helper.isModified = function (oVal, newVal) {
                return oVal != newVal;
            };

            helper.chkModified = function (element, newValue) {
                console.log('-------------->>>>>chkModified');
                console.log($(element).data('oval'));
                console.log(newValue);
                if (!this.isModified($(element).data('oval'), newValue)) {
                    console.log("not modified");
                    this.unSetRowStatus(element, 'modified');
                }
            };

            helper.toggleRowStatus = function (element, status) {
                $(element).toggleClass(status);
                $(element).parents('tr').toggleClass(status);
            };

            helper.setRowStatus = function (element, status) {
                $(element).addClass(status);
                $(element).parents('tr').addClass(status);
            };

            helper.unSetRowStatus = function (element, status) {
                $(element).removeClass(status);
                $(element).parents('tr').removeClass(status);
            };

            helper.getOValue = function (element) {
                var fieldType = $(element).data('field-type'),
                    oVal = $(element).find('.value:eq(0)').attr('title') || $(element).find('.value:eq(0)').text() || $(element).text();
                // var oVal = element.text() || element.find('.value:eq(0)').text() || element.find('.value:eq(0)').attr('title');
                // console.log('>>>>>>>>>>>>>>>>>>>oVal ' + oVal);
                // console.log($( element ).find('.value:eq(0)').attr('title'));
                // console.log($( element ).find('.value:eq(0)').text());
                // console.log($( element ).text());
                if (fieldType === 'dropdown') {
                    return $(element).find('.value:eq(0)').text() || $(element).text();
                }
                if (fieldType === 'checkbox') {
                    return this.getCheckValue(oVal);
                } if ($(element).hasClass('helper')) {
                    // console.log('hahahahaha ' + $(this).text());
                    return $(this).text();
                } else {
                    return oVal;
                }
            };

            helper.setOValue = function (element) {
                if (typeof $(element).data('oval') === 'undefined') {
                    console.log('set me oval');
                    // console.log(element);
                    // console.log(this.getOValue( element ));
                    // console.log("set oValue");
                    // console.log( this.getOValue( element ) );
                    $(element).data('oval', this.getOValue(element));
                    $(element).attr('data-oVal', this.getOValue(element));
                    // console.log($(element).data());
                }
            };

            helper.getCheckValue = function (value) {
                if (typeof value === 'undefined')
                    return;

                if (value === 'done')
                    return true;
                else
                    return false;
            };

            helper.updateTableSorter = function (target) {
                $(target).trigger('update', [true, noop]); // trigger update for tablesorter
            };

            helper.isFiltered = function ($tableObj) {
                if ($tableObj.find('.filtered').length > 0)
                    return true;
                else
                    return false;
            };

            helper.prepareSaveData = function () {
                // consolidate modified data and new data
            }

            // helper.addRow( '#item-list-container table', )
            /*
             helper.addRow( {
               target: '#employee-list-container table',
               rowTemplate: '<tr class=""> \
               <td class="helper"></td> \
               <td class="editable code" data-field-type="textfield">NEW</td> \
               <td class="editable" data-field-type="textfield">NEW</td> \
               <td class="editable" data-field-type="textfield"><span class="value">NEW</span></td> \
               <td class="editable type dropdown" data-field-type="dropdown" data-field-style="normal"><span class="value">4</span></td> \
               <td class="editable group dropdown" data-field-type="dropdown" data-field-style="normal">AD</td></tr>',
               dataAbstract: {
                 XF_NAME: "New Employee Name"
                 XF_PASSWORD: "0000"
                 XF_STAFFCODE: "0000"
                 XF_STAFFTYPE: "0"
                 XF_USERGROUP: "AD"
               }
             })
            */
            helper.addRow = function (settings) {
                console.log('add row');
                if (typeof settings === 'undefined' || typeof settings != 'object')
                    return;

                console.log('add row 1');
                console.log(settings);
                // target, rowTemplate, dataAbstract
                if (typeof settings.target === 'undefined' || typeof settings.rowTemplate === 'undefined' || typeof settings.dataAbstract === 'undefined' || typeof settings.$scope === 'undefined')
                    return;

                console.log('add row 2');
                var target = settings.target,
                    rowTemplate = settings.rowTemplate,
                    dataAbstract = settings.dataAbstract,
                    bindUid = settings.bindUid ? settings.bindUid : 'row',
                    $scope = settings.$scope,
                    $table = $(target) ? $(target) : $('#item-list-container table'), // demo
                    posID = $table.find('tbody tr').length, // total length is the latest ID
                    row = rowTemplate,
                    $row = $(row).addClass('newRow').data('rowid', posID);
                // resort table using the current sort; set to false to prevent resort, otherwise 
                // any other value in resort will automatically trigger the table resort. 
                resort = false;

                console.log('add row to item list');
                $table
                  .find('tbody').prepend($row)
                  .trigger('addRows', [$row, resort])
                // .trigger('update', [ true, [ [1,0] ] ] ); // trigger update for tablesorter [1,0]

                var newItemData = dataAbstract ? dataAbstract : {};

                this.makeFieldEditable($scope, $row);

                // method 1: store in an array, $scope.newItemArr.push( newItemData ); hard to relate it from outside without index
                // method 2: $scope.newItemObj[ rowID ] // can relate it with rowID
                // $scope.newItemObj[posID] = newItemData;
                // method 3: store data object in row // destroy the row will destroy the data together
                $row.data('rowdata', newItemData);
                // $scope.newItemObj[posID] = newItemData;
                // console.log($row.data('rowdata'));
                var item = new Item(bindUid + posID, 'newItemData');

                // loop through the object and assign value to field
                for (var key in newItemData) {
                    // item list need this, if there is more field need to customize, use switch instead
                    switch (key) {
                        case 'name':
                            for (var key2 in newItemData[key]) {
                                // console.log('key ' + key);
                                // console.log('key2 ' + key2);
                                // console.log(newItemData[key][key2]);
                                switch (key2) {
                                    case '001':
                                        var keyName = 'name2';
                                        break;
                                    case '003':
                                        var keyName = 'name1';
                                        break;
                                    default:
                                        break;
                                }
                                item.set(keyName, newItemData[key][key2]);
                            }
                            break;
                        default:
                            item.set(key, newItemData[key]);
                            break;
                    }
                    // if( key === 'name') {
                    //   for( var key2 in newItemData[key] ) {
                    //     console.log('key ' + key);
                    //     console.log('key2 ' + key2);
                    //     // console.log(newItemData[key][key2]);
                    //     item.set( key2, newItemData[key][key2] );
                    //   }
                    // } else {
                    //   item.set( key, newItemData[key] );
                    // }
                }

                scrollToRow($table.parent(), 'tr.newRow:eq(0)');
            };

            helper.changeIndicator = function (action, iconText) {
                if ($(this).length === 0)
                    return;

                iconText = iconText ? iconText : 'chevron_right';
                // if( typeof $(this).data('oval') === 'undefined' ) {
                //   $(this).data('oval', $(this).find('.indicator').text() );
                // }
                switch (action) {
                    case 'remove':
                        $(this)
                          .find('.helper').removeClass('highlight')
                          .find('.indicator').text($(this).data('oval'));
                        break;
                    case 'add':
                        // console.log('add oval to indicator');
                        // $rootScope.helper.setOValue( $(this).find('.helper') );
                        $(this)
                          .find('.helper').addClass('highlight')
                          .find('.indicator').text(iconText);
                        break;
                } // switch
            } // changeIndicator

            helper.test = function () {
                console.log(dataBind);
            }

            helper.bindData2Way = function (settings) {
                var dataBind = settings.dataBind,
                    $target = $(settings.target),
                    // bindKeyArr = settings.bindKeyArr,
                    rowID = settings.rowID,
                    bindUid = settings.bindUid ? settings.bindUid : 'row';

                if ($.isEmptyObject(dataBind))
                    return; // empty, no need to bind

                // console.log(dataBind);
                // $target.attr('data-bind-row'+rowID, bindKey);
                // console.log(bindKey.indexOf('name'));
                var item = new Item(bindUid + rowID, dataBind, $target);
                $target.find('input.data-binder, .option-container.data-binder').each(function (e) {
                    // console.log($(this));
                    var bindKey = $(this).attr('id');
                    $(this).attr('data-bind-' + bindUid + rowID, bindKey);

                    console.log('----------------------------');
                    console.log(bindKey);
                    console.log('----------------------------');

                    switch (bindKey) {
                        case 'color':
                            item.set(bindKey, dataBind['color']);
                            break;
                        case 'name1':
                            console.log('bind chinese name');
                            item.set(bindKey, dataBind.name['003']);
                            break;
                        case 'name2':
                            // console.log('test');
                            item.set(bindKey, dataBind.name['001']);
                            break;
                        default:
                            item.set(bindKey, dataBind[bindKey]);
                    }
                });
            } // bindData2Way

            // event handler helper (畫量 reusable)
            helper.materialSelectHandler = function (e) {
                var rowID = $('#item-details form').data('rowid'),
                    binderKey = $(this).parents('.option-container').attr('data-bind-row' + rowID),
                    newValue = $(this).text(),
                    target = 'td[data-bind-row' + rowID + '=' + binderKey + ']',
                    $target = $(target);

                console.log(rowID);
                console.log(target);
                console.log('binderKey ' + binderKey);
                console.log($target);
                helper.setOValue(target);

                helper.setRowStatus($target, 'modified');
                helper.chkModified($target, newValue);

                $(this).parents('.option-container').find("input.select-dropdown").trigger('fieldChange');
            };

            helper.inputHandler = function (e) {
                // console.log(e);
                var helper = $rootScope.helper,
                    $form = $(e.data.form),
                    bindUid = $form.data('binduid') ? $form.data('binduid') : 'row',
                    rowID = $form.data('rowid'),
                    binderKey = $(this).attr('data-bind-' + bindUid + rowID),
                    newValue = $(this).val(),
                    target = 'td[data-bind-' + bindUid + rowID + '=' + binderKey + ']',
                    $target = $(target);

                // console.log("_originalValue " + $(this).data('_originalValue'));
                // console.log(rowID);
                // console.log('binderKey ' + binderKey);
                // console.log($target);
                // console.log($target.data());

                switch (e.type) {
                    case 'focus':
                        // when focus, only make original value once, so it will not keep updating the original value during changing/typing
                        console.log('focus me');
                        console.log('making original value');
                        if (!$(this).data('_originalValue')) {
                            $(this).data('_originalValue', $target.data('_originalValue') || $(this).val())
                        } else {
                            $(this).data('_originalValue', $(this).val());
                        }
                        console.log('original value ' + $(this).data('_originalValue'));
                        break;
                    case 'keydown':
                        // should be available on all related binding element

                        // if( !$(this).data('_originalValue') ) {
                        //   console.log('store original');
                        //   $(this).data('_originalValue', $(this).val());
                        // }
                        break;
                    case 'keyup':
                        helper.setOValue(target);
                        helper.setRowStatus($target, 'modified');
                        helper.chkModified($target, newValue);

                        switch (e.which) {
                            case $rootScope.keymap.ESC:
                                // reset value
                                // restore original with ESC (right before typing)
                                console.log('ESC');
                                // reset value to previous
                                // true means reset value, 
                                // false means update
                                helper.chkModified($target, $(this).data('_originalValue'));
                                helper.unSetRowStatus($target, 'modified');
                                // $(this).removeData('$(this).data( '_originalValue')');
                                $(this).val($(this).data('_originalValue'));
                                $(this).trigger('keyup'); // trigger an update so the linked TD field will be updated too
                                // // not reset if not same as original
                                return;
                                break;
                            default:
                                break;
                        }
                        break;
                    default:

                        break;
                }
            }

            helper.disableInput = function ($form) {
                if (isUndefined($form))
                    return;
                // add readonly attribute to data-binder field
                $form.find('.data-binder:not([type=checkbox], div)').attr('disabled', true); // div wrapper
                $form.find('.data-binder[type=checkbox]').attr('disabled', true); // checkbox
                $form.find('div.data-binder').find('input').attr('disabled', true); // other input
            }

            helper.TableArrowControl = function (e) {
                var helper = $rootScope.helper;
                console.log('keydown');
                // now using DOM,
                // alternative is using rowID to match, maybe faster
                var keymap = $rootScope.keymap,
                    key = e.which,
                    $row = $(this).parents('tr'),
                    $tableObj = $(this).parents('table'),
                    $prevRow = helper.isFiltered($tableObj) ? $row.prevAll(':not(".filtered"):eq(0)') : $row.prev(),
                    $nextRow = helper.isFiltered($tableObj) ? $row.nextAll(':not(".filtered"):eq(0)') : $row.next();
                // console.log($row);

                switch (key) {
                    case keymap.ARROW_DOWN:
                        // console.log("hahahaha");
                        $nextRow.find('.helper')
                          .focus().click()

                        // $(this).parents('table tbody tr.active').removeClass('active');
                        break;
                    case keymap.ARROW_UP:
                        // console.log("hahahaha");
                        $prevRow.find('.helper').focus().click();
                        break;
                    default:
                        e.preventDefault();
                        break;
                }
            };
            return helper;
        })();

        // create separate scope for item and employee admin
        $rootScope.itemAdmin = (function () {
            // load food information
            // socket.emit('loadStaff', function (r) {
            // $scope.renderItemTable( r, {catSumTarget:"#category-shortlist-container", 
            //                             foodListTarget:"#item-list-container", 
            //                             catListTarget:"#category-list-container",
            //                             subItemListTarget:"#subitem-list"
            //                            }, $scope.afterRender, {} );
            // admin page scope
            // scope for item/food admin
            var $scope = {}; // will save loaded json data here for sharing amongst this scope
            $test = $scope;

            $scope.load = function () {
                var currentScope = this;
                var rootScope = $scope;
                $.ajax({
                    url: "data/food.json",
                    dataType: "JSON",
                    success: function (data) {
                        console.log(currentScope);
                        // console.log(Array.prototype.slice.call(arguments).splice(0, 1).concat(args));
                        // put the data in the first arguments for working with custom render extensibility
                        //$scope[callback].apply($scope, Array.prototype.slice.call(arguments).splice(0, 1).concat(args)) || noop;
                        currentScope.renderItemTable(data, {
                            catSumTarget: "#category-shortlist-container",
                            foodListTarget: "#item-list-container",
                            catListTarget: "#category-list-container",
                            subItemListTarget: "#subitem-list"
                        }, function () {
                            currentScope.afterRender();
                        })
                    }
                });
            }

            $scope.renderItemTable = function (data, targetObj, callback) {
                this.data = data; // pass data to $scope
                // localStorage.foodData = JSON.stringify(data);
                // console.log( JSON.parse(localStorage.foodData) );
                // console.log(arguments);
                // console.log(data);
                // console.log(arguments);
                var contextCategorySummary = {},
                     content = "";
                // table category short
                contextCategorySummary.tableHead = [
                    { name: "螢幕", sortOrder: 1, widthClass: "group-code" },
                    { name: "名稱", sortOrder: 2, widthClass: "group-name" }
                ];
                contextCategorySummary.tableContent = data.category;
                content = Handlebars.compile($("#tpl-table-category-short").html())(contextCategorySummary);
                // $( targetObj.catSumTarget ).append( content );

                // table food list
                HandlebarsIntl.registerWith(Handlebars);
                var intlData = {
                    "locales": "en-US"
                };
                var contextItemList = {};
                contextItemList.tableHead = [
                  { "name": "食品編號", "fieldType": "textfield", "widthClass": "item-code" },
                  { "name": "中文名稱", "fieldType": "textfield", "widthClass": "name1" },
                  { "name": "英文名稱", "fieldType": "textfield", "widthClass": "name2" },
                  { "name": "螢幕", "fieldType": "dropdown", "widthClass": "display" },
                  { "name": "排列#", "fieldType": "textfield", "widthClass": "sort-order" },
                  { "name": "顏色", "fieldType": "dropdown", "widthClass": "color" },
                  { "name": "PLU", "fieldType": "textfield", "widthClass": "plu" },
                  { "name": "價錢#0", "fieldType": "textfield", "widthClass": "price" },
                  { "name": "積分", "fieldType": "textfield", "widthClass": "point" },
                  { "name": "列印到", "fieldType": "textfield", "widthClass": "print-to" },
                  { "name": "獨立列印", "fieldType": "checkbox", "widthClass": "separate-print" },
                  { "name": "廚房名稱", "fieldType": "textfield", "widthClass": "kitchen-name" },
                  { "name": "計服務費", "fieldType": "checkbox", "widthClass": "service-charge" },
                  { "name": "套餐", "fieldType": "checkbox", "widthClass": "combo" },
                  { "name": "細項", "fieldType": "textfield", "widthClass": "detail-item" },
                  { "name": "禮券樣式", "fieldType": "textfield", "widthClass": "discount-template" },
                  { "name": "備註", "fieldType": "textfield", "widthClass": "remark" }];
                contextItemList.tableContent = data.category;
                content = Handlebars.compile($("#tpl-table-itemlist").html())(contextItemList);
                $(targetObj.foodListTarget).append(content);

                // table category full list
                var contextCategoryList = {};
                contextCategoryList.tableHead = [
                  { "name": "螢幕", "fieldType": "textfield", "widthClass": "display" },
                  { "name": "排列", "fieldType": "textfield", "widthClass": "sort-order" },
                  { "name": "中文名稱", "fieldType": "textfield", "widthClass": "name1" },
                  { "name": "英文名稱", "fieldType": "textfield", "widthClass": "name2" },
                  { "name": "可使用", "fieldType": "checkbox", "widthClass": "usable" },
                  { "name": "顏色", "fieldType": "dropdown", "widthClass": "color" }];
                contextCategoryList.tableContent = data.category;
                content = Handlebars.compile($("#tpl-table-category").html())(contextCategoryList);
                $(targetObj.catListTarget).append(content);

                var helper = $rootScope.helper,
                    makeFieldEditable = helper.makeFieldEditable;

                // makeEitable before moving to shortlist
                $(targetObj.catListTarget).each(function () {
                    makeFieldEditable($rootScope.itemAdmin, $(this));
                });

                $(targetObj.catListTarget).find('table').tablesorter({
                    // initialize zebra striping and filter widgets
                    widgets: ["filter", "columnSelector"],

                    ignoreCase: false,

                    widgetOptions: {
                        columnSelector_columns: {
                            0: 'disable', /* set to disabled; not allowed to unselect it */
                            2: 'disable' /* set to disabled; not allowed to unselect it */
                        },
                        // filter_anyMatch options was removed in v2.15; it has been replaced by the filter_external option

                        // if true, filter child row content by column; filter_childRows must also be true
                        filter_childByColumn: false,

                        // if true, a filter will be added to the top of each table column;
                        // disabled by using -> headers: { 1: { filter: false } } OR add class="filter-false"
                        // if you set this to false, make sure you perform a search using the second method below
                        filter_columnFilters: false,

                        // class added to filtered rows (rows that are not showing); needed by pager plugin
                        filter_filteredRow: 'filtered',

                        // hide filter row when table is empty
                        filter_hideEmpty: true,

                        // if true, filters are collapsed initially, but can be revealed by hovering over the grey bar immediately
                        // below the header row. Additionally, tabbing through the document will open the filter row when an input gets focus
                        filter_hideFilters: true,

                        // Set this option to false to make the searches case sensitive
                        filter_ignoreCase: true,

                        // if true, search column content while the user types (with a delay)
                        filter_liveSearch: true,

                        // jQuery selector string of an element used to reset the filters
                        // filter_reset : 'button.reset-view',

                        // Delay in milliseconds before the filter widget starts searching; This option prevents searching for
                        // every character while typing and should make searching large tables faster.
                        filter_searchDelay: 300,

                        // allow searching through already filtered rows in special circumstances; will speed up searching in large tables if true
                        filter_searchFiltered: true,

                        // if true, server-side filtering should be performed because client-side filtering will be disabled, but
                        // the ui and events will still be used.
                        filter_serversideFiltering: false,

                        // Set this option to true to use the filter to find text from the start of the column
                        // So typing in "a" will find "albert" but not "frank", both have a's; default is false
                        filter_startsWith: false,

                        // data attribute in the header cell that contains the default filter value
                        filter_defaultAttrib: 'data-value'

                    }
                });


                $(targetObj.catSumTarget).append($(targetObj.catListTarget).find('table'));


                $('#category-shortlist-container tr').on('click', function (e) {
                    e.stopPropagation();
                    // console.log("cat tr");
                    // console.log("something");
                    // console.log($(this).find('.code').text());
                    $filterTarget = $('#item-list-container table').length > 0 ? $('#item-list-container table') : $('#item-shortlist-container table');

                    $.tablesorter.setFilters($filterTarget, ["", "", "", "", $(this).find('.code').text(), "", "", "", "", "", "", "", "", "", "", "", "", ""], true);
                    $(this).siblings().removeClass('active');
                    $(this).addClass('active');
                });

                // hide column for shortlist
                $(targetObj.catSumTarget).find('table').trigger('refreshColumnSelector', [[]]);

                var contextSubitemList = {};
                contextSubitemList.tableHead = [
                   { "name": "排列", "fieldType": "textfield", "widthClass": "sort-order" },
                   { "name": "子項目類別", "fieldType": "textfield", "widthClass": "sub-item-type" },
                   { "name": "套餐食品", "fieldType": "textfield", "widthClass": "combo" },
                   { "name": "數量", "fieldType": "textfield", "widthClass": "qty" },
                   { "name": "最少數量", "fieldType": "textfield", "widthClass": "minoq" },
                   { "name": "最多數量", "fieldType": "textfield", "widthClass": "maxoq" },
                   { "name": "自由更改控制金額", "fieldType": "textfield", "widthClass": "free-price" },
                   { "name": "最大項目金額", "fieldType": "textfield", "widthClass": "max-price" },
                   { "name": "我的細項", "fieldType": "textfield", "widthClass": "detail-item" },
                   { "name": "金額到主項目", "fieldType": "checkbox", "widthClass": "amount-to-main-item" },
                   { "name": "自訂名稱1", "fieldType": "textfield", "widthClass": "name1" },
                   { "name": "自訂名稱2", "fieldType": "textfield", "widthClass": "name2" },
                   { "name": "金額", "fieldType": "textfield", "widthClass": "amount" },
                   { "name": "列印到", "fieldType": "textfield", "widthClass": "print-to" },
                   { "name": "自訂廚房名稱", "fieldType": "textfield", "widthClass": "custom-kitchen-name" },
                   { "name": "獨立列印", "fieldType": "checkbox", "widthClass": "separate-print" },
                   { "name": "可更改為", "fieldType": "textfield", "widthClass": "changeable" },
                   { "name": "顯示螢幕組別", "fieldType": "textfield", "widthClass": "category-display" }];
                // contextSubitemList.tableContent = {};

                content = Handlebars.compile($("#tpl-table-subitem").html())(contextSubitemList);
                $(targetObj.subItemListTarget).append(content);

                // loadItemDetails() calling test and example, can be trigger by click event for specified food table row
                // $scope.loadItemDetails( 'SO29' );
                // $("#item-details").find('#kitchen-name').val( loadedData.group );
                callback();
                // callback after render
                // console.log(afterRender);
                // return content;
                // console.log(this);
            }; // renderItemTable

            // reference pointer for object/array
            // return a list of reference key in Array for specified object in pointerName
            $scope.updateRefPointer = function (dataObj, refKey, pointerName) {
                if (typeof dataObj === 'undefined')
                    return;

                if (typeof dataObj.splice === 'function') {
                    // array
                } else {
                    // object
                }
            }

            // for food or category
            $scope.saveData = function (dataObj, newData) {
                /*
                  new (add), existing (update)
                  update to object
                  when object is updated, fire an event of update for callback (category, )
         
                  structure
                  ----------
                  code: "AA"
                  color: "rms-palette-orange"
                  icon: 8
                  items: Array[21]
                  memberOnly: false
                  name: Object 001 003
                  001: "Sushi & Roast Sushi"
                  003: "壽司及火炙壽司"
                  specialOffer: false
                */
            };

            $scope.renderSubItemList = function (subitemArr) {
                // assume $( targetObj.subItemListTarget ) is already exist

                // bail
                if (typeof subitemArr === 'undefine' && typeof subitemArr.length === 'undefine')
                    return;

                // remove previous rendered rows


                console.log(subitemArr);
                content = Handlebars.compile($("#tpl-table-subitem-content").html())(subitemArr);
                console.log(content);
                // console.log($( targetObj.subItemListTarget ).find('tbody'));
                $("#subitem-list tbody").html(content);

                // trigger update for tablesorter
                $('.subitem-table').trigger('update', [true, noop]);
            };

            $scope.renderForm = function () {
                var helper = $rootScope.helper;

                this.itemColor = [{ "name": "rms-palette-orange", "background": "#FFA500", "text": "#000080" }, { "name": "rms-palette-gold", "background": "#FFD700", "text": "black" }, { "name": "rms-palette-grass", "background": "#008000", "text": "#FFD700" }, { "name": "rms-palette-sky", "background": "#E0FFFF", "text": "#00008B" }, { "name": "rms-palette-lemon", "background": "#FFFACD", "text": "black" }, { "name": "rms-palette-jade", "background": "#00FA9A", "text": "black" }, { "name": "rms-palette-navy", "background": "#000080", "text": "#FFF" }];

                this.groupColor = [{ "name": "rms-palette-orange", "background": "#FFA500", "text": "#000080" }, { "name": "rms-palette-gold", "background": "#FFD700", "text": "black" }, { "name": "rms-palette-grass", "background": "#008000", "text": "#FFD700" }, { "name": "rms-palette-sky", "background": "#E0FFFF", "text": "#00008B" }, { "name": "rms-palette-lemon", "background": "#FFFACD", "text": "black" }, { "name": "rms-palette-jade", "background": "#00FA9A", "text": "black" }, { "name": "rms-palette-navy", "background": "#000080", "text": "#FFF" }];

                // for other pages such as item details
                // color selector
                var option = "<option value=\"\" disabled selected>顏色</option>";
                for (var i = 0; i < this.itemColor.length; i++) {
                    option += '<option value="' + this.itemColor[i].name + '">' + this.itemColor[i].name + '</option>';
                }
                var itemColorSelector = $('<select class="color-option item-color-option">' + option + '</select>');
                // console.log(itemColorSelector);
                $('#item-details .item-color-option-container').append(itemColorSelector);
                itemColorSelector.material_select_color();
                var $colorEle = $('#item-details .item-color-option-container').find('.select-wrapper.item-color-option');

                var materialSelectHandler = helper.materialSelectHandler;

                /*
                function (e) {
                            e.stopPropagation();
                            $colorEle.find("input.select-dropdown").trigger('fieldChange');
         
                            // add modify flag class
                            // editor._setStatus( 'modified' );
                            // console.log($(element).data('oval')); // oVal
                            // if( !isModified( $(element).data('oval'), $(this).text()) ) {
                            //   $( element ).removeClass('modified');
                            //   $( element ).parents('tr').removeClass('modified');
                            // }
                          }
                */

                // add click event to custom dropdown for category selector
                $colorEle.find("li")
                  .on('click', materialSelectHandler);


                // category selector
                var option = "<option value=\"\" disabled selected>螢幕</option>";
                for (var i = 0; i < this.data.category.length; i++) {
                    option += '<option value="' + this.data.category[i].code + '">' + this.data.category[i].code + '</option>';
                }
                var categorySelector = $('<select class="category-option">' + option + '</select>');
                // console.log(itemColorSelector);
                $('#item-details .category-option-container').append(categorySelector);
                categorySelector.material_select();
                var $categoryEle = $('#item-details .category-option-container').find('.select-wrapper.category-option');

                // add click event to custom dropdown for category selector
                $categoryEle.find("li")
                  .on('click', materialSelectHandler);

                $('#item-details input[type=text]').on('focus keydown keyup', function (e) {
                    console.log(e);
                    var rowID = $('#item-details form').data('rowid'),
                        binderKey = $(this).attr('data-bind-row' + rowID),
                        newValue = $(this).val(),
                        target = 'td[data-bind-row' + rowID + '=' + binderKey + ']',
                        $target = $(target);

                    // console.log("_originalValue " + $(this).data('_originalValue'));
                    // console.log(rowID);
                    // console.log('binderKey ' + binderKey);
                    // console.log($target);
                    // console.log($target.data());


                    switch (e.type) {
                        case 'focus':
                            // when focus, only make original value once, so it will not keep updating the original value during changing/typing
                            console.log('focus me');
                            console.log('making original value');
                            if (!$(this).data('_originalValue')) {
                                $(this).data('_originalValue', $target.data('_originalValue') || $(this).val())
                            } else {
                                $(this).data('_originalValue', $(this).val());
                            }
                            console.log('original value ' + $(this).data('_originalValue'));
                            break;
                        case 'keydown':
                            // should be available on all related binding element

                            // if( !$(this).data('_originalValue') ) {
                            //   console.log('store original');
                            //   $(this).data('_originalValue', $(this).val());
                            // }
                            break;
                        case 'keyup':
                            helper.setOValue(target);
                            helper.setRowStatus($target, 'modified');
                            helper.chkModified($target, newValue);

                            switch (e.which) {
                                case $rootScope.keymap.ESC:
                                    // reset value
                                    // restore original with ESC (right before typing)
                                    console.log('ESC');
                                    // reset value to previous
                                    // true means reset value, 
                                    // false means update
                                    helper.chkModified($target, $(this).data('_originalValue'));
                                    helper.unSetRowStatus($target, 'modified');
                                    // $(this).removeData('$(this).data( '_originalValue')');
                                    $(this).val($(this).data('_originalValue'));
                                    $(this).trigger('keyup'); // trigger an update so the linked TD field will be updated too
                                    // // not reset if not same as original
                                    return;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        default:

                            break;
                    }
                });


                // console.log($scope);
            };

            // add item/food
            $scope.newItemObj = {};

            $scope.addCategory = function () {
            }

            $scope.afterRender = function (afterRenderOpts) {
                //console.log("afterRender");
                var helper = $rootScope.helper,
                    bindData2Way = helper.bindData2Way,
                    makeFieldEditable = helper.makeFieldEditable,
                    changeIndicator = helper.changeIndicator;

                this.renderForm();

                /*
                  table events
                */
                /*
                  $('#item-details form #code').val( dataBind.code ).attr('data-bind-row'+rowID, 'code');
                  // means: add attribute data-bind-row0="code" with value copied from dataObject "dataBind.code"
         
                  de', dataBind.code, 'code' );
                  // means make a custom event name 'code':change, set the initial value as dataBind.code and bind to object key 'code'
         
                  to be used in double click even under #item-list-container and single click under #item-shortlist-container
                  for usability and UX enhancement
                */



                // table cell event when it is in different container


                var activateItemDetails = function (e) {
                    var $tableObj = $(this).parents('table'),
                        $row = $(this).parents('tr'),
                        rowID = $row.data('rowid'),
                        dataPos = $row.data('object-pos') ? $(this).parents('tr').data('object-pos') : -1,
                        dataBind = getItemObjByObjPos(dataPos, $rootScope.itemAdmin.data.category) ? getItemObjByObjPos(dataPos, $rootScope.itemAdmin.data.category) : $(this).parents('tr').data('rowdata'); // for existing data

                    changeIndicator.apply($tableObj.find('tr.active'), ['remove']);
                    $tableObj.find('.active').removeClass('active');
                    $row.addClass('active');

                    if ($(this).parents('tr').hasClass('newRow')) {
                        // changeIndicator.apply( $(this).parents('tr.newRow'), ['add', 'cancel'] );
                    } else {
                        changeIndicator.apply($(this).parents('tr'), ['add']);
                    }

                    // bindDataToForm.apply( $(this).parents('tr')[0], [$("#item-details")] )
                    // bind event to form field for 2 way data binding with row with same row

                    // $scope.newItemObj[ rowID ]
                    // add to new item array prepared for pushing
                    console.log(dataBind);
                    console.log(rowID);
                    console.log('row ' + rowID + ' is selected');


                    // generic bind of the following statements
                    // $('#item-details form #code').attr('data-bind-row'+rowID, 'code');
                    // item.set( 'code', dataBind.code );
                    // console.log('bind row ID to form ' + rowID);
                    // console.log($(this).parents('tr').data('rowid'));
                    // console.log(dataBind);

                    // set form row ID
                    $('#item-details form').data('rowid', rowID);
                    // bind fields and add data-bind-row# attribute

                    // unbind previous event
                    // $('#item-shortlist-container').off( 'click' ); // unbind this whole container and previous Item instance created inside, WILL REMOVE the CLICK event, not working as expected, place in dblClick instead
                    // ["field-type", "field-style"]
                    $('#item-details form').off('fieldChange'); // unbind previous form event
                    // removeAllData( '#item-details form #code' );
                    // removeDataAttributes( '#item-details form #code', [] );

                    $('#item-details form').find('.data-binder').each(function () {
                        removeAllData('#' + $(this).attr('id'));
                        removeDataAttributes('#' + $(this).attr('id'), ["field-type", "field-style"]);
                        if (typeof $(this).attr('id') === 'undefined') {

                            // console.log($(this));
                        }
                        // console.log($(this).attr('id'));
                    })

                    // bind event
                    bindData2Way({
                        dataBind: dataBind,
                        target: '#item-details form',
                        rowID: rowID,
                        bindUid: 'row'
                    });

                    // bind event (old)
                    // function bindData2Way( $target, bindKey ) {
                    //  if( $.isEmptyObject( dataBind ) )
                    //    return; // empty, no need to bind

                    //   // console.log(dataBind);
                    //   $target.attr('data-bind-row'+rowID, bindKey);
                    //   // console.log(bindKey.indexOf('name'));
                    //   if( bindKey.indexOf('name') != -1 ) {
                    //     switch( bindKey ) {
                    //       case 'name1':
                    //       console.log('bind chinese name');
                    //         item.set( bindKey, dataBind.name['003'] );
                    //         break;
                    //       case 'name2':
                    //       // console.log('test');
                    //         item.set( bindKey, dataBind.name['001'] );
                    //         break;
                    //     }
                    //   } else {
                    //     item.set( bindKey, dataBind[bindKey] );
                    //   }
                    // }

                    /*var item = new Item( 'row'+rowID, dataBind, $('#item-details form') )
           
                    bindData2Way( $('#item-details form #code'), 'code' );
                    bindData2Way( $('#item-details form .category-option-container'), 'group' );
                    bindData2Way( $('#item-details form .data-binder.item-color-option-container'), 'color' );
                    
                    bindData2Way( $('#item-details form #separate-print.data-binder'), 'separatePrint' );
                    bindData2Way( $('#item-details form #sevice-charge.data-binder'), 'seviceCharge' );
                    bindData2Way( $('#item-details form #unitprice.data-binder'), 'unitprice' );
                    // bindData2Way( $('#item-details form #setMeal.data-binder'), 'setMeal' );
                    bindData2Way( $('#item-details form #printer.data-binder'), 'printer' );
                    bindData2Way( $('#item-details form #name2.data-binder'), 'name2' );
                    bindData2Way( $('#item-details form #name1.data-binder'), 'name1' );
                    bindData2Way( $('#item-details form #kitchen-label.data-binder'), 'label' );
                    bindData2Way( $('#item-details form #sort-order.data-binder'), 'sortOrder' );
                    bindData2Way( $('#item-details form #plu.data-binder'), 'plu' );
                    bindData2Way( $('#item-details form #coupon.data-binder'), 'coupon' );
                    bindData2Way( $('#item-details form #remark.data-binder'), 'remark' );*/



                    // $('#item-details form .category-option-container input').val( dataBind.group ).attr('data-bind-row'+rowID, 'group');
                    // item.set( 'code', dataBind.group, 'group' );

                    // if( $.fn.makeEditable.editable === true )
                    //    return;
                    // $(this).focus().select() // not work like ng-click, expected effect: make :focus turn on like Chrome inspector

                    $('ul.tabs').tabs('select_tab', 'item-details');

                    // // if cell is modified, don't load, will dead because when category and code is changed, cannot find anything
                    // if( $(this).find('.modified').length === 0 ) {
                    //   // console.log($scope);
                    //   $scope.loadItemDetails( cat, code );
                    // }
                    // else
                    //   console.log("data is modified, details data will not load");

                    // $(this).on('change', 'input', function(){
                    //   console.log("selected row have input updated");
                    // });
                }; // activateItemDetails

                $('#item-list-container').each(function () {
                    // $container = $(this);
                    // console.log($scope);
                    // console.log($(this));
                    makeFieldEditable($scope, $(this));
                    // avoid the helper to be listened when the table is moved to elsewhere in #item-details
                    $(this).on('dblclick', '.helper', activateItemDetails);
                });

                // when item-list table in phantom-container
                // helper is used as arrow navigation, use conteneditable as the cursor focus point
                $('#item-shortlist-container').on('click', '.helper', activateItemDetails);
                $('#item-shortlist-container').on('keydown', '.helper:focus', helper.TableArrowControl);

                // prevent edit the table when it is being a short list, edit details instead in this mode
                // $('#item-shortlist-container').on('click', '.editable', function(e) {
                //  console.log('editable should be disabled');
                //   return false;
                // });

                // init tables
                // $('#category-list-container table').each(function(){


                // $('#category-shortlist-container').on('click', 'table tbody .editable', function(e) {
                //   return false;
                // });

                // $('table:not("#item-list-container table, #employee-list-container table")').each(function(){
                //   $(this).tablesorter({
                //     sortList: [[0,0]]
                //   });
                // });

                $("#item-list-container table").tablesorter({
                    // initialize zebra striping and filter widgets
                    widgets: ["filter", "columnSelector"],

                    ignoreCase: false,

                    widgetOptions: {
                        columnSelector_columns: {
                            0: 'disable', /* set to disabled; not allowed to unselect it */
                            1: 'disable', /* set to disabled; not allowed to unselect it */
                            2: 'disable' /* set to disabled; not allowed to unselect it */
                        },
                        // filter_anyMatch options was removed in v2.15; it has been replaced by the filter_external option

                        // if true, filter child row content by column; filter_childRows must also be true
                        filter_childByColumn: false,

                        // if true, a filter will be added to the top of each table column;
                        // disabled by using -> headers: { 1: { filter: false } } OR add class="filter-false"
                        // if you set this to false, make sure you perform a search using the second method below
                        filter_columnFilters: false,

                        // class added to filtered rows (rows that are not showing); needed by pager plugin
                        filter_filteredRow: 'filtered',

                        // hide filter row when table is empty
                        filter_hideEmpty: true,

                        // if true, filters are collapsed initially, but can be revealed by hovering over the grey bar immediately
                        // below the header row. Additionally, tabbing through the document will open the filter row when an input gets focus
                        filter_hideFilters: true,

                        // Set this option to false to make the searches case sensitive
                        filter_ignoreCase: true,

                        // if true, search column content while the user types (with a delay)
                        filter_liveSearch: true,

                        // jQuery selector string of an element used to reset the filters
                        filter_reset: 'button.reset-view',

                        // Delay in milliseconds before the filter widget starts searching; This option prevents searching for
                        // every character while typing and should make searching large tables faster.
                        filter_searchDelay: 300,

                        // allow searching through already filtered rows in special circumstances; will speed up searching in large tables if true
                        filter_searchFiltered: true,

                        // if true, server-side filtering should be performed because client-side filtering will be disabled, but
                        // the ui and events will still be used.
                        filter_serversideFiltering: false,

                        // Set this option to true to use the filter to find text from the start of the column
                        // So typing in "a" will find "albert" but not "frank", both have a's; default is false
                        filter_startsWith: false,

                        // data attribute in the header cell that contains the default filter value
                        filter_defaultAttrib: 'data-value'

                    }
                });


                // $(".item-table:eq(0)").trigger( 'filterReset' );
                // $.tablesorter.setFilters( $('.item-table:eq(0)'), ["AA", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""], true );
                // filter relations, category-sort <-> food list


                // save and edit need $scope from food/item


                // hide category short table when it is not needed to use
                function categoryShortListCtrl() {
                    var curTab = $('.menu-area.active ul.tabs a.active').attr("href");
                    if (curTab === '#item-list-container')
                        $('#category-shortlist-container').removeClass('hidden');
                    else
                        $('#category-shortlist-container').addClass('hidden');
                }

                /*
                 because the mechanis of the indicator of the tabs by materializecss is not working properly when the tabs is inside a side element with another side menu show and hiding dynamically
                */
                function adjustIndicator() {
                    var $link = $(this),
                        $tab = $link.parents('.tabs'),
                        $active = $tab.find('li.tab a.active'),
                        $prev_index = $tab.find('li.tab a').index($active),
                        // var $tab = $('#employee-form-content ul.tab'),
                        // $link = $tab.find('li a[href=#employee-list-container]'),
                        $indicator = $tab.find('.indicator'),
                        $index = $tab.find('li.tab a').index($link),
                        $tabs_width = $tab.width(),
                        $tab_width = $tab.find('li').first().outerWidth();
                    // console.log('adjustIndicator');
                    // if( typeof $link === 'undefined' )
                    //     return;

                    // console.log($tab.find('li.tab a'));
                    // console.log($link);
                    // console.log($indicator);
                    // console.log($tab);
                    // console.log($tab.find('li'));
                    // console.log($tab.find('li.active'));
                    // console.log($active);
                    // console.log('index ' + $index);
                    // console.log('prev_index ' + $prev_index);
                    // console.log('$tabs_width ' + $tabs_width);
                    // console.log('$tab_width ' + $tab_width);
                    // console.log('$index * $tab_width ' + $index * $tab_width);

                    // Update indicator
                    // console.log('force me?');
                    // $indicator.velocity("finish"); // mark it to finish

                    if (($index - $prev_index) >= 0) {
                        // console.log('($index - $prev_index) >= 0');
                        // console.log('left ' + $index * $tab_width);
                        // console.log('right ' + ($tabs_width - (($index + 1) * $tab_width)));
                        setTimeout(function () {
                            $indicator.velocity('stop'); // delay to run at the same time of original animation delay in source
                            $indicator.css({ "right": $tabs_width - (($index + 1) * $tab_width) });
                            $indicator.css({ "left": $index * $tab_width });
                        }, 90);
                    }
                    else {
                        // console.log('elsex');
                        // console.log('left ' + $index * $tab_width);
                        // console.log('right ' + $tabs_width - (($index + 1) * $tab_width));
                        setTimeout(function () {
                            $indicator.velocity('stop'); // delay to run at the same time of original animation delay in source
                            $indicator.css({ "right": $tabs_width - (($index + 1) * $tab_width) });
                            $indicator.css({ "left": $index * $tab_width });
                        }, 90);
                    }
                }

                function catShortListCtrl(source, target, tabName) {
                    var curTab = $(this).attr('href'),
                        // source = '#item-list-container',
                        $source = $(source),
                        // target = '#item-shortlist-container',
                        $target = $(target);

                    switch (curTab) {
                        case tabName:
                            console.log('category-list-container');
                            $target.addClass('hidden');
                            $source.append($target.find('table'));
                            $source.find('table').trigger('refreshColumnSelector', true);
                            $source.find('table tbody tr .helper').attr('contentEditable', false);

                            scrollToRow(source, 'tr.active');
                            break;
                        default:
                            $target.removeClass('hidden');
                            $target.append($source.find('table'));
                            $target.find('table').trigger('refreshColumnSelector', [[]]); // show all rows, unset row filter

                            // [4, 6, 11]
                            // $target.find('table').trigger('refreshColumnSelector', [ [] ]); // first 3 columns only is setup in init, cannot be omitted
                            $target.find('table tbody tr .helper').attr('contentEditable', true);
                            scrollToRow(target, 'tr.active');

                            // click first row if no row is selected
                            if (isUndefined($(tabName).find('form').data('rowid')))
                                $target.find('td.helper:eq(0)').click();
                            else {
                                // make a dead loop?
                                // $('#item-shortlist-container tr#itemlist-0 .helper').click()
                                // var rowID = $('#item-details form').data('rowid');                
                                // $('#item-shortlist-container #itemlist-' + rowID + ' .helper').click();
                            }
                            break;
                    }
                }

                // move item-list table to item-details left hand side and show only 4 rows to control
                function shortListCtrl(source, target, tabName) {
                    // var curTab = $('.menu-area.active ul.tabs a.active').attr("href"),
                    var curTab = $(this).attr('href'),
                        // source = '#item-list-container',
                        $source = $(source),
                        // target = '#item-shortlist-container',
                        $target = $(target);
                    // console.log(curTab);
                    // console.log('curTab ' + curTab);
                    // console.log('tabname ' + tabName);
                    switch (curTab) {
                        case tabName:
                            // console.log('now in');
                            $target.removeClass('hidden');
                            $target.append($source.find('table'));
                            $target.find('table').trigger('refreshColumnSelector', [[]]); // show all rows, unset row filter

                            // [4, 6, 11]
                            // $target.find('table').trigger('refreshColumnSelector', [ [] ]); // first 3 columns only is setup in init, cannot be omitted
                            $target.find('table tbody tr .helper').attr('contentEditable', true);
                            scrollToRow(target, 'tr.active');

                            // click first row if no row is selected
                            if (isUndefined($(tabName).find('form').data('rowid')))
                                $target.find('td.helper:eq(0)').click();
                            else {
                                // make a dead loop?
                                // $('#item-shortlist-container tr#itemlist-0 .helper').click()
                                // var rowID = $('#item-details form').data('rowid');                
                                // $('#item-shortlist-container #itemlist-' + rowID + ' .helper').click();
                            }
                            break;
                        default:
                            console.log('default');
                            $target.addClass('hidden');
                            $source.append($target.find('table'));
                            $source.find('table').trigger('refreshColumnSelector', true);
                            $source.find('table tbody tr .helper').attr('contentEditable', false);

                            scrollToRow(source, 'tr.active');
                            break;
                    }

                    adjustIndicator.apply(this);
                }

                $('#item-form-content .tab-container').on('click', 'a', function () {
                    // console.log('test');
                    // var $link = $(this);
                    // categoryShortListCtrl();
                    shortListCtrl.apply(this, ['#item-list-container', '#item-shortlist-container', '#item-details']);
                    catShortListCtrl.apply(this, ['#category-list-container', '#category-shortlist-container', '#category-list-container']);
                });

                $('#employee-form-content .tab-container').on('click', 'a', function () {
                    shortListCtrl.apply(this, ['#employee-list-container', '#employee-shortlist-container', '#employee-details']);
                });

                function addSubItem() {
                    // add two rows
                    var row = '<tr><td class="helper">新</td><td class="sort-order editable" data-field-type="textfield">-</td> \
                        <td class="sub-item-type editable">-</td> \
                        <td class="combo editable">-</td><td class="qty editable" data-field-type="textfield">0</td><td class="minoq editable">0</td><td class="maxoq editable">0</td><td class="free-price editable">0</td><td class="max-price editable">0</td><td class="detail-item editable">-</td><td class="amount-to-main-item editable">-</td><td class="name1 editable">-</td><td class="name2 editable">-</td><td class="amount editable">0</td><td class="print-to editable">-</td><td class="custom-kitchen-name editable">-</td><td class="separate-print editable">-</td><td class="changeable editable">-</td><td class="category-display editable">-</td></tr>',
                      $row = $(row).addClass('newRow'),
                      // resort table using the current sort; set to false to prevent resort, otherwise 
                      // any other value in resort will automatically trigger the table resort. 
                      resort = false;

                    console.log('add row to item subitem list');
                    $('#subitem-list table')
                      .find('tbody').prepend($row)
                      .trigger('addRows', [$row, resort])
                      .trigger('update', [true, noop]); // trigger update for tablesorter [1,0]

                    console.log('makeEditable subitem');
                    makeFieldEditable($scope, $row);
                };

                $('button.add-sub-item').click(function () {
                    console.log('add-sub-item');
                    addSubItem();
                });

                helper.disableInput($('#item-details'));

                // console.log($rootScope.itemAdmin.data.category);
                // $('.category-option-container select').attr('data-bind-row0', 'category');
            }; // afterRender

            //  loadJSON("files/food.json", $scope.renderItemTable, 
            //   {catSumTarget:"#category-shortlist-container", 
            //   foodListTarget:"#item-list-container", 
            //   catListTarget:"#category-list-container",
            //   subItemListTarget:"#subitem-list"
            // }, afterRender, {});

            // simulate loading JSON, will use socket.io
            // comment it out and run socket emit loadFoodInfo something in between $scope
            // loadJSON2("files/food.json", $scope, 'renderItemTable',
            //   {catSumTarget:"#category-shortlist-container", 
            //   foodListTarget:"#item-list-container", 
            //   catListTarget:"#category-list-container",
            //   subItemListTarget:"#subitem-list"
            // }, $scope.afterRender, {});

            return $scope;
        })(); // end of $scope for food/item

        $rootScope.employeeAdmin = (function () {
            // console.log(arguments);
            // console.log(JSON.stringify(r));
            var $scope = {},
                helper = $rootScope.helper;

            $scope.renderForm = function () {
                this.employeeGroup = ["系統管理員", "經理", "服務員", "收銀員"];
                this.employeeType = [];

                var option = '<option value="" disabled selected>員工組別</option>';
                for (var i = 0; i < this.employeeGroup.length; i++) {
                    option += '<option value="' + this.employeeGroup[i] + '">' + this.employeeGroup[i] + '</option>';
                }
                var groupSelector = $('<select class="employee-group-option">' + option + '</select>');
                // console.log(itemColorSelector);
                $('#employee-details .employee-group-option-container').append(groupSelector);
                groupSelector.material_select();


                option = '<option value="" disabled selected>員工級別</option>';
                for (var i = 0; i < 9; i++) {
                    option += '<option value="' + i + '">' + i + '</option>';
                    this.employeeType.push(i);
                }
                var typeSelector = $('<select class="employee-type-option">' + option + '</select>');
                // console.log(itemColorSelector);
                $('#employee-details .employee-type-option-container').append(typeSelector);
                typeSelector.material_select();

                var $typeEle = $('#employee-details .option-container').find('.select-wrapper.employee-type-option'),
                    $groupEle = $('#employee-details .option-container').find('.select-wrapper.employee-group-option'),
                    materialSelectHandler = helper.materialSelectHandler,
                    inputHandler = helper.inputHandler;

                $typeEle.find("li")
                  .on('click', materialSelectHandler);

                $groupEle.find("li")
                  .on('click', materialSelectHandler);

                $('#employee-details input[type=text]').on('focus keydown keyup', { form: '#employee-details form' }, inputHandler);
                // console.log($scope);
            };

            $scope.renderEmployeeTable = function (data) {
                this.data = data;
                var helper = $rootScope.helper,
                    makeFieldEditable = helper.makeFieldEditable,
                    changeIndicator = helper.changeIndicator,
                    bindData2Way = helper.bindData2Way,
                    test = helper.test,
                    $scope = this,
                    wrapper = '#employee-list-container';
                // table employee list
                
                var contextEmployeeList = {};
                contextEmployeeList.tableHead = [
                { "name": "員工編號", "fieldType": "textfield", "widthClass": "item-code" },
                { "name": "員工名稱", "fieldType": "textfield", "widthClass": "name1" },
                { "name": "員工組別", "fieldType": "dropdown", "widthClass": "name1" }];
                contextEmployeeList.tableContent = this.data;
                content = Handlebars.compile($("#tpl-table-employee").html())(contextEmployeeList);
                $(wrapper).append(content);

                $(wrapper).find('table').tablesorter({
                    // initialize zebra striping and filter widgets
                    widgets: ["filter", "columnSelector"],

                    ignoreCase: false,

                    widgetOptions: {
                        columnSelector_columns: {
                            0: 'disable', /* set to disabled; not allowed to unselect it */
                            1: 'disable', /* set to disabled; not allowed to unselect it */
                            2: 'disable' /* set to disabled; not allowed to unselect it */
                        },
                        // filter_anyMatch options was removed in v2.15; it has been replaced by the filter_external option

                        // if true, filter child row content by column; filter_childRows must also be true
                        filter_childByColumn: false,

                        // if true, a filter will be added to the top of each table column;
                        // disabled by using -> headers: { 1: { filter: false } } OR add class="filter-false"
                        // if you set this to false, make sure you perform a search using the second method below
                        filter_columnFilters: false,

                        // class added to filtered rows (rows that are not showing); needed by pager plugin
                        filter_filteredRow: 'filtered',

                        // hide filter row when table is empty
                        filter_hideEmpty: true,

                        // if true, filters are collapsed initially, but can be revealed by hovering over the grey bar immediately
                        // below the header row. Additionally, tabbing through the document will open the filter row when an input gets focus
                        filter_hideFilters: true,

                        // Set this option to false to make the searches case sensitive
                        filter_ignoreCase: true,

                        // if true, search column content while the user types (with a delay)
                        filter_liveSearch: true,

                        // jQuery selector string of an element used to reset the filters
                        // filter_reset : 'button.reset-view',

                        // Delay in milliseconds before the filter widget starts searching; This option prevents searching for
                        // every character while typing and should make searching large tables faster.
                        filter_searchDelay: 300,

                        // allow searching through already filtered rows in special circumstances; will speed up searching in large tables if true
                        filter_searchFiltered: true,

                        // if true, server-side filtering should be performed because client-side filtering will be disabled, but
                        // the ui and events will still be used.
                        filter_serversideFiltering: false,

                        // Set this option to true to use the filter to find text from the start of the column
                        // So typing in "a" will find "albert" but not "frank", both have a's; default is false
                        filter_startsWith: false,

                        // data attribute in the header cell that contains the default filter value
                        filter_defaultAttrib: 'data-value'

                    }
                });

                // callback after render
                this.renderForm();

                var activateEmployeeDetails = function (e) {
                    var $tableObj = $(this).parents('table'),
                        $row = $(this).parents('tr'),
                        rowID = $row.data('rowid'),
                        dataPos = isDefined($row.data('object-pos')) ? $row.data('object-pos') : -1,
                        dataBind = dataPos !== -1 ? getObjByPos(dataPos, $rootScope.employeeAdmin.data) : $row.data('rowdata'); // for existing data

                    changeIndicator.apply($tableObj.find('tr.active'), ['remove']);
                    $tableObj.find('.active').removeClass('active');
                    $row.addClass('active');

                    if ($(this).parents('tr').hasClass('newRow')) {
                        // changeIndicator.apply( $(this).parents('tr.newRow'), ['add', 'cancel'] );
                    } else {
                        changeIndicator.apply($(this).parents('tr'), ['add']);
                    }

                    // bind event to form field for 2 way data binding with row with same row
                    // $scope.newItemObj[ rowID ]
                    // add to new item array prepared for pushing
                    // console.log(dataPos);
                    // console.log($row.data('rowdata'));
                    // console.log(dataBind);
                    // console.log(rowID);
                    // console.log('row ' + rowID +' is selected');
                    // generic bind of the following statements
                    // $('#item-details form #code').attr('data-bind-row'+rowID, 'code');
                    // item.set( 'code', dataBind.code );

                    // set form row ID
                    $('#employee-details form').data('rowid', rowID);
                    // bind fields and add data-bind-row# attribute

                    $('#employee-details form').off('fieldChange'); // unbind previous form event
                    // removeAllData( '#item-details form #code' );
                    // removeDataAttributes( '#item-details form #code', [] );

                    $('#employee-details form').find('.data-binder').each(function () {
                        removeAllData('#' + $(this).attr('id'));
                        removeDataAttributes('#' + $(this).attr('id'), ["field-type", "field-style"]);
                        if (typeof $(this).attr('id') === 'undefined') {
                        }
                    })

                    // bind event
                    bindData2Way({
                        dataBind: dataBind,
                        target: '#employee-details form',
                        rowID: rowID,
                        bindUid: 'eprow'
                    });

                    // turn to details tab
                    $('ul.tabs').tabs('select_tab', 'employee-details');
                }; // activateEmployeeDetails

                $('#employee-list-container').each(function () {
                    makeFieldEditable($scope, $(this));

                    // avoid the helper to be listened when the table is moved to elsewhere in #item-details
                    $(this).on('dblclick', '.helper', activateEmployeeDetails);
                });

                // when item-list table in phantom-container
                // helper is used as arrow navigation, use conteneditable as the cursor focus point
                $('#employee-shortlist-container').on('click', '.helper', activateEmployeeDetails);

                $('#employee-shortlist-container').on('keydown', '.helper', helper.TableArrowControl2);
                // $('#employee-shortlist-container').on('click', '.helper', activateEmployeeDetails);

                helper.disableInput($('#employee-details'));
            };

            $scope.load = function () {
                socket.emit('loadStaff',null, function (r) {
                    $rootScope.employeeAdmin.renderEmployeeTable(JSON.parse(r));
                });

            }

            return $scope;
        })(); // end of $scope for employee/item

        // load food information
        // replace with socket emit easily in this case
        $rootScope.itemAdmin.load();
        $rootScope.employeeAdmin.load();
        $rootScope.reload = function () {
            $rootScope.itemAdmin.load();
            //$rootScope.employeeAdmin.load();
        }

        //loadJSON2("data/food.json", $rootScope.itemAdmin, 'renderItemTable',
        //  {catSumTarget:"#category-shortlist-container", 
        //  foodListTarget:"#item-list-container", 
        //  catListTarget:"#category-list-container",
        //  subItemListTarget:"#subitem-list"
        //}, $rootScope.itemAdmin.afterRender, {});

        // socket.emit('loadStaff', function (r) {
        // $scope.itemAdmin.renderItemTable( r, {catSumTarget:"#category-shortlist-container", 
        //                             foodListTarget:"#item-list-container", 
        //                             catListTarget:"#category-list-container",
        //                             subItemListTarget:"#subitem-list"
        //                            }, $scope.itemAdmin.afterRender, {} );
        // });

        // load staff information
        // scope for staff
        /*socket.emit('loadStaff', function (r) {
            // var args = Array.prototype.slice.call(arguments);
            // $rootScope.employeeAdmin.apply(this, args);
            $rootScope.employeeAdmin.renderEmployeeTable( r );
            // console.log( r );
        });*/

        // test
        // $('button.go-employee').click();
        // }); // socket wrapper end

        // $('.modal-trigger').leanModal();

        // exclude subitem table because subitem table is always change, need to do separately with unload

        // manually match item value
        // activateOption( $('.item-color-option.select-wrapper'), findOptionByValue( $('.item-color-option.select-wrapper'), 'rms-palette-grass' ) )

        // prevent propagate to parent item when it is appearing in table cell

        //set focus to 1st input field
        // $(".editor-input-text").focus().select(); //select for IE, focus for Chrome and FF
    }); // ready

    // other test
    $('body').keyup(function (e) {
        // console.log(e.keyCode);
        // console.log(e.which);
        // console.log($(':focus'));
    });

    $('body').on('keypress keydown keyup', function (e) {
        // e.preventDefault();
        // return false;
        // $( 'table').find('tbody').find('.editable-checkbox').trigger('customKeydown')
    });

})(jQuery);