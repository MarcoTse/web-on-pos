   (function($) {
    // console.log(language);
    // Handlebars.registerHelper('each', function(context, options) {
    //   var ret = "";
      
    //   // bail if no content
    //   if( typeof context === 'undefined')
    //       return;

    //   // console.log(context.length);
    //   if( typeof context.length != 'undefined') {
    //     for(var i=0, j=context.length; i<j; i++) {
    //       ret = ret + options.fn(context[i]);
    //     }
    //   } 

    //   return ret;
    // });

    // var globalLang = "003";
    Handlebars.registerHelper('getName', function(context, lang, options) {
      // console.log(arguments);
      // console.log(context);
      for(var key in context) {
        // console.log(options);
        // console.log( context[lang]);
        // ret = ret + context[globalLang];
        return context[lang];
      }
    });

    Handlebars.registerHelper('getPermission', function(context, lang, options) {
      // console.log(arguments);

      // console.log(context);
      if( context === true)
        return language[lang]['y'];
      else
        return language[lang]['n'];
    });

    Handlebars.registerHelper('getPermissionName', function(context, lang) {
      return language[lang][context];
    });

    Handlebars.registerHelper('getCheck', function(context, options) {
      var ret = '';
      if( context === true || context === 'true') {
        console.log(context);
        ret = '<i class="md-icon value" title="true">done</i>';
      } else {
        ret = '<i class="md-icon value" title="false">clear</i>';
      }
      return new Handlebars.SafeString( ret );
    });

    Handlebars.registerHelper('getFieldType', function(context, index, options) {
      if( context[index] ) {
        if( context[index].fieldType ) {
          // console.log(context[index]);
          // return context[index].fieldType;
          return index;
        }
      }
      // console.log(something);
      // return context[index]
    });

    Handlebars.registerHelper('getFood', function(context, options) {
      var ret = "";

      // console.log(context);
     for(var i=0; i<context.length; i++) {
       // ret = ret + options.fn(context[i].items);
       for(var j=0; j<context[i].items.length; j++) {
        console.log(context[i].items[j]);
        options.fn(context[i].items[j]);
       }
     }
    });



    // scope in handlebar declaration

    $scope = {};
    $scope.itemCount = 0;
    $scope.count = 0;
    $scope.flag = false; // off
    var itemCounter = function( context, parentIdx, childIdx ){
      // console.log(context);
      //console.log(parentFirst + '' + parentIdx);
      //console.log(childFirst + '' + childIdx);

      $scope.flag = true; // means used
      if (parentIdx === 0 && childIdx === 0) {
      //    console.log()
        $scope.itemCount = 0; // reset;
      }
        
      // console.log($scope.itemCount);

      // rowID used to bind counter to object for data binding use later, new row ID is needed to be added by add item function
      if( typeof parentIdx === 'number' ) {
        // console.log(parentIdx);
        // console.log(childIdx);
        // console.log('============================');
        // console.log(context[parentIdx].items[childIdx]);
        context[parentIdx].items[childIdx]['rowID'] = $scope.itemCount;
        var item = new Item( 'row'+$scope.itemCount, context[parentIdx].items[childIdx] )
        
        // dummy data for testing function integrity
        context[parentIdx].items[childIdx]['separatePrint'] = 0;
        context[parentIdx].items[childIdx]['remark'] = 'remark';
        context[parentIdx].items[childIdx]['coupon'] = 'coupon';
        context[parentIdx].items[childIdx]['plu'] = 'plu';
        context[parentIdx].items[childIdx]['sortOrder'] = '1';
        // console.log( context[parentIdx].items[childIdx].code );

        // bind data
        // {dataObj: context[parentIdx].items[childIdx], key: 'code'}
        // item.set( 'code', context[parentIdx].items[childIdx].code );
        // item.set( 'group', context[parentIdx].items[childIdx].group );
        // item.set( "code", "AA01" ); // multiple subscription test to see if duplicated event will be registered
      }
      return $scope.itemCount++;
    }

    var genericCounter = function( context, index, bindUid ){
      bindUid = bindUid ? bindUid : 'row'
      // console.log(context);
      // console.log(index);
      // console.log(parentFirst);
      // console.log(childFirst);
      // console.log(typeof index);
      // console.log(index);
      // console.log(context);
      // if used before, reset counter
      // rowID used to bind counter to object for data binding use later, new row ID is needed to be added by add item function
      if( typeof index === 'number' ) {
        // console.log(parentIdx);
        // console.log(childIdx);
        // console.log('============================');
        // console.log(context[parentIdx].items[childIdx]);
        context[index]['rowID'] = $scope.count;
        var item = new Item( bindUid+$scope.count, context[index], $('#employee-list-container') )
        // console.log(context[index]);
        
        // dummy data for testing function integrity
        // context[parentIdx].items[childIdx]['separatePrint'] = 0;
        // context[parentIdx].items[childIdx]['remark'] = 'remark';
        // context[parentIdx].items[childIdx]['coupon'] = 'coupon';
        // context[parentIdx].items[childIdx]['plu'] = 'plu';
        // context[parentIdx].items[childIdx]['sortOrder'] = '1';
        // console.log( context[parentIdx].items[childIdx].code );
      }
      return $scope.count++;
    }

    Handlebars.registerHelper("itemCounter", itemCounter);
    Handlebars.registerHelper("counter", genericCounter);

    Handlebars.registerHelper('with', function(context, options) {
      return options.fn(context);
    });



     /*
        // table head elements for food list, since handlebar does not support multiple variable
     */

     $(document).ready(function($) {
       //  loadJSON("files/food.json", renderTable, 
       //   {catSumTarget:"#category-short-list", 
       //   foodListTarget:"#food-list", 
       //   catListTarget:"#category-list",
       //   subItemListTarget:"#subitem-list"
       // }, afterRender;
        // loadJSON("files/food.json", renderFoodTable, "#food-list");

        
     });
   })(jQuery);