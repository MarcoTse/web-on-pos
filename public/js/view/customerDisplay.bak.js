(function () {
    var app = angular.module('fdm', ['rms']);

    app.controller('FDMController', function (SocketFactory, INFO, $scope, $window, $http, $q, $filter, uiLanguage, userConfig, $rootScope, MainService, $timeout) {
        SocketFactory.on('connect', function (msg) {
            console.log('receive');
        });
        $scope.MainService = MainService;

        
        //MainService.takeOutInOrder.mode = MainService.takeOutInOrder.schema.paymentPage;

        if (!MainService.getOtherWriteOrderService()) {
            MainService.initOtherWriteOrderService(MainService.schema.modeOtherWriteOrderService.fastFood);
        }

        MainService.isCustomerDisplay = true;

        MainService.mode = MainService.getOtherWriteOrderService().mode = MainService.getOtherWriteOrderService().schema.paymentPage;

        MainService.checkIsTakeAwayMode = function () {
            return false;
        }

        var updateCart = function (data) {
            try {
                console.log("updateCart", JSON.parse(data), MainService.Cart);
                //MainService.Cart = angular.copy(JSON.parse(data), MainService.Cart);
                MainService.Cart = JSON.parse(data)
                //obj = angular.copy(JSON.parse(event.newValue), obj);
                //console.log("obj.Cart", $scope.Cart);
                if (MainService.Cart.cartList.length) {
                    $('.sidebar').addClass('active');
                }
                else {
                    $('.sidebar').removeClass('active');
                    MainService.appliedPaymentMethod = [];
                    MainService.appliedDiscount = [];
                    MainService.remainerAmount = 0;
                }

                MainService.Cart.cartList.forEach(function (item) {
                    $.extend(true, item, MainService.schema.baseItem, { qty: item.qty, isEdit: item.isEdit });
                    if (item.option) {
                        item.option.forEach(function (opt) {
                            opt.items.forEach(function (optItem) {
                                $.extend(true, optItem, MainService.schema.baseItem, { qty: optItem.qty });
                            });
                        });
                    }
                    if (item.subitem) {
                        item.subitem.forEach(function (subItem) {
                            $.extend(true, subItem, MainService.schema.baseItem, { qty: subItem.qty });
                        });
                    }
                })
                console.log("$scope.Cart", MainService.Cart.cartList);
                $timeout(function () {
                    $('.md-icon.delete, .md-icon.remove').hide();
                });
                $timeout(function () {
                    //MainService.scrollToBottom(".order-list-wrapper");
                    if (angular.element(".list-group-item.active").length) {
                        var target = ".order-list-wrapper";
                        var scrollTo = angular.element(".list-group-item.active").offset().top;
                        angular.element(target).scrollTop(scrollTo);
                    }
                }, 10);
            } catch (e) {
                console.log(e);
            }
        }

        window.addEventListener('storage', function (event) {
            switch (event.key) {
                case "customer-display-cart": {
                    $scope.$apply(function () {
                        console.log(event.key, JSON.parse(event.newValue));
                        updateCart(event.newValue);
                    });
                    break;
                }
                case "customer-display-appliedPaymentMethod": {
                    $scope.$apply(function () {
                        if (typeof event.newValue === "undefined") return;
                        MainService.appliedPaymentMethod = JSON.parse(event.newValue);
                        console.log(event.key, JSON.parse(event.newValue));
                    });
                    updateUI();
                    break;
                }
                case "customer-display-appliedDiscount": {
                    $scope.$apply(function () {
                        if (typeof event.newValue === "undefined") return;
                        MainService.appliedDiscount = JSON.parse(event.newValue);
                        console.log(event.key, JSON.parse(event.newValue));
                    });
                    updateUI();
                    break;
                }
                case "customer-display-remainerAmount": {
                    $scope.$apply(function () {
                        MainService.remainerAmount = event.newValue;
                        console.log(event.key, event.newValue);
                    });
                    updateUI();
                    break;
                }
                case "customer-display-isCash": {
                    $scope.$apply(function () {
                        MainService.isCash = event.newValue == "true";
                        console.log(event.key, event.newValue);
                    });
                    updateUI();
                    break;
                }
                case "customer-display-mode": {
                    $scope.$apply(function () {
                        MainService.mode = JSON.parse(event.newValue);
                        console.log(event.key, JSON.parse(event.newValue));
                    });
                    updateUI();
                    break;
                }
                case "customer-display-modeOrder": {
                    $scope.$apply(function () {
                        MainService.modeOrder = event.newValue;
                        console.log(event.key, event.newValue);
                    });
                    updateUI();
                    break;
                }
                case "customer-display-modeDeliveryOrder": {
                    $scope.$apply(function () {
                        MainService.modeDeliveryOrder = JSON.parse(event.newValue);
                        console.log(event.key, JSON.parse(event.newValue));
                    });
                }
                case "customer-display-modeTakeAway": {
                    $scope.$apply(function () {
                        MainService.modeTakeAway = JSON.parse(event.newValue);
                        console.log(event.key, JSON.parse(event.newValue));
                    });
                }
                case "customer-display-modeFastFood":{
                    $scope.$apply(function () {
                        MainService.modeFastFood = JSON.parse(event.newValue);
                        console.log(event.key, JSON.parse(event.newValue));
                    });
                }
            }
        });

        if (localStorage) {
            if (localStorage.getItem("customer-display-appliedPaymentMethod")) {
                MainService.appliedPaymentMethod = JSON.parse(localStorage.getItem("customer-display-appliedPaymentMethod"));
            }
            if (localStorage.getItem("customer-display-appliedDiscount")) {
                MainService.appliedDiscount = JSON.parse(localStorage.getItem("customer-display-appliedDiscount"));
            }
            if (localStorage.getItem("customer-display-remainerAmount")) {
                MainService.remainerAmount = localStorage.getItem("customer-display-remainerAmount");
            }
            if (localStorage.getItem("customer-display-isCash")) {
                MainService.isCash = localStorage.getItem("customer-display-isCash") == "true";
            }
            if (localStorage.getItem("customer-display-mode")) {
                MainService.mode = localStorage.getItem("customer-display-mode");
            }
            if (localStorage.getItem("customer-display-modeOrder")) {
                MainService.modeOrder = localStorage.getItem("customer-display-modeOrder");
            }
            if (localStorage.getItem("customer-display-cart")) {
                updateCart(localStorage.getItem("customer-display-cart"));
            }
        }
    });

    app.directive('advertImage', function () {
        return {
            restrict: 'E',
            controller: function ($scope, $http) {
                $http.get('customer-display-advert.json').success(function (data) {
                    var adverts = [];
                    data.adverts.forEach(function (adv) {
                        var date = adv.available.split('-');
                        var now = new Date();
                        if (now > new Date(date[0]) && now < new Date(date[1])) {
                            adverts.push(adv);
                        }
                    });
                    console.log(adverts);
                    $scope.adverts = adverts;
                    updateUI();
                });
            },
            template: '<img ng-repeat="adv in adverts" ng-src="{{adv.image}}" />',
            link: function (scope, element, attrs, ngModel) {
                ngModel.$render = function () {
                    console.log("test")
                }
            }
        };
    });
}());
$(window).resize(function () {
    updateUI();
});
var resizeInterval = 0;
function updateUI() {
    var windowH = $(window).height();
    var windowW = $(window).width();
    var footerH = $('.footer.account-summary').height();
    console.log("updateUI", windowH, windowW);
    $('.order-list-wrapper').height(windowH - footerH);
    $('.background').height(windowH);
    $('.signage-image .slides_container .adv').height(windowH);
    $('.signage-image .slides_container .adv').width(windowW);
    //$(".signage-image").slides({ preload: true, preloadImage: '/images/loading.gif', slideSpeed: 0, play: 5000, pause: 2500, hoverPause: true, prev: 'deals-left', next: 'deals-right' });
    clearTimeout(resizeInterval);
    resizeInterval = setTimeout(function () {
        console.log("timeout settings");
        $('.signage-image').slidesjs({
            width: windowW,
            height: windowH,
            navigation: {
                active: false
            },
            pagination: {
                active: false
            },
            play: {
                effect: "fade",
                auto: true
            }
        });
        $('.slidesjs-container, .slidesjs-control').css({"width": windowW, "height": windowH});
    }, 1000);
}

$(function () {
    updateUI();
});