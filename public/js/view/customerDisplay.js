(function () {
    var app = angular.module('fdm', ['rms']);

    app.controller('FDMController', function (SocketFactory, INFO, $scope, $window, $http, $q, $filter, uiLanguage, userConfig, $rootScope, MainService, $timeout) {
        SocketFactory.on('connect', function (msg) {
            console.log('receive');
        });
        $scope.MainService = MainService;


        //MainService.takeOutInOrder.mode = MainService.takeOutInOrder.schema.paymentPage;

        if (!MainService.getOtherWriteOrderService()) {
            MainService.initOtherWriteOrderService(MainService.schema.modeOtherWriteOrderService.fastFood);
        }

        MainService.isCustomerDisplay = true;

        MainService.mode = MainService.getOtherWriteOrderService().mode = MainService.getOtherWriteOrderService().schema.paymentPage;
        
        MainService.checkIsNotTakeAwayMode = function () {
            return false;
        }

        var updateCart = function (data) {
            try {
                console.log("updateCart", JSON.parse(data), MainService.Cart);
                //MainService.Cart = angular.copy(JSON.parse(data), MainService.Cart);
                MainService.Cart = JSON.parse(data)
                //obj = angular.copy(JSON.parse(event.newValue), obj);
                //console.log("obj.Cart", $scope.Cart);
                if (MainService.Cart.cartList.length) {
                    $('.sidebar').addClass('active');
                }
                else {
                    $('.sidebar').removeClass('active');
                    MainService.appliedPaymentMethod = [];
                    MainService.appliedDiscount = [];
                    MainService.remainerAmount = 0;
                }

                MainService.Cart.cartList.forEach(function (item) {
                    $.extend(true, item, MainService.schema.baseItem, { qty: item.qty, isEdit: item.isEdit });
                    if (item.option) {
                        item.option.forEach(function (opt) {
                            opt.items.forEach(function (optItem) {
                                $.extend(true, optItem, MainService.schema.baseItem, { qty: optItem.qty });
                            });
                        });
                    }
                    if (item.subitem) {
                        item.subitem.forEach(function (subItem) {
                            $.extend(true, subItem, MainService.schema.baseItem, { qty: subItem.qty });
                        });
                    }
                })
                console.log("$scope.Cart", MainService.Cart.cartList);
                $timeout(function () {
                    $('.md-icon.delete, .md-icon.remove').hide();
                });
                $timeout(function () {
                    //MainService.scrollToBottom(".order-list-wrapper");
                    if (angular.element(".list-group-item.active").length) {
                        //var target = ".order-list-wrapper";
                        //var scrollTo = angular.element(".list-group-item.active").offset().top;
                        //angular.element(target).scrollTop(scrollTo);
                        var target = ".order-list-wrapper";
                        var maxScrollable = angular.element(target)[0].scrollHeight - angular.element(target).height();
                        angular.element(target).scrollTop(maxScrollable);
                    }
                }, 10);
            } catch (e) {
                console.log(e);
            }
        }

        var updateOctopusBalance = function (data) {
            if (data) {

            }
            else {

            }
        }

        window.addEventListener('storage', function (event) {
            switch (event.key) {
                case "customer-display-cart": {
                    $scope.$apply(function () {
                        console.log(event.key, JSON.parse(event.newValue));
                        updateCart(event.newValue);
                    });
                    break;
                }
                case "customer-display-combo": {
                    $scope.$apply(function () {
                        console.log(event.key, JSON.parse(event.newValue));
                        angular.copy(MainService.combo, JSON.parse(event.newValue));
                        updateCart(MainService.Cart);
                    });
                    break;
                }
                case "customer-display-appliedPaymentMethod": {
                    $scope.$apply(function () {
                        if (typeof event.newValue === "undefined") return;
                        MainService.appliedPaymentMethod = JSON.parse(event.newValue);
                        console.log(event.key, JSON.parse(event.newValue));
                    });
                    updateUI();
                    break;
                }
                case "customer-display-appliedDiscount": {
                    $scope.$apply(function () {
                        if (typeof event.newValue === "undefined") return;
                        MainService.appliedDiscount = JSON.parse(event.newValue);
                        console.log(event.key, JSON.parse(event.newValue));
                    });
                    updateUI();
                    break;
                }
                case "customer-display-remainerAmount": {
                    $scope.$apply(function () {
                        MainService.remainerAmount = event.newValue;
                        console.log(event.key, event.newValue);
                    });
                    updateUI();
                    break;
                }
                case "customer-display-enquiredOctopusBalance": {
                    $scope.$apply(function () {
                        MainService.enquiredOctopusBalance = JSON.parse(event.newValue);
                        console.log(event.key, event.newValue);
                    });
                    updateUI();
                    break;
                }
                case "customer-display-isCash": {
                    $scope.$apply(function () {
                        MainService.isCash = event.newValue == "true";
                        console.log(event.key, event.newValue);
                    });
                    updateUI();
                    break;
                }
                case "customer-display-mode": {
                    $scope.$apply(function () {
                        MainService.mode = JSON.parse(event.newValue);
                        console.log(event.key, JSON.parse(event.newValue));
                    });
                    updateUI();
                    break;
                }
                case "customer-display-modeOrder": {
                    $scope.$apply(function () {
                        MainService.modeOrder = event.newValue;
                        console.log(event.key, event.newValue);
                    });
                    updateUI();
                    break;
                }
                case "customer-display-modeDeliveryOrder": {
                    $scope.$apply(function () {
                        MainService.modeDeliveryOrder = JSON.parse(event.newValue);
                        console.log(event.key, JSON.parse(event.newValue));
                    });
                    break;
                }
                case "customer-display-modeTakeAway": {
                    $scope.$apply(function () {
                        MainService.modeTakeAway = JSON.parse(event.newValue);
                        console.log(event.key, JSON.parse(event.newValue));
                    });
                    break;
                }
                case "customer-display-modeFastFood": {
                    $scope.$apply(function () {
                        MainService.modeFastFood = JSON.parse(event.newValue);
                        console.log(event.key, JSON.parse(event.newValue));
                    });
                    break;
                }
                case "scrollToTarget": {
                    $timeout(function () {
                        scrollToTarget(event.newValue)
                    }, 100);
                    break;
                }

            }
        });

        function scrollToTarget(itemId) {
            var container = $(".order-list-wrapper"), scrollTo = $('.list-group-item.new-item[itemid="' + itemId + '"]');
            container.animate({
                scrollTop: scrollTo.offset().top - container.offset().top + container.scrollTop()
            }, "fast");
        }

        if (localStorage) {
            if (localStorage.getItem("customer-display-appliedPaymentMethod")) {
                MainService.appliedPaymentMethod = JSON.parse(localStorage.getItem("customer-display-appliedPaymentMethod"));
            }
            if (localStorage.getItem("customer-display-appliedDiscount")) {
                MainService.appliedDiscount = JSON.parse(localStorage.getItem("customer-display-appliedDiscount"));
            }
            if (localStorage.getItem("customer-display-remainerAmount")) {
                MainService.remainerAmount = localStorage.getItem("customer-display-remainerAmount");
            }
            if (localStorage.getItem("customer-display-isCash")) {
                MainService.isCash = localStorage.getItem("customer-display-isCash") == "true";
            }
            if (localStorage.getItem("customer-display-mode")) {
                MainService.mode = localStorage.getItem("customer-display-mode");
            }
            if (localStorage.getItem("customer-display-modeOrder")) {
                MainService.modeOrder = localStorage.getItem("customer-display-modeOrder");
            }
            if (localStorage.getItem("customer-display-cart")) {
                updateCart(localStorage.getItem("customer-display-cart"));
            }
        }
    });

    app.directive('advertImage', function () {
        return {
            restrict: 'E',
            controller: function ($scope, $http, $location) {
                var query = $location.search();
                $http.get('customer-display-advert.json').success(function (data) {
                    console.log("till", data.adverts, query, query.till);
                    var availableAdverts = [];
                    var adverts = []
                    if (query.till && data.adverts[query.till]) {
                        data.adverts[query.till].forEach(function (adv) {
                            adverts.push(adv);
                        });
                    }
                    else {
                        for (key in data.adverts) {
                            data.adverts[key].forEach(function (adv) {
                                var checkExists = adverts.filter(function (x) { return x.image == adv.image; });
                                if (!checkExists.length) {
                                    adverts.push(adv);
                                }
                            });
                        }
                    }
                    adverts.forEach(function (adv) {
                        var date = adv.available.split('-');
                        var now = new Date();
                        if (now > new Date(date[0]) && now < new Date(date[1])) {
                            availableAdverts.push(adv);
                        }
                    });
                    console.log(adverts, availableAdverts);
                    $scope.adverts = availableAdverts;
                    updateUI();
                });
            },
            template: '<img class="signage-advert-image" ng-repeat="adv in adverts" ng-src="{{adv.image}}" />',
            link: function (scope, element, attrs, ngModel) {
                ngModel.$render = function () {
                    console.log("test");
                    //updateUI();
                }
            }
        };
    });
}());
$(window).resize(function () {
    updateUI();
});
var resizeInterval = 0;
function updateUI() {
    var windowH = $(window).height();
    var windowW = $(window).width();
    var footerH = $('.footer.account-summary').height();
    console.log("updateUI", windowH, windowW);
    $('.order-list-wrapper').height(windowH - footerH);
    $('.background, .md-grid-container').height(windowH);
    $('.signage-image .slides_container .adv').height(windowH);
    $('.signage-image .slides_container .adv').width(windowW);
    //$(".signage-image").slides({ preload: true, preloadImage: '/images/loading.gif', slideSpeed: 0, play: 5000, pause: 2500, hoverPause: true, prev: 'deals-left', next: 'deals-right' });
    clearTimeout(resizeInterval);
    resizeInterval = setTimeout(function () {
        console.log("timeout settings");
        if ($('.signage-image .signage-advert-image').length > 1) {
            $('.signage-image').slidesjs({
                width: windowW,
                height: windowH,
                navigation: {
                    active: false
                },
                pagination: {
                    active: false
                },
                play: {
                    effect: "fade",
                    auto: true
                }
            });
        }
        $('.slidesjs-container, .slidesjs-control').css({ "width": windowW, "height": windowH });
    }, 1000);
}

//$(function () {
//    updateUI();
//});