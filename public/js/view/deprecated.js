// from jQuery.makeEditable plugin
// deprecated
  function addEventForGlobal( element, editorElement ) {
    $('body').click(function(e){
      e.stopPropagation();
      // console.log(e);
      // restore the current cell, because current cell click will not propagate to td parent with stopPropagation()
      // $(element).html(editor._originalElement);
      // trigger blur to custom drop down
      editorElement.find('.select-dropdown').trigger('customBlur');

      editorElement.remove();
      // console.log(editorElement);
      $( element ).find('.color-box').removeClass('hidden');
      $( element ).parents('table tbody tr').removeClass('active');
    });
  }

// from adminController
$scope.addItem = function addItem() {
  // add two rows
  var posID = $('#item-list-container table tbody tr').length,
      row = '<tr role="row"> \
                <td class="helper">新<a href="#"><i class="md-icon indicator">cancel</i></a></td> \
                <td class="editable code" data-field-type="textfield" data-bind-row'+posID+'="code"></td> \
                <td class="editable name1" data-field-type="textfield" data-bind-row'+posID+'="name1"></td> \
                <td class="editable name2" data-field-type="textfield" data-bind-row'+posID+'="name2"></td> \
                <td class="editable category dropdown" data-field-type="dropdown" data-field-style="normal" data-bind-row'+posID+'="group"><span class="group">AA</span></td> \
                <td class="editable sortNo" data-field-type="textfield" data-bind-row'+posID+'="sortNo">#</td> \
                <td class="editable color dropdown" data-field-type="dropdown" data-field-style="color" data-bind-row'+posID+'="color"><div class="color-box value" title=""></div></td> \
                <td class="editable plu" data-field-type="textfield">-</td> \
                <td class="editable price1" data-field-type="textfield" data-bind-row'+posID+'="unitprice">0</td> \
                <td class="editable point" data-field-type="textfield">-</td> \
                <td class="editable printer" data-field-type="textfield">-</td> \
                <td class="editable separate-print" data-field-type="textfield" data-bind-row'+posID+'="separatePrint">-</td> \
                <td class="editable label" data-field-type="textfield" data-bind-row'+posID+'="label">-</td> \
                <td class="editable service-charge" data-field-type="checkbox">-</td> \
                <td class="editable set" data-field-type="checkbox">-</td> \
                <td class="editable subitem" data-field-type="textfield">-</td> \
                <td class="editable coupon" data-field-type="textfield" data-bind-row'+posID+'="coupon">-</td> \
                <td class="editable remark" data-field-type="textfield" data-bind-row'+posID+'="remark">-</td></tr>',
    $row = $(row).addClass('newRow').data('rowid', posID );
    // resort table using the current sort; set to false to prevent resort, otherwise 
    // any other value in resort will automatically trigger the table resort. 
    resort = false;

    console.log('add row to item list');
    $('#item-list-container table')
      .find('tbody').prepend($row)
      .trigger('addRows', [$row, resort])
      // .trigger('update', [ true, [ [1,0] ] ] ); // trigger update for tablesorter [1,0]

   var newItemData = {
       ID: -1,
       code: '',
       color: 'rms-palette-grass',
       coupon: '',
       group: '',
       kitchenmsggroup: null,
       label: '',
       modifiergroup: null,
       name: {
         '001': '',
         '003': ''
       },
       plu: '',
       printer: '',
       remark: '',
       rowID: -1,
       separatePrint: 0,
       sortOrder: '1',
       time: '02:00-23:59',
       unitprice: 999
    };

    makeFieldEditable( $row );

    // method 1: store in an array, $scope.newItemArr.push( newItemData ); hard to relate it from outside without index
    // method 2: $scope.newItemObj[ rowID ] // can relate it with rowID
    // $scope.newItemObj[posID] = newItemData;
    // method 3: store data object in row // destroy the row will destroy the data together
    $row.data('rowdata', newItemData);
    $scope.newItemObj[posID] = newItemData;
    // console.log($row.data('rowdata'));
    var item = new Item( 'row'+posID, 'newItemData' );

    // loop through the object and assign value to field
    for( var key in newItemData ) {
     if( key === 'name') {
       for( var key2 in newItemData[key] ) {
         item.set( key, newItemData[key][key2] );
       }
     } else {
       item.set( key, newItemData[key] );
     }
    }
    
    scrollToRow( '#item-list-container', 'tr.newRow:eq(0)');
};

// it is not used at the moment
$scope.loadItemDetails = function( itemCat, itemCode ) {
  // data retreived from JSON for single item, for layout and data binding test
  // console.log(itemCode);
  // console.log(getItemByCode( itemCode, $scope.data.category ));
  console.log('=======================================');
  console.log(itemCat);
  console.log(itemCode);
  console.log('=======================================');
  var loadedData = getItemByCode( itemCat, itemCode, $scope.data.category );
  var $targetForm = $("#item-details");
  console.log(loadedData);
  // console.log($("#item-details").find('#code'));
  _bindData( $targetForm.find('#code'), loadedData.code);
  _bindData( $targetForm.find('#group'), loadedData.group);
  _bindData( $targetForm.find('#name1'), loadedData.name['001']);
  _bindData( $targetForm.find('#name2'), loadedData.name['003']);
  _bindData( $targetForm.find('#kitchen-name'), loadedData.label);
  _bindData( $targetForm.find('#printer'), loadedData.printer);
  _bindData( $targetForm.find('.item-color-option-container'), loadedData.color);
  // $targetForm.find('#code').val( loadedData.code );
  // $("#item-details").find('#group').val( loadedData.group );
  // $("#item-details").find('#name1').val( loadedData.name['001'] );
  // $("#item-details").find('#name2').val( loadedData.name['003'] );
  // $("#item-details").find('#printer').val( loadedData.printer );

  // load subitem list in THIS item
  $scope.renderSubItemList( loadedData.subitem );
};

// callback for food list in editable cell by makeEditable() plugin
var foodRowCallback = function ( element ) {
  console.log(this);
  // console.log('I am row callback');
  // console.log( element );
  // console.log($(element).parents('tr').find('.code').text());
  // console.log($(element).parents('tr').find('.code .value').text());
  // console.log($(element).parents('tr').find('.category').text());
  // console.log($(element).parents('tr').find('.category .value').text());
  var code = $(element).parents('tr').find('.code').text() || $(element).parents('tr').find('.code .value').text();
  var cat = $(element).parents('tr').find('.category .value').text() || $(element).parents('tr').find('.category').text();
  // console.log(code);

  // if cell is modified, don't load, will dead because when category and code is changed, cannot find anything
  if( $('.item-table').find('.modified').length === 0 ) {
    console.log($scope);
    $scope.loadItemDetails( cat, code );
  }
  else
    console.log("data is modified, details data will not load");

  $(element).parents('tr').on('change', 'input', function(){
    console.log("selected row have input updated");
  });
};


// $('#item-shortlist-container, #item-list-container').on('mouseenter mouseleave', 'tr',function( e ){
//   // var orgText = $(this).find('.indicator').text();
//   // console.log(orgText);
//   switch( e.type ) {
//     case 'mouseenter':
//       // changeIndicator( 'add' );
//       break;
//     case 'mouseleave':
//       $(this)
//         // changeIndicator( 'remove' );
//       break;
//   }
// })


/*$('.tab-container ul.tabs li').on('click', 'a', function(){
 console.log('me after 2');
 // adjustIndicator.apply( this );
 // console.log('test');
  // var $link = $(this);
  // adjustIndicator( $link );
});
*/

var $tableObj = $( '#employee-list-container table tbody' );

$tableObj.find('.editable[data-field-type="textfield"]').makeEditable( {
  // rowCallback: foodRowCallback,
  $scope: $scope // for some callbacks using $scope to access original caller scope for data access and sharing
});

$tableObj.find('.editable.group[data-field-style="normal"]').makeEditable( {dropdown: {
  cssClass: "group-option", // select css class
  optionData: $scope.employeeGroup,
  optionKey: null,
  type: "employee-group-option",
  // rowCallback: foodRowCallback,
  $scope: $scope
}} );

$tableObj.find('.editable.type[data-field-style="normal"]').makeEditable( {dropdown: {
  cssClass: "type-option", // select css class
  optionData: $scope.employeeType,
  optionKey: null,
  type: "employee-type-option",
  // rowCallback: foodRowCallback,
  $scope: $scope
}} );



$scope.loadDetails = function( code ) {
  // console.log(_bindData);
  var loadedData = getEmployeeByCode( code, this.data ),
      $targetForm = $('#employee-details');

  console.log(loadedData);
  _bindData( $targetForm.find('#employee-code'), loadedData.XF_STAFFCODE);
  _bindData( $targetForm.find('#employee-name'), loadedData.XF_NAME);
  _bindData( $targetForm.find('#employee-password'), loadedData.XF_PASSWORD);
  $targetForm.find('.employee-group-option.select-wrapper input.select-dropdown').val( loadedData.XF_USERGROUP );
  $targetForm.find('.employee-type-option.select-wrapper input.select-dropdown').val( loadedData.XF_STAFFTYPE );
  // _bindData( $targetForm.find('#code'), loadedData.XF_USERGROUP);
  // _bindData( $targetForm.find('#code'), loadedData.XF_STAFFTYPE);
};

// $tableObj.find('tr').on('dblclick', function(e) {
//     if( $.fn.makeEditable.editable === true )
//        return;

//     var code = $(this).find('.code').text() ||  $(this).find('.code .value').text();
//     console.log(code);
//     $('ul.tabs').tabs('select_tab', 'employee-details');

//     // if cell is modified, don't load, will dead because when category and code is changed, cannot find anything
//     if( $(this).find('.modified').length === 0 )
//       $scope.loadDetails( code );
//     else
//       console.log("data is modified, details data will not load");

//     // $(this).on('change', 'input', function(){
//     //   console.log("selected row have input updated");
//     // });
// });
// afterRender.apply(this, [afterRenderOpts] ) || noop(); // it can be this.afterRender.run( this.