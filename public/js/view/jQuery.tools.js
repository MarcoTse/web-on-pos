$.fn.injectSVGIcon = function(){
  return $(this).each(function(){
    // mcss-svg-src
    // mcss-icon
    var src = $(this).attr('mcss-svg-src');
    // console.log($(this));
    $(this).load(src, function() {
      $(this).find('svg').attr({'height': '100%', 'width': '100%', 'preserveAspectRatio' : 'xMidYMid meet'})
      // $(this).attr('class', $(this).attr('color-class'));
      // console.log("completed");
      // console.log($(this).find('svg').attr({'height': '100%', 'width': '100%'}));
    });
  }); // return
}; // injectSVGIcon