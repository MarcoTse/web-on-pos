/*
  any key or button can trigger this abandon changes
  determine by current active menu and active tab
*/
function getItemObjByObjPos( arrayPos, dataArr ) {
  // console.log(arguments);
  // console.log(typeof arrayPos);
  // undefined or -1 is new data
  if( arrayPos === -1 || isUndefined( arrayPos ) ) {
      return null; // new row
  } else {
    // 2-dimension position
    var posNoArr = arrayPos.split('-');
    return dataArr[posNoArr[0]].items[posNoArr[1]];
  }
}

function getObjByPos( arrayPos, dataArr ) {
  // console.log(arguments);
  // console.log(typeof arrayPos);
  // undefined or -1 is new data

  // console.log(dataArr);
  // console.log(isUndefined( arrayPos ));
  // console.log(arrayPos);
  if( arrayPos === -1 || isUndefined( arrayPos ) ) {
      return null; // new row
  } else {
    // 2-dimension position

    return dataArr[arrayPos];
  }
}



function abandonChanges() {
  console.log('abandon changes');
  var $table = $('table'),
  // var $table = $('#item-list-container table, #category-list-container table, #employee-list-container table, #employee-shortlist-container table, #item-shortlist-container table, #subitem-list table'),
      $tdModified = $table.find('td.modified'),
      $anyModified = $table.find('.modified'),
      $newRow = $table.find('tr.newRow');

  // console.log($tdModified);

  $tdModified.each(function(){
    var oVal = $(this).data('oval'),
        $row = $(this).parents('tr'),
        $container = $(this).parents('.table-container'),
        fieldType = $(this).data('field-type'),
        fieldStyle = $(this).data('field-style'),
        rowID = $row.data('rowid'),
        dataPos = isDefined( $row.data('object-pos') ) ? $row.data('object-pos') : -1,
        bindKey = $(this).data('bind-row'+rowID);

    // item
    if( $container.hasID( 'item-list-container' ) || $container.hasID('item-shortlist-container')) {
      dataBind = dataPos !== -1 ? getItemObjByObjPos( dataPos, $rootScope.itemAdmin.data.category ) : row.data('rowdata');
    }

    // employee
    if( $container.hasID('employee-list-container') || $container.hasID('employee-shortlist-container')) {
      dataBind = dataPos !== -1 ? getObjByPos( dataPos, $rootScope.employeeAdmin.data ) : row.data('rowdata');
    }
    
    // category


    // console.log(dataBind);
    // console.log(dataPos);
    // console.log(oVal);
    bindValToObj( dataBind, bindKey, oVal );

    switch(fieldType) {
      case 'textfield':
        $(this).text( oVal );
        break;
      case 'dropdown':
        if( fieldStyle === 'color') {
          var cssClass = $(this).find('.value').attr('class').split(" ").splice(0,2);
          cssClass.push(oVal);
          $(this).find('.value').attr( 'class', cssClass.join(" ") );
        } else {
          $(this).text( oVal );
        }
        break;
    } // switch

    // reset object
    $scope.newItemObj = {};
    // $scope.newItemArr = [];
});
  // console.log(fieldType);
  // console.log(typeof fieldType);
  // $tdModified.data('field-style') // category, color
  // if( fieldType === 'textfield') {
  // }

  // remove new table data
  $newRow.remove();
  $anyModified.removeClass('modified');

  // remove new details data
  $('.form-container form').removeClass('newData');

  // reset data in form (lazy method to reset)
  var curTab = $('.menu-area.active ul.tabs a.active').attr("href");
  if( curTab === '#item-details') {
    $('#item-shortlist-container tr#itemlist-0 .helper').click();
  }

  // disable editable mode
  // $.fn.makeEditable.editable = false;
  // $('button.edit').removeClass('enable');
  if( $('button.edit').hasClass('enable') ) {
    disableEdit();
  }
} // abandonChanges

var continueEdit = function(){
  console.log('continue');
  // $.fn.makeEditable.editable = true;
  // if( !$('button.edit').hasClass('enable') )
  //   $('button.edit').click();
}

// switch menu
function switchMenu() {
  $(this).siblings('.switch').removeClass('hidden');
  $(this).addClass('hidden');
  $('.menu-area').addClass('hidden').removeClass('active');
}; // switchMenu

function enableEdit() {
  $.fn.makeEditable.editable = true;
  $('button.edit').addClass('enable');
  $("button.add-sub-item").toggleClass('hidden');
  $("button.add-sub-item-type").toggleClass('hidden');

  $('.data-binder:not([type=checkbox], div)').attr('disabled', false); // div wrapper
  $('.data-binder[type=checkbox]').attr('disabled', false); // checkbox
  $('div.data-binder').find('input').attr('disabled', false); // other input

  $('.edit-group').removeClass('hidden');
}

function disableEdit() {
  $.fn.makeEditable.editable = false;
  $('button.edit').removeClass('enable');
  $("button.add-sub-item").removeClass('hidden');
  $("button.add-sub-item-type").removeClass('hidden');

  $('.data-binder:not([type=checkbox], div)').attr('disabled', true); // div wrapper
  $('.data-binder[type=checkbox]').attr('disabled', true); // checkbox
  $('div.data-binder').find('input').attr('disabled', true); // other input

  $('.edit-group').addClass('hidden');
}

// function customHide() {
//   $(this).addClass('hidden');
// }

// function customHide() {
//   $(this).addClass('hidden');
// }

// function customHide() {
//   $(this).addClass('hidden');
// }

$('button.go-item').on('click', function() {
    switchMenu.apply(this);
    $('#item-menu')
      .removeClass('hidden')
      .addClass('active');
});

$('button.go-employee').on('click', function() {
    switchMenu.apply(this);
    $('#employee-menu')
      .removeClass('hidden')
      .addClass('active');
      $('#employee-form-content ul.tabs').tabs('select_tab', 'employee-list-container');

    // setTimeout(function(){$('.tab-container ul.tabs li a[href=#employee-details]').click();}, 500)


    // $('#employee-form-content ul.tabs').find('li.tab a').index($target);
});


// abandon changes, restore modified
$('button.abandon').on('click', function() {

    // bail rule
    // if( !$.fn.makeEditable.editable ) {
    if( !$('button.edit').hasClass('enable') ) {
      // console.log('done nothing');
      return;
    }
    // if confirm
    if( $('.newRow, .modified').length > 0 )
      $('#abandon-dialog').openModal();
});

$('.abandon-changes').on('click', function() {
    console.log($.fn.makeEditable.editable);
    // bail rule
    if( !$.fn.makeEditable.editable ) {
      // console.log('done nothing');
      return;
    }
    // if confirm
    abandonChanges();
});

$('#abandon-dialog a').click(function(e){e.preventDefault();});

$('button.refresh').on('click', function() {
    refreshPage();
});

// reset table filter
$('button.reset-view').on('click', function(){
  // console.log("cat tr");
  $('#category-shortlist-container .data-table tbody tr.active').removeClass('active');
});

// toggle table editable
$('button.edit').on('click', function() {
    
    // $(this).toggleClass('enable');
    // $("button.add-sub-item").toggleClass('hidden');
    // $("button.add-sub-item-type").toggleClass('hidden');

    // assume it is disabled by default
    // if it is enabled and if 
    if( $(this).hasClass('enable') ) {
      // console.log($.fn.makeEditable.editable);
      // $.fn.makeEditable.editable = true;
        // $.fn.makeEditable.editable = true;
        if( $('.newRow, .modified').length > 0 ) {
          $('#abandon-dialog').openModal();
          return;
        }
    }

    if( $.fn.makeEditable.editable ) {
      disableEdit();
    }
     
    else {
      enableEdit();      
    }
    // $.fn.makeEditable.toggleEditMode();
});

function saveData() {
  // reset modified status
  var $table = $('#item-list-container table, #category-list-container table, #employee-list-container table, #item-shortlist-container table, #subitem-list table'),
      $anyModified = $table.find('.modified');

  $anyModified.removeClass('modified');

  // kill all saved oVal
  var $withOVal = $('[data-oval]');

  var inclusionList = ['oval'];
  removeAllData( $withOVal, inclusionList, true );
  removeDataAttributes( $withOVal, inclusionList, true );

  // submit data to server
}

$('button.save').on('click', function() {
  // save the data
  saveData();
  // close the edit mode
  disableEdit();
});

// $('button.save').click(function(){
//   var curTab = $('.menu-area.active ul.tabs a.active').attr("href");
//   if( !$.fn.makeEditable.editable )
//     return;

//   // console.log(curTab);
//   switch( curTab ) {
//     case '#item-list-container':
//       // create new data object
//       // $scope.newFoodData = [];
//       var $newRow = $('#item-list-container table').find('.newRow');

//       console.log("save item list");
//       $newRow.each(function(){
//       // console.log(getfieldVal.apply(this, ['.color']));
//       var newData = {
//           code: getfieldVal.apply(this, ['.code']),
//           color: getfieldVal.apply(this, ['.color']),
//           group: getfieldVal.apply(this, ['.category']),
//           kitchenmsggroup: null,
//           label: getfieldVal.apply(this, ['.label']),
//           modifiergroup: null,
//           name: {
//             '001': getfieldVal.apply(this, ['.name2']),
//             '003': getfieldVal.apply(this, ['.name1'])
//           },
//           printer: getfieldVal.apply(this, ['.printer']),
//           time: "00:00-23:59",
//           unitprice: getfieldVal.apply(this, ['.price1'])
//       };

//       console.log(newData);
//         // $scope.newFoodData( newData );
//       });
//       break;
//     case '#item-details':
//         // reset all fields
        
//       break;
//     case '#display-category':
      
//       break;
//     case '#employee-list-container':
      
//       break;
//     case '#employee-details':
      
//       break;
//     default:
//       $.fn.makeEditable.editable = false;
//       $(this).removeClass('enable');
//       break;
//   }

    
//     // reset table sort status, if not, will error
//     $('table').trigger('update', [[0,0]]);
//     return false;
// });

$('button.add').click(function(){
  var curTab = $('.menu-area.active ul.tabs a.active').attr("href")
      $scope = $rootScope.itemAdmin;
      helper = $rootScope.helper;
  if( !$.fn.makeEditable.editable )
    $('button.edit').click();

  // console.log(curTab);
  switch( curTab ) {
    case '#item-details':
    case '#item-list-container':
      // $scope.addItem();
      // $scope.$watch();

        var table = '#item-list-container table',
            posID = $( table ).find('tbody tr').length;

        helper.addRow( {
          target: table,
          rowTemplate: '<tr role="row"> \
                       <td class="helper">新<a href="#"><i class="md-icon indicator">cancel</i></a></td> \
                       <td class="editable code" data-field-type="textfield" data-bind-row'+posID+'="code"></td> \
                       <td class="editable name1" data-field-type="textfield" data-bind-row'+posID+'="name1"></td> \
                       <td class="editable name2" data-field-type="textfield" data-bind-row'+posID+'="name2"></td> \
                       <td class="editable category dropdown" data-field-type="dropdown" data-field-style="normal" data-bind-row'+posID+'="group"><span class="group">AA</span></td> \
                       <td class="editable sortNo" data-field-type="textfield" data-bind-row'+posID+'="sortNo">#</td> \
                       <td class="editable color dropdown" data-field-type="dropdown" data-field-style="color" data-bind-row'+posID+'="color"><div class="color-box value" title=""></div></td> \
                       <td class="editable plu" data-field-type="textfield">-</td> \
                       <td class="editable price1" data-field-type="textfield" data-bind-row'+posID+'="unitprice">0</td> \
                       <td class="editable point" data-field-type="textfield">-</td> \
                       <td class="editable printer" data-field-type="textfield">-</td> \
                       <td class="editable separate-print" data-field-type="textfield" data-bind-row'+posID+'="separatePrint">-</td> \
                       <td class="editable label" data-field-type="textfield" data-bind-row'+posID+'="label">-</td> \
                       <td class="editable service-charge" data-field-type="checkbox">-</td> \
                       <td class="editable set" data-field-type="checkbox">-</td> \
                       <td class="editable subitem" data-field-type="textfield">-</td> \
                       <td class="editable coupon" data-field-type="textfield" data-bind-row'+posID+'="coupon">-</td> \
                       <td class="editable remark" data-field-type="textfield" data-bind-row'+posID+'="remark">-</td></tr>',
          dataAbstract: {
              ID: -1,
              code: 'XXX',
              color: 'rms-palette-grass',
              coupon: '',
              group: 'AA',
              kitchenmsggroup: null,
              label: '',
              modifiergroup: null,
              name: {
                '001': 'NEW Name',
                '003': '新名稱'
              },
              plu: '',
              printer: '',
              remark: '',
              rowID: -1,
              separatePrint: 0,
              sortOrder: '1',
              time: '02:00-23:59',
              unitprice: 999
           },
           $scope: $rootScope.itemAdmin
        });
      break;
    // case '#item-details':
    //     // reset all fields
    //     // $('#item-details form').addClass('newData');

    //     // data is read from just added new row
    //     $scope.addItem();
    //     // $scope.$watch();
    //   break;
    case '#display-category':
        var table = '#category-list-container table',
            posID = $( table ).find('tbody tr').length;

        helper.addRow( {
          target: table,
          rowTemplate: '<tr role="row"> \
              <td class="editable" data-field-type="textfield" data-bind-catrow'+posID+'="code">NEW</td> \
              <td class="editable" data-field-type="textfield" data-bind-catrow'+posID+'="sortNo">-</td> \
              <td class="editable" data-field-type="textfield" data-bind-catrow'+posID+'="name1">NEW</td> \
              <td class="editable" data-field-type="textfield" data-bind-catrow'+posID+'="name2">NEW</td> \
              <td class="editable" data-field-type="checkbox" data-bind-catrow'+posID+'="enabled">-</td> \
              <td class="editable color dropdown" data-field-type="dropdown" data-field-style="color" data-bind-catrow'+posID+'="color"><div class="color-box value rms-palette-orange" title="rms-palette-orange"></div></td></tr>',
          dataAbstract: {
              ID: -1,
              code: "AA",
              sortNo: 0,
              color: "rms-palette-orange",
              icon: 8,
              items: [],
              memberOnly: false,
              enabled: false,
              name: {
                  '001': 'NEW Name',
                  '003': '新名稱'
                },
              specialOffer: false
           },
           $scope: $rootScope.itemAdmin,
           bindUid: 'catrow'
        });
      break;
    case '#employee-details':
    case '#employee-list-container':
      var table = '#employee-list-container table',
          posID = $( table ).find('tbody tr').length;

      helper.addRow( {
        target: table,
        rowTemplate: '<tr class=""> \
        <td class="helper">新<a href="#"><i class="md-icon indicator">cancel</i></a></td> \
        <td class="editable code" data-field-type="textfield" data-bind-row'+posID+'="XF_STAFFCODE">NEW</td> \
        <td class="editable name1" data-field-type="textfield" data-bind-row'+posID+'="XF_NAME">NEW Username</td> \
        <td class="editable password" data-field-type="textfield" data-bind-row'+posID+'="XF_PASSWORD"><span class="value">PASSWORD</span></td> \
        <td class="editable type dropdown" data-field-type="dropdown" data-field-style="normal" data-bind-row'+posID+'="XF_STAFFTYPE"><span class="value">0</span></td> \
        <td class="editable group dropdown" data-field-type="dropdown" data-field-style="normal" data-bind-row'+posID+'="XF_USERGROUP">AD</td></tr>',
        dataAbstract: {
          XF_NAME: "New Employee Name",
          XF_PASSWORD: "9999",
          XF_STAFFCODE: "9999",
          XF_STAFFTYPE: "9",
          XF_USERGROUP: "AC",
        },
        $scope: $rootScope.employeeAdmin
      });
      // var row = '<tr class=""><td class="helper"></td><td class="editable code" data-field-type="textfield">NEW</td><td class="editable" data-field-type="textfield">NEW</td><td class="editable" data-field-type="textfield"><span class="value">NEW</span></td><td class="editable type dropdown" data-field-type="dropdown" data-field-style="normal"><span class="value">4</span></td><td class="editable group dropdown" data-field-type="dropdown" data-field-style="normal">AD</td></tr>',
      //   $row = $(row).addClass('newRow'),
      //   // resort table using the current sort; set to false to prevent resort, otherwise 
      //   // any other value in resort will automatically trigger the table resort. 
      //   resort = false;
      
      //   $('#employee-list table')
      //     .find('tbody').prepend($row)
      //     .trigger('addRows', [$row, resort]);

      //   makeFieldEditable( $row );
      break;
    // case '#employee-details':
      
    //   break;
    default:
      $.fn.makeEditable.editable = false;
      $(this).removeClass('enable');
      break;
  }

    
    
    $('table').trigger('update', [[0,0]]);
    return false;
});

$('a.continue-edit').click(continueEdit);
$('body').on('click' ,'.lean-overlay', continueEdit)



/*
  navigate previous and next row or next set of data depends on current active menu > active tab
  in food list, this will select the next row relatively to current, if there is no selected row, first row is assumed

  in food details, this will select the next row of data relatively to current in the food list table, if there is no selected row, first row is assumed, there will be position mark(last selected table row) of current set of match item to the form like data-data-row="", the mark consists of new and existing data (since new row might be added)
*/
var navControl = function( target ){
  var helper = $rootScope.helper;
  // var curTab = $('.menu-area.active ul.tabs a.active').attr("href");

  var $tableObj = $( target ),
      $row = $tableObj.find('tr.active'),
      $prevRow = helper.isFiltered( $tableObj ) ? $row.prevAll(':not(".filtered"):eq(0)') : $row.prev(),
      $nextRow = helper.isFiltered( $tableObj ) ? $row.nextAll(':not(".filtered"):eq(0)') : $row.next(),
      direction = $(this).hasClass('next') ? 'next' : 'prev';

  switch( direction ) {
    case 'next':
      $nextRow.find('.helper').focus().click();
      break;
    case 'prev':
      $prevRow.find('.helper').focus().click();
      break;
    default:
      break;
  }
};

$('button.next').click(function(e){
  navControl.apply( $(this), ['#item-shortlist-container table'] );
});

$('button.prev').click(function(e){
  navControl.apply( $(this), ['#item-shortlist-container table'] );
});

$('button.last').click(function(){
  if( $('#item-list-container table tbody').length > 0) {
    var target = '#item-list-container';
    $(target).find('table tbody td.helper:last()').click();
    scrollToRow(target, 'tr:last');
  } else {
    target = '#item-shortlist-container'
    $(target).find('table tbody td.helper:last()').click();
    scrollToRow(target, 'tr:last');
  }
});

$('button.first').click(function(){
  if( $('#item-list-container table tbody').length > 0) {
    var target = '#item-list-container';
    $(target).find('table tbody td.helper:first()').click();
    scrollToRow(target, 'tr:first');
  } else {
    target = '#item-shortlist-container'
    $(target).find('table tbody td.helper:first()').click();
    scrollToRow(target, 'tr:first');
  }
});



// create SVG icon from SVG file by custom jQuery plugin
$('.mcss-icon').injectSVGIcon();