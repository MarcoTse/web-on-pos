var TYPES = require('tedious').TYPES;
var moment = require('moment');
var async = require("async");
var S = require('string');
var fs = require('fs');

var languageCode = "001";
var DefaultPrinter = "";
var _food = {};

var database = require("./database.js");

exports.create = function (lang, p) {

    languageCode = lang;
    DefaultPrinter = p;


    return {
        setData: setData,
        dailyReport: dailyReport,
        dailyAttendanceReport: dailyAttendanceReport,
        dailyVoidBillReport: dailyVoidBillReport,
        dailyPaymentDetailReport: dailyPaymentDetailReport,
        dailyDiscountReport: dailyDiscountReport,
        dailyPerHourSalesReport: dailyPerHourSalesReport,
        dailySalesNetReport: dailySalesNetReport,
        dailyMostSalesFoodReport: dailyMostSalesFoodReport,
        dailyCategoriesReport: dailyCategoriesReport,
        dailyFoodTimeslotReport: dailyFoodTimeslotReport,
        dailyCancelItemReport: dailyCancelItemReport,
        dailyArchiveReport: dailyArchiveReport, // pos only
        dailyItemReportByCategory: dailyItemReportByCategory, // pos only
        dailyVoidECRReport: dailyVoidECRReport, // pos only
        dailyWatchServiceReport: dailyWatchServiceReport, // pos only
        dailyPaymentCommissionReport: dailyPaymentCommissionReport, // cloud only
        dailyBillDetailReport: dailyBillDetailReport // cloud only

    }
}
var GetGlobalTransactionStatement =
       {
           sql: "SELECT t.[refNo], [startTime] ,td.tableCode, i.name1 ,t.[price] , td.qty, td.[itemId] , td.[type], td.seq, td.[time] as [Time], staff.name as staff, s2.name as ApproveName, td.voidRemark, td.price as subPrice,td.round , [option].name1 as optionName, [subitem].name1 as SubitemName, category.name1 as CategoryName, category.categoryId , og.name1 as CategoryOption, ogo.optionGroupCode FROM [##transaction##] as t  inner join [##transactionD##] as td on  td.refNo = t.refNo   Left join item as i on i.itemId = td.itemId   Left join [option] on  [option].[optionCode] = td.itemId       Left join [staff] on staff.username = td.staffId  Left join [staff] as s2 on s2.username = td.approveStaffId       Left join [subitem] on [subitem].subitemCode = td.itemId  Left join [itemCategory] on [itemCategory].itemId = td.itemId         Left join category  on category.categoryId = [itemCategory].categoryId         Left join optionGroupOption as ogo on td.itemId = ogo.optionCode       Left join optionGroup as og on ogo.optionGroupCode = og.optionGroupCode       where t.void = 0  ##conditionWhere##     order by t.refNo, td.round, td.seq, t.startTime, td.time;",
           archiveSql: "select * from (select * from (SELECT i.type as openkey, td.customName, t.[refNo],t.[startTime], t.transTime , t.void,\
            td.tableCode, t.[price], td.qty, td.[itemId] , td.seq, td.[time] as [Time], staff.name as staff, s2.name as ApproveName, td.voidRemark, \
            td.price as subPrice, td.round , td.[type],  (i.name1 + '||' + isnull(i.name2, '') + '||' + isnull(i.name3,'')) as name1 , ([option].name1 + '||' + isNull([option].name2, '') + '||' + isNull([option].name3, '')) as optionName, [subitem].name1 as SubitemName, (category.name1 +'||'+ isnull(category.name2, '') + '||' + isnull(category.name3, '')) as CategoryName, (og.name1 + '||' + isnull(og.name2, '') + '||' + isnull(og.name3,'')) as CategoryOption,  category.categoryId ,\
             ogo.optionGroupCode FROM [transaction] as t  \
                inner join [transactionD] as td on  td.refNo = t.refNo \
                Left join item as i on i.itemId = td.itemId \
                Left join [option] on  [option].[optionCode] = td.itemId \
                Left join [staff] on staff.staffId = td.staffId \
                Left join [staff] as s2 on s2.staffId = td.approveStaffId \
                Left join [subitem] on [subitem].subitemCode = td.itemId \
                Left join [itemCategory] on [itemCategory].itemId = td.itemId \
                Left join category  on category.categoryId = [itemCategory].categoryId \
                Left join optionGroupOption as ogo on td.itemId = ogo.optionCode \
                Left join optionGroup as og on ogo.optionGroupCode = og.optionGroupCode \
                where void = 0   ##conditionWhere1## \
            ) as [templ] union \
            select * from (SELECT i.type as openkey, td.customName, t.[refNo],t.[startTime],t.transTime , t.void,\
            td.tableCode, t.[price], td.qty, td.[itemId] , td.seq, td.[time] as [Time], staff.name as staff, s2.name as ApproveName, td.voidRemark,\
            td.price as subPrice, td.round , td.[type],  (i.name1 + '||' + isnull(i.name2, '') + '||' + isnull(i.name3,'')) as name1 , ([option].name1 + '||' + isNull([option].name2, '') + '||' + isNull([option].name3, '')) as optionName, [subitem].name1 as SubitemName, (category.name1 +'||'+ isnull(category.name2, '') + '||' + isnull(category.name3, '')) as CategoryName, (og.name1 + '||' + isnull(og.name2, '') + '||' + isnull(og.name3,'')) as CategoryOption,  category.categoryId , \
             ogo.optionGroupCode  FROM [transactionArchive] as t  \
            inner join [transactionDArchive] as td on  td.refNo = t.refNo  \
            Left join item as i on i.itemId = td.itemId \
            Left join [option] on  [option].[optionCode] = td.itemId    \
            Left join [staff] on staff.staffId = td.staffId \
            Left join [staff] as s2 on s2.staffId = td.approveStaffId    \
            Left join [subitem] on [subitem].subitemCode = td.itemId \
            Left join [itemCategory] on [itemCategory].itemId = td.itemId  \
            Left join category  on category.categoryId = [itemCategory].categoryId \
            Left join optionGroupOption as ogo on td.itemId = ogo.optionCode    \
            Left join optionGroup as og on ogo.optionGroupCode = og.optionGroupCode \
            where void = 0   ##conditionWhere2## \
            ) as [templ] ) as [result] order by refNo, round, seq, type, transTime, time",
           viewSql: " select * from (select * from  salesfood as t  ##conditionWhere1## union select * from salesArchivefood as t ##conditionWhere2## ) as tmpl order by refNo, round, seq, type",
           archiveDiscount: " SELECT [tableCode],[tableSubCode],[pax] ,[memberId] ,[memberName] ,[startTime] ,t.[refNo] ,[transTime] ,[payAmount] ,[price] ,[serviceCharge] ,[remainings],[reprint],[orderId] , (select SUM(discountValue) from [tenderDiscount] as tdd where refNo = t.refNo and tdd.void = 0) as discountValue   FROM [transaction] as t  where t.void = 0  ##conditionWhere1## union   SELECT [tableCode],[tableSubCode],[pax] ,[memberId] ,[memberName] ,[startTime] ,t.[refNo] ,[transTime] ,[payAmount] ,[price] ,[serviceCharge] ,[remainings],[reprint],[orderId] , (select SUM(discountValue) from [tenderDiscountArchive] as tdd where refNo = t.refNo and tdd.void = 0) as discountValue  FROM [transactionArchive] as t where t.void = 0  ##conditionWhere2## "
       };

/** For revise all options qty/name etc  **/
function Package(food) {
    this.PackageList = [];

    Package.prototype.GroupPackage = function (TransactionList, itemOptionGroupList) {
        var Global = this;
        TransactionList.forEach(function (item) {
            /** filter non category transaction */
            if (item.categoryId == null) {
                //item.categoryId = item.optionGroupCode == null ? null : item.optionGroupCode;
                //item.name1 = item.CategoryOption == null ? "Other" : item.CategoryOption;
            }



            if (item.categoryId == null && item.type != 'S' && item.type != 'O') { return true; }


            /*******************************************/


            if (item.type === "I" || Global.PackageList.length == 0) {
                Global.PackageList.push(Global.PackageItem(item));
            } else {
                var tempPackageItem = Global.PackageItem(item);

                var templ = itemOptionGroupList.filter(function (optionGroup) {
                    return optionGroup.itemCode == Global.PackageList[Global.PackageList.length - 1].itemId;
                });

                ///filter the option is which option group of item
                ///In sql, the option will be duplicated, this ensure the qty of option , if the option inside many option group
                if (templ.length != 0) {
                    if (tempPackageItem.transaction.optionGroupCode != null) {
                        if (templ.filter(function (optionGroupInTempl) {
                            return optionGroupInTempl.optionGroupCode == tempPackageItem.transaction.optionGroupCode
                        }).length == 0) return true;
                    }
                }
                tempPackageItem.qty = Global.PackageList[Global.PackageList.length - 1].qty * tempPackageItem.qty;
                tempPackageItem.totalPrice = tempPackageItem.qty * tempPackageItem.subPrice;
                Global.PackageList[Global.PackageList.length - 1].options.push(tempPackageItem)
            }

        })
        return Global.PackageList;
    }

    this.PackageItem = function (food) {

        var index = "0";
        switch (languageCode) {
            case "003":
                index = 1;
                break;
        }

        //console.log(134, food);

        var fieldList = ["name1", 'optionName', 'CategoryName', 'CategoryOption', 'SubitemName']
        for (var a = 0; a < fieldList.length; a++) {
            if (food[fieldList[a]]) {
                food[fieldList[a]] = food[fieldList[a]].split('||')[index];
            }
        }

        var Item = { transaction: food, qty: food.qty, subPrice: food.subPrice, totalPrice: (food.qty * food.subPrice), itemId: food.itemId, namek: food.name1, options: [], openKey: food.openkey, customName: food.customName }

        if (Item.namek == null) {
            if (food.optionName != null) Item.namek = food.optionName;
            else if (food.SubitemName != null) Item.namek = food.SubitemName;
            else Item.namek = Item.itemId;
        }

        return Item;
    }
}
/** For  group same food **/
function Food() {
    this.FoodList = [];
    this.FoodItemIdList = [];
    this.SaleTotalPrice = 0;

    Food.prototype.addFoodByCategory = function (item) {
        //if (item.qty < 0 || item.subPrice == 0) return;
        //if (item.subPrice == 0) return;
        //////var isExist = this.FoodItemIdList.indexOf(item.itemId);

        //////if (isExist == -1) {
        //////    var tempFood = {};
        //////    tempFood.Name = item.namek;
        //////    tempFood.TotalPrice = 0;
        //////    tempFood.TotalPrice += item.totalPrice;
        //////    tempFood.TotalQty = 0;
        //////    tempFood.TotalQty += item.qty;
        //////    //tempFood.CategoryId = item.transaction.categoryId == null ? item.transaction.optionGroupCode : item.transaction.categoryId.toString();
        //////    tempFood.CategoryId = item.transaction.categoryId == null ? item.transaction.optionGroupCode : item.transaction.categoryId.toString();
        //////    //tempFood.CategoryName = item.transaction.name1 == null ? item.transaction.CategoryOption : item.transaction.name1;
        //////    tempFood.CategoryName = item.transaction.CategoryName == null ? item.transaction.CategoryOption : item.transaction.CategoryName;
        //////    tempFood.SubPrice = item.subPrice;
        //////    tempFood.itemId = item.itemId;
        //////    tempFood.options = [];


        //////    this.FoodList.push(tempFood);
        //////    this.FoodItemIdList.push(item.itemId);

        //////}
        //////else {
        //////    this.FoodList[isExist].TotalPrice += item.totalPrice;
        //////    this.FoodList[isExist].TotalQty += item.qty;

        //////}
        if (item.openKey == 'o') {
            item.itemId = item.customName;
            item.namek = item.customName;
        }

        if (item.itemId == 'O9999') {
            item.namek = "未選擇菜單";
        }

        var templCategoryId = item.transaction.categoryId == null ? item.transaction.optionGroupCode : item.transaction.categoryId.toString();
        if (templCategoryId == null) templCategoryId = "other";
        var templ = this.FoodList.filter(function (food) {
            return food.CategoryId == templCategoryId && food.itemId == item.itemId;
        });

        var fieldName = "name1";
        if (languageCode == "003") {
            fieldName = "name2";
        }

        //console.log(209, item);

        if (templ.length == 0) {
            var tempFood = {};
            tempFood.Name = item.namek;
            tempFood.openKey = item.openKey;
            tempFood.customName = item.customName;
            tempFood.TotalPrice = 0;
            tempFood.TotalPrice += item.totalPrice;
            tempFood.TotalQty = 0;
            tempFood.TotalQty += item.qty;
            //tempFood.CategoryId = item.transaction.categoryId == null ? item.transaction.optionGroupCode : item.transaction.categoryId.toString();
            tempFood.CategoryId = templCategoryId;
            //tempFood.CategoryName = item.transaction.name1 == null ? item.transaction.CategoryOption : item.transaction.name1;
            tempFood.CategoryName = item.transaction.CategoryName == null ? item.transaction.CategoryOption : item.transaction.CategoryName;
            tempFood.SubPrice = item.subPrice;
            tempFood.itemId = item.itemId;
            tempFood.options = [];
            this.FoodList.push(tempFood);
        } else {
            templ[0].TotalPrice += item.totalPrice;
            templ[0].TotalQty += item.qty;

        }

    }
}
/** For group food by category **/
function Category() {
    this.CategoryList = [];
    this.CategoryIdList = [];
    this.AllCateQty = 0;
    this.AllCatePrice = 0;

    /**Pass all Category **/
    Category.prototype.GroupCategory = function (CateList, FoodList) {


        var Global = this;


        var fieldName = "name1";
        var otherName = "其他"
        var otherObj = { "categoryId": "other", "name1": otherName };
        if (languageCode == "003") {
            fieldName = "name2";
            otherName = "Other";
            otherObj.name2 = "Other";
        }
        CateList.push(otherObj)

        //CateList.push({ categoryId: "0", code: "0", name1: "Other" })

        CateList.forEach(function (cate) {
            var tempObject = {};
            tempObject.CateTransaction = {};
            tempObject.CateName = cate[fieldName];
            tempObject.PackageList = new Array;
            tempObject.CateQty = 0;
            tempObject.CatePrice = 0;
            //cate.categoryId = cate.categoryId.toString();
            tempObject.CateTransaction = cate;
            tempObject.Percentage = 0.00;
            tempObject.categoryId = cate.categoryId;
            Global.CategoryIdList.push(cate.categoryId.toString());
            Global.CategoryList.push(tempObject);

        });


        //console.log(this.CategoryIdList);
        FoodList.forEach(function (food) {

            var isExist = Global.CategoryIdList.indexOf(food.CategoryId);
            if (isExist != -1) {
                if (food.TotalQty == 0) return true;
                Global.CategoryList[isExist].PackageList.push(food);
                Global.CategoryList[isExist].CateQty += food.TotalQty;
                Global.CategoryList[isExist].CatePrice += food.TotalPrice;

                //try to group open key food
            }

        })


        //return false;

    }
}


function pad(n, width, z) {
    z = z || '0';
    n = n + '';
    return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

function extend(from, to) {
    if (from == null || typeof from != "object") return from;
    if (from.constructor != Object && from.constructor != Array) return from;
    if (from.constructor == Date || from.constructor == RegExp || from.constructor == Function ||
        from.constructor == String || from.constructor == Number || from.constructor == Boolean)
        return new from.constructor(from);

    to = to || new from.constructor();

    for (var name in from) {
        to[name] = typeof to[name] == "undefined" ? extend(from[name], null) : to[name];
    }

    return to;
}
//var date = moment().format("DD/MM/YYYY");

function getItemByItem(currentItem) {
    var items = [];

    //console.log("TEST", currentItem);
    if (currentItem.type == "I" || currentItem.type == "T") {
        items = _food.item.filter(function (entry) {
            return entry.code == currentItem.itemId;
        });
    } else if (currentItem.type == "M") {
        items = _food.modifiers.filter(function (entry) {
            return entry.code == currentItem.itemId;
        });
    } else if (currentItem.type == "S") {
        items = _food.subitem.filter(function (entry) {
            return entry.code == currentItem.itemId;
        });
    } else if (currentItem.type == "K") {
        items = _food.kitchenmsg.filter(function (entry) {
            return entry.code == currentItem.itemId;
        });
    } else if (currentItem.type == "O") {
        items = _food.option.filter(function (entry) {
            return entry.code == currentItem.itemId;
        });
        if (items.length == 0 && currentItem.itemId == 'O9999') {
            items[0] = { name: {} };

            items[0].name1 = '未選擇菜單';
            items[0].name2 = 'unselect Option';
            items[0].name3 = 'unselect Option';
            items[0].namek = 'unselect Option';
            items[0].printer = '';
        }
    }
    return items;
}


function sortDbItem(dbItems) {
    //dbItems.sort((a, b) => {
    //    if (a.seq != b.seq) { return a.seq - b.seq } else
    //        if (a.type == 'I') { return -1 } else
    //            if (a.type == 'O') { return -1 } else
    //                if (a.type == 'S') { return -1 } else
    //                    if (a.type == 'T') { return -1 } else
    //                        if (a.type == 'M') { return -1 } else
    //                            if (a.type == 'K') { return -1 } else
    //                            { return a.type < b.type }
    //});
    var map = ["I", "O", "S", "T", "M", "K"];

    dbItems.sort((a, b) => {
        if (a.seq < b.seq) return -1;
        if (a.seq === b.seq) {
            if (map.indexOf(a.type) < map.indexOf(b.type)) return -1;
            else if (map.indexOf(a.type) > map.indexOf(b.type)) return 1;
            else return moment(a.time).format('HHmmss') - moment(b.time).format('HHmmss');
        }
        if (a.seq > b.seq) return 1;
    });
}

function setData(data) {
    _food = data;
}

function dailyReport(args, mode, DM) {

    this.date = moment().format("DD/MM/YYYY");

    dailyReport.prototype.getQuery = function () {

        var query = [];

        var shopCode = args.shop;

        if (shopCode) {
            shopCode = shopCode + "-%";
        }
        else
            shopCode = "%"

        var argsdate = ""

        if (mode == "pos") argsdate = pad(args.day, 2) + "/" + pad(args.month, 2) + "/" + args.year;
        else argsdate = args.startDate;

        var self = this;

        function toYear() {
            var argsTodate = "";
            var sqlFromDates = "";
            var sqlToDates = "";
            if (mode == "pos") {
                argsTodate = pad(args.toDay, 2) + "/" + pad(args.toMonth, 2) + "/" + args.toYear;
                sqlFromDates = new Date(args.year, parseInt(args.month) - 1, args.day);
                sqlToDates = new Date(args.toYear, parseInt(args.toMonth) - 1, parseInt(args.toDay) + 1);

            } else {
                argsTodate = args.endDate;
                sqlFromDates = args.startDate;
                sqlToDates = args.endDate;
                sqlFromDates = moment(sqlFromDates, 'DD/MM/YYYY').toDate();
                sqlToDates = moment(sqlToDates, 'DD/MM/YYYY').add(1, 'day').toDate();

            }

            self.date = argsdate + ' - ' + argsTodate;

            query = [{ "sql": "select t.*, d.depositNo from (select * from tenderpaymentArchive where exists(select * from transactionArchive where transTime between @fromDate and @toDate and transactionArchive.refNo = tenderPaymentArchive.refNo and void = 0 and refNo like @shopCode) and void = 0  union  select * from tenderpayment where  exists (select * from [transaction] where transTime between @fromDate and @toDate and [transaction].refNo = tenderPayment.refNo and void = 0 and refNo like @shopCode) and void = 0 ) as t left join deposit as d on d.refNo = t.refNo and t.seq = 1 order by refNo, seq", "args": [{ "c": "fromDate", "t": TYPES.DateTime, "v": sqlFromDates }, { "c": "toDate", "t": TYPES.DateTime, "v": sqlToDates }, { "c": "shopCode", "t": TYPES.VarChar, "v": shopCode }] },
                    { "sql": "select * from tenderDiscountArchive where   exists(select * from transactionArchive where transTime between @fromDate and @toDate  and transactionArchive.refNo = tenderDiscountArchive.refNo and void = 0 and refNo like @shopCode) and void = 0    union  select * from tenderDiscount where exists (select * from [transaction] where transTime between @fromDate and @toDate  and [transaction].refNo = tenderDiscount.refNo and void = 0 and refNo like @shopCode )  and void = 0 ", "args": [{ "c": "fromDate", "t": TYPES.DateTime, "v": sqlFromDates }, { "c": "toDate", "t": TYPES.DateTime, "v": sqlToDates }, { "c": "shopCode", "t": TYPES.VarChar, "v": shopCode }] },
                    { "sql": "select [tableCode] ,[tableSubCode],[pax] ,[staffId] ,[memberId] ,[memberName] ,[startTime] ,[refNo] ,[transTime] ,[payAmount],[price],[serviceCharge],[remainings]    ,[reprint]   ,[void]    ,[voidStaff]    ,[orderId] from transactionArchive where  transTime between  @fromDate and @toDate and void = 0 and refNo like @shopCode union  select [tableCode] ,[tableSubCode],[pax] ,[staffId] ,[memberId] ,[memberName] ,[startTime] ,[refNo] ,[transTime] ,[payAmount],[price], [serviceCharge],[remainings]    ,[reprint]   ,[void]    ,[voidStaff]    ,[orderId] from [transaction] where  transTime between  @fromDate and @toDate and void = 0 and refNo like @shopCode", "args": [{ "c": "fromDate", "t": TYPES.DateTime, "v": sqlFromDates }, { "c": "toDate", "t": TYPES.DateTime, "v": sqlToDates }, { "c": "shopCode", "t": TYPES.VarChar, "v": shopCode }] },
                    { "sql": "select * from [tableOrder] ", "args": [] },
                    { "sql": "select * from [tender]", "args": [] },
                    { "sql": "select * from [deposit] where createDatetime between  @fromDate and @toDate and void = 0;" }
            ];



        }
        function today() {

            argsdate = self.date;
            query = [
                    { "sql": "select t.*, d.depositNo  from ( select * from tenderpayment where void = 0  and refNo in (select refNo from [transaction] where void = 0 and refNo like @shopCode) and void = 0 union  select * from tenderpaymentArchive where exists(select * from transactionArchive where convert(char(10), transTime, 103) = @date and transactionArchive.refNo = tenderpaymentArchive.refNo and void = 0 and refNo like @shopCode) and void = 0 ) as t left join deposit as d on d.refNo = t.refNo and t.seq = 1 order by refNo, seq", "args": [{ "c": "date", "t": TYPES.VarChar, "v": argsdate }, { "c": "shopCode", "t": TYPES.VarChar, "v": shopCode }] },
                    { "sql": "select * from tenderDiscount where void = 0 and refNo in (select refNo from [transaction] where void = 0 and refNo like @shopCode) and void = 0 union  select * from tenderDiscountArchive where exists(select * from transactionArchive where convert(char(10), transTime, 103 ) = @date and transactionArchive.refNo = tenderDiscountArchive.refNo and void = 0 and refNo like @shopCode) and void = 0 ", "args": [{ "c": "date", "t": TYPES.VarChar, "v": argsdate }, { "c": "shopCode", "t": TYPES.VarChar, "v": shopCode }] },
                    { "sql": "select [tableCode] ,[tableSubCode] ,[pax],[staffId],[memberId],[memberName],[startTime],[refNo],[transTime],[payAmount],[price],[serviceCharge],[remainings],[reprint],[void],[voidStaff] from [transaction] where void = 0 and refNo like @shopCode union  select [tableCode],[tableSubCode] ,[pax],[staffId],[memberId],[memberName],[startTime],[refNo],[transTime],[payAmount],[price],[serviceCharge],[remainings],[reprint],[void],[voidStaff] from transactionArchive where convert(char(10), transTime, 103 ) = @date and void = 0  and refNo like @shopCode  ", "args": [{ "c": "date", "t": TYPES.VarChar, "v": argsdate }, { "c": "shopCode", "t": TYPES.VarChar, "v": shopCode }] },
                    { "sql": "select * from [tableOrder]", "args": [] },
                    { "sql": "select * from [tender]", "args": [] },
                    { "sql": "select * from [deposit] where convert(char(10), createDatetime, 103) = @date and void = 0" }
                    //{ "sql": "select * from deposit where convert(char(10), createDatetime, 103)" }
            ];
        }
        function specificDay() {
            self.date = argsdate;
            query = [{ "sql": "select t.*, d.depositNo from (select * from tenderpaymentArchive where exists(select * from transactionArchive where convert(char(10), transTime, 103) = @date and transactionArchive.refNo = tenderPaymentArchive.refNo and void = 0 and refNo like @shopCode) and void = 0   union   select * from tenderpayment where  exists (select * from [transaction] where convert(char(10), transTime, 103) = @date and [transaction].refNo = tenderPayment.refNo and refNo like @shopCode and void = 0) and void = 0 ) as t left join deposit as d on d.refNo = t.refNo and t.seq = 1 order by refNo, seq", "args": [{ "c": "date", "t": TYPES.VarChar, "v": argsdate }, { "c": "shopCode", "t": TYPES.VarChar, "v": shopCode }] },
                { "sql": "select * from tenderDiscountArchive where   exists(select * from transactionArchive where convert(char(10), transTime, 103) = @date and transactionArchive.refNo = tenderDiscountArchive.refNo and void = 0 and refNo like @shopCode) and void = 0  union  select * from tenderDiscount where exists (select * from [transaction] where convert(char(10), transTime, 103) = @date  and [transaction].refNo = tenderDiscount.refNo and refNo like @shopCode and void = 0 ) and void = 0", "args": [{ "c": "date", "t": TYPES.VarChar, "v": argsdate }, { "c": "shopCode", "t": TYPES.VarChar, "v": shopCode }] },
                { "sql": "select [tableCode] ,[tableSubCode] ,[pax],[staffId],[memberId],[memberName],[startTime],[refNo],[transTime],[payAmount],[price],[serviceCharge],[remainings],[reprint] ,[void],[voidStaff] from transactionArchive where convert(char(10), transTime, 103 ) = @date  and void = 0 and refNo like @shopCode union  select [tableCode] ,[tableSubCode] ,[pax],[staffId],[memberId],[memberName],[startTime],[refNo],[transTime],[payAmount],[price],[serviceCharge],[remainings],[reprint] ,[void],[voidStaff] from [transaction] where convert(char(10), transTime, 103) = @date and void = 0 and refNo like @shopCode ", "args": [{ "c": "date", "t": TYPES.VarChar, "v": argsdate }, { "c": "shopCode", "t": TYPES.VarChar, "v": shopCode }] },
                { "sql": "select * from [tableOrder]", "args": [] },
                { "sql": "select * from [tender]", "args": [] },
                { "sql": "select * from [deposit] where convert(char(10), createDatetime, 103) = @date and void = 0" }
            ];
        }

        switch (mode) {
            case "pos": {
                if (args.year) {
                    if (args.toYear) {
                        toYear();
                    }
                    else {
                        if (self.date == argsdate) {
                            today();
                        }
                        else {
                            specificDay();
                        }
                    }
                } else {
                    today();
                }

                break;
            }
            case "cloud": {
                if (args.startDate == args.endDate) args.endDate = null;

                if (args.startDate) {

                    if (args.endDate) {                    //args.toYear
                        toYear();
                    }
                    else {

                        if (self.date == argsdate) {
                            today();
                        }
                        else {
                            specificDay();
                        }
                    }
                }
                break;
            }
        }

        if (args.till) {
            if (typeof args.till == 'string') {
                query[0].sql = "select t.*, d.depositNo from (select * from tenderPayment where void = 0 and refNo in (select refNo from [transaction] where till = '" + args.till + "' and void = 0)) as t left join deposit as d on d.refNo = t.refNo and t.seq = 1 order by refNo, seq";
                query[1].sql = "select * from tenderDiscount where void = 0 and refNo in (select refNo from [transaction] where till = '" + args.till + "' and void = 0)";
                query[2].sql = "select * from [transaction] where void = 0 and till = '" + args.till + "'";
            }
            else if (typeof args.till == 'object') {
                query[0].sql = "select t.*, d.depositNo from (select * from tenderPayment where void = 0 and refNo in (select refNo from [transaction] where till in (" + "'" + args.till.join("','") + "'" + ") and void = 0)) as t left join deposit as d on d.refNo = t.refNo and t.seq = 1 order by refNo, seq";

                query[1].sql = "select * from tenderDiscount where void = 0 and refNo in (select refNo from [transaction] where till in (" + "'" + args.till.join("','") + "'" + ") and void = 0)";
                query[2].sql = "select * from [transaction] where void = 0 and till in (" + "'" + args.till.join("','") + "'" + ")";
            }
        }

        if (_food.config != undefined && _food.config.dailyReport != undefined) {
            if (_food.config.dailyReport.timeslot) {
                query.push({ sql: "select * from timeslot order by timeStart;" })
                this.indexOfTimeslot = query.length - 1;
            }
        }

        //console.log(query);
        return query;
    }

    dailyReport.prototype.getResult = function (result) {
        var tender = _food.tender;

        var date = this.date;
        var self = this;
        var obj = {};

        function pos() {
            var title = [];

            switch (languageCode) {
                case "003":
                    title['all'] = "Daily Business Report";
                    title['eat'] = "Daily Dine In Report";
                    title['takeaway'] = "Daily Takeaway Report";
                    title['delivery'] = "Daily Delivery Report";
                    break;
                default: {
                    title['all'] = "每日營業報表";
                    title['eat'] = "每日堂食報表";
                    title['takeaway'] = "每日外賣報表";
                    title['delivery'] = "每日外送報表";
                    break;
                }
            }

            var transaction = result[2];
            var discount = result[1];
            var payment = result[0];
            var tableOrder = result[3];
            var pettyCashList = result[4];
            var depositList = result[5];


            switch (args.dailyReportType) {
                case 'eat':
                    payment = payment.filter(function (item) {
                        return item.refNo.indexOf('-TO-') == -1 && item.refNo.indexOf('-D-') == -1;
                    });

                    transaction = transaction.filter(function (item) {
                        return item.refNo.indexOf('-TO-') == -1 && item.refNo.indexOf('-D-') == -1;;
                    });

                    discount = discount.filter(function (item) {
                        return item.refNo.indexOf('-TO-') == -1 && item.refNo.indexOf('-D-') == -1;;
                    });
                    break;
                case 'takeaway':
                    payment = payment.filter(function (item) {
                        return item.refNo.indexOf('-TO-') != -1;
                    });

                    transaction = transaction.filter(function (item) {
                        return item.refNo.indexOf('-TO-') != -1;
                    });

                    discount = discount.filter(function (item) {
                        return item.refNo.indexOf('-TO-') != -1;
                    });

                    tableOrder = [];
                    break;
                case 'delivery':
                    //console.log('delivery');
                    payment = payment.filter(function (item) {
                        return item.refNo.indexOf('-D-') != -1;
                    });

                    transaction = transaction.filter(function (item) {
                        return item.refNo.indexOf('-D-') != -1;
                    });

                    discount = discount.filter(function (item) {
                        return item.refNo.indexOf('-D-') != -1;
                    });

                    tableOrder = [];
                    break;
            }

            obj = self.getDailyReportObj(transaction, payment, discount, tableOrder, tender, title[args.dailyReportType] || title['all'], result, depositList);

            if (args.dailyReportType == "all") {
                pettyCashList.forEach(function (item) {
                    obj.pettyCash -= item.cash;
                })
            }

            obj.REPORT_DATE = this.date;
        }

        function cloud() {

            var transaction = result[2];
            var discount = result[1];
            var payment = result[0];
            var tableOrder = result[3];
            var tender = result[4];

            var dateList = [];
            var dateObjList = [];

            if (args.excelExport) {

                transaction.forEach(function (t) {
                    var tmpDate = moment(t.transTime).format('YYYY-MM-DD');
                    if (dateList.indexOf(tmpDate) == -1) {
                        var tmpDateObj = {};
                        tmpDateObj.date = tmpDate;
                        tmpDateObj.transaction = [];
                        tmpDateObj.refNoList = [];
                        tmpDateObj.refNoList.push(t.refNo);
                        tmpDateObj.transaction.push(t);
                        tmpDateObj.discount = [];
                        tmpDateObj.payment = [];
                        dateList.push(tmpDate);
                        dateObjList.push(tmpDateObj)

                    } else {
                        var targetIndex = dateList.indexOf(tmpDate);
                        dateObjList[targetIndex].transaction.push(t);
                        dateObjList[targetIndex].refNoList.push(t.refNo);
                    }
                });

                dateObjList.forEach(function (d) {
                    d.payment = payment.filter(function (p) {
                        return d.refNoList.indexOf(p.refNo) != -1;
                    })

                    d.discount = discount.filter(function (dis) {
                        return d.refNoList.indexOf(dis.refNo) != -1;
                    })

                    d.resultObj = {};
                    d.resultObj = self.getDailyReportObj(d.transaction, d.payment, d.discount, null, tender, "");
                    d.resultObj.DECIMAIL_POINT = d.resultObj.DECIMAIL_POINT.toFixed(2)
                    d.resultObj.TOTAL_CARD_TIPS = parseFloat(d.resultObj.TOTAL_CARD_TIPS).toFixed(2);

                    delete d.transaction;
                    delete d.payment;
                    delete d.discount;
                    delete d.refNoList;
                })

                dateObjList.sort(function (a, b) {
                    return parseInt(moment(a.date).format('YYYYMMDD')) - parseInt(moment(b.date).format('YYYYMMDD'));
                });

                //callback(dateObjList);
                obj = dateObjList;

                //console.log(530, obj);
            }
            else {

                obj = self.getDailyReportObj(transaction, payment, discount, tableOrder, tender, "每日營業報表");
                obj.REPORT_DATE = date;

                //callback(obj);
            }
        }

        if (mode == 'pos') pos();
        else cloud();

        obj.REPORT_DATE = this.date;

        return obj;
    }

    dailyReport.prototype.getDailyReportObj = function (transaction, payment, discount, tableOrder, tender, title, result, deposit) {

        var obj = { title: title, GROSS_TOTAL_AMOUNT: 0, DISCOUNT: 0, NO_OF_DISCOUNT: 0, SERVICE_CHARGE: 0, GROSS_SALES_NET: 0, DECIMAIL_POINT: 0, TOTAL_CARD_TIPS: 0, NO_OF_CARD_TIPS: 0, TOTAL_NO_OF_CARD: 0, TOTAL_CARD_AMOUNT: 0, NONCASHSALES: [], CASHSALES: [], CARDTIPS: [], DISCOUNTDETAILS: [], NO_OF_INVOICE: 0, GROSS_SALES_NET_PER_INVOICE: 0, TOTAL_PAX: 0, TOTAL_PAX_PER_AMOUNT: 0, TABLEORDER: [], TOTAL_SALES_AMOUNT: 0, ENTERTAINMENT: [], REPORT_DATE: '', TOTAL_CASH_AMOUNT: 0, TOTAL_CASH_COUNT: 0, pettyCash: 0, CARDS: [], tenderPayment: [], depositTender: [], depositTips: 0, newDepositTender: [] };


        tender.forEach(function (t) {
            obj.tenderPayment[t.tenderCode] = [];
        })

        var fieldName = "tenderName1";
        switch (languageCode) {
            case "003":
                fieldName = "tenderName3";
                break;
        }

        if (deposit)
            deposit.forEach(function (d) {

                var target = obj.newDepositTender.filter(function (e) {
                    return d.tenderCode == e.paymentType;
                })

                if (target.length == 0) {
                    var tenderObj = tender.filter((t) => { return t.tenderCode == d.tenderCode })[0];

                    obj.newDepositTender.push({
                        paymentName: tenderObj[fieldName],
                        paymentType: d.tenderCode,
                        price: d.price,
                        qty: 1
                    })

                }
                else {
                    target[0].qty++;
                    target[0].price += d.price;
                }

            })

        if (tableOrder) {
            for (var i = 0; i < tableOrder.length; i++) {
                var to = tableOrder[i];
                if (i == 0) {
                    obj.TABLEORDER.push({ "pax": to.pax, "price": to.price + to.serviceCharge, "count": 1 })
                } else {
                    obj.TABLEORDER[0].pax += to.pax;
                    obj.TABLEORDER[0].price += to.price + to.serviceCharge;
                    obj.TABLEORDER[0].count += 1;
                }
            }
        }

        if (this.indexOfTimeslot != undefined) {

            var timeslot = [];

            timeslot = result[this.indexOfTimeslot];

            timeslot.forEach(function (t) {
                var tmpStart = moment(t.timeStart).toDate();
                t.start = tmpStart.toLocaleTimeString({ hour12: false }).split(':').join("");
                var tmpEnd = moment(t.timeEnd).toDate();
                t.end = tmpEnd.toLocaleTimeString({ hour12: false }).split(':').join("");
                t.pax = 0;
                t.turnover = 0;
                t.dineInPrice = 0;
                t.takeawayPrice = 0;
                t.avgPrice = 0;
            })

        }

        function checkTimeslot(trans, totalPrice) {

            var transTime = trans.startTime.toLocaleTimeString({ hour12: false }).split(':').join("")

            timeslot.forEach(function (t) {
                if (parseInt(transTime) >= parseInt(t.start) && parseInt(transTime) <= parseInt(t.end)) {
                    t.pax += trans.pax;
                    t.turnover += totalPrice;

                    if (trans.refNo.indexOf('-TO-') != -1) t.takeawayPrice += totalPrice
                    else t.dineInPrice += totalPrice

                }
            })
        }

        if (transaction) {

            for (var i = 0; i < transaction.length; i++) {
                var t = transaction[i];
                obj.TOTAL_PAX += t.pax;
                obj.GROSS_TOTAL_AMOUNT += t.price;
                obj.SERVICE_CHARGE += t.serviceCharge;
                obj.DECIMAIL_POINT += t.remainings;
                var totalPrice = t.price + t.serviceCharge + t.remainings;



                var dis = discount.filter(function (dis) {
                    return dis.refNo == t.refNo;
                })
                for (var j = 0; j < dis.length; j++) {
                    totalPrice -= dis[j].discountValue;
                }

                if (this.indexOfTimeslot != undefined) {
                    checkTimeslot(t, totalPrice);
                }

                var po = payment.filter(function (po) {
                    return po.refNo == t.refNo;
                })

                var pricefornoncash = totalPrice


                if (deposit && deposit.length != 0)
                for (var j = 0; j < po.length; j++) {

                    if (j == 0 && po[j].depositNo != null) {

                        if (po.length == 1) {
                            if ((po[j].paymentValue - totalPrice) != 0) {
                                obj.depositTips += (po[j].paymentValue - totalPrice);
                                //po[j].paymentValue = totalPrice;
                            }
                        }

                        var target = obj.depositTender.filter(function (d) {
                            return d.paymentType == po[j].paymentType && d.till == po[j].till;
                        })



                        if (target.length != 0) {
                            target[0].price += po[j].paymentValue;
                            target[0].qty++;
                        }
                        else {

                            var tenderObj = tender.filter((t) => { return t.tenderCode == po[j].paymentType })[0];

                            obj.depositTender.push({
                                paymentName: tenderObj[fieldName],
                                paymentType: po[j].paymentType,
                                price: po[j].paymentValue,
                                till: po[j].till,
                                qty: 1
                            })

                        }

                        pricefornoncash -= po[j].paymentValue;
                    }
                }

                //console.log('po', po);
                for (var j = 0; j < po.length; j++) {
                    var p = po[j];
                    if (deposit && deposit.length != 0 && p.depositNo != null) continue;
                    if (p.paymentType == 'cash') {

                        //console.log(p)
                        //console.log('pricefornoncash:', pricefornoncash);
                        var cashSales = pricefornoncash;

                        if (p.priceValue) {
                            pricefornoncash -= p.priceValue;
                            cashSales = p.priceValue;
                        } else {

                            if (pricefornoncash < p.paymentValue) {
                                pricefornoncash = 0;
                            } else {
                                cashSales = p.paymentValue;
                                pricefornoncash -= p.paymentValue;
                            }

                        }


                        var foundTill = false;

                        obj.CASHSALES.every((cs) => {
                            //console.log(cs.till === p.till);
                            //console.log(cs.till);
                            //console.log(p.till);
                            if (cs.till === p.till) {
                                cs.paymentValue += cashSales;
                                cs.count += 1;
                                foundTill = true;
                                return false;
                            }
                            return true;
                        })
                        if (!foundTill) {
                            obj.CASHSALES.push({ paymentValue: cashSales, count: 1, till: p.till });
                        }

                        obj.TOTAL_CASH_COUNT += 1;
                        obj.TOTAL_CASH_AMOUNT += cashSales;

                        obj.tenderPayment[p.paymentType].push({ refNo: p.refNo, paymentValue: cashSales });
                        //obj.CASHSALES.count += 1;
                        //console.log("price", obj.CASHSALES.paymentValue);
                        //break;?
                    }
                }

                for (var j = 0; j < po.length; j++) {
                    var p = po[j];
                    if (deposit && deposit.length != 0 && p.depositNo != null) continue;
                    var tenderObj = tender.filter((t) => { return t.tenderCode == p.paymentType })[0];
                    if (tenderObj == undefined) break;
                    if (p.paymentType != 'cash') {
                        var nonCashPayment = 0;
                        var remainingPrice = pricefornoncash;
                        if (p.priceValue && tenderObj.isCard) {
                            remainingPrice = p.priceValue;
                        }

                        if (tenderObj.isCard) {
                            obj.TOTAL_NO_OF_CARD += 1;
                        }




                        if (p.paymentValue > remainingPrice) {
                            nonCashPayment = remainingPrice;
                            //if (p.paymentType != 'octopus' && p.paymentType != 'entertainment' && p.paymentType != 'on_acct') {
                            if (tenderObj.isCard) {
                                //obj.CARDS..push({ paymentType: p.paymentType, tipsValue: cardtips, count: 1, till: p.till })
                                obj.TOTAL_CARD_AMOUNT += remainingPrice;

                                var cardtips = p.paymentValue - remainingPrice;

                                obj.tenderPayment[p.paymentType].push({ refNo: p.refNo, paymentValue: remainingPrice, tips: cardtips });


                                var foundMatchCardPayment = false;
                                for (var k = 0; k < obj.CARDS.length; k++) {
                                    if (p.paymentType === obj.CARDS[k].paymentType) {
                                        foundMatchCardPayment = true;
                                        obj.CARDS[k].paymentValue += p.paymentValue;

                                        if (cardtips != p.paymentValue) {
                                            obj.CARDS[k].count += 1;
                                        }
                                    }
                                }
                                if (!foundMatchCardPayment) {
                                    obj.CARDS.push({ paymentType: p.paymentType, paymentValue: p.paymentValue, count: 1, paymentName: tenderObj[fieldName] })
                                }


                                obj.TOTAL_CARD_TIPS += cardtips;
                                obj.NO_OF_CARD_TIPS += 1;
                                var foundMatchCardTipsPayment = false;
                                for (var k = 0; k < obj.CARDTIPS.length; k++) {
                                    if (p.paymentType === obj.CARDTIPS[k].paymentType && p.till === obj.CARDTIPS[k].till) {
                                        obj.CARDTIPS[k].tipsValue += cardtips
                                        obj.CARDTIPS[k].count += 1;
                                        foundMatchCardTipsPayment = true;
                                        break;
                                    }
                                }
                                if (!foundMatchCardTipsPayment) {
                                    var tenderObj = tender.filter((t) => { return t.tenderCode == p.paymentType })[0];
                                    obj.CARDTIPS.push({ paymentType: p.paymentType, tipsValue: cardtips, count: 1, till: p.till, paymentName: tenderObj[fieldName] })
                                }
                            }
                            remainingPrice = 0;
                        }
                        else {
                            //if (p.paymentType != 'octopus' && p.paymentType != 'entertainment' && p.paymentType != 'on_acct') {
                            var foundMatchCardPayment = false;
                            for (var k = 0; k < obj.CARDS.length; k++) {
                                if (p.paymentType === obj.CARDS[k].paymentType) {
                                    foundMatchCardPayment = true;
                                    obj.CARDS[k].paymentValue += p.paymentValue;

                                    if (cardtips != p.paymentValue) {
                                        obj.CARDS[k].count += 1;
                                    }
                                }
                            }
                            if (!foundMatchCardPayment) {
                                obj.CARDS.push({ paymentType: p.paymentType, paymentValue: p.paymentValue, count: 1, paymentName: tenderObj[fieldName] })
                            }

                            if (tenderObj.isCard) {
                                obj.TOTAL_CARD_AMOUNT += p.paymentValue;
                                //obj.tenderPayment[p.paymentType].push({ refNo: p.refNo, paymentValue: p.paymentValue, tips: 0 });
                            }
                            nonCashPayment = p.paymentValue;
                            pricefornoncash -= p.paymentValue
                        }

                        //obj.tenderPayment[p.paymentType].push({ refNo: p.refNo, paymentValue: nonCashPayment, tips: 0 });
                        var foundMatchPaymentType = false;
                        for (var k = 0; k < obj.NONCASHSALES.length; k++) {
                            if (obj.NONCASHSALES[k].paymentType === p.paymentType && p.till === obj.NONCASHSALES[k].till) {
                                if (p.paymentType == "cashCoupon") {
                                    obj.NONCASHSALES[k].paymentValue += p.paymentValue;
                                    obj.TOTAL_CASH_AMOUNT -= p.paymentValue;
                                    obj.TOTAL_CASH_AMOUNT += nonCashPayment;
                                    var tillCash = obj.CASHSALES.filter(function (t) { return t.till == p.till; });
                                    if (tillCash.length != 0) {
                                        tillCash[0].paymentValue -= p.paymentValue;
                                        tillCash[0].paymentValue += nonCashPayment;
                                    }

                                }
                                else {
                                    //obj.NONCASHSALES[k].paymentValue += p.paymentValue;
                                    obj.NONCASHSALES[k].paymentValue += nonCashPayment;
                                }
                                obj.NONCASHSALES[k].count += 1;
                                foundMatchPaymentType = true;
                            }
                        }
                        if (!foundMatchPaymentType) {
                            var tenderObj = tender.filter((t) => { return t.tenderCode == p.paymentType })[0];
                            obj.NONCASHSALES.push({ "paymentType": p.paymentType, "paymentValue": p.paymentType == "cashCoupon" ? p.paymentValue : nonCashPayment, "count": 1, "till": p.till, "paymentName": tenderObj[fieldName] })
                            if (p.paymentType == "cashCoupon") {
                                obj.TOTAL_CASH_AMOUNT -= p.paymentValue;
                                obj.TOTAL_CASH_AMOUNT += nonCashPayment;
                                var tillCash = obj.CASHSALES.filter(function (t) { return t.till == p.till; });
                                if (tillCash.length != 0) {
                                    tillCash[0].paymentValue -= p.paymentValue;
                                    tillCash[0].paymentValue += nonCashPayment;
                                }
                            }
                        }

                        // console.log(obj.NONCASHSALES)
                    }
                }
            }
        }

        for (var i = 0; i < obj.NONCASHSALES.length; i++) {

            var tenderObj = tender.filter((t) => { return t.tenderCode == obj.NONCASHSALES[i].paymentType })[0];
            //if (obj.NONCASHSALES[i].paymentType == "entertainment") {
            if (tenderObj.isEnt) {
                obj.ENTERTAINMENT.push({ count: obj.NONCASHSALES[i].count, paymentValue: obj.NONCASHSALES[i].paymentValue, paymentType: tenderObj.tenderName1 })
            }

        }

        //console.log(obj.TOTAL_CARD_AMOUNT);
        for (var i = 0; i < discount.length; i++) {
            var d = discount[i];
            //console.log(2145, d);

            obj.DISCOUNT += d.discountValue;

            //console.log(2149, obj.DISCOUNT);
            var foundMatchDiscountDetails = false;
            for (var k = 0; k < obj.DISCOUNTDETAILS.length; k++) {
                if (obj.DISCOUNTDETAILS[k].discountCode == d.discountCode) {
                    obj.DISCOUNTDETAILS[k].discountValue += d.discountValue;
                    obj.DISCOUNTDETAILS[k].count += 1;
                    foundMatchDiscountDetails = true;
                    break;
                }
            }

            if (!foundMatchDiscountDetails) {
                var disName = "";
                var tmp = _food.discount.filter(function (t) {
                    return t.discountCode == d.discountCode;
                })
                if (tmp.length > 0) disName = tmp[0]["discountName1"];
                obj.DISCOUNTDETAILS.push({ discountCode: d.discountCode, discountValue: d.discountValue, count: 1, name: disName })
            }
        }
        obj.GROSS_SALES_NET = obj.GROSS_TOTAL_AMOUNT + obj.SERVICE_CHARGE + obj.DECIMAIL_POINT - obj.DISCOUNT
        if (obj.ENTERTAINMENT.length != 0) {

            obj.ENTERTAINMENT.forEach(function (e) {
                obj.GROSS_SALES_NET -= e.paymentValue;
            })
            //obj.GROSS_SALES_NET -= obj.ENTERTAINMENT[0].paymentValue;
        }

        //obj.tender.

        //var cardPayment = payment.filter((p) => {
        //    //return p.paymentType != 'cash' && p.paymentType != 'octopus' && p.paymentType != 'entertainment' 
        //    return this.tender.filter((t) => {
        //        return t.tenderCode == p.paymentType && t.isCard;
        //    }).length > 0;
        //});

        //pending
        //for (var i = 0; i < obj.CARDS.length; i++) {
        //    obj.TOTAL_NO_OF_CARD += obj.CARDS[i].count;
        //    for (var j = 0; j < obj.NONCASHSALES.length; j++) {
        //        obj.NONCASHSALES[j].paymentType == 
        //        var tenderObj = this.tender.filter((t) => { return t.tenderCode == obj.NONCASHSALES[i].paymentType })[0];
        //        //if (obj.NONCASHSALES[i].paymentType == "entertainment") {
        //        if (tenderObj.isEnt) {
        //            obj.ENTERTAINMENT.push({ count: obj.NONCASHSALES[i].count, paymentValue: obj.NONCASHSALES[i].paymentValue, paymentType: tenderObj.tenderName1 })
        //        }

        //    }

        //}





        //console.log(cardPayment);
        obj.NO_OF_DISCOUNT = discount.length;
        //obj.TOTAL_NO_OF_CARD = cardPayment.length;
        obj.NO_OF_INVOICE = transaction.length;
        obj.GROSS_SALES_NET_PER_INVOICE = obj.GROSS_SALES_NET / obj.NO_OF_INVOICE;
        obj.TOTAL_PAX_PER_AMOUNT = obj.GROSS_SALES_NET / obj.TOTAL_PAX;
        obj.TOTAL_SALES_AMOUNT = obj.GROSS_SALES_NET;

        obj.GROSS_SALES_NET += obj.depositTips;

        if (obj.TABLEORDER.length != 0) {
            obj.TOTAL_SALES_AMOUNT += obj.TABLEORDER[0].price;
        }
        //if (obj.TOTAL_NO_OF_CARD != 0) {
        //    obj.TOTAL_CARD_AMOUNT = cardPayment.reduce((sum, val) => { return sum + val.paymentValue },0)
        //}
        //obj.TOTAL_CARD_TIPS = obj.TOTAL_CARD_TIPS.format(1, 3);
        //obj.GROSS_SALES_NET = obj.GROSS_SALES_NET.format(1, 3);
        //obj.GROSS_TOTAL_AMOUNT = obj.GROSS_TOTAL_AMOUNT.format(1, 3);

        if (this.indexOfTimeslot != undefined) {
            obj.timeslot = timeslot;
            obj.timeslot.forEach(function (t) {
                t.avgPrice = t.dineInPrice / t.pax;
                if (t.pax == 0) t.avgPrice = 0;
            })
        }

        //obj.TOTAL_CARD_TIPS = parseFloat(obj.TOTAL_CARD_TIPS).toFixed(2);

        //console.log(obj);
        return obj;

    }

    dailyReport.prototype.getCallbackResult = function (callback) {
        var self = this;
        var DB = new database();
        DB.executeStatement(self.getQuery(), (err, result) => {
            var result = self.getResult(result)
            result.voidBillObj = {};
            var exec = [];
            if (_food.config.dailyReport.voidBill) {
                exec.push((cb) => {
                    self.dailyVoidBillReport = new dailyVoidBillReport(args, mode, DM);
                    DB.executeStatement(self.dailyVoidBillReport.getQuery(), (err, voidBill) => {
                        var voidBillObj = self.dailyVoidBillReport.getResult(voidBill)
                        result.voidBillObj = voidBillObj
                        cb(null);
                    });
                });
            }

            /*result.discountObj = {};
            if (_food.config.dailyReport.show100percent) {
                exec.push((cb) => {
                    self.dailyDiscountReport = new dailyDiscountReport(args, mode, DM);
                    DM.executeStatement(self.dailyDiscountReport.getQuery(), (err, discount) => {
                        var discountObj = self.dailyDiscountReport.getResult(discount)
                        result.discountObj = discountObj
                        cb(null);
                    });
                });
            }*/

            async.waterfall(exec, function () {
                callback(result);
            });


            if (args.isPrint) {
                self.printReport(result);
            }
        }, 'parallel')

    }

    dailyReport.prototype.printReport = function (obj) {
        var printer = args.printer ? args.printer : DefaultPrinter;
        obj.printer = printer;

        var _obj = {};
        extend(obj, _obj);
        _obj.TOTAL_CASH_AMOUNT = S("$" + _obj.TOTAL_CASH_AMOUNT.format(1, 3)).padLeft(10).s;
        _obj.GROSS_TOTAL_AMOUNT = S("$" + _obj.GROSS_TOTAL_AMOUNT.format(1, 3)).padLeft(10).s;
        _obj.DISCOUNT = S("$" + _obj.DISCOUNT.format(1, 3)).padLeft(10).s;
        //_obj.NO_OF_DISCOUNT = S("$" + _obj.NO_OF_DISCOUNT.format(1, 3)).padLeft(10).s;
        _obj.SERVICE_CHARGE = S("$" + _obj.SERVICE_CHARGE.format(1, 3)).padLeft(10).s;
        _obj.GROSS_SALES_NET = S("$" + _obj.GROSS_SALES_NET.format(1, 3)).padLeft(10).s;
        _obj.DECIMAIL_POINT = S("$" + _obj.DECIMAIL_POINT.format(1, 3)).padLeft(10).s;
        _obj.TOTAL_CARD_TIPS = S("$" + _obj.TOTAL_CARD_TIPS.format(1, 3)).padLeft(10).s;
        //_obj.NO_OF_CARD_TIPS = S("$" + _obj.NO_OF_CARD_TIPS.format(1, 3)).padLeft(10).s;
        //_obj.TOTAL_NO_OF_CARD = S("$" + _obj.TOTAL_NO_OF_CARD.format(1, 3)).padLeft(10).s;
        _obj.TOTAL_CARD_AMOUNT = S("$" + _obj.TOTAL_CARD_AMOUNT.format(1, 3)).padLeft(10).s;

        _obj.depositTips = S("$" + _obj.depositTips.format(1, 3)).padLeft(10).s;


        for (var i = 0; i < _obj.NONCASHSALES.length; i++) {
            _obj.NONCASHSALES[i].paymentValue = S("$" + _obj.NONCASHSALES[i].paymentValue.format(1, 3)).padLeft(10).s;
        }

        for (var i = 0; i < _obj.CASHSALES.length; i++) {
            _obj.CASHSALES[i].paymentValue = S("$" + _obj.CASHSALES[i].paymentValue.format(1, 3)).padLeft(10).s;
        }
        //_obj.CASHSALES.paymentValue = S("$" + _obj.CASHSALES.paymentValue.format(1, 3)).padLeft(10).s;

        for (var i = 0; i < _obj.CARDTIPS.length; i++) {
            _obj.CARDTIPS[i].tipsValue = S("$" + _obj.CARDTIPS[i].tipsValue.format(1, 3)).padLeft(10).s;
        }

        for (var i = 0; i < _obj.CARDS.length; i++) {
            _obj.CARDS[i].paymentValue = S("$" + _obj.CARDS[i].paymentValue.format(1, 3)).padLeft(10).s;
        }

        for (var i = 0; i < _obj.depositTender.length; i++) {
            _obj.depositTender[i].price = S("$" + _obj.depositTender[i].price.format(1, 3)).padLeft(10).s;
        }

        for (var i = 0; i < _obj.newDepositTender.length; i++) {
            _obj.newDepositTender[i].price = S("$" + _obj.newDepositTender[i].price.format(1, 3)).padLeft(10).s;
        }

        for (var i = 0; i < _obj.DISCOUNTDETAILS.length; i++) {
            _obj.DISCOUNTDETAILS[i].discountValue = S("$" + _obj.DISCOUNTDETAILS[i].discountValue.format(1, 3)).padLeft(10).s;
        }
        _obj.GROSS_SALES_NET_PER_INVOICE = S("$" + _obj.GROSS_SALES_NET_PER_INVOICE.format(1, 3)).padLeft(10).s;
        _obj.TOTAL_PAX_PER_AMOUNT = S("$" + _obj.TOTAL_PAX_PER_AMOUNT.format(1, 3)).padLeft(10).s;


        for (var i = 0; i < _obj.TABLEORDER.length; i++) {
            _obj.TABLEORDER[i].price = S("$" + _obj.TABLEORDER[i].price.format(1, 3)).padLeft(10).s;
        }

        _obj.TOTAL_SALES_AMOUNT = S("$" + _obj.TOTAL_SALES_AMOUNT.format(1, 3)).padLeft(10).s;



        if (_obj.ENTERTAINMENT.length != 0) {
            _obj.ENTERTAINMENT[0].paymentValue = S("$" + _obj.ENTERTAINMENT[0].paymentValue.format(1, 3)).padLeft(10).s;
        }
        //_obj.CASHSALESPAYMENTVALUE = _obj.CASHSALES.paymentValue;
        //_obj.CASHSALESPAYMENTCOUNT = _obj.CASHSALES.count;

        _obj.timeslot = []

        if (this.indexOfTimeslot != undefined) {
            for (var i = 0; i < obj.timeslot.length; i++) {
                var name = obj.timeslot[i].timeStart.toLocaleTimeString({ hour12: false }) + " - " + obj.timeslot[i].timeEnd.toLocaleTimeString({ hour12: false }) + "[" + obj.timeslot[i].name + "]"

                _obj.timeslot.push({ displayName: name, name: obj.timeslot[i].name, pax: obj.timeslot[i].pax, totalPrice: S("$" + obj.timeslot[i].turnover.format(1, 3)).padLeft(10).s, dineInPrice: S("$" + obj.timeslot[i].dineInPrice.format(1, 3)).padLeft(10).s, takeawayPrice: S("$" + obj.timeslot[i].takeawayPrice.format(1, 3)).padLeft(10).s, avgPrice: S("$" + obj.timeslot[i].avgPrice.format(1, 3)).padLeft(10).s });
            }
        }

        //console.log(_obj);
        fs.writeFile("./printer/" + obj.printer + "/dailyReport_" + moment().format("YYMMDDHHmmss") + ".json", JSON.stringify(_obj), function (err) {
            if (err) {
                //console.log(err);
            } else {
                ////console.log("The file was saved!");
            }
        });

    }
    //callback && callback("test");
}

function dailyAttendanceReport(args, mode, DM) {

    //console.log(1185, args);
    this.date = moment().format("DD/MM/YYYY");

    dailyAttendanceReport.prototype.getQuery = function () {

        var query = [];

        var shopCode = args.shop;
        if (shopCode == null) shopCode = "-1";
        var extrCondition = "";
        //console.log(args);
        if (shopCode != -1) {
            extrCondition = " and shopId = (select shopId from shop where code=@shopId) ";
        }

        var staffName = args.userName;

        if (staffName && staffName != "") {
            extrCondition += " and staffName like @staffName";
            staffName = "%" + staffName + "%"
        }

        var argsdate = ""

        if (mode == "pos") argsdate = pad(args.day, 2) + "/" + pad(args.month, 2) + "/" + args.year;
        else argsdate = args.startDate;

        var self = this;

        function toYear() {

            var argsTodate = "";
            var sqlFromDates = "";
            var sqlToDates = "";

            if (mode == "pos") {
                argsTodate = pad(args.toDay, 2) + "/" + pad(args.toMonth, 2) + "/" + args.toYear;
                sqlFromDates = new Date(args.year, parseInt(args.month) - 1, args.day);
                sqlToDates = new Date(args.toYear, parseInt(args.toMonth) - 1, parseInt(args.toDay) + 1);
            } else {
                argsTodate = args.endDate;
                sqlFromDates = args.startDate;
                sqlToDates = args.endDate;
                sqlFromDates = moment(sqlFromDates, 'DD/MM/YYYY').toDate();
                sqlToDates = moment(sqlToDates, 'DD/MM/YYYY').add(1, 'day').toDate();
            }

            self.date = argsdate + ' - ' + argsTodate;

            query = [{ "sql": "select * from staffAtt where datetime between @fromDate and @toDate " + extrCondition + " order by datetime , staffusername, type", "args": [{ "c": "fromDate", "t": TYPES.DateTime, "v": sqlFromDates }, { "c": "toDate", "t": TYPES.DateTime, "v": sqlToDates }, { "c": "shopId", "t": TYPES.VarChar, "v": shopCode }, { "c": "staffName", "t": TYPES.NVarChar, "v": staffName }] }];
        }

        function today() {

            argsdate = self.date;
            query = [{ "sql": "select * from staffAtt where convert(char(10), datetime, 103) = @date " + extrCondition + " order by datetime , staffusername, type ", "args": [{ "c": "date", "t": TYPES.VarChar, "v": argsdate }, { "c": "shopId", "t": TYPES.VarChar, "v": shopCode }, { "c": "staffName", "t": TYPES.NVarChar, "v": staffName }] }];


        }

        function specificDay() {
            self.date = argsdate;
            query = [{ "sql": "select * from staffAtt where convert(char(10), datetime, 103) = @date " + extrCondition + " order by datetime , staffusername, type ", "args": [{ "c": "date", "t": TYPES.VarChar, "v": argsdate }, { "c": "shopId", "t": TYPES.VarChar, "v": shopCode }, { "c": "staffName", "t": TYPES.NVarChar, "v": staffName }] }];
        }

        switch (mode) {
            case "pos": {
                if (args.year) {
                    if (args.toYear) {
                        toYear();
                    } else {
                        if (self.date == argsdate) {
                            today();
                        } else {
                            specificDay();
                        }
                    }
                } else {
                    today();
                }

                break;
            }
            case "cloud": {
                if (args.startDate == args.endDate) args.endDate = null;
                if (args.startDate) {
                    var argsdate = args.startDate;

                    if (args.endDate) {
                        toYear();
                    }
                    else {
                        if (self.date == argsdate) {
                            today();
                        } else {
                            specificDay();
                        }
                    }
                }
                break;
            }
        }

        return query;
    }

    dailyAttendanceReport.prototype.getResult = function (result) {

        var obj = {};
        obj.REPORT_DATE = this.date;
        obj.attendances = [];
        obj.attendances = result[0];
        if (mode == "pos") {
            obj.attendances.forEach(function (attendance) {
                attendance.datetime = moment(attendance.datetime).format("DD/MM/YYYY HH:mm:ss");
            });

        }
        else {

            if (!_food.config.dailyAttendanceReport) _food.config.dailyAttendanceReport = {}

            if (!_food.config.dailyAttendanceReport.isWorkingHour && args.isExcel) {
                obj.attendances.forEach(function (attendance) {
                    var tmp = moment(attendance.datetime);
                    attendance.date = tmp.format("YYYYMMDD");
                    attendance.time = tmp.format("HHmm");
                    attendance.datetime = moment(attendance.datetime).format("DD/MM/YYYY HH:mm:ss");
                });
            } else {

                if (_food.config.dailyAttendanceReport.workingHoursPeriod == 'long') {
                    var DutyResult = [];
                    obj.attendances.forEach(function (attendance, index) {
                        var dutyRecord = {
                            staffNo: attendance.staffUsername,
                            staffName: attendance.staffName,
                            datetime: moment(attendance.datetime).format("DD/MM/YYYY HH:mm:ss"),
                            date: moment(attendance.datetime).format("DD/MM/YYYY"),
                            status: attendance.type,
                            shop: attendance.shopId,
                            index: index,
                            image: attendance.image
                        };

                        var dutyDate = {
                            staffNo: dutyRecord.staffNo,
                            staffName: dutyRecord.staffName,
                            date: dutyRecord.date,
                            dutyRecord: [dutyRecord]
                        }

                        var templ = DutyResult.filter(function (duty) {
                            return duty.staffNo == dutyRecord.staffNo && duty.date == dutyRecord.date;
                        });



                        if (templ.length == 0) {
                            DutyResult.push(dutyDate);
                        } else {
                            templ[0].dutyRecord.push(dutyRecord);
                        }
                    })

                    obj.attendances = [];

                    DutyResult.forEach(function (duty) {
                        var reportResult = {
                            staffUsername: duty.staffNo,
                            staffName: duty.staffName,
                            inDatetime: null,
                            outDatetime: null,
                            index: "",
                            image: {
                                "inImage": null,
                                "outImage": null
                            }
                        };

                        duty.dutyRecord.forEach(function (record, $index) {
                            if (record.status == "in") {
                                if (reportResult.inDatetime == null) {
                                    reportResult.inDatetime = record.datetime;
                                    reportResult.image.inImage = record.image;
                                }
                                else {
                                    var target = moment(reportResult.inDatetime, 'DD/MM/YYYY HH:mm:ss');
                                    var source = moment(record.datetime, 'DD/MM/YYYY HH:mm:ss');
                                    if (source < target) {
                                        reportResult.image.inImage = record.image;
                                    }

                                }
                            }
                            else if (record.status == "out") {
                                if (reportResult.outDatetime == null) {
                                    reportResult.outDatetime = record.datetime;
                                    reportResult.image.outImage = record.image;
                                }
                                else {
                                    var target = moment(reportResult.outDatetime, 'DD/MM/YYYY HH:mm:ss');
                                    var source = moment(record.datetime, 'DD/MM/YYYY HH:mm:ss');
                                    if (source > target) {
                                        reportResult.outDatetime = record.datetime;
                                        reportResult.image.outImage = record.image;
                                    }
                                }
                            }

                            if ($index == duty.dutyRecord.length - 1)
                                obj.attendances.push(reportResult);
                        });
                    })
                } else {
                    var DutyResult = [];
                    obj.attendances.forEach(function (attendance, index) {

                        var dutyRecord = {
                            staffNo: attendance.staffUsername,
                            staffName: attendance.staffName,
                            datetime: moment(attendance.datetime).format("DD/MM/YYYY HH:mm:ss"),
                            date: moment(attendance.datetime).format("DD/MM/YYYY"),
                            status: attendance.type,
                            shop: attendance.shopId,
                            index: index
                        };

                        var dutyDate = {
                            staffNo: dutyRecord.staffNo,
                            staffName: dutyRecord.staffName,
                            date: dutyRecord.date,
                            dutyRecord: [dutyRecord]
                        }

                        var templ = DutyResult.filter(function (duty) {
                            return duty.staffNo == dutyRecord.staffNo && duty.date == dutyRecord.date;
                        });



                        if (templ.length == 0) {
                            DutyResult.push(dutyDate);
                        } else {
                            templ[0].dutyRecord.push(dutyRecord);
                        }

                    });
                    obj.attendances = [];

                    DutyResult.forEach(function (duty) {

                        var reportResult = {
                            staffUsername: duty.staffNo,
                            staffName: duty.staffName,
                            inDatetime: null,
                            outDatetime: null,
                            index: ""
                        };

                        var checkStatus = -1; // 1= check next record status is out | 0 = check next record status is in 

                        duty.dutyRecord.forEach(function (record, $index) {

                            if (checkStatus == -1) {
                                reportResult = {
                                    staffUsername: duty.staffNo,
                                    staffName: duty.staffName,
                                    inDatetime: null,
                                    outDatetime: null,
                                    index: ""
                                };
                                var attr = record.status + "Datetime";
                                reportResult[attr] = record.datetime;
                                reportResult.index += " " + record.index + " ";
                                checkStatus = record.status == "out" ? 0 : 1;
                                if ($index == duty.dutyRecord.length - 1) obj.attendances.push(reportResult);
                            }
                            else if (checkStatus == 0) {

                                if (record.status != "in") {
                                    var tempInsert = {};
                                    extend(reportResult, tempInsert);
                                    obj.attendances.push(tempInsert);
                                    reportResult = {
                                        staffUsername: duty.staffNo,
                                        staffName: duty.staffName,
                                        inDatetime: null,
                                        outDatetime: null,
                                        index: "",
                                    };
                                    var attr = record.status + "Datetime";
                                    reportResult[attr] = record.datetime
                                    checkStatus = record.status == "out" ? 0 : 1;
                                    reportResult.index += " " + record.index + " ";
                                    if ($index == duty.dutyRecord.length - 1) obj.attendances.push(reportResult);
                                }
                                else {
                                    var attr = record.status + "Datetime";
                                    reportResult[attr] = record.datetime;
                                    reportResult.index += " " + record.index + " ";
                                    var tempInsert = {};
                                    extend(reportResult, tempInsert);
                                    obj.attendances.push(tempInsert);
                                    checkStatus = -1;
                                }
                            }
                            else if (checkStatus == 1) {
                                if (record.status != "out") {
                                    var tempInsert = {};
                                    extend(reportResult, tempInsert);
                                    //if(tempInsert.inDatetime.inDatetime != null && tempInsert.outDatetime != null)
                                    obj.attendances.push(tempInsert);
                                    reportResult = {
                                        staffUsername: duty.staffNo,
                                        staffName: duty.staffName,
                                        inDatetime: null,
                                        outDatetime: null,
                                        index: "",
                                    };
                                    var attr = record.status + "Datetime";
                                    reportResult[attr] = record.datetime
                                    checkStatus = record.status == "out" ? 0 : 1;
                                    reportResult.index += " " + record.index + " ";
                                    if ($index == duty.dutyRecord.length - 1) obj.attendances.push(reportResult);
                                } else {
                                    var attr = record.status + "Datetime";
                                    reportResult[attr] = record.datetime;
                                    reportResult.index += " " + record.index + " ";
                                    var tempInsert = {};
                                    extend(reportResult, tempInsert);
                                    obj.attendances.push(tempInsert);
                                    checkStatus = -1;
                                }
                            }


                        })

                    });
                }

                obj.attendances.forEach(function (a) {
                    a.workingHours = null;
                    if (a.inDatetime == null || a.outDatetime == null) {
                        a.workingHours = "00:00:00";
                        return true;
                    }
                    a.workingHours = moment(a.outDatetime, 'DD/MM/YYYY HH:mm:ss').diff(moment(a.inDatetime, 'DD/MM/YYYY HH:mm:ss'));
                    a.workingHours = moment.utc(a.workingHours).format("HH:mm:ss");
                })

                obj.attendances.sort(function (a, b) {
                    if (a.staffName < b.staffName) return 1;
                    else if (a.staffName > b.staffName) return -1;
                    else {
                        if ((a.inDatetime != null && a.inDatetime != "") && (b.inDatetime != null && b.inDatetime != ""))
                            return moment(a.inDatetime, 'DD/MM/YYYY HH:mm:ss').format('YYYYMMDDHHmmss') - moment(b.inDatetime, 'DD/MM/YYYY HH:mm:ss').format('YYYYMMDDHHmmss');
                        else
                            return moment(a.outDatetime, 'DD/MM/YYYY HH:mm:ss').format('YYYYMMDDHHmmss') - moment(b.outDatetime, 'DD/MM/YYYY HH:mm:ss').format('YYYYMMDDHHmmss');
                    }
                })

            }


        }

        return obj;
    }

    dailyAttendanceReport.prototype.getCallbackResult = function (callback) {
        var self = this;
        var DB = new database();
        DB.executeStatement(self.getQuery(), (err, result) => {
            callback(self.getResult(result));
        })
    }

}

function dailyVoidBillReport(args, mode, DM) {

    this.date = moment().format("DD/MM/YYYY");

    dailyVoidBillReport.prototype.getQuery = function () {

        var shopCode = args.shop;

        if (shopCode != null) {

            // shopCode = "%" + shopCode + "-%";
            shopCode = shopCode + "-%";
        }
        else
            shopCode = "%"

        var query = [
            {
                "sql": "select * from (SELECT t.[tableCode], [tableSubCode],[pax] ,t.[staffId] ,[memberId] ,[memberName],[startTime] ,t.[refNo]  ,[transTime] ,[payAmount]   , CASE WHEN td.discountValue is not null then t.price - discountValue  else t.price end as price ,[serviceCharge] ,[remainings] ,[reprint] ,t.[void] ,t.[voidStaff],[orderId], s.name  FROM [transaction] as t  left join [staff] as s on s.username = t.voidStaff left join [tenderDiscount] as td on td.refNo = t.refNo where t.void = 1 ##conditionWhere1##   union   SELECT t.[tableCode], [tableSubCode],[pax] ,t.[staffId] ,[memberId] ,[memberName],[startTime] ,t.[refNo]  ,[transTime] ,[payAmount]   , CASE WHEN td.discountValue is not null then t.price - discountValue  else t.price end as price ,[serviceCharge]     ,[remainings] ,[reprint] ,t.[void] ,t.[voidStaff]  ,[orderId], s.name FROM [transactionArchive] as t   left join [staff] as s on s.username = t.voidStaff left join [tenderDiscountArchive] as td on td.refNo = t.refNo  where t.void = 1 ##conditionWhere2## ) as temp order by transTime",
                "args": []
            }
        ];

        var argsdate = ""

        if (mode == "pos") argsdate = pad(args.day, 2) + "/" + pad(args.month, 2) + "/" + args.year;
        else argsdate = args.startDate;

        var self = this;

        function toYear() {
            var argsTodate = "";
            var sqlFromDates = "";
            var sqlToDates = "";

            if (mode == "pos") {
                argsTodate = pad(args.toDay, 2) + "/" + pad(args.toMonth, 2) + "/" + args.toYear;
                sqlFromDates = new Date(args.year, parseInt(args.month) - 1, args.day);
                sqlToDates = new Date(args.toYear, parseInt(args.toMonth) - 1, parseInt(args.toDay) + 1);
            } else {
                argsTodate = args.endDate;
                sqlFromDates = args.startDate;
                sqlToDates = args.endDate;
                sqlFromDates = moment(sqlFromDates, 'DD/MM/YYYY').toDate();
                sqlToDates = moment(sqlToDates, 'DD/MM/YYYY').add(1, 'day').toDate();
            }

            self.date = argsdate + ' - ' + argsTodate;

            query[0].sql = query[0].sql.replace("##conditionWhere1##", " and transTime between @fromDate and @toDate and t.refNo like @shopCode ")
            query[0].sql = query[0].sql.replace("##conditionWhere2##", " and transTime between @fromDate and @toDate and t.refNo like @shopCode ")
            query[0].args = [];

            query[0].args.push({ "c": "fromDate", "t": TYPES.DateTime, "v": sqlFromDates }, { "c": "toDate", "t": TYPES.DateTime, "v": sqlToDates });
            query[0].args.push({ "c": "shopCode", "t": TYPES.VarChar, "v": shopCode });

        }
        function today() {
            argsdate = self.date;
            query[0].sql = query[0].sql.replace("##conditionWhere1##", " and t.refNo like @shopCode  ")
            query[0].sql = query[0].sql.replace("##conditionWhere2##", " and  convert(varchar(10), transTime, 103) = @date and t.refNo like @shopCode  ")
            query[0].args = [];
            query[0].args.push({ "c": "date", "t": TYPES.VarChar, "v": argsdate });
            query[0].args.push({ "c": "shopCode", "t": TYPES.VarChar, "v": shopCode });
        }
        function specificDay() {

            self.date = argsdate;

            query[0].sql = query[0].sql.replace("##conditionWhere1##", " and  convert(varchar(10), transTime, 103) = @date and t.refNo like @shopCode     ")
            query[0].sql = query[0].sql.replace("##conditionWhere2##", " and  convert(varchar(10), transTime, 103) = @date and t.refNo like @shopCode    ")
            query[0].args = [];
            query[0].args.push({ "c": "date", "t": TYPES.VarChar, "v": argsdate });
            query[0].args.push({ "c": "shopCode", "t": TYPES.VarChar, "v": shopCode });

        }


        switch (mode) {
            case "pos": {
                if (args.year) {
                    if (args.toYear) {
                        toYear();
                    } else {
                        if (self.date == argsdate) {
                            today();
                        } else {
                            specificDay();
                        }
                    }
                } else {
                    today();
                }

                break;
            }
            case "cloud": {
                if (args.startDate == args.endDate) args.endDate = null;
                if (args.startDate) {
                    var argsdate = args.startDate;

                    if (args.endDate) {
                        toYear();
                    }
                    else {
                        if (self.date == argsdate) {
                            today();
                        } else {
                            specificDay();
                        }
                    }
                }
                break;
            }
        }

        return query;
    }

    dailyVoidBillReport.prototype.getResult = function (result) {
        var obj = {}
        obj.REPORT_DATE = this.date;
        obj.voidBillList = result[0];
        obj.AllQty = result[0].length;
        obj.AllPrice = 0;
        result[0].forEach(function (e) {
            obj.AllPrice += e.price;
        })
        return obj;
    }

    dailyVoidBillReport.prototype.getCallbackResult = function (callback) {
        var self = this;
        var DB = new database();
        DB.executeStatement(self.getQuery(), (err, result) => {
            var obj = self.getResult(result)
            callback(obj);
            if (args.isPrint) {
                self.printReport(obj)
            }
        })
    }

    dailyVoidBillReport.prototype.printReport = function (obj) {

        var printer = args.printer ? args.printer : DefaultPrinter;
        obj.printer = printer;

        obj.voidBillList.forEach(function (bill) {
            bill.payAmount = S("$" + bill.payAmount.format(1, 3)).padLeft(10).s;
            bill.price = S("$" + bill.price.format(1, 3)).padLeft(10).s;
            bill.serviceCharge = S("$" + bill.serviceCharge.format(1, 3)).padLeft(10).s;
            bill.remainings = S("$" + bill.remainings.format(1, 3)).padLeft(10).s;
        });

        obj.AllPrice = S("$" + obj.AllPrice.format(1, 3)).padLeft(10).s;

        //console.log('args.isPrint', obj.printer);
        fs.writeFile("./printer/" + obj.printer + "/voidBillReport_" + moment().format("YYMMDDHHmmss") + ".json", JSON.stringify(obj), function (err) {
            if (err) {
                //console.log(err);
            } else {
                ////console.log("The file was saved!");
            }
        });
    }
}

function dailyPaymentDetailReport(args, mode, DM) {

    this.date = moment().format("DD/MM/YYYY");
    this.dailyReport = new dailyReport(args, mode, DM);

    dailyPaymentDetailReport.prototype.getQuery = function () {
        var query = this.dailyReport.getQuery();
        if (_food.config.dailyReport.timeslot) delete this.dailyReport.indexOfTimeslot; // not need in this report

        return query;
    }

    dailyPaymentDetailReport.prototype.getResult = function (result) {
        var tender = _food.tender
        var transaction = result[2];
        var discount = result[1];
        var payment = result[0];
        var deposit = result[5];

        var obj = this.dailyReport.getDailyReportObj(transaction, payment, discount, null, tender, "", result);
        obj.REPORT_DATE = this.dailyReport.date;
        obj.PaymentList = [];
        var fieldName = "tenderName1";
        switch (languageCode) {
            case "003":
                fieldName = "tenderName3";
                break;
        }

        obj.AllPrice = 0;
        tender.forEach(function (t) {
            //console.log(t);
            var target = obj.tenderPayment[t.tenderCode];
            if (target.length == 0) return true;
            var tmpl = {}
            tmpl.tenderName = t[fieldName];
            tmpl.records = [];
            tmpl.totalAmount = 0;
            tmpl.qty = 0;
            if (t.tenderCode != 'cash') {
                tmpl.tips = 0;
            }
            for (var i = 0; i < target.length; i++) {
                tmpl.qty += 1;
                tmpl.totalAmount += target[i].paymentValue;
                obj.AllPrice += target[i].paymentValue;
                tmpl.records.push(target[i]);
                if (t.tenderCode != 'cash') {
                    tmpl.tips += target[i].tips;
                }
            }

            obj.PaymentList.push(tmpl);
        })

        //console.log(obj);
        return obj;
    }

    dailyPaymentDetailReport.prototype.getCallbackResult = function (callback) {
        var self = this;
        var DB = new database();
        DB.executeStatement(self.getQuery(), (err, result) => {
            callback(self.getResult(result));
        }, 'parallel')

    }


}

function dailyDiscountReport(args, mode, DM) {

    this.date = moment().format("DD/MM/YYYY");

    dailyDiscountReport.prototype.getQuery = function () {
        var shopCode = args.shop;

        if (shopCode != null) {

            // shopCode = "%" + shopCode + "-%";
            shopCode = shopCode + "-%";
        }
        else
            shopCode = "%"

        var query = [{ sql: "select * from (SELECT td.[discountType] ,td.[discountCode] ,[discountValue]  ,td.[staffId]  ,td.[refNo]   ,[discountDate] ,td.[seq], td.[void]  ,td.[voidStaff] ,td.[discountAmount]  ,[voidDate] , [discount].discountName1, [discount].discountName2, [discount].discountName3  FROM [tenderDiscount] as td  Left join [discount] on discount.discountCode = td.discountCode  inner join [transaction] as t on t.refNo = td.refNo and t.void = 0 where td.void = 0 ##conditionWhere1##  union  SELECT td.[discountType] ,td.[discountCode] ,[discountValue]  , td.[staffId]  , td.[refNo]  ,[discountDate] ,td.[seq], td.[void]  ,td.[voidStaff] ,td.[discountAmount] , [voidDate] , [discount].discountName1 , [discount].discountName2, [discount].discountName3  FROM [tenderDiscountArchive] as td  Left join [discount] on discount.discountCode = td.discountCode inner join [transactionArchive] as t on t.refNo = td.refNo and t.void = 0 where td.void = 0 ##conditionWhere2##  ) as templ order by discountDate", args: [] }, { sql: "Select * from discount where discountType != 'item' and discountType != 'itemPer';", args: [] }]

        var argsdate = ""

        if (mode == "pos") argsdate = pad(args.day, 2) + "/" + pad(args.month, 2) + "/" + args.year;
        else argsdate = args.startDate;

        var self = this;

        function toYear() {
            var argsTodate = "";
            var sqlFromDates = "";
            var sqlToDates = "";

            if (mode == "pos") {
                argsTodate = pad(args.toDay, 2) + "/" + pad(args.toMonth, 2) + "/" + args.toYear;
                sqlFromDates = new Date(args.year, parseInt(args.month) - 1, args.day);
                sqlToDates = new Date(args.toYear, parseInt(args.toMonth) - 1, parseInt(args.toDay) + 1);
            } else {
                argsTodate = args.endDate;
                sqlFromDates = args.startDate;
                sqlToDates = args.endDate;
                sqlFromDates = moment(sqlFromDates, 'DD/MM/YYYY').toDate();
                sqlToDates = moment(sqlToDates, 'DD/MM/YYYY').add(1, 'day').toDate();
            }

            self.date = argsdate + ' - ' + argsTodate;

            query[0].sql = query[0].sql.replace("##conditionWhere1##", " and discountDate between @fromDate and @toDate  and t.refNo like @shopCode  ")
            query[0].sql = query[0].sql.replace("##conditionWhere2##", " and discountDate between @fromDate and @toDate  and t.refNo like @shopCode  ")

            query[0].args = [];

            query[0].args.push({ "c": "fromDate", "t": TYPES.DateTime, "v": sqlFromDates }, { "c": "toDate", "t": TYPES.DateTime, "v": sqlToDates });
            query[0].args.push({ "c": "shopCode", "t": TYPES.VarChar, "v": shopCode });

        }
        function today() {
            argsdate = self.date;
            query[0].sql = query[0].sql.replace("##conditionWhere1##", "  and t.refNo like @shopCode   ")
            query[0].sql = query[0].sql.replace("##conditionWhere2##", " and  convert(varchar(10), discountDate, 103) = @date  and t.refNo like @shopCode    ")
            query[0].args = [];
            query[0].args.push({ "c": "date", "t": TYPES.VarChar, "v": argsdate });
            query[0].args.push({ "c": "shopCode", "t": TYPES.VarChar, "v": shopCode });
        }
        function specificDay() {

            self.date = argsdate;

            query[0].sql = query[0].sql.replace("##conditionWhere1##", " and  convert(varchar(10), discountDate, 103) = @date  and t.refNo like @shopCode   ")
            query[0].sql = query[0].sql.replace("##conditionWhere2##", " and  convert(varchar(10), discountDate, 103) = @date  and t.refNo like @shopCode   ")

            query[0].args = [];
            query[0].args.push({ "c": "date", "t": TYPES.VarChar, "v": argsdate });
            query[0].args.push({ "c": "shopCode", "t": TYPES.VarChar, "v": shopCode });
        }


        switch (mode) {
            case "pos": {
                if (args.year) {
                    if (args.toYear) {
                        toYear();
                    } else {
                        if (self.date == argsdate) {
                            today();
                        } else {
                            specificDay();
                        }
                    }
                } else {
                    today();
                }

                break;
            }
            case "cloud": {
                if (args.startDate == args.endDate) args.endDate = null;
                if (args.startDate) {
                    var argsdate = args.startDate;

                    if (args.endDate) {
                        toYear();
                    }
                    else {
                        if (self.date == argsdate) {
                            today();
                        } else {
                            specificDay();
                        }
                    }
                }
                break;
            }
        }

        return query;
    }

    dailyDiscountReport.prototype.getResult = function (result) {



        var discountList = result[1];
        var discountRecord = result[0];

        var ResultList = [];
        var DiscountCodeList = [];

        var obj = { "DiscountList": new Array, "REPORT_DATE": moment().format("DD/MM/YYYY"), "AllPrice": 0.0, "AllQty": 0 }

        obj.REPORT_DATE = this.date;

        discountList.forEach(function (discount) {

            //record.discountName1 = record.discountName1.split('||')[index];

            var tempDiscount = {};
            tempDiscount.TransactionDiscount = discount;
            tempDiscount.Records = [];
            tempDiscount.AllQty = 0;
            tempDiscount.AllPrice = 0.0;
            DiscountCodeList.push(discount.discountCode);
            ResultList.push(tempDiscount);
        })

        discountRecord.forEach(function (record) {

            var isExist = DiscountCodeList.indexOf(record.discountCode);
            if (isExist != -1) {
                ResultList[isExist].Records.push(record);
                ResultList[isExist].AllQty += 1;
                ResultList[isExist].AllPrice += record.discountValue;
                obj.AllPrice += record.discountValue;
                obj.AllQty += 1;
            }
        })

        obj.DiscountList = ResultList;

        return obj
    }

    dailyDiscountReport.prototype.getCallbackResult = function (callback) {
        var self = this;
        var DB = new database();
        DB.executeStatement(self.getQuery(), (err, result) => {
            callback(self.getResult(result));
        })
    }
}

function dailyPerHourSalesReport(args, mode, DM) {

    this.date = moment().format("DD/MM/YYYY");

    dailyPerHourSalesReport.prototype.getQuery = function () {

        var shopCode = args.shop;

        if (shopCode != null) {

            // shopCode = "%" + shopCode + "-%";
            shopCode = shopCode + "-%";
        }
        else
            shopCode = "%"

        var query = [{ "sql": "select * from (SELECT [tableCode],[tableSubCode],[pax] ,[memberId] ,[memberName] ,[startTime] ,t.[refNo] ,[transTime] ,[payAmount] ,[price],[serviceCharge] ,[remainings],[reprint],[orderId] ,(select SUM(discountValue) from [tenderDiscount] as tdd where refNo = t.refNo and t.void = 0) as discountValue , convert(char(8), [startTime], 114) as time  FROM [transaction] as t where t.void = 0  ##conditionWhere1##  union    SELECT [tableCode],[tableSubCode],[pax] ,[memberId] ,[memberName] ,[startTime] ,t.[refNo] ,[transTime] ,[payAmount] ,[price],    [serviceCharge] ,[remainings],[reprint],[orderId] ,(select SUM(discountValue) from [tenderDiscountArchive] as tdd where refNo = t.refNo and t.void = 0) as discountValue , convert(char(8), [startTime], 114) as time  FROM [transactionArchive] as t  where t.void = 0  ##conditionWhere2## ) as temp  order by startTime, refNo;", "args": [] }];

        var argsdate = ""

        if (mode == "pos") argsdate = pad(args.day, 2) + "/" + pad(args.month, 2) + "/" + args.year;
        else argsdate = args.startDate;

        var self = this;

        function toYear() {
            var argsTodate = "";
            var sqlFromDates = "";
            var sqlToDates = "";

            if (mode == "pos") {
                argsTodate = pad(args.toDay, 2) + "/" + pad(args.toMonth, 2) + "/" + args.toYear;
                sqlFromDates = new Date(args.year, parseInt(args.month) - 1, args.day);
                sqlToDates = new Date(args.toYear, parseInt(args.toMonth) - 1, parseInt(args.toDay) + 1);
            } else {
                argsTodate = args.endDate;
                sqlFromDates = args.startDate;
                sqlToDates = args.endDate;
                sqlFromDates = moment(sqlFromDates, 'DD/MM/YYYY').toDate();
                sqlToDates = moment(sqlToDates, 'DD/MM/YYYY').add(1, 'day').toDate();
            }

            self.date = argsdate + ' - ' + argsTodate;

            query[0].sql = query[0].sql.replace("##conditionWhere1##", " and transTime between @fromDate and @toDate and t.refNo like @shopCode ")
            query[0].sql = query[0].sql.replace("##conditionWhere2##", " and transTime between @fromDate and @toDate and t.refNo like @shopCode ")

            query[0].args = [];

            query[0].args.push({ "c": "fromDate", "t": TYPES.DateTime, "v": sqlFromDates }, { "c": "toDate", "t": TYPES.DateTime, "v": sqlToDates });
            query[0].args.push({ "c": "shopCode", "t": TYPES.VarChar, "v": shopCode });

        }
        function today() {
            argsdate = self.date;
            query[0].sql = query[0].sql.replace("##conditionWhere1##", " and t.refNo like @shopCode  ")
            query[0].sql = query[0].sql.replace("##conditionWhere2##", " and  convert(varchar(10), transTime, 103) = @date and t.refNo like @shopCode  ")
            query[0].args = [];
            query[0].args.push({ "c": "date", "t": TYPES.VarChar, "v": argsdate });
            query[0].args.push({ "c": "shopCode", "t": TYPES.VarChar, "v": shopCode });
        }
        function specificDay() {

            self.date = argsdate;

            query[0].sql = query[0].sql.replace("##conditionWhere1##", " and  convert(varchar(10), transTime, 103) = @date and t.refNo like @shopCode  ")
            query[0].sql = query[0].sql.replace("##conditionWhere2##", " and  convert(varchar(10), transTime, 103) = @date and t.refNo like @shopCode  ")

            query[0].args = [];
            query[0].args.push({ "c": "date", "t": TYPES.VarChar, "v": argsdate });
            query[0].args.push({ "c": "shopCode", "t": TYPES.VarChar, "v": shopCode });
        }


        switch (mode) {
            case "pos": {
                if (args.year) {
                    if (args.toYear) {
                        toYear();
                    } else {
                        if (self.date == argsdate) {
                            today();
                        } else {
                            specificDay();
                        }
                    }
                } else {
                    today();
                }

                break;
            }
            case "cloud": {
                if (args.startDate == args.endDate) args.endDate = null;
                if (args.startDate) {
                    var argsdate = args.startDate;

                    if (args.endDate) {
                        toYear();
                    }
                    else {
                        if (self.date == argsdate) {
                            today();
                        } else {
                            specificDay();
                        }
                    }
                }
                break;
            }
        }

        return query;
    }
    dailyPerHourSalesReport.prototype.getResult = function (result) {

        var obj = { "Timeslot": [], "REPORT_DATE": moment().format("DD/MM/YYYY"), "AllPax": 0, "AllPrice": 0.0, "AllQty": 0 };

        obj.REPORT_DATE = this.date;

        var TimeslotList = [];
        for (var i = 0; i < 24 ; i++) {
            var temp = {};
            temp.startTime = i <= 9 ? "0" + i + ":00:00" : i + ":00:00";
            temp.Display = (i <= 9 ? "0" + i : i) + "-" + ((i + 1) <= 9 ? "0" + (i + 1) : (i + 1));
            temp.endTime = (i + 1) <= 9 ? "0" + (i + 1) + ":00:00" : (i + 1) + ":00:00";
            if (i == 23) {
                temp.endTime = "00:00:00";
                temp.Display = (i <= 9 ? "0" + i : i) + "-" + "00";
            }
            temp.CompareStartTime = parseInt(temp.startTime.replace(new RegExp(':', 'g'), ''));
            temp.CompareEndTime = parseInt(temp.endTime.replace(new RegExp(':', 'g'), ''));
            temp.AllOrderQty = 0;
            temp.AllPax = 0;
            temp.AllPrice = 0.0;
            temp.Percentage = 0.0;
            TimeslotList.push(temp);
        }



        TimeslotList.forEach(function (timeslot) {
            var transaction = result[0];
            transaction = transaction.filter(function (item) {
                var startTime = item.time.replace(new RegExp(':', 'g'), '');
                startTime = parseInt(startTime);
                return startTime >= timeslot.CompareStartTime && startTime <= timeslot.CompareEndTime;
            })

            transaction.forEach(function (item) {

                timeslot.AllOrderQty += 1;
                obj.AllQty += 1

                timeslot.AllPax += item.pax;
                obj.AllPax += item.pax;

                timeslot.AllPrice += item.price + item.remainings + item.serviceCharge;
                obj.AllPrice += item.price + item.remainings + item.serviceCharge;

                if (item.discountValue != null) {
                    timeslot.AllPrice -= item.discountValue;
                    obj.AllPrice -= item.discountValue;
                }
            })



        });

        if (obj.AllPrice > 0) {
            TimeslotList.forEach(function (item) {
                item.Percentage = ((item.AllPrice / obj.AllPrice) * 100.00).toFixed(2);
            })
        }


        obj.Timeslot = TimeslotList;

        return obj;
    }
    dailyPerHourSalesReport.prototype.getCallbackResult = function (callback) {
        var self = this;
        var DB = new database();
        DB.executeStatement(self.getQuery(), (err, result) => {
            callback(self.getResult(result));
        })
    }
}

function dailySalesNetReport(args, mode, DM) {
    this.date = moment().format("DD/MM/YYYY");

    dailySalesNetReport.prototype.getQuery = function () {

        var shopCode = args.shop;

        if (shopCode != null) {

            // shopCode = "%" + shopCode + "-%";
            shopCode = shopCode + "-%";
        }
        else
            shopCode = "%"

        var tempGlobalSql = GetGlobalTransactionStatement.viewSql;
        var tempGlobalDiscountSql = GetGlobalTransactionStatement.archiveDiscount;

        var query = [
              { "sql": tempGlobalSql, "args": [] },
              { "sql": tempGlobalDiscountSql, "args": [] },
              { "sql": "SELECT ConVert(varchar, c.categoryId) as categoryId, c.name1 , c.name2, c.name3 FROM [category] as c UNION  select optionGroupCode as categoryId, name1, name2, name3 from optionGroup", "args": [] },
              { "sql": "select itemCode, optionGroupCode from [itemOptionGroup]" }
        ];

        var argsdate = ""

        if (mode == "pos") argsdate = pad(args.day, 2) + "/" + pad(args.month, 2) + "/" + args.year;
        else argsdate = args.startDate;

        var self = this;

        function toYear() {
            var argsTodate = "";
            var sqlFromDates = "";
            var sqlToDates = "";

            if (mode == "pos") {
                argsTodate = pad(args.toDay, 2) + "/" + pad(args.toMonth, 2) + "/" + args.toYear;
                sqlFromDates = new Date(args.year, parseInt(args.month) - 1, args.day);
                sqlToDates = new Date(args.toYear, parseInt(args.toMonth) - 1, parseInt(args.toDay) + 1);
            } else {
                argsTodate = args.endDate;
                sqlFromDates = args.startDate;
                sqlToDates = args.endDate;
                sqlFromDates = moment(sqlFromDates, 'DD/MM/YYYY').toDate();
                sqlToDates = moment(sqlToDates, 'DD/MM/YYYY').add(1, 'day').toDate();
            }

            self.date = argsdate + ' - ' + argsTodate;

            query[0].sql = query[0].sql.replace("##conditionWhere1##", " where transTime between @fromDate and @toDate and refNo like @shopCode ")
            query[0].sql = query[0].sql.replace("##conditionWhere2##", " where transTime between @fromDate and @toDate and refNo like @shopCode ")

            query[0].args = [];

            query[0].args.push({ "c": "fromDate", "t": TYPES.DateTime, "v": sqlFromDates }, { "c": "toDate", "t": TYPES.DateTime, "v": sqlToDates });
            query[0].args.push({ "c": "shopCode", "t": TYPES.VarChar, "v": shopCode });

            query[1].sql = query[1].sql.replace("##conditionWhere1##", " and transTime between @fromDate and @toDate and t.refNo like @shopCode ")
            query[1].sql = query[1].sql.replace("##conditionWhere2##", " and transTime between @fromDate and @toDate and t.refNo like @shopCode ")
            query[1].args = [];

            query[1].args.push({ "c": "fromDate", "t": TYPES.DateTime, "v": sqlFromDates }, { "c": "toDate", "t": TYPES.DateTime, "v": sqlToDates });
            query[1].args.push({ "c": "shopCode", "t": TYPES.VarChar, "v": shopCode });

        }
        function today() {
            argsdate = self.date;
            query[0].sql = query[0].sql.replace("##conditionWhere1##", " where refNo like @shopCode  ")
            query[0].sql = query[0].sql.replace("##conditionWhere2##", " where convert(varchar(10), transTime, 103) = @date and refNo like @shopCode  ")
            query[0].args = [];
            query[0].args.push({ "c": "date", "t": TYPES.VarChar, "v": argsdate });
            query[0].args.push({ "c": "shopCode", "t": TYPES.VarChar, "v": shopCode });

            query[1].sql = query[1].sql.replace("##conditionWhere1##", " and t.refNo like @shopCode  ")
            query[1].sql = query[1].sql.replace("##conditionWhere2##", " and  convert(varchar(10), transTime, 103) = @date and t.refNo like @shopCode  ")
            query[1].args = [];
            query[1].args.push({ "c": "date", "t": TYPES.VarChar, "v": argsdate });
            query[1].args.push({ "c": "shopCode", "t": TYPES.VarChar, "v": shopCode });
        }
        function specificDay() {

            self.date = argsdate;

            query[0].sql = query[0].sql.replace("##conditionWhere1##", " where  convert(varchar(10), transTime, 103) = @date and refNo like @shopCode  ")
            query[0].sql = query[0].sql.replace("##conditionWhere2##", " where  convert(varchar(10), transTime, 103) = @date and refNo like @shopCode  ")
            query[0].args = [];

            query[0].args.push({ "c": "date", "t": TYPES.VarChar, "v": argsdate });
            query[0].args.push({ "c": "shopCode", "t": TYPES.VarChar, "v": shopCode });

            query[1].sql = query[1].sql.replace("##conditionWhere1##", " and  convert(varchar(10), transTime, 103) = @date and t.refNo like @shopCode  ")
            query[1].sql = query[1].sql.replace("##conditionWhere2##", " and  convert(varchar(10), transTime, 103) = @date and t.refNo like @shopCode  ")
            query[1].args = [];
            query[1].args.push({ "c": "date", "t": TYPES.VarChar, "v": argsdate });
            query[1].args.push({ "c": "shopCode", "t": TYPES.VarChar, "v": shopCode });

        }


        switch (mode) {
            case "pos": {
                if (args.year) {
                    if (args.toYear) {
                        toYear();
                    } else {
                        if (self.date == argsdate) {
                            today();
                        } else {
                            specificDay();
                        }
                    }
                } else {
                    today();
                }

                break;
            }
            case "cloud": {
                if (args.startDate == args.endDate) args.endDate = null;
                if (args.startDate) {
                    var argsdate = args.startDate;

                    if (args.endDate) {
                        toYear();
                    }
                    else {
                        if (self.date == argsdate) {
                            today();
                        } else {
                            specificDay();
                        }
                    }
                }
                break;
            }
        }

        return query;
    }

    dailySalesNetReport.prototype.getResult = function (result) {

        var obj = { "Category": new Object, "REPORT_DATE": moment().format("DD/MM/YYYY"), "AllPrice": 0.0, "AllQty": 0, "Remaining": 0.0, "SaleNetPrice": 0.0, "DiscountPrice": 0.0, "ServiceCharge": 0.0, "SaleAllPrice": 0.0, "menuList": [], "menuSelected": false }

        obj.REPORT_DATE = this.date;

        var transaction = result[0];

        var category = result[2];
        var OrderList = result[1];
        var itemOptionGroupList = result[3];
        var PackageList = [];


        var packageObject = new Package();
        PackageList = packageObject.GroupPackage(transaction, itemOptionGroupList);


        var TempFood = new Food();
        PackageList.forEach(function (packageItem, idx) {
            TempFood.addFoodByCategory(packageItem);
            packageItem.options.forEach(function (option) {
                TempFood.addFoodByCategory(option);
            })
        });



        var categoryObject = new Category();
        categoryObject.GroupCategory(category, TempFood.FoodList);

        categoryObject.CategoryList = categoryObject.CategoryList.filter(function (cate) {
            return cate.CateQty > 0;
        });

        extend(categoryObject.CategoryList, obj.menuList);

        if (args.menuList != null) {

            args.menuList.forEach(function (m) {
                categoryObject.CategoryList = categoryObject.CategoryList.filter(function (cate) {
                    return args.menuList.indexOf(cate.categoryId) != -1;
                });
            });

            if (obj.menuList.length != args.menuList.length) obj.menuSelected = true;
        }

        categoryObject.CategoryList.forEach(function (item) {
            obj.AllPrice += item.CatePrice;
            obj.AllQty += item.CateQty;
        })

        categoryObject.CategoryList.forEach(function (item) {
            item.PackageList.forEach(function (food) {
                if (item.CatePrice > 0) food.Percentage = ((food.TotalPrice / item.CatePrice) * 100.00).toFixed(2);
                else food.Percentage = 0.00
            })

            item.Percentage = ((item.CatePrice / obj.AllPrice) * 100.00).toFixed(2);
            if (item.CatePrice == 0 && obj.AllPrice == 0) item.Percentage = 0;
        })

        obj.CategoryList = categoryObject.CategoryList;


        for (var a = 0; a < OrderList.length; a++) {
            obj.ServiceCharge += OrderList[a].serviceCharge;
            obj.Remaining += OrderList[a].remainings;
            obj.DiscountPrice += OrderList[a].discountValue;

        }

        obj.SaleNetPrice = obj.AllPrice + obj.Remaining;
        obj.SaleAllPrice = obj.SaleNetPrice + obj.ServiceCharge - obj.DiscountPrice;

        //console.log(moment().format('YYYY-MM-DD HH:mm:ss'))
        return obj;
    }

    dailySalesNetReport.prototype.getCallbackResult = function (callback) {
        var self = this;
        var DB = new database();
        DB.executeStatement(self.getQuery(), (err, result) => {
            callback(self.getResult(result));
        })
    }
}

function dailyMostSalesFoodReport(args, mode, DM) {

    this.date = moment().format("DD/MM/YYYY");

    dailyMostSalesFoodReport.prototype.getQuery = function () {

        var shopCode = args.shop;

        if (shopCode != null) {

            // shopCode = "%" + shopCode + "-%";
            shopCode = shopCode + "-%";
        }
        else
            shopCode = "%"

        var tempGlobalSql = GetGlobalTransactionStatement.viewSql;

        var query = [
                { "sql": tempGlobalSql, "args": [] },
                { "sql": "select itemCode, optionGroupCode from [itemOptionGroup]" }
        ];

        var argsdate = ""

        if (mode == "pos") argsdate = pad(args.day, 2) + "/" + pad(args.month, 2) + "/" + args.year;
        else argsdate = args.startDate;

        var self = this;

        function toYear() {
            var argsTodate = "";
            var sqlFromDates = "";
            var sqlToDates = "";

            if (mode == "pos") {
                argsTodate = pad(args.toDay, 2) + "/" + pad(args.toMonth, 2) + "/" + args.toYear;
                sqlFromDates = new Date(args.year, parseInt(args.month) - 1, args.day);
                sqlToDates = new Date(args.toYear, parseInt(args.toMonth) - 1, parseInt(args.toDay) + 1);
            } else {
                argsTodate = args.endDate;
                sqlFromDates = args.startDate;
                sqlToDates = args.endDate;
                sqlFromDates = moment(sqlFromDates, 'DD/MM/YYYY').toDate();
                sqlToDates = moment(sqlToDates, 'DD/MM/YYYY').add(1, 'day').toDate();
            }

            self.date = argsdate + ' - ' + argsTodate;

            query[0].sql = query[0].sql.replace("##conditionWhere1##", " where transTime between @fromDate and @toDate and t.refNo like @shopCode ")
            query[0].sql = query[0].sql.replace("##conditionWhere2##", " where transTime between @fromDate and @toDate and t.refNo like @shopCode ")
            query[0].args = [];

            query[0].args.push({ "c": "fromDate", "t": TYPES.DateTime, "v": sqlFromDates }, { "c": "toDate", "t": TYPES.DateTime, "v": sqlToDates });
            query[0].args.push({ "c": "shopCode", "t": TYPES.VarChar, "v": shopCode });

        }
        function today() {
            argsdate = self.date;

            query[0].sql = query[0].sql.replace("##conditionWhere1##", " where t.refNo like @shopCode  ")
            query[0].sql = query[0].sql.replace("##conditionWhere2##", " where  convert(varchar(10), transTime, 103) = @date and t.refNo like @shopCode  ")
            query[0].args = [];
            query[0].args.push({ "c": "date", "t": TYPES.VarChar, "v": argsdate });
            query[0].args.push({ "c": "shopCode", "t": TYPES.VarChar, "v": shopCode });

        }
        function specificDay() {

            self.date = argsdate;

            query[0].sql = query[0].sql.replace("##conditionWhere1##", " where  convert(varchar(10), transTime, 103) = @date and t.refNo like @shopCode  ")
            query[0].sql = query[0].sql.replace("##conditionWhere2##", " where  convert(varchar(10), transTime, 103) = @date and t.refNo like @shopCode  ")
            query[0].args = [];

            query[0].args.push({ "c": "date", "t": TYPES.VarChar, "v": argsdate });
            query[0].args.push({ "c": "shopCode", "t": TYPES.VarChar, "v": shopCode });


        }

        switch (mode) {
            case "pos": {
                if (args.year) {
                    if (args.toYear) {
                        toYear();
                    } else {
                        if (self.date == argsdate) {
                            today();
                        } else {
                            specificDay();
                        }
                    }
                } else {
                    today();
                }

                break;
            }
            case "cloud": {
                if (args.startDate == args.endDate) args.endDate = null;
                if (args.startDate) {
                    var argsdate = args.startDate;

                    if (args.endDate) {
                        toYear();
                    }
                    else {
                        if (self.date == argsdate) {
                            today();
                        } else {
                            specificDay();
                        }
                    }
                }
                break;
            }
        }

        return query;
    }


    dailyMostSalesFoodReport.prototype.getResult = function (result, sortBy) {
        var obj = { "FoodList": [], "REPORT_DATE": moment().format("DD/MM/YYYY") };

        obj.REPORT_DATE = this.date;


        var transaction = result[0];
        var itemOptionGroup = result[1];
        var PackageList = [];
        var packageObject = new Package();
        PackageList = packageObject.GroupPackage(transaction, itemOptionGroup);

        var food = new Food();

        PackageList.forEach(function (item) {
            food.addFoodByCategory(item);
            item.options.forEach(function (option) {
                food.addFoodByCategory(option);
            })
        })


        //sort Price
        if (sortBy == 0) {
            food.FoodList.sort(function (a, b) {
                if (a.TotalPrice < b.TotalPrice) {
                    return 1;
                }
                if (a.TotalPrice > b.TotalPrice) {
                    return -1;
                }
                return 0;
            });
        }
        else if (sortBy == 1) {
            food.FoodList.sort(function (a, b) {
                if (a.TotalQty < b.TotalQty) {
                    return 1;
                }
                if (a.TotalQty > b.TotalQty) {
                    return -1;
                }
                return 0;
            });
        }


        food.FoodList = food.FoodList.filter(function (f) {
            return f.itemId != 'O9999';
        })


        if (food.FoodList.length > 25)
            food.FoodList.splice(25, food.FoodList.length - 25);

        obj.FoodList = food.FoodList;

        return obj;
    }

    dailyMostSalesFoodReport.prototype.getCallbackResult = function (sortBy, callback) {
        var self = this;
        var DB = new database();
        DB.executeStatement(self.getQuery(), (err, result) => {
            callback(self.getResult(result, sortBy));
        })
    }
}

function dailyCategoriesReport(args, mode, DM) {

    this.date = moment().format("DD/MM/YYYY");

    dailyCategoriesReport.prototype.getQuery = function () {

        var shopCode = args.shop;

        if (shopCode != null) {

            // shopCode = "%" + shopCode + "-%";
            shopCode = shopCode + "-%";
        }
        else
            shopCode = "%"

        var tempGlobalSql = GetGlobalTransactionStatement.viewSql;
        var tempGlobalDiscountSql = GetGlobalTransactionStatement.archiveDiscount;

        var query = [
           { "sql": tempGlobalSql, "args": [] },
           { "sql": "SELECT ConVert(varchar, c.categoryId) as categoryId, c.name1, c.name2, c.name3 FROM [category] as c UNION  select optionGroupCode as categoryId, name1, name2, name3 from optionGroup", "args": [] },

           { "sql": tempGlobalDiscountSql, "args": [] },

           { "sql": "select itemCode, optionGroupCode from [itemOptionGroup]" }
        ];

        var argsdate = ""

        if (mode == "pos") argsdate = pad(args.day, 2) + "/" + pad(args.month, 2) + "/" + args.year;
        else argsdate = args.startDate;

        var self = this;

        function toYear() {
            var argsTodate = "";
            var sqlFromDates = "";
            var sqlToDates = "";

            if (mode == "pos") {
                argsTodate = pad(args.toDay, 2) + "/" + pad(args.toMonth, 2) + "/" + args.toYear;
                sqlFromDates = new Date(args.year, parseInt(args.month) - 1, args.day);
                sqlToDates = new Date(args.toYear, parseInt(args.toMonth) - 1, parseInt(args.toDay) + 1);
            } else {
                argsTodate = args.endDate;
                sqlFromDates = args.startDate;
                sqlToDates = args.endDate;
                sqlFromDates = moment(sqlFromDates, 'DD/MM/YYYY').toDate();
                sqlToDates = moment(sqlToDates, 'DD/MM/YYYY').add(1, 'day').toDate();
            }

            self.date = argsdate + ' - ' + argsTodate;

            query[0].sql = query[0].sql.replace("##conditionWhere1##", " where transTime between @fromDate and @toDate and t.refNo like @shopCode ")
            query[0].sql = query[0].sql.replace("##conditionWhere2##", " where transTime between @fromDate and @toDate  and t.refNo like @shopCode ")
            query[0].args = [];

            query[0].args.push({ "c": "fromDate", "t": TYPES.DateTime, "v": sqlFromDates }, { "c": "toDate", "t": TYPES.DateTime, "v": sqlToDates });
            query[0].args.push({ "c": "shopCode", "t": TYPES.VarChar, "v": shopCode });


            query[2].sql = query[2].sql.replace("##conditionWhere1##", " and transTime between @fromDate and @toDate  and t.refNo like @shopCode   ")
            query[2].sql = query[2].sql.replace("##conditionWhere2##", " and transTime between @fromDate and @toDate  and t.refNo like @shopCode  ")
            query[2].args = [];

            query[2].args.push({ "c": "fromDate", "t": TYPES.DateTime, "v": sqlFromDates }, { "c": "toDate", "t": TYPES.DateTime, "v": sqlToDates });
            query[2].args.push({ "c": "shopCode", "t": TYPES.VarChar, "v": shopCode });

        }
        function today() {
            argsdate = self.date;

            query[0].sql = query[0].sql.replace("##conditionWhere1##", "  where t.refNo like @shopCode   ")
            query[0].sql = query[0].sql.replace("##conditionWhere2##", " where convert(varchar(10), transTime, 103) = @date  and t.refNo like @shopCode   ")
            query[0].args = [];

            query[0].args.push({ "c": "date", "t": TYPES.VarChar, "v": argsdate });
            query[0].args.push({ "c": "shopCode", "t": TYPES.VarChar, "v": shopCode });

            query[2].sql = query[2].sql.replace("##conditionWhere1##", " and t.refNo like @shopCode ")
            query[2].sql = query[2].sql.replace("##conditionWhere2##", " and  convert(varchar(10), transTime, 103) = @date and t.refNo like @shopCode ")
            query[2].args = [];
            query[2].args.push({ "c": "date", "t": TYPES.VarChar, "v": argsdate });
            query[2].args.push({ "c": "shopCode", "t": TYPES.VarChar, "v": shopCode });

        }
        function specificDay() {

            self.date = argsdate;

            query[0].sql = query[0].sql.replace("##conditionWhere1##", " where convert(varchar(10), transTime, 103) = @date and t.refNo like @shopCode ")
            query[0].sql = query[0].sql.replace("##conditionWhere2##", " where convert(varchar(10), transTime, 103) = @date and t.refNo like @shopCode ")
            query[0].args = [];

            query[0].args.push({ "c": "date", "t": TYPES.VarChar, "v": argsdate });
            query[0].args.push({ "c": "shopCode", "t": TYPES.VarChar, "v": shopCode });

            query[2].sql = query[2].sql.replace("##conditionWhere1##", " and  convert(varchar(10), transTime, 103) = @date and t.refNo like @shopCode  ")
            query[2].sql = query[2].sql.replace("##conditionWhere2##", " and  convert(varchar(10), transTime, 103) = @date and t.refNo like @shopCode  ")
            query[2].args = [];

            query[2].args.push({ "c": "date", "t": TYPES.VarChar, "v": argsdate });
            query[2].args.push({ "c": "shopCode", "t": TYPES.VarChar, "v": shopCode });


        }

        switch (mode) {
            case "pos": {
                if (args.year) {
                    if (args.toYear) {
                        toYear();
                    } else {
                        if (self.date == argsdate) {
                            today();
                        } else {
                            specificDay();
                        }
                    }
                } else {
                    today();
                }

                break;
            }
            case "cloud": {
                if (args.startDate == args.endDate) args.endDate = null;
                if (args.startDate) {
                    var argsdate = args.startDate;

                    if (args.endDate) {
                        toYear();
                    }
                    else {
                        if (self.date == argsdate) {
                            today();
                        } else {
                            specificDay();
                        }
                    }
                }
                break;
            }
        }

        return query;
    }

    dailyCategoriesReport.prototype.getResult = function (result) {

        var obj = { "Category": [], "REPORT_DATE": moment().format("DD/MM/YYYY"), "AllPrice": 0.0, "AllQty": 0, "ServiceCharge": 0.0, "Remaining": 0.0, "DiscountPrice": 0.0, "SaleNetPrice": 0.0, "SaleAllPrice": 0.0 };
        obj.REPORT_DATE = this.date;

        var transaction = result[0];
        var itemOptionGroupList = result[3];
        var PackageList = [];
        var packageObject = new Package();
        PackageList = packageObject.GroupPackage(transaction, itemOptionGroupList);


        var food = new Food();
        PackageList.forEach(function (item, idx) {
            //if (item.CategoryId == "0") return true;
            food.addFoodByCategory(item);
            item.options.forEach(function (option) {
                food.addFoodByCategory(option);
            })
        })


        var category = result[1];
        var categoryObject = new Category();
        categoryObject.GroupCategory(category, food.FoodList)


        obj.AllPrice = 0;
        obj.AllQty = 0;
        obj.ServiceCharge = 0;
        obj.Remaining = 0;
        obj.DiscountPrice = 0;
        obj.SaleNetPrice = 0;
        obj.SaleAllPrice = 0;

        categoryObject.CategoryList.forEach(function (cate) {
            obj.AllPrice += cate.CatePrice;
            obj.AllQty += cate.CateQty;
        })


        obj.Category = categoryObject.CategoryList;
        //obj.FoodList = food.FoodList;

        var OrderList = result[2];

        for (var a = 0; a < OrderList.length; a++) {
            obj.ServiceCharge += OrderList[a].serviceCharge;
            obj.Remaining += OrderList[a].remainings;
            obj.DiscountPrice += OrderList[a].discountValue;

        }

        obj.SaleNetPrice = obj.AllPrice + obj.Remaining;
        obj.SaleAllPrice = obj.SaleNetPrice + obj.ServiceCharge - obj.DiscountPrice;


        if (obj.AllPrice > 0) {
            categoryObject.CategoryList.forEach(function (item) {
                item.Percentage = ((item.CatePrice / obj.AllPrice) * 100.00).toFixed(2);
            })
        }

        return obj;

    }

    dailyCategoriesReport.prototype.getCallbackResult = function (callback) {
        var self = this;
        var DB = new database();
        DB.executeStatement(self.getQuery(), (err, result) => {
            callback(self.getResult(result));
        })
    }
}

function dailyFoodTimeslotReport(args, mode, DM) {

    this.date = moment().format("DD/MM/YYYY");

    dailyFoodTimeslotReport.prototype.getCallbackResult = function (callback) {

        var conditionWhere = { where: "", args: [], where1: "", where2: "" };
        var obj = { "timeslots": [], "REPORT_DATE": moment().format("DD/MM/YYYY") }
        var timeslot = {};
        var exeFunc = [];
        var itemOptionGroup = [];
        var self = this;

        var shopCode = args.shop;

        if (shopCode != null) {

            // shopCode = "%" + shopCode + "-%";
            shopCode = shopCode + "-%";
        }
        else
            shopCode = "%"


        var argsdate = ""

        if (mode == "pos") argsdate = pad(args.day, 2) + "/" + pad(args.month, 2) + "/" + args.year;
        else argsdate = args.startDate;



        function toYear() {
            var argsTodate = "";
            var sqlFromDates = "";
            var sqlToDates = "";

            if (mode == "pos") {
                argsTodate = pad(args.toDay, 2) + "/" + pad(args.toMonth, 2) + "/" + args.toYear;
                sqlFromDates = new Date(args.year, parseInt(args.month) - 1, args.day);
                sqlToDates = new Date(args.toYear, parseInt(args.toMonth) - 1, parseInt(args.toDay) + 1);
            } else {
                argsTodate = args.endDate;
                sqlFromDates = args.startDate;
                sqlToDates = args.endDate;
                sqlFromDates = moment(sqlFromDates, 'DD/MM/YYYY').toDate();
                sqlToDates = moment(sqlToDates, 'DD/MM/YYYY').add(1, 'day').toDate();
            }



            self.date = argsdate + ' - ' + argsTodate;

            conditionWhere.where = " and transTime between @fromDate and @toDate ";
            conditionWhere.where1 = " where transTime between @fromDate and @toDate and void = 0  and refNo like @shopCode   ";
            conditionWhere.where2 = " where transTime between @fromDate and @toDate and void = 0  and refNo like @shopCode   ";
            conditionWhere.args = [];

            conditionWhere.args.push({ "c": "fromDate", "t": TYPES.DateTime, "v": sqlFromDates }, { "c": "toDate", "t": TYPES.DateTime, "v": sqlToDates });
            conditionWhere.args.push({ "c": "shopCode", "t": TYPES.VarChar, "v": shopCode });
        }
        function today() {
            argsdate = self.date;

            conditionWhere.where = " and convert(varchar(10), transTime, 103) = @date ";
            conditionWhere.where1 = " where void = 0 and refNo like @shopCode ";
            conditionWhere.where2 = " where convert(varchar(10), transTime, 103) = @date and void = 0 and refNo like @shopCode ";
            conditionWhere.args = [];
            conditionWhere.args.push({ "c": "date", "t": TYPES.VarChar, "v": argsdate });
            conditionWhere.args.push({ "c": "shopCode", "t": TYPES.VarChar, "v": shopCode });
        }
        function specificDay() {
            self.date = argsdate;

            conditionWhere.where = " and convert(varchar(10), transTime, 103) = @date ";
            conditionWhere.where1 = " where convert(varchar(10), transTime, 103) = @date and void = 0 and refNo like @shopCode ";
            conditionWhere.where2 = " where convert(varchar(10), transTime, 103) = @date and void = 0  and refNo like @shopCode ";
            conditionWhere.args = [];
            conditionWhere.args.push({ "c": "date", "t": TYPES.VarChar, "v": self.date });
            conditionWhere.args.push({ "c": "shopCode", "t": TYPES.VarChar, "v": shopCode });
        }

        switch (mode) {
            case "pos": {
                if (args.year) {
                    if (args.toYear) {
                        toYear();
                    } else {
                        if (self.date == argsdate) {
                            today();
                        } else {
                            specificDay();
                        }
                    }
                } else {
                    today();
                }

                break;
            }
            case "cloud": {
                if (args.startDate == args.endDate) args.endDate = null;
                if (args.startDate) {
                    var argsdate = args.startDate;

                    if (args.endDate) {
                        toYear();
                    }
                    else {
                        if (self.date == argsdate) {
                            today();
                        } else {
                            specificDay();
                        }
                    }
                }
                break;
            }
        }

        obj.REPORT_DATE = self.date;

        exeFunc.push((cb) => {
            var timeslotQuery = [
                { "sql": " select * from timeslot order by timeStart;" },
                { "sql": "select itemCode, optionGroupCode from [itemOptionGroup];" }
            ]

            var DB = new database();
            DB.executeStatement(timeslotQuery, (err, timeslotList) => {
                itemOptionGroup = timeslotList[1];
                cb(null, timeslotList[0]);
            })

        });

        exeFunc.push((timeslotList, cb) => {
            var exeReport = []



            timeslotList.forEach(function (t, idx) {

                exeReport.push((cb2) => {
                    var transQuery = [

                    ]


                    var q = { "sql": " select * from (select * from salesfood " + conditionWhere.where1 + " union select * from salesArchivefood " + conditionWhere.where2 + ") as tmpl where  cast(startTime as time) >= '" + t.timeStart.toLocaleTimeString({ hour12: false }) + "' and cast(startTime as time)  < '" + t.timeEnd.toLocaleTimeString({ hour12: false }) + "' " + "" + " order by refNo, round, seq, type, transTime, time ", args: conditionWhere.args }

                    /*var d = { "sql": "select * from (SELECT [tableCode],[tableSubCode],[pax] ,[memberId] ,[memberName] ,[startTime] ,t.[refNo] ,[transTime] ,[payAmount],[price] ,[serviceCharge] ,[remainings],[reprint],[orderId] , (select SUM(discountValue) from [tenderDiscount] as tdd where refNo = t.refNo and tdd.void = 0 " + conditionWhere.where1 + ") as discountValue  FROM [transaction] as t where t.void = 0 union  SELECT [tableCode],[tableSubCode],[pax] ,[memberId] ,[memberName] ,[startTime] ,t.[refNo] ,  [transTime] ,[payAmount] ,[price] ,[serviceCharge] ,[remainings],[reprint],[orderId] , (select SUM(discountValue) from [tenderDiscountArchive] as tdd where refNo = t.refNo and tdd.void = 0 " + conditionWhere.where + ") as discountValue  FROM [transactionArchive] as t where t.void = 0 ) a where cast(transTime as time) >= '" + t.timeStart.toLocaleTimeString({ hour12: false }) + "' and cast(transTime as time)  < '" + t.timeEnd.toLocaleTimeString({ hour12: false }) + "' " + "" + "  order by refNo", args: conditionWhere.args }*/

                    var d = { "sql": "select * from (SELECT [tableCode],[tableSubCode],[pax] ,[memberId] ,[memberName] ,[startTime] ,t.[refNo] ,[transTime] ,[payAmount],[price] ,[serviceCharge] ,[remainings],[reprint],[orderId] , (select SUM(discountValue) from [tenderDiscount] as tdd where refNo = t.refNo and tdd.void = 0 " + "" + ") as discountValue  FROM [transaction] as t " + conditionWhere.where1 + " union  SELECT [tableCode],[tableSubCode],[pax] ,[memberId] ,[memberName] ,[startTime] ,t.[refNo] ,  [transTime] ,[payAmount] ,[price] ,[serviceCharge] ,[remainings],[reprint],[orderId] , (select SUM(discountValue) from [tenderDiscountArchive] as tdd where refNo = t.refNo and tdd.void = 0 " + "" + ") as discountValue  FROM [transactionArchive] as t  " + conditionWhere.where2 + ") a where cast(startTime as time) >= '" + t.timeStart.toLocaleTimeString({ hour12: false }) + "' and cast(startTime as time)  < '" + t.timeEnd.toLocaleTimeString({ hour12: false }) + "' " + "" + "  order by refNo", args: conditionWhere.args }

                    transQuery.push(q, d);

                    var DB = new database();
                    DB.executeStatement(transQuery, (err, result) => {
                        var itemOptionGroupList = itemOptionGroup;
                        //var transactionOrderList = result[1];

                        var timeslot = { "TIMESLOT_VALUE": "", "TIMESLOT_NAME": "", "FOODMENU": [], "SaleNetPrice": 0.0, "NumOfOrder": 0, "AvgOfNumOfOrder": 0.0, "NumOfPax": 0, "AvgTotalPriceOfPax": 0.0, "ServiceCharge": 0.0, "Remaining": 0.0, "DiscountPrice": 0.0 };
                        timeslot.TIMESLOT_NAME = t.name;
                        timeslot.TIMESLOT_VALUE = t.timeStart.toLocaleTimeString({ hour12: false }) + " - " + t.timeEnd.toLocaleTimeString({ hour12: false })

                        var OrderList = result[1];

                        timeslot.NumOfOrder = OrderList.length;
                        for (var a = 0; a < OrderList.length; a++) {
                            timeslot.SaleNetPrice += OrderList[a].price;
                            timeslot.NumOfPax += OrderList[a].pax;
                            timeslot.ServiceCharge += OrderList[a].serviceCharge;
                            timeslot.Remaining += OrderList[a].remainings;
                            timeslot.DiscountPrice += OrderList[a].discountValue;
                            if (a == OrderList.length - 1) {
                                timeslot.SaleNetPrice = (timeslot.SaleNetPrice + timeslot.ServiceCharge + timeslot.Remaining) - timeslot.DiscountPrice;
                                timeslot.AvgOfNumOfOrder = timeslot.SaleNetPrice / timeslot.NumOfOrder;
                                timeslot.AvgTotalPriceOfPax = timeslot.SaleNetPrice / timeslot.NumOfPax;
                            }

                        }

                        var TransactionList = result[0];



                        var PackageList = [];
                        var packageObject = new Package();
                        PackageList = packageObject.GroupPackage(TransactionList, itemOptionGroupList);


                        var TempFood = new Food();
                        var optionsList = [];
                        var optionsListId = [];

                        PackageList.forEach(function (packageItem) {
                            TempFood.addFoodByCategory(packageItem);
                            //packageItem.options = packageItem.options.filter(function (opts) { return opts.qty >= 0; })
                            if (packageItem.options.length > 0) {
                                var isExist = optionsListId.indexOf(packageItem.itemId)
                                if (isExist == -1) {
                                    var temp = new Object;
                                    temp.options = new Array;
                                    temp.parentItemId = packageItem.itemId;

                                    packageItem.options.forEach(function (option) {
                                        temp.options.push(option);
                                    })

                                    optionsListId.push(packageItem.itemId)
                                    optionsList.push(temp);


                                } else {
                                    packageItem.options.forEach(function (option) {
                                        optionsList[isExist].options.push(option);
                                    })
                                }
                            }

                        });

                        //console.log(JSON.stringify(TempFood.FoodList));

                        optionsList.forEach(function (opts, index) {
                            var tempFood = new Food();
                            opts.options.forEach(function (item) {
                                tempFood.addFoodByCategory(item);
                            });

                            var templ = TempFood.FoodList.filter(function (t) { return t.itemId == opts.parentItemId });
                            templ[0].options = tempFood.FoodList;
                        })



                        //timeslot.SaleNetPrice =Food.SaleTotalPrice;
                        timeslot.FOODMENU = TempFood.FoodList;


                        obj.timeslots.push(timeslot);
                        cb2(null);


                    })

                })
            });

            async.waterfall(exeReport, function () {
                cb(null);
            });

        })

        async.waterfall(exeFunc, function (result) {
            callback(obj);
            if (args.isPrint) {
                self.printReport(obj);
            }
        });
    }

    dailyFoodTimeslotReport.prototype.printReport = function (obj) {
        var printer = args.printer ? args.printer : DefaultPrinter;
        obj.printer = printer;

        obj.timeslots.forEach(function (tlts) {
            tlts.FOODMENU.forEach(function (food) {
                food.TotalPrice = S("$" + food.TotalPrice.format(1, 3)).padLeft(10).s;
                food.options.forEach(function (opt) {
                    opt.TotalPrice = S("$" + opt.TotalPrice.format(1, 3)).padLeft(10).s;
                })
            });
            tlts.SaleNetPrice = S("$" + tlts.SaleNetPrice.format(1, 3)).padLeft(10).s;
            tlts.AvgTotalPriceOfPax = S("$" + tlts.AvgTotalPriceOfPax.format(1, 3)).padLeft(10).s;
            tlts.ServiceCharge = S("$" + tlts.ServiceCharge.format(1, 3)).padLeft(10).s;
            tlts.Remaining = S("$" + tlts.Remaining.format(1, 3)).padLeft(10).s;
            tlts.DiscountPrice = S("$" + tlts.DiscountPrice.format(1, 3)).padLeft(10).s;
            tlts.AvgOfNumOfOrder = S("$" + tlts.AvgOfNumOfOrder.format(1, 3)).padLeft(10).s;
        });

        fs.writeFile("./printer/" + obj.printer + "/timeslotFoodReport_" + moment().format("YYMMDDHHmmss") + ".json", JSON.stringify(obj), function (err) {
            if (err) {
                //console.log(err);
            } else {
                ////console.log("The file was saved!");
            }
        });
    }
}

function dailyCancelItemReport(args, mode, DM) {

    this.date = moment().format("DD/MM/YYYY");

    dailyCancelItemReport.prototype.getQuery = function () {
        var shopCode = args.shop;

        if (shopCode != null) {

            // shopCode = "%" + shopCode + "-%";
            shopCode = shopCode + "-%";
        }
        else
            shopCode = "%"

        var tempGlobalSql = GetGlobalTransactionStatement.viewSql;

        var query = [
              { "sql": tempGlobalSql, "args": [] },
              { "sql": "select itemCode, optionGroupCode from [itemOptionGroup]" },
              { "sql": "select staffId, name from [staff]" }
        ];

        var argsdate = ""

        if (mode == "pos") argsdate = pad(args.day, 2) + "/" + pad(args.month, 2) + "/" + args.year;
        else argsdate = args.startDate;

        var self = this;

        function toYear() {
            var argsTodate = "";
            var sqlFromDates = "";
            var sqlToDates = "";

            if (mode == "pos") {
                argsTodate = pad(args.toDay, 2) + "/" + pad(args.toMonth, 2) + "/" + args.toYear;
                sqlFromDates = new Date(args.year, parseInt(args.month) - 1, args.day);
                sqlToDates = new Date(args.toYear, parseInt(args.toMonth) - 1, parseInt(args.toDay) + 1);
            } else {
                argsTodate = args.endDate;
                sqlFromDates = args.startDate;
                sqlToDates = args.endDate;
                sqlFromDates = moment(sqlFromDates, 'DD/MM/YYYY').toDate();
                sqlToDates = moment(sqlToDates, 'DD/MM/YYYY').add(1, 'day').toDate();
            }

            self.date = argsdate + ' - ' + argsTodate;

            query[0].sql = query[0].sql.replace("##conditionWhere1##", " where transTime between @fromDate and @toDate and refNo like @shopCode")
            query[0].sql = query[0].sql.replace("##conditionWhere2##", " where transTime between @fromDate and @toDate and refNo like @shopCode")
            query[0].args = [];

            query[0].args.push({ "c": "fromDate", "t": TYPES.DateTime, "v": sqlFromDates }, { "c": "toDate", "t": TYPES.DateTime, "v": sqlToDates });
            query[0].args.push({ "c": "shopCode", "t": TYPES.VarChar, "v": shopCode });

        }
        function today() {
            argsdate = self.date;
            query[0].sql = query[0].sql.replace("##conditionWhere1##", " where refNo like @shopCode")
            query[0].sql = query[0].sql.replace("##conditionWhere2##", " where  convert(varchar(10), transTime, 103) = @date  and t.refNo like @shopCode ")
            query[0].args = [];
            query[0].args.push({ "c": "date", "t": TYPES.VarChar, "v": argsdate });
            query[0].args.push({ "c": "shopCode", "t": TYPES.VarChar, "v": shopCode });
        }
        function specificDay() {

            self.date = argsdate;

            query[0].sql = query[0].sql.replace("##conditionWhere1##", " where  convert(varchar(10), transTime, 103) = @date and refNo like @shopCode  ")
            query[0].sql = query[0].sql.replace("##conditionWhere2##", " where  convert(varchar(10), transTime, 103) = @date and refNo like @shopCode  ")
            query[0].args = [];

            query[0].args.push({ "c": "date", "t": TYPES.VarChar, "v": argsdate });
            query[0].args.push({ "c": "shopCode", "t": TYPES.VarChar, "v": shopCode });

        }


        switch (mode) {
            case "pos": {
                if (args.year) {
                    if (args.toYear) {
                        toYear();
                    } else {
                        if (self.date == argsdate) {
                            today();
                        } else {
                            specificDay();
                        }
                    }
                } else {
                    today();
                }

                break;
            }
            case "cloud": {
                if (args.startDate == args.endDate) args.endDate = null;
                if (args.startDate) {
                    var argsdate = args.startDate;

                    if (args.endDate) {
                        toYear();
                    }
                    else {
                        if (self.date == argsdate) {
                            today();
                        } else {
                            specificDay();
                        }
                    }
                }
                break;
            }
        }

        return query;
    }

    dailyCancelItemReport.prototype.getResult = function (result) {
        var date = this.date;
        var obj = { "CancelList": [], "NumOfAllQty": 0, "TotalPrice": 0.0, "REPORT_DATE": moment().format("DD/MM/YYYY") }

        obj.REPORT_DATE = date;

        var TransactionList = result[0];
        var itemOptionGroupList = result[1];
        var staffList = result[2]
        var PackageList = [];
        var packageObject = new Package();
        packageObject.GroupPackage(TransactionList, itemOptionGroupList);
        PackageList = packageObject.PackageList

        PackageList = PackageList.filter(function (item) {
            return item.qty < 0;
        })

        PackageList.forEach(function (element) {
            AddItemToCancelList(element);
        })

        function AddItemToCancelList(element) {

            var CancelItem = { "refNo": "", "TableCode": "", "Time": "", "Staff": "", "ApproveStaff": "", "FoodName": "", "Qty": 0, "Price": 0.0, "Reason": "", "Options": new Array };
            try {

                CancelItem.refNo = element.transaction.refNo;
                CancelItem.TableCode = element.transaction.tableCode;
                CancelItem.Time = moment(element.transaction.transTime).format('YYYY-MM-DD HH:mm:ss')
                CancelItem.Staff = staffList.filter(function (t) {
                    return t.staffId == element.transaction.staffId;
                })
                //
                CancelItem.ApproveStaff = element.transaction.ApproveName == null ? "" : element.ApproveName;
                //CancelItem.FoodName = element.transaction.namek == null ? (element.transaction.optionName == null ? element.transaction.SubitemName : element.transaction.optionName) : element.transaction.namek;
                CancelItem.FoodName = element.namek;

                if (CancelItem.FoodName == null) element.transaction.itemId;

                CancelItem.Qty = Math.abs(element.qty)
                obj.NumOfAllQty += CancelItem.Qty;

                CancelItem.Price = element.transaction.subPrice;
                obj.TotalPrice = obj.TotalPrice + (CancelItem.Qty * CancelItem.Price);


                CancelItem.Reason = element.transaction.voidRemark;
                element.options.forEach(function (option) {
                    //console.log(option)
                    var TempOption = { FoodName: "", Qty: 0, Price: 0.0 };
                    //TempOption.FoodName = option.transaction.namek == null ? (option.transaction.optionName == null ? option.transaction.SubitemName : option.transaction.optionName) : option.transaction.namek;
                    TempOption.FoodName = option.namek;

                    TempOption.Price = option.transaction.subPrice;
                    TempOption.Qty = CancelItem.Qty;
                    obj.TotalPrice = obj.TotalPrice + (CancelItem.Qty * TempOption.Price);
                    CancelItem.Options.push(TempOption)

                })

                obj.CancelList.push(CancelItem);

                return false;

            } catch (err) { console.log("AddItemToCancelList: " + err.message); }

        }

        console.log(3276, obj);
        return obj;
    }

    dailyCancelItemReport.prototype.getCallbackResult = function (callback) {

        var self = this;
        var DB = new database();
        DB.executeStatement(self.getQuery(), (err, result) => {
            var obj = self.getResult(result);
            callback(obj);
            /*if (args.isPrint) {
                self.printReport(obj);
            }*/
        })
    }
}

function dailyArchiveReport(args, mode, DM) {

    this.date = moment().format("DD/MM/YYYY");
    this.dailyReport = new dailyReport(args, mode, DM);

    dailyArchiveReport.prototype.getQuery = function () {

        var shopCode = args.shop;

        if (shopCode != null) {

            // shopCode = "%" + shopCode + "-%";
            shopCode = shopCode + "-%";
        }
        else
            shopCode = "%"

        var query = [{ "sql": "select * from tenderPaymentArchive where void = 0 and refNo in (select refNo from transactionArchive where archiveDate in (select distinct archiveDate from  (select transTime, archiveDate from [transactionArchive] where convert(char(10), transTime, 103) = @date ) as Temp) ) order by refNo ", "args": [{ "c": "date", "t": TYPES.VarChar, "v": this.date }] },

        { "sql": "select * from tenderDiscountArchive where void = 0 and refNo in (select refNo from transactionArchive where archiveDate in (select distinct archiveDate from  (select transTime, archiveDate from [transactionArchive] where convert(char(10), transTime, 103) = @date ) as Temp) ) order by refNo ", "args": [{ "c": "date", "t": TYPES.VarChar, "v": this.date }] },

        { "sql": "select * from transactionArchive where void = 0 and archiveDate in (select distinct archiveDate from  (select transTime, archiveDate from [transactionArchive] where convert(char(10), transTime, 103) = @date) as Temp) order by archiveDate, refNo", "args": [{ "c": "date", "t": TYPES.VarChar, "v": this.date }] },
        {
            "sql": "select distinct archiveDate from  (select transTime, archiveDate from [transactionArchive] where convert(char(10), transTime, 103) = @date " + (args.till ? (" and till = '" + args.till) + "'" : "") + " ) as Temp  order by archiveDate  ", "args": [{ "c": "date", "t": TYPES.VarChar, "v": this.date }]
        }
        ]

        var argsdate = ""

        if (mode == "pos") argsdate = pad(args.day, 2) + "/" + pad(args.month, 2) + "/" + args.year;
        else argsdate = args.startDate;

        var self = this;

        function toYear() {
            var argsTodate = "";
            var sqlFromDates = "";
            var sqlToDates = "";

            if (mode == "pos") {
                argsTodate = pad(args.toDay, 2) + "/" + pad(args.toMonth, 2) + "/" + args.toYear;
                sqlFromDates = new Date(args.year, parseInt(args.month) - 1, args.day);
                sqlToDates = new Date(args.toYear, parseInt(args.toMonth) - 1, parseInt(args.toDay) + 1);
            } else {
                argsTodate = args.endDate;
                sqlFromDates = args.startDate;
                sqlToDates = args.endDate;
                sqlFromDates = moment(sqlFromDates, 'DD/MM/YYYY').toDate();
                sqlToDates = moment(sqlToDates, 'DD/MM/YYYY').add(1, 'day').toDate();
            }

            self.date = argsdate + ' - ' + argsTodate;

            query[3].sql = "select distinct archiveDate from  (select transTime, archiveDate from [transactionArchive] where transTime between @fromDate and @toDate " + (args.till ? (" and till = '" + args.till) + "'" : "") + " ) as Temp  order by archiveDate ";
            query[3].args = [];
            query[3].args.push(
                { "c": "fromDate", "t": TYPES.DateTime, "v": sqlFromDates },
                { "c": "toDate", "t": TYPES.DateTime, "v": sqlToDates }
            );

            query[2].sql = "select * from transactionArchive where void = 0 and archiveDate in (select distinct archiveDate from  (select transTime, archiveDate from [transactionArchive] where transTime between @fromDate and @toDate ) as Temp ) order by archiveDate, refNo";
            query[2].args = [];
            query[2].args.push(
                { "c": "fromDate", "t": TYPES.DateTime, "v": sqlFromDates },
                { "c": "toDate", "t": TYPES.DateTime, "v": sqlToDates }
            );

            query[1].sql = "select * from tenderDiscountArchive where void = 0 and refNo in (select refNo from transactionArchive where archiveDate in  (select distinct archiveDate from  (select transTime, archiveDate from [transactionArchive] where transTime between @fromDate and @toDate ) as Temp ) ) order by refNo";
            query[1].args = [];
            query[1].args.push(
                { "c": "fromDate", "t": TYPES.DateTime, "v": sqlFromDates },
                { "c": "toDate", "t": TYPES.DateTime, "v": sqlToDates }
            );

            query[0].sql = "select * from tenderPaymentArchive where void = 0 and refNo in (select refNo from transactionArchive where archiveDate in  (select distinct archiveDate from  (select transTime, archiveDate from [transactionArchive] where transTime between @fromDate and @toDate  ) as Temp ) ) order by refNo";
            query[0].args = [];
            query[0].args.push(
                { "c": "fromDate", "t": TYPES.DateTime, "v": sqlFromDates },
                { "c": "toDate", "t": TYPES.DateTime, "v": sqlToDates }
            );
        }
        function today() {
            argsdate = self.date;

            query.forEach(function (sql) {
                sql.args[0].v = argsdate;
            })
        }
        function specificDay() {
            self.date = argsdate;
            query.forEach(function (sql) {
                sql.args[0].v = self.date;
            })
        }

        switch (mode) {
            case "pos": {
                if (args.year) {
                    if (args.toYear) {
                        toYear();
                    } else {
                        if (self.date == argsdate) {
                            today();
                        } else {
                            specificDay();
                        }
                    }
                } else {
                    today();
                }

                break;
            }
            case "cloud": {
                if (args.startDate == args.endDate) args.endDate = null;
                if (args.startDate) {
                    var argsdate = args.startDate;

                    if (args.endDate) {
                        toYear();
                    }
                    else {
                        if (self.date == argsdate) {
                            today();
                        } else {
                            specificDay();
                        }
                    }
                }
                break;
            }
        }

        if (_food.config != undefined && _food.config.dailyReport != undefined) {
            if (_food.config.dailyReport.timeslot) {
                query.push({ sql: "select * from timeslot order by timeStart;" })
                this.dailyReport.indexOfTimeslot = query.length - 1;
            }
        }


        return query;
    }

    dailyArchiveReport.prototype.getResult = function (result) {
        var tender = _food.tender;

        var title = "每日清機報表"

        if (languageCode == "003") {
            title = "Daily Clearnce Report";
        }

        var transaction = result[2];
        var TempDiscount = result[1];
        var discount = [];

        var TempPayment = result[0];
        var payment = [];
        var cleanTime = null;


        var cleanTimeList = result[3];

        var isExist = false;

        cleanTimeList.forEach(function (item) {
            item.archiveDate = moment(item.archiveDate).format("YYYY-MM-DD HH:mm:ss");
            if (item.archiveDate == args.selectCleanTime) isExist = true;
        });



        if (result[3].length != 0)
            cleanTime = result[3][0].archiveDate;


        if (args.selectCleanTime != null) {

            cleanTime = args.selectCleanTime;

            if (!isExist) {
                if (result[3].length > 0)
                    cleanTime = result[3][0].archiveDate;
            }
        }

        if (args.multiTills != undefined && args.multiTills.length > 0) {
            transaction = transaction.filter(function (item) {
                return moment(item.archiveDate).format("YYYYMMDDHHmmss") == moment(cleanTime).format("YYYYMMDDHHmmss") && args.multiTills.indexOf(item.till) != -1;
            });

        } else {
            transaction = transaction.filter(function (item) {
                return moment(item.archiveDate).format("YYYYMMDDHHmmss") == moment(cleanTime).format("YYYYMMDDHHmmss") && item.till == args.till;
            });
        }





        transaction.forEach(function (item) {

            var PayTempObj = TempPayment.filter(function (pay) {
                return pay.refNo == item.refNo;
            });
            PayTempObj.forEach(function (p) {
                payment.push(p);
            })

            var DisTempObj = TempDiscount.filter(function (dis) {
                return dis.refNo == item.refNo;
            });

            DisTempObj.forEach(function (d) {
                discount.push(d);
            })
        });

        var obj = this.dailyReport.getDailyReportObj(transaction, payment, discount, null, tender, title, result);

        obj.REPORT_DATE = this.date;
        obj.cleanTimeList = cleanTimeList;

        if (cleanTime != null)
            obj.cleanTime = moment(cleanTime).format("YYYY-MM-DD HH:mm:ss");
        else
            obj.cleanTime = null;

        return obj;
    }

    dailyArchiveReport.prototype.getCallbackResult = function (callback) {
        var self = this;
        var DB = new database();
        DB.executeStatement(self.getQuery(), (err, result) => {
            var obj = self.getResult(result);
            callback(obj);
            if (args.isPrint) {
                self.printReport(obj);
            }
        })
    }

    dailyArchiveReport.prototype.printReport = function (obj) {

        this.dailyReport.printReport(obj);
    }
}

function dailyItemReportByCategory(args, mode, DM) {

    this.date = moment().format("DD/MM/YYYY");

    dailyItemReportByCategory.prototype.getQuery = function () {
        var shopCode = args.shop;

        if (shopCode != null) {

            // shopCode = "%" + shopCode + "-%";
            shopCode = shopCode + "-%";
        }
        else
            shopCode = "%"

        var tempGlobalSql = GetGlobalTransactionStatement.viewSql;

        var query = [
            { "sql": "SELECT ConVert(varchar, c.categoryId) as categoryId, c.name1 FROM [category] as c UNION  select optionGroupCode as categoryId, name1 from optionGroup", args: [] },
            {
                "sql": "  SELECT  o.[optionCode] as itemId, o.name1, og.name1 as CategoryName , og.optionGroupCode as categoryId \
        FROM [option] as o \
        inner join optionGroupOption as ogo on ogo.optionCode = o.optionCode \
        inner join optionGroup as og on og.optionGroupCode = ogo.optionGroupCode \
        union  \
        SELECT i.itemid , i.name1, c.name1 as CategoryName, Convert(varchar, C.categoryId) from item as i  inner join itemCategory as ic on ic.itemId = i.itemId \
        inner join category as c on c.categoryId = ic.categoryId", "args": []
            },
           { "sql": tempGlobalSql, "args": [] },
           { "sql": "select itemCode, optionGroupCode from [itemOptionGroup]" }

        ];

        var argsdate = ""

        if (mode == "pos") argsdate = pad(args.day, 2) + "/" + pad(args.month, 2) + "/" + args.year;
        else argsdate = args.startDate;

        var self = this;

        function toYear() {
            var argsTodate = "";
            var sqlFromDates = "";
            var sqlToDates = "";

            if (mode == "pos") {
                argsTodate = pad(args.toDay, 2) + "/" + pad(args.toMonth, 2) + "/" + args.toYear;
                sqlFromDates = new Date(args.year, parseInt(args.month) - 1, args.day);
                sqlToDates = new Date(args.toYear, parseInt(args.toMonth) - 1, parseInt(args.toDay) + 1);
            } else {
                argsTodate = args.endDate;
                sqlFromDates = args.startDate;
                sqlToDates = args.endDate;
                sqlFromDates = moment(sqlFromDates, 'DD/MM/YYYY').toDate();
                sqlToDates = moment(sqlToDates, 'DD/MM/YYYY').add(1, 'day').toDate();
            }

            self.date = argsdate + ' - ' + argsTodate;

            query[2].sql = query[2].sql.replace("##conditionWhere1##", " where transTime between @fromDate and @toDate and refNo like @shopCode")
            query[2].sql = query[2].sql.replace("##conditionWhere2##", " where transTime between @fromDate and @toDate and refNo like @shopCode")
            query[2].args = [];

            query[2].args.push({ "c": "fromDate", "t": TYPES.DateTime, "v": sqlFromDates }, { "c": "toDate", "t": TYPES.DateTime, "v": sqlToDates });
            query[2].args.push({ "c": "shopCode", "t": TYPES.VarChar, "v": shopCode });

        }
        function today() {
            argsdate = self.date;
            query[2].sql = query[2].sql.replace("##conditionWhere1##", " where refNo like @shopCode")
            query[2].sql = query[2].sql.replace("##conditionWhere2##", " where  convert(varchar(10), transTime, 103) = @date  and t.refNo like @shopCode ")
            query[2].args = [];
            query[2].args.push({ "c": "date", "t": TYPES.VarChar, "v": argsdate });
            query[2].args.push({ "c": "shopCode", "t": TYPES.VarChar, "v": shopCode });
        }
        function specificDay() {

            self.date = argsdate;

            query[2].sql = query[2].sql.replace("##conditionWhere1##", " where  convert(varchar(10), transTime, 103) = @date and refNo like @shopCode  ")
            query[2].sql = query[2].sql.replace("##conditionWhere2##", " where  convert(varchar(10), transTime, 103) = @date and refNo like @shopCode  ")
            query[2].args = [];

            query[2].args.push({ "c": "date", "t": TYPES.VarChar, "v": argsdate });
            query[2].args.push({ "c": "shopCode", "t": TYPES.VarChar, "v": shopCode });

        }


        switch (mode) {
            case "pos": {
                if (args.year) {
                    if (args.toYear) {
                        toYear();
                    } else {
                        if (self.date == argsdate) {
                            today();
                        } else {
                            specificDay();
                        }
                    }
                } else {
                    today();
                }

                break;
            }
            case "cloud": {
                if (args.startDate == args.endDate) args.endDate = null;
                if (args.startDate) {
                    var argsdate = args.startDate;

                    if (args.endDate) {
                        toYear();
                    }
                    else {
                        if (self.date == argsdate) {
                            today();
                        } else {
                            specificDay();
                        }
                    }
                }
                break;
            }
        }

        return query;
    }

    dailyItemReportByCategory.prototype.getResult = function (result) {

        var obj = { "Category": [], "REPORT_DATE": moment().format("DD/MM/YYYY"), "AllQty": 0 };
        obj.REPORT_DATE = this.date;

        var CategoryList = result[0];
        var itemOptionGroupList = result[3];

        var CategoryId = [];

        var transaction = result[2];
        var packageObject = new Package();
        PackageList = packageObject.GroupPackage(transaction, itemOptionGroupList);

        var TempFood = new Food();
        PackageList.forEach(function (packageItem) {
            TempFood.addFoodByCategory(packageItem);
            packageItem.options.forEach(function (option) {
                TempFood.addFoodByCategory(option);
            })
        });

        var categoryObject = new Category();
        categoryObject.GroupCategory(CategoryList, TempFood.FoodList);
        categoryObject.CategoryList = categoryObject.CategoryList.filter(function (cate) {
            return cate.CateQty > 0;
        });

        obj.Category = categoryObject.CategoryList;
        categoryObject.CategoryList.forEach(function (t) {
            obj.AllQty += t.CateQty;
        })

        return obj;

    }

    dailyItemReportByCategory.prototype.getCallbackResult = function (callback) {

        var self = this;
        var DB = new database();
        DB.executeStatement(self.getQuery(), (err, result) => {
            var obj = self.getResult(result);
            callback(obj);
            /*if (args.isPrint) {
                self.printReport(obj);
            }*/
        })

    }

}

function dailyVoidECRReport(args, mode, DM) {

    this.date = moment().format("DD/MM/YYYY");

    dailyVoidECRReport.prototype.getQuery = function () {
        var shopCode = args.shop;

        if (shopCode != null) {

            // shopCode = "%" + shopCode + "-%";
            shopCode = shopCode + "-%";
        }
        else
            shopCode = "%"

        var sql = "select * from [ecrTransaction] where Archive = 0 and ( Command_Code = '3' or Command_Code = 'd') " + (args.till ? (" and Till = '" + args.till) + "'" : "");

        var query = { "sql": sql, "args": [] }

        var argsdate = ""

        if (mode == "pos") argsdate = pad(args.day, 2) + "/" + pad(args.month, 2) + "/" + args.year;
        else argsdate = args.startDate;

        var self = this;

        function toYear() {
            var argsTodate = "";
            var sqlFromDates = "";
            var sqlToDates = "";

            if (mode == "pos") {
                argsTodate = pad(args.toDay, 2) + "/" + pad(args.toMonth, 2) + "/" + args.toYear;
                sqlFromDates = new Date(args.year, parseInt(args.month) - 1, args.day);
                sqlToDates = new Date(args.toYear, parseInt(args.toMonth) - 1, parseInt(args.toDay) + 1);
            } else {
                argsTodate = args.endDate;
                sqlFromDates = args.startDate;
                sqlToDates = args.endDate;
                sqlFromDates = moment(sqlFromDates, 'DD/MM/YYYY').toDate();
                sqlToDates = moment(sqlToDates, 'DD/MM/YYYY').add(1, 'day').toDate();
            }

            self.date = argsdate + ' - ' + argsTodate;

            query = [{ "sql": "select * from [ecrTransaction] where Archive = 0 and ( Command_Code = '3' or Command_Code = 'd')  and  [DateTime] between @fromDate and @toDate ;", "args": [{ "c": "fromDate", "t": TYPES.DateTime, "v": sqlFromDates }, { "c": "toDate", "t": TYPES.DateTime, "v": sqlToDates }] }
            ];

        }
        function today() {
            argsdate = self.date;
            query = [{ "sql": "select * from [ecrTransaction] where Archive = 0 and ( Command_Code = '3' or Command_Code = 'd')  and convert(char(10), [DateTime] , 103) = @date ;", "args": [{ "c": "date", "t": TYPES.VarChar, "v": argsdate }] }
            ];
        }
        function specificDay() {

            self.date = argsdate;

            query = [{ "sql": "select * from [ecrTransaction] where Archive = 0 and ( Command_Code = '3' or Command_Code = 'd')  and convert(char(10), [DateTime] , 103) = @date ;", "args": [{ "c": "date", "t": TYPES.VarChar, "v": argsdate }] }
            ];

        }


        switch (mode) {
            case "pos": {
                if (args.year) {
                    if (args.toYear) {
                        toYear();
                    } else {
                        if (self.date == argsdate) {
                            today();
                        } else {
                            specificDay();
                        }
                    }
                } else {
                    today();
                }

                break;
            }
            case "cloud": {
                if (args.startDate == args.endDate) args.endDate = null;
                if (args.startDate) {
                    var argsdate = args.startDate;

                    if (args.endDate) {
                        toYear();
                    }
                    else {
                        if (self.date == argsdate) {
                            today();
                        } else {
                            specificDay();
                        }
                    }
                }
                break;
            }
        }

        return query;
    }

    dailyVoidECRReport.prototype.getResult = function (result) {
        var obj = {};
        obj.voidList = result;
        obj.REPORT_DATE = this.date;
        return obj;
    }

    dailyVoidECRReport.prototype.getCallbackResult = function (callback) {

        var self = this;
        var DB = new database();
        DB.executeStatement(self.getQuery(), (err, result) => {
            var obj = self.getResult(result);
            callback(obj);
            /*if (args.isPrint) {
                self.printReport(obj);
            }*/
        })
    }

}

function dailyWatchServiceReport(args, mode, DM) {

    this.date = moment().format("DD/MM/YYYY");

    dailyWatchServiceReport.prototype.getQuery = function () {
        var shopCode = args.shop;

        if (shopCode != null) {

            // shopCode = "%" + shopCode + "-%";
            shopCode = shopCode + "-%";
        }
        else
            shopCode = "%"



        var argsdate = ""

        if (mode == "pos") argsdate = pad(args.day, 2) + "/" + pad(args.month, 2) + "/" + args.year;
        else argsdate = args.startDate;

        var self = this;

        var query = [{ "sql": "SELECT * from [message] as m, [staff] as s where s.staffId = m.staffId and convert(char(10), [date], 103) = @date order by [date] desc", "args": [{ "c": "date", "t": TYPES.VarChar, "v": self.date }] }];

        function toYear() {
            var argsTodate = "";
            var sqlFromDates = "";
            var sqlToDates = "";

            if (mode == "pos") {
                argsTodate = pad(args.toDay, 2) + "/" + pad(args.toMonth, 2) + "/" + args.toYear;
                sqlFromDates = new Date(args.year, parseInt(args.month) - 1, args.day);
                sqlToDates = new Date(args.toYear, parseInt(args.toMonth) - 1, parseInt(args.toDay) + 1);
            } else {
                argsTodate = args.endDate;
                sqlFromDates = args.startDate;
                sqlToDates = args.endDate;
                sqlFromDates = moment(sqlFromDates, 'DD/MM/YYYY').toDate();
                sqlToDates = moment(sqlToDates, 'DD/MM/YYYY').add(1, 'day').toDate();
            }

            self.date = argsdate + ' - ' + argsTodate;

            query = [{ "sql": "SELECT * from [message] as m, [staff] as s where s.staffId = m.staffId and [date] between @fromDate and @toDate order by [date] desc", "args": [{ "c": "fromDate", "t": TYPES.DateTime, "v": sqlFromDates }, { "c": "toDate", "t": TYPES.DateTime, "v": sqlToDates }] }];

        }
        function today() {
            argsdate = self.date;
            query = [{ "sql": "SELECT * from [message] as m, [staff] as s where s.staffId = m.staffId and convert(char(10), [date], 103) = @date order by [date] desc", "args": [{ "c": "date", "t": TYPES.VarChar, "v": argsdate }] }];
        }
        function specificDay() {

            self.date = argsdate;

            query = [{ "sql": "SELECT * from [message] as m, [staff] as s where s.staffId = m.staffId and convert(char(10), [date], 103) = @date order by [date] desc ", "args": [{ "c": "date", "t": TYPES.VarChar, "v": argsdate }] }];

        }


        switch (mode) {
            case "pos": {
                if (args.year) {
                    if (args.toYear) {
                        toYear();
                    } else {
                        if (self.date == argsdate) {
                            today();
                        } else {
                            specificDay();
                        }
                    }
                } else {
                    today();
                }

                break;
            }
            case "cloud": {
                if (args.startDate == args.endDate) args.endDate = null;
                if (args.startDate) {
                    var argsdate = args.startDate;

                    if (args.endDate) {
                        toYear();
                    }
                    else {
                        if (self.date == argsdate) {
                            today();
                        } else {
                            specificDay();
                        }
                    }
                }
                break;
            }
        }

        return query;
    }

    dailyWatchServiceReport.prototype.getResult = function (result) {
        var obj = { "message": [], "REPORT_DATE": '' };

        obj.REPORT_DATE = this.date;

        obj.message = result[0];

        obj.message.forEach(function (m) {
            var resultUpdate = moment.utc(moment(m.updateDate, 'YYYY-MM-DD HH:mm:ss').diff(moment(m.date, 'YYYY-MM-DD HH:mm:ss'))).format("HH:mm:ss");
            console.log(result);
            if (resultUpdate == "Invalid date") resultUpdate = "";

            var resultDelete = moment.utc(moment(m.deleteDate, 'YYYY-MM-DD HH:mm:ss').diff(moment(m.updateDate, 'YYYY-MM-DD HH:mm:ss'))).format("HH:mm:ss");
            console.log(result);
            if (resultDelete == "Invalid date") resultDelete = "";

            m.updateDate = resultUpdate;
            m.deleteDate = resultDelete;
        })

        return obj;

    }

    dailyWatchServiceReport.prototype.getCallbackResult = function (callback) {

        var self = this;
        var DB = new database();
        DB.executeStatement(self.getQuery(), (err, result) => {
            var obj = self.getResult(result);
            callback(obj);
            /*if (args.isPrint) {
                self.printReport(obj);
            }*/
        })
    }
}

function dailyPaymentCommissionReport(args, mode, DM) {

    this.date = moment().format("DD/MM/YYYY");

    dailyPaymentCommissionReport.prototype.getQuery = function () {

        var shopCode = args.shop;

        if (shopCode != null) {

            // shopCode = "%" + shopCode + "-%";
            shopCode = shopCode + "-%";
        }
        else
            shopCode = "%"

        var query = [
            { "sql": "select * from (select t.startTime, t.refNo, t.price, t.serviceCharge, t.remainings, tda.discountValue from [transaction] as t left join [tenderDiscount] as tda on tda.refNo = t.refNo and tda.void = 0 where t.void = 0 ##conditionWhere1##  union select t.startTime, t.refNo, t.price, t.serviceCharge, t.remainings, tda.discountValue from [transactionArchive] as t left join [tenderDiscountArchive] as tda on tda.refNo = t.refNo and tda.void = 0 where t.void = 0 ##conditionWhere2## ) as templ order by refNo", "args": [] },
            { "sql": "select * from (select paymentType, seq, paymentValue, refNo, paymentDate from tenderPayment where void = 0 and refNo in (select refNo from [transaction] as t where t.void = 0 ##conditionWhere1## ) union select  paymentType, seq, paymentValue, refNo, paymentDate from tenderPaymentArchive where void = 0  and refNo in (select refNo from [transactionArchive] as t where t.void = 0 ##conditionWhere2## )) as templ order by refNo", "args": [] }

        ];

        var argsdate = ""

        if (mode == "pos") argsdate = pad(args.day, 2) + "/" + pad(args.month, 2) + "/" + args.year;
        else argsdate = args.startDate;

        var self = this;

        function toYear() {
            var argsTodate = "";
            var sqlFromDates = "";
            var sqlToDates = "";

            if (mode == "pos") {
                argsTodate = pad(args.toDay, 2) + "/" + pad(args.toMonth, 2) + "/" + args.toYear;
                sqlFromDates = new Date(args.year, parseInt(args.month) - 1, args.day);
                sqlToDates = new Date(args.toYear, parseInt(args.toMonth) - 1, parseInt(args.toDay) + 1);
            } else {
                argsTodate = args.endDate;
                sqlFromDates = args.startDate;
                sqlToDates = args.endDate;
                sqlFromDates = moment(sqlFromDates, 'DD/MM/YYYY').toDate();
                sqlToDates = moment(sqlToDates, 'DD/MM/YYYY').add(1, 'day').toDate();
            }

            self.date = argsdate + ' - ' + argsTodate;
            query[0].sql = query[0].sql.replace("##conditionWhere1##", " and t.startTime between @fromDate and @toDate and t.refNo like @shopCode  ")
            query[0].sql = query[0].sql.replace("##conditionWhere2##", " and t.startTime between @fromDate and @toDate and t.refNo like @shopCode ")
            query[0].args = [];
            query[0].args.push({ "c": "fromDate", "t": TYPES.DateTime, "v": sqlFromDates }, { "c": "toDate", "t": TYPES.DateTime, "v": sqlToDates });
            query[0].args.push({ "c": "shopCode", "t": TYPES.VarChar, "v": shopCode });

            query[1].sql = query[1].sql.replace("##conditionWhere1##", " and t.startTime between @fromDate and @toDate  and t.refNo like @shopCode ")
            query[1].sql = query[1].sql.replace("##conditionWhere2##", " and t.startTime between @fromDate and @toDate  and t.refNo like @shopCode ")
            query[1].args = [];
            query[1].args.push({ "c": "fromDate", "t": TYPES.DateTime, "v": sqlFromDates }, { "c": "toDate", "t": TYPES.DateTime, "v": sqlToDates });
            query[1].args.push({ "c": "shopCode", "t": TYPES.VarChar, "v": shopCode });

        }
        function today() {
            argsdate = self.date;
            query[0].sql = query[0].sql.replace("##conditionWhere1##", " and t.refNo like @shopCode  ")
            query[0].sql = query[0].sql.replace("##conditionWhere2##", " and  convert(varchar(10), t.startTime, 103) = @date and t.refNo like @shopCode  ")
            query[0].args = [];
            query[0].args.push({ "c": "date", "t": TYPES.VarChar, "v": argsdate });
            query[0].args.push({ "c": "shopCode", "t": TYPES.VarChar, "v": shopCode });

            query[1].sql = query[1].sql.replace("##conditionWhere1##", " and t.refNo like @shopCode ")
            query[1].sql = query[1].sql.replace("##conditionWhere2##", " and  convert(varchar(10), t.startTime, 103) = @date  and t.refNo like @shopCode   ")
            query[1].args = [];
            query[1].args.push({ "c": "date", "t": TYPES.VarChar, "v": argsdate });
            query[1].args.push({ "c": "shopCode", "t": TYPES.VarChar, "v": shopCode });
        }
        function specificDay() {

            self.date = argsdate;

            query[0].sql = query[0].sql.replace("##conditionWhere1##", " and  convert(varchar(10), t.startTime, 103) = @date and t.refNo like @shopCode   ")
            query[0].sql = query[0].sql.replace("##conditionWhere2##", " and  convert(varchar(10), t.startTime, 103) = @date and t.refNo like @shopCode   ")
            query[0].args = [];
            query[0].args.push({ "c": "date", "t": TYPES.VarChar, "v": argsdate });
            query[0].args.push({ "c": "shopCode", "t": TYPES.VarChar, "v": shopCode });

            query[1].sql = query[1].sql.replace("##conditionWhere1##", " and  convert(varchar(10), t.startTime, 103) = @date and t.refNo like @shopCode   ")
            query[1].sql = query[1].sql.replace("##conditionWhere2##", " and  convert(varchar(10), t.startTime, 103) = @date and t.refNo like @shopCode   ")
            query[1].args = [];
            query[1].args.push({ "c": "date", "t": TYPES.VarChar, "v": argsdate });
            query[1].args.push({ "c": "shopCode", "t": TYPES.VarChar, "v": shopCode });

        }


        switch (mode) {
            case "pos": {
                if (args.year) {
                    if (args.toYear) {
                        toYear();
                    } else {
                        if (self.date == argsdate) {
                            today();
                        } else {
                            specificDay();
                        }
                    }
                } else {
                    today();
                }

                break;
            }
            case "cloud": {
                if (args.startDate == args.endDate) args.endDate = null;
                if (args.startDate) {
                    var argsdate = args.startDate;

                    if (args.endDate) {
                        toYear();
                    }
                    else {
                        if (self.date == argsdate) {
                            today();
                        } else {
                            specificDay();
                        }
                    }
                }
                break;
            }
        }

        return query;
    }

    dailyPaymentCommissionReport.prototype.getResult = function (result) {

        var obj = { commissionList: [] };

        var transactionList = result[0];
        var paymentMethodList = result[1];
        var percentageList = args.percentage;
        //console.log(JSON.stringify(transactionList));
        //console.log(JSON.stringify(paymentMethodList));

        var CommissionList = [];
        var CommissionKeyList = [];

        function addPaymentCommission(obj, paymentCode) {
            var target = null;
            if (CommissionKeyList.indexOf(paymentCode) == -1) target = null;
            else target = CommissionList[CommissionKeyList.indexOf(paymentCode)];

            if (target) {
                target.billAmount += obj.billAmount;
                target.paidAmount += obj.paidAmount;
                target.sales += obj.sales;
                target.cardServiceCharge += obj.cardServiceCharge;
                target.netSales += obj.netSales;
                target.tips += obj.tips;
            } else {
                target = {};
                target.paymentType = paymentCode;
                target.percentage = obj.percentage;

                target.billAmount = obj.billAmount;
                target.paidAmount = obj.paidAmount;
                target.sales = obj.sales;
                target.cardServiceCharge = obj.cardServiceCharge;
                target.netSales = obj.netSales;
                target.tips = obj.tips;
                CommissionKeyList.push(paymentCode);
                CommissionList[CommissionKeyList.length - 1] = target;
            }

        }

        function calculator(billObj, paymentObj, billPrice) {
            var obj = billObj;
            var p = paymentObj;
            obj.tips = 0;

            var oriBillPrice = billPrice;
            billPrice = billPrice - obj.billPrice;
            //if (p.paymentType == 'cash') console.log('billPrice = billPrice - obj.billPrice', billPrice);

            //if (p.paymentType != 'cash') {
            //    if (billPrice < 0) obj.tips = Math.abs(billPrice);
            //}

            if (billPrice < 0) {
                if (p.paymentType != 'cash') obj.tips = Math.abs(billPrice);
                else {
                    obj.billPrice = oriBillPrice;
                }
            }

            obj.billAmount = obj.billPrice;
            obj.sales = obj.billAmount - obj.tips;
            obj.cardServiceCharge = parseFloat((obj.billAmount * obj.percentage) / 100);
            if (obj.percentage == 0) obj.cardServiceCharge = 0;
            obj.paidAmount = obj.billAmount - obj.cardServiceCharge;
            obj.netSales = obj.paidAmount - obj.tips;
            addPaymentCommission(obj, p.paymentType);
            return billPrice;
        }

        function getPercentage(code) {
            var temp = percentageList.filter(function (p) {
                return p.code == code;
            });

            if (temp.length != 0) return parseFloat(temp[0].value).toFixed(2);
            else return 0;
        }

        transactionList.forEach(function (t, offset) {
            var MatchPaymentList = paymentMethodList.filter(function (p) {
                return p.refNo == t.refNo;
            });

            //console.log(offset);
            //console.log("________________________");
            //console.log(JSON.stringify(MatchPaymentList));
            //console.log("===========================================")

            if (MatchPaymentList.length == 1) {

                var obj = {};
                var priceBill = t.price + t.serviceCharge + t.remainings - (t.discountValue ? t.discountValue : 0);
                if (MatchPaymentList[0].paymentType == 'cash') {
                    //console.log(MatchPaymentList[0].refNo);
                    //console.log(MatchPaymentList[0]);
                    //console.log('priceBill', priceBill);
                }
                obj.billPrice = MatchPaymentList[0].paymentValue;
                obj.percentage = getPercentage(MatchPaymentList[0].paymentType);
                calculator(obj, MatchPaymentList[0], priceBill);
            }
            else if (MatchPaymentList.length > 1) {
                var billPrice = t.price + t.serviceCharge + t.remainings - (t.discountValue ? t.discountValue : 0); //78
                var commissionObjList = [];
                //console.log('^^^billPrice1:', billPrice);

                //var isHaveCashMethod = MatchPaymentList.filter(function (p) {
                //    return p.paymentType == 'cash';
                //});

                //isHaveCashMethod.length != 0
                if (true) {
                    MatchPaymentList = MatchPaymentList.sort(function (a, b) {
                        if (a.paymentType == 'cash') return 1;
                        else return 0;
                    });

                    //console.log('isHaveCashMethod', MatchPaymentList);
                }

                MatchPaymentList.forEach(function (p, i) {
                    var obj = {}

                    if (p.paymentType != 'cash') {
                        obj = {}
                        obj.billPrice = p.paymentValue; //50
                        obj.percentage = getPercentage(p.paymentType);
                        billPrice = calculator(obj, p, billPrice);

                    } else {
                        if (i == MatchPaymentList.length - 1) {
                            obj = {}
                            obj.billPrice = billPrice;
                            obj.percentage = getPercentage(p.paymentType);
                            billPrice = calculator(obj, p, billPrice);
                        } else {
                            obj = {}
                            obj.billPrice = p.paymentValue;
                            obj.percentage = getPercentage(p.paymentType);
                            billPrice = calculator(obj, p, billPrice);
                        }

                    }

                    //console.log('billPrice:', billPrice);
                    //console.log("~~~~~~~~~~~~")


                })
            }

            /**obj.billAmount(單據金額) = billPrice + tips**/
            /**obj.sales(營業額) = billPrice - tips**/
            /**obj.percentage = 0**/
            /**obj.cardServiceCharge(卡服務費) = obj.billAmount * obj.percentage**/
            /**obj.paidAmount(實收金額) = obj.billAmount - obj.cardServiceCharge**/
            /**obj.netSales(淨金額) = obj.paidAmount - tips **/
            /**
            百分比	單據金額	    實收金額	            營業額	        卡服務費	            淨金額	    貼士
            0	    22688	    22688	            22688	        0	                22688	    0
            1.70%	26616.50 	26164.02 	        26171.50 	    452.48 	            25719.02 	445
            1.70%	21231.30 	20870.37 	        20904.30 	    360.93 	            20543.37 	327
            2.95%	17525.50 	17008.50 	        17369.00 	    517.00 	            16852.00 	156.5
            0	    1133.00 	1133.00 	        1133.00 	    0.00 	            1133.00 	0
                                單據金額 - 卡服務費	單據金額 - 貼士	單據金額 x 百分比	    實收金額 - 貼士	
            **/


        });


        obj.commissionList = CommissionList;
        return obj;
    }

    dailyPaymentCommissionReport.prototype.getCallbackResult = function (callback) {

        var self = this;
        var DB = new database();
        DB.executeStatement(self.getQuery(), (err, result) => {
            callback(self.getResult(result));
        })

    }
}

function dailyBillDetailReport(args, mode, DM) {

    this.date = moment().format("DD/MM/YYYY");

    this.dailyReport = new dailyReport(args, mode, DM);

    dailyBillDetailReport.prototype.getQuery = function () {
        var shopCode = args.shop;

        if (shopCode != null) {

            // shopCode = "%" + shopCode + "-%";
            shopCode = shopCode + "-%";
        }
        else
            shopCode = "%"

        var tempGlobalSql = GetGlobalTransactionStatement.viewSql;

        var query = [
            {
                "sql": "select * from (select [tableCode],[tableSubCode],[pax],[staffId],[startTime],[refNo],[transTime] ,[payAmount],[price],[serviceCharge],[remainings],[void] from [transaction] as td where void = 0 ##conditionWhere1##  union  select [tableCode],[tableSubCode],[pax],[staffId],[startTime],[refNo],[transTime] ,[payAmount],[price],[serviceCharge],[remainings],[void] from [transactionArchive] as td where void = 0 ##conditionWhere2## ) as [transction] order by refNo;", "args": []

            },
            {
                "sql": "select * from (select * from [transactionD] as td where td.refNo in (select refNo from [transaction] where void = 0 ##conditionWhere1## ) union  select * from transactionDArchive as td where td.refNo in (select refNo from [transactionArchive] where void = 0 ##conditionWhere2##  ) ) as transactionD order by refNo,round,seq, type;", "args": []
            },
            {
                "sql": "select * from (select * from [tenderDiscount] where refNo in (select refNo from [transaction] where void = 0  ##conditionWhere1## ) and void = 0 union  select * from [tenderDiscountArchive] as td where td.refNo in (select refNo from [transactionArchive] where void = 0 ##conditionWhere2## ) and void = 0) as tenderDiscount order by refNo;", "args": []


            },
            {
                "sql": "select * from (select * from [tenderPayment] as td where td.refNo in (select refNo from [transaction] where void = 0  ##conditionWhere1## ) and void = 0 union  select * from [tenderPaymentArchive] as td where td.refNo in (select refNo from [transactionArchive] where void = 0  ##conditionWhere2## ) and void = 0 ) as tenderPayment order by refNo;", "args": []


            }

        ]

        var argsdate = ""

        if (mode == "pos") argsdate = pad(args.day, 2) + "/" + pad(args.month, 2) + "/" + args.year;
        else argsdate = args.startDate;

        var self = this;

        function toYear() {
            var argsTodate = "";
            var sqlFromDates = "";
            var sqlToDates = "";

            if (mode == "pos") {
                argsTodate = pad(args.toDay, 2) + "/" + pad(args.toMonth, 2) + "/" + args.toYear;
                sqlFromDates = new Date(args.year, parseInt(args.month) - 1, args.day);
                sqlToDates = new Date(args.toYear, parseInt(args.toMonth) - 1, parseInt(args.toDay) + 1);
            } else {
                argsTodate = args.endDate;
                sqlFromDates = args.startDate;
                sqlToDates = args.endDate;
                sqlFromDates = moment(sqlFromDates, 'DD/MM/YYYY').toDate();
                sqlToDates = moment(sqlToDates, 'DD/MM/YYYY').add(1, 'day').toDate();
            }

            self.date = argsdate + ' - ' + argsTodate;

            for (var i = 0; i < 4; i++) {
                query[i].sql = query[i].sql.replace("##conditionWhere1##", " and  startTime between @fromDate and @toDate and refNo like @shopCode  ")
                query[i].sql = query[i].sql.replace("##conditionWhere2##", " and  startTime between @fromDate and @toDate and refNo like @shopCode  ")
                query[i].args.push({ "c": "fromDate", "t": TYPES.DateTime, "v": sqlFromDates }, { "c": "toDate", "t": TYPES.DateTime, "v": sqlToDates });
                query[i].args.push({ "c": "shopCode", "t": TYPES.VarChar, "v": shopCode });
            }

        }
        function today() {
            argsdate = self.date;
            for (var i = 0; i < 4; i++) {
                query[i].sql = query[i].sql.replace("##conditionWhere1##", " and refNo like @shopCode   ")
                query[i].sql = query[i].sql.replace("##conditionWhere2##", " and  convert(varchar(10), startTime, 103) = @date and refNo like @shopCode ")
                query[i].args.push({ "c": "date", "t": TYPES.VarChar, "v": argsdate });
                query[i].args.push({ "c": "shopCode", "t": TYPES.VarChar, "v": shopCode });
            }
        }
        function specificDay() {

            self.date = argsdate;

            for (var i = 0; i < 4; i++) {
                query[i].sql = query[i].sql.replace("##conditionWhere1##", " and  convert(varchar(10), startTime, 103) = @date  and refNo like @shopCode   ")
                query[i].sql = query[i].sql.replace("##conditionWhere2##", " and  convert(varchar(10), startTime, 103) = @date   and refNo like @shopCode  ")
                query[i].args.push({ "c": "date", "t": TYPES.VarChar, "v": argsdate });
                query[i].args.push({ "c": "shopCode", "t": TYPES.VarChar, "v": shopCode });
            }
        }

        switch (mode) {
            case "pos": {
                if (args.year) {
                    if (args.toYear) {
                        toYear();
                    } else {
                        if (self.date == argsdate) {
                            today();
                        } else {
                            specificDay();
                        }
                    }
                } else {
                    today();
                }

                break;
            }
            case "cloud": {
                if (args.startDate == args.endDate) args.endDate = null;
                if (args.startDate) {
                    var argsdate = args.startDate;

                    if (args.endDate) {
                        toYear();
                    }
                    else {
                        if (self.date == argsdate) {
                            today();
                        } else {
                            specificDay();
                        }
                    }
                }
                break;
            }
        }

        return query;
    }

    dailyBillDetailReport.prototype.getResult = function (result) {
        //for (var i = 0; i < transaction[2].length; i++) {
        //    var _o = transaction[2][i];
        //    var payMsg = _o.paymentType;
        //    var tenderObj = this.tender.filter((t) => { return t.tenderCode == _o.paymentType })[0];

        //    _obj.payment.push({ "paymentType": tenderObj.tenderName1, "paymentValue": S("$" + _o.paymentValue.format(1, 3)).padLeft(10).s, "remark": _o.remark, "paymentCode": _o.paymentType })
        //}

        var tender = _food.tender;
        var discount = _food.discount;
        var self = this;
        var obj = {};
        var objList = [];
        result[0].forEach(function (t) {
            var _obj = {}
            _obj.transaction = { refNo: t.refNo, tableCode: t.tableCode, price: t.price, serviceCharge: t.serviceCharge, remainings: t.remainings, startTime: t.startTime };
            if (t.tableSubCode) {
                _obj.transaction.tableCode += "-" + t.tableSubCode;
            }

            var transactionD = result[1].filter(function (d) {
                return d.refNo == t.refNo;
            });

            var discountList = result[2].filter(function (d) {
                return d.refNo == t.refNo;
            });

            var tenderList = result[3].filter(function (tender) {
                return tender.refNo == t.refNo;
            });

            _obj.transaction.sales = t.price + t.serviceCharge + t.remainings - 0;

            _obj.payment = tenderList;

            var dailyObj = self.dailyReport.getDailyReportObj([t], tenderList, discountList, null, tender, "");

            _obj.transaction.tips = dailyObj.TOTAL_CARD_TIPS;
            //console.log(4090, dailyObj)
            //console.log(4091, JSON.stringify(dailyObj.tenderPayment));

            _obj.payment.map(function (p) {
                p.paymentName = "";
                p.records = dailyObj.tenderPayment[p.paymentType];
                p.paymentAmount = 0;
                p.records.forEach(function (pay) {
                    p.paymentAmount += pay.paymentValue
                })
                /*p.paymentAmount = p.records.reduce((previousValue, currentValue) => {
                    return previousValue.paymentValue + currentValue.paymentValue;
                }, 0);*/

                tender.forEach(function (t) {
                    if (t.tenderCode == p.paymentType) {
                        p.paymentName = t.tenderName1;
                        return false;
                    }
                })
                return p;
            })


            //console.log(4095, _obj.payment);


            /*var isHaveCard = false;
            for (var i = 0; i < tenderList.length; i++) {
                var _o = tenderList[i];
                var payMsg = _o.paymentType;
                var tenderObj = RAS.tender.filter((t) => { return t.tenderCode == _o.paymentType })[0];
                _obj.payment.push({ "paymentType": tenderObj.tenderName1, "paymentValue": _o.paymentValue, "remark": _o.remark, "paymentCode": _o.paymentType });
                if (_o.paymentType == 'cash') isHaveCard = true;
            }

            if (_obj.payment.length == 1) {
                if (_obj.payment[0].paymentCode != 'cash')
                    _obj.transaction.tips = _obj.payment[0].paymentValue - _obj.transaction.sales;
                else _obj.transaction.tips = 0;
            } else {
                if (isHaveCard) {
                    _obj.transaction.tips = 0;
                }
                else {
                    var billPrice = _obj.transaction.sales;
                    _obj.payment.forEach(function (p, i) {
                        billPrice -= p.paymentValue;
                        if (billPrice < 0 && i == _obj.payment.length - 1) _obj.transaction.tips = Math.abs(billPrice);
                    })
                }
            }*/

            var totaldiscount = 0;
            _obj.discount = [];
            for (var i = 0; i < discountList.length; i++) {
                var _o = discountList[i];

                var discountMsg = _o.discountCode;
                discount.forEach((d) => {
                    if (d.discountCode == _o.discountCode) {
                        discountMsg = d.discountName1;
                    }
                })

                //var dvForPrint = discountValue;
                //if (_o.discountValue > 0) {
                //    dvForPrint = "-" + dvForPrint;
                //}
                //_obj.discount.push({ "discountCode": discountMsg, "discountValue": S(dvForPrint).padLeft(10).s })
                _obj.discount.push({ "discountCode": discountMsg, "discountValue": _o.discountValue })
                _obj.transaction.sales -= _o.discountValue;
                //totaldiscount += _o.discountValue;
            }

            sortDbItem(transactionD);

            if (transactionD.length != 0) {
                _obj.items = [];
                var lastIsFreeCombo = false;
                var lastItemQty = 0;
                for (var i = 0; i < transactionD.length; i++) {
                    var currentItem = transactionD[i];

                    var items = [];
                    var items = getItemByItem(currentItem);
                    //console.log(4164, items);
                    var name1, name2, name3;
                    name1 = name2 = name3 = "";

                    if (items.length > 0) {
                        var name1 = items[0].name1;
                        var name2 = items[0].name2;
                        var name3 = items[0].name3;
                        //if (items[0].name != undefined) {
                        //    if (items[0].name[config.lang1] != undefined)
                        //        name1 = items[0].name[config.lang1];
                        //    if (items[0].name[config.lang2] != undefined)
                        //        name2 = items[0].name[config.lang2];
                        //    if (items[0].name[config.lang3] != undefined)
                        //        name3 = items[0].name[config.lang3];
                        //}
                    }
                    if (currentItem.customName != "") {
                        name1 = name2 = name3 = currentItem.customName;
                    }

                    //if (name1 != "") {
                    //    var nameLenght = 25 - countDoubleByteChar(name1);
                    //    name1 = S(name1).padRight(nameLenght).s
                    //}
                    var itemTotalPrice = 0;

                    if (currentItem.type == "I" || currentItem.type == "T") {
                        lastItemQty = currentItem.qty;
                        itemTotalPrice = lastItemQty * currentItem.price;
                    } else {
                        itemTotalPrice = lastItemQty * currentItem.price * currentItem.qty;
                    }

                    //var itp = S("$" + itemTotalPrice.format(1, 3)).padLeft(10).s;
                    var itp = itemTotalPrice;
                    //var up = S("$" + currentItem.price.format(1, 3)).padLeft(10).s;
                    var up = currentItem.price;

                    if (currentItem.type == "I") {
                        if (itemTotalPrice == 0) {
                            lastIsFreeCombo = true;
                        } else {
                            lastIsFreeCombo = false;
                        }
                    }

                    if (currentItem.type != "K" && currentItem.type != "T" && !(currentItem.type == "I" && itemTotalPrice == 0 && !_food.config.dailyBillDetailReport.printZeroPriceItem) && !(lastIsFreeCombo && !_food.config.dailyBillDetailReport.printZeroPriceItem)) {
                        if (currentItem.itemId != 'O9999') {
                            _obj.items.push({ code: currentItem.itemId, type: currentItem.type, desc1: name1, desc2: name2, desc3: name3, qty: currentItem.qty, unitPrice: up, itemTotalPrice: itp, time: currentItem.time, voidIndex: currentItem.voidIndex, voidRemark: currentItem.voidRemark, staff: currentItem.staffId, index: currentItem.seq });
                        }
                    }
                }





                //var totalPrice = _obj.price + _obj.serviceCharge - totaldiscount;

                //var totalPrice = _obj.price + _obj.serviceCharge + _obj.remainings;

                //totalPrice = Math.round(totalPrice);
                //_obj.totalPrice = "$" + totalPrice.format(1, 3);
                //_obj.totalPrice = totalPrice;
                //_obj.price = S("$" + _obj.price.format(1, 3)).padLeft(10).s;
                //_obj.price = _obj.price;
                //_obj.serviceCharge = S("$" + _obj.serviceCharge.format(1, 3)).padLeft(10).s;
                //_obj.serviceCharge = _obj.serviceCharge;

                //var priceWithServiceCharge = _obj.price + _obj.serviceCharge;

                //var r = (priceWithServiceCharge % 1).toFixed(1);
                //if (r > 0.4) {
                //    r = (1 - r).toFixed(1);
                //} else if (r != 0) {
                //    r = '-' + r;
                //}
                //_obj.remainings = parseFloat(r);

            }

            if (_obj.items) {
                var filterVoidItemList = [];
                _obj.items.forEach(function (food) {
                    if (food.type == "M" || food.type == "K") return true;
                    food.Tqty = food.qty;
                    if (food.type == "I" || food.type == "T") {
                        var tempVoidItem = {};
                        tempVoidItem.Package = food;
                        tempVoidItem.options = [];
                        filterVoidItemList.push(tempVoidItem);
                    }
                    else {
                        food.Tqty = filterVoidItemList[filterVoidItemList.length - 1].Package.qty;
                        filterVoidItemList[filterVoidItemList.length - 1].options.push(food);
                    }
                });

                filterVoidItemList.forEach(function (food) {
                    if (food.Package.voidIndex != '-1') {
                        var tempFood = filterVoidItemList.filter(function (voidItem) {
                            return voidItem.Package.index == food.Package.voidIndex;
                        })
                        tempFood[0].Package.qty = tempFood[0].Package.qty + food.Package.qty;
                        var tempItemPrice = tempFood[0].Package.qty * tempFood[0].Package.unitPrice;
                        tempFood[0].Package.itemTotalPrice = tempItemPrice
                        //tempFood[0].Package.itemTotalPrice = tempFood[0].Package.itemTotalPrice - food.Package.itemTotalPrice;
                        tempFood[0].options.forEach(function (opts) {
                            //opts.qty = opts.qty + food.Package.qty;
                            opts.Tqty = opts.Tqty + food.Package.Tqty;
                            tempItemPrice = tempItemPrice;
                            opts.itemTotalPrice = tempItemPrice
                        });
                    }
                })

                filterVoidItemList = filterVoidItemList.filter(function (food) {
                    return food.Package.qty > 0;
                })

                _obj.items = [];
                filterVoidItemList.forEach(function (food) {
                    _obj.items.push(food.Package);
                    food.options.forEach(function (opts) {
                        _obj.items.push(opts);
                    })
                })
            }

            objList.push(_obj);
        });




        //console.log(result[1]);
        obj.transactionList = objList;
        //console.log(JSON.stringify(objList));
        return obj;

    }

    dailyBillDetailReport.prototype.getCallbackResult = function (callback) {

        var self = this;
        var DB = new database();
        DB.executeStatement(self.getQuery(), (err, result) => {
            var obj = self.getResult(result);
            callback(obj);
            /*if (args.isPrint) {
                self.printReport(obj);
            }*/
        })

    }


}